/*
   Class for extra ParaFile_adj characteristics panel
 */

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.text.*;
import java.util.*;

/**
 * This panel allows to define all the characteristics of an extra fitted parameter file.
 */
public class JPAdj extends    JPanel
                   implements ActionListener {

    // panels
    private JPanel pouest;
    private JPanel pcentre;
    private JPanel jpopt1;

    //
    private JFAdj    calling;                                        // calling JFAdj
    private int      adjnb;                                          // adj number
    private String[] ntransp;                                        // possible transition names
    private String   npack;                                          // package  name
    private String   nmol;                                           // molecule name

    //
    private JButton             jbtrans;                             // transition jb
    private String              ntradj;                              // transition name
    private JLabel              ltrans;                              // transition jl
    private int                 itrans;                              // transition index
    private JButton             jbparaf;                             // ParaFile jb
    private String              nparaf;                              // ParaFile name
    private JLabel              lparaf;                              // ParaFile jl
    private JRadioButton        jrbpol;                              // pol  jrb

    private String playd;                                            // XTDS installation directory
    private String lnsep;                                            // line separator
    private String fisep;                                            // file separator

/////////////////////////////////////////////////////////////////////

    /**
     * Constructs a new JPAdj.
     */
    public JPAdj( JFAdj ccalling, int cadjnb, String[] cntransp, String cnpack, String cnmol ) {

        calling  = ccalling;                                         // calling JFAdj
        adjnb    = cadjnb;                                           // adj number
        ntransp  = cntransp;
        npack    = cnpack;                                           // package  name
        nmol     = cnmol;                                            // molecule name

        playd = System.getProperty("xtds.home");                     // XTDS installation directory
        lnsep = System.getProperty("line.separator");
        fisep = System.getProperty("file.separator");
        setName("JPAdj");                                            // for help files
        setLayout(new BorderLayout());

        // local panels
        pouest  = new JPanel(new GridLayout(3,0,5,5));
        pcentre = new JPanel(new GridLayout(3,0,5,5));

        // ParaFile name
        jbtrans = new JButton("Transition");                         // transition button
        jbtrans.setBackground(Color.WHITE);
        jbtrans.addActionListener(this);
        pouest.add(jbtrans);
        ntradj = "";                                                 // transition name
        ltrans = new JLabel("");                                     // transition jl
        pcentre.add(ltrans);
        // ParaFile name
        jbparaf = new JButton("Parameter File");                     // ParaFile button
        jbparaf.setBackground(Color.WHITE);
        jbparaf.addActionListener(this);
        pouest.add(jbparaf);
        nparaf = "";                                                 // ParaFile name
        lparaf = new JLabel("");                                     // ParaFile jl
        pcentre.add(lparaf);
        // options
        jpopt1 = new JPanel(new GridLayout(0,6,5,5));
        // pol
        jrbpol = new JRadioButton("pol");                            // pol jrb
        jrbpol.setBackground(Color.WHITE);
        jrbpol.addActionListener(this);
        jpopt1.add(jrbpol);

        pcentre.add(jpopt1);

        add(pouest ,"West"  );
        add(pcentre,"Center");
        setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLUE,4),""+adjnb));

    }

/////////////////////////////////////////////////////////////////////

    /**
     * Processes the events.
     */
    public void actionPerformed(ActionEvent evt) {

        // Transition
        if(evt.getSource() == jbtrans) {
            ntradj = (String)JOptionPane.showInputDialog( null, "Choose a transition :"+lnsep, "Transition",
                                                          JOptionPane.PLAIN_MESSAGE, null, ntransp, ntransp[0]);
            // choice cancelled
            if( ntradj == null ) {
                ntradj = "";
            }
            ltrans.setText(ntradj);
            return;
        }

        // ParaFile
        if(evt.getSource() == jbparaf) {
            JFileChooser jfcparaf = new JFileChooser(playd+fisep+"packages"+fisep+npack+fisep+"para"+fisep+nmol);  // default choice directory
            jfcparaf.setSize(400,300);
            jfcparaf.setFileSelectionMode(JFileChooser.FILES_ONLY);  // files
            jfcparaf.setDialogTitle("Define the assignment file or directory to be used");
            Container parent = jbparaf.getParent();
            int choice = jfcparaf.showDialog(parent,"Select");       // Dialog, Select
            if (choice == JFileChooser.APPROVE_OPTION) {
                nparaf = jfcparaf.getSelectedFile().getAbsolutePath();
                lparaf.setText(nparaf);
                calling.pack();                                          // for long names
            }
            return;
        }

    }

/////////////////////////////////////////////////////////////////////

    /**
     * Test criteria
     */
    public boolean test() {

        if( ntradj == "" ) {
            JOptionPane.showMessageDialog(null,"Extra ParaFile_adj #"+adjnb+lnsep+
                                               "You have to define the Transition");
            return false;
        }
        if( nparaf == "" ) {
            JOptionPane.showMessageDialog(null,"Extra ParaFile_adj #"+adjnb+lnsep+
                                               "You have to define the Parameter File");
            return false;
        }
        return true;
    }

    /**
     * get transition name
     */
     public String getNtradj() {

        return ntradj;
    }

    /**
     * get ParaFile
     */
    public String getParaFile() {

      return nparaf;
    }

    /**
     * is jrbpol selected?
     */
    public boolean isPolSelected() {

        return jrbpol.isSelected();
    }

}
