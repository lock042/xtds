/*
   Class for polyad setting panel
 */

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

/**
 * This panel allows to define all the characteristics of a polyad.
 */
public class JPPolyad extends    JPanel
                      implements ActionListener {

    // panels
    private JPanel pouest;
    private JPanel pcentre;

    // variables
    private CJPint   calling;                                        // calling object
    private Polyad   polyad;                                         // associated polyad
    private String[] tpsc;                                           // polyad scheme coeficients array
    private JPanel   pnc;                                            // calling panel
    private int      nbfixdvo;                                       // nb of fixed development orders

    private int nbvqn;                                               // nb of vibrationnal quantum numbers
    private String[]    tdvo;                                        // development order choice array
    private String      npol;                                        // polyad number string
    private JComboBox   jcbpol;                                      // polyad number jcb
    private JPanel      jpqal;                                       // polyad quanta limit panel
    private JComboBox[] jcbqal;                                      // polyad quanta limit jcb
    private int         cvqal;                                       // current polyad quanta limit
    private int         polnb;                                       // polyad number
    private JPanel      jpdvo;                                       // hamiltonian development order panel
    private JComboBox[] jcbdvo;                                      // hamiltonian development order jcb
    private String      ndvo;                                        // hamiltonian development order name
    private int         cvdvo;                                       // hamiltonian development order value
    private boolean     ispolar;                                     // polarizability development order request
    private JPanel      jppdvo;                                      // polarizability development order panel
    private JComboBox[] jcbpdvo;                                     // polarizability development order jcb
    private String      npdvo;                                       // polarizability development order name
    private int         cvpdvo;                                      // polarizability development order value

////////////////////////////////////////////////////////////////////

    /**
     * Constructs a new JPPolyad without polarizability devel. order management.
     *
     * @param ccall     calling CJPXxx panel (managing JPPEvent)
     * @param cpolyad   associated polyad
     * @param ctpsc     polyad scheme coeficients array
     * @param cpnc      JPPolyad will be painted in this container
     */
    public JPPolyad(CJPint ccall, Polyad cpolyad, String[] ctpsc, JPanel cpnc) {

        // calling parameters
        calling = ccall;
        polyad  = cpolyad;
        tpsc = (String[]) ctpsc.clone();
        pnc  = cpnc;

        setName("JPPolyad");                                         // for help files

        nbvqn     = polyad.getNbvqn();                               // nb of vibrational quantum numbers of associated polyad
        nbfixdvo  = 0;
        ispolar   = polyad.isSetPolar();                             // polarizability status

        // local panels
        if( ispolar ) {
            pouest  = new JPanel(new GridLayout(4,0,5,5));
            pcentre = new JPanel(new GridLayout(4,0,5,5));
        }
        else {
            pouest  = new JPanel(new GridLayout(3,0,5,5));
            pcentre = new JPanel(new GridLayout(3,0,5,5));
        }

        // choose Polyad Number
        pouest.add(new JLabel("Polyad Number "));
        jcbpol = new JComboBox();
        jcbpol.addItem("");
        for( int i=0; i<tpsc.length; i++ ) {
            jcbpol.addItem(tpsc[i]);
        }
        jcbpol.setSelectedItem("");
        jcbpol.setBackground(Color.WHITE);
        jcbpol.addActionListener(this);
        pcentre.add(jcbpol);
        // choose quanta limit
        pouest.add(new JLabel("Quanta Limit"));
        // choose Development Order
        if( ispolar ) {
            pouest.add(new JLabel("Hamiltonian  Devel. Order "));
            pouest.add(new JLabel("Polarizability Devel. Order "));
        }
        else {
            pouest.add(new JLabel("Devel. Order"));
        }
        tdvo = new String[10];                                       // order 0 -> 9
        for(int i=0; i<10; i++) {
            tdvo[i] = String.valueOf(i);
        }
        pnc.add(pouest, "West");
        pnc.add(pcentre,"Center");
        pnc.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLUE,4),cpolyad.getName()));

    }

/////////////////////////////////////////////////////////////////////

    /**
     * Processes the events.
     */
    public void actionPerformed(ActionEvent evt) {

        // POLYAD
        boolean evtfound = false;
        int type = 0;
        int indice = 0;
        // polyad number
        if (evt.getSource() == jcbpol) {                             // polyad number
            npol = (String) jcbpol.getSelectedItem();
            removeJPqaldvo();                                        // remove dvo panel(s)
            nbfixdvo = 0;
            if(npol == "") {
                polyad.unsetPolnb();
            }
            else {
                polnb = Integer.parseInt(npol);
                polyad.setPolnb( polnb );
                addJPqaldvo();                                       // display dvo panel(s)
            }
            evtfound = true;
        }
        if( ! evtfound ) {
            // Quanta Limit
            for (int i=0; i<nbvqn; i++) {
                if(evt.getSource() == jcbqal[i] ) {                  // whatever qal
                    cvqal =  Integer.parseInt((String) jcbqal[i].getSelectedItem());  // save it
                    polyad.setVqal(cvqal, i);
                    evtfound = true;
                    type     = 1;                                    // be a sign of qal event
                    indice   = i;                                    // i th element
                    break;
                }
            }
        }
        if( ! evtfound ) {
            // Hamiltonian Development Order
            for ( int i=0; i<polnb+1; i++) {
                if(evt.getSource() == jcbdvo[i]) {
                    evtfound = true;
                    type     = 2;                                    // be a sign of hamiltonian dvo event
                    indice   = i;                                    // i th element
                    ndvo     = (String) jcbdvo[i].getSelectedItem();
                    if( ndvo != "" ) {
                        cvdvo = Integer.parseInt(ndvo);              // save value
                        polyad.setVdvo(cvdvo, i);
                        polyad.autoTestdef();
                    }
                    else {
                        polyad.unsetVdvo(i);
                    }
                    break;
                }
            }
        }
        if( ! evtfound ) {
            // Polarizability Development Order
            if( ispolar ) {
                for ( int i=0; i<polnb+1; i++) {
                    if(evt.getSource() == jcbpdvo[i]) {
                        evtfound = true;
                        type     = 3;                                    // be a sign of poalrizability dvo event
                        indice   = i;                                    // i th element
                        npdvo    = (String) jcbpdvo[i].getSelectedItem();
                        if( npdvo != "" ) {
                            cvpdvo = Integer.parseInt(npdvo);            // save value
                            polyad.setVpdvo(cvpdvo, i);
                            polyad.autoTestdef();
                        }
                        else {
                            polyad.unsetVpdvo(i);
                        }
                        break;
                    }
                }
            }
        }
        // an event occured, report to calling object
        calling.JPPEvent(polyad, type, indice);
    }

/////////////////////////////////////////////////////////////////////

    // add qal and dvo panels
    private void addJPqaldvo() {

        // quanta limit
        jpqal = new JPanel(new GridLayout(0,2*nbvqn+1,0,5));
        jpqal.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        jcbqal = new JComboBox[nbvqn];
        for ( int i=0; i<nbvqn; i++) {
            if(i == 0) {                                             // set label
                jpqal.add(new JLabel(" vi <="));
            }
            jcbqal[i] = new JComboBox(tpsc);
            jcbqal[i].setSelectedItem(tpsc[polnb]);                  // default polnb
            polyad.setVqal(polnb, i);
            jcbqal[i].addActionListener(this);
            jcbqal[i].setBackground(Color.WHITE);
            jpqal.add(new JLabel(""));
            jpqal.add(jcbqal[i]);
        }
        pcentre.add(jpqal);

        // hamiltonian dev. order
        jpdvo = new JPanel(new GridLayout(0,2*(polnb+1),0,5));
        jpdvo.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        jcbdvo = new JComboBox[polnb+1];
        for ( int i=0; i<polnb+1; i++) {
            jpdvo.add(new JLabel(" P"+i+" D ",null,JLabel.RIGHT));
            jcbdvo[i] = new JComboBox();
            jcbdvo[i].addItem("");
            for( int j=0; j<tdvo.length; j++ ) {
                jcbdvo[i].addItem(tdvo[j]);
            }
            if( i >= nbfixdvo ) {
                jcbdvo[i].setSelectedItem("");
                polyad.unsetVdvo(i);
                jcbdvo[i].addActionListener(this);
            }
            else {
                jcbdvo[i].setSelectedItem(tdvo[polyad.getVdvo(i)]);
                jcbdvo[i].setEnabled(false);
            }
            jcbdvo[i].setBackground(Color.WHITE);
            jpdvo.add(jcbdvo[i]);
        }
        pcentre.add(jpdvo);                                          // add panel

        // polarizability dev. order
        if( ispolar ) {
            jppdvo = new JPanel(new GridLayout(0,2*(polnb+1),0,5));
            jppdvo.setBorder(BorderFactory.createLineBorder(Color.BLACK));
            jcbpdvo = new JComboBox[polnb+1];
            for ( int i=0; i<polnb+1; i++) {
                jppdvo.add(new JLabel(" P"+i+" D ",null,JLabel.RIGHT));
                jcbpdvo[i] = new JComboBox();
                jcbpdvo[i].addItem("");
                for( int j=0; j<tdvo.length; j++ ) {
                    jcbpdvo[i].addItem(tdvo[j]);
                }
                if( i >= nbfixdvo ) {
                    jcbpdvo[i].setSelectedItem("");
                    polyad.unsetVpdvo(i);
                    jcbpdvo[i].addActionListener(this);
                }
                else {
                    jcbpdvo[i].setSelectedItem(tdvo[polyad.getVpdvo(i)]);
                    jcbpdvo[i].setEnabled(false);
                }
                jcbpdvo[i].setBackground(Color.WHITE);
                jppdvo.add(jcbpdvo[i]);
            }
            pcentre.add(jppdvo);                                     // add panel
        }

        jpqal.revalidate();                                          // re-display it if a suppression occured
        jpdvo.revalidate();                                          // re-display it if a suppression occured
        if( ispolar ) {
            jppdvo.revalidate();                                     // re-display it if a suppression occured
        }
    }

    // remove qal and dvo panels
     private void removeJPqaldvo() {

        if(jpqal != null) {
            pcentre.remove(jpqal);
            pcentre.remove(jpdvo);
            if( ispolar ) {
                pcentre.remove(jppdvo);
            }
            jpqal = null;
            jpdvo = null;
            if( ispolar ) {
                jppdvo = null;
            }
            pcentre.repaint();
            if(polyad.isSetPolnb() ) {
                polyad.setPolnb( polyad.getPolnb() );                // reset qal and dvos
            }
        }
    }

/////////////////////////////////////////////////////////////////////

    /**
     * Sets the value of the i<sup>th</sup> quanta limit.
     * @param cvqal   value of the quanta limit
     * @param i       index of the quanta limit
     *
     */
    public void setJcbqal(int cvqal, int i) {

        jcbqal[i].removeActionListener(this);                        // to keep clear of internal event
        jcbqal[i].setSelectedItem(tpsc[cvqal]);
        polyad.setVqal(cvqal,i);
        jcbqal[i].addActionListener(this);                           // manage external events
    }

    /**
     * Sets the value of the i<sup>th</sup> hamiltonian developpment order.
     * @param cvdvo   value of the hamiltonian developpment order
     * @param i       index of the hamiltonian developpment order
     *
     */
    public void setJcbdvo(int cvdvo, int i) {

        jcbdvo[i].removeActionListener(this);                        // to keep clear of internal event
        jcbdvo[i].setSelectedItem(tdvo[cvdvo]);
        polyad.setVdvo(cvdvo,i);
        polyad.autoTestdef();
        jcbdvo[i].addActionListener(this);                           // manage external events
    }

    /**
     * Sets the value of the i<sup>th</sup> polarizability developpment order.
     * @param cvpdvo  value of the polarizability developpment order
     * @param i       index of the polarizability developpment order
     *
     */
    public void setJcbpdvo(int cvpdvo, int i) {

        if( ispolar ) {
            jcbpdvo[i].removeActionListener(this);                   // to keep clear of internal event
            jcbpdvo[i].setSelectedItem(tdvo[cvpdvo]);
            polyad.setVpdvo(cvpdvo,i);
            polyad.autoTestdef();
            jcbpdvo[i].addActionListener(this);                      // manage external events
        }
    }

    /**
     * Resets JPPolyad.
     * <br>Clears polyad number, removes quanta limit and development order panels.
     */
    public void resetJP() {

        jcbpol.removeActionListener(this);                           // to keep clear of internal event
        jcbpol.setSelectedItem("");                                  // unset polyad number
        polyad.unsetPolnb();
        jcbpol.addActionListener(this);                              // manage external events
        removeJPqaldvo();                                            // remove qal and dvo panels
    }

    /**
     * Sets default quanta limit and hamiltonian fixed development order.
     *
     * @param cdepqal   default quanta limit
     * @param cfixdvo   hamiltonian    fixed development order
     */
    public void setFix(int[] cdepqal, int[] cfixdvo) {

        nbfixdvo = 0;
        removeJPqaldvo();
        nbfixdvo = cfixdvo.length;
        for ( int i=0; i<nbfixdvo; i++ ) {
            polyad.setVdvo(cfixdvo[i], i);
        }
        polyad.autoTestdef();
        addJPqaldvo();
        for( int i=0; i<nbvqn; i++ ) {
            setJcbqal(cdepqal[i], i);
        }
     }
    /**
     * Sets default quanta limit, hamiltonian and polarizability fixed development order.
     *
     * @param cdepqal   default quanta limit
     * @param cfixdvo   hamiltonian    fixed development order
     * @param cfixpdvo  polarizability fixed development order
     */
    public void setFix(int[] cdepqal, int[] cfixdvo, int[] cfixpdvo) {

        nbfixdvo = 0;
        removeJPqaldvo();
        nbfixdvo = cfixdvo.length;
        if( nbfixdvo != cfixpdvo.length ) {                          // have to be equally defined
            return;                    
        }
        for ( int i=0; i<nbfixdvo; i++ ) {
            polyad.setVdvo(cfixdvo[i], i);
            polyad.setVpdvo(cfixpdvo[i], i);
        }
        polyad.autoTestdef();
        addJPqaldvo();
        for( int i=0; i<nbvqn; i++ ) {
            setJcbqal(cdepqal[i], i);
        }
    }

}
