/**
 * This class allows to sort XY data.
 */
public class XYPoint implements Comparable {

    private double x;                                                // X
    private double y;                                                // Y

    /**
     * Construct a new XYPoint.
     *
     * @param cx  X
     * @param cy  Y
     */
    public XYPoint(double cx, double cy) {

        x = cx;                                                      // X
        y = cy;                                                      // Y
    }

/////////////////////////////////////////////////////////////////////

    /**
     * Get X.
     */
    public double getX() {

        return x;
    }

    /**
     * Get Y.
     */
    public double getY() {

        return y;
    }

    /**
     * Compare X then Y.
     *
     * @param cxyp the XYPoint to compare to
     */
    public int compareTo(Object cxyp) {

        if( ! (cxyp instanceof XYPoint) ) {                          // not the right object
            throw new ClassCastException();
        }
        double delta = ((XYPoint)cxyp).getX() - x;                   // compare X
        if     ( delta < 0 ) {
            return  1;
        }
        else if( delta > 0 ) {
            return -1;
        }
        else {
            delta = ((XYPoint)cxyp).getY() - y;                      // compare Y
            if     ( delta < 0 ) {
                return  1;
            }
            else if( delta > 0 ) {
                return -1;
            }
        }
        return 0;                                                    // equal
    }

}
