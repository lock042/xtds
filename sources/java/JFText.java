/*
 * Class to show a string array
 */

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.text.*;
import java.util.*;
import javax.swing.*;

/////////////////////////////////////////////////////////////////////

/**
 * Window to show a string array.
 */
public class JFText extends JFrame {

    private PanAffich calling;
    // panels
    private JPanel pcentre;

    // to show the string array
    private JScrollPane jsp;                                         // with lifts
    private JTextArea   jta;

    private Font mono15;                                             // Monospaced 15 font

/////////////////////////////////////////////////////////////////////

    /**
     * Constructs a JFText.
     *
     * @param cstr      string array
     * @param ccalling  calling PanAffich
     * @param cp_x      x location of frame
     * @param cp_y      y location of frame
     */
    public JFText( String[] cstr , PanAffich ccalling, int cp_x, int cp_y ) {

        super("");                                                   // main window

        calling = ccalling;                                          // calling PanAffich

        addWindowListener(new WindowAdapter() {                      // clean end
            public void windowClosing(WindowEvent e) {
                if( calling != null ) {
                    // let know to calling that this frame is closed
                    calling.unPredsel();
                }
                dispose();                                           // free window
            }
        });
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        mono15 = new Font("Monospaced", Font.PLAIN, 15);

        getContentPane().setLayout(new BorderLayout());
        JPanel jp = new JPanel(new BorderLayout());
        jp.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLUE,4),null));
        getContentPane().add(jp,"Center");

        // panels
        pcentre = new JPanel();

        // center
        jta = new JTextArea();
        int ml = 0;                                                  // max length of strings
        for( int i=0; i<cstr.length; i++ ) {
            if( cstr[i].length() > ml ) {
                ml = cstr[i].length();
            }
        }
        jta.setColumns(ml);                                          // jta width
        for( int i=0; i<cstr.length; i++ ) {
            jta.append(cstr[i]);                                     // add strings
        }
        jta.setBackground(Color.BLACK);
        jta.setForeground(Color.WHITE);
        jta.setEditable(false);                                      // not editable
        jta.setFont( mono15 );
        jta.setLineWrap(false);                                      // no line wrap
        jta.setRows(8);                                              // nb of lines
        jsp = new JScrollPane(jta, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);  // with lifts
        try {
            jta.setCaretPosition(0);                                 // at the top of the text
        }
        catch(IllegalArgumentException iae) {
        }
        pcentre.add(jsp);

        // insert panels
        jp.add(pcentre,"Center");

        setLocation( cp_x, cp_y );
        pack();
    }

}
