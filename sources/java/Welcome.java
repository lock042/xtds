/*
 * Class for XTDS welcome message
 */

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;

/**
 * This panel shows the welcome message to XTDS application.
 */
public class Welcome extends JPanel {

    private JLabel      jl;
    private JTextArea   ta;
    private JScrollPane jsp;
    private String lnsep;                                            // line separator
    private Font   serif14;                                          // Serif 14 font

/////////////////////////////////////////////////////////////////////

    /**
     * Constructs a new Welcome.
     */
    public Welcome() {

        lnsep = System.getProperty("line.separator");
        serif14 = new Font("Serif", Font.PLAIN, 14);
        setName("Welcome");                                          // for help files
        setLayout(new BorderLayout());
        setBackground(Color.WHITE);

        ta = new JTextArea(lnsep+lnsep+"The Dijon spectroscopy group (ICB - UMR CNRS 5209) has developed powerful techniques based on group theory and tensorial formalism [1] in order to analyze and simulate absorption and Raman spectra of molecules with various symmetries. Software packages and databases [2] implementing these tools have been created. This concerns:"+lnsep+lnsep+
        "XY4 \t[3] (tetrahedral, STDS package)"+lnsep+
        "XY6 \t[4] (octahedral, HTDS package)"+lnsep+
        "XY2Z2 \t[5] (C2v symmetry, C2vTDS package)"+lnsep+
        "XY3Z \t[6] (C3v symmetry, C3vTDS package)"+lnsep+
        "XY3Z \t[7] (C3vs rovibronic symmetry, C3vsTDS package)"+lnsep+
        "XY5Z \t[8] (C4v symmetry, C4vTDS package)"+lnsep+
        "X2Y4 \t[9] (D2h symmetry and D2hTDS package)"+lnsep+lnsep+
        "These packages all consist in FORTRAN 77 programs called by UNIX scripts."+lnsep+lnsep+
"Here is a user-friendly java-based interface, called XTDS, which allows to interactively build and launch spectrum calculation or analysis jobs using any of the above-mentioned packages. This software runs on UNIX systems (UNIX, Linux or Mac OS X workstations). It allows the treatment of complex spectroscopic problems, including molecules with complex polyads, like methane for instance. The user can define the polyad scheme for the molecule under consideration. All interaction terms up to a given order of the development are automatically determined for both the Hamiltonian or transition moments (dipole moment or polarizability). Least-squares fits of experimental data can also be run interactively."+lnsep+lnsep+
"[1] V. Boudon, J.-P. Champion, T. Gabard, M. Loete, F. Michelot, G. Pierre, M. Rotger, Ch. Wenger and M. Rey, J. Mol. Spectrosc., 228, 620-634 (2003)"+lnsep+
"[2] http://icb.u-bourgogne.fr/OMR/SMA/SHTDS"+lnsep+
"[3] Ch. Wenger and J.-P. Champion, J. Quant. Spectrosc. Radiat. Transfer, 59, 471-480 (1998)"+lnsep+
"[4] Ch. Wenger, V. Boudon, J.-P. Champion and G. Pierre, J. Quant. Spectrosc. Radiat. Transfer, 66, 1-16 (2000)"+lnsep+
"[5] Ch. Wenger, M. Rotger and V. Boudon, J. Quant. Spectrosc. Radiat. Transfer, 93, 429-446 (2005)"+lnsep+
"[6] Ch. Wenger, A. El Hilali and V. Boudon, in preparation"+lnsep+
"[7] Ch. Wenger, A. El Hilali and V. Boudon, in preparation"+lnsep+
"[8] Ch. Wenger, M. Rotger and V. Boudon, J. Quant. Spectrosc. Radiat. Transfer, 74, 621-636 (2002)"+lnsep+
"[9] Ch. Wenger, W. Raballand, M. Rotger and V. Boudon, J. Quant. Spectrosc. Radiat. Transfer, 95, 521-538 (2005)"+lnsep+lnsep+lnsep+
"ICB, UMR 5209 CNRS - Universite de Bourgogne, 9 Av. A. Savary, BP 47 870, F-21078 Dijon, France"
);

        ta.setFont( serif14 );
        ta.setLineWrap(true);                                        // wrappable lines
        ta.setWrapStyleWord(true);                                   // at word boundaries
        ta.setEditable(false);                                       // not editable
        ta.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLUE,4), " Welcome to the XTDS Technology"));

        jsp = new JScrollPane(ta, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        add(jsp, BorderLayout.CENTER);
    }

}
