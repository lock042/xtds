/*
 * Class for polyad definition
 */

import java.io.*;
import java.util.*;
import javax.swing.*;

/**
 * This class defines a polyad.
 */
public class Polyad {

    // instance variables
    private int     polnb;                                           // polyad number
    private int[]   vqal;                                            // quanta limit
    private int[]   vdvo;                                            // hamiltonian    development order
    private boolean ispolar;                                         // polazirability management status
    private int[]   vpdvo;                                           // polarizability development order
    private boolean fullydef;                                        // fully defined
    private String  name;                                            // its name
    // variables
    private int nbvqn;                                               // nb of vibrational quantum numbers
    private int[] ind;                                               // cvs sorting indexes
    private int[] cvs;                                               // current vibrational state
    private ArrayList altcvs;                                        // to save cvs arrays
    private int nc;
    private int[] nbvs;                                              // nb of vs
    private int itcvs;
    private boolean finvs;                                           // end of vs calculation

    // returned by getXxxx
    private int    ret_nbvs;                                         // nb of vibrationnal states
    private int    ret_vdvo;                                         // hamiltonian    development order value
    private String ret_str;                                          // cvs string

    private String lnsep;                                            // line separator

/////////////////////////////////////////////////////////////////////

    /**
     * Constructs a new Polyad.
     *
     * @param cnbvqn   number of vibrational quantum numbers
     */
    public Polyad( int cnbvqn ) {

        nbvqn = cnbvqn;                                              // nb of vqn

        lnsep = System.getProperty("line.separator");

        // initialisation
        fullydef = false;                                            // not fully defined
        polnb    = -1;                                               // polyad number undefined
        vqal     = new int[nbvqn];                                   // rightly sized array
        Arrays.fill(vqal, -1);                                       // undefined content
        cvs      = new int[nbvqn];                                   // rightly sized array
        name     = null;                                             // undefined name
        ispolar  = false;                                            // polarizability not managed
    }

////////////////////////////////////////////////////////////////////

    /**
     * Returns the number of vibrational quantum numbers.
     */
    public int getNbvqn() {

        return nbvqn;
    }

    /**
     * Sets the polyad name.
     *
     * @param cname   polyad name
     */
    public void setName( String cname ) {

        name = cname;
    }

    /**
     * Returns the polyad name.
     */
    public String getName() {

        if( name == null ) {
            return "--Undefined Polyad Name--";
        }
        else {
            return name;
        }
    }

    /**
     * Sets the polyad number.
     *
     * @param cpolnb   polyad number
     */
    public void setPolnb( int cpolnb ) {

        if( cpolnb < 0 ) {                                           // prohibited
            unsetPolnb();
        }
        else {
            polnb = cpolnb;
            // initialisation
            vdvo  = new int[polnb+1];
            Arrays.fill(vdvo, -1);
            vpdvo = new int[polnb+1];
            Arrays.fill(vpdvo, -1);
            nbvs  = new int[polnb+1];
            Arrays.fill(nbvs, -1);
        }
    }

    /**
     * Unsets the polyad number.
     */
    public void unsetPolnb() {

        polnb    = -1;
        Arrays.fill(vqal, -1);
        vdvo     = null;
        vpdvo    = null;
        nbvs     = null;
        fullydef = false;
    }

    /**
     * Is the polyad number set ?
     */
    public boolean isSetPolnb() {

        if( polnb == -1 ) {
            return false;
        }
        return true;
    }

    /**
     * Returns the polyad number.
     */
    public int getPolnb() {

        return polnb;
    }

    /**
     * Sets a quanta limit value.
     *
     * @param cvqal   quanta limit value
     * @param ci      quanta limit index
     */
    public void setVqal( int cvqal, int ci ) {

        if( polnb != -1 ) {
            if( ci>=0 && ci<nbvqn ) {
                if( cvqal < 0 ) {
                    vqal[ci] = -1;
                    fullydef = false;
                }
                else {
                    vqal[ci] = cvqal;
                }
            }
            else {
                JOptionPane.showMessageDialog(null," !!! index ("+ci+") out of range [0,"+(nbvqn-1)+"] in "+name+".setVqal");
            }
        }
        else {
            JOptionPane.showMessageDialog(null," !!!  polnb undefined in "+name+".setVqal");
        }
    }

    /**
     * Returns a quanta limit value.
     *
     * @param ci   quanta limit index
     */
    public int getVqal( int ci ) {

        if( polnb != -1 ) {
            if( ci>=0 && ci<nbvqn ) {
                return vqal[ci];
            }
            else {
                JOptionPane.showMessageDialog(null," !!! index ("+ci+") out of range [0,"+(nbvqn-1)+"] in "+name+".getVqal");
            }
        }
        else {
            JOptionPane.showMessageDialog(null," !!!  polnb undefined in "+name+".getVqal");
        }
        return -1;
    }

    /**
     * Sets an hamiltonian development order value.
     *
     * @param cvdvo   development order value
     * @param ci      development order index
     */
    public void setVdvo( int cvdvo, int ci ) {

        if( polnb != -1 ) {
            if( ci>=0 && ci<=polnb ) {
                if(cvdvo < 0 ) {
                    unsetVdvo(ci);
                }
                else {
                    vdvo[ci] = cvdvo;
                }
            }
            else {
                JOptionPane.showMessageDialog(null," !!! index ("+ci+") out of range [0,"+polnb+"] in "+name+".setVdvo");
            }
        }
        else {
            JOptionPane.showMessageDialog(null," !!!  polnb undefined in "+name+".setVdvo");
        }
    }

    /**
     * Unsets an hamiltonian development order value.
     *
     * @param ci   development order index
     */
    public void unsetVdvo(int ci) {
        if( polnb != -1 ) {
            if( ci>=0 && ci<=polnb ) {
                vdvo[ci] = -1;
                fullydef = false;
            }
            else {
                JOptionPane.showMessageDialog(null," !!! index ("+ci+") out of range [0,"+polnb+"] in "+name+".setVdvo");
            }
        }
        else {
            JOptionPane.showMessageDialog(null," !!!  polnb undefined in "+name+".setVdvo");
        }
    }

    /**
     * Sets polarizability management status.
     *
     * @param cispolar polarizability management status
     */
    public void setPolar(boolean cispolar) {

        ispolar = cispolar;
    }

    /**
     * Get polarizability management status.
     *
     */
    public boolean isSetPolar() {

        return ispolar;
    }
 
    /**
     * Sets a polarizability development order value.
     *
     * @param cvpdvo   development order value
     * @param ci       development order index
     */
    public void setVpdvo( int cvpdvo, int ci ) {

        if( polnb != -1 ) {
            if( ci>=0 && ci<=polnb ) {
                if(cvpdvo < 0 ) {
                    unsetVpdvo(ci);
                }
                else {
                    vpdvo[ci] = cvpdvo;
                }
            }
            else {
                JOptionPane.showMessageDialog(null," !!! index ("+ci+") out of range [0,"+polnb+"] in "+name+".setVpdvo");
            }
        }
        else {
            JOptionPane.showMessageDialog(null," !!!  polnb undefined in "+name+".setVpdvo");
        }
    }

    /**
     * Unsets a polarizability development order value.
     *
     * @param ci   development order index
     */
    public void unsetVpdvo(int ci) {
        if( polnb != -1 ) {
            if( ci>=0 && ci<=polnb ) {
                vpdvo[ci] = -1;
                fullydef  = false;
            }
            else {
                JOptionPane.showMessageDialog(null," !!! index ("+ci+") out of range [0,"+polnb+"] in "+name+".setVpdvo");
            }
        }
        else {
            JOptionPane.showMessageDialog(null," !!!  polnb undefined in "+name+".setVpdvo");
        }
    }

    /**
     * Is an hamiltonian development order set ?
     *
     * @param ci   development order index
     */
    public boolean isSetVdvo(int ci) {
        if( polnb != -1 ) {
            if( ci>=0 && ci<=polnb ) {
                if( vdvo[ci] == -1 ) {
                    return false;
                }
                else {
                    return true;
                }
            }
            else {
                JOptionPane.showMessageDialog(null," !!! index ("+ci+") out of range [0,"+polnb+"] in "+name+".isSetVdvo");
            }
        }
        else {
            JOptionPane.showMessageDialog(null," !!!  polnb undefined in "+name+".getVdvo");
        }
        return false;
    }

    /**
     * Returns an hamiltonian development order value.
     *
     * @param ci   development order index
     */
    public int getVdvo( int ci ) {

        if( polnb != -1 ) {
            if( ci>=0 && ci<=polnb ) {
                return vdvo[ci];
            }
            else {
                JOptionPane.showMessageDialog(null," !!! index ("+ci+") out of range [0,"+polnb+"] in "+name+".getVdvo");
            }
        }
        else {
            JOptionPane.showMessageDialog(null," !!!  polnb undefined in "+name+".getVdvo");
        }
        return -1;
    }

    /**
     * Is a polarizability development order set ?
     *
     * @param ci   development order index
     */
    public boolean isSetVpdvo(int ci) {
        if( polnb != -1 ) {
            if( ci>=0 && ci<=polnb ) {
                if( vpdvo[ci] == -1 ) {
                    return false;
                }
                else {
                    return true;
                }
            }
            else {
                JOptionPane.showMessageDialog(null," !!! index ("+ci+") out of range [0,"+polnb+"] in "+name+".isSetVpdvo");
            }
        }
        else {
            JOptionPane.showMessageDialog(null," !!!  polnb undefined in "+name+".getVpdvo");
        }
        return false;
    }

    /**
     * Returns a polarizability development order value.
     *
     * @param ci   development order index
     */
    public int getVpdvo( int ci ) {

        if( polnb != -1 ) {
            if( ci>=0 && ci<=polnb ) {
                return vpdvo[ci];
            }
            else {
                JOptionPane.showMessageDialog(null," !!! index ("+ci+") out of range [0,"+polnb+"] in "+name+".getVpdvo");
            }
        }
        else {
            JOptionPane.showMessageDialog(null," !!!  polnb undefined in "+name+".getVpdvo");
        }
        return -1;
    }

    /**
     * Returns a number of vibrational states.
     *
     * @param ci   number of vibrational states index
     */
    public int getNbvs( int ci ) {

        if( polnb != -1 ) {
            if( ci>=0 && ci<=polnb ) {
                return nbvs[ci];
            }
            else {
                JOptionPane.showMessageDialog(null," !!! index ("+ci+") out of range [0,"+polnb+"] in "+name+".getNbvs");
            }
        }
        else {
            JOptionPane.showMessageDialog(null," !!!  polnb undefined in "+name+".getNbvs");
        }
        return -1;
    }

    // auto define polyad status
    /**
     * Sets the status of the polyad.
     * <br>(fully defined or not)
     */
    public boolean autoTestdef() {

        fullydef = false;
        if( polnb == -1 ) {
            return fullydef;
        }
        for( int i=0; i<nbvqn; i++) {
            if( vqal[i] == -1 ) {
                return fullydef;
            }
        }
        for( int i=0; i<=polnb; i++) {
            if( vdvo[i] == -1 ) {
                return fullydef;
            }
            if( ispolar        &&
                vpdvo[i] == -1    ) {
                return fullydef;
            }
            
        }
        fullydef = true;
        return fullydef;
    }

    /**
     * Is the polyad fully defined ?
     */
    public boolean isFullydef() {

        return fullydef;
    }

////////////////////////////////////////////////////////////////////

    /**
     * Virtual states calculation.
     *
     * @param vpsc   polyad scheme coefficients
     */
    public void calCvs(int[] vpsc) {

        altcvs = new ArrayList();                                    // cvs array

        nbvs[0] = 1;

        for (nc=1; nc<=polnb; nc++) {                                // for each other polyad
            nbvs[nc]=0;                                              // set nb of vs of this polyade
            finvs = false;
            maxCvs();                                                // start from cvs max (nc everywhere)
            while (true) {
                decrCvs(ind, 0);                                     // decrement(up to 0 everywhere) following setInd given frequencies
                if(finvs) {                                          // finished
                    break;
                }
                if(testCvs(vpsc)) {                                  // a good cvs ?
                    nbvs[nc] = nbvs[nc]+1;                           // set nbvs
                    int[] ccvs = new int[nbvqn];                     // cvs copy array !!each time a new one!!
                    System.arraycopy(cvs,0,ccvs,0,nbvqn);            // copy cvs in ccvs
                    altcvs.add(ccvs);                                // add this object to tcvs
                }
            }
        }
    }

    // set cvs to its max value
    private void maxCvs() {

        for (int i=0; i<nbvqn; i++) {
            cvs[i] = nc;                                             // current polyad number
        }
    }

    // decrement cvs (recurssive)
    private void decrCvs(int[] ind, int k) {

        int iccvs = ind[k];                                          // current cvs index
        if(cvs[iccvs] == 0) {                                        // if min value (0)
            if(k == nbvqn-1) {                                       // if last index
                finvs = true;                                        // finished
                return;
            }
            cvs[iccvs] = nc;                                         // set to max value (nc)
            decrCvs(ind, k+1);                                       // decrement next index
        }
        else {
            cvs[iccvs]=cvs[iccvs]-1;                                 // only decrement cvs
        }
    }

    // check if nc = cvs
    private boolean testCvs(int[] vpsc) {

        int ncc = 0;

        for(int i=0; i<nbvqn; i++) {                                 // for each vqn
            if(vpsc[i] == 0 && cvs[i] != 0) {                        // vpsc[i] null => cvs[i] null
                return false;
            }
            if(cvs[i] > vqal[i]) {                                   // quanta limit
                return false;
            }
            ncc=ncc+cvs[i]*vpsc[i];                                  // add contribution
        }
        if(ncc == nc) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Returns the virtual states string of a sub-polyad.
     *
     * @param ask_polsec   sub-polyad number
     */
    public String getCvs(int ask_polsec) {

        if( ask_polsec>=0 && ask_polsec<=polnb ) {
            getCvsInternal(ask_polsec);
            return ret_str;
        }
        else {
            JOptionPane.showMessageDialog(null," !!! index ("+ask_polsec+") out of range [0,"+polnb+"] in "+name+".getCvs");
            return "";
        }
    }

    // Internally returns the virtual states string of a sub-polyad.
    private void getCvsInternal(int ask_polsec) {

        ret_nbvs = nbvs[ask_polsec];                                 // nb of vibrational states
        ret_vdvo = vdvo[ask_polsec];                                 // development order value

        StringBuffer sb = new StringBuffer();

        if( ask_polsec == 0 ) {
            for(int k=0; k<nbvqn; k++) {
                sb.append("0");                                      // pol 0 -> 0...0
            }
        }
        else {
            itcvs = 0;                                               // cvs array index
            for( int i=1; i<ask_polsec; i++) {                       // set due to asked polyad
                itcvs = itcvs+nbvs[i];
            }
            for(int j=0; j<nbvs[ask_polsec]; j++) {                  // for each right cvs
                System.arraycopy((int[]) altcvs.get(itcvs),0,cvs,0,nbvqn);  // copy saved array in cvs
                itcvs++;                                             // increment tcvs index
                for(int k=0; k<nbvqn; k++) {                         // add to StringBuffer
                    sb.append(""+cvs[k]);
                 }
            }
        }
        ret_str = sb.toString();                                     // convert to string
    }

    /**
     * Formats text fully describing the polyad.
     *
     * @param sb   output
     */
    public void toSb( StringBuffer sb ) {

        sb.append(lnsep+lnsep+name+" P"+polnb+lnsep);                // add some text
        for (int nc=0; nc<=polnb; nc++) {                            // for each other polyad
            getCvsInternal(nc);
            sb.append(lnsep+"P"+nc+" N"+ret_nbvs);                   // add specification of this polyad
            if(ret_nbvs < 10) {
                sb.append(" ");                                      // alignement space ( >99 not managed )
            }
            sb.append(" D"+ret_vdvo);
            int curcar = 0;
            for( int j=0; j<ret_nbvs; j++ ) {
                sb.append(" ");
                for( int k=0; k<nbvqn; k++ ) {
                    sb.append(ret_str.substring(curcar,++curcar));
                }
            }
        }
    }

    /**
     * Prints out all the sub-polyads.
     *
     * @param out1   output
     * @param ham    hmodel case
     */
    public void ecrPol( PrintWriter out1, boolean ham) {

        for( int i=0; i<=polnb; i++) {                               // for each sub-polyad
            getCvsInternal(i);                                       // read data
            out1.print  (" N"+ret_nbvs);                             // nb of vibrationnal states
            if(ret_nbvs < 10) {
                out1.print  (" ");                                   // aligne
            }
            if( ham ) {
                out1.print  (" D"+ret_vdvo);                         // hmodel case (dev.order)
            }
            int curcar = 0;                                          // current index in cvs string
            for( int j=0; j<ret_nbvs; j++ ) {                        // for each vibrationnal state
                out1.print  (" ");
                for( int k=0; k<nbvqn; k++ ) {                       // write cvs
                    out1.print  (" "+ret_str.substring(curcar,++curcar));
                }
            }
            if(i != polnb) {                                         // next line
                out1.println(" \\");
                out1.print  ("                      ");
            }
        }
    }

    /**
     * Asks for complete definition of the polyad.
     */
    public void askPol() {

        JOptionPane.showMessageDialog(null,"You have to first fully define "+getName()+" specifications");
        return;
    }

    /**
     * Calculates the sorting indexes for virtual states.
     *
     * @param npack    package name
     * @param nparaf   parameter file name
     */
    public boolean setInd( String npack, String nparaf) {

        String  str;                                                 // parameter file reading string
        String cstr;                                                 // current string
        BufferedReader br;                                           // parameter file reading buffer
        float[] freq;                                                // vibrational state sorting frequencies

        ind = new int[nbvqn];
        if( npack.equals("STDS") || npack.equals("C2vTDS")) {        // steady for these packages
            ind[0] = 4-1;
            ind[1] = 2-1;
            ind[2] = 3-1;
            ind[3] = 1-1;
            return true;
        }
        try {                                                        // read parameter file
            File f = new File(nparaf);                               // the file
            br = new BufferedReader (new FileReader(f));             // oopen through Readers
            int i = 0;                                               // current line #
            while ((str = br.readLine()) != null) {
                i++;
                if(i != 4 ) {
                    continue;                                        // go to 4th line
                }
                StringTokenizer st = new StringTokenizer(str);       // separate frequencies
                if( st.countTokens() < nbvqn ) {                     // not enough tokens
                    JOptionPane.showMessageDialog(null,"Bad number of virtual quantum numbers in file"+lnsep+
                                                       nparaf                                         +lnsep+
                                                       str);
                    return false;
                }
                freq = new float[nbvqn];
                for( int j=0; j<nbvqn; j++) {
                    cstr = st.nextToken();                           // convert to string
                    try {
                        freq[j] = Float.parseFloat(cstr);            // convert to float
                    }
                    catch (NumberFormatException e) {                // it is not a float
                        JOptionPane.showMessageDialog(null,"Frequency format error in file"+lnsep+
                                                           nparaf                          +lnsep+
                                                           str                             +lnsep+
                                                           e);
                        return false;
                    }
                }
                float[] frec = new float[nbvqn];                     // index selection array
                System.arraycopy(freq,0,frec,0,nbvqn);               // copy freq to frec
                Arrays.sort(frec);                                   // frec in increasing order
                for( int j=0; j<nbvqn; j++) {                        // set indexes
                    for(int k=0; k<nbvqn; k++) {
                        if(freq[k] == frec[j]) {
                            ind[j]=k;                                // increasing frequencies
                            freq[k] = -1;                            // for doubloon management
                            break;                                   // frequency found, find next one
                        }
                    }
                }
                br.close();
                return true;                                         // happy end
            }
            br.close();
            return false;                                            // unexpected EOF
        }
        catch(IOException ioe) {                                     // IO error
            JOptionPane.showMessageDialog(null,"IO error while reading file"+lnsep+
                                               nparaf                       +lnsep+
                                               ioe);
            return false;
        }
    }

}
