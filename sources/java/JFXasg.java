/*
 * Class to manage assignment characterisation of a given transition; called by CJPXfit
 * informations used later by xasg.
 */

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;
import java.text.*;
import java.util.*;

/**
 * This panel collects informations concerning assignments selection for a given transition.
 */

public class JFXasg extends    JFrame
                    implements ActionListener {

    private String  pmp;                                             // transition id

    private JPXasg[] jpxasg;                                         // assignment extractions jp
    private String   npack;                                          // package  name
    private String   nmol;                                           // molecule name
    private int      nbxasg;                                         // nb of extractions
    private int      mxxasg = 8;                                     // nbxasg max
    private boolean  showtemp;                                       // show temperature choice

    // panels
    private JPanel pouest;
    private JPanel   pon;
    private JPanel   pos;
    private JPanel pcentre;
    private JPanel   pcn;

    //
    private Box box;
    private JButton     jbadd;
    private JButton     jbremlast;
    private JButton     jbset;
    private Point       pt;
    private Rectangle   rect;

/////////////////////////////////////////////////////////////////////

    /**
     * Construct a new JFXasg.
     *
     * @param cix0      window X location
     * @param ciy0      window Y location
     * @param cpmp      identificator
     * @param cnpack    package  name
     * @param cnmol     molecule name
     */
    public JFXasg( int cix0, int ciy0, String cpmp, String cnpack, String cnmol ) {

        super(cpmp+" : Assignment extraction criteria");             // main window

        pmp     = cpmp;                                              // transition id

        nbxasg = 0;                                                  // nb of extractions
        jpxasg = new JPXasg[mxxasg];
        npack  = cnpack;
        nmol   = cnmol;

        showtemp = false;                                            // do NOT show temperature choice

        getContentPane().setLayout(new BorderLayout());
        setLocation( cix0, ciy0 );

        // west panel for add/remlast/set extraction buttons
        box= Box.createVerticalBox();                                // add buttons to box
        jbadd= new JButton("Add");
        jbadd.setBackground(Color.WHITE);
        jbadd.addActionListener(this);
        box.add(jbadd);
        box.add(Box.createVerticalStrut(15));
        jbremlast= new JButton("Remove last one");
        jbremlast.setBackground(Color.WHITE);
        jbremlast.addActionListener(this);
        box.add(jbremlast);
        pon = new JPanel();
        pon.add(box);
        jbset= new JButton("Set");
        jbset.setBackground(Color.WHITE);
        jbset.addActionListener(this);
        pos = new JPanel();
        pos.add(jbset);
        pouest = new JPanel(new BorderLayout());
        pouest.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLUE,4),null));
        pouest.add(pon,"North");
        pouest.add(pos,"South");

        // center panel for extraction panels
        pcentre = new JPanel(new BorderLayout());
        pcn = new JPanel(new GridLayout(0,1,5,5));
        pcentre.add(pcn,"North");
        jbadd.doClick();

        // display panels
        getContentPane().add(pouest ,"West");
        getContentPane().add(pcentre,"Center");

        setVisible(false);
        pack();
    }

/////////////////////////////////////////////////////////////////////

    /**
     * Process events.
     */
    public void actionPerformed(ActionEvent evt) {

        // add button
        if( evt.getSource() == jbadd ) {
            if( nbxasg != 0 ) {
                // test existing extractions
                for( int ixasg=0; ixasg<nbxasg; ixasg++ ) {
                    if( ! jpxasg[ixasg].test() ) {
                        return;
                    }
                }
            }
            if( nbxasg == mxxasg ) {
                JOptionPane.showMessageDialog(null,"Max number of extractions ("+mxxasg+") reached");
                return;
            }
            jpxasg[nbxasg] = new JPXasg(this, nbxasg+1, pmp, npack, nmol);
            if( showtemp ) {
                jpxasg[nbxasg].setShowTemp(showtemp);
            }
            pcn.add(jpxasg[nbxasg]);
            pcn.revalidate();
            pack();
            //
            nbxasg ++;
            return;
        }

        // remlast button
        if( evt.getSource() == jbremlast ) {
            if( nbxasg != 0 ) {
                pcn.remove(jpxasg[nbxasg-1]);
                pcn.revalidate();
                pack();
                nbxasg --;
            }
            return;
        }

        // set button
        if( evt.getSource() == jbset ) {
            if( nbxasg != 0 ) {
                // test existing extractions
                for( int ixasg=0; ixasg<nbxasg; ixasg++ ) {
                    if( ! jpxasg[ixasg].test() ) {
                        return;
                    }
                }
            }
            setVisible(false);
            return;
        }
    }

/////////////////////////////////////////////////////////////////////

    /**
     * Get nbxasg.
     */
    public int getNbXasg() {

        return nbxasg;
    }

    /**
     * Get JPXasg.
     */
    public JPXasg getJPXasg(int cixasg) {

        if( cixasg <  0      ||
            cixasg >= nbxasg    ) {
            JOptionPane.showMessageDialog(null,"Invalid index ("+cixasg+") in JFXasg.getJPXasg");
            return null;
        }
        return jpxasg[cixasg];
    }

    /**
     * show temperature choice in JPXasg
     */
    public void setShowTemp( boolean cshow ) {

        showtemp = cshow;
        if( nbxasg != 0 ) {
            for( int ixasg=0; ixasg<nbxasg; ixasg++ ) {
                jpxasg[ixasg].setShowTemp(cshow);
            }
            pack();
        }
        return;
    }

}
