/*
 * Class for level job creation, called by CreateJob
 */

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;

/**
 * This panel creates a job to calculate rovibrational energy levels.
 */
public class CJPLevel extends    JPanel
                      implements CJPint, ActionListener {

    // panels
    private JPanel pnord;
    private JPanel pcentre;
    private JPanel psud;

    // north panel accessories
    private JPanel       pnn;                                        // north-north panel
    private JPanel       pnno;
    private JPanel       pnnc;
    private JPanel       pnc;                                        // north-center panel

    private JComboBox    jcbmol;                                     // molecule jcb
    private String[]     trep;                                       // representation choice array
    private JComboBox    jcbrep;                                     // representation jcb
    private String       nrep;                                       // representation name
    private String[]     tjmax;                                      // jmax choice array
    private JComboBox    jcbjmax;                                    // jmax jcb
    private String       njmax;                                      // jmax name

    private JButton      jbparaf;                                    // parameter file button
    private String       nparaf;                                     // parameter file name
    private String       nparar;                                     // reduced parameter file name
    private JLabel       lparaf;                                     // parameter file label
    private JComboBox[]  jcbpsc;                                     // polyad scheme coef. jcb
    private String[]     tpsc;                                       // polyad scheme coef. choice array
    private String       npsc;                                       // current psc name
    private JRadioButton jrblevlst;                                  // job type
    private String       npredf;                                     // prediction file name
    private JLabel       jlpredf;                                    // prediction file label

    // center panel accessories
    private JTextArea   jta;                                         // polyad display text
    private JScrollPane jsp;                                         // with lifts

    // south panel accessories
    private JButton jbreset;                                         // reset
    private JButton jbsave;                                          // save
    private Box     boxsud;                                          // button box

    // CreateJob parameters
    private String   playd;                                          // XTDS installation directory
    private String   workd;                                          // working directory
    private String   npack;                                          // package name
    private String[] moldirs;                                        // molecule directories
    private int      nbvqn;                                          // nb of vibrational quantum numbers
    private int      mxpol;                                          // max nb of polyads
    // parameters catched in this CJPanel
    private String   nmol;                                           // molecule
    private double   jmax;                                           // jmax
    private int[]    vpsc;                                           // polyad scheme coef.

    // polyad
    private Polyad   lowpolyad;                                      // lower polyad
    private int      low_polnb;                                      // lower polyad #
    private JPPolyad jpplow;                                         // lower polyad panel
    // variables
    private int         nc;                                          // current polyad #

    private String      njobf;                                       // job file name
    private PrintWriter out1;                                        // job writing pw
    private String lnsep;                                            // line separator
    private String fisep;                                            // file separator
    private Font   mono15;                                           // Monospaced 15 font

////////////////////////////////////////////////////////////////////

    /**
     * Constructs a new CJPLevel.
     */
    public CJPLevel() {

        playd = System.getProperty("xtds.home");                     // XTDS installation directory
        workd = System.getProperty("user.dir");                      // working directory
        lnsep = System.getProperty("line.separator");
        fisep = System.getProperty("file.separator");
        mono15 = new Font("Monospaced", Font.PLAIN, 15);
        setName("CJPLevel");                                         // for help files
        setLayout(new BorderLayout());

        // south panel
        jbreset = new JButton();                                     // managed by CreateJob
        jbreset.setBackground(Color.WHITE);
        jbsave  = new JButton("Save");                               // job file saving button
        jbsave.setBackground(Color.WHITE);
        jbsave.addActionListener (new ActionListener () {
            public void actionPerformed (ActionEvent ae) {           // jbsave action
                saveCJP();
            }
        }
        );

        boxsud = Box.createHorizontalBox();                          // add buttons to box
        boxsud.add(jbreset);
        boxsud.add(Box.createHorizontalStrut(15));
        boxsud.add(jbsave);
        psud = new JPanel();
        psud.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        psud.add(boxsud);                                            // add box

        // center panel polyad display
        pcentre = new JPanel(new BorderLayout());
        jta = new JTextArea();
        jta.setBackground(Color.BLACK);
        jta.setForeground(Color.WHITE);
        jta.setEditable(false);                                      // not editable
        jta.setFont( mono15 );
        jta.setLineWrap(false);                                      // NO line wrap
        jsp = new JScrollPane(jta);                                  // lifts
        pcentre.add(jsp, "Center");

        // display panels
        add(pcentre, "Center");
        add(psud,    "South");
    }

/////////////////////////////////////////////////////////////////////

    /**
     * Returns the reset button.
     * <br>Called by CreateJob which manages it.
     */
    public JButton getJBreset() {

        return jbreset;
    }

    /**
     * Sets basic parameters and creates north panel.
     * <br>Called by CreateJob to activate this panel.
     *
     * @param cnpack     name of the package
     * @param cmoldirs   molecule directories
     * @param cmxpol     MXPOL
     * @param cnbclab    number of characters of parameter label
     * @param cnbvqn     number of vibrational quantum numbers
     */
    public void setBasicPara(String cnpack, String[] cmoldirs, int cmxpol, int cnbclab, int cnbvqn) {

        npack   = cnpack;                                            // package name
        moldirs = cmoldirs;                                          // molecule directories
        mxpol   = cmxpol;                                            // MXPOL
        nbvqn   = cnbvqn;                                            // nb of vibrational quantum numbers

        createNorth();
    }

/////////////////////////////////////////////////////////////////////

    // north panel for polyad characteristics setting and display
    private void createNorth() {

        pnord = new JPanel(new BorderLayout());                      // north panel
        pnn   = new JPanel(new BorderLayout());                      // north-north panel
        pnno = new JPanel(new GridLayout(4,0,5,5));
        pnnc = new JPanel(new GridLayout(4,0,5,5));
        pnc   = new JPanel(new BorderLayout());                      // north-center panel

        // BASICS
        // molecule choice
        pnno.add(new JLabel(""));
        JPanel jpmrj;
        jpmrj =  new JPanel(new GridLayout(0,6,0,5));
        jpmrj.add(new JLabel("Molecule ",null,JLabel.RIGHT));
        jcbmol = new JComboBox();
        jcbmol.addItem("");
        for( int i=0; i<moldirs.length; i++ ) {
            jcbmol.addItem(moldirs[i]);
        }
        jcbmol.setSelectedItem("");                                  // default to space
        jcbmol.setBackground(Color.WHITE);
        nmol = "";
        jcbmol.addActionListener(this);
        jpmrj.add(jcbmol);
        if( npack.equals("D2hTDS") ) {
            // representation choice
            jpmrj.add(new JLabel("Representation ",null,JLabel.RIGHT));
            trep = new String[4];
            trep[0] = "";                                            // space first
            trep[1] = "Ir";
            trep[2] = "IIr";
            trep[3] = "IIIr";
            jcbrep = new JComboBox(trep);
            jcbrep.setSelectedItem("");                              // default to space
            jcbrep.setBackground(Color.WHITE);
            jpmrj.add(jcbrep);
        }
        // jmax choice
        jpmrj.add(new JLabel("Jmax ",null,JLabel.RIGHT));
        tjmax = new String[201];                                     // 201 due to space plus jmax : 0(.5) -> 199(.5)
        tjmax[0] = "";                                               // space first
        for( int i=1; i<tjmax.length; i++) {                         // array fill
            if( npack.equals("C3vsTDS") ) {
                tjmax[i] = String.valueOf(i-1+0.5);
            }
        else {
                tjmax[i] = String.valueOf(i-1);
            }
        }
        jcbjmax = new JComboBox(tjmax);
        jcbjmax.setSelectedItem("");                                 // default to space
        jcbjmax.setBackground(Color.WHITE);
        jpmrj.add(jcbjmax);
        pnnc.add(jpmrj);
        // parameter file choice
        jbparaf = new JButton("Parameter File");
        jbparaf.setBackground(Color.WHITE);
        jbparaf.addActionListener(this);
        nparaf = "";
        pnno.add(jbparaf);
        lparaf = new JLabel("");                                     // default to space
        lparaf.setOpaque(true);
        lparaf.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        pnnc.add(lparaf);
        // polyad scheme coefficients choice
        pnno.add(new JLabel("Polyad Scheme "));
        JPanel jppsc = new JPanel(new GridLayout(0,2*nbvqn+1,0,5));  // nbvqn = nb of vibrational quantum numbers
        jppsc.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        jcbpsc = new JComboBox[nbvqn];
        tpsc = new String[mxpol];                                    // polyad scheme coefficient array
        for( int i=0; i<mxpol; i++) {
            tpsc[i] = String.valueOf(i);                             // 0 -> mxpol
        }
        for ( int i=0; i<nbvqn; i++) {                               // choice Label
            if(i == 0) {
                jppsc.add(new JLabel(" [P]n = "));                   // polyad number
            }
            jcbpsc[i] = new JComboBox();
            jcbpsc[i].addItem("");
            for( int j=0; j<tpsc.length; j++ ) {
                jcbpsc[i].addItem(tpsc[j]);
            }
            jcbpsc[i].setSelectedItem("");                           // default to space
            jcbpsc[i].setBackground(Color.WHITE);
            jcbpsc[i].addActionListener(this);
            vpsc = new int[nbvqn];                                   // polyad scheme coefficient values
            jppsc.add(jcbpsc[i]);
            if(i != nbvqn-1) {                                       // set label
                jppsc.add(new JLabel("*v"+(i+1)+" +"));
            }
            else {
                jppsc.add(new JLabel("*v"+(i+1)));
            }
        }
        pnnc.add(jppsc);
        jrblevlst = new JRadioButton("Mixings");                     // for job with mixings
        jrblevlst.setBackground(Color.WHITE);
        jrblevlst.addActionListener(this);
        pnno.add(jrblevlst);
        npredf = "";
        jlpredf = new JLabel(npredf);                                // level selecting prediction file
        jlpredf.setOpaque(true);
        jlpredf.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        pnnc.add(jlpredf);
        pnn.add(pnno,"West");
        pnn.add(pnnc,"Center");
        pnn.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLUE,4),"BASICS"));

        // LOWER POLYAD
        lowpolyad = new Polyad(nbvqn);                               // lower polyad
        lowpolyad.setName("Polyad");                                 // its name
        jpplow = new JPPolyad(this, lowpolyad, tpsc, pnc);           // its panel

        // add panels and sub-panels
        pnord.add(pnn,"North");
        pnord.add(pnc,"Center");
        add(pnord, "North");
    }

/////////////////////////////////////////////////////////////////////

    /**
     * Processes the events.
     */
    public void actionPerformed(ActionEvent evt) {

        // BASICS
        // molecule
        if (evt.getSource() == jcbmol ) {
            nmol = (String) jcbmol.getSelectedItem();
            // unselect representation, jmax and parameter file
            if( npack.equals("D2hTDS") ) {
                jcbrep.setSelectedItem("");
            }
            jcbjmax.setSelectedItem("");
            nparaf = "";
            lparaf.setText(nparaf);
        }
        // parameter file
        if (evt.getSource() == jbparaf) {
            // molecule required
            if( nmol == "" ) {
                JOptionPane.showMessageDialog(null,"You have to first define the molecule");
                return;
            }
            JFileChooser jfcparaf = new JFileChooser(playd+fisep+"packages"+fisep+npack+fisep+"para"+fisep+nmol);  // default choice directory
            jfcparaf.setSize(400,300);
            jfcparaf.setFileSelectionMode(JFileChooser.FILES_ONLY);  // files only
            Container parent = jbparaf.getParent();
            int choice = jfcparaf.showDialog(parent,"Select");       // Dialog, Select
            if (choice == JFileChooser.APPROVE_OPTION) {
                nparaf= jfcparaf.getSelectedFile().getAbsolutePath();
                nparar= jfcparaf.getSelectedFile().getName();
                lparaf.setText(nparaf);
            }
            if( ! lowpolyad.setInd(npack, nparaf) ) {                // set frequency indexes
                nparaf = "";                                         // file error : unselect parameter file
                lparaf.setText(nparaf);
            }
        }
        // Polyad Scheme Coefficients
        for (int i=0; i<nbvqn; i++) {
            if(evt.getSource() == jcbpsc[i] ) {                      // whatever coef.
                npsc = (String) jcbpsc[i].getSelectedItem();
                if(npsc != "") {
                    vpsc[i] =  Integer.parseInt(npsc);               // save coef. value
                }
            }
        }
        // levlst
        if (evt.getSource() == jrblevlst) {
            if( jrblevlst.isSelected() ) {
                // constraint file used ?
                int n = JOptionPane.showConfirmDialog(null,"Would you like to choose"+lnsep+
                                                           "a prediction file ?"     +lnsep,
                                                           "Prediction File Choice",JOptionPane.YES_NO_OPTION);
                if(n == JOptionPane.YES_OPTION) {
                    JFileChooser jfclevlst = new JFileChooser(workd);  // default choice directory
                    jfclevlst.setSize(400,300);
                    jfclevlst.setFileSelectionMode(JFileChooser.FILES_ONLY);  // files only
                    Container parent = jrblevlst.getParent();
                    int choice = jfclevlst.showDialog(parent,"Select a Pred_mix_ file");  // Dialog, Select
                    if (choice == JFileChooser.APPROVE_OPTION) {
                        npredf= jfclevlst.getSelectedFile().getAbsolutePath();
                        jlpredf.setText(npredf);
                    }
                    else {
                        jrblevlst.setSelected(false);
                    }
                }
            }
            else {
                npredf = "";
                jlpredf.setText(npredf);
            }
        }
        // end of basics
        jpplow.resetJP();                                            // finally resetJP
    }

    /**
     * Manages JPPolyad event.
     */
    public void JPPEvent( Polyad curpol, int type, int indice ) {

        if( ! testBasics() ) {                                       // basics checked ?
            askBasics();                                             // no, ask for
            jpplow.resetJP();                                        // JPP reset
        }
        showPol();                                                   // update display
    }

/////////////////////////////////////////////////////////////////////

    // display polyad via jta
    private void showPol() {

        jta.setText("");                                             // empty screen
        if( lowpolyad.isFullydef() ) {                               // polyad defined
            lowpolyad.calCvs(vpsc);                                  // cvs calculation
            StringBuffer sb = new StringBuffer();
            lowpolyad.toSb(sb);                                      // display polyad
            jta.append(sb.toString());
        }
    }

/////////////////////////////////////////////////////////////////////

    // save job
    private void saveCJP() {

        // check if everything is defined
        // basics
        if( ! testBasics()) {
            askBasics();
            return;
        }
        // polyad
        if( ! lowpolyad.isFullydef() ) {
            lowpolyad.askPol();
            return;
        }
        // check if there is at least one vs per sub-polyad
        low_polnb = lowpolyad.getPolnb();
        for( int i=0; i<=low_polnb; i++) {
            if(lowpolyad.getNbvs(i) == 0 ) {
                JOptionPane.showMessageDialog(null,"WARNING : your choice produces NO vibrational state for P"+i);
            }
        }

        // everything is well defined
        // choose job name
        JFileChooser jfcjobf = new JFileChooser(workd);
        jfcjobf.setSize(400,300);
        jfcjobf.setFileSelectionMode(JFileChooser.FILES_ONLY);
        jfcjobf.setDialogTitle("Define the job file to be created");
        if( jrblevlst.isSelected() ) {
            jfcjobf.setSelectedFile(new File("job_levlst_"));
        }
        else {
            jfcjobf.setSelectedFile(new File("job_lev_"));
        }
        int choice = jfcjobf.showSaveDialog(this);
        if (choice == JFileChooser.APPROVE_OPTION) {
            njobf = jfcjobf.getSelectedFile().getAbsolutePath();     // job file name
        }
        else {
            return;                                                  // name NOT chosen
        }
        // create job
        try {
            out1 = new PrintWriter(new BufferedWriter(new FileWriter(njobf)));  // write job

            out1.println("#! /bin/sh");
            out1.println(" set -v");
            out1.println("##");
            out1.println("## Level Calculation job created by XTDS");
            out1.print  ("## P"+low_polnb+" of "+nmol+".");
            out1.println("##");
            out1.println("BASD="+playd+fisep+"packages"+fisep+npack);
            out1.println("#");
            out1.println(" SCRD=$BASD"+fisep+"prog"+fisep+"exe");
            out1.println(" PARD=$BASD"+fisep+"para"+fisep+nmol);
            out1.println("##");
            out1.println("## Jmax value.");
            out1.println("##");
            out1.print  (" JPlow=");
            if( npack.equals("C3vsTDS") ) {
                 out1.println(""+jmax);
            }
            else {
                 out1.println(""+(int) jmax);
            }
            out1.println("##");
            out1.println("## Parameter file.");
            out1.println("##");
            out1.println(" PARA="+nparaf);
            out1.println("##");
            out1.println("#################################################");
            out1.println("##");
            out1.println("## Hamiltonian matrix elements.");
            out1.println("##");
            out1.println("## Polyad.");
            out1.println("##");
            out1.print  (" $SCRD"+fisep+"passx hmodel P"+low_polnb);
            lowpolyad.ecrPol(out1, true);
            if( npack.equals("D2hTDS") ) {
                out1.println(" \\"+lnsep+
                             "                    "+nrep);
            }
            else {
                out1.println("");
            }
            out1.print  (" $SCRD"+fisep+"passx parchk P"+low_polnb+"     D");
            for(int j=0; j<low_polnb+1; j++) {
                out1.print  (lowpolyad.getVdvo(j));
            }
            out1.println(" $PARD $PARA");
            int cur_nbvs = lowpolyad.getNbvs(low_polnb);
            out1.print  (" $SCRD"+fisep+"passx rovbas P"+low_polnb+" N"+cur_nbvs);
            if(cur_nbvs < 10) {
                out1.print  (" ");
            }
            out1.print  (" D");
            for(int j=0; j<low_polnb+1; j++) {
                out1.print  (lowpolyad.getVdvo(j));
            }
            out1.println(" $JPlow");
            out1.print  (" $SCRD"+fisep+"passx hmatri P"+low_polnb+" N"+cur_nbvs);
            if(cur_nbvs < 10) {
                out1.print  (" ");
            }
            out1.print  (" D");
            for(int j=0; j<low_polnb+1; j++) {
                out1.print  (lowpolyad.getVdvo(j));
            }
            out1.println(" $JPlow");
            if( jrblevlst.isSelected() ) {
                out1.print  (" $SCRD"+fisep+"passx hdiag ");
            }
            else {
                out1.print  (" $SCRD"+fisep+"passx hdi   ");
            }
            out1.print  (" P"+low_polnb+" N"+cur_nbvs);
            if(cur_nbvs < 10) {
                out1.print  (" ");
            }
            out1.print  (" D");
            for(int j=0; j<low_polnb+1; j++) {
                out1.print  (lowpolyad.getVdvo(j));
            }
            out1.println(" $JPlow $PARA");
            if( jrblevlst.isSelected() ) {
                out1.print  (" $SCRD"+fisep+"passx levlst P"+low_polnb+" 0 $JPlow");
                if( npredf != "" ) {
                    out1.print  (" assign "+npredf);
                }
                out1.print  (" comp MH_P"+low_polnb+"_D");
                for(int j=0; j<low_polnb+1; j++) {
                    out1.print  (lowpolyad.getVdvo(j));
                }
                out1.println("");
            }
            else {
                out1.println(" $SCRD"+fisep+"passx jener  P"+low_polnb);
            }
            out1.println("##");
            if( jrblevlst.isSelected() ) {
                out1.print  ("  rm ED*");
            }
            else {
                out1.print  ("  rm EN*");
            }
            out1.print  (" FN* HA*");
            if( ! jrblevlst.isSelected() ) {
                out1.print  (" ME_*");
            }
            out1.println(" MH* VP*");
        }
        catch (IOException ioe) {                                    // IO error
            JOptionPane.showMessageDialog(null,"IO error while writing file"+lnsep+
                                               njobf                        +lnsep+
                                               ioe);
            return;
        }
        finally {
            if( out1 != null ) {
                out1.close();
                if( out1.checkError() ) {
                    JOptionPane.showMessageDialog(null,"PrintWriter error while creating file"+lnsep+
                                                       njobf);
                    return;
                }
            }
        }
        JOptionPane.showMessageDialog(null,"The job "+njobf+" has been created");
        try {
            Process monproc = Runtime.getRuntime().exec("chmod u+x "+njobf);  // allow execution
        }
        catch (IOException ioe) {                                    // rights modification error
            JOptionPane.showMessageDialog(null,"IO error while changing rights of file"+lnsep+
                                               njobf                                   +lnsep+
                                               ioe);
            return;
        }
    }

/////////////////////////////////////////////////////////////////////

    // check Basics panel
    private boolean testBasics() {

        if(nmol == "") {
            return false;                                            // molecule
        }
        if( npack.equals("D2hTDS") ) {
            nrep = (String) jcbrep.getSelectedItem();
            if(nrep == "") {
                return false;                                        // representation
            }
        }
        njmax = (String) jcbjmax.getSelectedItem();
        if(njmax == "") {
            return false;                                            // jmax
        }
        jmax = Double.valueOf(njmax);
        if(nparaf == "") {
            return false;                                            // parameter file name
        }
        if( ! testPsc()) {
            return false;                                            // psc
        }
        return true;
    }

    // ask to fully define Basics
    private void askBasics() {

        JOptionPane.showMessageDialog(null,"You have to first fully define BASICS specifications");
        return;
    }

    // check polyad scheme coefficients
    private boolean testPsc() {

        for (int i=0; i<nbvqn; i++) {
            npsc = (String) jcbpsc[i].getSelectedItem();
            if (npsc == "") {
                return false;
            }
        }
        return true;
    }

}
