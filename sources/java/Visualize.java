/*
 * Class for job result display
 */

import java.awt.*;
import java.awt.event.*;
import java.awt.print.*;
import java.io.*;
import javax.swing.*;
import java.text.*;
import java.util.*;

/////////////////////////////////////////////////////////////////////

/**
 * This panel allows to visualize the results of the different types of calculations.
 */
public class Visualize extends    JPanel
                       implements ActionListener , Runnable {

    // panels
    private JPanel pouest;
    private JPanel pcentre;
    private JPanel  pcn;
    private JPanel  pcc;
    private JPanel    pccn;
    private JPanel      pccno;
    private JPanel      pccnc;
    private JPanel    pccc;
    private JPanel      pcccn;
    private JPanel  pcs;
    private JPanel psud;

    private Box    boxouest;
    private String ntfile;                                           // file type
    private String[] nxyf;                                           // full file name

    private ButtonGroup  group;                                      // RadioButton group
    private JRadioButton jrbtrni;                                    // for Ener[_assign]_Pn
    private JRadioButton jrbjener;                                   // for jener.xy
    private JRadioButton jrbjenert;                                  // for jener.t
    private JRadioButton jrbspectr;                                  // for spectr.xy
    private JRadioButton jrbspectrt;                                 // for spectr.t
    private JRadioButton jrbsimul;                                   // for simul.xy
    private JRadioButton jrbpredmix;                                 // for Pred_mix
    private JRadioButton jrbnone;                                    // untrue button
    private JRadioButton jrbpmjinf;                                  // for Pred_mix Jinf sorting
    private JRadioButton jrbpmjsup;                                  // for Pred_mix Jsup sorting
    private JRadioButton jrbpmfreq;                                  // for Pred_mix Freq sorting
    private JRadioButton jrbpmint;                                   // for Pred_mix Int  sorting

    private String   playd;                                          // XTDS  installation directory
    private String   workd;                                          // working directory

    private JButton jbreset;                                         // reset button
    private JButton jbshow;                                          // TRNI show button

    private JButton        jbprint;                                  // print button
    private JButton        jbunzoom;                                 // unzoom button
    private JRadioButton[] jrbcomp;                                  // component selection jrb
    private Color[] coul = { Color.BLUE            , Color.RED           ,     // predefined colors
                             Color.GRAY            , Color.GREEN         ,
                             Color.MAGENTA         , Color.ORANGE        ,
                             Color.BLACK           , Color.PINK          ,
                             Color.CYAN            , Color.YELLOW        ,
                             new Color(153,102,51) , new Color(127,0,127),     // brun  , violet
                             new Color(255,102,102), new Color(128,128,0)  };  // saumon, asperge

    private JButton jbaddfich;                                       // ADD FILE button
    private JButton jbaddcomp;                                       // ADD COMPONENT button
    private JButton jbloadcompf;                                     // LOAD COMP. FILE button
    private JButton jbsavecompf;                                     // SAVE COMP. FILE button
    private PanAffich panaff;                                        // display panel
    private String threadType;                                       // read or print

    private String str;                                              // data file reading string
    private BufferedReader br;

    private double[] x;                                              // data X and Y
    private double[] y;
    private int[][]  concomp;                                        // Trni : component contributions
    private int[][]  limcomp;                                        // Trni : component limits
    private int[]    icompa;                                         // Trni : displayable component index
    private String[] smark;                                          // Pred_mix : selection mark
    private String[] jsyn;                                           // Pred_mix : assignment
    private int[]    comp;                                           // Pred_mix : related component

    private JLabel jlx;                                              // labels for mouse position
    private JLabel jly;

    private JScrollPane jsp;                                         // with lifts
    private JRadioButton jrballej;                                   // All Energ. and J button
    private JFormattedTextField jftfemin;
    private JFormattedTextField jftfemax;
    private JFormattedTextField jftfjmin;
    private JFormattedTextField jftfjmax;
    private JRadioButton jrbassign;                                  // Assigned button

    // Number Format
    private NumberFormat nffloat;
    private NumberFormat nfint;

    private double emin;
    private double emax;
    private int    jmin;
    private int    jmax;
    private double beta0;                                            // for trni
    private double gama0;                                            // for trni
    private double pi0;                                              // for trni
    private int    nbfich;                                           // nb of data files
    private int    mxnbfich = 10;                                    // nb of data files (max)
    private File[] fich;                                             // these files
    private int[]  nbop;                                             // nb of operators  of Ener file
    private int    nsv;                                              // nb of sub-levels of Ener file
    private String[] tsubl;                                          // sub-level choice array
    private int    nbcomp;                                           // nb of components
    private int    mxnbcomp = 100;                                   // nb of components (max)
    private Box[]  boxcomp;                                          // component liminf and limsup box
    private JComboBox[] jcbliminf;                                   // component lower limit jcb
    private JComboBox[] jcblimsup;                                   // component upper limit jcb
    private int[]  liminf;                                           // component lower limit
    private int[]  limsup;                                           // component upper limit
    private String climinf;                                          // current liminf
    private String climsup;                                          // current limsup
    private boolean toussniv;                                        // component include all sub-levels
    private String  ncompf;                                          // name of component file
    private int     nbflim;                                          // nb of data in component file
    private int[]   fliminf;                                         // lower limit data of comp. file
    private int[]   flimsup;                                         // upper limit data of comp. file

    private TrniTFile    tntf;                                       // trni    file
    private PredmixTFile pmtf;                                       // predmix file
    private boolean      xfile;                                      // xpafit (not eq_*.f) Pred_mix file
    private JComboBox    jcbtrans;                                   // transition choice jcb
    private String       pntrans;                                    // previous selected transition
    private String       cntrans;                                    // currrent selected transition

    private PrintWriter out1;                                        // component file output
    private String lnsep;                                            // line separator

/////////////////////////////////////////////////////////////////////

    /**
     * Constructs a new Visualize.
     */
    public Visualize() {

        playd = System.getProperty("xtds.home");                     // XTDS installation directory
        workd = System.getProperty("user.dir");                      // working directory
        lnsep = System.getProperty("line.separator");
        setName("Visualize");                                        // for help files
        askFileType();                                               // choose a file type
    }

    // choose a file type
    private void askFileType() {

        setVisible(false);
        setBorder(null);
        removeAll();                                                 // remove all panel components
        panaff  = null;                                              // unset panaff (PanAffich) if it exists
        jrbcomp = null;                                              // unset jrbcomp
        jlx = new JLabel();                                          // create labels
        jly = new JLabel();

        setLayout(new FlowLayout());                                 // layout choice

        Box box=Box.createVerticalBox();                             // choice box
        box.add(Box.createVerticalStrut(100));
        box.add(new JLabel("Choose a File Type to visualize"));      // type choice
        jrbjener = new JRadioButton("jener.xy file");
        jrbjener.setBackground(Color.WHITE);
        jrbjener.addActionListener(this);
        jrbjenert = new JRadioButton("jener.t (or starkl.t) file");
        jrbjenert.setBackground(Color.WHITE);
        jrbjenert.addActionListener(this);
        jrbtrni = new JRadioButton("Ener[_assign]_Pn file");
        jrbtrni.setBackground(Color.WHITE);
        jrbtrni.addActionListener(this);
        jrbspectr = new JRadioButton("spectr.xy file");
        jrbspectr.setBackground(Color.WHITE);
        jrbspectr.addActionListener(this);
        jrbspectrt = new JRadioButton("spectr.t file");
        jrbspectrt.setBackground(Color.WHITE);
        jrbspectrt.addActionListener(this);
        jrbsimul = new JRadioButton("simul.xy file");
        jrbsimul.setBackground(Color.WHITE);
        jrbsimul.addActionListener(this);
        jrbpredmix = new JRadioButton("Pred_mix file");
        jrbpredmix.setBackground(Color.WHITE);
        jrbpredmix.addActionListener(this);
        jrbnone = new JRadioButton();                                // to unselect others
        group = new ButtonGroup();                                   // jrb group
        group.add(jrbjener);                                         // buttons in group
        group.add(jrbjenert);
        group.add(jrbtrni);
        group.add(jrbspectr);
        group.add(jrbspectrt);
        group.add(jrbsimul);
        group.add(jrbpredmix);
        group.add(jrbnone);
        box.add(Box.createVerticalStrut(20));                        // buttons in box
        box.add(jrbjener);
        box.add(Box.createVerticalStrut(10));
        box.add(jrbjenert);
        box.add(Box.createVerticalStrut(10));
        box.add(jrbtrni);
        box.add(Box.createVerticalStrut(10));
        box.add(jrbspectr);
        box.add(Box.createVerticalStrut(10));
        box.add(jrbspectrt);
        box.add(Box.createVerticalStrut(10));
        box.add(jrbsimul);
        box.add(Box.createVerticalStrut(10));
        box.add(jrbpredmix);

        add(box);

    }

/////////////////////////////////////////////////////////////////////

    /**
     * Processes events.
     */
    public void actionPerformed(ActionEvent evt) {

        // reset button
        if (evt.getSource() == jbreset) {
            if( panaff != null ) {
                panaff.unPredsel();
            }
            askFileType();                                           // restart from beginning
            return;
        }
        // print button
        if (evt.getSource() == jbprint) {
            // using thread
            prepThread("Printing File ...");                         // wait message
            threadType = "print";                                    // run() action type
            Thread threadThis = new Thread(this);                    // set thread (run of this instance)
            threadThis.start();                                      // launch it
            return;
        }
        // unzoom button
        if (evt.getSource() == jbunzoom) {
            panaff.init();                                           // reinitialize PanAffich instance
            panaff.repaint();                                        // repaint it
            return;
        }
        // component jrb
        if( jrbcomp != null ) {
            for( int i=0; i<nbcomp; i++ ) {
                if (evt.getSource() == jrbcomp[i] ) {
                    int j = icompa[i];
                    limcomp[0][j] = -limcomp[0][j];
                    limcomp[1][j] = -limcomp[1][j];
                    panaff.redRepaint();                             // repaint it
                    return;
                }
            }
        }
        // display TRNI
        // All Energ. and J
        if(evt.getSource() == jrballej ) {
            if( jrballej.isSelected() ) {
                jftfemin.setEnabled(false);
                jftfjmin.setEnabled(false);
                jftfemax.setEnabled(false);
                jftfjmax.setEnabled(false);
            }
            else {
                jftfemin.setEnabled(true);
                jftfjmin.setEnabled(true);
                jftfemax.setEnabled(true);
                jftfjmax.setEnabled(true);
            }
            return;
        }
        // choose a file: trni | predmix
        if(evt.getSource() == jbaddfich) {
            if( ntfile.equals("trni") ) {                            // trni
                if( testBasics() ) {                                          // BASICS OK?
                    if( nbfich == mxnbfich ) {                                // too much files
                        JOptionPane.showMessageDialog(null,"Max number of files is reached ("+mxnbfich+")");
                    }
                    else if( newTrniFile() ) {                                // file beginning reading OK?
                        pccn.add(new JLabel("  "+fich[nbfich-1].getPath()));  // display its name
                        pccn.revalidate();
                    }
                }
            }
            else {                                                   // predmix
                pccno.removeAll();
                pccnc.removeAll();
                pcccn.removeAll();
                nbcomp = 0;
                pcccn.revalidate();
                if( newPredmixFile() ) {                             // file defined
                    pccno.add(new JLabel("  File "));
                    pccnc.add(new JLabel("  "+fich[0].getPath()));   // display its name
                    if( xfile ) {                                    // xpafit (not eq_*.f) Pred_mix file
                        jcbtrans = new JComboBox(pmtf.getXntrans());
                        jcbtrans.addItem("");
                        jcbtrans.setBackground(Color.WHITE);
                        jcbtrans.setSelectedItem("");
                        jcbtrans.addActionListener(this);
                        pntrans = "";                                // previous selected transition
                        pccno.add(new JLabel("  Transition  "));
                        pccnc.add(jcbtrans);
                    }
                    else {
                        jcbtrans = null;
                    }
                }
                pccno.add(new JLabel("  Plot versus  "));
                JPanel jpplot = new JPanel(new GridLayout(0,4,5,5));
                jpplot.add(jrbpmjinf);
                jpplot.add(jrbpmjsup);
                jpplot.add(jrbpmfreq);
                jpplot.add(jrbpmint);
                pccnc.add(jpplot);
                pccno.revalidate();
                pccnc.revalidate();
            }
            return;
        }
        // select a transition
        if( jcbtrans != null ) {
            if(evt.getSource() == jcbtrans) {
                cntrans = (String) jcbtrans.getSelectedItem();
                if( ! pntrans.equals(cntrans) ) {                    // transition changed
                    pcccn.removeAll();
                    pcccn.revalidate();
                    nbcomp = 0;
                }
                if( ! cntrans.equals("") ) {
                    nsv = pmtf.getXnsv( cntrans );
                    tsubl = new String[nsv];                         // sub-level choice array
                    for(int j=0; j<nsv; j++) {
                        tsubl[j] = String.valueOf(j+1);              // 1 -> nsv
                    }
                }
                pntrans = cntrans;
                return;
            }
        }
        // add a component: trni | predmix
        if(evt.getSource() == jbaddcomp) {
            if( (ntfile.equals("trni"   ) && testBasics() ) ||
                (ntfile.equals("predmix")                 )    ) {
                if( nbfich == 0 ) {
                    if( ntfile.equals("trni") ) {
                        JOptionPane.showMessageDialog(null,"You have to first choose at least one data file");
                    }
                    else {
                        JOptionPane.showMessageDialog(null,"You have to first choose a data file");
                    }
                    return;
                }
                if( ntfile.equals("predmix")) {
                    if( xfile ) {
                        cntrans = (String) jcbtrans.getSelectedItem();
                        if( cntrans.equals("") ) {
                            JOptionPane.showMessageDialog(null,"You have to first choose a transition");
                            return;
                        }
                    }
                }
                if( nbcomp == mxnbcomp ) {
                    JOptionPane.showMessageDialog(null,"Max number of components is reached ("+mxnbcomp+")");
                    return;
                }
                if( (nbcomp == 0) || testComp() ) {                  // existing component OK?
                    int vliminf;                                     // current lower limit
                    if( nbcomp == 0 ) {
                        vliminf = 1;                                 // beggining
                    }
                    else {
                        climinf = (String) jcblimsup[nbcomp-1].getSelectedItem();  // foregoing upper limit
                        vliminf = Integer.parseInt(climinf)+1;                     // incremented
                        if( vliminf > nsv ) {
                            // max reached
                            JOptionPane.showMessageDialog(null,"No more selectable sub-levels");
                            return;
                        }
                    }
                    newComp(vliminf,vliminf);                            // create component
                    pcccn.revalidate();
                }
            }
            return;
        }
        // load a component file: trni | predmix
        if(evt.getSource() == jbloadcompf) {
            if( (ntfile.equals("trni"   ) && testBasics() ) ||
                (ntfile.equals("predmix")                 )    ) {
                if( nbfich == 0 ) {
                    if( ntfile.equals("trni") ) {
                        JOptionPane.showMessageDialog(null,"You have to first choose at least one data file");
                    }
                    else {
                        JOptionPane.showMessageDialog(null,"You have to first choose a data file");
                    }
                    return;
                }
                if( ntfile.equals("predmix") ) {
                    if( xfile ) {
                        cntrans = (String) jcbtrans.getSelectedItem();
                        if( cntrans.equals("") ) {
                            JOptionPane.showMessageDialog(null,"You have to first choose a transition");
                            return;
                        }
                    }
                }
                // file choice
                JFileChooser jfcfile = new JFileChooser(workd);      // default choice directory
                jfcfile.setSize(400,300);
                jfcfile.setFileSelectionMode(JFileChooser.FILES_ONLY);  // files only
                jfcfile.setDialogTitle("Define the Component File to be loaded");
                Container parent = jrbnone.getParent();
                int choice = jfcfile.showDialog(parent,"Select");    // Dialog, Select
                if (choice == JFileChooser.APPROVE_OPTION) {
                    ncompf = jfcfile.getSelectedFile().getAbsolutePath();  // component file name (full)
                }
                else {
                    return;
                }
                // read the component file
                if( readCompFile() ) {
                    if( nbcomp != 0 ) {
                        pcccn.removeAll();
                        nbcomp = 0;
                    }
                    // add components
                    for(int i=0; i<nbflim; i++) {
                        if( nbcomp == mxnbcomp ) {
                            JOptionPane.showMessageDialog(null,"Max number of components is reached ("+mxnbcomp+")");
                            break;
                        }
                        newComp(fliminf[i],flimsup[i]);              // create component
                    }
                    pcccn.revalidate();
                    pccc.repaint();
                }
            }
            return;
        }
        // save a component file: trni | predmix
        if(evt.getSource() == jbsavecompf) {
            if( nbcomp == 0 ) {
                JOptionPane.showMessageDialog(null,"No component to save");
                return;
            }
            // test components
            if( testComp() ) {
                // file choice
                JFileChooser jfcfile = new JFileChooser(workd);      // default choice directory
                jfcfile.setSize(400,300);
                jfcfile.setFileSelectionMode(JFileChooser.FILES_ONLY);  // files only
                jfcfile.setDialogTitle("Define the Component File to be saved");
                jfcfile.setSelectedFile(new File("components_"));
                int choice = jfcfile.showSaveDialog(this);
                if (choice == JFileChooser.APPROVE_OPTION) {
                    ncompf = jfcfile.getSelectedFile().getAbsolutePath();  // component file name (full)
                    // save component file
                    saveCompFile();
                }
            }
            return;
        }
        // show it: trni | predmix
        if(evt.getSource() == jbshow) {
            // predmix: plot Jmin/Jmax
            if( ntfile.equals("predmix") &&
                jrbnone.isSelected()        ) {
                JOptionPane.showMessageDialog(null,"You have to first choose the plot J type.");
                return;
            }
            if( (ntfile.equals("trni"   ) && testTrni() ) ||
                (ntfile.equals("predmix") && testComp() )    ) {  // good one ?
                // save component file ?
                int n = JOptionPane.showConfirmDialog(null,"Would you like to save"+lnsep+
                                                           "components in a file ?"+lnsep,
                                                           "Save component file",JOptionPane.YES_NO_OPTION);
                if(n == JOptionPane.YES_OPTION) {                    // create
                    jbsavecompf.doClick();
                }
                icompa  = new int[nbcomp];
                // using thread
                prepThread("Loading File ...");
                threadType = "read";                                 // read data in files and show
                Thread threadThis = new Thread(this);
                threadThis.start();
            }
            return;
        }

        // file type choice
        // trni case
        if(evt.getSource() == jrbtrni) {
            ntfile = "trni";
            getTrniParm();                                           // display parmeters setting panel
            return;
        }
        // predmix case
        if(evt.getSource() == jrbpredmix) {
            ntfile  = "predmix";
            getPredmixParm();                                        // display parmeters setting panel
            return;
        }
        // other cases
        if(evt.getSource() == jrbjener) {
            ntfile = "jener";
        }
        if(evt.getSource() == jrbjenert) {
            ntfile = "jenert";
        }
        else if(evt.getSource() == jrbspectr) {
            ntfile = "spectr";
        }
        else if(evt.getSource() == jrbspectrt) {
            ntfile = "spectrt";
        }
        else if(evt.getSource() == jrbsimul) {
            ntfile = "simul";
        }
        // file choice
        JFileChooser jfcfile = new JFileChooser(workd);              // default choice directory
        jfcfile.setSize(400,300);
        jfcfile.setFileSelectionMode(JFileChooser.FILES_ONLY);       // files only
        jfcfile.setDialogTitle("Define the "+ntfile+" file to be visualized");
        Container parent = jrbnone.getParent();
        int choice = jfcfile.showDialog(parent,"Select");            // Dialog, Select
        nxyf = new String[1];
        if (choice == JFileChooser.APPROVE_OPTION) {
            nxyf[0] = jfcfile.getSelectedFile().getAbsolutePath();   // data file name (full)
            // using thread
            prepThread("Loading File ...");                          // wait message
            threadType = "read";                                     // set run() action
            Thread threadThis = new Thread(this);                    // set thread (run of this instance)
            threadThis.start();                                      // launch it
        }
        else {
            askFileType();
        }
    }

/////////////////////////////////////////////////////////////////////

    /**
     * Thread launched by start.
     * <br>Processes read of data files and draw of graphs,
     * <br>or print of graphs.
     */
    public void run() {

        // action type selection
        if( threadType.equals("read") ) {
            // read and display data
            if( readFile() ) {                                       // read data in file(s)
                CJPdraw();                                           // display data
            }
            else {                                                   // bad file, ask for an other one
                askFileType();
            }
        }
        else {
            // print data
            PrinterJob printJob = PrinterJob.getPrinterJob();        // set printer job
            PageFormat pf = printJob.defaultPage();                  // set page format
            pf.setOrientation(PageFormat.LANDSCAPE);
            printJob.setPrintable(panaff,pf);                        // associate to PanAffich
            if (printJob.printDialog()) {                            // set printing parameters
                try {
                    printJob.print();                                // print
                }
                catch (PrinterException pe) {
                    JOptionPane.showMessageDialog(null,"Printer exception"+lnsep+
                                                       pe);
                }
            }
            CJPdraw();                                               // redraw all windows
        }
        revalidate();                                                // confirm
    }

/////////////////////////////////////////////////////////////////////

    // thread preparation by adding progress bar
    private void prepThread(String text) {

        removeAll();                                                 // remove all
        repaint();                                                   // actualize
        setLayout(new BorderLayout());
        JProgressBar progressBar = new JProgressBar();               // cretae a progress bar
        progressBar.setStringPainted(true);                          // with  a title
        progressBar.setString(text);                                 // set the title
        progressBar.setIndeterminate(true);                          // task length unknown
        Box box =Box.createVerticalBox();                            // box
        box.add(Box.createVerticalStrut( (int) (this.getHeight()/2.) ));  // window centered
        box.add(progressBar);                                        // add JProgressBar
        add(box,"North");                                            // add box
    }

/////////////////////////////////////////////////////////////////////

    // add display window
    private void CJPdraw() {

        removeAll();                                                 // remove all
        setLayout(new BorderLayout());
        if     ( ntfile.equals("jener") ) {
            setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLUE,4),"Level Display"));
        }
        else if( ntfile.equals("trni") ) {
            setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLUE,4),"Level Mixing Display"));
        }
        else if( ntfile.equals("spectr") ) {
            setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLUE,4),"Stick Spectrum Display"));
        }
        else if( ntfile.equals("simul") ) {
            setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLUE,4),"Simulation Display"));
        }
        else if( ntfile.equals("predmixf_I") ||
                 ntfile.equals("predmixf_R")    ) {
            setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLUE,4),"Frequency Obs-Cal Display"));
        }
        else if( ntfile.equals("predmixi_I") ||
                 ntfile.equals("predmixi_R")    ) {
            setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLUE,4),"Intensity Obs-Cal Display"));
        }

        // south panel to go back to data file type choice
        jbreset = new JButton("Reset All");                          // reset button
        jbreset.setBackground(Color.WHITE);
        jbreset.addActionListener(this);
        Box boxsud = Box.createHorizontalBox();                      // add buttons to box
        boxsud.add(jbreset);
        psud = new JPanel();
        psud.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        psud.add(boxsud);                                            // add box

        // west panel for control buttons
        boxouest = Box.createVerticalBox();                          // add buttons to box
        jbprint = new JButton("Print");                              // print button
        jbprint.setBackground(Color.WHITE);
        jbprint.addActionListener(this);
        boxouest.add(jbprint);
        boxouest.add(Box.createVerticalStrut(5));
        boxouest.add(new JLabel(" ------------ "));
        boxouest.add(Box.createVerticalStrut(5));
        boxouest.add(new JLabel("Left click and"));                  // zoom explaination
        boxouest.add(Box.createVerticalStrut(5));
        boxouest.add(new JLabel("drag to select"));
        boxouest.add(Box.createVerticalStrut(5));
        boxouest.add(new JLabel("a zoom area"));
        boxouest.add(Box.createVerticalStrut(15));
        jbunzoom = new JButton("Unzoom");                            // unzoom button
        jbunzoom.setBackground(Color.WHITE);
        jbunzoom.addActionListener(this);
        boxouest.add(jbunzoom);
        boxouest.add(Box.createVerticalStrut(5));
        boxouest.add(new JLabel(" ------------ "));
        boxouest.add(Box.createVerticalStrut(5));
        if( ntfile.equals("predmixf_I") ||
            ntfile.equals("predmixf_R") ||
            ntfile.equals("predmixi_I") ||
            ntfile.equals("predmixi_R")    ) {
            boxouest.add(new JLabel("Right click and"));             // selection explaination
            boxouest.add(Box.createVerticalStrut(5));
            boxouest.add(new JLabel("drag to select"));
            boxouest.add(Box.createVerticalStrut(5));
            boxouest.add(new JLabel("an obs-cal"));
            boxouest.add(Box.createVerticalStrut(5));
            boxouest.add(new JLabel("selection area"));
            boxouest.add(Box.createVerticalStrut(5));
            boxouest.add(new JLabel(" ------------ "));
            boxouest.add(Box.createVerticalStrut(5));
        }
        //
        if( ntfile.equals("trni")       ||
            ntfile.equals("predmixf_I") ||
            ntfile.equals("predmixf_R") ||
            ntfile.equals("predmixi_I") || 
            ntfile.equals("predmixi_R")    ) {
            jrbcomp = new JRadioButton[nbcomp];
            for( int i=0; i<nbcomp; i++ ) {
                jrbcomp[i] = new JRadioButton(limcomp[0][i]+"-"+limcomp[1][i]);
                jrbcomp[i].setSelected(true);
                jrbcomp[i].setBackground(coul[i - coul.length*(i/coul.length)]);
                jrbcomp[i].addActionListener(this);
                boxouest.add(jrbcomp[i]);
            }
        }

        pouest = new JPanel();
        pouest.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLUE,4),null));
        pouest.add(boxouest);

        // center panel for drawing panel
        pcentre = new JPanel(new BorderLayout());
        pcentre.setBackground(Color.WHITE);
        // if panaff exists, it is due to a print thread endr,
        // then keep it (for its possible zoom state)
        if( panaff == null ) {                                       // if panaff does not exist
            // create it
            if( ntfile == "trni" ) {
                panaff = new PanAffich(x,y,nxyf,ntfile,jlx,jly,concomp,limcomp,icompa,coul);
            }
            else if( ntfile.equals("predmixf_I") ||
                     ntfile.equals("predmixf_R") ||
                     ntfile.equals("predmixi_I") ||
                     ntfile.equals("predmixi_R")    ) {
                panaff = new PanAffich(x,y,nxyf,ntfile,cntrans,jlx,jly,smark,jsyn,comp,limcomp,icompa,coul);
            }
            else {
                panaff = new PanAffich(x,y,nxyf,ntfile,jlx,jly);
            }
            panaff.setBackground(Color.WHITE);
        }
        pcc = new JPanel(new BorderLayout());
        pcc.add(panaff,"Center");
        pcc.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        pcentre.add(pcc,"Center");
        // for mouse position
        pcs =  new JPanel(new GridLayout(0,4,5,5));
        pcs.setBackground(Color.WHITE);
        pcs.add(new JLabel("x = ",null,JLabel.RIGHT));
        pcs.add(jlx);
        pcs.add(new JLabel("y = ",null,JLabel.RIGHT));
        pcs.add(jly);
        pcs.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        pcentre.add(pcs,"South");

        // add panels
        JScrollPane   jsp;                                       // with lifts
        jsp = new JScrollPane(pouest, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);  // with lifts
        add(jsp    ,"West");
        add(pcentre,"Center");
        add(psud   ,"South");
    }

/////////////////////////////////////////////////////////////////////

    // read data file(s)
    private boolean readFile() {

        if     ( ntfile == "jenert" ) {                              // jener.t case
            return readJenerTFile();
        }
        else if( ntfile == "trni" ) {                                // trni case
            return readTrniFiles();
        }
        else if( ntfile == "spectrt" ) {                             // spectr.t case
            return readSpectrTFile();
        }
        else if( ntfile == "predmix" ) {                             // predmix case
            return readPredmixFile();
        }
        else {                                                       // other cases
            return readXyFile();
        }
    }

/////////////////////////////////////////////////////////////////////

    // read *.xy data file
    private boolean readXyFile() {

        Double cx;                                                   // to keep X
        Double cy;                                                   // to keep Y
        // to keep points, unknown nb of points
        ArrayList alx = new ArrayList();                             // X
        ArrayList aly = new ArrayList();                             // Y
        try{
            File f = new File(nxyf[0]);
            br = new BufferedReader (new FileReader(f));             // open
            while ((str = br.readLine()) != null) {                  // as long as there is something to read
                StringTokenizer st = new StringTokenizer(str);       // separate X and Y
                if( st.countTokens() < 2 ) {                         // not enough tokens
                    JOptionPane.showMessageDialog(null,"Bad XY line in file"+lnsep+
                                                       nxyf[0]           +lnsep+
                                                       str);
                    return false;
                }
                try {
                    cx = Double.valueOf(st.nextToken());             // convert X in Double
                    cy = Double.valueOf(st.nextToken());             // and Y
                }
                catch (NumberFormatException e) {                    // format error
                    JOptionPane.showMessageDialog(null,"Format error in file"+lnsep+
                                                       nxyf[0]               +lnsep+
                                                       str                   +lnsep+
                                                       e);
                    return false;
                }
                alx.add(cx);                                         // save it
                aly.add(cy);
            }
        }
        catch(IOException ioe) {                                     // IO error
            JOptionPane.showMessageDialog(null,"IO error while reading file"+lnsep+
                                               nxyf[0]                      +lnsep+
                                               ioe);
            return false;
        }
        finally {                                                    // close
            if (br !=null) {
                try {
                    br.close();
                }
                catch (Exception ignored) {
                }
            }
        }
        // save data
        int nbxy = alx.size();                                       // nb of data
        if( nbxy == 0 ) {
            // no point
            JOptionPane.showMessageDialog(null,"No valid data found in file"+lnsep+
                                               nxyf[0]);
            return false;                                            // invalid read
        }
        x = new double[nbxy];                                        // create arrays
        y = new double[nbxy];
        // sort
        XYPoint[] xypoint = new XYPoint[nbxy];                       // compare X then Y
        for(int i=0; i<nbxy; i++) {
            xypoint[i] = new XYPoint( ((Double) alx.get(i)).doubleValue(), ((Double) aly.get(i)).doubleValue() );  // X, Y
        }
        Arrays.sort(xypoint);                                        // sort
        for(int i=0; i<nbxy; i++) {
            // save data
            x[i] = xypoint[i].getX();
            y[i] = xypoint[i].getY();
        }
        return true;
    }

/////////////////////////////////////////////////////////////////////

    // Ener (trni) file choice
    // and set nbop, beta0, gama0, pi0, nsv
    private boolean newTrniFile() {

        // file choice
        JFileChooser jfcfile = new JFileChooser(workd);              // default choice directory
        jfcfile.setSize(400,300);
        jfcfile.setFileSelectionMode(JFileChooser.FILES_ONLY);       // files only
        jfcfile.setDialogTitle("Define the "+ntfile+" file to be visualized");
        Container parent = jrbnone.getParent();
        int choice = jfcfile.showDialog(parent,"Select");            // Dialog, Select
        if (choice != JFileChooser.APPROVE_OPTION) {
            return false;
        }

        fich[nbfich] = new File(jfcfile.getSelectedFile().getAbsolutePath());
        tntf         = new TrniTFile( fich[nbfich].getAbsolutePath() );
        if( tntf.readHeader() ) {
            int    nsvc   = tntf.getNsv();
            double beta0c = tntf.getBeta0();
            double gama0c = tntf.getGama0();
            double pi0c   = tntf.getPi0();
            if( nbfich == 0 ) {
                nsv = nsvc;
                beta0 = beta0c;
                gama0 = gama0c;
                pi0   = pi0c;
                tsubl = new String[nsv];                             // sub-level choice array
                for(int j=0; j<nsv; j++) {
                    tsubl[j] = String.valueOf(j+1);                  // 1 -> nsv
                }
            }
            else if( nsvc   != nsv   ||
                     beta0c != beta0 ||
                     gama0c != gama0 ||
                     pi0c   != pi0      ) {
                JOptionPane.showMessageDialog(null,"Number of sublevels or "                           +lnsep+
                                                   "beta0               or "                           +lnsep+
                                                   "gama0               or "                           +lnsep+
                                                   "pi0"                                               +lnsep+
                                                   "incompatible in file"                              +lnsep+
                                                   fich[nbfich].getName()                              +lnsep+
                                                   "Found    ("+nsvc+" "+beta0c+" "+gama0c+" "+pi0c+")"+lnsep+
                                                   "Expected ("+nsv +" "+beta0 +" "+gama0 +" "+pi0 +")");
                return false;
            }
            nbop[nbfich] = tntf.getNbop();
            nbfich ++ ;
            return true;
        }
        else {
            return false;
        }

    }

/////////////////////////////////////////////////////////////////////

    // read Ener file data
    private boolean readTrniFiles() {
        // according to levlst.f FORMAT 1011 and 1012

        // intermediate save
        ArrayList alj     = new ArrayList();
        ArrayList alenerg = new ArrayList();
        ArrayList altic   = new ArrayList();
        boolean nextniv;                                             // if next level required
        int nsvl;                                                    // nb of read sub-levels
        int nblnsv    = (int) ((nsv+19.)/20.);                       // nb of nsv lines
        boolean allej = jrballej.isSelected();                       // All Energ. and J ?

        // first define extended (drawable or NOT) components
        int     xliminf[];                                           // extended liminf
        int     xlimsup[];                                           // extended limsup
        boolean xstate[];                                            // extended status : drawable or NOT
        xliminf = new int[nsv];
        xlimsup = new int[nsv];
        xstate  = new boolean[nsv];
        int elim    = 1;                                             // expected liminf
        int xnbcomp = 0;                                             // nb of extended components

        for( int i=0; i<nbcomp; i++ ) {                              // for each drawable component
            if( liminf[i] != elim ) {
                // NOT drawable component, create it
                xliminf[xnbcomp] = elim;
                xlimsup[xnbcomp] = liminf[i]-1;
                xstate[xnbcomp]  = false;
                xnbcomp ++;
            }
            // drawable component, add it
            xliminf[xnbcomp] = liminf[i];
            xlimsup[xnbcomp] = limsup[i];
            xstate[xnbcomp]  = true;
            icompa[i]        = xnbcomp;                              // current extended component is displayable
            xnbcomp ++;
            elim = limsup[i]+1;
        }
        if( elim <= nsv ) {
            // NOT drawable component, create it
            xliminf[xnbcomp] = elim;
            xlimsup[xnbcomp] = nsv;
            xstate[xnbcomp]  = false;
            xnbcomp ++;
        }
        // component limits
        limcomp = new int[2][xnbcomp];
        for( int j=0; j<xnbcomp; j++ ) {
            limcomp[0][j] = xliminf[j];                              // lower limit
            limcomp[1][j] = xlimsup[j];                              // upper limit
            if( ! xstate[j] ) {
                // NOT drawable => negative
                limcomp[0][j] = -limcomp[0][j];
                limcomp[1][j] = -limcomp[1][j];
            }
        }

        // then read data
        for( int nfc=0; nfc<nbfich; nfc++ ) {                        // for each data file
            try{
                br = new BufferedReader (new FileReader(fich[nfc]));  // open
                int i = 0;                                           // current line number
                // go to the right line
                while ((str = br.readLine()) != null) {              // as long as there is something to read
                    i++;
                    if(i == nbop[nfc]+9 ) {
                        break;
                    }
                }
                // extract data
                while ((str = br.readLine()) != null) {              // as long as there is something to read
                    nextniv = false;
                    nsvl = 0;
                    int[] tic = new int[xnbcomp];                    // percent array of each component of this level
                    Arrays.fill(tic,0);                              // set it to 0
                    try {
                        char   cfasc = str.charAt(1);                                    // CFAS  format 1011
                        int    jc    = Integer.parseInt(str.substring(5,8).trim());      // J     format 1011
                        double ec    = Double.parseDouble(str.substring(16,31).trim());  // ENERG format 1011
                        int aj = jc*(jc+1);
                        ec = ec-aj*(beta0+aj*(gama0+aj*pi0));        // set energy

                        if( jrbassign.isSelected()         &&        // assigned only
                            (cfasc == ' ' || cfasc == '-') ) {       // not assigned
                            nextniv = true;
                        }
                        if( ! allej     &&
                            (jc<jmin ||
                             jc>jmax ||
                             ec<emin ||
                             ec>emax   )   ) {
                            nextniv = true;                          // level out of selected choice
                        }
                        if( ! nextniv ) {
                            alj.add(new Double(jc));                 // J
                            alenerg.add(new Double(ec));             // ENERG
                        }
                        for( i=0; i<nblnsv; i++ ) {                  // format 1012
                            if( (str = br.readLine()) == null ) {    // read a line
                                JOptionPane.showMessageDialog(null,"Unexpected EOF while reading file"+lnsep+
                                                                   fich[nfc].getName());
                                return false;
                            }
                            if( nextniv ) {                          // nothing to do
                                continue;
                            }
                            for( int j=0; j<20; j++ ) {              // 20 items per line
                                nsvl++;
                                int cdeb = 10+j*(2*4+1);             // 2I4,'%'
                                int kcent;
                                try {
                                    kcent = Integer.parseInt(str.substring(cdeb,cdeb+4).trim());  // KCENT format I4
                                }
                                catch (NumberFormatException e) {    // format error
                                    JOptionPane.showMessageDialog(null,"KCENT format error in file"+lnsep+
                                                                       fich[nfc].getName()         +lnsep+
                                                                       str                         +lnsep+
                                                                       e);
                                    return false;
                                }
                                for( int k=0; k<xnbcomp; k++ ) {     // for each extended component
                                    if( (kcent >= xliminf[k]) &&
                                        (kcent <= xlimsup[k])    ) {
                                        try {
                                            tic[k] = tic[k]+Integer.parseInt(str.substring(cdeb+4,cdeb+8).trim());  // add ICENT format I4
                                        }
                                        catch (NumberFormatException e) {  // format error
                                            JOptionPane.showMessageDialog(null,"ICENT format error in file"+lnsep+
                                                                               fich[nfc].getName()         +lnsep+
                                                                               str                         +lnsep+
                                                                               e);
                                            return false;
                                        }
                                        break;                       // component found
                                    }
                                }
                                if( nsvl == nsv ) {                  // everything is read
                                    break;
                                }
                            }
                        }
                        if( ! nextniv ) {
                            altic.add(tic);
                        }
                    }
                    catch (NumberFormatException e) {                // format error
                        JOptionPane.showMessageDialog(null,"J or ENERG format error in file"+lnsep+
                                                           fich[nfc].getName()              +lnsep+
                                                           str                              +lnsep+
                                                           e);
                        return false;
                    }
                }

            }
            catch(IOException ioe) {                                 // IO error
                JOptionPane.showMessageDialog(null,"IO error while reading file"+lnsep+
                                                   fich[nfc].getName()          +lnsep+
                                                   ioe);
                return false;
            }
            finally {                                                // close
                if (br !=null) {
                    try {
                        br.close();
                    }
                    catch (IOException ignored) {
                    }
                }
            }
        }

        // transfer data in X and Y
        int nbxy = alj.size();                                       // nb of data
        if( nbxy == 0 ) {
            JOptionPane.showMessageDialog(null,"No level found");
            return false;
        }
        // sort
        TrniPoint[] tpt = new TrniPoint[nbxy];                       // compare X then Y
        for(int i=0; i<nbxy; i++) {
            tpt[i] = new TrniPoint( ((Double) alj.get(i)).doubleValue(),
                                    ((Double) alenerg.get(i)).doubleValue(),
                                    (int[]) altic.get(i) );
        }
        Arrays.sort(tpt);                                            // sort
        // save data
        x = new double[nbxy];                                        // create arrays
        y = new double[nbxy];
        concomp = new int[nbxy][xnbcomp];
        for(int i=0; i<nbxy; i++) {
            x[i] = tpt[i].getJ();
            y[i] = tpt[i].getEnerg();
            System.arraycopy(tpt[i].getTic(),0,concomp[i],0,xnbcomp);
            int somcomp = 0;
            for( int j=0; j<xnbcomp; j++ ) {
                concomp[i][j] = concomp[i][j]*100;                   // 100 -> 10000 for stick length precision
                somcomp       = somcomp + concomp[i][j];             // component sum total
            }
            for( int j=0; j<xnbcomp; j++ ) {
                concomp[i][j] = (int) (concomp[i][j]*10000./somcomp);  // to get exactly 100%
            }
        }
        return true;
    }

/////////////////////////////////////////////////////////////////////

    // get Ener file drawing parameters
    private void getTrniParm() {

        removeAll();
        setLayout(new BorderLayout());
        // west panel to add file or component
        nbfich = 0;
        fich = new File[mxnbfich];
        nbop = new int[mxnbfich];

        boxouest = Box.createVerticalBox();                          // add button to box
        boxouest.add(Box.createVerticalStrut(100));
        jbaddfich = new JButton("Add File");                         // ADD FILE button
        jbaddfich.setBackground(Color.WHITE);
        jbaddfich.addActionListener(this);
        boxouest.add(jbaddfich);
        boxouest.add(Box.createVerticalStrut(50));

        // initialization
        nbcomp    = 0;
        boxcomp   = new Box[mxnbcomp];
        jcbliminf = new JComboBox[mxnbcomp];
        jcblimsup = new JComboBox[mxnbcomp];
        liminf    = new int[mxnbcomp];
        limsup    = new int[mxnbcomp];

        jbaddcomp = new JButton("Add Component");                    // ADD COMPONENT button
        jbaddcomp.setBackground(Color.WHITE);
        jbaddcomp.addActionListener(this);
        boxouest.add(jbaddcomp);
        boxouest.add(Box.createVerticalStrut(20));
        jbloadcompf = new JButton("Load Comp. File");                // LOAD COMP. FILE button
        jbloadcompf.setBackground(Color.WHITE);
        jbloadcompf.addActionListener(this);
        boxouest.add(jbloadcompf);
        boxouest.add(Box.createVerticalStrut(20));
        jbsavecompf = new JButton("Save Comp. File");                // SAVE COMP. FILE button
        jbsavecompf.setBackground(Color.WHITE);
        jbsavecompf.addActionListener(this);
        boxouest.add(jbsavecompf);
        pouest = new JPanel();
        pouest.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLUE,4),null));
        pouest.add(boxouest);

        // pcn panel for parameter, except components
        pcentre = new JPanel(new BorderLayout());
        pcn   = new JPanel(new GridLayout(3,5,5,5));
        // catching formats
        nffloat = NumberFormat.getInstance(Locale.US);
        nffloat.setGroupingUsed(false);                              // ie. no thousands seperator
        nffloat.setMaximumFractionDigits(100);                       // ie. no more then 10 digits for fractionnal part
        nfint = NumberFormat.getInstance();
        nfint.setGroupingUsed(false);                                // ie. no thousands seperator
        nfint.setParseIntegerOnly(true);                             // ie. only integers
        nfint.setMaximumIntegerDigits(7);                            // ie. no more then 7 digits
        // All E and J
        jrballej = new JRadioButton("All Energ. and J");
        jrballej.setBackground(Color.WHITE);
        jrballej.setSelected(true);
        jrballej.addActionListener(this);
        pcn.add(jrballej);
        // EMIN choice
        pcn.add(new JLabel("Emin  ",null,JLabel.RIGHT));
        jftfemin = new JFormattedTextField(nffloat);
        jftfemin.setValue(new Float(0.0));
        jftfemin.setEnabled(false);
        pcn.add(jftfemin);
        // JMIN choice
        pcn.add(new JLabel("Jmin  ",null,JLabel.RIGHT));
        jftfjmin = new JFormattedTextField(nfint);
        jftfjmin.setValue(new Integer(0));
        jftfjmin.setEnabled(false);
        pcn.add(jftfjmin);
        //
        pcn.add(new JLabel());
        // EMAX choice
        pcn.add(new JLabel("Emax  ",null,JLabel.RIGHT));
        jftfemax = new JFormattedTextField(nffloat);
        jftfemax.setValue(new Float(0.0));
        jftfemax.setEnabled(false);
        pcn.add(jftfemax);
        // JMAX choice
        pcn.add(new JLabel("Jmax  ",null,JLabel.RIGHT));
        jftfjmax = new JFormattedTextField(nfint);
        jftfjmax.setValue(new Integer(0));
        jftfjmax.setEnabled(false);
        pcn.add(jftfjmax);
        // assignment choice
        jrbassign = new JRadioButton("Assigned");
        jrbassign.setBackground(Color.WHITE);
        pcn.add(jrbassign);
        pcn.add(new JLabel());
        pcn.add(new JLabel());
        pcn.add(new JLabel());
        pcn.add(new JLabel());

        pcn.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLUE,4),"BASICS"));
        pcc  = new JPanel(new BorderLayout());
        jsp  = new JScrollPane(pcc, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        pccn = new JPanel(new GridLayout(0,1,5,5));                  // for files
        pccn.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLUE,4),"DATA FILES"));
        pccc  = new JPanel(new BorderLayout());
        pcccn = new JPanel(new GridLayout(0,3,10,10));
        pcccn.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLUE,4),"COMPONENTS"));
        pccc.add(pcccn,"North");
        pcc.add(pccn,"North");
        pcc.add(pccc,"Center");

        pcentre.add(pcn,"North");
        pcentre.add(jsp,"Center");

        // south panel for reset and show
        jbreset = new JButton("Reset All");                          // reset button
        jbreset.setBackground(Color.WHITE);
        jbreset.addActionListener(this);
        jbshow = new JButton("Show");                                // show trni button
        jbshow.setBackground(Color.WHITE);
        jbshow.addActionListener(this);
        Box boxsud = Box.createHorizontalBox();                      // add button to box
        boxsud.add(jbreset);
        boxsud.add(Box.createHorizontalStrut(15));
        boxsud.add(jbshow);
        psud = new JPanel();
        psud.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        psud.add(boxsud);                                            // add box

        // display panels
        add(pouest, "West");
        add(pcentre,"Center");
        add(psud,   "South");
    }

/////////////////////////////////////////////////////////////////////

    // check drawing parameters
    private boolean testTrni() {

        // BASICS
        if( ! testBasics() ) {
            return false;
        }
        // file(s)
        if( nbfich == 0 ) {
            JOptionPane.showMessageDialog(null,"You have to first choose at least one data file");
            return false;
        }
        // save their names
        nxyf = new String[nbfich];
        for( int nfc=0; nfc<nbfich; nfc++ ) {
            nxyf[nfc] = fich[nfc].getAbsolutePath();
        }
        // check components
        if( ! testComp() ) {
            return false;
        }
        return true;
    }

/////////////////////////////////////////////////////////////////////

    // check BASICS
    private boolean testBasics() {

        if( ! jrballej.isSelected() ) {
          // EMIN
          emin = ((Number)jftfemin.getValue()).doubleValue();
          // EMAX
          emax = ((Number)jftfemax.getValue()).doubleValue();
          if( emax <= emin ) {
              JOptionPane.showMessageDialog(null,"Emax > Emin requested");
              jftfemax.setValue(new Float(0.0));
              return false;
          }
          // JMIN
          jmin = ((Number)jftfjmin.getValue()).intValue();
          if( jmin < 0 ) {
              JOptionPane.showMessageDialog(null,"Jmin >= 0 requested");
              jftfjmin.setValue(new Integer(0));
              return false;
          }
          // JMAX
          jmax = ((Number)jftfjmax.getValue()).intValue();
          if( jmax < jmin ) {
              JOptionPane.showMessageDialog(null,"Jmax >= Jmin requested");
              jftfjmax.setValue(new Integer(0));
              return false;
          }
        }
        return true;
    }

/////////////////////////////////////////////////////////////////////

    // read a double from a jtf
    private boolean testDouble(JTextField jtf, double dbl) {

        try {
            dbl = Double.parseDouble(jtf.getText());
        }
        catch(NumberFormatException nfe) {                           // format error
            return false;
        }
        return true;
    }

/////////////////////////////////////////////////////////////////////

    // check component consistency
    private boolean testComp() {

        // nb of components
        if( nbcomp == 0 ) {
            JOptionPane.showMessageDialog(null,"You have to first choose at least one component");
            return false;
        }
        // suppress last component if it is empty
        climinf = (String) jcbliminf[nbcomp-1].getSelectedItem();
        climsup = (String) jcblimsup[nbcomp-1].getSelectedItem();
        if( (climinf == "") &&
            (climsup == "") ) {
            int n = JOptionPane.showConfirmDialog(null,"Last component is empty"+lnsep+
                                                       "OK to suppress ?"       +lnsep,
                                                       "Empty component",JOptionPane.YES_NO_OPTION);
            if(n == JOptionPane.YES_OPTION) {
                pcccn.remove(boxcomp[nbcomp-1]);
                pcccn.revalidate();
                pccc.repaint();
                // go to bottom to show the last component
                SwingUtilities.invokeLater(new Runnable() {
                     public void run() {
                         final JScrollBar jsbv = jsp.getVerticalScrollBar();
                         jsbv.setValue(jsbv.getMaximum());
                     }
                });
                //
                nbcomp--;
            }
            return false;
        }
        // check each component
        toussniv = true;
        for( int i=0; i<nbcomp; i++ ) {
            // empty ?
            climinf = (String) jcbliminf[i].getSelectedItem();
            climsup = (String) jcblimsup[i].getSelectedItem();
            if( (climinf == "") &&
                (climsup == "") ) {
                JOptionPane.showMessageDialog(null,"Component #"+(i+1)+" is empty"                    +lnsep+
                                                   "Only the last component may be supressed if empty");
                return false;
            }
            if( climinf == "" ) {
                JOptionPane.showMessageDialog(null,"Lower limit empty in component #"+(i+1));
                return false;
            }
            if( climsup == "" ) {
                JOptionPane.showMessageDialog(null,"Upper limit empty in component #"+(i+1));
                return false;
            }
            // save
            liminf[i] = Integer.parseInt(climinf);
            limsup[i] = Integer.parseInt(climsup);
            // consistency checking
            if( liminf[i] < 1 ) {
                JOptionPane.showMessageDialog(null,"Lower limit >= 1"              +lnsep+
                                                   "requested in component #"+(i+1));
                return false;
            }
            if( limsup[i] < 1 ) {
                JOptionPane.showMessageDialog(null,"Upper limit >= 1"              +lnsep+
                                                   "requested in component #"+(i+1));
                return false;
            }
            if( liminf[i] > limsup[i] ) {
                JOptionPane.showMessageDialog(null,"Lower limit <= Upper limit"    +lnsep+
                                                   "requested in component #"+(i+1));
                return false;
            }
            if( liminf[i] > nsv ) {
                JOptionPane.showMessageDialog(null,"Lower limit <= "+nsv+" (number of sublevels)"+lnsep+
                                                   "requested in component #"+(i+1));
                return false;
            }
            if( limsup[i] > nsv ) {
                JOptionPane.showMessageDialog(null,"Upper limit <= "+nsv+" (number of sublevels)"+lnsep+
                                                   "requested in component #"+(i+1));
                return false;
            }
            if( (i>0) && (liminf[i] <= limsup[i-1]) ) {
                JOptionPane.showMessageDialog(null,"Lower limit of #"+(i+1)+" is <= to Upper limit of #"+i);
                return false;
            }
            if( (i>0) && (liminf[i] != (limsup[i-1]+1)) ) {
                toussniv = false;
            }
        }
        // are all levels included ?
        if( liminf[0] != 1 ) {
            toussniv = false;
        }
        if( limsup[nbcomp-1] != nsv ) {
            toussniv = false;
        }
        return true;
    }

/////////////////////////////////////////////////////////////////////

    // Predmix file choice
    private boolean newPredmixFile() {

        // file choice
        JFileChooser jfcfile = new JFileChooser(workd);              // default choice directory
        jfcfile.setSize(400,300);
        jfcfile.setFileSelectionMode(JFileChooser.FILES_ONLY);       // files only
        jfcfile.setDialogTitle("Define the "+ntfile+" file to be visualized");
        Container parent = jrbnone.getParent();
        int choice = jfcfile.showDialog(parent,"Select");            // Dialog, Select
        if (choice != JFileChooser.APPROVE_OPTION) {
            return false;
        }

        fich[0] = new File(jfcfile.getSelectedFile().getAbsolutePath());
        nxyf[0] = fich[0].getAbsolutePath();
        pmtf    = new PredmixTFile( nxyf[0] );
        if( pmtf.readHeader() ) {
            xfile    = pmtf.isXfile();
            if( ! xfile ) {
                nsv = pmtf.getNsv();
                tsubl = new String[nsv];                                 // sub-level choice array
                for(int j=0; j<nsv; j++) {
                    tsubl[j] = String.valueOf(j+1);                      // 1 -> nsv
                }
            }
            nbfich  = 1;
            return true;
        }
        else {
            return false;
        }
    }

/////////////////////////////////////////////////////////////////////

    // read a Pred_mix file
    private boolean readPredmixFile() {

        // plot J type
        if     ( jrbpmjinf.isSelected() ) {
           pmtf.setPlotJinf();
        }
        else if( jrbpmjsup.isSelected() ) {
          pmtf.setPlotJsup();
        }
        else if( jrbpmfreq.isSelected() ) {
          pmtf.setPlotFreq();
        }
        else if( jrbpmint.isSelected()  ) {
          pmtf.setPlotInt();
        }
        
        // component limits
        limcomp = new int[2][nbcomp];
        for( int j=0; j<nbcomp; j++ ) {
            limcomp[0][j] = liminf[j];                              // lower limit
            limcomp[1][j] = limsup[j];                              // upper limit
            icompa[j]     = j;                                      // current component is displayable
        }

        // read Pred_mix file
        if( pmtf.read(cntrans,limsup,liminf) ) {
//          int nbxy;
//          nbxy   = pmtf.getNbxy();                                 // nb of pred data
            x      = pmtf.getX();
            y      = pmtf.getY();
            smark  = pmtf.getSmark();
            jsyn   = pmtf.getJsyn();
            comp   = pmtf.getComp();
            ntfile = pmtf.getNtfile();
            return true;
        }
        else {
            return false;
        }
    }

/////////////////////////////////////////////////////////////////////

    // get Predmix file drawing parameters
    private void getPredmixParm() {

        removeAll();
        setLayout(new BorderLayout());
        // west panel to add file or component
        nbfich = 0;
        fich = new File[1];
        nxyf = new String[1];
        cntrans = "";

        boxouest = Box.createVerticalBox();                          // add button to box
        boxouest.add(Box.createVerticalStrut(100));
        jrbpmjinf = new JRadioButton("Jinf");
        jrbpmjsup = new JRadioButton("Jsup");
        jrbpmfreq = new JRadioButton("Freqency");
        jrbpmint = new JRadioButton("Intensity");
        jrbnone.setSelected(true);
        group = new ButtonGroup();                                   // jrb group
        group.add(jrbpmjinf);                                        // buttons in group
        group.add(jrbpmjsup);
        group.add(jrbpmfreq);
        group.add(jrbpmint);
        group.add(jrbnone);
        jbaddfich = new JButton("Add File");                         // ADD FILE button
        jbaddfich.setBackground(Color.WHITE);
        jbaddfich.addActionListener(this);
        boxouest.add(jbaddfich);
        boxouest.add(Box.createVerticalStrut(50));

        // initialization
        nbcomp    = 0;
        boxcomp   = new Box[mxnbcomp];
        jcbliminf = new JComboBox[mxnbcomp];
        jcblimsup = new JComboBox[mxnbcomp];
        liminf    = new int[mxnbcomp];
        limsup    = new int[mxnbcomp];

        jbaddcomp = new JButton("Add Component");                    // ADD COMPONENT button
        jbaddcomp.setBackground(Color.WHITE);
        jbaddcomp.addActionListener(this);
        boxouest.add(jbaddcomp);
        boxouest.add(Box.createVerticalStrut(20));
        jbloadcompf = new JButton("Load Comp. File");                // LOAD COMP. FILE button
        jbloadcompf.setBackground(Color.WHITE);
        jbloadcompf.addActionListener(this);
        boxouest.add(jbloadcompf);
        boxouest.add(Box.createVerticalStrut(20));
        jbsavecompf = new JButton("Save Comp. File");                // SAVE COMP. FILE button
        jbsavecompf.setBackground(Color.WHITE);
        jbsavecompf.addActionListener(this);
        boxouest.add(jbsavecompf);
        pouest = new JPanel();
        pouest.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLUE,4),null));
        pouest.add(boxouest);

        // pcn panel for parameter, except components
        pcentre = new JPanel(new BorderLayout());

        pcc  = new JPanel(new BorderLayout());
        jsp  = new JScrollPane(pcc, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        pccn = new JPanel(new BorderLayout());                  // for files
        pccn.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLUE,4),"DATA"));
        pccno = new JPanel(new GridLayout(3,0,5,5));
        pccno.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        pccnc = new JPanel(new GridLayout(3,0,5,5));
        pccc  = new JPanel(new BorderLayout());
        pcccn = new JPanel(new GridLayout(0,3,10,10));
        pcccn.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLUE,4),"COMPONENTS"));
        pccn.add(pccno,"West");
        pccn.add(pccnc,"Center");
        pccc.add(pcccn,"North");
        pcc.add(pccn,"North");
        pcc.add(pccc,"Center");

        pcentre.add(jsp,"Center");

        // south panel for reset and show
        jbreset = new JButton("Reset All");                          // reset button
        jbreset.setBackground(Color.WHITE);
        jbreset.addActionListener(this);
        jbshow = new JButton("Show");                                // show trni button
        jbshow.setBackground(Color.WHITE);
        jbshow.addActionListener(this);
        Box boxsud = Box.createHorizontalBox();                      // add button to box
        boxsud.add(jbreset);
        boxsud.add(Box.createHorizontalStrut(15));
        boxsud.add(jbshow);
        psud = new JPanel();
        psud.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        psud.add(boxsud);                                            // add box

        // display panels
        add(pouest, "West");
        add(pcentre,"Center");
        add(psud,   "South");
    }

/////////////////////////////////////////////////////////////////////

    // read a spectr.t file
    private boolean readSpectrTFile() {

        // instanciate a spectr.t file
        SpectrTFile cstf = new SpectrTFile( nxyf[0] );
        // read spectr.t file
        if( cstf.read() ) {
//          int nbxy;
//          nbxy   = cstf.getNbxy();                                 // nb of pred data
//          x      = new double[nbxy];                               // frequency
            x      = cstf.getX();
//          y      = new double[nbxy];                               // intensity
            y      = cstf.getY();
            ntfile = "spectr";
            return true;
        }
        else {
            return false;
        }
    }

/////////////////////////////////////////////////////////////////////

    // read a jener.t file
    private boolean readJenerTFile() {

        // instanciate a jener.t file
        JenerTFile cjtf = new JenerTFile( nxyf[0] );
        // read jener.t file
        if( cjtf.read() ) {
//          int nbxy;
//          nbxy   = cjtf.getNbxy();                                 // nb of pred data
//          x      = new double[nbxy];                               // J
            x      = cjtf.getX();
//          y      = new double[nbxy];                               // energy
            y      = cjtf.getY();
            ntfile = "jener";
            return true;
        }
        else {
            return false;
        }
    }

/////////////////////////////////////////////////////////////////////

    // add a component to pcccn
    private void newComp( int vinf, int vsup ) {

        // initialize the new component
        jcbliminf[nbcomp] = new JComboBox(tsubl);
        jcbliminf[nbcomp].addItem("");
        jcbliminf[nbcomp].setBackground(Color.WHITE);
        jcbliminf[nbcomp].setSelectedItem(""+vinf);
        jcblimsup[nbcomp] = new JComboBox(tsubl);
        jcblimsup[nbcomp].addItem("");
        jcblimsup[nbcomp].setBackground(Color.WHITE);
        jcblimsup[nbcomp].setSelectedItem(""+vsup);
        boxcomp[nbcomp] = Box.createHorizontalBox();
        boxcomp[nbcomp].add(new JLabel("  #"+(nbcomp+1)+" "));
        boxcomp[nbcomp].add(jcbliminf[nbcomp]);
        boxcomp[nbcomp].add(new JLabel(" up to "));
        boxcomp[nbcomp].add(jcblimsup[nbcomp]);
        boxcomp[nbcomp].setBorder(BorderFactory.createLineBorder(Color.BLACK));
        pcccn.add(boxcomp[nbcomp]);
        // go to bottom to show the last component
        SwingUtilities.invokeLater(new Runnable() {
             public void run() {
                 final JScrollBar jsbv = jsp.getVerticalScrollBar();
                 jsbv.setValue(jsbv.getMaximum());
             }
        });
        //
        nbcomp++;
    }

/////////////////////////////////////////////////////////////////////

    // read component file
    private boolean readCompFile() {

        Integer cx;                                                  // to keep lower limit
        Integer cy;                                                  // to keep upper limit
        // to keep points, unknown nb of points
        ArrayList alx = new ArrayList();                             // X
        ArrayList aly = new ArrayList();                             // Y
        try{
            File f = new File(ncompf);
            br = new BufferedReader (new FileReader(f));             // open
            while ((str = br.readLine()) != null) {                  // as long as there is something to read
                StringTokenizer st = new StringTokenizer(str);       // separate X and Y
                if( st.countTokens() < 2 ) {                         // not enough tokens
                    JOptionPane.showMessageDialog(null,"Bad line in file"+lnsep+
                                                       ncompf            +lnsep+
                                                       str);
                    return false;
                }
                try {
                    cx = Integer.valueOf(st.nextToken());            // convert X in Integer
                    cy = Integer.valueOf(st.nextToken());            // and Y
                }
                catch (NumberFormatException e) {                    // format error
                    JOptionPane.showMessageDialog(null,"Format error in file"+lnsep+
                                                       ncompf                +lnsep+
                                                       str                   +lnsep+
                                                       e);
                    return false;
                }
                if( cx.intValue() < 1   ||
                    cx.intValue() > nsv ||
                    cy.intValue() < 1   ||
                    cy.intValue() > nsv   ) {
                    JOptionPane.showMessageDialog(null,"Invalid value in file"+lnsep+
                                                       ncompf                 +lnsep+
                                                       str);
                    return false;
                }
                alx.add(cx);                                         // save it
                aly.add(cy);
            }
        }
        catch(IOException ioe) {                                     // IO error
            JOptionPane.showMessageDialog(null,"IO error while reading file"+lnsep+
                                               ncompf                       +lnsep+
                                               ioe);
            return false;
        }
        finally {                                                    // close
            if (br !=null) {
                try {
                    br.close();
                }
                catch (Exception ignored) {
                }
            }
        }
        // save data
        nbflim = alx.size();                                         // nb of components in file
        if( nbflim == 0 ) {
            // no point
            JOptionPane.showMessageDialog(null,"No valid data found in file"+lnsep+
                                               ncompf);
            return false;                                            // invalid read
        }
        fliminf = new int[nbflim];                                   // create arrays
        flimsup = new int[nbflim];
        // save
        for(int i=0; i<nbflim; i++) {
            fliminf[i] =  ((Integer) alx.get(i)).intValue();
            flimsup[i] =  ((Integer) aly.get(i)).intValue();
        }
        return true;
    }

/////////////////////////////////////////////////////////////////////

    // save component file
    private void saveCompFile() {

        String str;
        int    len;

        try {
            out1 = new PrintWriter( new BufferedWriter( new FileWriter( ncompf ) ) );  // write the file

            for(int i=0; i<nbcomp; i++) {
                // for each component
                str = "   "+liminf[i];                               // at least 4 characters
                len = str.length();
                out1.print  (str.substring(len-4,len));              // I4
                out1.print  ("  ");                                  // 2X
                str = "   "+limsup[i];
                len = str.length();                                  // I4
                out1.println(str.substring(len-4,len));
            }
        }
        catch (IOException ioe) {                                    // IO error
            JOptionPane.showMessageDialog(null,"IO error while writing file"+lnsep+
                                               ncompf                       +lnsep+
                                               ioe);
            return;
        }
        finally {
            if( out1 != null ) {
                out1.close();
                if( out1.checkError() ) {
                    JOptionPane.showMessageDialog(null,"PrintWriter error while creating component file"+lnsep+
                                                       ncompf);
                    return;
                }
            }
        }
    }

}
