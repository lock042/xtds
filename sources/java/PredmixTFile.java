/*
 * Class for prediction_mix.t file
 */

import java.io.*;
import java.util.*;
import javax.swing.*;

/**
 * This class defines prediction_mix.t data.
 */
class PredmixTFile {

    private String   name;                                           // file name
    private int      nbxy;                                           // nb of data
    private double[] x;                                              // X
    private double[] y;                                              // Y
    private String[] smark;                                          // Pred_mix : selection mark
    private String[] jsyn;                                           // Pred_mix : assignment
    private int[]    comp;                                           // Pred_mix : component
    private String   ntfile;                                         // file type
    private int      nsv;                                            // nb of upper vibrational sublevels
    private int      nbtrans;                                        // nb              of transitions
    private String[] ntrans;                                         // name            of transitions
    private int[]    nsvtrans;                                       // nb of sublevels of transitions

    // intermediate save
    private ArrayList alj     = new ArrayList();
    private ArrayList aldiff  = new ArrayList();
    private ArrayList alsmark = new ArrayList();
    private ArrayList aljsyn  = new ArrayList();
    private ArrayList alcomp  = new ArrayList();

    private int      nbsftr;                                         // NBSTR/NBFTR from xws/xwf
    private int      lpcmax;                                         // nb of sublevels
    private int      nblnsv;                                         // nb of lines of sublevels
    private boolean  freq;                                           // freq   (not int)    Pred_mix file
    private boolean  xfile;                                          // xpafit (not eq_*.f) Pred_mix file
    private boolean  headOK;                                         // header read
    private int      nbxhl;                                          // nb of header lines of xfile file
    private int      nbpol;                                          // nb              of polyads
    private String[] npol;                                           // name            of polyads
    private int[]    nsvpol;                                         // nb of sublevels of polyads
    private int      nbcomp;                                         // nb          of components
    private int[]    limsup;                                         // upper limit of components
    private int[]    liminf;                                         // lower limit of components
    private boolean  plotjinf;                                       // plot Jinf type
    private boolean  plotjsup;                                       // plot Jsup type
    private boolean  plotfreq;                                       // plot Freq. type
    private boolean  plotint;                                        // plot Int.  type

    private String  str;                                             // to read file
    private BufferedReader br;
    private boolean eof;
    private String  lnsep;                                           // line separator

/////////////////////////////////////////////////////////////////////

    /**
     * Constructs a new PredmixTFile.
     * <br> (EQ_TDS, EQ_INT, XWF or XWS format)
     *
     * @param cname  name of the file
     */
    public PredmixTFile(String cname) {

        name = cname;                                                // file name

        lnsep  = System.getProperty("line.separator");
        nbxy   = 0;                                                  // nb of points
        freq   = true;                                               // default to freq   Pred_mix file
        xfile  = false;                                              // default to eq_*.f Pred_mix file
        headOK = false;                                              // header NOT read
        resetPlot();
    }

/////////////////////////////////////////////////////////////////////

    /**
     *  read file header
     */
    public boolean readHeader() {

        if( headOK ) {                                               // already done
            return true;
        }
        // check file type
        if( ! checkFileX() ) {                                       // error
            return false;
        }
        if( xfile ) {
            headOK = locReadHeaderX();                               // XW*  file
        }
        else {
            headOK = locReadHeader();                                // EQ_* file
        }
        return headOK;
    }

/////////////////////////////////////////////////////////////////////

    /**
     * Read file.
     * <br> validity is tested following FORMAT of eq_tds.f, eq_int.f, xwf.f and xws.f
     * <br> in particular 2005 and 2006 FORMAT.
     */
    public boolean read( String cntrans, int[] climsup, int[] climinf) {

        nbcomp = climsup.length;                                     // nb          of components
        limsup = climsup;                                            // upper limit of components
        liminf = climinf;                                            // lower limit of components
        // read file header
        if( ! readHeader() ) {
            return false;                                            // error
        }

        // is plot type set ?
        if( ! isSetPlot() ) {
             JOptionPane.showMessageDialog(null,"Plot type not set in PredmixTFile");
             return false;
        }
        // read
        if( xfile ) {
            if( ! readDataX(cntrans) ) {
                return false;                                        // error
            }
        }
        else {
            if( ! readData() ) {
                return false;                                        // error
            }
        }

        // transfer data in X, Y, smark, jsyn and comp
        nbxy = alj.size();                                           // nb of data
        if( nbxy == 0 ) {
            JOptionPane.showMessageDialog(null,"No valid data found in file"+lnsep+
                                               name);
            return false;
        }
        x     = new double[nbxy];                                    // create arrays
        y     = new double[nbxy];
        smark = new String[nbxy];
        jsyn  = new String[nbxy];
        comp  = new int[nbxy];
        // sort
        PredmixPoint[] ppt = new PredmixPoint[nbxy];                 // compare J then diff
        for(int i=0; i<nbxy; i++) {
            ppt[i] = new PredmixPoint( ((Double)  alj.get(i)).doubleValue()   ,
                                       ((Double)  aldiff.get(i)).doubleValue(),
                                       (String)   alsmark.get(i)              ,
                                       (String)   aljsyn.get(i)               ,
                                       ((Integer) alcomp.get(i)).intValue()     );
        }
        Arrays.sort(ppt);                                            // sort
        // save data
        for(int i=0; i<nbxy; i++) {
            x[i]     = ppt[i].getX();
            y[i]     = ppt[i].getDiff();
            smark[i] = ppt[i].getSmark();
            jsyn[i]  = ppt[i].getJsyn();
            comp[i]  = ppt[i].getComp();
        }
        // set ntfile
        if( freq ) {
            ntfile = "predmixf";
        }
        else {
            ntfile = "predmixi";
        }
        if     ( plotjinf ||
                 plotjsup    ) {
            ntfile = ntfile+"_I";
        }
        else if( plotfreq ||
                 plotint     ) {
            ntfile = ntfile+"_R";
        }
        return true;
    }

/////////////////////////////////////////////////////////////////////

    /**
     * Get number of data points.
     */
    public int getNbxy() {

        return nbxy;
    }

/////////////////////////////////////////////////////////////////////

    /**
     * Get X array.
     */
    public double[] getX() {

        return x;
    }

/////////////////////////////////////////////////////////////////////

    /**
     * Get Y array.
     */
    public double[] getY() {

        return y;
    }

/////////////////////////////////////////////////////////////////////

    /**
     * Get smark array.
     */
    public String[] getSmark() {

        return smark;
    }

/////////////////////////////////////////////////////////////////////

    /**
     * Get jsyn array.
     */
    public String[] getJsyn() {

        return jsyn;
    }

/////////////////////////////////////////////////////////////////////

    /**
     * Get comp array.
     */
    public int[] getComp() {

        return comp;
    }

/////////////////////////////////////////////////////////////////////

    /**
     * Get ntfile.
     */
    public String getNtfile() {

        return ntfile;
    }

/////////////////////////////////////////////////////////////////////

    /**
     * is it a freq (not int) Pred_mix file
     */
    public boolean isFreqFile() {

        return freq;
    }

/////////////////////////////////////////////////////////////////////

    /**
     * is it a xpafit (not eq_*.f) Pred_mix file
     */
    public boolean isXfile() {

        return xfile;
    }

/////////////////////////////////////////////////////////////////////

    /**
     * Get nb of valid transitions (xpafit)
     */
    public int getXnbtrans() {

        return nbtrans;
    }

/////////////////////////////////////////////////////////////////////

    /**
     * Get name of valid transitions (xpafit)
     */
    public String[] getXntrans() {

        return ntrans;
    }

/////////////////////////////////////////////////////////////////////

    /**
     * Get sublevel of a given valid transition (eq_*)
     */
    public int getNsv() {

        return nsv;
    }

/////////////////////////////////////////////////////////////////////

    /**
     * Get sublevel of a given valid transition (xpafit)
     */
    public int getXnsv( String cntrans ) {

        int i;
        for( i=0; i<nbtrans; i++ ) {
            if( ntrans[i].equals(cntrans) ) {
                break;
            }
        }
        return nsvtrans[i];
    }

/////////////////////////////////////////////////////////////////////

    /**
     * Set plot type to Jinf
     */
    public void setPlotJinf() {

        resetPlot();
        plotjinf = true;
        return;
    }

/////////////////////////////////////////////////////////////////////

    /**
     * Set plot type to Jsup
     */
    public void setPlotJsup() {

        resetPlot();
        plotjsup = true;
        return;
    }

/////////////////////////////////////////////////////////////////////

    /**
     * Set plot type to Freq
     */
    public void setPlotFreq() {

        resetPlot();
        plotfreq = true;
        return;
    }

/////////////////////////////////////////////////////////////////////

    /**
     * Set plot type to Int
     */
    public void setPlotInt() {

        resetPlot();
        plotint = true;
        return;
    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // check if xpafit or eq_*.f prediction_mix.t file
    private boolean checkFileX() {

        // read 1st line
        try{
            File f = new File(name);
            br = new BufferedReader (new FileReader(f));             // open
            str = readLine(1);                                       // read 1st line
            if( str == null) {
                JOptionPane.showMessageDialog(null,"Empty file"+lnsep+
                                                   name);
                return false;
            }
            if( str.length() < 1 ) {
                JOptionPane.showMessageDialog(null,"Line length error while reading file"+lnsep+
                                                   name);
                return false;
            }
            if( str.substring(0,1).equals("P") ) {
              xfile = true;                                          //xpafit predmix file
            }
        }
        catch(IOException ioe) {                                     // IO error
            JOptionPane.showMessageDialog(null,"IO error while reading file"+lnsep+
                                               name                         +lnsep+
                                               ioe);
            return false;
        }
        finally {                                                    // close
            if (br !=null) {
                try {
                    br.close();
                }
                catch (IOException ignored) {
                }
            }
        }
        return true;
    }

/////////////////////////////////////////////////////////////////////

    // read header of a XW* file (xpafit.f)
    private boolean locReadHeaderX() {

        ArrayList alnpol   = new ArrayList();                        // name            of polyads
        ArrayList alnsvpol = new ArrayList();                        // nb of sublevels of polyads
        String    cntrans;
        ArrayList alntrans = new ArrayList();                        // name of transitions

        nbxhl = 0;                                                   // nb of header lines of xfile file

        // read heading
        try{
            File f = new File(name);
            br  = new BufferedReader (new FileReader(f));            // open
            str = readLine(1);                                       // read 1st line
            while( str.length() != 0 ) {                             // polyad loop
                alnpol.add(str.substring(0,2));                      // polyad name
                str = readLine(2);
                if( str == null ) {                                  // erreur
                    JOptionPane.showMessageDialog(null,"Unexpected EOF while reading file"+lnsep+
                                                       name);
                    return false;
                }
                try {
                    nsv = Integer.parseInt(str.substring(0,4).trim());  // NSV
                }
                catch (NumberFormatException e) {                    // format error
                    JOptionPane.showMessageDialog(null,"NSV format error in file"+lnsep+
                                                       name                      +lnsep+
                                                       str                       +lnsep+
                                                       e);
                    return false;
                }
                alnsvpol.add(nsv);                                   // nb of sublevels of polyad
                // go to the right line
                str = readLine(nsv+4);
                if( str == null ) {                                  // erreur
                    JOptionPane.showMessageDialog(null,"Unexpected EOF while reading file"+lnsep+
                                                       name);
                    return false;
                }
                nbxhl = nbxhl+nsv+6;                                 // nb of header lines of xfile file
            }
            // manage polyads and their sublevels
            nbpol  = alnpol.size();                                  // nb of polyads
            npol   = new String[nbpol];                              // name of polyads
            nsvpol = new int[nbpol];                                 // nb of sublevels of polyads
            for( int i=0; i<nbpol; i++ ) {                           // copy data
                npol[i]   = (String) alnpol.get(i);                  // name of polyad
                nsvpol[i] = ((Integer) alnsvpol.get(i)).intValue();  // nb of sublevels of polyad
            }
            // check 1st data line to set file type (freq or int)
            while( true ) {                                          // for each transition
                // go to the right line
                str = readLine(3);
                if( str == null ) {                                  // EOF
                    break;
                }
                // NBSTR/NBFTR
                try {
                    nbsftr = Integer.parseInt(str.substring(0,8).trim());                          // NBSTR/NBFTR
                }
                catch (NumberFormatException e) {                                                  // format error
                    JOptionPane.showMessageDialog(null,"NBSTR/NBFTR format error in file"+lnsep+
                                                            name                         +lnsep+
                                                            str                          +lnsep+
                                                            e);
                    return false;
                }
                str = readLine(2);
                if( str == null ) {                                                                // EOF
                    break;
                }
                if( nbsftr == 0 ) {
                    continue;
                }
                // LPCMAX
                try {
                    lpcmax = Integer.parseInt(str.substring(0,4).trim());  // LPCMAX
                }
                catch (NumberFormatException e) {                        // format error
                    JOptionPane.showMessageDialog(null,"LPCMAX format error in file"+lnsep+
                                                       name                         +lnsep+
                                                       str                          +lnsep+
                                                       e);
                    return false;
                }
                nblnsv = (int) ((lpcmax+19.)/20.);                       // nb of lines of sublevels
                str = readLine(2);
                if( str.length() < 37 ) {
                    JOptionPane.showMessageDialog(null,"Invalid 1st data or NBOTR line length in file"+lnsep+
                                                       name);
                    return false;
                }
                if( str.substring(14,37).equals(" TRANSITION OPERATOR(S)") ) {
                    // intensity Pred_mix file
                    freq = false;
                }
                break;
            }
        }
        catch(IOException ioe) {                                     // IO error
            JOptionPane.showMessageDialog(null,"IO error while reading file"+lnsep+
                                               name                         +lnsep+
                                               ioe);
            return false;
        }
        finally {                                                    // close
            if (br !=null) {
                try {
                    br.close();
                }
                catch (IOException ignored) {
                }
            }
        }
        // reopen the file to set transitions and nb of sublevels
        try{
            File f = new File(name);
            br = new BufferedReader (new FileReader(f));             // open
            // go to the right line                                  // header
            str = readLine(nbxhl+1);
            // continue reading

            eof = false;
            while( true ) {                                          // for each transition
                str = readLine(1);
                if( str == null ) {                                  // EOF
                    break;
                }
                cntrans = str.substring(0,5);
                // go to the right line
                str = readLine(1);
                if( str == null ) {                                  // EOF
                    break;
                }
                str = readLine(1);
                if( str == null ) {                                  // EOF
                    break;
                }
                // NBSTR/NBFTR
                try {
                    nbsftr = Integer.parseInt(str.substring(0,8).trim());                          // NBSTR/NBFTR
                }
                catch (NumberFormatException e) {                                                  // format error
                    JOptionPane.showMessageDialog(null,"NBSTR/NBFTR format error in file"+lnsep+
                                                            name                         +lnsep+
                                                            str                          +lnsep+
                                                            e);
                    return false;
                }
                str = readLine(2);
                if( str == null ) {                                                                // EOF
                    break;
                }
                if( nbsftr == 0 ) {
                    continue;
                }
                alntrans.add(cntrans);
                // LPCMAX
                try {
                    lpcmax = Integer.parseInt(str.substring(0,4).trim());  // LPCMAX
                }
                catch (NumberFormatException e) {                        // format error
                    JOptionPane.showMessageDialog(null,"LPCMAX format error in file"+lnsep+
                                                       name                         +lnsep+
                                                       str                          +lnsep+
                                                       e);
                    return false;
                }
                nblnsv = (int) ((lpcmax+19.)/20.);                       // nb of lines of sublevels
                if( freq ) {
                    str = readLine(1);
                }
                else {
                    str = readLine(4);
                }
                // DATA
                while(true ) {                                       // for each data
                    str = readLine(1);
                    if( str == null ) {                              // EOF
                        eof = true;
                        break;
                    }
                    if( str.length() == 0 ) {                        // new transition
                        break;
                    }
                    // check line length
                    if( (freq  && str.length() != 73) ||
                        (!freq && str.length() != 92)    ) {
                        JOptionPane.showMessageDialog(null,"Invalid data line length in file"+lnsep+
                                                           name);
                        return false;
                    }
                    // read sub-levels
                    for( int i=0; i<nblnsv; i++ ) {                  // format 2006
                        if( (str = readLine(1)) == null ) {          // read one line
                            JOptionPane.showMessageDialog(null,"Unexpected EOF while reading file"+lnsep+
                                                               name);
                            return false;
                        }
                    }
                }
                if( eof ) {                                          // EOF occured
                    break;
                }
            }
        }
        catch(IOException ioe) {                                     // IO error
            JOptionPane.showMessageDialog(null,"IO error while reading file"+lnsep+
                                               name                         +lnsep+
                                               ioe);
            return false;
        }
        finally {                                                    // close
            if (br !=null) {
                try {
                    br.close();
                }
                catch (IOException ignored) {
                }
            }
        }
        // set transitions and nb of sublevels
        nbtrans  = alntrans.size();                                  // nb of transitions
        ntrans   = new String[nbtrans];                              // names           of valid transitions
        nsvtrans = new int[nbtrans];                                 // nb of sublevels of valid transitions
        for( int i=0; i<nbtrans; i++ ) {
            ntrans[i] = (String) alntrans.get(i);
            for( int j=0; j<nbpol; j++ ) {
                if( ntrans[i].substring(0,2).equals(npol[j]) ) {
                    nsvtrans[i] = nsvpol[j];
                    break;
                }
            }
        }
        return true;
    }

/////////////////////////////////////////////////////////////////////

    // read header of an EQ_* file
    private boolean locReadHeader() {

        try{
            File f = new File(name);
            br = new BufferedReader (new FileReader(f));             // open
            str = readLine(1);                                       // read 1st line
            if( str == null) {
                JOptionPane.showMessageDialog(null,"Empty file"+lnsep+
                                                   name);
                return false;
            }
            try {
                nsv = Integer.parseInt(str.substring(0,4).trim());   // NSVSUP
            }
            catch (NumberFormatException e) {                        // format error
                JOptionPane.showMessageDialog(null,"NSVSUP format error in file"+lnsep+
                                                   name                         +lnsep+
                                                   str                          +lnsep+
                                                   e);
                return false;
            }
            int i = 0;
            // go to the right line
            while ((str = readLine(1)) != null) {                    // as long as there is something to read
                i++;
                if(i == nsv ) {
                    break;
                }
            }
            str = readLine(1);                                       // read next line
            if( str == null) {
                JOptionPane.showMessageDialog(null,"Unexpected EOF while reading LPCMAX in file"+lnsep+
                                                   name);
                return false;
            }
            try {
                lpcmax = Integer.parseInt(str.substring(0,4).trim());  // LPCMAX
            }
            catch (NumberFormatException e) {                        // format error
                JOptionPane.showMessageDialog(null,"LPCMAX format error in file"+lnsep+
                                                   name                         +lnsep+
                                                   str                          +lnsep+
                                                   e);
                return false;
            }
            nblnsv = (int) ((lpcmax+19.)/20.);                       // nb of lines of sublevels
            // check 1st data line to set file type (freq or int)
            str = readLine(1);                                       // read next line
            if( str == null) {
                JOptionPane.showMessageDialog(null,"Unexpected EOF while reading 1st data line in file"+lnsep+
                                                   name);
                return false;
            }
            str = readLine(1);                                       // read next line
            if( str == null) {
                JOptionPane.showMessageDialog(null,"Unexpected EOF while reading 1st data line in file"+lnsep+
                                                   name);
                return false;
            }
            if( str.length() < 68 ) {
                JOptionPane.showMessageDialog(null,"Invalid 1st data line length in file"+lnsep+
                                                   name);
                return false;
            }
            if( str.substring(67,69).equals("% ") ) {
                // intensity Pred_mix file
                freq = false;
            }
        }
        catch(IOException ioe) {                                     // IO error
            JOptionPane.showMessageDialog(null,"IO error while reading file"+lnsep+
                                               name                         +lnsep+
                                               ioe);
            return false;
        }
        finally {                                                    // close
            if (br !=null) {
                try {
                    br.close();
                }
                catch (IOException ignored) {
                }
            }
        }
        return true;
    }

/////////////////////////////////////////////////////////////////////

    // read data of a XW* file (xpafit.f)
    private boolean readDataX( String cntrans ) {

        String ntrans;                                               // transition name
        try{
            File f = new File(name);
            br = new BufferedReader (new FileReader(f));             // open
            // go to the right line                                  // header
            str = readLine(nbxhl+1);
            // continue reading
            double diff;
            double xisfi;
            String csmark;
            String cjsyn;
            int    dsnv;
            int    ind1 = 0;                                         // string index for xisfi
            int    ind2 = 0;                                         // string index for xisfi

            // set index for each plot type
            if( freq ) {
                if     ( plotjinf ) {
                    ind1 = 52;
                    ind2 = 55;
                }
                else if( plotjsup ) {
                    ind1 = 63;
                    ind2 = 66;
                }
                else if( plotfreq ) {
                    ind1 =  2;
                    ind2 = 13;
                }
                else if( plotint  ) {
                    ind1 = 35;
                    ind2 = 48;
                }
            }
            else {
                if     ( plotjinf ) {
                    ind1 = 71;
                    ind2 = 74;
                }
                else if( plotjsup ) {
                    ind1 = 82;
                    ind2 = 85;
                }
                else if( plotfreq ) {
                    ind1 =  2;
                    ind2 = 13;
                }
                else if( plotint  ) {
                    ind1 = 35;
                    ind2 = 48;
                }
            }

            eof = false;
            while( true ) {                                          // for each transition
                // go to the right line
                str = readLine(1);
                if( str == null ) {                                  // EOF
                    break;
                }
                ntrans = str.substring(0,5);                         // transition name
                str    = readLine(2);
                if( str == null ) {                                  // EOF
                    break;
                }
                // NBSTR/NBFTR
                try {
                    nbsftr = Integer.parseInt(str.substring(0,8).trim());                          // NBSTR/NBFTR
                }
                catch (NumberFormatException e) {                                                  // format error
                    JOptionPane.showMessageDialog(null,"NBSTR/NBFTR format error in file"+lnsep+
                                                            name                         +lnsep+
                                                            str                          +lnsep+
                                                            e);
                    return false;
                }
                str = readLine(2);
                if( str == null ) {                                                                // EOF
                    break;
                }
                if( nbsftr == 0 ) {
                    continue;
                }
                // LPCMAX
                try {
                    lpcmax = Integer.parseInt(str.substring(0,4).trim());  // LPCMAX
                }
                catch (NumberFormatException e) {                        // format error
                    JOptionPane.showMessageDialog(null,"LPCMAX format error in file"+lnsep+
                                                       name                         +lnsep+
                                                       str                          +lnsep+
                                                       e);
                    return false;
                }
                nblnsv = (int) ((lpcmax+19.)/20.);                       // nb of lines of sublevels
                if( freq ) {
                    str = readLine(1);
                }
                else {
                    str = readLine(4);
                }
                // DATA
                while(true ) {                                       // for each data
                    str = readLine(1);
                    if( str == null ) {                              // EOF
                        eof = true;
                        break;
                    }
                    if( str.length() == 0 ) {                        // new transition
                        break;
                    }
                    // check line length
                    if( (freq  && str.length() != 73) ||
                        (!freq && str.length() != 92)    ) {
                        JOptionPane.showMessageDialog(null,"Invalid data line length in file"+lnsep+
                                                           name);
                        return false;
                    }

                    try {
                        if( freq ) {
                            diff  = Double.parseDouble(str.substring(24,35).trim());      // format 2005 DIFF
                            xisfi = Double.parseDouble(str.substring(ind1,ind2).trim());  // format 2005 Jinf, Jsup, frequency or intensity
                            cjsyn = str.substring(52,73);                                 // format 2005 assignment
                        }
                        else {
                            diff  = Double.parseDouble(str.substring(61,67).trim());      // format 2005 DIFS       format 2005
                            xisfi = Double.parseDouble(str.substring(ind1,ind2).trim());  // format 2005 J          format 2005
                            cjsyn = str.substring(71,92);                                 // format 2005 assignment format 2005
                        }
                    }
                    catch (NumberFormatException e) {                // format error
                        JOptionPane.showMessageDialog(null,"DIFF or X format error in file"+lnsep+
                                                           name                            +lnsep+
                                                           str                             +lnsep+
                                                           e);
                        return false;
                    }
                    csmark = str.substring(1,2);
                    // read sub-levels
                    for( int i=0; i<nblnsv; i++ ) {                  // format 2006
                        if( (str = readLine(1)) == null ) {          // read one line
                            JOptionPane.showMessageDialog(null,"Unexpected EOF while reading file"+lnsep+
                                                               name);
                            return false;
                        }
                        if( i == 0 ) {                                   // get dominating sublevel
                            try {
                                dsnv = Integer.parseInt(str.substring(10,14).trim());
                            }
                            catch(NumberFormatException e) {
                                JOptionPane.showMessageDialog(null,"Dominating sublevel format error in file"+lnsep+
                                                                   name                                      +lnsep+
                                                                   str                                       +lnsep+
                                                                   e);
                                return false;
                            }
                            for( int j=0; j<nbcomp; j++ ) {
                                if( dsnv <= limsup[j] &&
                                    dsnv >= liminf[j]    ) {
                                    if( ntrans.equals(cntrans) ) {
                                        aldiff.add(new Double(diff));
                                        alj.add(new Double(xisfi));
                                        alsmark.add(csmark);
                                        aljsyn.add(cjsyn);
                                        alcomp.add(j);
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
                if( eof ) {                                          // EOF occured
                    break;
                }
            }
        }
        catch(IOException ioe) {                                     // IO error
            JOptionPane.showMessageDialog(null,"IO error while reading file"+lnsep+
                                               name                         +lnsep+
                                               ioe);
            return false;
        }
        finally {                                                    // close
            if (br !=null) {
                try {
                    br.close();
                }
                catch (IOException ignored) {
                }
            }
        }
        return true;
    }

/////////////////////////////////////////////////////////////////////

    // read data of an EQ_* file
    private boolean readData() {

        try{
            File f = new File(name);
            br = new BufferedReader (new FileReader(f));             // open
            // go to the right line
            int i = 0;
            while ((str = readLine(1)) != null) {                    // as long as there is something to read
                i++;
                if(i == nsv+3 ) {
                    break;
                }
            }
            // continue reading
            double diff;
            double xisfi;
            String csmark;
            String cjsyn;
            int    dsnv;
            int    ind1 = 0;                                         // string index for xisfi
            int    ind2 = 0;                                         // string index for xisfi

            // set index for each plot type
            if( freq ) {
                if     ( plotjinf ) {
                    ind1 = 52;
                    ind2 = 55;
                }
                else if( plotjsup ) {
                    ind1 = 63;
                    ind2 = 66;
                }
                else if( plotfreq ) {
                    ind1 =  2;
                    ind2 = 13;
                }
                else if( plotint  ) {
                    ind1 = 35;
                    ind2 = 48;
                }
            }
            else {
                if     ( plotjinf ) {
                    ind1 = 71;
                    ind2 = 74;
                }
                else if( plotjsup ) {
                    ind1 = 82;
                    ind2 = 85;
                }
                else if( plotfreq ) {
                    ind1 =  2;
                    ind2 = 13;
                }
                else if( plotint  ) {
                    ind1 = 35;
                    ind2 = 48;
                }
            }

            while ((str = readLine(1)) != null) {                    // as long as there is something to read
                // check line length
                if( (freq  && str.length() != 73) ||
                    (!freq && str.length() != 92)    ) {
                    JOptionPane.showMessageDialog(null,"Invalid data line length in file"+lnsep+
                                                       name);
                    return false;
                }

                try {
                    if( freq ) {
                        diff  = Double.parseDouble(str.substring(24,35).trim());      // format 2005 DIFF
                        xisfi = Double.parseDouble(str.substring(ind1,ind2).trim());  // format 2005 Jinf, Jsup, frequency or intensity
                        cjsyn = str.substring(52,73);                                 // format 2005 assignment
                    }
                    else {
                        diff  = Double.parseDouble(str.substring(61,67).trim());      // format 2005 DIFS
                        xisfi = Double.parseDouble(str.substring(ind1,ind2).trim());  // format 2005 Jinf, Jsup, frequency or intensity
                        cjsyn = str.substring(71,92);                                 // format 2005 assignment
                    }
                }
                catch (NumberFormatException e) {                    // format error
                    JOptionPane.showMessageDialog(null,"DIFF or X format error in file"+lnsep+
                                                       name                            +lnsep+
                                                       str                             +lnsep+
                                                       e);
                    return false;
                }
                csmark = str.substring(1,2);
                // read sub-levels
                for( i=0; i<nblnsv; i++ ) {                          // format 2006
                    if( (str = readLine(1)) == null ) {              // read one line
                        JOptionPane.showMessageDialog(null,"Unexpected EOF while reading file"+lnsep+
                                                           name);
                        return false;
                    }
                    if( i == 0 ) {                                   // get dominating sublevel
                        try {
                            dsnv = Integer.parseInt(str.substring(10,14).trim());
                        }
                        catch(NumberFormatException e) {
                            JOptionPane.showMessageDialog(null,"Dominating sublevel format error in file"+lnsep+
                                                               name                                      +lnsep+
                                                               str                                       +lnsep+
                                                               e);
                            return false;
                        }
                        for( int j=0; j<nbcomp; j++ ) {
                            if( dsnv <= limsup[j] &&
                                dsnv >= liminf[j]    ) {
                                aldiff.add(new Double(diff));
                                alj.add(new Double(xisfi));
                                alsmark.add(csmark);
                                aljsyn.add(cjsyn);
                                alcomp.add(j);
                                break;
                            }
                        }
                    }
                }
            }
        }
        catch(IOException ioe) {                                     // IO error
            JOptionPane.showMessageDialog(null,"IO error while reading file"+lnsep+
                                               name                         +lnsep+
                                               ioe);
            return false;
        }
        finally {                                                    // close
            if (br !=null) {
                try {
                    br.close();
                }
                catch (IOException ignored) {
                }
            }
        }
        return true;
    }

/////////////////////////////////////////////////////////////////////

    /*
     * Read lines
     */
    private String readLine( int cnbl ) throws IOException {

        String cstr = "";
        for( int nbl=0; nbl<cnbl; nbl++ ) {
            cstr = br.readLine();
        }
        return cstr;
    }

/////////////////////////////////////////////////////////////////////

    /*
     * reset plot types
     */

    private void resetPlot() {

        plotjinf = false;
        plotjsup = false;
        plotfreq = false;
        plotint  = false;
        return;
    }

/////////////////////////////////////////////////////////////////////

    /*
     * is set plot type?
     */

    private boolean isSetPlot() {

        if( plotjinf ||
            plotjsup ||
            plotfreq ||
            plotint     ) {
            return true;
        }
        return false;
    }

}
