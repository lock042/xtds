/*
 * Class for jener.t file (and starkl.t)
 */

import java.io.*;
import java.util.*;
import javax.swing.*;

/**
 * This class defines jener.t data (and starkl.t).
 */
class JenerTFile {

    private String   name;                                           // file name
    private double[] x;                                              // X
    private double[] y;                                              // Y

    private int nbxy;                                                // nb of points

    private String str;                                              // to read file
    private BufferedReader br;
    private String lnsep;                                            // line separator

////////////////////////////////////////////////////////////////////

    /**
     * Construct a new JenerTFile.
     *
     * @param cname  name of the file
     */
    public JenerTFile(String cname) {

        name = cname;                                                // file name

        lnsep = System.getProperty("line.separator");
        nbxy = 0;                                                    // nb of points
    }

////////////////////////////////////////////////////////////////////

    /**
     * Read file.
     * <br> data are read following FORMAT : 1201 of jener.f (and starkl.f)
     */
    public boolean read() {

        double B0 = 0;
        double D0 = 0;
        Double cx;                                                   // to keep X
        Double cy;                                                   // to keep Y
        // to keep points, unknown nb of points
        ArrayList alx    = new ArrayList();                          // J
        ArrayList aly    = new ArrayList();                          // energy
        //
        try{
            br = new BufferedReader( new FileReader( new File( name ) ));  // open
            // get B0 and D0 (BB0,DD0 in C3v*TDS) for energy correction
            int i = 0;                                               // current line #
            while ((str = br.readLine()) != null) {                  // as long as there is something to read
                i++;
                if(i != 4 ) {
                    continue;                                        // go to the right line
                }
                StringTokenizer st = new StringTokenizer(str);       // separate strings
                int nb2 = st.countTokens();
                if( nb2 < 3 ) {                                      // not enough tokens
                    JOptionPane.showMessageDialog(null,"Bad VIBNU, B0, D0 line in file"+lnsep+
                                                        name                           +lnsep+
                                                        str);
                    return false;
                }
                for( int j=0; j<nb2-2; j++ ) {
                    st.nextToken();
                }
                try {
                    B0 = Double.parseDouble(st.nextToken());
                    D0 = Double.parseDouble(st.nextToken());
                }
                catch (NumberFormatException e) {                    // format error
                    JOptionPane.showMessageDialog(null,"B0 or D0 format error in file"+lnsep+
                                                       name                           +lnsep+
                                                       str                            +lnsep+
                                                       e);
                    return false;
                }
                break;
            }
            // read data
            while ((str = br.readLine()) != null) {                  // line read
                str = str+"                            ";            // pour etendre a 28 caras mini
                try {
                    // extraction
                    cy = Double.valueOf(str.substring(0,15));        // Double for Y
                    if( str.substring(19,20).equals(".") ) {
                        // C3vsTDS
                        cx = Double.valueOf(str.substring(16,21));   // Double for X
                    }
                    else {
                        // other TDS
                        cx = Double.valueOf(str.substring(16,19));   // Double for X
                    }
                }
                catch (NumberFormatException e) {                    // format error
                    continue;                                        // skip the line
                }
                alx.add(cx);                                         // keep data
                aly.add(cy);
            }
        }
        catch(IOException ioe) {                                     // IO error
            JOptionPane.showMessageDialog(null,"IO error while reading file"+lnsep+
                                               name                         +lnsep+
                                               ioe);
            return false;
        }
        finally {
            // close
            if (br != null) {
                try {
                    br.close();
                }
                catch (IOException ignored) {
                }
            }
        }
        // save data
        nbxy = alx.size();                                           // nb of points
        if( nbxy == 0 ) {
            // no point
            JOptionPane.showMessageDialog(null,"No valid data found in file"+lnsep+
                                               name);
            return false;
        }
        x    = new double[nbxy];
        y    = new double[nbxy];
        // sort
        XYPoint[] xypt = new XYPoint[nbxy];                          // compare X
        for(int i=0; i<nbxy; i++) {
            xypt[i] = new XYPoint( ((Double) alx.get(i)).doubleValue(),
                                   ((Double) aly.get(i)).doubleValue() );
        }
        Arrays.sort(xypt);                                           // sort
        // save data
        double J;
        double corr;                                                 // energy correction
        for(int i=0; i<nbxy; i++) {
            x[i] = xypt[i].getX();                                   // J
            J    = x[i];
            corr = B0*J*(J+1)-D0*J*J*(J+1)*(J+1);
            y[i] = xypt[i].getY()-corr;
        }

        return true;
    }

    /**
     * Get number of data points.
     */
    public int getNbxy() {

        return nbxy;
    }

    /**
     * Get X array.
     */
    public double[] getX() {

        return x;
    }

    /**
     * Get Y array.
     */
    public double[] getY() {

        return y;
    }

}
