/*
 * Class to use xTDS packages
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

/////////////////////////////////////////////////////////////////////

/**
 * Filter to get clean package names.
 */
class TDSFilter implements FilenameFilter {

    /**
     * Accepts (or reject) as a package name.
     *
     * @param dir    unused
     * @param name   file name to test
     */
    public boolean accept(File dir, String name) {
         if( name.startsWith(".") ) {                                // package names do not begin with dot
             return false;
         }
         return (name.endsWith("TDS"));                              // package names end with TDS
    }
}

/////////////////////////////////////////////////////////////////////

/**
 * This is the master window.
 */
public class JobPlay extends    JFrame
                     implements ActionListener {

    private String playd;                                            // XTDS installation directory
    private String[] packdirs;                                       // package directories
    private JTabbedPane tabbedPane;                                  // tabbed pane

    private JMenuItem jmiexit;                                       // file menu exit
    private JMenuItem jmicurpage;                                    // help menu curpage
    private JMenuItem jmiabout;                                      // help menu about

    private File           helpf;                                    // help file
    private BufferedReader br;
    private String         str;
    private String lnsep;                                            // line separator
    private String fisep;                                            // file separator
    private Font   serif14;                                          // Serif 14 font

/////////////////////////////////////////////////////////////////////

    /**
     * Constructs a new JobPlay.
     */
    public JobPlay() {

        super("Managing  xTDS jobs");                                // main window

        addWindowListener(new WindowAdapter() {                      // clean exit
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });

        playd = System.getProperty("xtds.home");                     // XTDS installation directory
        lnsep = System.getProperty("line.separator");
        fisep = System.getProperty("file.separator");
        serif14 = new Font("Serif", Font.PLAIN, 14);
        // set packages
        File packd = new File(playd+fisep+"packages");               // package directories
        packdirs = packd.list(new TDSFilter());                      // filtered list
        Arrays.sort(packdirs);                                       // sort
        tabbedPane = new JTabbedPane();                              // tabbed pane
        tabbedPane.setBackground(Color.BLUE);
        tabbedPane.setForeground(Color.BLACK);
        // set each tab action
        populateTabbedPane();
        buildMenu();                                                 // define menu
        getContentPane().add(tabbedPane);                            // add tabbed pane to current window
        pack();                                                      // pack
    }

/////////////////////////////////////////////////////////////////////

    //  an action for each tab
    private void populateTabbedPane() {

        tabbedPane.addTab("Welcome",                                 // create tabs
                          null,
                          new Welcome(),
                          "Welcome to XTDS");

        tabbedPane.addTab("Create a job",
                          null,
                          new CreateJob(packdirs),
                         "Click here to create a new job");

        tabbedPane.addTab("Run a job",
                          null,
                          new RunJob(),
                          "Click here to run a xTDS job");

        tabbedPane.addTab("Visualize results",
                          null,
                          new Visualize(),
                          "Click here to visualize job results");

        tabbedPane.addTab("Create a molecule",
                          null,
                          new CreateMol(packdirs),
                          "Click here to create a new molecule");
    }

    //  File and Help menus
    private void buildMenu() {

        // menu bar
        JMenuBar  jmb     = new JMenuBar();
        // File
        JMenu     jmfile  = new JMenu("File");
        jmiexit = new JMenuItem("Exit");
        jmiexit.addActionListener(this);
        jmfile.add(jmiexit);
        // Help
        JMenu     jmhelp  = new JMenu("Help");
        jmicurpage = new JMenuItem("Current Panel");
        jmicurpage.addActionListener(this);
        jmiabout   = new JMenuItem("About XTDS");
        jmiabout.addActionListener(this);
        jmhelp.add(jmicurpage);
        jmhelp.add(jmiabout);

        jmb.add(jmfile);
        jmb.add(jmhelp);
        setJMenuBar(jmb);
    }

/////////////////////////////////////////////////////////////////////

    /**
     * Processes the events.
     */
    public void actionPerformed(ActionEvent evt) {

        // File menu -> Exit
        if (evt.getSource() == jmiexit) {
            System.exit(0);
            return;
        }
        // Help menu -> Current Page
        if (evt.getSource() == jmicurpage) {
            if( ((Container)tabbedPane.getSelectedComponent()).getComponent(0).getName() == null ) {
                popFile(((Container)tabbedPane.getSelectedComponent()).getName());
            }
            else {
                popFile(((Container)tabbedPane.getSelectedComponent()).getComponent(0).getName());
            }
            return;
        }
        // Help menu -> About
        if (evt.getSource() == jmiabout) {
            popFile("About");
            return;
        }
    }

    // display file text
    private void popFile(String nhelpf) {

        if( nhelpf == null ) {
            JOptionPane.showMessageDialog(null,"No help provided for this item");
            return;
        }

        JTextArea jtahelp = new JTextArea();
        jtahelp.setFont( serif14 );
        jtahelp.setLineWrap(true);                                   // wrappable lines
        jtahelp.setWrapStyleWord(true);                              // at word boundaries
        jtahelp.setEditable(false);                                  // not editable
        JScrollPane jsp = new JScrollPane(jtahelp, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

        helpf = new File(playd+fisep+"help"+fisep+nhelpf+".hlp");
        boolean fgood = false;
        try {
            br = new BufferedReader (new FileReader(helpf));
            while ((str = br.readLine()) != null) {
                jtahelp.append(str+lnsep);
            }
            fgood = true;
        }
        catch(FileNotFoundException fnfe) {                          // no help file
            JOptionPane.showMessageDialog(null,"No help provided for "+nhelpf);
        }
        catch(IOException ioe) {                                     // IO error
            JOptionPane.showMessageDialog(null,"IO error while reading file"+lnsep+
                                               helpf.getAbsolutePath()      +lnsep+
                                               ioe);
        }
        finally {                                                    // close file
            if (br !=null) {
                try {
                    br.close();
                }
                catch (Exception ignored) {
                }
            }
        }

        if( fgood ) {
            try {
                jtahelp.setCaretPosition(0);
            }
            catch(IllegalArgumentException iae) {
            }
            JFrame frame = new JFrame("Help on "+nhelpf);
            frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            frame.getContentPane().setBackground(Color.WHITE);
            frame.setLocation(getX()+100,getY()+100);
            frame.setSize(600,400);
            frame.getContentPane().add(jsp, BorderLayout.CENTER);
            frame.setVisible(true);
        }
    }

}
