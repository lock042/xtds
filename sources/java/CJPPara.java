/*
 * Class for creation of a parameter file creation job, called by CreateJob
 */

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;

/**
 * This panel creates a blank parameter file thanks to a dedicated job.
 */
public class CJPPara extends    JPanel
                     implements CJPint, ActionListener {

    // panels
    private JPanel pnord;
    private JPanel pcentre;
    private JPanel psud;

    // north panel accessories
    private JPanel       pnn;                                        // north-north panel
    private JPanel       pnnc;
    private JPanel       pnnco;
    private JPanel       pnncc;
    private JPanel       pnc;                                        // north-center panel
    private JPanel       pncn;
    private JPanel       pncc;
    private JPanel       pns;                                        // north-south panel
    private JPanel       pnso;
    private JPanel       pnsc;

    private JComboBox    jcbmol;                                     // molecule jcb
    private String[]     trep;                                       // representation choice array
    private JComboBox    jcbrep;                                     // representation jcb
    private String       nrep;                                       // representation name

    private JButton      jbparaf;                                    // parameter file button
    private String       nparaf;                                     // parameter file name
    private JLabel       lparaf;                                     // parameter file label
    private JComboBox[]  jcbpsc;                                     // polyad scheme coef. jcb
    private String[]     tpsc;                                       // polyad scheme coef. choice array
    private String       npsc;                                       // current psc name
    private String[]     tdvo;                                       // development order choice array

    private JPanel       trm_jpdvo;                                  // transition moment development order panel
    private JComboBox[]  trm_jcbdvo;                                 // transition moment development order jcb
    private String       trm_ndvo;                                   // transition moment development order name
    private String[]     ttmtype;                                    // transition moment type choice array
    private int          nbtmtype;                                   // transition moment type size
    private JComboBox    jcbtmtype;                                  // transition moment type jcb
    private String       ntmtype;                                    // transition moment type name

    // center panel accessories
    private JTextArea   jta;                                         // polyad display text
    private JScrollPane jsp;                                         // with lifts

    // south panel accessories
    private JButton jbreset;                                         // reset
    private JButton jbsave;                                          // save
    private Box     boxsud;                                          // button box

    // CreateJob parameters
    private String   playd;                                          // XTDS installation directory
    private String   workd;                                          // working directory
    private String   npack;                                          // package name
    private String[] moldirs;                                        // molecule directories
    private int      nbvqn;                                          // nb of vibrational quantum numbers
    private int      mxpol;                                          // max nb of polyads
    // parameters catched in this CJPanel
    private String   nmol;                                           // molecule
    private int[]    vpsc;                                           // polyad scheme coef.
    private int[]    trm_vdvo;                                       // development order

    // polyads
    private Polyad   lowpolyad;                                      // lower polyad
    private int      low_polnb;                                      // lower polyad #
    private JPPolyad jpplow;                                         // lower polyad panel
    private Polyad   upppolyad;                                      // upper polyad
    private int      upp_polnb;                                      // upper polyad #
    private JPPolyad jppupp;                                         // upper polyad panel
    private int      dif_polnb;                                      // upp_polnb-low_polnb

    // variables
    private int     nc;                                              // current polyad #

    private String  str;                                             // Pa_skel file reading string
    private BufferedReader br;                                       // Pa_skel file reading buffer
    private String   nskelf;                                         // Pa_skel file name
    private String   nskeld;                                         // Pa_skel directory
    private String   njobf;                                          // job file name
    private PrintWriter out1;                                        // job writing pw
    private boolean evt_basics;                                      // if basics            event
    private boolean evt_trm;                                         // if transition moment event
    private String lnsep;                                            // line separator
    private String fisep;                                            // file separator
    private Font   mono15;                                           // Monospaced 15 font

/////////////////////////////////////////////////////////////////////

    /**
     * Constructs a new CJPPara.
     */
    public CJPPara() {

        playd = System.getProperty("xtds.home");                     // XTDS installation directory
        workd = System.getProperty("user.dir");                      // working directory
        lnsep = System.getProperty("line.separator");
        fisep = System.getProperty("file.separator");
        mono15 = new Font("Monospaced", Font.PLAIN, 15);
        setName("CJPPara");                                          // for help files
        setLayout(new BorderLayout());

        // south panel
        jbreset = new JButton();                                     // managed by CreateJob
        jbreset.setBackground(Color.WHITE);
        jbsave  = new JButton("Save");                               // job file saving button
        jbsave.setBackground(Color.WHITE);
        jbsave.addActionListener (new ActionListener () {
            public void actionPerformed (ActionEvent ae) {           // jbsave action
                saveCJP();
            }
        }
        );

        boxsud = Box.createHorizontalBox();                          // add buttons to box
        boxsud.add(jbreset);
        boxsud.add(Box.createHorizontalStrut(15));
        boxsud.add(jbsave);
        psud = new JPanel();
        psud.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        psud.add(boxsud);                                            // add box

        // center panel polyad display
        pcentre = new JPanel(new BorderLayout());
        jta = new JTextArea();
        jta.setBackground(Color.BLACK);
        jta.setForeground(Color.WHITE);
        jta.setEditable(false);                                      // not editable
        jta.setFont( mono15 );
        jta.setLineWrap(false);                                      // NO line wrap
        jsp = new JScrollPane(jta);                                  // lifts
        pcentre.add(jsp, "Center");

        // display panels
        add(pcentre, "Center");
        add(psud,    "South");
    }

/////////////////////////////////////////////////////////////////////

    /**
     * Returns the reset button.
     * <br>Called by CreateJob which manages it.
     */
    public JButton getJBreset() {

        return jbreset;
    }

    /**
     * Sets basic parameters and creates north panel.
     * <br>Called by CreateJob to activate this panel.
     *
     * @param cnpack     name of the package
     * @param cmoldirs   molecule directories
     * @param cmxpol     MXPOL
     * @param cnbclab    number of characters of parameter label
     * @param cnbvqn     number of vibrational quantum numbers
     */
    public void setBasicPara(String cnpack, String[] cmoldirs, int cmxpol, int cnbclab, int cnbvqn) {

        npack   = cnpack;                                            // package name
        moldirs = cmoldirs;                                          // molecule directories
        mxpol   = cmxpol;                                            // MXPOL
        nbvqn   = cnbvqn;                                            // nb of vibrational quantum numbers

        createNorth();
    }

/////////////////////////////////////////////////////////////////////

    // north panel for polyad characteristics setting and display
    private void createNorth() {

        pnord = new JPanel(new BorderLayout());                      // north panel
        pnn   = new JPanel(new BorderLayout());                      // north-north panel
        pnnc  = new JPanel(new BorderLayout());
        pnnco = new JPanel(new GridLayout(3,0,5,5));
        pnncc = new JPanel(new GridLayout(3,0,5,5));
        pnc   = new JPanel(new BorderLayout());                      // north-center panel
        pncn  = new JPanel(new BorderLayout());
        pncc  = new JPanel(new BorderLayout());
        pns   = new JPanel(new BorderLayout());                      // north-south panel
        pnso  = new JPanel(new GridLayout(2,0,5,5));
        pnsc  = new JPanel(new GridLayout(2,0,5,5));

        // BASICS
        // molecule choice
        pnnco.add(new JLabel(""));
        JPanel jpmr;
        jpmr = new JPanel(new GridLayout(0,4,0,5));
        jpmr.add(new JLabel("Molecule ",null,JLabel.RIGHT));
        jcbmol = new JComboBox();
        jcbmol.addItem("");
        for( int i=0; i<moldirs.length; i++ ) {
            jcbmol.addItem(moldirs[i]);
        }
        jcbmol.setSelectedItem("");                                  // default to space
        jcbmol.setBackground(Color.WHITE);
        nmol = "";
        jcbmol.addActionListener(this);
        jpmr.add(jcbmol);
        if( npack.equals("D2hTDS") ) {
            // representation choice
            jpmr.add(new JLabel("Representation ",null,JLabel.RIGHT));
            trep = new String[4];
            trep[0] = "";                                            // space first
            trep[1] = "Ir";
            trep[2] = "IIr";
            trep[3] = "IIIr";
            jcbrep = new JComboBox(trep);
            jcbrep.setSelectedItem("");                              // default to space
            jcbrep.setBackground(Color.WHITE);
            jpmr.add(jcbrep);
        }
        pnncc.add(jpmr);
        // parameter file choice
        jbparaf = new JButton("New Para. File");
        jbparaf.setBackground(Color.WHITE);
        jbparaf.addActionListener(this);
        nparaf = "";
        pnnco.add(jbparaf);
        lparaf = new JLabel("");                                     // default to space
        lparaf.setOpaque(true);
        lparaf.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        pnncc.add(lparaf);
        // polyad scheme coefficients choice
        pnnco.add(new JLabel("Polyad Scheme "));
        JPanel jppsc = new JPanel(new GridLayout(0,2*nbvqn+1,0,5));  // nbvqn = nb of vibrational quantum numbers
        jppsc.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        jcbpsc = new JComboBox[nbvqn];
        tpsc = new String[mxpol];                                    // polyad scheme coefficient array
        for( int i=0; i<mxpol; i++) {
            tpsc[i] = String.valueOf(i);                             // 0 -> mxpol
        }
        for ( int i=0; i<nbvqn; i++) {                               // choice Label
            if(i == 0) {
                jppsc.add(new JLabel(" [P]n = "));                   // polyad number
            }
            jcbpsc[i] = new JComboBox();
            jcbpsc[i].addItem("");
            for( int j=0; j<tpsc.length; j++ ) {
                jcbpsc[i].addItem(tpsc[j]);
            }
            jcbpsc[i].setSelectedItem("");                           // default to space
            jcbpsc[i].setBackground(Color.WHITE);
            jcbpsc[i].addActionListener(this);
            vpsc = new int[nbvqn];                                   // polyad scheme coefficient values
            jppsc.add(jcbpsc[i]);
            if(i != nbvqn-1) {                                       // set label
                jppsc.add(new JLabel("*v"+(i+1)+" +"));
            }
            else {
                jppsc.add(new JLabel("*v"+(i+1)));
            }
        }
        pnncc.add(jppsc);
        pnnc.add(pnnco,"West");
        pnnc.add(pnncc,"Center");
        pnn.add(pnnc,"Center");
        pnn.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLUE,4),"BASICS"));

        // LOWER POLYAD
        lowpolyad = new Polyad(nbvqn);                               // lower polyad
        lowpolyad.setName("Lower Polyad");                           // its name
        jpplow = new JPPolyad(this, lowpolyad, tpsc, pncn);          // its panel

        // UPPER POLYAD
        upppolyad = new Polyad(nbvqn);                               // upper polyad
        upppolyad.setName("Upper Polyad");                           // its name
        jppupp = new JPPolyad(this, upppolyad, tpsc, pncc);          // its panel

        tdvo = new String[10];                                       // order 0 -> 9
        for(int i=0; i<10; i++) {
            tdvo[i] = String.valueOf(i);
        }
        // TRANSITION MOMENT
        // type choice
        pnso.add(new JLabel("Type"));
        nbtmtype = 2;                                                // dip, pol
        if(npack.equals("STDS")) {                                   // STDS case
            nbtmtype = 3;                                            // dip, pol, str
        }
        ttmtype = new String[nbtmtype+1];
        ttmtype[0] = "";
        ttmtype[1] = "dip";
        ttmtype[2] = "pol";
        if(npack.equals("STDS")) {                                   // STDS case
            ttmtype[3] = "str";
        }
        jcbtmtype = new JComboBox(ttmtype);
        jcbtmtype.setSelectedItem("");                               // default to space
        jcbtmtype.setBackground(Color.WHITE);
        pnsc.add(jcbtmtype);
        // Development Order choice
        pnso.add(new JLabel("Devel. Order "));

        pns.add(pnso,"West");
        pns.add(pnsc,"Center");
        pns.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLUE,4),"TRANSITION MOMENT"));

        pnc.add(pncn,"North");
        pnc.add(pncc,"Center");

        // add panels and sub-panels
        pnord.add(pnn,"North");
        pnord.add(pnc,"Center");
        pnord.add(pns,"South");
        add(pnord, "North");

    }

/////////////////////////////////////////////////////////////////////

    /**
     * Processes the events.
     */
    public void actionPerformed(ActionEvent evt) {

        // BASICS
        evt_basics = false;
        // molecule
        if (evt.getSource() == jcbmol ) {
            evt_basics = true;
            nmol = (String) jcbmol.getSelectedItem();
            // unselect representation and parameter file
            if( npack.equals("D2hTDS") ) {
                jcbrep.setSelectedItem("");
            }
            nparaf = "";
            lparaf.setText(nparaf);
            if( nmol != "" ) {
                nskeld = playd+fisep+"packages"+fisep+npack+fisep+"para"+fisep+nmol;
                nskelf = nskeld+fisep+"Pa_skel";
                if( (! lowpolyad.setInd(npack, nskelf)) ||
                    (! upppolyad.setInd(npack, nskelf))    ) {       // set frequency indexes
                    nmol = "";
                    jcbmol.setSelectedItem("");                      // file error : unselect molecule
                }
            }
        }
        // parameter file
        if (evt.getSource() == jbparaf) {
            evt_basics = true;
            if( nmol == "" ) {
                JOptionPane.showMessageDialog(null,"You have to first define the molecule");
                return;
            }
            JFileChooser jfcparaf = new JFileChooser(workd);         // default choice directory
            jfcparaf.setSize(400,300);
            jfcparaf.setFileSelectionMode(JFileChooser.FILES_ONLY);  // files only
            jfcparaf.setDialogTitle("Define the parameter file to be created");
            Container parent = jbparaf.getParent();
            int choice = jfcparaf.showDialog(parent,"Select");       // Dialog, Select
            if (choice == JFileChooser.APPROVE_OPTION) {
                nparaf= jfcparaf.getSelectedFile().getAbsolutePath();
                lparaf.setText(nparaf);
            }
        }
        // Polyad Scheme Coefficients
        for (int i=0; i<nbvqn; i++) {
            if(evt.getSource() == jcbpsc[i] ) {                      // whatever coef.
                evt_basics = true;
                npsc = (String) jcbpsc[i].getSelectedItem();
                if(npsc != "") {
                    vpsc[i] =  Integer.parseInt(npsc);               // save coef. value
                }
            }
        }
        // end of basics
        if(evt_basics) {
            jpplow.resetJP();
            jppupp.resetJP();
            resetTrmJP();
            return;
        }
    }

    /**
     * Manages JPPolyad event.
     */
    public void JPPEvent( Polyad curpol, int type, int indice ) {

        // first
        if( ! testBasics() ) {                                       // basics checked ?
            askBasics();                                             // no, ask for
            jpplow.resetJP();                                        // JPP low reset
            jppupp.resetJP();                                        // JPP upp reset
            resetTrmJP();
        }
        else {
            // if lower polyad
            if( curpol == lowpolyad ) {
                jppupp.resetJP();                                    // JPP upp reset
                resetTrmJP();
                if( lowpolyad.isSetPolnb() ) {
                    low_polnb = lowpolyad.getPolnb();
                    addTrmJP();
                }
            }
            // if upper polyad
            else {
                // lowpolyad fully defined ?
                if( ! lowpolyad.isFullydef() ) {
                    lowpolyad.askPol();                              // no, ask for
                    jppupp.resetJP();                                // JPP upp reset
                    resetTrmJP();
                    if( lowpolyad.isSetPolnb() ) {
                        addTrmJP();
                    }
                }
                else {
                    // validate upp % low
                    switch( type ) {
                        // polyad #
                        case 0 : {
                            if( upppolyad.isSetPolnb() ) {
                                // upp polnb defined
                                upp_polnb = upppolyad.getPolnb();
                                if( upp_polnb < low_polnb ) {
                                    // restriction
                                    JOptionPane.showMessageDialog(null,"The upper polyad number"           +lnsep+
                                                                       "has to be greater than or equal to"+lnsep+
                                                                       "the lower polyad number");
                                    jppupp.resetJP();
                                }
                                else {
                                    // qal restriction
                                    int[] depqal = new int[nbvqn];
                                    for( int i=0; i<nbvqn; i++ ) {
                                        depqal[i] = Math.max(lowpolyad.getVqal(i),upp_polnb);
                                    }
                                    // dvo restriction
                                    int nbfixdvo = low_polnb+1;
                                    int[] fixdvo = new int[nbfixdvo];
                                    for( int i=0; i<nbfixdvo; i++ ) {
                                        fixdvo[i] = lowpolyad.getVdvo(i);
                                    }
                                    jppupp.setFix(depqal, fixdvo);
                                }
                            }
                            break;
                        }
                        // quanta limit
                        case 1 : {
                            int upp_vqal = upppolyad.getVqal(indice);
                            int low_vqal = lowpolyad.getVqal(indice);
                            if( upp_vqal < low_vqal ) {
                                JOptionPane.showMessageDialog(null,"Each upper quanta limit"           +lnsep+
                                                                   "has to be greater than or equal to"+lnsep+
                                                                   "its respective lower quanta limit");
                                jppupp.setJcbqal(Math.max(low_vqal,upp_polnb), indice);
                            }
                            break;
                        }
                    }
                }
            }
        }
        showPol();
    }

/////////////////////////////////////////////////////////////////////

    // display polyads
    private void showPol() {

        jta.setText("");
        if( lowpolyad.isFullydef() ) {                               // lower polyad defined
            lowpolyad.calCvs(vpsc);                                  // cvs calculation
            StringBuffer sb = new StringBuffer();
            lowpolyad.toSb(sb);                                      // display lower polyad
            jta.append(sb.toString());
            if( upppolyad.isFullydef() ) {                           // upper polyad defined
                upppolyad.calCvs(vpsc);                              // cvs calculation
                sb.setLength(0);
                upppolyad.toSb(sb);                                  // display upper polyad
                jta.append(sb.toString());
            }
        }
    }

    // add transition moment JPanel
    private void addTrmJP() {

        trm_jpdvo = new JPanel(new GridLayout(0,2*(low_polnb+1),0,5));
        trm_jpdvo.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        trm_vdvo = new int[low_polnb+1];                             // define value array
        trm_jcbdvo = new JComboBox[low_polnb+1];
        for ( int i=0; i<low_polnb+1; i++) {
            if( i == 0) {
                trm_jpdvo.add(new JLabel(" D ",null,JLabel.RIGHT));
            }
            else {
                trm_jpdvo.add(new JLabel(""));
            }
            trm_jcbdvo[i] = new JComboBox();
            trm_jcbdvo[i].addItem("");
            for( int j=0; j<tdvo.length; j++ ) {
                trm_jcbdvo[i].addItem(tdvo[j]);
            }
            trm_jcbdvo[i].setSelectedItem("");
            trm_jcbdvo[i].setBackground(Color.WHITE);
            trm_jpdvo.add(trm_jcbdvo[i]);
        }
        pnsc.add(trm_jpdvo);                                         // add panel
        trm_jpdvo.revalidate();                                      // re-display it if a suppression occured
    }

    // remove transition moment JPanel
    private void removeTrmJP() {

        if(trm_jpdvo != null) {
            pnsc.remove(trm_jpdvo);
            pnsc.repaint();
        }
    }

    // reset transition moment JPanel
    private void resetTrmJP() {
        jcbtmtype.setSelectedItem("");
        removeTrmJP();
    }

/////////////////////////////////////////////////////////////////////

    // save job
    private void saveCJP() {

        // check if everything is defined
        // BASICS
        if( ! testBasics()) {
            askBasics();
            return;
        }
        // LOWER POLYAD
        if( ! lowpolyad.isFullydef() ) {
            lowpolyad.askPol();
            return;
        }
        // check if there is at least one vs per sub-polyad
        for(int i=0; i<=low_polnb; i++) {
            if( lowpolyad.getNbvs(i) == 0 ) {
                JOptionPane.showMessageDialog(null,"WARNING : your choice produces NO vibrational state for P"+i+" of "+lowpolyad.getName());
            }
        }
        // UPPER and TRM (yes/no)
        if( fuzzyUppPolTrm() ) {
            JOptionPane.showMessageDialog(null,"Upper polyad and/or transition moment characteristics are partialy defined."+lnsep+
                                               "Fully clear them or fully define them.");
            return;
        }

        // everything is well defined
        // is there an upper polyad ?
        if( ! upppolyad.isFullydef() ) {
            int n = JOptionPane.showConfirmDialog(null,"Do you really want to create"    +lnsep+
                                                       "Hamiltonian parameter only file ?",
                                                       "Type of parameter file",JOptionPane.YES_NO_OPTION);
            if(n != JOptionPane.YES_OPTION) {
                return;
            }
        }
        else {
            // there is an upper polyad
            dif_polnb = upp_polnb-low_polnb;
            // if lower different from upper
            if( dif_polnb != 0 ) {
                // check if there is at least one vs per sub-polyad
                for(int i=0; i<=upp_polnb; i++) {
                    if(upppolyad.getNbvs(i) == 0 ) {
                        JOptionPane.showMessageDialog(null,"WARNING : your choice produces NO vibrational state for P"+i+" of "+upppolyad.getName());
                    }
                }
                // test if lower and upper are consistent
                for(int i=1; i<low_polnb+1; i++) {
                    if( lowpolyad.getNbvs(i) != upppolyad.getNbvs(i) ) {
                        JOptionPane.showMessageDialog(null,"Inconsistent number of vibrational states for P"+i);
                        return;
                    }
                    if( ! lowpolyad.getCvs(i).equals(upppolyad.getCvs(i)) ) {
                        JOptionPane.showMessageDialog(null,"Inconsistent values of vibrational states for P"+i);
                        return;
                    }
                }
            }
        }
        // choose job name
        JFileChooser jfcjobf = new JFileChooser(workd);
        jfcjobf.setSize(400,300);
        jfcjobf.setFileSelectionMode(JFileChooser.FILES_ONLY);
        jfcjobf.setDialogTitle("Define the job file to be created");
        jfcjobf.setSelectedFile(new File("job_par_"));
        int choice = jfcjobf.showSaveDialog(this);
        if (choice == JFileChooser.APPROVE_OPTION) {
            njobf = jfcjobf.getSelectedFile().getAbsolutePath();     // job file name
        }
        else {
            return;
        }
        // create job
        try {
            out1 = new PrintWriter(new BufferedWriter(new FileWriter(njobf)));  // write job

            out1.println("#! /bin/sh");
            out1.println(" set -v");
            out1.println("##");
            out1.println("## Parameter creation job created by XTDS");
            out1.println("##");
            out1.println("BASD="+playd+fisep+"packages"+fisep+npack);
            out1.println("#");
            out1.println(" SCRD=$BASD"+fisep+"prog"+fisep+"exe");
            out1.println("##");
            out1.println("#################################################");
            out1.println("##");
            out1.println("## Hamiltonian matrix elements.");
            out1.println("##");
            if( ! upppolyad.isFullydef() ) {
                // hamiltonian parameter ONLY case
                out1.println("## Lower Polyad.");
                out1.println("##");
                out1.print  (" $SCRD"+fisep+"passx hmodel P"+low_polnb);
                lowpolyad.ecrPol(out1, true);
                if( npack.equals("D2hTDS") ) {
                    out1.println(" \\"+lnsep+
                                 "                    "+nrep);
                }
                else {
                    out1.println("");
                }
                out1.println("##");
                out1.println("## Parameter file creation.");
                out1.println("##");
                out1.print  (" $SCRD"+fisep+"passx parmk  P"+low_polnb+" D");
                for(int j=0; j<low_polnb+1; j++) {
                    out1.print  (lowpolyad.getVdvo(j));
                }
                out1.println(" "+nskeld+" "+nparaf);
                out1.println("rm MH*");
                out1.println("##");
            }
            else {
                // hamiltonian AND transition moment parameter case
                if( dif_polnb != 0 ) {
                    // lower and upper polyad differ
                    out1.println("## Lower Polyad.");
                    out1.println("##");
                    out1.print  (" $SCRD"+fisep+"passx hmodel P"+low_polnb);
                    lowpolyad.ecrPol(out1, true);
                    if( npack.equals("D2hTDS") ) {
                        out1.println(" \\"+lnsep+
                                     "                    "+nrep);
                    }
                    else {
                        out1.println("");
                    }
                    out1.println("##");
                }
                out1.println("## Upper Polyad.");
                out1.println("##");
                out1.print  (" $SCRD"+fisep+"passx hmodel P"+upp_polnb);
                upppolyad.ecrPol(out1, true);
                if( npack.equals("D2hTDS") ) {
                    out1.println(" \\"+lnsep+
                                 "                    "+nrep);
                }
                else {
                    out1.println("");
                }
                out1.println("##");
                out1.println("## Transition moment matrix elements.");
                out1.println("##");
                if( ntmtype.equals("pol") ) {
                    out1.print  (" $SCRD"+fisep+"passx polmod P"+upp_polnb);
                }
                else {
                    out1.print  (" $SCRD"+fisep+"passx dipmod P"+upp_polnb);
                }
                upppolyad.ecrPol(out1, false);
                out1.println(" \\");
                out1.print  ("                    P"+low_polnb);
                lowpolyad.ecrPol(out1, false);
                out1.println(" \\");
                out1.print  ("                    D");
                for(int j=0; j<low_polnb+1; j++) {
                    out1.print  (trm_vdvo[j]);
                }
                if( npack.equals("D2hTDS") ) {
                    out1.println(" \\"+lnsep+
                                 "                    "+nrep);
                }
                else {
                    out1.println("");
                }
                out1.println("##");
                out1.println("## Parameter file creation.");
                out1.println("##");
                out1.print  (" $SCRD"+fisep+"passx parmk  P"+upp_polnb+" D");
                for(int j=0; j<upp_polnb+1; j++) {
                    out1.print  (upppolyad.getVdvo(j));
                }
                out1.print  (" P"+low_polnb+" D");
                for(int j=0; j<low_polnb+1; j++) {
                    out1.print  (trm_vdvo[j]);
                }
                out1.println(" "+ntmtype+" "+nskeld+" "+nparaf);
                out1.println("rm MH*");
                if( ntmtype.equals("pol") ) {
                    out1.println("rm MP*");
                }
                else {
                    out1.println("rm MD*");
                }
                out1.println("##");
            }
        }
        catch (IOException ioe) {                                    // IO error
            JOptionPane.showMessageDialog(null,"IO error while writing file"+lnsep+
                                               njobf                        +lnsep+
                                               ioe);
            return;
        }
        finally {
            if( out1 != null ) {
                out1.close();
                if( out1.checkError() ) {
                    JOptionPane.showMessageDialog(null,"PrintWriter error while creating file"+lnsep+
                                                       njobf);
                    return;
                }
            }
        }
        JOptionPane.showMessageDialog(null,"The job "+njobf+" has been created"              +lnsep+
                                           "You have to run it to create the parameter file.");
        try {
            Process monproc = Runtime.getRuntime().exec("chmod u+x "+njobf);  // allow execution
        }
        catch (IOException ioe) {                                    // rights modification error
            JOptionPane.showMessageDialog(null,"IO error while changing rights of file"+lnsep+
                                               njobf                                   +lnsep+
                                               ioe);
            return;
        }
    }

/////////////////////////////////////////////////////////////////////

    // check Basics panel
    private boolean testBasics() {

        if(nmol == "") {
            return false;                                            // molecule
        }
        if( npack.equals("D2hTDS") ) {
            nrep = (String) jcbrep.getSelectedItem();
            if(nrep == "") {
                return false;                                        // representation
            }
        }
        if(nparaf == "") {
            return false;                                            // parameter file name
        }
        if( ! testPsc()) {
            return false;                                            // psc
        }
        return true;
    }

    // ask to fully define Basics
    private void askBasics() {

        JOptionPane.showMessageDialog(null,"You have to first fully define BASICS specifications");
        return;
    }

    // check polyad scheme coefficients
    private boolean testPsc() {

        for (int i=0; i<nbvqn; i++) {
            npsc = (String) jcbpsc[i].getSelectedItem();
            if (npsc == "") {
                return false;
            }
        }
        return true;
    }

    // UPPER and TRM fully defined OR undefined
    private boolean fuzzyUppPolTrm() {                               // upper polyad + transition moment

        int nbdef = 0;                                               //   defined items #
        int nbundef = 0;                                             // undefined items #

        if( ! upppolyad.isSetPolnb() ) {
            nbundef++;
        }
        else {
            nbdef++;
            for ( int i=lowpolyad.getPolnb()+1; i<upppolyad.getPolnb()+1 ; i++) {  // dvo
                if( upppolyad.isSetVdvo(i) ) {
                    nbdef++;
                }
                else {
                    nbundef++;
                }
            }
        }
        // trm
        ntmtype = (String) jcbtmtype.getSelectedItem();              // tmtype
        if(ntmtype == "") {
            nbundef++;
        }
        else {
            nbdef++;
        }
        for ( int i=0; i<low_polnb+1; i++) {
            trm_ndvo = (String) trm_jcbdvo[i].getSelectedItem();
            if( trm_ndvo == "" ) {
                nbundef++;
            }
            else {
                nbdef++;
                trm_vdvo[i] = Integer.parseInt(trm_ndvo);
            }
        }
        if( (nbdef != 0) && (nbundef != 0) ) {
            return true;                                             // fuzzy
        }
        else {
            return false;                                            // fully defined OR undefined
        }
    }

}
