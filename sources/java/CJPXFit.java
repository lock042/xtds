/*
 * Class for xfit jobs creation, called by CreateJob
 */

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;
import java.text.*;
import java.util.*;

/**
 * This panel creates two jobs to xfit experimental data.
 */
public class CJPXFit extends    JPanel
                     implements CJPint, ActionListener {

    // panels
    private JPanel pnord;
    private JPanel  pno;
    private JPanel  pnc;
    private JPanel  pns;
    private JPanel pouest;
    private JPanel pcentre;
    private JPanel   pcn;
    private JPanel[]  pcnx;
    private JPanel[]   pcnxn;
    private JPanel[]    pcnxno;
    private JPanel[]    pcnxnc;
    private JPanel[]   pcnxo;
    private JPanel[]   pcnxc;
    private JPanel[]    pcnxc1;
    private JPanel[]    pcnxc2;
    private JPanel[]    pcnxc3;
    private JPanel[]    pcnxc31;
    private JPanel psud;

    // BASICS
    // molecule
    private JComboBox    jcbmol;                                     // molecule jcb
    private String       nmol;                                       // molecule name
    private String[]     trep;                                       // representation choice array
    private JComboBox    jcbrep;                                     // representation jcb
    private String       nrep;                                       // representation name
    // constraint file
    private JButton jbconsf;                                         // constraint file button
    private String  nconsf;                                          // constraint file name
    private JLabel  lconsf;                                          // constraint file label
    // scheme coefficients
    private JComboBox[] jcbpsc;                                      // polyad scheme coef. jcb
    private String[]    tpsc;                                        // polyad scheme coef. choice array
    private String      npsc;                                        // current psc name
    private int[]       vpsc;                                        // polyad scheme coef. values
    // options
    private JRadioButton jrblpcmax;                                  // lpcmax jrb
    private String[]     tlpcmax;                                    // lpcmax choice array
    private JComboBox    jcblpcmax;                                  // lpcmax jcb
    private JRadioButton jrbrmxomc;                                  // rmxomc jrb
    private String[]     trmxomc;                                    // rmxomc choice array
    private JComboBox    jcbrmxomc;                                  // rmxomc jcb
    private JRadioButton jrballcal;                                  // allcal jrb
    private JRadioButton jrballdip;                                  // alldip jrb
    private JRadioButton jrballpol;                                  // allpol jrb
    // POLYADS
    private boolean[]     polisu;                                    // polyad in-use
    private int           polnbmax;                                  // max polnb
    private int           nbpol;                                     // nb of polyads
    private int           icpol;                                     // current main polyad index
    private int           icpols;                                    // current  sub-polyad index
    private int           icpolmax;                                  // polnbmax     polyad index
    private JScrollPane   jsp;                                       // with lifts
    private String[]      tjpol;                                     // jpol choice array
    private JComboBox[]   jcbjpol;                                   // jmax of each polyad jcb
    private String        njpol;                                     // current jpol name
    private double[]      vjpol;                                     // polyads J values
    private Polyad[]      polyads;                                   // polyads
    private JPPolyad[]    jpps;                                      // polyad JPanels
    private int           cur_polnb;                                 // current polyad #
    private int           inf_polnb;                                 // lower   polyad #
    private int           sup_polnb;                                 // upper   polyad #
    private int           mxtdvo = 10;                               // tdvo size : 0 -> 9 order
    private String[]      tdvo;                                      // development order choice array
    // TRANSITIONS
    private int                   basepol = 10;                      // nb of polyad base for mxtrans evaluation
    private String[]              ntransp;                           // possible transition names (unique)
    private int                   nbtransp;                          // nb of possible transitions
    private boolean[]             transpisu;                         // this possible transition is in-use
    private int                   mxtrans;                           // max nb of transitions
    private String[]              ntrans;                            // transition name
    private int                   nbtrans;                           // nb of transitions
    private int                   ictrans;                           // current                transition index
    private int                   ictransp;                          // current       possible transition index
    private int[]                 itransp;                           // corresponding possible transition index
    private JComboBox[][]         trm_jcbdvo;                        // transition moment development order jcb
    private String                trm_ndvo;                          // transition moment development order name
    private int[][]               transp_vdvo;                       // dvo values for possible transitions
    private int[][]               trm_vdvo;                          // transition moment development order values
    private int[]                 trm_nbdvo;                         // nb of dev. orders for each          transition
    private int[]                 transp_nbdvo;                      // nb of dev. orders for each possible transition
    private JRadioButton[]        jrbpara;                           // para option jrb
    private JLabel[]              jlpara;                            // para option file jl
    private String[]              spara;                             // para option file full name
    private ButtonGroup[]         group;                             // RadioButton group
    private JRadioButton[]        jrbHfit;                           // Hamiltonian       fit jrb
    private JRadioButton[]        jrbTfit;                           // transition moment fit jrb
    private JRadioButton[]        jrbNfit;                           // NO                fit jrb
    private JRadioButton[]        jrbdvo;                            // dev. order option jrb
    private JRadioButton[]        jrbpol;                            // pol (raman) option jrb
    private JRadioButton[]        jrbpolst;                          // polarization status jrb
    private JComboBox[]           jcbpolst;                          // polst jcb
    private String[]              tpolst;                            // polst choice array
    private JButton[]             jbdefasg;                          // define ASG jb
    private JFXasg[]              jfxasg;                            // define ASG jf
    private int[]                 isup;                              // upper polyad index
    private int[]                 iinf;                              // lower polyad index
    private int                   jdiff;                             // upper and lower polyads j difference
    private int                   xmy;                               // x-y for PxmPy
    private int                   cxmy;                              // current xmy
    private boolean               lhpf;                              // only H param. file
    private boolean               ltpf;                              // only T param. file(s)
    private boolean               lpol[];                            // pol possible transitions present
    private boolean               ldip[];                            // dip possible transitions present
    // ParaFile_adj
    private JFAdj                 jfadj;                             // define extra ParaFile_adj
    private int                   nbadj;                             // nb of  extra ParaFile_adj

    // WEST
    private JButton jbaddpol;                                        // add polyad             button
    private JButton jbshowpol;                                       // show polyad            button
    private JButton jbremlastpol;                                    // remove last polyad     button
    private JButton jbsettrans;                                      // set transitions        button
    private JButton jbaddtrans;                                      // add transition         button
    private JButton jbremlasttrans;                                  // remove last transition button
    private JButton jbsetadj;                                        // set ParaFile_adj
    private Box     boxouest;                                        // west panel box
    // SOUTH
    private JButton jbreset;                                         // reset
    private JButton jbsave;                                          // save
    private Box     boxsud;                                          // button box

    // CreateJob parameters
    private String   playd;                                          // XTDS installation directory
    private String   workd;                                          // working directory
    private String   npack;                                          // package name
    private String[] moldirs;                                        // molecule directories
    private int      nbvqn;                                          // nb of vibrational quantum numbers
    private int      nbclab;                                         // nb of characters of a parameter label
    private int      mxpol;                                          // max nb of polyads
    private int      mxsnv   = 20;                                   // max nb of vibrational sublevels to be choosen for LPCMAX
    private int      mxnbadj = 20;                                   // max nb of extra ParaFile_adj

    // miscellaneous
    private StringBuffer   sb;                                       // show polyad sb
    private StringBuffer   sbcmd;                                    // constraint file creation command
    private String         ncmd;                                     // commande string
    private String         str;                                      // parameter file reading string
    private BufferedReader br;                                       // parameter file reading buffer
    private BufferedReader bropt;                                    // optional parameter file reading buffer
    private String         nskelf;                                   // Pa_skel file name
    private String         nskeld;                                   // Pa_skel directory
    private int            n;                                        // n !
    private String         njobf;                                    // job name
    private String         njobr;                                    // job name (reduced)
    private PrintWriter    out1;                                     // job writing pw
    private File           f;                                        // f !
    private String lnsep;                                            // line separator
    private String fisep;                                            // file separator

/////////////////////////////////////////////////////////////////////

    /**
     * Constructs a new CJPXFit.
     */
    public CJPXFit() {

        playd = System.getProperty("xtds.home");                     // XTDS installation directory
        workd = System.getProperty("user.dir");                      // working directory
        lnsep = System.getProperty("line.separator");
        fisep = System.getProperty("file.separator");
        setName("CJPXFit");                                          // for help files
        setLayout(new BorderLayout());

        // south panel
        jbreset = new JButton();                                     // managed by CreateJob
        jbreset.setBackground(Color.WHITE);
        jbsave  = new JButton("Save");                               // job file saving button
        jbsave.setBackground(Color.WHITE);
        jbsave.addActionListener (new ActionListener () {
            public void actionPerformed (ActionEvent ae) {           // jbsave action
                saveCJP();
            }
        }
        );

        boxsud = Box.createHorizontalBox();                          // add buttons to box
        boxsud.add(jbreset);
        boxsud.add(Box.createHorizontalStrut(15));
        boxsud.add(jbsave);
        psud = new JPanel();
        psud.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        psud.add(boxsud);                                            // add box

        // west panel for polyad and transition buttons
        boxouest = Box.createVerticalBox();                          // add buttons to box
        jbaddpol = new JButton("Add Polyad");
        jbaddpol.setBackground(Color.WHITE);
        jbaddpol.addActionListener(this);
        boxouest.add(jbaddpol);
        boxouest.add(Box.createVerticalStrut(15));
        jbshowpol = new JButton("Show Polyads");
        jbshowpol.setBackground(Color.WHITE);
        jbshowpol.addActionListener(this);
        boxouest.add(jbshowpol);
        boxouest.add(Box.createVerticalStrut(15));
        jbremlastpol = new JButton("Remove Last Polyad");
        jbremlastpol.setBackground(Color.WHITE);
        jbremlastpol.addActionListener(this);
        boxouest.add(jbremlastpol);
        boxouest.add(Box.createVerticalStrut(120));
        jbsettrans = new JButton("Set Transitions");
        jbsettrans.setBackground(Color.WHITE);
        jbsettrans.addActionListener(this);
        boxouest.add(jbsettrans);
        pouest = new JPanel();
        pouest.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLUE,4),null));
        pouest.add(boxouest);

        // center panel for polyad display
        pcentre = new JPanel(new BorderLayout());
        jsp = new JScrollPane(pcentre);                              // lifts
        pcn = new JPanel(new GridLayout(0,1,5,5));
        pcentre.add(pcn,"North");

        // display panels
        add(pouest,  "West");
        add(jsp,     "Center");
        add(psud,    "South");
    }

/////////////////////////////////////////////////////////////////////

    /**
     * Returns the reset button.
     * <br>Called by CreateJob which manages it.
     */
    public JButton getJBreset() {

        return jbreset;
    }

/////////////////////////////////////////////////////////////////////

    /**
     * Sets basic parameters and creates north panel.
     * <br>Called by CreateJob to activate this panel.
     * @param cnpack     name of the package
     * @param cmoldirs   molecule directories
     * @param cmxpol     MXPOL
     * @param cnbclab    number of characters of parameter label
     * @param cnbvqn     number of vibrational quantum numbers
     */
    public void setBasicPara(String cnpack, String[] cmoldirs, int cmxpol, int cnbclab, int cnbvqn) {

        npack   = cnpack;                                            // package name
        moldirs = cmoldirs;                                          // molecule directories
        mxpol   = cmxpol;                                            // MXPOL
        nbclab  = cnbclab;                                           // nb of characters of a parameter label
        nbvqn   = cnbvqn;                                            // nb of vibrational quantum numbers

        createNorth();
    }

/////////////////////////////////////////////////////////////////////

    // north panel for polyad characteristics setting and display
    private void createNorth() {

        pnord = new JPanel(new BorderLayout());                      // north panel
        pno = new JPanel(new GridLayout(2,0,5,5));
        pnc = new JPanel(new GridLayout(2,0,5,5));

        // BASICS
        // molecule choice
        pno.add(new JLabel(""));
        JPanel jpmr;
        jpmr =  new JPanel(new GridLayout(0,4,0,5));
        jpmr.add(new JLabel("Molecule ",null,JLabel.RIGHT));
        jcbmol = new JComboBox();
        jcbmol.addItem("");
        for( int i=0; i<moldirs.length; i++ ) {
            jcbmol.addItem(moldirs[i]);
        }
        jcbmol.setSelectedItem("");                                  // default to space
        jcbmol.setBackground(Color.WHITE);
        nmol = "";
        jcbmol.addActionListener(this);
        jpmr.add(jcbmol);
        if( npack.equals("D2hTDS") ) {
            // representation choice
            jpmr.add(new JLabel("Representation ",null,JLabel.RIGHT));
            trep = new String[4];
            trep[0] = "";                                            // space first
            trep[1] = "Ir";
            trep[2] = "IIr";
            trep[3] = "IIIr";
            jcbrep = new JComboBox(trep);
            jcbrep.setSelectedItem("");                              // default to space
            jcbrep.setBackground(Color.WHITE);
            jpmr.add(jcbrep);
        }
        pnc.add(jpmr);
        // polyad scheme coefficients choice
        pno.add(new JLabel("Polyad Scheme "));
        JPanel jppsc = new JPanel(new GridLayout(0,2*nbvqn+1,5,5));  // nbvqn = nb of vibrational quantum numbers
        jppsc.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        jcbpsc = new JComboBox[nbvqn];
        tpsc = new String[mxpol];                                    // polyad scheme coefficients array
        for( icpol=0; icpol<mxpol; icpol++) {
            tpsc[icpol] = String.valueOf(icpol);                     // 0 -> mxpol
        }
        for ( int i=0; i<nbvqn; i++) {                               // choice Label
            if(i == 0) {
                jppsc.add(new JLabel(" [P]n = "));                   // polyad number
            }
            jcbpsc[i] = new JComboBox();
            jcbpsc[i].addItem("");
            for( int j=0; j<tpsc.length; j++ ) {
                jcbpsc[i].addItem(tpsc[j]);
            }
            jcbpsc[i].setSelectedItem("");                           // default to space
            jcbpsc[i].setBackground(Color.WHITE);
            jcbpsc[i].addActionListener(this);
            vpsc = new int[nbvqn];                                   // polyad scheme coefficients values
            jppsc.add(jcbpsc[i]);
            if(i != nbvqn-1) {                                       // set label
                jppsc.add(new JLabel("*v"+(i+1)+" +"));
            }
            else {
                jppsc.add(new JLabel("*v"+(i+1)));
            }
        }
        pnc.add(jppsc);
        pnord.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLUE,4),"BASICS"));

        // POLYADS
        pcnx  = new JPanel[mxpol];                                   // polyad panel
        tjpol = new String[201];                                     // 201 due to space plus jmax : 0(.5) -> 199(.5)
        tjpol[0] = "";                                               // space first
        for( int i=1; i<tjpol.length; i++) {                         // array set
            if( npack.equals("C3vsTDS") ) {
                tjpol[i] = String.valueOf(i-1+0.5);
            }
        else {
                tjpol[i] = String.valueOf(i-1);
            }
        }
        vjpol     = new double[mxpol];

        tdvo = new String[mxtdvo];
        for(int i=0; i<mxtdvo; i++) {                                // 0 -> 9
            tdvo[i] = String.valueOf(i);
        }

        polyads = new Polyad[mxpol];                                 // polyads
        jpps    = new JPPolyad[mxpol];                               // their JPanel
        polisu  = new boolean[mxpol];                                // polyade used ?
        Arrays.fill(polisu, false);                                  // no one

        nbpol = 0;                                                   // nb of polyads

        // add panels and sub-panels
        pnord.add(pno,"West");
        pnord.add(pnc,"Center");
        add(pnord, "North");
    }

/////////////////////////////////////////////////////////////////////

    /**
     * Processes the events.
     */
    public void actionPerformed(ActionEvent evt) {

        // BASICS
        // molecule
        if (evt.getSource() == jcbmol ) {
            nmol = (String) jcbmol.getSelectedItem();
            // unselect representation and parameter file
            if( npack.equals("D2hTDS") ) {
                jcbrep.setSelectedItem("");
            }
            if( nmol != "" ) {
                nskeld = playd+fisep+"packages"+fisep+npack+fisep+"para"+fisep+nmol;  // Pa_skel directory
                nskelf = nskeld+fisep+"Pa_skel";
            }
            return;
        }
        // constraint file
        if (evt.getSource() == jbconsf) {
            JFileChooser jfcconsf = new JFileChooser(playd+fisep+"packages"+fisep+npack+fisep+"ctrp"+fisep+nmol);  // default choice directory
            jfcconsf.setSize(400,300);
            jfcconsf.setFileSelectionMode(JFileChooser.FILES_ONLY);  // files only
            jfcconsf.setDialogTitle("Define the constraint file to be used");
            Container parent = jbconsf.getParent();
            int choice = jfcconsf.showDialog(parent,"Select");       // Dialog, Select
            if (choice == JFileChooser.APPROVE_OPTION) {
                nconsf= jfcconsf.getSelectedFile().getAbsolutePath();
            }
            else {
                nconsf = "";
            }
            lconsf.setText(nconsf);
            pcn.revalidate();
            return;
        }
        // Polyad Scheme Coefficients
        for (int i=0; i<nbvqn; i++) {
            if(evt.getSource() == jcbpsc[i] ) {                      // whatever coef.
                npsc = (String) jcbpsc[i].getSelectedItem();
                if(npsc != "") {
                    vpsc[i] =  Integer.parseInt(npsc);               // save coef. value
                }
                return;
            }
        }

        // ADD POLYAD
        if(evt.getSource() == jbaddpol) {
            // BASICS checked ?
            if( ! testBasics()) {
                askBasics();
                return;
            }
            // existing polyad(s) checked ?
            if( nbpol >= 1 ) {
                if( ! testAllPol( nbpol ) ) {
                    return;
                }
            }
            // max reached ?
            if(nbpol == mxpol) {
                JOptionPane.showMessageDialog(null,"The maximum number of polyads is reached ("+mxpol+")");
                return;
            }
            icpol = nbpol;
            nbpol++;
            // prepare choice
            pcnx[icpol]  = new JPanel(new BorderLayout());           // plain panel
            polyads[icpol] = new Polyad(nbvqn);                      // polyad
            if( ! polyads[icpol].setInd(npack, nskelf) ) {           // set frequency indexes
                jcbmol.setSelectedItem("");                          // file error : unselect molecule
            }
            polyads[icpol].setName("("+nbpol+") POLYAD");            // its name
            jpps[icpol] = new JPPolyad(this, polyads[icpol], tpsc, pcnx[icpol]);  // its JPP panel

            pcn.add(pcnx[icpol]);
            pcnx[icpol].revalidate();
            // go to bottom to show the last polyad panel
            SwingUtilities.invokeLater(new Runnable() {
                 public void run() {
                     final JScrollBar jsbv = jsp.getVerticalScrollBar();
                     jsbv.setValue(jsbv.getMaximum());
                 }
            });
            //
            return;
        }

        // SHOW POLYADS
        if(evt.getSource() == jbshowpol) {
            if( testAllPol( nbpol ) ) {                              // if everything checked
                showPol();
            }
            return;
        }

        // REMOVE LAST POLYAD
        if(evt.getSource() == jbremlastpol) {
            if(nbpol != 0) {
                pcn.remove(pcnx[nbpol-1]);
                pcn.repaint();
                nbpol --;
            }
            return;
        }

        // SET TRANSITIONS
        if(evt.getSource() == jbsettrans) {
            // existing polyad(s) checked ?
            if( nbpol >= 1 ) {
                if( ! testBasics() ) {
                    askBasics();
                    return;
                }
                if( ! testAllPol( nbpol ) ) {
                    return;
                }
            }
            // at least one polyad
            if(nbpol == 0) {
                JOptionPane.showMessageDialog(null,"No polyad defined");
                return;
            }
            // set number of possible transitions
            nbtransp = 0;
            for(int i=1; i<nbpol+1; i++) {
                nbtransp = nbtransp +i;
            }
            ntransp = new String[nbtransp];
            ictrans = 0;
            for(int j=0; j<nbpol; j++) {
                for(int i=j; i<nbpol; i++) {
                    ntransp[ictrans] = "P"+polyads[i].getPolnb()+"mP"+polyads[j].getPolnb();  // their names
                    ictrans++;
                }
            }
            // characteristics array
            transpisu    = new boolean[nbtransp];
            transp_vdvo  = new int[nbtransp][];
            transp_nbdvo = new int[nbtransp];
            isup         = new int[nbtransp];
            iinf         = new int[nbtransp];
            lpol         = new boolean[nbtransp];                    // pol possible transitions present
            ldip         = new boolean[nbtransp];                    // dip possible transitions present
            // set max number of transitions
            basepol = Math.max(basepol,mxpol);
            mxtrans = 0;
            for(int i=1; i<basepol+1; i++) {
                mxtrans = mxtrans +i;
            }
            mxtrans = mxtrans*4;                                     // NOT pol, pol with 3 POLST
            // and implied polyad for each transition
            ictransp = 0;
            for(int j=0; j<nbpol; j++) {
                for(int i=j; i<nbpol; i++) {
                    isup[ictransp] = i;
                    iinf[ictransp] = j;
                    transp_nbdvo[ictransp] = polyads[iinf[ictransp]].getPolnb()+1;
                    transp_vdvo[ictransp]  = new int[transp_nbdvo[ictransp]];
                    ictransp++;
                }
            }
            // confirm
            sbPol();
            sb.append("Are you sure that"        +lnsep+
                      "All polyads are created ?");
            n = JOptionPane.showConfirmDialog(null,sb.toString(),"Set Transitions",JOptionPane.YES_NO_OPTION);
            if(n != JOptionPane.YES_OPTION) {
                return;
            }
            // north panel modification
            pnord.removeAll();
            pno = new JPanel(new GridLayout(4,0,5,5));
            pnc = new JPanel(new GridLayout(4,0,5,5));
            pno.add(new JLabel("Molecule "+nmol));
            // end tag options of xpafit
            // LPCMAX
            jrblpcmax = new JRadioButton("Initial Basis Components");
          //jrblpcmax.setBackground(Color.WHITE);
            jrblpcmax.addActionListener(this);
            tlpcmax  = new String[mxsnv];                            // lpcmax choice array
            for( int i=0; i<mxsnv; i++) {
                tlpcmax[i] = String.valueOf(i+1);                    // 1 -> mxsnv
            }
            jcblpcmax = new JComboBox();
            jcblpcmax.addItem("");
            for( int j=0; j<tlpcmax.length; j++ ) {
                jcblpcmax.addItem(tlpcmax[j]);
            }
            jcblpcmax.setSelectedItem("");                           // default to space
            jcblpcmax.setBackground(Color.WHITE);
            jcblpcmax.setEnabled(false);
            // RMXOMC
            jrbrmxomc = new JRadioButton("Intensity Relative Threshold");
          //jrbrmxomc.setBackground(Color.WHITE);
            jrbrmxomc.addActionListener(this);
            trmxomc = new String[18];
            for( int j=0; j<18; j++ ) {
                if( j <10 ) {
                    trmxomc[j] = String.valueOf((j+1)*10);           // each 10 up to 100
                }
                else {
                    trmxomc[j] = String.valueOf(150+(j-10)*50);      // each 50 up to 500
                }
            }
            jcbrmxomc = new JComboBox();
            jcbrmxomc.addItem("");
            for( int j=0; j<trmxomc.length; j++ ) {
                jcbrmxomc.addItem(trmxomc[j]);
            }
            jcbrmxomc.setSelectedItem("");                           // default to space
            jcbrmxomc.setBackground(Color.WHITE);
            jcbrmxomc.setEnabled(false);
            //
            JPanel jpopt = new JPanel(new GridLayout(0,4,5,5));
            jpopt.add(jrblpcmax);
            jpopt.add(jcblpcmax);
            jpopt.add(jrbrmxomc);
            jpopt.add(jcbrmxomc);
            pnc.add(jpopt);
            // all* options
            pno.add(new JLabel(""));
            jrballcal = new JRadioButton("allcal");
          //jrballcal.setBackground(Color.WHITE);
            jrballcal.addActionListener(this);
            jrballdip = new JRadioButton("alldip");
          //jrballdip.setBackground(Color.WHITE);
            jrballdip.addActionListener(this);
            jrballpol = new JRadioButton("allpol");
          //jrballpol.setBackground(Color.WHITE);
            jrballpol.addActionListener(this);
            JPanel jpall = new JPanel(new GridLayout(0,4,5,5));
            jpall.add(jrballcal);
            jpall.add(jrballdip);
            jpall.add(jrballpol);
            pnc.add(jpall);
            // add polyad Jmax panel
            pno.add(new JLabel("Polyad Jmax "));
            JPanel jpjpol = new JPanel(new GridLayout(0,2*nbpol,5,5));
            jcbjpol  = new JComboBox[nbpol];
            for( icpol=0; icpol<nbpol; icpol++) {
                jcbjpol[icpol] = new JComboBox(tjpol);
                jcbjpol[icpol].setSelectedItem("");
                jcbjpol[icpol].setBackground(Color.WHITE);
                jcbjpol[icpol].addActionListener(this);
                jpjpol.add(new JLabel("JP"+polyads[icpol].getPolnb()+" ",null,JLabel.RIGHT));
                jpjpol.add(jcbjpol[icpol]);
                jcbjpol[icpol].setEnabled(false);
            }
            jpjpol.setBorder(BorderFactory.createLineBorder(Color.BLACK));
            pnc.add(jpjpol);
            // constraint file choice
            jbconsf = new JButton("Constraint File");
            jbconsf.setBackground(Color.WHITE);
            jbconsf.addActionListener(this);
            pno.add(jbconsf);
            lconsf = new JLabel("");                                 // default to space
            lconsf.setOpaque(true);
            lconsf.setBorder(BorderFactory.createLineBorder(Color.BLACK));
            nconsf = "";                                             // unselect constraint file
            lconsf.setText(nconsf);
            pnc.add(lconsf);
            //
            pnord.add(pno,"West");
            pnord.add(pnc,"Center");
            // add transition and ParaFile_adj buttons
            pouest.removeAll();

            boxouest = Box.createVerticalBox();
            jbaddtrans = new JButton("Add Transition");
            jbaddtrans.setBackground(Color.WHITE);
            jbaddtrans.addActionListener(this);
            boxouest.add(jbaddtrans);
            boxouest.add(Box.createVerticalStrut(15));
            jbremlasttrans = new JButton("Remove Last Transition");
            jbremlasttrans.setBackground(Color.WHITE);
            jbremlasttrans.addActionListener(this);
            boxouest.add(jbremlasttrans);
            boxouest.add(Box.createVerticalStrut(60));
            jbsetadj = new JButton("Set Extra ParaFile_adj");
            jbsetadj.setBackground(Color.WHITE);
            jbsetadj.addActionListener(this);
            boxouest.add(jbsetadj);
            boxouest.add(Box.createVerticalStrut(80));
            boxouest.add(jbshowpol);
            pouest.add(boxouest);

            // empty center panel
            for(int icpol=0; icpol<nbpol; icpol++) {
                pcn.remove(pcnx[icpol]);
            }
            pcn.repaint();
            //
            Point pt = pcentre.getLocationOnScreen();                // to allow relative location
            jfadj    = new JFAdj(this, pt, ntransp, npack, nmol, mxnbadj);
            // create arrays
            ntrans     = new String[mxtrans];
            itransp    = new int[mxtrans];
            trm_jcbdvo = new JComboBox[mxtrans][];
            trm_vdvo   = new int[mxtrans][];
            trm_nbdvo  = new int[mxtrans];
            jrbpara    = new JRadioButton[mxtrans];
            jlpara     = new JLabel[mxtrans];
            spara      = new String[mxtrans];
            group      = new ButtonGroup[mxtrans];
            jrbHfit    = new JRadioButton[mxtrans];
            jrbTfit    = new JRadioButton[mxtrans];
            jrbNfit    = new JRadioButton[mxtrans];
            jrbdvo     = new JRadioButton[mxtrans];
            jrbpol     = new JRadioButton[mxtrans];
            jrbpolst   = new JRadioButton[mxtrans];
            jcbpolst   = new JComboBox[mxtrans];
            tpolst     = new String[4];
            tpolst[0]  = "";
            tpolst[1]  = "R111";
            tpolst[2]  = "R110";
            tpolst[3]  = "R001";
            jbdefasg   = new JButton[mxtrans];
            jfxasg     = new JFXasg[mxtrans];
            // create panels
            pcnx    = new JPanel[mxtrans];
            pcnxo   = new JPanel[mxtrans];
            pcnxc   = new JPanel[mxtrans];
            pcnxc1  = new JPanel[mxtrans];
            pcnxc2  = new JPanel[mxtrans];
            pcnxc3  = new JPanel[mxtrans];
            pcnxc31 = new JPanel[mxtrans];

            nbtrans = 0;                                             // nb of transitions
            return;
        }

        // LPCMAX
        if(evt.getSource() == jrblpcmax ) {
            if( jrblpcmax.isSelected() ) {
                jcblpcmax.setEnabled(true);
            }
            else {
                jcblpcmax.setSelectedItem("");
                jcblpcmax.setEnabled(false);
            }
            return;
         }

        // RMXOMC
        if(evt.getSource() == jrbrmxomc ) {
            if( jrbrmxomc.isSelected() ) {
                jcbrmxomc.setEnabled(true);
            }
            else {
                jcbrmxomc.setSelectedItem("");
                jcbrmxomc.setEnabled(false);
            }
            return;
         }

        // ALLCAL
        if(evt.getSource() == jrballcal ) {
            for(ictrans=0; ictrans<nbtrans; ictrans++) {
                if( jrbHfit[ictrans].isSelected() ) {
                    if( jrballcal.isSelected() ) {
                        jrbdvo[ictrans].setEnabled(true);
                      //jrbdvo[ictrans].setBackground(Color.WHITE);
                        jrbdvo[ictrans].setSelected(true);
                        for( int j=0; j<trm_nbdvo[ictrans]; j++) {
                          //trm_jcbdvo[ictrans][j].setSelectedItem("");
                            trm_jcbdvo[ictrans][j].setEnabled(true);
                        }
                    }
                    else {
                        if( jrbdvo[ictrans].isSelected() ) {
                            jrbdvo[ictrans].doClick();
                        }
                        jrbdvo[ictrans].setEnabled(false);
                      //jrbdvo[ictrans].setBackground(this.getBackground());
                    }
                }
            }
            return;
        }

        // ALLDIP
        if(evt.getSource() == jrballdip ) {
            if( jrballdip.isSelected() ) {
                jrballcal.setSelected(true);
                jrballcal.setEnabled(false);
                for(ictrans=0; ictrans<nbtrans; ictrans++) {
                    if( ! jrbTfit[ictrans].isSelected() ) {
                        jrbdvo[ictrans].setEnabled(true);
                      //jrbdvo[ictrans].setBackground(Color.WHITE);
                        jrbdvo[ictrans].setSelected(true);
                        for( int j=0; j<trm_nbdvo[ictrans]; j++) {
                          //trm_jcbdvo[ictrans][j].setSelectedItem("");
                            trm_jcbdvo[ictrans][j].setEnabled(true);
                        }
                    }
                }
            }
            else {
                if( ! jrballpol.isSelected() ) {
                    jrballcal.setSelected(false);
                    jrballcal.setEnabled(true);
                    for(ictrans=0; ictrans<nbtrans; ictrans++) {
                        if( ! jrbTfit[ictrans].isSelected() ) {
                            if( jrbdvo[ictrans].isSelected() ) {
                                jrbdvo[ictrans].doClick();
                            }
                            jrbdvo[ictrans].setEnabled(false);
                          //jrbdvo[ictrans].setBackground(this.getBackground());
                        }
                    }
                }
            }
            return;
        }

        // ALLPOL
        if(evt.getSource() == jrballpol ) {
            if( jrballpol.isSelected() ) {
                jrballcal.setSelected(true);
                jrballcal.setEnabled(false);
                for(ictrans=0; ictrans<nbtrans; ictrans++) {
                    if( ! jrbTfit[ictrans].isSelected() ) {
                        jrbdvo[ictrans].setEnabled(true);
                      //jrbdvo[ictrans].setBackground(Color.WHITE);
                        jrbdvo[ictrans].setSelected(true);
                        for( int j=0; j<trm_nbdvo[ictrans]; j++) {
                          //trm_jcbdvo[ictrans][j].setSelectedItem("");
                            trm_jcbdvo[ictrans][j].setEnabled(true);
                        }
                    }
                }
            }
            else {
                if( ! jrballdip.isSelected() ) {
                    jrballcal.setSelected(false);
                    jrballcal.setEnabled(true);
                    for(ictrans=0; ictrans<nbtrans; ictrans++) {
                        if( ! jrbTfit[ictrans].isSelected() ) {
                            if( jrbdvo[ictrans].isSelected() ) {
                                jrbdvo[ictrans].doClick();
                             }
                             jrbdvo[ictrans].setEnabled(false);
                           //jrbdvo[ictrans].setBackground(this.getBackground());
                         }
                    }
                }
            }
            return;
        }

        // ADD TRANSITION
        if(evt.getSource() == jbaddtrans) {

            // existing transition(s) checked ?
            if( nbtrans >= 1 ) {
                if( ! testAllTrans() ) {
                    return;
                }
            }

            // choose one transition
            if( nbtrans == 0 ) {
                ictransp = 0;
            }
            else {
              //ictransp = itransp[nbtrans-1];
                ictransp = Math.min(itransp[nbtrans-1]+1,nbtransp-1);
            }
            String s = (String)JOptionPane.showInputDialog( null, "Choose a transition :"+lnsep, "Add Transition",
                                                            JOptionPane.PLAIN_MESSAGE, null, ntransp, ntransp[ictransp]);
            // choice cancelled
            if( s == null ) {
                return;
            }

            // 1st ? initialize
            if( nbtrans == 0 ) {
                Arrays.fill(transpisu, false);
            }

            ntrans[nbtrans] = (nbtrans+1)+"_"+s;

            // set ntransp index and transpisu
            for(ictransp=0; ictransp<nbtransp; ictransp++) {
                if(s.equals(ntransp[ictransp])) {
                    if( nbtrans != 0 ) {
                        // test increasing order (possible transition)
                        if( itransp[nbtrans-1] > ictransp ) {
                            // not allowed
                            JOptionPane.showMessageDialog(null,"Transitions have to be defined in increasing order");
                            return;
                        }
                    }
                    //
                    itransp[nbtrans] = ictransp;
                    transpisu[ictransp] = true;
                    // set polyads associated to this transition as used
                    polisu[iinf[ictransp]] = true;
                    polisu[isup[ictransp]] = true;
                    // allow jcbjpol of associated polyads
                    jcbjpol[isup[ictransp]].setEnabled(true);
                    jcbjpol[iinf[ictransp]].setEnabled(true);
                    // set number of dev.order of this transition
                    trm_nbdvo[nbtrans] = transp_nbdvo[ictransp];
                    break;
                }
            }
            // create panel
            pcnx[nbtrans]    = new JPanel(new BorderLayout());
            pcnxo[nbtrans]   = new JPanel(new GridLayout(4,0,5,5));
            pcnxc[nbtrans]   = new JPanel(new GridLayout(4,0,5,5));
            pcnxc1[nbtrans]  = new JPanel(new GridLayout(0,2*trm_nbdvo[nbtrans],5,5));
            pcnxc2[nbtrans]  = new JPanel(new GridLayout(0,2*trm_nbdvo[nbtrans],5,5));
            pcnxc3[nbtrans]  = new JPanel(new GridLayout(0,2,5,5));
            pcnxc31[nbtrans] = new JPanel(new GridLayout(0,2,5,5));
            // extraction
            jbdefasg[nbtrans] = new JButton(" Set Extraction Criteria");
            jbdefasg[nbtrans].setBackground(Color.WHITE);
            jbdefasg[nbtrans].setBorder(BorderFactory.createLineBorder(Color.BLACK));
            jbdefasg[nbtrans].addActionListener(this);
            pcnxo[nbtrans].add(jbdefasg[nbtrans]);
            pcnxc[nbtrans].add(new JLabel(""));
            // parameter file and fit type
            jrbpara[nbtrans] = new JRadioButton("Parameter File ");
            jrbpara[nbtrans].setBorder(BorderFactory.createLineBorder(Color.BLACK));
            jrbpara[nbtrans].setEnabled(false);
            jrbpara[nbtrans].addActionListener(this);
            pcnxo[nbtrans].add(jrbpara[nbtrans]);
            spara[nbtrans] = "";
            jlpara[nbtrans] = new JLabel(spara[nbtrans]);
            jlpara[nbtrans].setOpaque(true);
            jlpara[nbtrans].setBorder(BorderFactory.createLineBorder(Color.BLACK));
            pcnxc3[nbtrans].add(jlpara[nbtrans]);
            jrbHfit[nbtrans] = new JRadioButton("H fit ");
            jrbHfit[nbtrans].setBorder(BorderFactory.createLineBorder(Color.BLACK));
            jrbHfit[nbtrans].setEnabled(false);
            jrbHfit[nbtrans].addActionListener(this);
            pcnxc31[nbtrans].add(jrbHfit[nbtrans]);
            jrbTfit[nbtrans] = new JRadioButton("T fit ");
            jrbTfit[nbtrans].setBorder(BorderFactory.createLineBorder(Color.BLACK));
            jrbTfit[nbtrans].setEnabled(false);
            jrbTfit[nbtrans].addActionListener(this);
            jrbNfit[nbtrans] = new JRadioButton();
            group[nbtrans]   = new ButtonGroup();
            group[nbtrans].add(jrbHfit[nbtrans]);
            group[nbtrans].add(jrbTfit[nbtrans]);
            group[nbtrans].add(jrbNfit[nbtrans]);
            jrbNfit[nbtrans].setSelected(true);
            pcnxc31[nbtrans].add(jrbTfit[nbtrans]);
            pcnxc3[nbtrans].add(pcnxc31[nbtrans]);
            pcnxc[nbtrans].add(pcnxc3[nbtrans]);
            // Development Order
            jrbdvo[nbtrans] = new JRadioButton("Transition Moment Devel. Order ");
            jrbdvo[nbtrans].setBorder(BorderFactory.createLineBorder(Color.BLACK));
            jrbdvo[nbtrans].addActionListener(this);
            pcnxo[nbtrans].add(jrbdvo[nbtrans]);
            trm_jcbdvo[nbtrans] = new JComboBox[trm_nbdvo[nbtrans]];
            trm_vdvo[nbtrans]   = new int[trm_nbdvo[nbtrans]];
            xmy = polyads[isup[itransp[nbtrans]]].getPolnb()-polyads[iinf[itransp[nbtrans]]].getPolnb();
            for( int j=0; j<trm_nbdvo[nbtrans]; j++) {
                trm_jcbdvo[nbtrans][j] = new JComboBox();
                trm_jcbdvo[nbtrans][j].addItem("");
                for( int k=0; k<tdvo.length; k++ ) {
                    trm_jcbdvo[nbtrans][j].addItem(tdvo[k]);
                }
                trm_jcbdvo[nbtrans][j].setSelectedItem("");
                trm_jcbdvo[nbtrans][j].setBackground(Color.WHITE);
                trm_jcbdvo[nbtrans][j].setEnabled(false);
                pcnxc1[nbtrans].add(new JLabel("P"+(j+xmy)+"mP"+j+" D ",null,JLabel.RIGHT));
                pcnxc1[nbtrans].add(trm_jcbdvo[nbtrans][j]);
            }
            if( jrballpol.isSelected() ||
                jrballdip.isSelected()    ) {
                jrbdvo[nbtrans].setSelected(true);
                jrbdvo[nbtrans].setEnabled(true);
              //jrbdvo[nbtrans].setBackground(Color.WHITE);
                for( int j=0; j<trm_nbdvo[nbtrans]; j++) {
                  //trm_jcbdvo[nbtrans][j].setSelectedItem("");
                    trm_jcbdvo[nbtrans][j].setEnabled(true);
                }
            }
            else {
                jrbdvo[nbtrans].setEnabled(false);
              //jrbdvo[nbtrans].setBackground(this.getBackground());
            }
            // pre-initialize trm_jcbdvo (if possible)
            if( nbtrans > 0) {
                for(ictrans=0; ictrans<nbtrans; ictrans++) {
                    cxmy=polyads[isup[itransp[ictrans]]].getPolnb()-polyads[iinf[itransp[ictrans]]].getPolnb();
                    if( cxmy == xmy ) {
                        for( int j=0; j<Math.min(trm_nbdvo[ictrans],trm_nbdvo[nbtrans]); j++) {
                            trm_jcbdvo[nbtrans][j].setSelectedItem(String.valueOf(trm_vdvo[ictrans][j]));
                        }
                    }
                }
            }
            pcnxc1[nbtrans].setBorder(BorderFactory.createLineBorder(Color.BLACK));
            pcnxc[nbtrans].add(pcnxc1[nbtrans],"Center");
            // pol and polst
            jrbpol[nbtrans] = new JRadioButton("pol");
            jrbpol[nbtrans].setBorder(BorderFactory.createLineBorder(Color.BLACK));
            jrbpol[nbtrans].setEnabled(false);
            jrbpol[nbtrans].addActionListener(this);
            pcnxo[nbtrans].add(jrbpol[nbtrans]);
            jrbpolst[nbtrans] = new JRadioButton("Polarization State");
            jrbpolst[nbtrans].setBorder(BorderFactory.createLineBorder(Color.BLACK));
            jrbpolst[nbtrans].setEnabled(false);
            jrbpolst[nbtrans].addActionListener(this);
            jcbpolst[nbtrans] = new JComboBox(tpolst);
            jcbpolst[nbtrans].setBorder(BorderFactory.createLineBorder(Color.BLACK));
            jcbpolst[nbtrans].setEnabled(false);
            jcbpolst[nbtrans].setBackground(Color.WHITE);
            pcnxc2[nbtrans].add(jrbpolst[nbtrans]);
            pcnxc2[nbtrans].add(jcbpolst[nbtrans]);
            pcnxc[nbtrans].add(pcnxc2[nbtrans],"Center");

            pcnx[nbtrans].add(pcnxo[nbtrans],"West");
            pcnx[nbtrans].add(pcnxc[nbtrans],"Center");
            pcnx[nbtrans].setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLUE,4),ntrans[nbtrans]));
            pcn.add(pcnx[nbtrans]);
            pcn.revalidate();
            // go to bottom to show the last transition panel
            SwingUtilities.invokeLater(new Runnable() {
                 public void run() {
                     final JScrollBar jsbv = jsp.getVerticalScrollBar();
                     jsbv.setValue(jsbv.getMaximum());
                 }
            });
            //
            Point pt = pcentre.getLocationOnScreen();                // to allow relative location
            int ix  = (int) (pt.getX()+(pcentre.getSize().width)*.20);
            int iy  = (int)  pt.getY();
            int idx = (int) ((pcentre.getSize().width)/20.);
            int idy = (int) ((pcentre.getSize().height)/10.);
            jfxasg[nbtrans] = new JFXasg(ix+idx*nbtrans, iy+idy*nbtrans, ntrans[nbtrans], npack, nmol);
            nbtrans++;
            return;
        }

        // Jmax
        if( nbpol != 0 ) {
            for (int icpol=0; icpol<nbpol; icpol++) {
                if(evt.getSource() == jcbjpol[icpol] ) {
                    njpol = (String) jcbjpol[icpol].getSelectedItem();
                    if(njpol != "") {
                        vjpol[icpol] = Double.valueOf(njpol);
                    }
                    return;
                }
            }
        }

        // OPTION para
        if(jrbpara != null) {
            for(ictrans=0; ictrans<nbtrans; ictrans++) {
                if(evt.getSource() == jrbpara[ictrans]) {
                    if( jrbdvo[ictrans].isSelected() ) {
                        jrbdvo[ictrans].doClick();
                    }
                    if( jrballdip.isSelected() ||
                        jrballpol.isSelected()    ) {
                        jrbdvo[ictrans].setEnabled(true);
                      //jrbdvo[ictrans].setBackground(Color.WHITE);
                        if( ! jrbdvo[ictrans].isSelected() ) {
                            jrbdvo[ictrans].doClick();
                        }
                    }
                    else {
                        jrbdvo[ictrans].setEnabled(false);
                      //jrbdvo[ictrans].setBackground(this.getBackground());
                    }
                    if(jrbpara[ictrans].isSelected()) {
                        JFileChooser jfcpara = new JFileChooser(playd+fisep+"packages"+fisep+npack+fisep+"para"+fisep+nmol);  // default choice directory
                        jfcpara.setSize(400,300);
                        jfcpara.setFileSelectionMode(JFileChooser.FILES_ONLY);  // files only
                        jfcpara.setDialogTitle("Define the parameter file to be used");
                        Container parent = jrbpara[ictrans].getParent();
                        int choice = jfcpara.showDialog(parent,"Select");  // Dialog, Select
                        if (choice == JFileChooser.APPROVE_OPTION) {
                            spara[ictrans]= jfcpara.getSelectedFile().getAbsolutePath();
                            jlpara[ictrans].setText(jfcpara.getSelectedFile().getName());
                            jrbHfit[ictrans].setSelected(true);
                            jrbHfit[ictrans].setEnabled(true);
                            jrbTfit[ictrans].setEnabled(true);
                            JOptionPane.showMessageDialog(null,"WARNING: default is H fit, select T fit if necessary");
                        }
                        else {
                            jrbpara[ictrans].setSelected(false);
                            jrbNfit[ictrans].setSelected(true);
                            jrbHfit[ictrans].setEnabled(false);
                            jrbTfit[ictrans].setEnabled(false);
                        }
                    }
                    else {
                        spara[ictrans] = "";
                        jlpara[ictrans].setText("");
                        jrbNfit[ictrans].setSelected(true);
                        jrbHfit[ictrans].setEnabled(false);
                        jrbTfit[ictrans].setEnabled(false);
                    }
                    return;
                }
            }
        }

        // H fit
        if( jrbHfit != null ) {
            for(ictrans=0; ictrans<nbtrans; ictrans++) {
                if(evt.getSource() == jrbHfit[ictrans]) {
                    if( jrbHfit[ictrans].isSelected() ) {
                        jfxasg[ictrans].setShowTemp(false);
                        if( jrbdvo[ictrans].isSelected() ) {
                            jrbdvo[ictrans].doClick();
                        }
                        if( jrballcal.isSelected() ||
                            jrballdip.isSelected() ||
                            jrballpol.isSelected()    ) {
                            jrbdvo[ictrans].setEnabled(true);
                            jrbdvo[ictrans].setSelected(true);
                            for( int j=0; j<trm_nbdvo[ictrans]; j++) {
                                trm_jcbdvo[ictrans][j].setEnabled(true);
                            }
                        }
                        else {
                            jrbdvo[ictrans].setEnabled(false);
                        }
                    }
                    return;
                }
            }
        }

        // T fit
        if( jrbTfit != null ) {
            for(ictrans=0; ictrans<nbtrans; ictrans++) {
                if(evt.getSource() == jrbTfit[ictrans]) {
                    if( jrbTfit[ictrans].isSelected() ) {
                        jfxasg[ictrans].setShowTemp(true);
                        jrbdvo[ictrans].setEnabled(true);
                        jrbdvo[ictrans].setSelected(false);
                        jrbdvo[ictrans].doClick();
                    }
                    return;
                }
            }
        }

        // OPTION Devel. Order
        if(jrbdvo != null) {
            for(ictrans=0; ictrans<nbtrans; ictrans++) {
                if(evt.getSource() == jrbdvo[ictrans]) {
                    if( jrbdvo[ictrans].isSelected() ) {
                        for( int j=0; j<trm_nbdvo[ictrans]; j++) {
                            trm_jcbdvo[ictrans][j].setSelectedItem("");
                            trm_jcbdvo[ictrans][j].setEnabled(true);
                        }
                        if( jrbTfit[ictrans].isSelected() ) {
                            jrbpol[ictrans].setEnabled(true);
                          //jrbpol[ictrans].setBackground(Color.WHITE);
                        }
                    }
                    else {
                        for( int j=0; j<trm_nbdvo[ictrans]; j++) {
                            trm_jcbdvo[ictrans][j].setSelectedItem("");
                            trm_jcbdvo[ictrans][j].setEnabled(false);
                        }
                        if( jrbpol[ictrans].isSelected() ) {
                            jrbpol[ictrans].doClick();
                        }
                        if( ! jrbTfit[ictrans].isSelected() ) {
                            jrbpol[ictrans].setEnabled(false);
                          //jrbpol[ictrans].setBackground(this.getBackground());
                            if( jrballdip.isSelected()               ||
                                jrballpol.isSelected()               ||
                                (jrballcal.isSelected()         &&
                                 jrbpara[ictrans].isSelected()     )     ) {
                                jrbdvo[ictrans].setSelected(true);
                              //jrbdvo[ictrans].setEnabled(true);
                              //jrbdvo[ictrans].setBackground(Color.WHITE);
                                for( int j=0; j<trm_nbdvo[ictrans]; j++) {
                                    trm_jcbdvo[ictrans][j].setEnabled(true);
                                }
                            }
                            else {
                                jrbdvo[ictrans].setEnabled(false);
                              //jrbdvo[ictrans].setBackground(this.getBackground());
                                for( int j=0; j<trm_nbdvo[ictrans]; j++) {
                                    trm_jcbdvo[ictrans][j].setEnabled(false);
                                }
                            }
                        }
                        else {
                            jrbdvo[ictrans].setSelected(true);
                          //jrbdvo[ictrans].setEnabled(true);
                          //jrbdvo[ictrans].setBackground(Color.WHITE);
                            for( int j=0; j<trm_nbdvo[ictrans]; j++) {
                                trm_jcbdvo[ictrans][j].setEnabled(true);
                            }
                        }
                    }
                    return;
                }
            }
        }

        // OPTION pol
        if( jrbpol != null ) {
            for(ictrans=0; ictrans<nbtrans; ictrans++) {
                if(evt.getSource() == jrbpol[ictrans]) {
                    if( jrbpol[ictrans].isSelected() ) {
                        jrbpolst[ictrans].setEnabled(true);
                      //jrbpolst[ictrans].setBackground(Color.WHITE);
                    }
                    else {
                        if( jrbpolst[ictrans].isSelected() ) {
                            jrbpolst[ictrans].doClick();
                        }
                        jrbpolst[ictrans].setEnabled(false);
                      //jrbpolst[ictrans].setBackground(this.getBackground());
                    }
                    return;
                }
            }
        }

        // OPTION polst
        if( jrbpolst != null ) {
            for(ictrans=0; ictrans<nbtrans; ictrans++) {
                if(evt.getSource() == jrbpolst[ictrans]) {
                    if( jrbpolst[ictrans].isSelected() ) {
                        jcbpolst[ictrans].setEnabled(true);
                    }
                    else {
                        jcbpolst[ictrans].setSelectedItem("");
                        jcbpolst[ictrans].setEnabled(false);
                    }
                    return;
                }
            }
        }

        // Set Assignments
        if( jbdefasg != null ) {
            for(ictrans=0; ictrans<nbtrans; ictrans++) {
                if(evt.getSource() == jbdefasg[ictrans]) {
                    jfxasg[ictrans].setVisible(false);
                    jfxasg[ictrans].setVisible(true);
                    jfxasg[ictrans].pack();
                    jrbpara[ictrans].setEnabled(true);
                  //jrbpara[ictrans].setBackground(Color.WHITE);
                    return;
                }
            }
        }

        // REMOVE LAST TRANSITION
        if(evt.getSource() == jbremlasttrans) {
            if(nbtrans != 0) {
                // reset transpisu
                for(ictransp=0; ictransp<nbtransp; ictransp++) {
                    transpisu[ictransp] = false;
                }
                // reset polisu, jcbpol
                String[] cjpol = new String[nbpol];                  // save previous
                for(icpol=0; icpol<nbpol; icpol++) {
                    polisu[icpol] = false;
                    cjpol[icpol]  = (String) jcbjpol[icpol].getSelectedItem();
                    jcbjpol[icpol].setSelectedItem("");
                    jcbjpol[icpol].setEnabled(false);
                }
                if( nbtrans > 1 ) {
                    // set transpisu
                    for(ictrans=0; ictrans<nbtrans-1; ictrans++) {
                        ictransp            = itransp[ictrans];
                        transpisu[ictransp] = true;
                        int icsup           = isup[ictransp];
                        polisu[icsup]       = true;
                        int icinf           = iinf[ictransp];
                        polisu[icinf]       = true;
                        if( ! cjpol[icsup].equals("") ) {
                            jcbjpol[icsup].setSelectedItem(cjpol[icsup]);
                            jcbjpol[icsup].setEnabled(true);
                        }
                        if( ! cjpol[icinf].equals("") ) {
                            jcbjpol[icinf].setSelectedItem(cjpol[icinf]);
                            jcbjpol[icinf].setEnabled(true);
                        }
                    }
                }
                // suppress transition panel
                jfxasg[nbtrans-1].setVisible(false);
                pcn.remove(pcnx[nbtrans-1]);
                pcn.repaint();
                nbtrans --;
            }
            return;
        }

        // SET EXTRA ParaFile_adj
        if(evt.getSource() == jbsetadj) {
          // test
          if( nbtrans == 0 ) {
              JOptionPane.showMessageDialog(null,"No transition defined");
              return;
          }
          jfadj.setVisible(false);
          jfadj.setVisible(true);
          testAdj();
          return;
        }
    }

/////////////////////////////////////////////////////////////////////

    /**
     * Manages JPPolyad event.
     */
    public void JPPEvent( Polyad curpol, int type, int indice ) {

        if( ! testBasics() ) {                                       // basics checked ?
            askBasics();                                             // no, ask for
            for(int i=0; i<nbpol; i++ ) {
                if( curpol == polyads[i] ) {
                    jpps[i].resetJP();
                    break;
                }
            }
        }
        else {
            if( nbpol   > 1                &&                        // more than 1 polyad
                curpol == polyads[nbpol-1]    ) {                    // last polyad involved
                if( ! testAllPol( nbpol-1 ) ) {                      // test previous polyad(s)
                    jpps[nbpol-1].resetJP();
                    return;
                }
                switch( type ) {
                    // polyad #
                    case 0 : {
                        if( curpol.isSetPolnb() ) {
                            if( curpol.getPolnb() <= polyads[nbpol-2].getPolnb() ) {  // test increasing order
                                JOptionPane.showMessageDialog(null,"WARNIG"+lnsep+
                                                                   "Polyads have to be defined in polyad number increasing order"+lnsep+
                                                                   "see "+polyads[nbpol-2].getName()+" and "+curpol.getName());
                                jpps[nbpol-1].resetJP();
                            }
                            else {                                   // set default development order
                                int nbdefdvo = polyads[nbpol-2].getPolnb()+1;
                                for( int i=0; i<nbdefdvo; i++ ) {
                                    jpps[nbpol-1].setJcbdvo(polyads[nbpol-2].getVdvo(i),i);
                                }
                            }
                        }
                        break;
                    }
                    // quanta limit
                    case 1 : {
                        if( curpol.getVqal(indice) < polyads[nbpol-2].getVqal(indice) ) {  // test vqal consitency
                            JOptionPane.showMessageDialog(null,"Quanta limit of "+curpol.getName()+lnsep+
                                                               "have to be >= to those of "+polyads[nbpol-2].getName());
                            jpps[nbpol-1].setJcbqal(curpol.getPolnb(),indice);
                        }
                        break;
                    }
                    // developpment order
                    case 2 : {
                        if( curpol.isSetVdvo(indice)                                   &&
                            indice                 <= polyads[nbpol-2].getPolnb()      &&
                            curpol.getVdvo(indice) != polyads[nbpol-2].getVdvo(indice)    ) {  // test vdvo consistency
                            JOptionPane.showMessageDialog(null,"Development order must be consistent"+lnsep+
                                                               "P"+indice+" in "+curpol.getName()+
                                                               " and "+polyads[nbpol-2].getName());
                            jpps[nbpol-1].setJcbdvo(polyads[nbpol-2].getVdvo(indice),indice);
                        }
                        break;
                    }
                }
            }
        }
    }

/////////////////////////////////////////////////////////////////////

    // display polyad in a window
    private void showPol() {

        sbPol();
        JOptionPane.showMessageDialog(null,sb.toString(),"POLYADS",JOptionPane.PLAIN_MESSAGE);
    }

/////////////////////////////////////////////////////////////////////

    // create text
    private void sbPol() {

        sb  = new StringBuffer();
        for(int jcpol=nbpol-1; jcpol>=0; jcpol--) {                  // main polyad decreasing order
            if( polyads[jcpol].isFullydef() ) {
                polyads[jcpol].toSb( sb );
            }
        }
        sb.append(lnsep+lnsep+lnsep);
    }

/////////////////////////////////////////////////////////////////////

    // constraint file creation
    private void createConsf() {

        int pos = 0;
        nconsf = "CL_all_";
        JFileChooser jfcconsf = new JFileChooser(workd);
        jfcconsf.setSize(400,300);
        jfcconsf.setFileSelectionMode(JFileChooser.FILES_ONLY);
        jfcconsf.setDialogTitle("Define the constraint file to be created");
        jfcconsf.setSelectedFile(new File(nconsf));
        int choice = jfcconsf.showSaveDialog(this);
        if (choice == JFileChooser.APPROVE_OPTION) {
            nconsf = jfcconsf.getSelectedFile().getAbsolutePath();   // job file name
        }
        else {
            nconsf = "";
            return;
        }
        // command text
        sbcmd = new StringBuffer();
        sbcmd.append("cd "+playd+fisep+"tempo ; ");
        sbcmd.append(playd+fisep+"packages"+fisep+npack+fisep+"prog"+fisep+"exe"+fisep+"passx xctrmk");
        for(ictrans=0; ictrans<nbtrans; ictrans++ ) {
            if( jrbpara[ictrans].isSelected() ) {
                sbcmd.append(" trans "+ntransp[itransp[ictrans]]+" "+spara[ictrans]);
                if( jrbdvo[ictrans].isSelected() ) {
                    sbcmd.append(" D");
                    for( int j=0; j<trm_nbdvo[ictrans]; j++) {
                        sbcmd.append(""+trm_vdvo[ictrans][j]);
                    }
                    if( jrbpol[ictrans].isSelected() ) {
                        sbcmd.append(" pol");
                    }
                }
            }
        }
        sbcmd.append(" end "+nconsf);
        sbcmd.append(" >xctrmk.out 2>&1");
        ncmd = sbcmd.toString();
        // run command in a shell
        try {
            String[] cmd = {"/bin/sh", "-c", ncmd};
            Process monproc = Runtime.getRuntime().exec(cmd);
        }
        catch (IOException ioe) {
            JOptionPane.showMessageDialog(null,"IO error while running command"+lnsep+
                                               ncmd                            +lnsep+
                                               ioe);
            return;
        }
        //
        JOptionPane.showMessageDialog(null,"The job for constraint file creation has been launched"+lnsep+
                                           "The log file of the job is: "+playd+fisep+"tempo"+fisep+"xctrmk.out");
    }

/////////////////////////////////////////////////////////////////////

    // save jobs
    private void saveCJP() {

        // check if everything is defined
        if( ! testFinal()) {
            return;
        }

        // constraint file creation ?
        if( nconsf.equals("") ) {
            n = JOptionPane.showConfirmDialog(null,"Would you like to create"           +lnsep+
                                                   "the corresponding Constraint file ?"+lnsep,
                                                   "Constraint File Creation",JOptionPane.YES_NO_OPTION);
            if(n == JOptionPane.YES_OPTION) {                        // create
                createConsf();
                lconsf.setText(nconsf);
                pcn.revalidate();
            }
            else {
                JOptionPane.showMessageDialog(null,"Constraint file has to be defined");
                return;
            }
        }
        // choose job_hme name
        JFileChooser jfcjobf = new JFileChooser(workd);
        jfcjobf.setSize(400,300);
        jfcjobf.setFileSelectionMode(JFileChooser.FILES_ONLY);
        jfcjobf.setDialogTitle("Define the hamiltonian matrix elements job file to be created");
        jfcjobf.setSelectedFile(new File("job_hme_"));
        int choice = jfcjobf.showSaveDialog(this);
        if (choice == JFileChooser.APPROVE_OPTION) {
            File f = jfcjobf.getSelectedFile();                      // job file
            njobf  = f.getAbsolutePath();                            // job file name
            njobr  = f.getName();                                    // job file name (reduced)
            if( ! njobr.startsWith("job_hme_") ) {
                JOptionPane.showMessageDialog(null,"The hamiltonian matrix elements job file name must start with"+lnsep+
                                                   "job_hme_");
                return;
            }
        }
        else {                                                       // name NOT chosen
            return;
        }
        // create job_hme
        try {
            out1 = new PrintWriter(new BufferedWriter(new FileWriter(njobf)));  // write job

            out1.println("#! /bin/sh");
            out1.println(" set -v");
            out1.println("##");
            out1.println("## Calculation of the matrix elements (job created by XTDS)");
            out1.print  ("##");
            for(icpol=0; icpol<nbpol; icpol++) {
                if( polisu[icpol] ) {
                    out1.print  (" P"+polyads[icpol].getPolnb());
                }
            }
            out1.println(" of "+nmol+".");
            out1.println("##");
            out1.println("BASD="+playd+fisep+"packages"+fisep+npack);
            out1.println("##");
            out1.println(" SCRD=$BASD"+fisep+"prog"+fisep+"exe");
            out1.println(" PARD=$BASD"+fisep+"para"+fisep+nmol);
            out1.println("##");
            out1.println("## Jmax values.");
            out1.println("##");
            for(icpol=0; icpol<nbpol; icpol++) {                     // polyads
                if( polisu[icpol] ) {
                    out1.print  (" JP"+polyads[icpol].getPolnb()+"=");
                    if( npack.equals("C3vsTDS") ) {
                        out1.println(""+vjpol[icpol]);
                    }
                    else {
                        out1.println(""+(int) vjpol[icpol]);
                    }
                }
            }
            if( ltpf ) {                                             // transitions
                for( int ictransp=0; ictransp<nbtransp; ictransp++) {  // initialize
                    lpol[ictransp] = false;
                    ldip[ictransp] = false;
                }
                for(ictrans=0; ictrans<nbtrans; ictrans++) {         // for each transition
                    if( jrbpara[ictrans].isSelected() ) {            // parameter file present
                        ictransp = itransp[ictrans];                 // index of corresponding possible transition
                        // test if this possible transition is already processed (dip/pol)
                        if( jrbpol[ictrans].isSelected() ) {         // pol
                            if( lpol[ictransp] ) {                   // already processed
                                continue;
                            }
                            lpol[ictransp] = true;
                        }
                        else {
                            if( ldip[ictransp] ) {                   // already processed
                                continue;
                            }
                            ldip[ictransp] = true;
                        }
                        // JP*mP*
                        double cvj;                                      // current value of J
                        sup_polnb = polyads[isup[ictransp]].getPolnb();  // upper polyad number
                        inf_polnb = polyads[iinf[ictransp]].getPolnb();  // lower polyad number
                        cvj       = vjpol[isup[ictransp]];               // default value (Jsup)
// !!! correction INTRA (dipmat) + RAMAN (polmat)
      if( sup_polnb == inf_polnb ) {
          cvj   = Math.max(0.,cvj-1.);                                   // Jsup-1
          if( jrbpol[ictrans].isSelected() ) {
              cvj   = Math.max(0.,cvj-1.);                               // Jsup-2
          }
      }
                        if( npack.equals("C3vsTDS") ) {
                            out1.println(" J"+ntransp[ictransp]+"="+      cvj);  // real
                        }
                        else {
                            out1.println(" J"+ntransp[ictransp]+"="+(int) cvj);  // integer
                        }
                    }
                }
            }
            out1.println("##");
            out1.println("#################################################");
            out1.println("##");
            out1.println("## Hamiltonian matrix elements.");
            out1.println("##");
            boolean lhparchk = true;                                 // parchk for lhpf case has to be done
            for(icpol=0; icpol<nbpol; icpol++) {
                if( polisu[icpol] ) {
                    cur_polnb = polyads[icpol].getPolnb();
                    out1.println("## P"+cur_polnb+" polyad.");
                    out1.println("##");
                    out1.print  (" $SCRD"+fisep+"passx hmodel P"+cur_polnb);
                    polyads[icpol].ecrPol(out1, true);
                    if( npack.equals("D2hTDS") ) {
                        out1.println(" \\"+lnsep+
                                     "                    "+nrep);
                    }
                    else {
                        out1.println("");
                    }
                    // parchk
                    if( ltpf ) {                                           // T (including H) parameter files
                        for( ictrans=nbtrans-1; ictrans>=0; ictrans-- ) {  // transition decreasing order
                            if( isup[itransp[ictrans]] == icpol &&         // current polyad concerned
                                jrbpara[ictrans].isSelected()      ) {     // parameter file present
                                out1.println(" PARA="+spara[ictrans]);
                                out1.print  (" $SCRD"+fisep+"passx parchk P"+cur_polnb+"   ");
                                if(polyads[icpol].getNbvs(cur_polnb) < 10) {
                                    out1.print  (" ");
                                }
                                out1.print  (" D");
                                for(icpols=0; icpols<cur_polnb+1; icpols++) {
                                    out1.print  (polyads[icpol].getVdvo(icpols));
                                }
                                out1.println(" $PARD $PARA");
                                break;                                     // done for this polyad
                            }
                        }
                    }
                    else {                                                      // H parameter file
                        if( lhparchk ) {                                        // parchk for lhpf case has to be done
                            for( ictrans=0; ictrans<nbtrans; ictrans++ ) {      // search for the parameter file
                                if( jrbpara[ictrans].isSelected() ) {           // parameter file present
                                    if( isup[itransp[ictrans]] == icpol ) {     // upper polyad of current transition = current polyad
                                        out1.println(" PARA="+spara[ictrans]);
                                        out1.print  (" $SCRD"+fisep+"passx parchk P"+cur_polnb+"   ");
                                        if(polyads[icpol].getNbvs(cur_polnb) < 10) {
                                            out1.print  (" ");
                                        }
                                        out1.print  (" D");
                                        for(icpols=0; icpols<cur_polnb+1; icpols++) {
                                            out1.print  (polyads[icpol].getVdvo(icpols));
                                        }
                                        out1.println(" $PARD $PARA");
                                        lhparchk = false;                    // done, only once
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    //
                    out1.print  (" $SCRD"+fisep+"passx rovbas P"+cur_polnb+" N"+polyads[icpol].getNbvs(cur_polnb));
                    if(polyads[icpol].getNbvs(cur_polnb) < 10) {
                        out1.print  (" ");
                    }
                    out1.print  (" D");
                    for(icpols=0; icpols<cur_polnb+1; icpols++) {
                        out1.print  (polyads[icpol].getVdvo(icpols));
                    }
                    out1.println(" $JP"+cur_polnb);
                    out1.print  (" $SCRD"+fisep+"passx hmatri P"+cur_polnb+" N"+polyads[icpol].getNbvs(cur_polnb));
                    if(polyads[icpol].getNbvs(cur_polnb) < 10) {
                        out1.print  (" ");
                    }
                    out1.print  (" D");
                    for(icpols=0; icpols<cur_polnb+1; icpols++) {
                        out1.print  (polyads[icpol].getVdvo(icpols));
                    }
                    out1.println(" $JP"+cur_polnb);
                    out1.println("##");
                }
            }
            if( ltpf ) {
                for( int ictransp=0; ictransp<nbtransp; ictransp++) {  // initialize
                    lpol[ictransp] = false;
                    ldip[ictransp] = false;
                }
                out1.println("#################################################");
                out1.println("##");
                out1.println("## Transition moment matrix elements.");
                out1.println("##");
                for(ictrans=0; ictrans<nbtrans; ictrans++) {         // for each transition
                    if( jrbpara[ictrans].isSelected() ) {            // parameter file present
                        int ictransp = itransp[ictrans];            // index of corresponding possible transition
                        // test if this possible transition is already processed (dip/pol)
                        if( jrbpol[ictrans].isSelected() ) {         // pol
                            if( lpol[ictransp] ) {                  // already processed
                                continue;
                            }
                            lpol[ictransp] = true;
                        }
                        else {
                            if( ldip[ictransp] ) {                  // already processed
                                continue;
                            }
                            ldip[ictransp] = true;
                        }
                        out1.println("## "+ntrans[ictrans]+" transition.");
                        out1.println("##");
                        if( jrbpol[ictrans].isSelected() ) {
                            out1.println("## Polarizability matrix elements.");
                            out1.println("##");
                            out1.print  (" $SCRD"+fisep+"passx polmod");
                        }
                        else {
                            out1.println("## Dipole Moment matrix elements.");
                            out1.println("##");
                            out1.print  (" $SCRD"+fisep+"passx dipmod");
                        }
                        sup_polnb = polyads[isup[ictransp]].getPolnb();
                        out1.print  (" P"+sup_polnb);
                        polyads[isup[ictransp]].ecrPol(out1, false);
                        out1.println(" \\");
                        out1.print  ("                   ");
                        inf_polnb = polyads[iinf[ictransp]].getPolnb();
                        out1.print  (" P"+inf_polnb);
                        polyads[iinf[ictransp]].ecrPol(out1, false);
                        out1.println(" \\");
                        out1.print  ("                   ");
                        out1.print  (" D");
                        for( int j=0; j<transp_nbdvo[ictransp]; j++) {
                            out1.print  (transp_vdvo[ictransp][j]);
                        }
                        if( npack.equals("D2hTDS") ) {
                            out1.println(" \\"+lnsep+
                                         "                    "+nrep);
                        }
                        else {
                            out1.println("");
                        }
                        // parchk
                        out1.println(" PARA="+spara[ictrans]);
                        out1.print  (" $SCRD"+fisep+"passx parchk");
                        out1.print  (" P"+sup_polnb);
                        out1.print  ("   ");
                        if(polyads[isup[ictransp]].getNbvs(sup_polnb) < 10) {
                            out1.print  (" ");
                        }
                        out1.print  (" D");
                        for(icpols=0; icpols<sup_polnb+1; icpols++) {
                            out1.print  (polyads[isup[ictransp]].getVdvo(icpols));
                        }
                        out1.print  (" P"+inf_polnb);
                        out1.print  ("   ");
                        if(polyads[iinf[ictransp]].getNbvs(inf_polnb) < 10) {
                            out1.print  (" ");
                        }
                        out1.print  (" D");
                        for( int j=0; j<transp_nbdvo[ictransp]; j++) {
                            out1.print  (transp_vdvo[ictransp][j]);
                        }
                        if( jrbpol[ictrans].isSelected() ) {
                            out1.print  (" pol");
                        }
                        else {
                            out1.print  (" dip");
                        }
                        out1.println(" $PARD $PARA");
                        //
                        if( jrbpol[ictrans].isSelected() ) {
                            out1.print  (" $SCRD"+fisep+"passx polmat");
                        }
                        else {
                            out1.print  (" $SCRD"+fisep+"passx dipmat");
                        }
                        out1.print  (" P"+sup_polnb+" N"+polyads[isup[ictransp]].getNbvs(sup_polnb));
                        if(polyads[isup[ictransp]].getNbvs(sup_polnb) < 10) {
                            out1.print  (" ");
                        }
                        for(icpols=0; icpols<sup_polnb+3; icpols++) {
                            out1.print  (" ");
                        }
                        out1.print  (" P"+inf_polnb+" N"+polyads[iinf[ictransp]].getNbvs(inf_polnb));
                        if(polyads[iinf[ictransp]].getNbvs(inf_polnb) < 10) {
                            out1.print  (" ");
                        }
                        out1.print  (" D");
                        for( int j=0; j<transp_nbdvo[ictransp]; j++) {
                            out1.print  (transp_vdvo[ictransp][j]);
                        }
                        out1.println(" $J"+ntransp[ictransp]);
                        out1.println("##");
                    }
                }
                for(ictransp=0; ictransp<nbtransp; ictransp++) {
                  if( lpol[ictransp] ) {
                      out1.println(" \\rm MP_*");
                      break;
                  }
                }
                for(ictransp=0; ictransp<nbtransp; ictransp++) {
                  if( ldip[ictransp] ) {
                      out1.println(" \\rm MD_*");
                      break;
                  }
                }
            }
            // all* options
            if( jrballcal.isSelected() ||
                jrballdip.isSelected() ||
                jrballpol.isSelected()    ) {
                for( int ictransp=0; ictransp<nbtransp; ictransp++) {  // initialize
                    lpol[ictransp] = false;
                    ldip[ictransp] = false;
                }
                out1.println("#################################################");
                out1.println("##");
                out1.println("## Extras files for xpafit all* options.");
                out1.println("##");
                for(ictrans=0; ictrans<nbtrans; ictrans++) {         // for each transition
                    boolean lpolc = false;                           // pol files required
                    boolean ldipc = false;                           // dip files required
                    if( (jrballcal.isSelected()       &&
                         jrbHfit[ictrans].isSelected()  )        ||
                        (jrballdip.isSelected()              &&
                         ! (jrbTfit[ictrans].isSelected() &&
                            ! jrbpol[ictrans].isSelected()  )  )    ) {
                        ldipc = true;                                // dip
                    }
                    if( jrballpol.isSelected()               &&
                        ! (jrbTfit[ictrans].isSelected() &&
                           jrbpol[ictrans].isSelected()    )    ) {
                        lpolc = true;                                // pol
                    }
                    int ictransp = itransp[ictrans];                 // index of corresponding possible transition
                    // JP*mP*
                    double cvj;                                      // current value of J
                    double cvjdip;                                   // current value of J dip
                    double cvjpol;                                   // current value of J pol
                    sup_polnb = polyads[isup[ictransp]].getPolnb();  // upper polyad number
                    inf_polnb = polyads[iinf[ictransp]].getPolnb();  // lower polyad number
                    cvj       = vjpol[isup[ictransp]];               // default value (Jsup)
                    if( sup_polnb == inf_polnb ) {
                        cvjdip = cvj;                                // Jsup
                        cvjpol = cvj;                                // Jsup
// !!! correction INTRA (dipmat) + RAMAN (polmat)
  cvjdip = Math.max(0.,cvj-1.);                                      //Jsup-1
  cvjpol = Math.max(0.,cvj-2.);                                      //Jsup-2
                    }
                    else {
                        cvjdip = Math.max(0.,vjpol[iinf[ictransp]]-1.);   // dip -> Jinf-1
                        cvjdip = Math.min(cvj,cvjdip);                    // min(Jsup,Jinf-1)
                        cvjpol = Math.max(0.,vjpol[iinf[ictransp]]-2.);   // pol -> Jinf-2
                        cvjpol = Math.min(cvj,cvjpol);                    // min(Jsup,Jinf-2)
                    }

                    // test if this possible transition is already processed (pol)
                    if( lpolc            &&                          // pol
                        ! lpol[ictransp]    ) {                      // create files
                        lpol[ictransp] = true;
                        out1.println("## "+ntransp[ictransp]+" Polarizability matrix elements.");
                        out1.println("##");
                        if( npack.equals("C3vsTDS") ) {
                            out1.println(" J"+ntransp[ictransp]+"_pol="+      cvjpol);  // real
                        }
                        else {
                            out1.println(" J"+ntransp[ictransp]+"_pol="+(int) cvjpol);  // integer
                        }
                        out1.print  (" $SCRD"+fisep+"passx polmod");
                        out1.print  (" P"+sup_polnb);
                        polyads[isup[ictransp]].ecrPol(out1, false);
                        out1.println(" \\");
                        out1.print  ("                   ");
                        out1.print  (" P"+inf_polnb);
                        polyads[iinf[ictransp]].ecrPol(out1, false);
                        out1.println(" \\");
                        out1.print  ("                   ");
                        out1.print  (" D");
                        for( int j=0; j<transp_nbdvo[ictransp]; j++) {
                            out1.print  (transp_vdvo[ictransp][j]);
                        }
                        if( npack.equals("D2hTDS") ) {
                            out1.println(" \\"+lnsep+
                                         "                    "+nrep);
                        }
                        else {
                            out1.println("");
                        }
                        out1.print  (" $SCRD"+fisep+"passx polmat");
                        out1.print  (" P"+sup_polnb+" N"+polyads[isup[ictransp]].getNbvs(sup_polnb));
                        if(polyads[isup[ictransp]].getNbvs(sup_polnb) < 10) {
                            out1.print  (" ");
                        }
                        for(icpols=0; icpols<sup_polnb+3; icpols++) {
                            out1.print  (" ");
                        }
                        out1.print  (" P"+inf_polnb+" N"+polyads[iinf[ictransp]].getNbvs(inf_polnb));
                        if(polyads[iinf[ictransp]].getNbvs(inf_polnb) < 10) {
                            out1.print  (" ");
                        }
                        out1.print  (" D");
                        for( int j=0; j<transp_nbdvo[ictransp]; j++) {
                            out1.print  (transp_vdvo[ictransp][j]);
                        }
                        out1.println(" $J"+ntransp[ictransp]+"_pol");
                        out1.print  (" \\cp PO_"+ntransp[ictransp]+"_D");
                        for( int j=0; j<transp_nbdvo[ictransp]; j++) {
                            out1.print  (transp_vdvo[ictransp][j]);
                        }
                        out1.println("_ PO_"+ntransp[ictransp]+"_D-_");
                        out1.println("##");
                    }
                    // test if this possible transition is already processed (dip)
                    if( ldipc            &&                          // dip
                        ! ldip[ictransp]    ) {                      // create files
                        ldip[ictransp] = true;
                        out1.println("## "+ntransp[ictransp]+" Dipole Moment matrix elements.");
                        out1.println("##");
                        if( npack.equals("C3vsTDS") ) {
                            out1.println(" J"+ntransp[ictransp]+"_dip="+      cvjdip);  // real
                        }
                        else {
                            out1.println(" J"+ntransp[ictransp]+"_dip="+(int) cvjdip);  // integer
                        }
                        out1.print  (" $SCRD"+fisep+"passx dipmod");
                        out1.print  (" P"+sup_polnb);
                        polyads[isup[ictransp]].ecrPol(out1, false);
                        out1.println(" \\");
                        out1.print  ("                   ");
                        out1.print  (" P"+inf_polnb);
                        polyads[iinf[ictransp]].ecrPol(out1, false);
                        out1.println(" \\");
                        out1.print  ("                   ");
                        out1.print  (" D");
                        for( int j=0; j<transp_nbdvo[ictransp]; j++) {
                            out1.print  (transp_vdvo[ictransp][j]);
                        }
                        if( npack.equals("D2hTDS") ) {
                            out1.println(" \\"+lnsep+
                                         "                    "+nrep);
                        }
                        else {
                            out1.println("");
                        }
                        out1.print  (" $SCRD"+fisep+"passx dipmat");
                        out1.print  (" P"+sup_polnb+" N"+polyads[isup[ictransp]].getNbvs(sup_polnb));
                        if(polyads[isup[ictransp]].getNbvs(sup_polnb) < 10) {
                            out1.print  (" ");
                        }
                        for(icpols=0; icpols<sup_polnb+3; icpols++) {
                            out1.print  (" ");
                        }
                        out1.print  (" P"+inf_polnb+" N"+polyads[iinf[ictransp]].getNbvs(inf_polnb));
                        if(polyads[iinf[ictransp]].getNbvs(inf_polnb) < 10) {
                            out1.print  (" ");
                        }
                        out1.print  (" D");
                        for( int j=0; j<transp_nbdvo[ictransp]; j++) {
                            out1.print  (transp_vdvo[ictransp][j]);
                        }
                        out1.println(" $J"+ntransp[ictransp]+"_dip");
                        out1.print  (" \\cp DI_"+ntransp[ictransp]+"_D");
                        for( int j=0; j<transp_nbdvo[ictransp]; j++) {
                            out1.print  (transp_vdvo[ictransp][j]);
                        }
                        out1.println("_ DI_"+ntransp[ictransp]+"_D-_");
                        out1.println("##");
                    }
                }
                for(ictransp=0; ictransp<nbtransp; ictransp++) {
                  if( lpol[ictransp]         ||
                      jrballpol.isSelected()    ) {
                      out1.println(" \\rm MP_*");
                      break;
                  }
                }
                for(ictransp=0; ictransp<nbtransp; ictransp++) {
                  if( ldip[ictransp]         ||
                      jrballdip.isSelected()    ) {
                      out1.println(" \\rm MD_*");
                      break;
                  }
                }
            }
            out1.println(" \\rm FN_* MH_*");
            out1.println("");
        }
        catch (IOException ioe) {                                    // IO error
            JOptionPane.showMessageDialog(null,"IO error while writing file"+lnsep+
                                               njobf                        +lnsep+
                                               ioe);
            return;
        }
        finally {
            if( out1 != null ) {
                out1.close();
                if( out1.checkError() ) {
                    JOptionPane.showMessageDialog(null,"PrintWriter error while creating file"+lnsep+
                                                       njobf);
                    return;
                }
            }
        }
        JOptionPane.showMessageDialog(null,"The job "+njobf+" has been created.");
        try {
            Process monproc = Runtime.getRuntime().exec("chmod u+x "+njobf);  // allow execution
        }
        catch (IOException ioe) {                                    // rights modification error
                JOptionPane.showMessageDialog(null,"IO error while changing rights of file"+lnsep+
                                                   njobf                                   +lnsep+
                                                   ioe);
                return;
        }

        // choose job_xfit name
        f = new File(njobf);
        njobr = "job_xfit_"+njobr.substring(8);                      // job_hme_ -> job_xfit_
        njobf = f.getParent()+fisep+njobr;
        // create job_xfit
        try {
            out1 = new PrintWriter(new BufferedWriter(new FileWriter(njobf)));  // write job

            out1.println("#! /bin/sh");
            out1.println(" set -v");
            out1.println("##");
            if( ltpf ) {
                out1.print  ("## Frequency and Intensity");
            }
            else {
                out1.print  ("## Frequency");
            }
            out1.println(" parameter fitting (job created by XTDS)");
            out1.print  ("##");
            for(ictransp=0; ictransp<nbtransp; ictransp++ ) {
                if( transpisu[ictransp] ) {
                    out1.print  (" "+ntransp[ictransp]);
                }
            }
            out1.println(" of "+nmol+".");
            out1.println("##");
            out1.println("BASD="+playd+fisep+"packages"+fisep+npack);
            out1.println("##");
            out1.println(" SCRD=$BASD"+fisep+"prog"+fisep+"exe");
            out1.println("##");
            out1.println("## Jmax values.");
            out1.println("##");
            for(icpol=0; icpol<nbpol; icpol++) {
                if( polisu[icpol] ) {
                    out1.print  (" JP"+polyads[icpol].getPolnb()+"=");
                    if( npack.equals("C3vsTDS") ) {
                        out1.println(""+vjpol[icpol]);
                    }
                    else {
                        out1.println(""+(int) vjpol[icpol]);
                    }
                }
            }
            out1.println("##");
            out1.println("## Assignment files.");
            out1.println("##");
            int nbxasgc;                                             // current nb of extractions
            int nbxasgt = 0;                                         // total   nb of extractions
            for(ictrans=0; ictrans<nbtrans; ictrans++) {
                nbxasgt = nbxasgt+jfxasg[ictrans].getNbXasg();
            }
            String[] asgf = new String[nbxasgt];                     // names of assignment files
            nbxasgt = 0;                                             // current asg file index
            for(ictrans=0; ictrans<nbtrans; ictrans++) {
                nbxasgc = jfxasg[ictrans].getNbXasg();
                for(int ixasgc=0; ixasgc<nbxasgc; ixasgc++) {
                    asgf[nbxasgt] = "ASG_"+ntrans[ictrans]+"_"+(ixasgc+1);
                    String nasgfc = jfxasg[ictrans].getJPXasg(ixasgc).getASGfile();
                    f = new File(nasgfc);
                    if( f.isDirectory() ) {
                        out1.println(" "+asgf[nbxasgt]+"=`ls "+nasgfc+fisep+"*`");
                    }
                    else {
                        out1.println(" "+asgf[nbxasgt]+"="+nasgfc);
                    }
                    nbxasgt ++;
                }
            }
            out1.println("##");
            out1.println("## Parameter constraint file.");
            out1.println("##");
            out1.println(" CLF="+nconsf);
            out1.println("##");
            out1.println("#################################################");
            out1.println("##");
            out1.println("## Fit.");
            nbxasgt = 0;                                             // current asg file index
            for(ictrans=0; ictrans<nbtrans; ictrans++) {
                out1.println("##");
                out1.println("## "+ntrans[ictrans]);
                out1.println("##");
                nbxasgc = jfxasg[ictrans].getNbXasg();
                for(int ixasgc=0; ixasgc<nbxasgc; ixasgc++) {
                    JPXasg jpxasgc = jfxasg[ictrans].getJPXasg(ixasgc);  // current JPXasg
                    out1.print  (" $SCRD"+fisep+"xasg '"+jpxasgc.getSelstr()+"' \"$"+asgf[nbxasgt]+"\"");
                    out1.print  (" P"+polyads[isup[itransp[ictrans]]].getPolnb());
                    out1.print  (" P"+polyads[iinf[itransp[ictrans]]].getPolnb());
                    if( jpxasgc.isMHzSelected() ) {
                        out1.print  (" MHz");
                    }
                    else {
                        if( jpxasgc.isGHzSelected() ) {
                            out1.print  (" GHz");
                        }
                        else {
                            out1.print  ("    ");
                        }
                    }
                    if( jpxasgc.isTempSelected() ) {
                        out1.print  (" "+jpxasgc.getTemp());
                        if( jrbpol[ictrans].isSelected() ) {
                            out1.print  (" pol");
                            if( jrbpolst[ictrans].isSelected() ) {
                                out1.print  (" "+jcbpolst[ictrans].getSelectedItem());
                            }
                        }
                        if( jpxasgc.isFpvibSelected() ) {
                            out1.print  (" fpvib "+jpxasgc.getFpvib());
                        }
                        if( jpxasgc.isAbundSelected() ) {
                            out1.print  (" abund "+jpxasgc.getAbund());
                        }
                    }
                    out1.println("");
                    out1.println(" \\mv ASG_EXP "+asgf[nbxasgt]+".t");
                    nbxasgt ++;
                }
            }
            out1.println("##");
            out1.print  (" $SCRD"+fisep+"passx xtrias ");
            for(nbxasgc=0; nbxasgc<nbxasgt; nbxasgc++) {
                if( nbxasgc != 0 ) {
                    out1.print  ("                    ");
                }
                out1.print  (asgf[nbxasgc]+".t");
                if( nbxasgc != (nbxasgt-1) ) {
                    out1.println(" \\");
                }
                else {
                    out1.println("");
                }
            }
            out1.print  (" \\cp ASG_EXP ASG");
            for(ictransp=0; ictransp<nbtransp; ictransp++) {
                if( transpisu[ictransp] ) {
                    out1.print  ("_"+ntransp[ictransp]);
                }
            }
            out1.println(".t");
            out1.println(" \\mv ASG_EXP assignments.t");
            out1.println("##");
            out1.print  (" $SCRD"+fisep+"passx xpafit ");
            // polyads
            for(int icpol=0; icpol<nbpol; icpol++) {
                cur_polnb = polyads[icpol].getPolnb();
                if( icpol != 0 ) {
                    out1.print  ("                    ");
                }
                out1.print  ("polyad P"+cur_polnb+" N"+polyads[icpol].getNbvs(cur_polnb));
                if(polyads[icpol].getNbvs(cur_polnb) < 10) {
                    out1.print  (" ");
                }
                out1.print  (" D");
                for(icpols=0; icpols<cur_polnb+1; icpols++) {
                    out1.print  (polyads[icpol].getVdvo(icpols));
                }
                out1.println(" $JP"+cur_polnb+" \\");
            }
            // transitions
            for(int ictrans=0; ictrans<nbtrans; ictrans++) {
                out1.print  ("                    ");
                out1.print  ("trans  "+ntransp[itransp[ictrans]]);
                if( jrbpara[ictrans].isSelected() ) {
                    out1.print  (" "+spara[ictrans]);
                    if( jrbTfit[ictrans].isSelected() ) {
                        out1.print  (" D");
                        for( int j=0; j<trm_nbdvo[ictrans]; j++) {
                            out1.print  (""+trm_vdvo[ictrans][j]);
                        }
                        if( jrbpol[ictrans].isSelected() ) {
                            out1.print  (" pol");
                            if( jrbpolst[ictrans].isSelected() ) {
                                out1.print  (" "+jcbpolst[ictrans].getSelectedItem());
                            }
                        }
                    }
                }
                out1.println(" \\");
            }
            // adj
            if( nbadj != 0 ) {
                for(int iadj=0; iadj<nbadj; iadj++ ) {               // for each extra ParaFile_adj
                    JPAdj jpadj = jfadj.getJPAdj(iadj);
                    String ntradj = jpadj.getNtradj();
                    out1.print  ("                    ");
                    out1.print  ("adj    "+jpadj.getNtradj()+" "+jpadj.getParaFile());
                    if( jpadj.isPolSelected() ) {
                        out1.print  (" pol");
                    }
                    out1.println(" \\");
                }
            }
            // end
            out1.print  ("                    ");
            out1.print  ("end    $CLF");
            if( jrblpcmax.isSelected() ) {
                out1.print  (" mix "+jcblpcmax.getSelectedItem());
            }
            if( jrbrmxomc.isSelected() ) {
                out1.print  (" rmxomc "+jcbrmxomc.getSelectedItem());
            }
            if( jrballcal.isSelected() &&
                jrballcal.isEnabled()     ) {
                out1.print  (" allcal");
            }
            if( jrballdip.isSelected()    ) {
                out1.print  (" alldip");
            }
            if( jrballpol.isSelected()    ) {
                out1.print  (" allpol");
            }
            //out1.print  (" mxiter 0");
            out1.println("");
            out1.println("##");
            if( ltpf ) {
                out1.println(" \\rm XTRM_* XTRO_* XVP_*");
            }
            else {
                if( jrballcal.isSelected() ||
                    jrballdip.isSelected() ||
                    jrballpol.isSelected()    ) {
                    out1.println(" \\rm XTRM_*        XVP_*");
                }
            }
            out1.println(" \\rm XED_*  XEN_*");
            out1.println("");
        }
        catch (IOException ioe) {                                    // IO error
            JOptionPane.showMessageDialog(null,"IO error while writing file"+lnsep+
                                               njobf                        +lnsep+
                                               ioe);
            return;
        }
        finally {
            if( out1 != null ) {
                out1.close();
                if( out1.checkError() ) {
                    JOptionPane.showMessageDialog(null,"PrintWriter error while creating file"+lnsep+
                                                       njobf);
                    return;
                }
            }
        }
        JOptionPane.showMessageDialog(null,"The job "+njobf+" has been created.");
        try {
            Process monproc = Runtime.getRuntime().exec("chmod u+x "+njobf);  // allow execution
        }
        catch (IOException ioe) {                                    // rights modification error
            JOptionPane.showMessageDialog(null,"IO error while changing rights of file"+lnsep+
                                               njobf                                   +lnsep+
                                               ioe);
            return;
        }
    }

/////////////////////////////////////////////////////////////////////

    // check Basics panel
    private boolean testBasics() {

        if(nmol == "") {
            return false;                                            // molecule
        }
        if( npack.equals("D2hTDS") ) {
            nrep = (String) jcbrep.getSelectedItem();
            if(nrep == "") {
                return false;                                        // representation
            }
        }
        if( ! testPsc()) {
            return false;                                            // psc
        }
        return true;
    }

/////////////////////////////////////////////////////////////////////

    // ask to fully define Basics
    private void askBasics() {

        JOptionPane.showMessageDialog(null,"You have to first fully define BASICS specifications");
        return;
    }

/////////////////////////////////////////////////////////////////////

    // check polyad scheme coefficients
    private boolean testPsc() {

        for (int i=0; i<nbvqn; i++) {
            npsc = (String) jcbpsc[i].getSelectedItem();
            if (npsc == "") {
                return false;
            }
        }
        return true;
    }

/////////////////////////////////////////////////////////////////////

    // check all polyads
    private boolean testAllPol( int cnbpol ) {

        // something to check ?
        if(nbpol == 0) {
            JOptionPane.showMessageDialog(null,"No polyad defined");
            return false;
        }
        // all fully defined ?
        for(icpol=0; icpol<cnbpol; icpol++) {
            if( ! polyads[icpol].isFullydef() ) {
                JOptionPane.showMessageDialog(null,"You have to first fully define "+polyads[icpol].getName());
                return false;
            }
            polyads[icpol].calCvs(vpsc);
        }
        // each with a strictly increasing polyad number ?
        if(cnbpol > 1) {
            for(icpol=0; icpol<cnbpol-1; icpol++) {
                if(polyads[icpol].getPolnb() >= polyads[icpol+1].getPolnb()) {
                    JOptionPane.showMessageDialog(null,"Polyads have to be defined in polyad number increasing order"+lnsep+
                                                       "see "+polyads[icpol].getName()+" and "+polyads[icpol+1].getName());
                    return false;
                }
            }
        }
        // development orders consistent ?
        if(cnbpol > 1) {
            // set higher polyad
            icpolmax = cnbpol-1;
            polnbmax = polyads[icpolmax].getPolnb();
            // test dvo
            for(int i=0; i<polnbmax+1; i++) {                        // for each dvo of the higher polyad
                int cdvo = polyads[icpolmax].getVdvo(i);             // referent dvo
                for(icpol=0; icpol<cnbpol; icpol++) {                // for each polyad
                    if(i <= polyads[icpol].getPolnb()) {             // for each dvo of the checked polyad
                        if( cdvo != polyads[icpol].getVdvo(i) ) {    // compare dvo
                            JOptionPane.showMessageDialog(null,"Development order must be consistent"+lnsep+
                                                               "P"+i+" in "+polyads[icpol].getName()+
                                                               " and "+polyads[icpolmax].getName());
                            return false;
                        }
                    }
                }
            }
        }
        // test if there is at least one vs per sub-polyad
        for(icpol=cnbpol-1; icpol>=0; icpol--) {                     // main polyad decreasing order
            cur_polnb = polyads[icpol].getPolnb();
            for(icpols=0; icpols<=cur_polnb; icpols++) {
                if( polyads[icpol].getNbvs(icpols) == 0 ) {
                    sbPol();
                    sb.append("WARNING : your choice produces NO vibrational state for P"+icpols+" of "+polyads[icpol].getName());
                    JOptionPane.showMessageDialog(null,sb.toString());
                }
            }
        }
        if( nbpol > 1 ) {

            // test if quanta limit are consistent between polyads
            int vqalp[] = new int[nbvqn];                            // previous vqal(s)
            int vqalc;                                               // current vqal
            for(int i=0; i<nbvqn; i++) {                             // initialize
                vqalp[i] = polyads[0].getVqal(i);                    // vqal of the lowest main polyad
            }
            for(icpol=1; icpol<cnbpol; icpol++) {                    // for each main polyad except the 1st (lowest) one
                for ( int i=0; i<nbvqn; i++) {                       // for each vqn
                    vqalc = polyads[icpol].getVqal(i);
                    if( vqalc < vqalp[i] ) {                         // not allowed
                        JOptionPane.showMessageDialog(null,"Quanta limit of "+polyads[icpol].getName()+lnsep+
                                                           "have to be >= to those of "+polyads[icpol-1].getName());
                        return false;
                    }
                    vqalp[i] = vqalc;
                }
            }
        }
        // happy end
        return true;
    }

/////////////////////////////////////////////////////////////////////

    // check final definition of all transitions
    private boolean testFinal() {

        // 1st level test
        if( ! testAllTrans() ) {
            return false;
        }
        // ParaFile_adj
        if( ! testAdj() ) {
            return false;
        }
        // H/T type
        int nbhpf = 0;                                               // nb of H param. file
        lhpf = false;                                                // presence of H param. file
        ltpf = false;                                                // presence of T param. file
        for(ictrans=0; ictrans<nbtrans; ictrans++) {
            if( jrbNfit[ictrans].isSelected() ) {                    // transition without para. file
                continue;
            }
            if( jrbTfit[ictrans].isSelected() ) {                    // T type
                ltpf = true;
            }
            else {
                lhpf = true;                                         // H type
                nbhpf ++;
            }
            // H and T
            if( lhpf && ltpf ) {                                     // not allowed
                JOptionPane.showMessageDialog(null,"All parameter files must be of the same type:"+lnsep+
                                                   "H or T");
                return false;
            }
        }
        // not H and not T
        if( (! lhpf) &&
            (! ltpf)    ) {                                          // not allowed
            JOptionPane.showMessageDialog(null,"NO parameter file defined for transition(s)");
            return false;
        }
        // H and more than 1 file
        if( lhpf       &&
            nbhpf != 1    ) {                                        // not allowed
            JOptionPane.showMessageDialog(null,"A single H parameter file is required");
            return false;
        }
        // H and duplicated PimPj
        if( nbtrans > 1 ) {
            for( ictrans=0; ictrans<nbtrans-1; ictrans++ ) {
                for( int lctrans=ictrans+1; lctrans<nbtrans; lctrans++ ) {
                    if( itransp[ictrans] == itransp[lctrans] ) {         // not allowed
                        if( lhpf ) {
                            JOptionPane.showMessageDialog(null,"Frequency fit"                                        +lnsep+
                                                               ntransp[itransp[ictrans]]+" must be used only one time"+lnsep+
                                                               "see "+ntrans[lctrans]+" and "+ntrans[ictrans]);
                            return false;
                        }
                    }
                }
            }
        }
        // T, check transition types
        // see xpafit : SET TYPE OF EACH TRANSITION
        if( ltpf ) {
            int[] ktrt;
            ktrt  = new int[nbtrans];
            int[] intrt;
            intrt = new int[nbtrans];
            int   ktrtc;
            int   ntrt;
            int   idelc;
            int   irac;
            int   itr;
            // SET TYPE OF EACH TRANSITION
            // A TYPE IS UNIQUE AND DEPENDS ON DELTA AND ISRAM
            // DELTA = i-j FOR PimPj TRANSITION
            // ISRAM = RAMAN (0) OR NOT (1)
            //
            // TYPE IS 0 FOR H, NOT NULL (1,NTRT) FOR Ts
            for( itr=0; itr<nbtrans; itr++ ) {
                if( jrbNfit[itr].isSelected() ) {
                    ktrt[itr]  = -1;
                    intrt[itr] =  0;
                }
                else {
                    idelc = polyads[isup[itransp[itr]]].getPolnb()-polyads[iinf[itransp[itr]]].getPolnb();
                    if( jrbpol[itr].isSelected() ) {
                        irac = 0;
                    }
                    else {
                        irac = 1;
                    }
                    ktrt[itr] = idelc*10+irac;
                }
            }
            // SET THE NUMBER OF DIFFERENT TYPES
            // AND SET THE UNIQUE TYPE # FOR EACH TRANSITION
            ntrt = 0;
            for( int idelta=0; idelta<mxpol; idelta++ ) {
                for( irac=0; irac<2; irac++ ) {
                    ktrtc = idelta*10+irac;
                    for( itr=0; itr<nbtrans; itr++ ) {
                        if( ktrtc == ktrt[itr] ) {
                            ntrt ++;
                            for( int ltr=0; ltr<nbtrans; ltr++ ) {
                                if( ktrtc == ktrt[ltr] ) {
                                    intrt[ltr] = ntrt;
                                }
                            }
                            break;
                        }
                    }
                }
            }
            // SET (check) INTRT for fpara_adj
            if( nbadj != 0 ) {
                for(int iadj=0; iadj<nbadj; iadj++ ) {               // for each extra ParaFile_adj
                    JPAdj jpadj = jfadj.getJPAdj(iadj);
                    String ntradj = jpadj.getNtradj();
                    // get ictransp
                    for( ictransp=0; ictransp<nbtransp; ictransp++) {
                        if( ntransp[ictransp].equals(ntradj) ) {
                            break;
                        }
                    }
                    // set transition type
                    idelc = polyads[isup[ictransp]].getPolnb()-polyads[iinf[ictransp]].getPolnb();
                    if( jpadj.isPolSelected() ) {
                        irac = 0;
                    }
                    else {
                        irac = 1;
                    }
                    ktrtc = idelc*10+irac;
                    // check if it is transition defined
                    boolean bfound = false;
                    for( ictrans=0; ictrans<nbtrans; ictrans++) {
                        if( ktrtc == ktrt[ictrans] ) {
                            bfound = true;
                            break;
                        }
                    }
                    if( ! bfound ) {                                 // NOT found
                        JOptionPane.showMessageDialog(null,"WARNING : Extra ParaFile_adj "+ntradj+" TRANSITION TYPE NOT DEFINED THROUGH transitions"+lnsep+
                                                           "ONLY H PARAMETERS WILL BE UPDATED IN "+jpadj.getParaFile());
                    }
                }
            }
        }
        // at least one finalized extraction per transition
        for( ictrans=0; ictrans<nbtrans; ictrans++ ) {
            if( jfxasg[ictrans].getNbXasg() ==0 ) {
                JOptionPane.showMessageDialog(null,"You have to define at least one extraction for "+ntrans[ictrans]);
                return false;
            }
        }
        // mix
        if( jrblpcmax.isSelected() ) {
            if( jcblpcmax.getSelectedItem() == "" ) {
                JOptionPane.showMessageDialog(null,"You have to complete Initial Basis Components");
                return false;
            }
        }
        // rmxomc
        if( jrbrmxomc.isSelected() ) {
            if( jcbrmxomc.getSelectedItem() == "" ) {
                JOptionPane.showMessageDialog(null,"You have to complete Intensity Relative Threshold");
                return false;
            }
        }

        return true;
    }

/////////////////////////////////////////////////////////////////////

    // check (partially) definition of all transitions
    private boolean testAllTrans() {

        if( nbtrans == 0 ) {
            JOptionPane.showMessageDialog(null,"You have to select at least one transition");
            return false;
        }
        for(ictrans=0; ictrans<nbtrans; ictrans++) {
            if( ! testTrans(ictrans) ) {
                return false;
            }
        }
        // check trm_vdvo consistency
        if( nbtrans > 1) {
            for(ictrans=0; ictrans<nbtrans-1; ictrans++) {
                if( ! jrbdvo[ictrans].isSelected() ) {
                    continue;
                }
                cxmy=polyads[isup[itransp[ictrans]]].getPolnb()-polyads[iinf[itransp[ictrans]]].getPolnb();
                for( int lctrans=ictrans+1; lctrans<nbtrans; lctrans++) {
                    if( ! jrbdvo[lctrans].isSelected() ) {           // not T type
                        continue;
                    }
                    xmy=polyads[isup[itransp[lctrans]]].getPolnb()-polyads[iinf[itransp[lctrans]]].getPolnb();
                    if( cxmy == xmy ) {
                        for( int j=0; j<Math.min(trm_nbdvo[ictrans],trm_nbdvo[lctrans]); j++) {
                            if( (trm_vdvo[ictrans][j] != trm_vdvo[lctrans][j]) ) {
                                JOptionPane.showMessageDialog(null,"Development order must be consistent for P"+(j+xmy)+"mP"+j+lnsep+
                                                                   "see "+ntrans[ictrans]+" and "+ntrans[lctrans]);
                                return false;
                            }
                        }
                    }
                }
            }
        }
        for(ictrans=0; ictrans<nbtrans; ictrans++) {
            if( ! jrbdvo[ictrans].isSelected() ) {                   // not T type
                continue;
            }
            for( int j=0; j<trm_nbdvo[ictrans]; j++) {
                transp_vdvo[itransp[ictrans]][j] = trm_vdvo[ictrans][j];
            }
        }
        return true;
    }

/////////////////////////////////////////////////////////////////////

    // check full definition of a transition
    private boolean testTrans(int cictrans) {

        // are Jmax of lower and upper polyads defined ?
        njpol = (String) jcbjpol[isup[itransp[cictrans]]].getSelectedItem();
        if(njpol == "") {
            JOptionPane.showMessageDialog(null,"You have to select in BASICS"+lnsep+
                                               "the P"+polyads[isup[itransp[cictrans]]].getPolnb()+" Jmax for "+ntrans[cictrans]+" transition");
            return false;
        }
        njpol = (String) jcbjpol[iinf[itransp[cictrans]]].getSelectedItem();
        if(njpol == "") {
            JOptionPane.showMessageDialog(null,"You have to select in BASICS"+lnsep+
                                               "the P"+polyads[iinf[itransp[cictrans]]].getPolnb()+" Jmax for "+ntrans[cictrans]+" transition");
            return false;
        }
        // are they compatible ?
        jdiff = 1;                                                   // dip
        if( jrbpol[cictrans].isSelected() ) {
            jdiff = 2;                                               // pol
        }
        if( isup[itransp[cictrans]] != iinf[itransp[cictrans]] ) {
            if( ! (vjpol[isup[itransp[cictrans]]] <= (vjpol[iinf[itransp[cictrans]]]-jdiff)) ) {
                JOptionPane.showMessageDialog(null,"Jmax of Upper Polyad P"+polyads[isup[itransp[cictrans]]].getPolnb()+" has to be lower or equal to"+lnsep+
                                                   "Jmax -"+jdiff+" of Lower Polyad P"+polyads[iinf[itransp[cictrans]]].getPolnb()                    +lnsep+
                                                   "for "+ntrans[cictrans]+" transition");
                return false;
            }
        }
        // extractions
        for( int ixasg=0; ixasg<jfxasg[ictrans].getNbXasg(); ixasg++ ) {
            if( ! jfxasg[ictrans].getJPXasg(ixasg).test() ) {
                return false;
            }
        }
        // parameter file
        if( jrbpara[cictrans].isSelected() ) {
            if( spara[cictrans] == "" ) {
                JOptionPane.showMessageDialog(null,"You have to define the parameter file for para option  for "+ntrans[cictrans]+" transition");
                return false;
            }
        }
        // jrbdvo
        if( jrbdvo[cictrans].isEnabled() ) {
            if( ! jrbdvo[cictrans].isSelected() ) {
                JOptionPane.showMessageDialog(null,"You have to set Transition Moment Devel. Order for "+ntrans[cictrans]+" transition");
                return false;
            }
        }
        // Dev. Order
        if( jrbdvo[cictrans].isSelected() ) {
            for( int j=0; j<trm_nbdvo[cictrans]; j++) {
                trm_ndvo = (String) trm_jcbdvo[cictrans][j].getSelectedItem();
                if( trm_ndvo == "" ) {
                    JOptionPane.showMessageDialog(null,"You have to fully define the development order for "+ntrans[cictrans]+" transition");
                    return false;
                }
                else {
                    trm_vdvo[cictrans][j] = Integer.parseInt(trm_ndvo);  // save dev. order value
                }
            }
        }
        // POLST
        if( jrbpolst[cictrans].isSelected() ) {
            if( (String) jcbpolst[cictrans].getSelectedItem() == "" ) {
                JOptionPane.showMessageDialog(null,"You have to fully define the polarization state for "+ntrans[cictrans]+" transition");
                return false;
            }
        }
        return true;
    }

    // test ParaFile_adj vs. transitions concistency
    public boolean testAdj() {

        nbadj = jfadj.getNbAdj();                                    // nb of extra ParaFile_adj
        if( nbadj != 0 ) {                                           // some
            // jfadj test
            if( ! jfadj.test() ) {
                return false;
            }
            //
            if( nbtrans != 0 ) {                                     // transitions defined
                for(int iadj=0; iadj<nbadj; iadj++ ) {               // for each adj
                    JPAdj jpadj = jfadj.getJPAdj(iadj);              // it's panel
                    String cntradj = jpadj.getNtradj();              // current name of tradj
                    // polyads used by transitions ?
                    for( ictransp=0; ictransp<nbtransp; ictransp++ ) {  // for each possible transition
                        if( ntransp[ictransp].equals(cntradj) ) {
                            if( ! polisu[isup[ictransp]] ) {
                                JOptionPane.showMessageDialog(null,"Extra ParaFile_adj #"+(iadj+1)+lnsep+
                                                                   "Upper polyad NOT used by transitions");
                                return false;
                            }
                            if( ! polisu[iinf[ictransp]] ) {
                                JOptionPane.showMessageDialog(null,"Extra ParaFile_adj #"+(iadj+1)+lnsep+
                                                                   "Lower polyad NOT used by transitions");
                                return false;
                            }
                         }
                    }
                    // already defined in transitions ?
                    for( ictrans=0; ictrans<nbtrans; ictrans++) {    // for each transition
                        if(  ntransp[itransp[ictrans]].equals(cntradj)              &&
                            (jrbpol[ictrans].isSelected() == jpadj.isPolSelected()) &&
                            ( ! spara[ictrans].equals("")                         )    ) {
                            JOptionPane.showMessageDialog(null,"Extra ParaFile_adj #"+(iadj+1)+lnsep+
                                                               "already defined in "+ntrans[ictrans]+" transition");
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

}
