/*
 * Class to choose a job, a directory where to run it and run it
 */

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;

/**
 * This panel allows to execute jobs given in the examples or created in the "Create job" tab.
 */
public class RunJob extends JPanel implements  ActionListener {

    // labels
    private JLabel  tjob;                                            // job       choice
    private JLabel  tdir;                                            // directory choice
    private JLabel  trun;                                            // run       choice
    private JLabel  ljob;                                            // chosen job
    private JLabel  ldir;                                            // chosen directory
    // buttons
    private JButton jbjob;
    private JButton jbdir;
    private JButton jbrun;
    // names
    private String playd;                                            // XTDS installation directory
    private String workd;                                            // working directory
    private String njobr;                                            // reduced job name
    private String njdir;                                            // job directory
    private String njobf;                                            // full    job name
    private String noutf;                                            // full    output file
    private String nwdir;                                            // working directory
    private StringBuffer sbcmd;                                      // command creation sb
    private String ncmd;                                             // command
    // box
    private Box box;

    private String lnsep;                                            // line separator
    private String fisep;                                            // file separator

/////////////////////////////////////////////////////////////////////

    /**
     * Constructs a new RunJob.
     */
    public RunJob() {

        playd = System.getProperty("xtds.home");                     // XTDS installation directory
        workd = System.getProperty("user.dir");                      // working directory
        lnsep = System.getProperty("line.separator");
        fisep = System.getProperty("file.separator");
        setName("RunJob");                                           // for help files

        // texts
        tjob = new JLabel("1. Choose a job");
        tdir = new JLabel("2. Choose a directory where to run the job");
        trun = new JLabel("3. Run the job");

        // buttons
        jbjob = new JButton("Choose a job");
        jbjob.setBackground(Color.WHITE);
        jbdir = new JButton("Choose a directory");
        jbdir.setBackground(Color.WHITE);
        jbrun = new JButton("Run the job");
        jbrun.setBackground(Color.WHITE);

        // strings
        njobf="";
        nwdir="";

        // choice labels
        ljob = new JLabel(njobf);
        ldir = new JLabel(nwdir);

        // StringBuffer for command creation
        sbcmd = new StringBuffer("");

        // box
        box = Box.createVerticalBox();
        box.add(Box.createVerticalStrut(50));
        box.add(tjob);
        box.add(Box.createVerticalStrut(5));
        box.add(jbjob);
        box.add(Box.createVerticalStrut(5));
        box.add(ljob);
        box.add(Box.createVerticalStrut(50));
        box.add(tdir);
        box.add(Box.createVerticalStrut(5));
        box.add(jbdir);
        box.add(Box.createVerticalStrut(5));
        box.add(ldir);
        box.add(Box.createVerticalStrut(50));
        box.add(trun);
        box.add(Box.createVerticalStrut(5));
        box.add(jbrun);
        add(box);

        // listener for each button
        jbjob.addActionListener(this);
        jbdir.addActionListener(this);
        jbrun.addActionListener(this);

    }

/////////////////////////////////////////////////////////////////////

    /**
     * Processes the events.
     */
    public void actionPerformed(ActionEvent evt) {

        // job choice
        if (evt.getSource() == jbjob) {
            JFileChooser jfcjobf = new JFileChooser(workd);          // file choice
            jfcjobf.setSize(400,300);
            jfcjobf.setFileSelectionMode(JFileChooser.FILES_ONLY);   // files only
            Container parentjob = jbjob.getParent();
            int choicejob = jfcjobf.showDialog(parentjob,"Select");
            if (choicejob == JFileChooser.APPROVE_OPTION) {
                File f = jfcjobf.getSelectedFile();                  // job file
                njobf= f.getAbsolutePath();                          // full    job file name
                njobr = f.getName();                                 // reduced job file name
                njdir = f.getParent();                               // job directory
                ljob.setText(njobf);
            }
            return;
        }
        // directory choice
        if (evt.getSource() == jbdir) {
            if (njobf == "") {
                JOptionPane.showMessageDialog(null,
                                              "You have to choose a job first");
            }
            else {
                JFileChooser jfcdir = new JFileChooser(njdir);
                jfcdir.setSize(400,300);
                jfcdir.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                Container parentdir = jbdir.getParent();
                int choicedir = jfcdir.showDialog(parentdir,"Select");
                if (choicedir == JFileChooser.APPROVE_OPTION) {
                    nwdir= jfcdir.getSelectedFile().getAbsolutePath();  // working directory
                    ldir.setText(nwdir);
                }
            }
            return;
        }
        // launch the job
        if (evt.getSource() == jbrun) {
            if (nwdir == "") {
                JOptionPane.showMessageDialog(null,
                                              "You have to choose a directory first");
            }
            else {
                // suppress output file if exist
                noutf = nwdir+fisep+njobr+".out";                    // job output file
                File f = new File(noutf);
                if( f.exists() ) {
                    f.delete();
                }
                // create command
                sbcmd.append("cd "+nwdir+" ; ");                     // change to working directory
                if( ! nwdir.equals(njdir) ) {                        // if working and job directory differ
                    // then copy job file in working directory
                    sbcmd.append(" cp "+njdir+fisep+njobr+" . ; ");
                }
                // launch job in background and save output and error
                sbcmd.append("nohup ."+fisep+njobr+" >"+njobr+".out 2>&1 &");
                ncmd = sbcmd.toString();
                try {
                    String[] cmd = {"/bin/sh", "-c", ncmd};
                    Process monproc = Runtime.getRuntime().exec(cmd);
                }
                catch (IOException ioe) {
                    JOptionPane.showMessageDialog(null,"IO error while running command"+lnsep+
                                                       ncmd                            +lnsep+
                                                       ioe);
                    return;
                }
                // to show job output
                JFFileOut jffo = new JFFileOut(noutf, getSize().width, getSize().height);
                jffo.setVisible(true);

                sbcmd.setLength(0);
                JOptionPane.showMessageDialog(null,"The job has been launched in background"+lnsep+
                                                   "Job output file is :"                   +lnsep+
                                                   noutf);
            }
            return;
        }
    }

}
