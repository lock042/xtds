/*
 * Class for fit jobs creation, called by CreateJob
 */

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;
import java.text.*;
import java.util.*;

/**
 * This panel creates two jobs to fit experimental data.
 */
public class CJPFit extends    JPanel
                    implements CJPint, ActionListener {

    // panels
    private JPanel pnord;
    private JPanel  pno;
    private JPanel  pnc;
    private JPanel pouest;
    private JPanel pcentre;
    private JPanel   pcn;
    private JPanel[]  pcnx;
    private JPanel[]   pcnxn;
    private JPanel[]    pcnxno;
    private JPanel[]    pcnxnc;
    private JPanel[]   pcnxo;
    private JPanel[]   pcnxc;
    private JPanel[]    pcnxc1;
    private JPanel[]    pcnxc2;
    private JPanel[]    pcnxc3;
    private JPanel[]    pcnxc4;
    private JPanel psud;

    // BASICS
    // fit type
    private JRadioButton jrbfreq;                                    // frequency fit
    private JRadioButton jrbint;                                     // intensity fit
    private ButtonGroup  bgfi;                                       // jrbfreq and jrbint group
    private JRadioButton jrbpol;                                     // transition type
    private JRadioButton jrbpolst;                                   // polarization state
    private JPanel       jpifdp;                                     // dip/pol panel
    private boolean      fitint;                                     // true = intensity
    private JComboBox    jcbpolst;                                   // polarization state
    private String[]     tpolst;                                     // polarization state choice array
    private String       npolst;                                     // polarization state name
    // molecule
    private JComboBox    jcbmol;                                     // molecule jcb
    private String       nmol;                                       // molecule name
    private String[]     trep;                                       // representation choice array
    private JComboBox    jcbrep;                                     // representation jcb
    private String       nrep;                                       // representation name
    // parameter file
    private JButton jbparaf;                                         // parameter file button
    private String  nparaf;                                          // parameter file name
    private String  nparar;                                          // reduced parameter file name
    private JLabel  lparaf;                                          // parameter file label
    // constraint file
    private JButton jbconsf;                                         // constraint file button
    private String  nconsf;                                          // constraint file name
    private JLabel  lconsf;                                          // constraint file label
    // scheme coefficients
    private JComboBox[] jcbpsc;                                      // polyad scheme coef. jcb
    private String[]    tpsc;                                        // polyad scheme coef. choice array
    private String      npsc;                                        // current psc name
    private int[]       vpsc;                                        // polyad scheme coef. values
    // POLYADS
    private boolean[]     polisu;                                    // polyad in-use
    private int           polnbmax;                                  // max polnb
    private int[]         spolnb;                                    // sorted polyad number
    private int[]         ipolnb;                                    // sorted polyad number indexes
    private int           nbpol;                                     // nb of polyads
    private int           icpol;                                     // current main polyad index
    private int           icpols;                                    // current  sub-polyad index
    private int           icpolmax;                                  // polnbmax     polyad index
    private JScrollPane   jsp;                                       // with lifts
    private String[]      tjpol;                                     // jpol choice array
    private JComboBox[]   jcbjpol;                                   // jmax of each polyad jcb
    private String        njpol;                                     // current jpol name
    private double[]      vjpol;                                     // polyads J values

    private Polyad[]      polyads;                                   // polyads
    private JPPolyad[]    jpps;                                      // polyad JPanels
    private int           cur_polnb;                                 // current polyad #
    private int           inf_polnb;                                 // lower   polyad #
    private int           sup_polnb;                                 // upper   polyad #

    private int           mxtdvo = 10;                               // tdvo size : 0 -> 9 order
    private String[]      tdvo;                                      // development order choice array
    // TRANSITIONS
    private int                   basepol = 10;                      // nb of polyad base for mxtrans evaluation
    private String[]              ntransp;                           // possible transition names (unique)
    private int                   nbtransp;                          // nb of possible transitions
    private boolean[]             transpisu;                         // this possible transition is in-use
    private int                   mxtrans;                           // max nb of transitions
    private String[]              ntrans;                            // transition name
    private int                   nbtrans;                           // nb of transitions
    private int                   ictrans;                           // current                transition index
    private int                   ictransp;                          // current       possible transition index
    private int[]                 itransp;                           // corresponding possible transition index
    private JButton[]             jbasgf;                            // ASG file jb
    private String[]              nasgf;                             // ASG file name
    private JLabel[]              lasgf;                             // ASG file jl
    private JTextField[]          jtfselstr;                         // selection string jtf
    private String                nselstr;                           // selection string in ASG file
    private JComboBox[][]         trm_jcbdvo;                        // transition moment development order jcb
    private String                trm_ndvo;                          // transition moment development order name
    private int[][]               transp_vdvo;                       // dvo values for possible transitions
    private int[][]               trm_vdvo;                          // transition moment development order values
    private int[]                 trm_nbdvo;                         // nb of dev. orders for each          transition
    private int[]                 transp_nbdvo;                      // nb of dev. orders for each possible transition
    private JFormattedTextField[] jftftemp;                          // trot text field
    private float[]               vtemp;                             // trot value
    private JTextField[]          jtfrinmi;                          // rinmi text field
    private float[]               vrinmi;                            // rinmi value
    private JRadioButton[]        jrblpcmax;                         // lpcmax jrb
    private String[]              tlpcmax;                           // lpcmax choice array
    private JComboBox[]           jcblpcmax;                         // lpcmax jcb
    private JRadioButton[]        jrbprec;                           // prec option jrb
    private JRadioButton[]        jrbunit;                           // unit option jrb
    private JRadioButton[]        jrbMHz;                            // MHz  option jrb
    private JRadioButton[]        jrbGHz;                            // GHz  option jrb
    private JRadioButton[]        jrbdum;
    private ButtonGroup[]         bgunit;                            // jrbMHz,GHz,dum group
    private JRadioButton[]        jrbrmxomc;                         // rmxomc jrb
    private String[]              trmxomc;                           // rmxomc choice array
    private JComboBox[]           jcbrmxomc;                         // rmxomc jcb
    private JRadioButton[]        jrbfpvib;                          // fpvib option jrb
    private JTextField[]          jtffpvib;                          // fpvib option jtf
    private float[]               vfpvib;                            // fpvib value
    private JRadioButton[]        jrbabund;                          // abund option jrb
    private JTextField[]          jtfabund;                          // abund option jtf
    private float[]               vabund;                            // abund value
    private int[]                 isup;                              // upper polyad index
    private int[]                 iinf;                              // lower polyad index
    private int                   jdiff;                             // upper and lower polyads j difference
    private int                   xmy;                               // x-y for PxmPy
    private int                   cxmy;                              // current xmy

    // WEST
    private JButton jbaddpol;                                        // add polyad             button
    private JButton jbshowpol;                                       // show polyad            button
    private JButton jbremlastpol;                                    // remove last polyad     button
    private JButton jbsettrans;                                      // set transitions        button
    private JButton jbaddtrans;                                      // add transition         button
    private JButton jbremlasttrans;                                  // remove last transition button
    private Box     boxouest;                                        // west panel box
    // SOUTH
    private JButton jbreset;                                         // reset
    private JButton jbsave;                                          // save
    private Box     boxsud;                                          // button box

    // CreateJob parameters
    private String   playd;                                          // XTDS installation directory
    private String   workd;                                          // working directory
    private String   npack;                                          // package name
    private String[] moldirs;                                        // molecule directories
    private int      nbvqn;                                          // nb of vibrational quantum numbers
    private int      nbclab;                                         // nb of characters of a parameter label
    private int      mxpol;                                          // max nb of polyads
    private int      mxsnv = 20;                                     // max nb of vibrational sublevels to be choosen for LPCMAX

    // miscellaneous
    private StringBuffer   sb;                                       // show polyad sb
    private StringBuffer   sbcmd;                                    // constraint file creation command
    private String         ncmd;                                     // commande string
    private String         str;                                      // parameter file reading string
    private BufferedReader br;                                       // parameter file reading buffer
    private BufferedReader bropt;                                    // optional parameter file reading buffer
    private String         nskelf;                                   // Pa_skel file name
    private String         nskeld;                                   // Pa_skel directory
    private int            n;                                        // n !
    private String         njobf;                                    // job name
    private String         njobr;                                    // job name (reduced)
    private PrintWriter    out1;                                     // job writing pw
    private File           f;                                        // f !
    private String lnsep;                                            // line separator
    private String fisep;                                            // file separator
    // Number Format
    private NumberFormat nffloat;                                    // float   format
    private NumberFormat nfint;                                      // integer format

/////////////////////////////////////////////////////////////////////

    /**
     * Constructs a new CJPFit.
     */
    public CJPFit() {

        playd = System.getProperty("xtds.home");                     // XTDS installation directory
        workd = System.getProperty("user.dir");                      // working directory
        lnsep = System.getProperty("line.separator");
        fisep = System.getProperty("file.separator");
        setName("CJPFit");                                           // for help files
        setLayout(new BorderLayout());

        // south panel
        jbreset = new JButton();                                     // managed by CreateJob
        jbreset.setBackground(Color.WHITE);
        jbsave  = new JButton("Save");                               // job file saving button
        jbsave.setBackground(Color.WHITE);
        jbsave.addActionListener (new ActionListener () {
            public void actionPerformed (ActionEvent ae) {           // jbsave action
                saveCJP();
            }
        }
        );

        boxsud = Box.createHorizontalBox();                          // add buttons to box
        boxsud.add(jbreset);
        boxsud.add(Box.createHorizontalStrut(15));
        boxsud.add(jbsave);
        psud = new JPanel();
        psud.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        psud.add(boxsud);                                            // add box

        // west panel for polyad and transition buttons
        boxouest = Box.createVerticalBox();                          // add buttons to box
        jbaddpol = new JButton("Add Polyad");
        jbaddpol.setBackground(Color.WHITE);
        jbaddpol.addActionListener(this);
        boxouest.add(jbaddpol);
        boxouest.add(Box.createVerticalStrut(15));
        jbshowpol = new JButton("Show Polyads");
        jbshowpol.setBackground(Color.WHITE);
        jbshowpol.addActionListener(this);
        boxouest.add(jbshowpol);
        boxouest.add(Box.createVerticalStrut(15));
        jbremlastpol = new JButton("Remove Last Polyad");
        jbremlastpol.setBackground(Color.WHITE);
        jbremlastpol.addActionListener(this);
        boxouest.add(jbremlastpol);
        boxouest.add(Box.createVerticalStrut(120));
        jbsettrans = new JButton("Set Transitions");
        jbsettrans.setBackground(Color.WHITE);
        jbsettrans.addActionListener(this);
        boxouest.add(jbsettrans);
        pouest = new JPanel();
        pouest.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLUE,4),null));
        pouest.add(boxouest);

        // center panel for polyad display
        pcentre = new JPanel(new BorderLayout());
        jsp = new JScrollPane(pcentre);                              // lifts
        pcn = new JPanel(new GridLayout(0,1,5,5));
        pcentre.add(pcn,"North");

        // display panels
        add(pouest,  "West");
        add(jsp,     "Center");
        add(psud,    "South");
    }

/////////////////////////////////////////////////////////////////////

    /**
     * Returns the reset button.
     * <br>Called by CreateJob which manages it.
     */
    public JButton getJBreset() {

        return jbreset;
    }

    /**
     * Sets basic parameters and creates north panel.
     * <br>Called by CreateJob to activate this panel.
     * @param cnpack     name of the package
     * @param cmoldirs   molecule directories
     * @param cmxpol     MXPOL
     * @param cnbclab    number of characters of parameter label
     * @param cnbvqn     number of vibrational quantum numbers
     */
    public void setBasicPara(String cnpack, String[] cmoldirs, int cmxpol, int cnbclab, int cnbvqn) {

        npack   = cnpack;                                            // package name
        moldirs = cmoldirs;                                          // molecule directories
        mxpol   = cmxpol;                                            // MXPOL
        nbclab  = cnbclab;                                           // nb of characters of a parameter label
        nbvqn   = cnbvqn;                                            // nb of vibrational quantum numbers

        createNorth();
    }

/////////////////////////////////////////////////////////////////////

    // north panel for polyad characteristics setting and display
    private void createNorth() {

        pnord = new JPanel(new BorderLayout());                      // north panel
        pno = new JPanel(new GridLayout(5,0,5,5));
        pnc = new JPanel(new GridLayout(5,0,5,5));

        // BASICS
        // fit type choice
        pno.add(new JLabel(""));
        jrbfreq = new JRadioButton("Frequencies");
        jrbfreq.setBackground(Color.WHITE);
        jrbfreq.addActionListener(this);
        jrbint = new JRadioButton("Intensities  ");
        jrbint.setBackground(Color.WHITE);
        jrbint.addActionListener(this);
        bgfi = new ButtonGroup();
        bgfi.add(jrbfreq);
        bgfi.add(jrbint);
        jrbpol = new JRadioButton("pol");
        jrbpol.setEnabled(false);
        jrbpol.addActionListener(this);
        jpifdp = new JPanel(new GridLayout(0,8,5,5));
        jpifdp.add(new JLabel("Fit Type ",null,JLabel.RIGHT));
        jpifdp.add(jrbfreq);
        jpifdp.add(jrbint);
        jpifdp.add(new JLabel(""));
        jpifdp.add(jrbpol);
        jrbpolst = new JRadioButton("Polarization State");
        jrbpolst.setEnabled(false);
        jrbpolst.addActionListener(this);
        jpifdp.add(jrbpolst);
        tpolst = new String[4];
        tpolst[0] = "";
        tpolst[1] = "R111";
        tpolst[2] = "R110";
        tpolst[3] = "R001";
        jcbpolst = new JComboBox(tpolst);
        jcbpolst.setSelectedItem("");
        jcbpolst.setBackground(Color.WHITE);
        jcbpolst.setEnabled(false);
        jpifdp.add(jcbpolst);
        pnc.add(jpifdp);
        // molecule choice
        pno.add(new JLabel(""));
        JPanel jpmr;
        jpmr =  new JPanel(new GridLayout(0,8,0,5));
        jpmr.add(new JLabel("Molecule ",null,JLabel.RIGHT));
        jcbmol = new JComboBox();
        jcbmol.addItem("");
        for( int i=0; i<moldirs.length; i++ ) {
            jcbmol.addItem(moldirs[i]);
        }
        jcbmol.setSelectedItem("");                                  // default to space
        jcbmol.setBackground(Color.WHITE);
        nmol = "";
        jcbmol.addActionListener(this);
        jpmr.add(jcbmol);
        if( npack.equals("D2hTDS") ) {
            // representation choice
            jpmr.add(new JLabel(""));
            jpmr.add(new JLabel("Representation ",null,JLabel.RIGHT));
            trep = new String[4];
            trep[0] = "";                                            // space first
            trep[1] = "Ir";
            trep[2] = "IIr";
            trep[3] = "IIIr";
            jcbrep = new JComboBox(trep);
            jcbrep.setSelectedItem("");                              // default to space
            jcbrep.setBackground(Color.WHITE);
            jpmr.add(jcbrep);
        }
        pnc.add(jpmr);
        // parameter file choice
        jbparaf = new JButton("Parameter File");
        jbparaf.setBackground(Color.WHITE);
        jbparaf.addActionListener(this);
        nparaf = "";
        pno.add(jbparaf);
        lparaf = new JLabel("");                                     // default to space
        lparaf.setOpaque(true);
        lparaf.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        pnc.add(lparaf);
        // constraint file choice
        jbconsf = new JButton("Constraint File");
        jbconsf.setBackground(Color.WHITE);
        jbconsf.addActionListener(this);
        nconsf = "";
        pno.add(jbconsf);
        lconsf = new JLabel("");                                     // default to space
        lconsf.setOpaque(true);
        lconsf.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        pnc.add(lconsf);
        // polyad scheme coefficients choice
        pno.add(new JLabel("Polyad Scheme "));
        JPanel jppsc = new JPanel(new GridLayout(0,2*nbvqn+1,5,5));  // nbvqn = nb of vibrational quantum numbers
        jppsc.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        jcbpsc = new JComboBox[nbvqn];
        tpsc = new String[mxpol];                                    // polyad scheme coefficients array
        for( icpol=0; icpol<mxpol; icpol++) {
            tpsc[icpol] = String.valueOf(icpol);                     // 0 -> mxpol
        }
        for ( int i=0; i<nbvqn; i++) {                               // choice Label
            if(i == 0) {
                jppsc.add(new JLabel(" [P]n = "));                   // polyad number
            }
            jcbpsc[i] = new JComboBox();
            jcbpsc[i].addItem("");
            for( int j=0; j<tpsc.length; j++ ) {
                jcbpsc[i].addItem(tpsc[j]);
            }
            jcbpsc[i].setSelectedItem("");                           // default to space
            jcbpsc[i].setBackground(Color.WHITE);
            jcbpsc[i].addActionListener(this);
            vpsc = new int[nbvqn];                                   // polyad scheme coefficients values
            jppsc.add(jcbpsc[i]);
            if(i != nbvqn-1) {                                       // set label
                jppsc.add(new JLabel("*v"+(i+1)+" +"));
            }
            else {
                jppsc.add(new JLabel("*v"+(i+1)));
            }
        }
        pnc.add(jppsc);
        pnord.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLUE,4),"BASICS"));

        // POLYADS
        pcnx  = new JPanel[mxpol];                                   // polyad panel
        tjpol = new String[201];                                     // 201 due to space plus jmax : 0(.5) -> 199(.5)
        tjpol[0] = "";                                               // space first
        for( int i=1; i<tjpol.length; i++) {                         // array set
            if( npack.equals("C3vsTDS") ) {
                tjpol[i] = String.valueOf(i-1+0.5);
            }
        else {
                tjpol[i] = String.valueOf(i-1);
            }
        }
        vjpol     = new double[mxpol];

        tdvo = new String[mxtdvo];
        for(int i=0; i<mxtdvo; i++) {                                // 0 -> 9
            tdvo[i] = String.valueOf(i);
        }

        polyads = new Polyad[mxpol];                                 // polyads
        jpps    = new JPPolyad[mxpol];                               // their JPanel
        polisu = new boolean[mxpol];                                 // polyade used ?
        Arrays.fill(polisu, false);                                  // no one

        nbpol = 0;                                                   // nb of polyads

        // add panels and sub-panels
        pnord.add(pno,"West");
        pnord.add(pnc,"Center");
        add(pnord, "North");
    }

/////////////////////////////////////////////////////////////////////

    /**
     * Processes the events.
     */
    public void actionPerformed(ActionEvent evt) {

        // BASICS
        // fit type
        if (evt.getSource() == jrbfreq ) {
            fitint = false;                                          // NO access to intensity specifications
            if( jrbpol.isSelected() ) {
                jrbpol.doClick();
            }
            jrbpol.setEnabled(false);
            jrbpol.setBackground(this.getBackground());
            return;
        }
        if (evt.getSource() == jrbint ) {
            fitint = true;                                           // DO access to intensity specifications
            jrbpol.setEnabled(true);
            jrbpol.setBackground(Color.WHITE);
            return;
        }
        if (evt.getSource() == jrbpol ) {                            // DO access to polarization state
            if( jrbpol.isSelected() ) {
                jrbpolst.setEnabled(true);
                jrbpolst.setBackground(Color.WHITE);
            }
            else {
                if( jrbpolst.isSelected() ) {
                    jrbpolst.doClick();
                }
                jrbpolst.setEnabled(false);
                jrbpolst.setBackground(this.getBackground());
            }
            return;
        }
        if(evt.getSource() == jrbpolst) {
            if( jrbpolst.isSelected() ) {
                jcbpolst.setEnabled(true);
            }
            else {
                jcbpolst.setSelectedItem("");
                jcbpolst.setEnabled(false);
            }
            return;
        }
        // molecule
        if (evt.getSource() == jcbmol ) {
            if( (!jrbint.isSelected()) && (!jrbfreq.isSelected()) ) {
                JOptionPane.showMessageDialog(null,"You have to first choose a fit type (freq/int)");
                jcbmol.setSelectedItem("");
                return;
            }
            if( jrbpolst.isSelected() && jcbpolst.getSelectedItem() == "" ) {
                JOptionPane.showMessageDialog(null,"You have to first complete polarization state");
                jcbmol.setSelectedItem("");
                return;
            }
            nmol = (String) jcbmol.getSelectedItem();
            // unselect representation and parameter file
            if( npack.equals("D2hTDS") ) {
                jcbrep.setSelectedItem("");
            }
            nparaf = "";                                             // unselect parameter  file
            lparaf.setText(nparaf);
            nconsf = "";                                             // unselect constraint file
            lconsf.setText(nconsf);
            if( nmol != "" ) {
                nskeld = playd+fisep+"packages"+fisep+npack+fisep+"para"+fisep+nmol;  // Pa_skel directory
                nskelf = nskeld+fisep+"Pa_skel";
            }
            return;
        }
        // parameter file
        if (evt.getSource() == jbparaf) {
            if( nmol == "" ) {
                JOptionPane.showMessageDialog(null,"You have to first define the molecule");
                return;
            }
            JFileChooser jfcparaf = new JFileChooser(playd+fisep+"packages"+fisep+npack+fisep+"para"+fisep+nmol);  // default choice directory
            jfcparaf.setSize(400,300);
            jfcparaf.setFileSelectionMode(JFileChooser.FILES_ONLY);  // files only
            jfcparaf.setDialogTitle("Define the parameter file to be used");
            Container parent = jbparaf.getParent();
            int choice = jfcparaf.showDialog(parent,"Select");       // Dialog, Select
            if (choice == JFileChooser.APPROVE_OPTION) {
                nparaf= jfcparaf.getSelectedFile().getAbsolutePath();
                nparar= jfcparaf.getSelectedFile().getName();
                lparaf.setText(nparaf);
                // constraint file creation ?
                n = JOptionPane.showConfirmDialog(null,"Would you like to create"           +lnsep+
                                                       "the corresponding Constraint file ?"+lnsep,
                                                       "Constraint File Creation",JOptionPane.YES_NO_OPTION);
                if(n == JOptionPane.YES_OPTION) {                    // create
                    createConsf();
                    lconsf.setText(nconsf);
                }
            }
            return;
        }
        // constraint file
        if (evt.getSource() == jbconsf) {
            if( nmol == "" ) {
                JOptionPane.showMessageDialog(null,"You have to first define the molecule");
                return;
            }
            if( nparaf == "" ) {
                JOptionPane.showMessageDialog(null,"You have to first define the parameter file");
                return;
            }
            JFileChooser jfcconsf = new JFileChooser(playd+fisep+"packages"+fisep+npack+fisep+"ctrp"+fisep+nmol);  // default choice directory
            jfcconsf.setSize(400,300);
            jfcconsf.setFileSelectionMode(JFileChooser.FILES_ONLY);  // files only
            jfcconsf.setDialogTitle("Define the constraint file to be used");
            Container parent = jbconsf.getParent();
            int choice = jfcconsf.showDialog(parent,"Select");       // Dialog, Select
            if (choice == JFileChooser.APPROVE_OPTION) {
                nconsf= jfcconsf.getSelectedFile().getAbsolutePath();
                lconsf.setText(nconsf);
            }
            return;
        }
        // Polyad Scheme Coefficients
        for (int i=0; i<nbvqn; i++) {
            if(evt.getSource() == jcbpsc[i] ) {                      // whatever coef.
                npsc = (String) jcbpsc[i].getSelectedItem();
                if(npsc != "") {
                    vpsc[i] =  Integer.parseInt(npsc);               // save coef. value
                }
                return;
            }
        }

        // ADD POLYAD
        if(evt.getSource() == jbaddpol) {
            // BASICS checked ?
            if( ! testBasics()) {
                askBasics();
                return;
            }
            // existing polyad(s) checked ?
            if( nbpol >= 1 ) {
                if( ! testAllPol() ) {
                    return;
                }
            }
            // max reached ?
            if(nbpol == mxpol) {
                JOptionPane.showMessageDialog(null,"The maximum number of polyads is reached ("+mxpol+")");
                return;
            }
            icpol = nbpol;
            nbpol++;
            // prepare choice
            pcnx[icpol]  = new JPanel(new BorderLayout());           // plain panel
            polyads[icpol] = new Polyad(nbvqn);                      // polyad
            if( ! polyads[icpol].setInd(npack, nparaf) ) {           // set frequency indexes
                jcbmol.setSelectedItem("");                          // file error : unselect molecule
            }
            polyads[icpol].setName("("+nbpol+") POLYAD");            // its name
            jpps[icpol] = new JPPolyad(this, polyads[icpol], tpsc, pcnx[icpol]);  // its JPP panel

            pcn.add(pcnx[icpol]);
            pcnx[icpol].revalidate();
            // go to bottom to show the last polyad panel
            SwingUtilities.invokeLater(new Runnable() {
                 public void run() {
                     final JScrollBar jsbv = jsp.getVerticalScrollBar();
                     jsbv.setValue(jsbv.getMaximum());
                 }
            });
            //
            return;
        }

        // SHOW POLYADS
        if(evt.getSource() == jbshowpol) {
            if( testAllPol() ) {                                     // if everything checked
                showPol();
            }
            return;
        }

        // REMOVE LAST POLYAD
        if(evt.getSource() == jbremlastpol) {
            if(nbpol != 0) {
                pcn.remove(pcnx[nbpol-1]);
                pcn.repaint();
                nbpol --;
            }
            return;
        }

        // SET TRANSITIONS
        if(evt.getSource() == jbsettrans) {
            // existing polyad(s) checked ?
            if( nbpol >= 1 ) {
                if( ! testBasics() ) {
                    askBasics();
                    return;
                }
                if( ! testAllPol() ) {
                    return;
                }
            }
            // at least one polyad
            if(nbpol == 0) {
                JOptionPane.showMessageDialog(null,"No polyad defined");
                return;
            }
            // set number of possible transitions
            nbtransp = 0;
            for(int i=1; i<nbpol+1; i++) {
                nbtransp = nbtransp +i;
            }
            ntransp   = new String[nbtransp];
            ictrans = 0;
            for(int j=0; j<nbpol; j++) {
                for(int i=j; i<nbpol; i++) {
                    ntransp[ictrans] = "P"+polyads[ipolnb[i]].getPolnb()+"mP"+polyads[ipolnb[j]].getPolnb();  // their names
                    ictrans++;
                }
            }
            // characteristics array
            transpisu    = new boolean[nbtransp];
            transp_vdvo  = new int[nbtransp][];
            transp_nbdvo = new int[nbtransp];
            isup         = new int[nbtransp];
            iinf         = new int[nbtransp];
            // set max number of transitions
            basepol = Math.max(basepol,mxpol);
            mxtrans = 0;
            for(int i=1; i<basepol+1; i++) {
                mxtrans = mxtrans +i;
            }
            // and implied polyad for each transition
            ictransp = 0;
            for(int i=nbpol-1; i>=0; i--) {
                for(int j=i; j>=0; j--) {
                    isup[ictransp] = ipolnb[i];
                    iinf[ictransp] = ipolnb[j];
                    transp_nbdvo[ictransp] = polyads[iinf[ictransp]].getPolnb()+1;
                    transp_vdvo[ictransp]  = new int[transp_nbdvo[ictransp]];
                    ictransp++;
                }
            }
            // confirm
            sbPol();
            sb.append("Are you sure that"        +lnsep+
                      "All polyads are created ?");
            n = JOptionPane.showConfirmDialog(null,sb.toString(),"Set Transitions",JOptionPane.YES_NO_OPTION);
            if(n != JOptionPane.YES_OPTION) {
                return;
            }
            // north panel modification
            pnord.removeAll();
            pno = new JPanel(new GridLayout(3,0,5,5));
            pnc = new JPanel(new GridLayout(3,0,5,5));
            pno.add(new JLabel("Fit Type"));
            jrbfreq.setEnabled(false);
            jrbint.setEnabled(false);
            jrbpol.setEnabled(false);
            jrbpolst.setEnabled(false);
            jcbpolst.setEnabled(false);
            pnc.add(jpifdp);
            pno.add(new JLabel("Molecule"));
            jcbmol.setEnabled(false);
            pnc.add(jcbmol);
            // add polyad Jmax panel
            pno.add(new JLabel("Polyad Jmax "));
            JPanel jpjpol = new JPanel(new GridLayout(0,2*nbpol,5,5));
            jcbjpol  = new JComboBox[nbpol];
            for( icpol=0; icpol<nbpol; icpol++) {
                jcbjpol[ipolnb[icpol]] = new JComboBox(tjpol);
                jcbjpol[ipolnb[icpol]].setSelectedItem("");
                jcbjpol[ipolnb[icpol]].setBackground(Color.WHITE);
                jcbjpol[ipolnb[icpol]].addActionListener(this);
                jpjpol.add(new JLabel("JP"+polyads[ipolnb[icpol]].getPolnb()+" ",null,JLabel.RIGHT));
                jpjpol.add(jcbjpol[ipolnb[icpol]]);
                jcbjpol[ipolnb[icpol]].setEnabled(false);
            }
            jpjpol.setBorder(BorderFactory.createLineBorder(Color.BLACK));
            pnc.add(jpjpol);
            pnord.add(pno,"West");
            pnord.add(pnc,"Center");
            // add transition buttons
            pouest.removeAll();

            boxouest = Box.createVerticalBox();
            jbaddtrans = new JButton("Add Transition");
            jbaddtrans.setBackground(Color.WHITE);
            jbaddtrans.addActionListener(this);
            boxouest.add(jbaddtrans);
            boxouest.add(Box.createVerticalStrut(15));
            jbremlasttrans = new JButton("Remove Last Transition");
            jbremlasttrans.setBackground(Color.WHITE);
            jbremlasttrans.addActionListener(this);
            boxouest.add(jbremlasttrans);
            boxouest.add(Box.createVerticalStrut(160));
            boxouest.add(jbshowpol);
            pouest.add(boxouest);

            // empty center panel
            for(int icpol=0; icpol<nbpol; icpol++) {
                pcn.remove(pcnx[icpol]);
            }
            pcn.repaint();
            // catching formats
            nffloat = NumberFormat.getInstance(Locale.US);
            nffloat.setGroupingUsed(false);                          // ie. no thousands seperator
            nffloat.setMaximumFractionDigits(10);                    // ie. no more then 10 digits for fractionnal part
            nfint = NumberFormat.getInstance();
            nfint.setGroupingUsed(false);                            // ie. no thousands seperator
            nfint.setParseIntegerOnly(true);                         // ie. only integers
            nfint.setMaximumIntegerDigits(7);                        // ie. no more then 7 digits
            // create arrays
            ntrans    = new String[mxtrans];
            itransp   = new int[mxtrans];
            jbasgf    = new JButton[mxtrans];
            nasgf     = new String[mxtrans];
            lasgf     = new JLabel[mxtrans];
            jtfselstr = new JTextField[mxtrans];
            if(fitint) {                                             // if intensities
                trm_jcbdvo = new JComboBox[mxtrans][];
                trm_vdvo   = new int[mxtrans][];
                trm_nbdvo  = new int[mxtrans];
                jftftemp   = new JFormattedTextField[mxtrans];
                vtemp      = new float[mxtrans];
                jtfrinmi   = new JTextField[mxtrans];
                vrinmi     = new float[mxtrans];
            }
            jrblpcmax = new JRadioButton[mxtrans];
            jcblpcmax = new JComboBox[mxtrans];
            tlpcmax  = new String[mxsnv];                            // lpcmax choice array
            for( int i=0; i<mxsnv; i++) {
                tlpcmax[i] = String.valueOf(i+1);                    // 1 -> mxsnv
            }
            jrbprec   = new JRadioButton[mxtrans];
            jrbunit   = new JRadioButton[mxtrans];
            jrbMHz    = new JRadioButton[mxtrans];
            jrbGHz    = new JRadioButton[mxtrans];
            jrbdum    = new JRadioButton[mxtrans];
            bgunit    = new ButtonGroup[mxtrans];
            if( fitint ) {
                jrbrmxomc = new JRadioButton[mxtrans];
                trmxomc = new String[18];
                for( int j=0; j<18; j++ ) {
                    if( j <10 ) {
                        trmxomc[j] = String.valueOf((j+1)*10);       // each 10 up to 100
                    }
                    else {
                        trmxomc[j] = String.valueOf(150+(j-10)*50);  // each 50 up to 500
                    }
                }
                jcbrmxomc = new JComboBox[mxtrans];
                jrbfpvib  = new JRadioButton[mxtrans];
                jtffpvib  = new JTextField[mxtrans];
                vfpvib    = new float[mxtrans];
                jrbabund  = new JRadioButton[mxtrans];
                jtfabund  = new JTextField[mxtrans];
                vabund    = new float[mxtrans];
            }
            // create panels
            pcnx      = new JPanel[mxtrans];
            pcnxo     = new JPanel[mxtrans];
            pcnxc     = new JPanel[mxtrans];
            if( fitint ) {
                pcnxc1     = new JPanel[mxtrans];
                pcnxc2     = new JPanel[mxtrans];
                pcnxc4     = new JPanel[mxtrans];
            }
            pcnxc3     = new JPanel[mxtrans];

            nbtrans = 0;                                             // nb of transitions
            return;
        }

        // ADD TRANSITION
        if(evt.getSource() == jbaddtrans) {

            // existing transition(s) checked ?
            if( nbtrans >= 1 ) {
                if( ! testAllTrans() ) {
                    return;
                }
            }

            // max reached ?
            if(nbtrans == mxtrans) {
                JOptionPane.showMessageDialog(null,"The maximum number of transitions is reached ("+mxtrans+")");
                return;
            }

            // choose one transition
            String s = (String)JOptionPane.showInputDialog( null, "Choose a transition :"+lnsep, "Add Transition",
                                                            JOptionPane.PLAIN_MESSAGE, null, ntransp, ntransp[0]);
            // choice cancelled
            if( s == null ) {
                return;
            }

            // 1st ? initialize
            if( nbtrans == 0 ) {
                Arrays.fill(transpisu, false);
            }

            ntrans[nbtrans] = (nbtrans+1)+"_"+s;

            // set ntransp index and transpisu
            for(ictransp=0; ictransp<nbtransp; ictransp++) {
                if(s.equals(ntransp[ictransp])) {
                    itransp[nbtrans] = ictransp;
                    transpisu[ictransp] = true;
                    // set polyads associated to this transition as used
                    polisu[iinf[ictransp]] = true;
                    polisu[isup[ictransp]] = true;
                    // allow jcbjpol of associated polyads
                    jcbjpol[isup[ictransp]].setEnabled(true);
                    jcbjpol[iinf[ictransp]].setEnabled(true);
                    // set number of dev.order of this transition
                    if( fitint ) {
                        trm_nbdvo[nbtrans] = transp_nbdvo[ictransp];
                    }
                    break;
                }
            }
            // create panel
            pcnx[nbtrans] = new JPanel(new BorderLayout());
            if( fitint ) {
                pcnxo[nbtrans] = new JPanel(new GridLayout(6,0,5,5));
                pcnxc[nbtrans] = new JPanel(new GridLayout(6,0,5,5));
                pcnxc1[nbtrans] = new JPanel(new GridLayout(0,2*trm_nbdvo[nbtrans],5,5));
                pcnxc2[nbtrans] = new JPanel(new GridLayout(0,6,5,5));
                pcnxc4[nbtrans] = new JPanel(new GridLayout(0,6,5,5));
            }
            else {
                pcnxo[nbtrans] = new JPanel(new GridLayout(4,0,5,5));
                pcnxc[nbtrans] = new JPanel(new GridLayout(4,0,5,5));
            }
            pcnxc3[nbtrans] = new JPanel(new GridLayout(0,6,5,5));
            // Assignment File
            jbasgf[nbtrans] = new JButton("ASG File or Dir.");
            jbasgf[nbtrans].setBackground(Color.WHITE);
            jbasgf[nbtrans].addActionListener(this);
            pcnxo[nbtrans].add(jbasgf[nbtrans]);
            nasgf[nbtrans] = "";
            lasgf[nbtrans]=new JLabel(nasgf[nbtrans]);
            pcnxc[nbtrans].add(lasgf[nbtrans]);
            // Selection String
            pcnxo[nbtrans].add(new JLabel("Sel. String "));
            jtfselstr[nbtrans] = new JTextField(6);
            pcnxc[nbtrans].add(jtfselstr[nbtrans]);
            if( fitint ) {
                // Development Order
                pcnxo[nbtrans].add(new JLabel("Devel. Order"));
                trm_jcbdvo[nbtrans] = new JComboBox[trm_nbdvo[nbtrans]];
                trm_vdvo[nbtrans]   = new int[trm_nbdvo[nbtrans]];
                xmy=polyads[isup[itransp[nbtrans]]].getPolnb()-polyads[iinf[itransp[nbtrans]]].getPolnb();
                for( int j=0; j<trm_nbdvo[nbtrans]; j++) {
                    trm_jcbdvo[nbtrans][j] = new JComboBox();
                    trm_jcbdvo[nbtrans][j].addItem("");
                    for( int k=0; k<tdvo.length; k++ ) {
                        trm_jcbdvo[nbtrans][j].addItem(tdvo[k]);
                    }
                    trm_jcbdvo[nbtrans][j].setSelectedItem("");
                    trm_jcbdvo[nbtrans][j].setBackground(Color.WHITE);
                    pcnxc1[nbtrans].add(new JLabel("P"+(j+xmy)+"mP"+j+" D ",null,JLabel.RIGHT));
                    pcnxc1[nbtrans].add(trm_jcbdvo[nbtrans][j]);
                }
                // pre-initialize trm_jcbdvo (if possible)
                if( nbtrans > 0) {
                    for(ictrans=0; ictrans<nbtrans; ictrans++) {
                        cxmy=polyads[isup[itransp[ictrans]]].getPolnb()-polyads[iinf[itransp[ictrans]]].getPolnb();
                        if( cxmy == xmy ) {
                            for( int j=0; j<Math.min(trm_nbdvo[ictrans],trm_nbdvo[nbtrans]); j++) {
                                trm_jcbdvo[nbtrans][j].setSelectedItem(String.valueOf(trm_vdvo[ictrans][j]));
                            }
                        }
                    }
                }
                pcnxc1[nbtrans].setBorder(BorderFactory.createLineBorder(Color.BLACK));
                pcnxc[nbtrans].add(pcnxc1[nbtrans],"Center");
                // Temperature
                pcnxc2[nbtrans].add(new JLabel("Temperature ",null,JLabel.RIGHT));
                jftftemp[nbtrans] = new JFormattedTextField(nffloat);
                jftftemp[nbtrans].setValue(new Float(0.0));
                jftftemp[nbtrans].setBackground(Color.WHITE);
                pcnxc2[nbtrans].add(jftftemp[nbtrans]);
                // RINMI
                pcnxc2[nbtrans].add(new JLabel("RINMI ",null,JLabel.RIGHT));
                jtfrinmi[nbtrans] = new JTextField("");
                jtfrinmi[nbtrans].setBackground(Color.WHITE);
                pcnxc2[nbtrans].add(jtfrinmi[nbtrans]);
                pcnxc2[nbtrans].setBorder(BorderFactory.createLineBorder(Color.BLACK));
                pcnxc[nbtrans].add(pcnxc2[nbtrans],"Center");
                pcnxo[nbtrans].add(new JLabel(""));
            }
            else {
                pcnxo[nbtrans].add(new JLabel(""));
                pcnxc[nbtrans].add(new JLabel(""));
            }
            // Options
            jrblpcmax[nbtrans] = new JRadioButton("Initial Basis Components ");
            jrblpcmax[nbtrans].setBackground(Color.WHITE);
            jrblpcmax[nbtrans].addActionListener(this);
            pcnxo[nbtrans].add(jrblpcmax[nbtrans]);
            jcblpcmax[nbtrans] = new JComboBox();
            jcblpcmax[nbtrans].addItem("");
            for( int j=0; j<tlpcmax.length; j++ ) {
                jcblpcmax[nbtrans].addItem(tlpcmax[j]);
            }
            jcblpcmax[nbtrans].setSelectedItem("");
            jcblpcmax[nbtrans].setBackground(Color.WHITE);
            jcblpcmax[nbtrans].setEnabled(false);
            pcnxc3[nbtrans].add(jcblpcmax[nbtrans]);
            jrbprec[nbtrans] = new JRadioButton("prec");
            jrbprec[nbtrans].setBackground(Color.WHITE);
            jrbprec[nbtrans].setBorder(BorderFactory.createLineBorder(Color.BLACK));
            jrbprec[nbtrans].setSelected(true);
            pcnxc3[nbtrans].add(jrbprec[nbtrans]);
            pcnxc3[nbtrans].add(new JLabel(""));
            jrbunit[nbtrans] = new JRadioButton("unit");
            jrbunit[nbtrans].setBackground(Color.WHITE);
            jrbunit[nbtrans].setBorder(BorderFactory.createLineBorder(Color.BLACK));
            jrbunit[nbtrans].addActionListener(this);
            jrbMHz[nbtrans] = new JRadioButton("MHz");
            jrbMHz[nbtrans].setBorder(BorderFactory.createLineBorder(Color.BLACK));
            jrbMHz[nbtrans].setEnabled(false);
            jrbGHz[nbtrans] = new JRadioButton("GHz");
            jrbGHz[nbtrans].setBorder(BorderFactory.createLineBorder(Color.BLACK));
            jrbGHz[nbtrans].setEnabled(false);
            jrbdum[nbtrans] = new JRadioButton();
            bgunit[nbtrans] = new ButtonGroup();
            bgunit[nbtrans].add(jrbMHz[nbtrans]);
            bgunit[nbtrans].add(jrbGHz[nbtrans]);
            bgunit[nbtrans].add(jrbdum[nbtrans]);
            pcnxc3[nbtrans].add(jrbunit[nbtrans]);
            pcnxc3[nbtrans].add(jrbMHz[nbtrans]);
            pcnxc3[nbtrans].add(jrbGHz[nbtrans]);
            if( fitint ) {
                jrbrmxomc[nbtrans] = new JRadioButton("Intensity Relative Threshold");
                jrbrmxomc[nbtrans].setBackground(Color.WHITE);
                jrbrmxomc[nbtrans].addActionListener(this);
                pcnxo[nbtrans].add(jrbrmxomc[nbtrans]);
                jcbrmxomc[nbtrans] = new JComboBox();
                jcbrmxomc[nbtrans].addItem("");
                for( int j=0; j<trmxomc.length; j++ ) {
                    jcbrmxomc[nbtrans].addItem(trmxomc[j]);
                }
                jcbrmxomc[nbtrans].setSelectedItem("");                           // default to space
                jcbrmxomc[nbtrans].setBackground(Color.WHITE);
                jcbrmxomc[nbtrans].setEnabled(false);
                pcnxc4[nbtrans].add(jcbrmxomc[nbtrans]);
                jrbfpvib[nbtrans] = new JRadioButton("fpvib");
                jrbfpvib[nbtrans].setBackground(Color.WHITE);
                jrbfpvib[nbtrans].setBorder(BorderFactory.createLineBorder(Color.BLACK));
                jrbfpvib[nbtrans].addActionListener(this);
                pcnxc4[nbtrans].add(jrbfpvib[nbtrans]);
                jtffpvib[nbtrans] = new JTextField("");
                jtffpvib[nbtrans].setEnabled(false);
                jtffpvib[nbtrans].setBackground(this.getBackground());
                pcnxc4[nbtrans].add(jtffpvib[nbtrans]);
                jrbabund[nbtrans] = new JRadioButton("abund");
                jrbabund[nbtrans].setBackground(Color.WHITE);
                jrbabund[nbtrans].setBorder(BorderFactory.createLineBorder(Color.BLACK));
                jrbabund[nbtrans].addActionListener(this);
                pcnxc4[nbtrans].add(jrbabund[nbtrans]);
                jtfabund[nbtrans] = new JTextField("");
                jtfabund[nbtrans].setEnabled(false);
                jtfabund[nbtrans].setBackground(this.getBackground());
                pcnxc4[nbtrans].add(jtfabund[nbtrans]);
            }
            pcnxc[nbtrans].add(pcnxc3[nbtrans],"Center");
            if( fitint ) {
                pcnxc[nbtrans].add(pcnxc4[nbtrans],"Center");
            }

            pcnx[nbtrans].add(pcnxo[nbtrans],"West");
            pcnx[nbtrans].add(pcnxc[nbtrans],"Center");
            pcnx[nbtrans].setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLUE,4),ntrans[nbtrans]));
            pcn.add(pcnx[nbtrans]);
            pcn.revalidate();
            // go to bottom to show the last transition panel
            SwingUtilities.invokeLater(new Runnable() {
                 public void run() {
                     final JScrollBar jsbv = jsp.getVerticalScrollBar();
                     jsbv.setValue(jsbv.getMaximum());
                 }
            });
            //
            nbtrans++;
            return;
        }

        // Jmax
        if( nbpol != 0 ) {
            for (int icpol=0; icpol<nbpol; icpol++) {
                if(evt.getSource() == jcbjpol[icpol] ) {
                    njpol = (String) jcbjpol[icpol].getSelectedItem();
                    if(njpol != "") {
                        vjpol[icpol] = Double.valueOf(njpol);
                    }
                    return;
                }
            }
        }

        // ASG file
        if(jbasgf != null) {
            for(ictrans=0; ictrans<nbtrans; ictrans++) {
                if(evt.getSource() == jbasgf[ictrans]) {
                    JFileChooser jfcasgf = new JFileChooser(playd+fisep+"packages"+fisep+npack+fisep+"exp"+fisep+nmol);  // default choice directory
                    jfcasgf.setSize(400,300);
                    jfcasgf.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);  // files and directories
                    jfcasgf.setDialogTitle("Define the assignment file or directory to be used");
                    Container parent = jbasgf[ictrans].getParent();
                    int choice = jfcasgf.showDialog(parent,"Select");  // Dialog, Select
                    if (choice == JFileChooser.APPROVE_OPTION) {
                        nasgf[ictrans]= jfcasgf.getSelectedFile().getAbsolutePath();
                        lasgf[ictrans].setText(nasgf[ictrans]);
                    }
                    return;
                }
            }
        }

        // OPTION unit
        if( jrbunit != null ) {
            for(ictrans=0; ictrans<nbtrans; ictrans++) {
                if(evt.getSource() == jrbunit[ictrans]) {
                    if(jrbunit[ictrans].isSelected()) {
                        jrbMHz[ictrans].setEnabled(true);
                        jrbMHz[ictrans].setBackground(Color.WHITE);
                        jrbGHz[ictrans].setEnabled(true);
                        jrbGHz[ictrans].setBackground(Color.WHITE);
                    }
                    else {
                        jrbdum[ictrans].setSelected(true);
                        jrbMHz[ictrans].setEnabled(false);
                        jrbMHz[ictrans].setBackground(this.getBackground());
                        jrbGHz[ictrans].setEnabled(false);
                        jrbGHz[ictrans].setBackground(this.getBackground());
                    }
                    return;
                }
            }
        }

        // OPTION mix LPCMAX
        if(jrblpcmax != null) {
            for(ictrans=0; ictrans<nbtrans; ictrans++) {
                if(evt.getSource() == jrblpcmax[ictrans]) {
                    if(jrblpcmax[ictrans].isSelected()) {
                        jcblpcmax[ictrans].setEnabled(true);
                    }
                    else {
                        jcblpcmax[ictrans].setSelectedItem("");
                        jcblpcmax[ictrans].setEnabled(false);
                    }
                    return;
                }
            }
        }

        // OPTION RMXOMC
        if(jrbrmxomc != null) {
            for(ictrans=0; ictrans<nbtrans; ictrans++) {
                if(evt.getSource() == jrbrmxomc[ictrans]) {
                    if(jrbrmxomc[ictrans].isSelected()) {
                        jcbrmxomc[ictrans].setEnabled(true);
                    }
                    else {
                        jcbrmxomc[ictrans].setSelectedItem("");
                        jcbrmxomc[ictrans].setEnabled(false);
                    }
                    return;
                }
            }
        }


        // OPTION fpvib
        if(jrbfpvib != null) {
            for(ictrans=0; ictrans<nbtrans; ictrans++) {
                if(evt.getSource() == jrbfpvib[ictrans]) {
                    if(jrbfpvib[ictrans].isSelected()) {
                        jtffpvib[ictrans].setEnabled(true);
                        jtffpvib[ictrans].setBackground(Color.WHITE);
                    }
                    else {
                        jtffpvib[ictrans].setText("");
                        jtffpvib[ictrans].setEnabled(false);
                        jtffpvib[ictrans].setBackground(this.getBackground());
                    }
                    return;
                }
            }
        }

        // OPTION abund
        if(jrbabund != null) {
            for(ictrans=0; ictrans<nbtrans; ictrans++) {
                if(evt.getSource() == jrbabund[ictrans]) {
                    if(jrbabund[ictrans].isSelected()) {
                        jtfabund[ictrans].setEnabled(true);
                        jtfabund[ictrans].setBackground(Color.WHITE);
                    }
                    else {
                        jtfabund[ictrans].setText("");
                        jtfabund[ictrans].setEnabled(false);
                        jtfabund[ictrans].setBackground(this.getBackground());
                    }
                    return;
                }
            }
        }

        // REMOVE LAST TRANSITION
        if(evt.getSource() == jbremlasttrans) {
            if(nbtrans != 0) {
                // reset transpisu
                for(ictransp=0; ictransp<nbtransp; ictransp++) {
                    transpisu[ictransp] = false;
                }
                // reset polisu, jcbpol
                String[] cjpol = new String[nbpol];                  // save previous
                for(icpol=0; icpol<nbpol; icpol++) {
                    polisu[icpol] = false;
                    cjpol[icpol]  = (String) jcbjpol[icpol].getSelectedItem();
                    jcbjpol[icpol].setSelectedItem("");
                    jcbjpol[icpol].setEnabled(false);
                }
                if( nbtrans > 1 ) {
                    // set transpisu
                    for(ictrans=0; ictrans<nbtrans-1; ictrans++) {
                        ictransp            = itransp[ictrans];
                        transpisu[ictransp] = true;
                        int icsup           = isup[ictransp];
                        polisu[icsup]       = true;
                        int icinf           = iinf[ictransp];
                        polisu[icinf]       = true;
                        if( ! cjpol[icsup].equals("") ) {
                            jcbjpol[icsup].setSelectedItem(cjpol[icsup]);
                            jcbjpol[icsup].setEnabled(true);
                        }
                        if( ! cjpol[icinf].equals("") ) {
                            jcbjpol[icinf].setSelectedItem(cjpol[icinf]);
                            jcbjpol[icinf].setEnabled(true);
                        }
                    }
                }
                // suppress transition panel
                pcn.remove(pcnx[nbtrans-1]);
                pcn.repaint();
                nbtrans --;
            }
            return;
        }
    }

    /**
     * Manages JPPolyad event.
     */
    public void JPPEvent( Polyad curpol, int type, int indice ) {

        if( ! testBasics() ) {                                       // basics checked ?
            askBasics();                                             // no, ask for
            for(int i=0; i<nbpol; i++ ) {
                if( curpol == polyads[i] ) {
                    jpps[i].resetJP();
                    break;
                }
            }
        }
    }

/////////////////////////////////////////////////////////////////////

    // display polyad in a window
    private void showPol() {

        sbPol();
        JOptionPane.showMessageDialog(null,sb.toString(),"POLYADS",JOptionPane.PLAIN_MESSAGE);
    }

    // create text
    private void sbPol() {

        sb  = new StringBuffer();
        for(int jcpol=nbpol-1; jcpol>=0; jcpol--) {                  // main polyad decreasing order
            if( polyads[ipolnb[jcpol]].isFullydef() ) {
                polyads[ipolnb[jcpol]].toSb( sb );
            }
        }
        sb.append(lnsep+lnsep+lnsep);
    }

/////////////////////////////////////////////////////////////////////

    // constraint file creation
    private void createConsf() {

        int pos = 0;
        if(  nparar.startsWith("Pa_") ) {                            // standard name
            pos = 3;
        }
        if( fitint ) {
            nconsf="Cl_int_"+nparar.substring(pos);
        }
        else {
            nconsf="Cl_freq_"+nparar.substring(pos);
        }
        JFileChooser jfcconsf = new JFileChooser(workd);
        jfcconsf.setSize(400,300);
        jfcconsf.setFileSelectionMode(JFileChooser.FILES_ONLY);
        jfcconsf.setDialogTitle("Define the constraint file to be created");
        jfcconsf.setSelectedFile(new File(nconsf));
        int choice = jfcconsf.showSaveDialog(this);
        if (choice == JFileChooser.APPROVE_OPTION) {
            nconsf = jfcconsf.getSelectedFile().getAbsolutePath();   // job file name
        }
        else {
            return;
        }
        // command text
        sbcmd = new StringBuffer();
        sbcmd.append("cd "+playd+fisep+"tempo ; ");
        sbcmd.append(playd+fisep+"packages"+fisep+npack+fisep+"prog"+fisep+"exe"+fisep+"passx ctrpmk "+nparaf+" "+nconsf);
        if( fitint ) {
            sbcmd.append(" int");
        }
        else {
            sbcmd.append(" freq");
        }
        sbcmd.append(" >ctrpmk.out 2>&1");
        ncmd = sbcmd.toString();
        // run command in a shell
        try {
            String[] cmd = {"/bin/sh", "-c", ncmd};
            Process monproc = Runtime.getRuntime().exec(cmd);
        }
        catch (IOException ioe) {
            JOptionPane.showMessageDialog(null,"IO error while running command"+lnsep+
                                               ncmd                            +lnsep+
                                               ioe);
            return;
        }
        //
        JOptionPane.showMessageDialog(null,"The job for constraint file creation has been launched"+lnsep+
                                           "The log file of the job is: "+playd+fisep+"tempo"+fisep+"ctrpmk.out");
    }

/////////////////////////////////////////////////////////////////////

    // save jobs
    private void saveCJP() {

        // check if everything is defined
        if( ! testAllTrans()) {
            return;
        }
        // choose job_hme name
        JFileChooser jfcjobf = new JFileChooser(workd);
        jfcjobf.setSize(400,300);
        jfcjobf.setFileSelectionMode(JFileChooser.FILES_ONLY);
        jfcjobf.setDialogTitle("Define the hamiltonian matrix elements job file to be created");
        jfcjobf.setSelectedFile(new File("job_hme_"));
        int choice = jfcjobf.showSaveDialog(this);
        if (choice == JFileChooser.APPROVE_OPTION) {
            File f = jfcjobf.getSelectedFile();                      // job file
            njobf  = f.getAbsolutePath();                            // job file name
            njobr  = f.getName();                                    // job file name (reduced)
            if( ! njobr.startsWith("job_hme_") ) {
                JOptionPane.showMessageDialog(null,"The hamiltonian matrix elements job file name must start with"+lnsep+
                                                   "job_hme_");
                return;
            }
        }
        else {                                                       // name NOT chosen
            return;
        }
        // create job_hme
        try {
            out1 = new PrintWriter(new BufferedWriter(new FileWriter(njobf)));  // write job

            out1.println("#! /bin/sh");
            out1.println(" set -v");
            out1.println("##");
            out1.println("## Calculation of the matrix elements (job created by XTDS)");
            out1.print  ("##");
            for(icpol=0; icpol<nbpol; icpol++) {
                if( polisu[ipolnb[icpol]] ) {
                    out1.print  (" P"+polyads[ipolnb[icpol]].getPolnb());
                }
            }
            out1.println(" of "+nmol+".");
            out1.println("##");
            out1.println("BASD="+playd+fisep+"packages"+fisep+npack);
            out1.println("##");
            out1.println(" SCRD=$BASD"+fisep+"prog"+fisep+"exe");
            out1.println(" PARD=$BASD"+fisep+"para"+fisep+nmol);
            out1.println("##");
            out1.println("## Jmax values.");
            out1.println("##");
            for(icpol=0; icpol<nbpol; icpol++) {
                if( polisu[ipolnb[icpol]] ) {
                    out1.print  (" JP"+polyads[ipolnb[icpol]].getPolnb()+"=");
                    if( npack.equals("C3vsTDS") ) {
                        out1.println(""+vjpol[ipolnb[icpol]]);
                    }
                    else {
                        out1.println(""+(int) vjpol[ipolnb[icpol]]);
                    }
                }
            }
            if( fitint ) {
                for(ictransp=0; ictransp<nbtransp; ictransp++) {
                    if( transpisu[ictransp] ) {
                        out1.println(" J"+ntransp[ictransp]+"=$JP"+polyads[isup[ictransp]].getPolnb());
                    }
                }
            }
            out1.println("##");
            out1.println("## Parameter file.");
            out1.println("##");
            out1.println(" SPARA="+nparar);
            out1.println(" PARA="+nparaf);
            out1.println(" WPARA=$SPARA\"_work\"");
            out1.println(" \\cp $PARA $WPARA");
            out1.println("##");
            out1.println("#################################################");
            out1.println("##");
            out1.println("## Hamiltonian matrix elements.");
            out1.println("##");
            for(icpol=0; icpol<nbpol; icpol++) {
                if( polisu[ipolnb[icpol]] ) {
                    cur_polnb = polyads[ipolnb[icpol]].getPolnb();
                    out1.println("## P"+cur_polnb+" polyad.");
                    out1.println("##");
                    out1.print  (" $SCRD"+fisep+"passx hmodel P"+cur_polnb);
                    polyads[ipolnb[icpol]].ecrPol(out1, true);
                    if( npack.equals("D2hTDS") ) {
                        out1.println(" \\"+lnsep+
                                     "                    "+nrep);
                    }
                    else {
                        out1.println("");
                    }
                    // parchk
                    if( cur_polnb == spolnb[nbpol-1] ) {
                        out1.print  (" $SCRD"+fisep+"passx parchk P"+cur_polnb+"   ");
                        if(polyads[ipolnb[icpol]].getNbvs(cur_polnb) < 10) {
                            out1.print  (" ");
                        }
                        out1.print  (" D");
                        for(icpols=0; icpols<cur_polnb+1; icpols++) {
                            out1.print  (polyads[ipolnb[icpol]].getVdvo(icpols));
                        }
                        out1.println(" $PARD $PARA");
                    }
                    //
                    out1.print  (" $SCRD"+fisep+"passx rovbas P"+cur_polnb+" N"+polyads[ipolnb[icpol]].getNbvs(cur_polnb));
                    if(polyads[ipolnb[icpol]].getNbvs(cur_polnb) < 10) {
                        out1.print  (" ");
                    }
                    out1.print  (" D");
                    for(icpols=0; icpols<cur_polnb+1; icpols++) {
                        out1.print  (polyads[ipolnb[icpol]].getVdvo(icpols));
                    }
                    out1.println(" $JP"+cur_polnb);
                    out1.print  (" $SCRD"+fisep+"passx hmatri P"+cur_polnb+" N"+polyads[ipolnb[icpol]].getNbvs(cur_polnb));
                    if(polyads[ipolnb[icpol]].getNbvs(cur_polnb) < 10) {
                        out1.print  (" ");
                    }
                    out1.print  (" D");
                    for(icpols=0; icpols<cur_polnb+1; icpols++) {
                        out1.print  (polyads[ipolnb[icpol]].getVdvo(icpols));
                    }
                    out1.println(" $JP"+cur_polnb);
                    out1.println("##");
                }
            }
            if( fitint ) {
                out1.println("#################################################");
                out1.println("##");
                if( jrbpol.isSelected() ) {
                    out1.println("## Polarizability matrix elements.");
                }
                else {
                    out1.println("## Dipole moment matrix elements.");
                }
                out1.println("##");
                for(ictransp=0; ictransp<nbtransp; ictransp++) {
                    if( transpisu[ictransp] ) {
                        out1.println("## "+ntransp[ictransp]+" transition;");
                        out1.println("##");
                        if( jrbpol.isSelected() ) {
                            out1.print  (" $SCRD"+fisep+"passx polmod");
                        }
                        else {
                            out1.print  (" $SCRD"+fisep+"passx dipmod");
                        }
                        sup_polnb = polyads[isup[ictransp]].getPolnb();
                        out1.print  (" P"+sup_polnb);
                        polyads[isup[ictransp]].ecrPol(out1, false);
                        out1.println(" \\");
                        out1.print  ("                   ");
                        inf_polnb = polyads[iinf[ictransp]].getPolnb();
                        out1.print  (" P"+inf_polnb);
                        polyads[iinf[ictransp]].ecrPol(out1, false);
                        out1.println(" \\");
                        out1.print  ("                   ");
                        out1.print  (" D");
                        for( int j=0; j<transp_nbdvo[ictransp]; j++) {
                            out1.print  (transp_vdvo[ictransp][j]);
                        }
                        if( npack.equals("D2hTDS") ) {
                            out1.println(" \\"+lnsep+
                                         "                    "+nrep);
                        }
                        else {
                            out1.println("");
                        }
                        // parchk
                        out1.print  (" $SCRD"+fisep+"passx parchk");
                        out1.print  (" P"+sup_polnb);
                        out1.print  ("   ");
                        if(polyads[isup[ictransp]].getNbvs(sup_polnb) < 10) {
                            out1.print  (" ");
                        }
                        out1.print  (" D");
                        for(icpols=0; icpols<sup_polnb+1; icpols++) {
                            out1.print  (polyads[isup[ictransp]].getVdvo(icpols));
                        }
                        out1.print  (" P"+inf_polnb);
                        out1.print  ("   ");
                        if(polyads[iinf[ictransp]].getNbvs(inf_polnb) < 10) {
                            out1.print  (" ");
                        }
                        out1.print  (" D");
                        for( int j=0; j<transp_nbdvo[ictransp]; j++) {
                            out1.print  (transp_vdvo[ictransp][j]);
                        }
                        if( jrbpol.isSelected() ) {
                            out1.print  (" pol");
                        }
                        else {
                            out1.print  (" dip");
                        }
                        out1.println(" $PARD $PARA");
                        //
                        if( jrbpol.isSelected() ) {
                            out1.print  (" $SCRD"+fisep+"passx polmat");
                        }
                        else {
                            out1.print  (" $SCRD"+fisep+"passx dipmat");
                        }
                        out1.print  (" P"+sup_polnb+" N"+polyads[isup[ictransp]].getNbvs(sup_polnb));
                        if(polyads[isup[ictransp]].getNbvs(sup_polnb) < 10) {
                            out1.print  (" ");
                        }
                        for(icpols=0; icpols<sup_polnb+3; icpols++) {
                            out1.print  (" ");
                        }
                        out1.print  (" P"+inf_polnb+" N"+polyads[iinf[ictransp]].getNbvs(inf_polnb));
                        if(polyads[iinf[ictransp]].getNbvs(inf_polnb) < 10) {
                            out1.print  (" ");
                        }
                        out1.print  (" D");
                        for( int j=0; j<transp_nbdvo[ictransp]; j++) {
                            out1.print  (transp_vdvo[ictransp][j]);
                        }
                        out1.println(" $J"+ntransp[ictransp]);
                        out1.println("##");
                    }
                }
            }
            out1.println(" \\rm FN_* MH_*");
            out1.println(" if [ -f para_variance.t ] ; then");
            out1.println("   \\rm para_variance.t");
            out1.println(" fi");
            if( fitint ) {
                if( jrbpol.isSelected() ) {
                    out1.println(" \\rm MP_*");
                }
                else {
                    out1.println(" \\rm MD_*");
                }
            }
            out1.println("");
        }
        catch (IOException ioe) {                                    // IO error
            JOptionPane.showMessageDialog(null,"IO error while writing file"+lnsep+
                                               njobf                        +lnsep+
                                               ioe);
            return;
        }
        finally {
            if( out1 != null ) {
                out1.close();
                if( out1.checkError() ) {
                    JOptionPane.showMessageDialog(null,"PrintWriter error while creating file"+lnsep+
                                                       njobf);
                    return;
                }
            }
        }
        JOptionPane.showMessageDialog(null,"The job "+njobf+" has been created.");
        try {
            Process monproc = Runtime.getRuntime().exec("chmod u+x "+njobf);  // allow execution
        }
        catch (IOException ioe) {                                    // rights modification error
                JOptionPane.showMessageDialog(null,"IO error while changing rights of file"+lnsep+
                                                   njobf                                   +lnsep+
                                                   ioe);
                return;
        }

        // choose job_fit name
        f = new File(njobf);
        njobr = "job_fit_"+njobr.substring(8);                       // job_hme_ -> job_fit_
        njobf = f.getParent()+fisep+njobr;
        // create job_fit
        try {
            out1 = new PrintWriter(new BufferedWriter(new FileWriter(njobf)));  // write job

            out1.println("#! /bin/sh");
            out1.println(" set -v");
            out1.println("##");
            if( fitint ) {
                out1.print  ("## Intensity");
            }
            else {
                out1.print  ("## Frequency");
            }
            out1.println(" parameter fitting (job created by XTDS)");
            out1.print  ("##");
            for(ictransp=0; ictransp<nbtransp; ictransp++ ) {
                if( transpisu[ictransp] ) {
                    out1.print  (" "+ntransp[ictransp]);
                }
            }
            out1.println(" of "+nmol+".");
            out1.println("##");
            out1.println("BASD="+playd+fisep+"packages"+fisep+npack);
            out1.println("##");
            out1.println(" SCRD=$BASD"+fisep+"prog"+fisep+"exe");
            out1.println("##");
            out1.println("## Jmax values.");
            out1.println("##");
            for(icpol=0; icpol<nbpol; icpol++) {
                if( polisu[ipolnb[icpol]] ) {
                    out1.print  (" JP"+polyads[ipolnb[icpol]].getPolnb()+"=");
                    if( npack.equals("C3vsTDS") ) {
                        out1.println(""+vjpol[ipolnb[icpol]]);
                    }
                    else {
                        out1.println(""+(int) vjpol[ipolnb[icpol]]);
                    }
                }
            }
            for(ictransp=0; ictransp<nbtransp; ictransp++) {
                if( transpisu[ictransp] ) {
                    out1.println(" J"+ntransp[ictransp]+"=$JP"+polyads[isup[ictransp]].getPolnb());
                }
            }
            out1.println("##");
            out1.println("## Parameter file.");
            out1.println("##");
            out1.println(" SPARA="+nparar);
            out1.println(" WPARA=$SPARA\"_work\"");
            out1.println("##");
            out1.println("## Assignment files.");
            out1.println("##");
            for(ictrans=0; ictrans<nbtrans; ictrans++) {
                f = new File(nasgf[ictrans]);
                if( f.isDirectory() ) {
                    out1.println(" ASG_"+ntrans[ictrans]+"=`ls "+nasgf[ictrans]+fisep+"*`");
                }
                else {
                    out1.println(" ASG_"+ntrans[ictrans]+"="+nasgf[ictrans]);
                }
            }
            out1.println("##");
            out1.println("## Parameter constraint file.");
            out1.println("##");
            out1.println(" CLF="+nconsf);
            out1.println("##");
            out1.println(" PARA=$SPARA\"_nulpar\"");
            out1.print  (" $SCRD"+fisep+"passx nulpar");
            if( fitint ) {
                out1.print  (" T");
            }
            else {
                out1.print  (" H");
            }
            out1.println(" $CLF $WPARA $PARA");
            out1.println("#################################################");
            out1.println("##");
            out1.println("## Hamiltonian diagonalization.");
            out1.println("##");
            out1.print  ("##");
            for(icpol=0; icpol<nbpol; icpol++) {
                if( polisu[ipolnb[icpol]] ) {
                    out1.print  (" P"+polyads[ipolnb[icpol]].getPolnb());
                }
            }
            out1.println(" Levels.");
            out1.println("##");
            for(icpol=0; icpol<nbpol; icpol++) {
                if( polisu[ipolnb[icpol]] ) {
                    cur_polnb = polyads[ipolnb[icpol]].getPolnb();
                    out1.print  (" $SCRD"+fisep+"passx hdiag  P"+cur_polnb+" N"+polyads[ipolnb[icpol]].getNbvs(cur_polnb));
                    if(polyads[ipolnb[icpol]].getNbvs(cur_polnb) < 10) {
                        out1.print  (" ");
                    }
                    out1.print  (" D");
                    for(icpols=0; icpols<cur_polnb+1; icpols++) {
                         out1.print  (polyads[ipolnb[icpol]].getVdvo(icpols));
                    }
                    out1.println(" $JP"+cur_polnb+" $PARA");
                }
            }
            if( fitint ) {
                out1.println("##");
                out1.println("#################################################");
                out1.println("##");
                out1.println("## Transition moments.");
                out1.println("##");
                out1.print  ("##");
                for(ictransp=0; ictransp<nbtransp; ictransp++) {
                    if( transpisu[ictransp] ) {
                        out1.print  (" "+ntransp[ictransp]);
                    }
                }
                out1.println(" Transitions.");
                out1.println("##");
                for(ictransp=0; ictransp<nbtransp; ictransp++) {
                    if( transpisu[ictransp] ) {
                        out1.print  (" $SCRD"+fisep+"passx trmomt");
                        sup_polnb = polyads[isup[ictransp]].getPolnb();
                        out1.print  (" P"+sup_polnb+" N"+polyads[isup[ictransp]].getNbvs(sup_polnb));
                        if( polyads[isup[ictransp]].getNbvs(sup_polnb) < 10) {
                            out1.print  (" ");
                        }
                        inf_polnb = polyads[iinf[ictransp]].getPolnb();
                        out1.print  (" P"+inf_polnb+" N"+polyads[iinf[ictransp]].getNbvs(inf_polnb));
                        if( polyads[iinf[ictransp]].getNbvs(inf_polnb) < 10) {
                            out1.print  (" ");
                        }
                        out1.print  (" D");
                        for( int j=0; j<transp_nbdvo[ictransp]; j++) {
                            out1.print  (transp_vdvo[ictransp][j]);
                        }
                        out1.print  (" $J"+ntransp[ictransp]+" $PARA");
                        if( jrbpol.isSelected() ) {
                            out1.print  (" pol");
                            if( jrbpolst.isSelected() ) {
                                out1.println(" "+npolst);
                            }
                            else {
                                out1.println("");
                            }
                        }
                        else {
                            out1.println(" dip");
                        }
                    }
                }
            }
            out1.println("##");
            out1.println("#################################################");
            out1.println("##");
            out1.println("## Fit.");
            for(ictrans=0; ictrans<nbtrans; ictrans++) {
                out1.println("##");
                out1.println("## "+ntrans[ictrans]);
                out1.println("##");
                out1.println(" $SCRD"+fisep+"exasg '"+jtfselstr[ictrans].getText()+"' $ASG_"+ntrans[ictrans]);
                out1.println(" \\cp ASG_EXP ASG_"+ntrans[ictrans]+".t");
                out1.println(" \\mv ASG_EXP assignments.t");
                out1.println("##");
                if( fitint ) {
                    out1.print  (" $SCRD"+fisep+"passx eq_int");
                }
                else {
                    out1.print  (" $SCRD"+fisep+"passx eq_tds");
                }
                out1.print  (" P"+polyads[isup[itransp[ictrans]]].getPolnb()+" P"+polyads[iinf[itransp[ictrans]]].getPolnb());
                if( fitint ) {
                    out1.print  (" D");
                    for( int j=0; j<trm_nbdvo[ictrans]; j++) {
                        out1.print  (transp_vdvo[itransp[ictrans]][j]);
                    }
                    out1.print  (" "+vtemp[ictrans]+" "+vrinmi[ictrans]);
                }
                out1.print  (" $J"+ntransp[itransp[ictrans]]+" $PARA $CLF");
                if( jrbprec[ictrans].isSelected() ) {
                    out1.print  (" prec");
                }
                if( jrbMHz[ictrans].isSelected() ) {
                    out1.print  (" MHz");
                }
                else if( jrbGHz[ictrans].isSelected() ) {
                    out1.print  (" GHz");
                }
                if( jrblpcmax[ictrans].isSelected() ) {
                    out1.print  (" mix "+jcblpcmax[ictrans].getSelectedItem());
                }
                if( fitint ) {
                    if( jrbrmxomc[ictrans].isSelected() ) {
                        out1.print  (" thres "+jcbrmxomc[ictrans].getSelectedItem());
                    }
                    if( jrbfpvib[ictrans].isSelected() ) {
                        out1.print  (" fpvib "+vfpvib[ictrans]);
                    }
                    if( jrbabund[ictrans].isSelected() ) {
                        out1.print  (" abund "+vabund[ictrans]);
                    }
                }
                out1.println("");
                if( fitint ) {
                    out1.println(" \\mv normal_eq.t      NQ_T_"+ntrans[ictrans]);
                }
                else {
                    out1.println(" \\mv normal_eq.t      NQ_H_"+ntrans[ictrans]);
                }
                out1.println(" \\mv prediction.t     Pred_"+ntrans[ictrans]);
                out1.println(" \\mv prediction_mix.t Pred_mix_"+ntrans[ictrans]);
                out1.println(" \\mv statistics.t     Stat_"+ntrans[ictrans]);
            }
            out1.println("##");
            out1.println("#################################################");
            out1.println("##");
            if( fitint ) {
                out1.println("## New intensity parameter estimates.");
            }
            else {
                out1.println("## New Hamiltonian parameter estimates.");
            }
            out1.println("##");
            out1.print  (" $SCRD"+fisep+"passx paradj");
            if( fitint ) {
                out1.print  (" T");
            }
            else {
                out1.print  (" H");
            }
            for(ictrans=0; ictrans<nbtrans; ictrans++) {
                if( fitint ) {
                    out1.print  (" NQ_T_"+ntrans[ictrans]+" $PARA");
                }
                else {
                    out1.print  (" NQ_H_"+ntrans[ictrans]+" $PARA");
                }
                if(ictrans != (nbtrans-1) ) {
                    out1.println(" \\");
                    out1.print  ("                     ");
                }
                else {
                    out1.println("");
                }
            }
            out1.print  (" \\rm");
            for(ictrans=0; ictrans<nbtrans; ictrans++) {
                if( fitint ) {
                    out1.print  (" NQ_T_"+ntrans[ictrans]);
                }
                else {
                    out1.print  (" NQ_H_"+ntrans[ictrans]);
                }
            }
            out1.println("");
            out1.println("##");
            out1.println(" \\rm assignments.t");
            out1.print  (" \\rm");
            if( fitint ) {
                out1.print  (" CL_eqint");
            }
            else {
                out1.print  (" CL_eqtds");
            }
            out1.println(" ED_* VP_*");
            if( fitint ) {
                if( jrbpol.isSelected() ) {
                    out1.println(" \\rm TA_*");
                }
                else {
                    out1.println(" \\rm TD_*");
                }
            }
            out1.println("###");
            out1.println("### The new parameter files are :");
            out1.println("###");
            for(ictrans=0; ictrans<nbtrans; ictrans++) {
                if(fitint ) {
                    out1.println("###                    NQ_T_"+ntrans[ictrans]+"_adj");
                }
                else {
                    out1.println("###                    NQ_H_"+ntrans[ictrans]+"_adj");
                }
            }
            out1.println("###");
        }
        catch (IOException ioe) {                                    // IO error
            JOptionPane.showMessageDialog(null,"IO error while writing file"+lnsep+
                                               njobf                        +lnsep+
                                               ioe);
            return;
        }
        finally {
            if( out1 != null ) {
                out1.close();
                if( out1.checkError() ) {
                    JOptionPane.showMessageDialog(null,"PrintWriter error while creating file"+lnsep+
                                                       njobf);
                    return;
                }
            }
        }
        JOptionPane.showMessageDialog(null,"The job "+njobf+" has been created.");
        try {
            Process monproc = Runtime.getRuntime().exec("chmod u+x "+njobf);  // allow execution
        }
        catch (IOException ioe) {                                    // rights modification error
            JOptionPane.showMessageDialog(null,"IO error while changing rights of file"+lnsep+
                                               njobf                                   +lnsep+
                                               ioe);
            return;
        }
    }

/////////////////////////////////////////////////////////////////////

    // check Basics panel
    private boolean testBasics() {

        if( (!jrbint.isSelected()) && (!jrbfreq.isSelected()) ) {
            return false;                                            // freq int
        }
        npolst = (String) jcbpolst.getSelectedItem();
        if( jrbpolst.isSelected() && npolst == "" ) {
            return false;                                            // polst
        }
        if(nmol == "") {
            return false;                                            // molecule
        }
        if( npack.equals("D2hTDS") ) {
            nrep = (String) jcbrep.getSelectedItem();
            if(nrep == "") {
                return false;                                        // representation
            }
        }
        if(nparaf == "") {
            return false;                                            // parameter file name
        }
        if(nconsf == "") {
            return false;                                            // constraint file name
        }
        if( ! testPsc()) {
            return false;                                            // psc
        }
        return true;
    }

    // ask to fully define Basics
    private void askBasics() {

        JOptionPane.showMessageDialog(null,"You have to first fully define BASICS specifications");
        return;
    }

    // check polyad scheme coefficients
    private boolean testPsc() {

        for (int i=0; i<nbvqn; i++) {
            npsc = (String) jcbpsc[i].getSelectedItem();
            if (npsc == "") {
                return false;
            }
        }
        return true;
    }

    // check all polyads
    private boolean testAllPol() {

        // something to check ?
        if(nbpol == 0) {
            JOptionPane.showMessageDialog(null,"No polyad defined");
            return false;
        }
        // all fully defined ?
        for(icpol=0; icpol<nbpol; icpol++) {
            if( ! polyads[icpol].isFullydef() ) {
                JOptionPane.showMessageDialog(null,"You have to first fully define "+polyads[icpol].getName());
                return false;
            }
            polyads[icpol].calCvs(vpsc);
        }
        // each with a unique polyad number ?
        if(nbpol > 1) {
            for(icpol=0; icpol<nbpol-1; icpol++) {
                for(int jcpol=icpol+1; jcpol<nbpol; jcpol++) {
                    if(polyads[icpol].getPolnb() == polyads[jcpol].getPolnb()) {
                        JOptionPane.showMessageDialog(null,"All polyad numbers must differ one from each other"+lnsep+
                                                           "see "+polyads[icpol].getName()+" and "+polyads[jcpol].getName());
                        return false;
                    }
                }
            }
        }
        // development orders consistent ?
        icpolmax = 0;
        polnbmax = polyads[0].getPolnb();
        if(nbpol > 1) {
            // set higher polyad
            for(icpol=1; icpol<nbpol; icpol++) {
                if(polyads[icpol].getPolnb() > polnbmax) {
                    icpolmax = icpol;
                    polnbmax = polyads[icpol].getPolnb();
                }
            }
            // test dvo
            for(int i=0; i<polnbmax+1; i++) {                        // for each dvo of the higher polyad
                int cdvo = polyads[icpolmax].getVdvo(i);             // referent dvo
                for(icpol=0; icpol<nbpol; icpol++) {                 // for each polyad
                    if(i <= polyads[icpol].getPolnb()) {             // for each dvo of the checked polyad
                        if( cdvo != polyads[icpol].getVdvo(i) ) {    // compare dvo
                            if(icpol < icpolmax) {
                                JOptionPane.showMessageDialog(null,"Development order must be consistent"+lnsep+
                                                                   "P"+i+" in "+polyads[icpol].getName()+
                                                                   " and "+polyads[icpolmax].getName());
                            }
                            else {
                                JOptionPane.showMessageDialog(null,"Development order must be consistent"+lnsep+
                                                                   "P"+i+" in "+polyads[icpolmax].getName()+
                                                                   " and "+polyads[icpol].getName());
                            }
                            return false;
                        }
                    }
                }
            }
        }
        // sort polnb
        spolnb = new int[nbpol];                                     // sorted polnb
        ipolnb = new int[nbpol];                                     // polnb to spolnb indexes
        for( int i=0; i<nbpol; i++ ) {
            spolnb[i] = polyads[i].getPolnb();
        }
        Arrays.sort(spolnb);                                         // spolnb in increasing order
        for( int j=0; j<nbpol; j++) {                                // set indexes
            for(int k=0; k<nbpol; k++) {
                if(spolnb[j] == polyads[k].getPolnb()) {
                    ipolnb[j]=k;                                     // polyad number increasing order
                    break;                                           // polyad found, go to next one
                }
            }
        }
        // test if there is at least one vs per sub-polyad
        for(icpol=nbpol-1; icpol>=0; icpol--) {                      // main polyad decreasing order
            cur_polnb = polyads[ipolnb[icpol]].getPolnb();
            for(icpols=0; icpols<=cur_polnb; icpols++) {
                if( polyads[ipolnb[icpol]].getNbvs(icpols) == 0 ) {
                    sbPol();
                    sb.append("WARNING : your choice produces NO vibrational state for P"+icpols+" of "+polyads[ipolnb[icpol]].getName());
                    JOptionPane.showMessageDialog(null,sb.toString());
                }
            }
        }
        if( nbpol > 1 ) {

            // test if vs of sub-polyads are consistent between main polyads
            // ie. compare sub-polyads of     lower main polyads
            //     to      sub-polyads of THE upper main polyad
            //int ipolsup = ipolnb[nbpol-1];                           // index of       THE upper main polyad
            //for(icpol=0; icpol<nbpol-1; icpol++) {                   // for each           lower main polyad
            //                                                         // -1 because NOT the upper main polyad pointed by ipolnb[nbpol-1]
            //    int ipolinf = ipolnb[icpol];                         // index of           lower main polyad
            //    for (icpols=0; icpols<polyads[ipolinf].getPolnb()+1; icpols++) {  // for each sub-polyad
            //        if( polyads[ipolinf].getNbvs(icpols) != polyads[ipolsup].getNbvs(icpols) ) {  // same nb of vs ?
            //            sbPol();
            //            sb.append("Inconsistent number of vibrational states for P"+icpols);
            //            JOptionPane.showMessageDialog(null,sb.toString());
            //            return false;
            //        }
            //        if( ! polyads[ipolinf].getCvs(icpols).equals(polyads[ipolsup].getCvs(icpols)) ) {
            //            sbPol();
            //            sb.append("Inconsistent values of vibrational states for P"+icpols);
            //            JOptionPane.showMessageDialog(null,sb.toString());
            //            return false;
            //        }
            //    }
            //}

            // test if quanta limit are consistent between polyads
            int vqalp[] = new int[nbvqn];                            // previous vqal(s)
            int vqalc;                                               // current vqal
            int iopolp = ipolnb[0];                                  // index of previous ordered polyad
            int iopolc;                                              // index of current  ordered polyad
            for(int i=0; i<nbvqn; i++) {                             // initialize
                vqalp[i] = polyads[iopolp].getVqal(i);               // vqal of the lowest main polyad
            }
            for(icpol=1; icpol<nbpol; icpol++) {                     // for each main polyad except the 1st (lowest) one
                iopolc = ipolnb[icpol];                              // index of current ordered polyad
                for ( int i=0; i<nbvqn; i++) {                       // for each vqn
                    vqalc = polyads[iopolc].getVqal(i);
                    if( vqalc < vqalp[i] ) {                         // not allowed
                        JOptionPane.showMessageDialog(null,"Quanta limit of "+polyads[iopolc].getName()+lnsep+
                                                           "have to be >= to those of "+polyads[iopolp].getName());
                        return false;
                    }
                    vqalp[i] = vqalc;
                }
                iopolp = iopolc;
            }
        }
        // happy end
        return true;
    }

    // check full definition of all transitions
    private boolean testAllTrans() {

        if( nbtrans == 0 ) {
            JOptionPane.showMessageDialog(null,"You have to select at least one transition");
            return false;
        }
        for(ictrans=0; ictrans<nbtrans; ictrans++) {
            if( ! testTrans(ictrans) ) {
                return false;
            }
        }
        // check trm_vdvo consistency
        if( fitint ) {
            if( nbtrans > 1) {
                for(ictrans=0; ictrans<nbtrans-1; ictrans++) {
                    cxmy=polyads[isup[itransp[ictrans]]].getPolnb()-polyads[iinf[itransp[ictrans]]].getPolnb();
                    for( int i=ictrans+1; i<nbtrans; i++) {
                        xmy=polyads[isup[itransp[i]]].getPolnb()-polyads[iinf[itransp[i]]].getPolnb();
                        if( cxmy == xmy ) {
                            for( int j=0; j<Math.min(trm_nbdvo[ictrans],trm_nbdvo[i]); j++) {
                                if( (trm_vdvo[ictrans][j] != trm_vdvo[i][j]) ) {
                                    JOptionPane.showMessageDialog(null,"Development order must be consistent for P"+(j+xmy)+"mP"+j+lnsep+
                                                                       "in "+ntrans[ictrans]+" and "+ntrans[i]);
                                    return false;
                                }
                            }
                        }
                    }
                }
            }
            for(ictrans=0; ictrans<nbtrans; ictrans++) {
                for( int j=0; j<trm_nbdvo[ictrans]; j++) {
                    transp_vdvo[itransp[ictrans]][j] = trm_vdvo[ictrans][j];
                }
            }
        }
        return true;
    }

    // check full definition of a transition
    private boolean testTrans(int cictrans) {

        // are Jmax of lower and upper polyads defined ?
        njpol = (String) jcbjpol[isup[itransp[cictrans]]].getSelectedItem();
        if(njpol == "") {
            JOptionPane.showMessageDialog(null,"You have to select in BASICS"+lnsep+
                                               "the P"+polyads[isup[itransp[cictrans]]].getPolnb()+" Jmax for "+ntrans[cictrans]+" transition");
            return false;
        }
        njpol = (String) jcbjpol[iinf[itransp[cictrans]]].getSelectedItem();
        if(njpol == "") {
            JOptionPane.showMessageDialog(null,"You have to select in BASICS"+lnsep+
                                               "the P"+polyads[iinf[itransp[cictrans]]].getPolnb()+" Jmax for "+ntrans[cictrans]+" transition");
            return false;
        }
        // are they compatible ?
        jdiff = 1;                                                   // dip
        if( jrbpol.isSelected() ) {
            jdiff = 2;                                               // pol
        }
        if( isup[itransp[cictrans]] != iinf[itransp[cictrans]] ) {
            if( ! (vjpol[isup[itransp[cictrans]]] <= (vjpol[iinf[itransp[cictrans]]]-jdiff)) ) {
                JOptionPane.showMessageDialog(null,"Jmax of Upper Polyad P"+polyads[isup[itransp[cictrans]]].getPolnb()+" has to be lower or equal to"+lnsep+
                                                   "Jmax -"+jdiff+" of Lower Polyad P"+polyads[iinf[itransp[cictrans]]].getPolnb()                    +lnsep+
                                                   "for "+ntrans[cictrans]+" transition");
                return false;
            }
        }

        // ASG file
        if( nasgf[cictrans] == "" ) {
            JOptionPane.showMessageDialog(null,"You have to select the assignment file for "+ntrans[cictrans]+" transition");
            return false;
        }
        // Sel. String
        nselstr = jtfselstr[cictrans].getText();
        if( nselstr == "" ) {
            JOptionPane.showMessageDialog(null,"You have to define the selection string for "+ntrans[cictrans]+" transition");
            return false;
        }
        if( fitint ) {
            // Dev. Order
            for( int j=0; j<trm_nbdvo[cictrans]; j++) {
                trm_ndvo = (String) trm_jcbdvo[cictrans][j].getSelectedItem();
                if( trm_ndvo == "" ) {
                    JOptionPane.showMessageDialog(null,"You have to fully define the development order for "+ntrans[cictrans]+" transition");
                    return false;
                }
                else {
                    trm_vdvo[cictrans][j] = Integer.parseInt(trm_ndvo);  // save dev. order value
                }
            }
            // Temperature
            vtemp[cictrans] = ((Number)jftftemp[cictrans].getValue()).floatValue();
            if( vtemp[cictrans] <= 0 ) {
                JOptionPane.showMessageDialog(null,"Temperature > 0 requested for "+ntrans[cictrans]+" transition");
                jftftemp[cictrans].setValue(new Float(0.0));
                return false;
            }
            // RINMI
            try {
                vrinmi[cictrans] = Float.parseFloat(jtfrinmi[cictrans].getText());
            }
            catch(NumberFormatException nfe) {
                JOptionPane.showMessageDialog(null,"RINMI = "+jtfrinmi[cictrans].getText()+lnsep+
                                                   "is not valid !"                       +lnsep+
                                                   nfe);
                jtfrinmi[cictrans].setText("");
                return false;
            }
            if( vrinmi[cictrans] < 0 ) {
                JOptionPane.showMessageDialog(null,"RINMI >= 0 requested for "+ntrans[cictrans]+" transition");
                jtfrinmi[cictrans].setText("");
                return false;
            }
        }
        // unit
        if( jrbunit[cictrans].isSelected() ) {
            if( (! jrbMHz[cictrans].isSelected()) &&
                (! jrbGHz[cictrans].isSelected())    ) {
                JOptionPane.showMessageDialog(null,"You have to complete unit choice for "+ntrans[cictrans]+" transition");
                return false;
            }
        }
        // mix
        if( jrblpcmax[cictrans].isSelected() ) {
            if( jcblpcmax[cictrans].getSelectedItem() == "" ) {
                JOptionPane.showMessageDialog(null,"You have to complete Initial Basis Components for "+ntrans[cictrans]+" transition");
                return false;
            }
        }
        if( fitint ) {
            // rmxomc
            if( jrbrmxomc[cictrans].isSelected() ) {
                if( jcbrmxomc[cictrans].getSelectedItem() == "" ) {
                    JOptionPane.showMessageDialog(null,"You have to define the maximum relative (%) intensity OBS-CAL threshold"+lnsep+
                                                       "Intensity Relative Threshold option for "+ntrans[cictrans]+" transition");
                    return false;
                }
            }
            // fpvib
            if( jrbfpvib[cictrans].isSelected() ) {
                try {
                    vfpvib[cictrans] = Float.parseFloat(jtffpvib[cictrans].getText());
                }
                catch(NumberFormatException nfe) {
                    JOptionPane.showMessageDialog(null,"fpvib = "+jtffpvib[cictrans].getText()+lnsep+
                                                       "is not valid !"                       +lnsep+
                                                       nfe);
                    jtffpvib[cictrans].setText("");
                    return false;
                }
                if( vfpvib[cictrans] <= 0 ) {
                    JOptionPane.showMessageDialog(null,"0 < fpvib    requested for "+ntrans[cictrans]+" transition");
                    jtffpvib[cictrans].setText("");
                    return false;
                }
            }
            // abund
            if( jrbabund[cictrans].isSelected() ) {
                try {
                    vabund[cictrans] = Float.parseFloat(jtfabund[cictrans].getText());
                }
                catch(NumberFormatException nfe) {
                JOptionPane.showMessageDialog(null,"abund = "+jtfabund[cictrans].getText()+lnsep+
                                                   "is not valid !"                       +lnsep+
                                                   nfe);
                jtfabund[cictrans].setText("");
                return false;
                }
                if( vabund[cictrans] <= 0 ||
                    vabund[cictrans] >  1    ) {
                    JOptionPane.showMessageDialog(null,"0 < abund <= 1    requested for "+ntrans[cictrans]+" transition");
                    jtfabund[cictrans].setText("");
                    return false;
                }
            }
        }
        return true;
    }

}
