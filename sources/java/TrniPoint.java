/**
 * This class allows to sort trni file data.
 */
public class TrniPoint implements Comparable {

    private double j;                                                // J
    private double energ;                                            // ENERG
    private int[]  tic;                                              // ICENT array

    /**
     * Construct a new TrniPoint.
     *
     * @param cj      J
     * @param cenerg  ENERG
     * @param ctic    ICENT array
     */
    public TrniPoint(double cj, double cenerg, int[] ctic) {

        j     = cj;                                                  // J
        energ = cenerg;                                              // ENERG
        tic   = ctic;                                                // ICENT array
    }

/////////////////////////////////////////////////////////////////////

    /**
     * Return J.
     */
    public double getJ() {

        return j;
    }

    /**
     * Return ENERG.
     */
    public double getEnerg() {

        return energ;
    }

    /**
     * Return ICENT array.
     */
    public int[] getTic() {

        return tic;
    }

    /**
     * Compare J then ENERG.
     *
     * @param ctpt the TrniPoint to compare to
     */
    public int compareTo(Object ctpt) {

        if( !(ctpt instanceof TrniPoint) ) {                         // not the right object
            throw new ClassCastException();
        }
        double delta = ((TrniPoint)ctpt).getJ() - j;                 // compare j
        if     ( delta < 0. ) {
            return  1;
        }
        else if( delta > 0. ) {
            return -1;
        }
        else {
            delta = ((TrniPoint)ctpt).getEnerg() - energ;            // compare ENERG
            if     ( delta < 0 ) {
                return  1;
            }
            else if( delta > 0 ) {
                return -1;
            }
        }
        return 0;                                                    // equal
    }

}
