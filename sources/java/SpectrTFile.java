 /*
 * Class of spectr.t file
 */

import java.io.*;
import java.util.*;
import javax.swing.*;

/**
 * This class defines spectr.t data.
 */
class SpectrTFile {

    private String   name;                                           // file name
    private double[] x;                                              // X
    private double[] y;                                              // Y

    private int nbxy;                                                // nb of points

    private String str;                                              // to read file
    private BufferedReader br;
    private String lnsep;                                            // line separator

////////////////////////////////////////////////////////////////////

    /**
     * Construct a new SpectrTFile.
     * <br> (TDS, C3vsTDS, D2hStark or HITRAN format)
     *
     * @param cname  name of the file
     */
    public SpectrTFile(String cname) {

        name = cname;                                                // file name

        lnsep = System.getProperty("line.separator");
        nbxy = 0;                                                    // nb of points
    }

////////////////////////////////////////////////////////////////////

    /**
     * Read file.
     * <br> validity is tested following FORMAT : 1022 of spect.f, 1000 of spech.f, 1024 of spects.f
     */
    public boolean read() {

        Double cx;                                                   // to keep X
        Double cy;                                                   // to keep Y
        // to keep points, unknown nb of points
        ArrayList alx    = new ArrayList();                          // frequency
        ArrayList aly    = new ArrayList();                          // intensity
        try{
            br = new BufferedReader( new FileReader( new File( name ) ));  // open
            while ((str = br.readLine()) != null) {                  // line read
                // check line length
                if( (str.length() != 97)  &&                     // TDS      with population
                    (str.length() != 101) &&                     // C3vsTDS  with population
                    (str.length() != 92)  &&                     // D2hStark
                    (str.length() != 100) &&                     // HITRAN
                    (str.length() != 160) ) {                    // HITRAN 2004
                    // length error
                    continue;                                    // skip it
                }
                //
                if( (str.length() == 100) || (str.length() == 160) ) {
                    // HITRAN
                    try {
                        // extraction and validity test on 67 char (ie not all)
                        int n;
                        n = Integer.parseInt(str.substring(0,2).trim());  // validity test
                        n = Integer.parseInt(str.substring(2,3).trim());  // validity test
                        cx = Double.valueOf(str.substring(3,15));    // Double for X
                        cy = Double.valueOf(str.substring(15,25));   // Double for Y
                        Double d;
                        d = Double.valueOf(str.substring(25,35));    // validity test
                        d = Double.valueOf(str.substring(35,40));    // validity test
                        d = Double.valueOf(str.substring(40,45));    // validity test
                        d = Double.valueOf(str.substring(45,55));    // validity test
                        d = Double.valueOf(str.substring(55,59));    // validity test
                        d = Double.valueOf(str.substring(59,67));    // validity test
                    }
                    catch (NumberFormatException e) {                // format error
                        continue;                                    // skip the line
                    }
                }
                else if( str.length() == 92 ) {
                    //D2hStark
                    if( ! str.substring(18,19).equals(" ") ||
                        ! str.substring(28,29).equals(" ") ||
                        ! str.substring(32,33).equals(" ") ||
                        ! str.substring(36,37).equals(" ") ||
                        ! str.substring(40,41).equals(" ") ||
                        ! str.substring(44,45).equals(" ") ||
                        ! str.substring(48,49).equals(" ") ||
                        ! str.substring(52,53).equals(" ") ||
                        ! str.substring(56,57).equals(" ") ||
                        ! str.substring(60,61).equals(" ") ||
                        ! str.substring(76,77).equals(" ") ||
                        ! str.substring(80,81).equals(" ") ||
                        ! str.substring(84,85).equals(" ") ||
                        ! str.substring(88,89).equals(" ")    ) {    // format error
                        continue;                                    // skip the line
                    }
                    try {
                        cx = Double.valueOf(str.substring(0,18));    // Double for X
                        cy = Double.valueOf(str.substring(19,28));   // Double for Y
                        int n;
                        n = Integer.parseInt(str.substring(29,32).trim());  // validity test
                        n = Integer.parseInt(str.substring(33,36).trim());  // validity test
                        n = Integer.parseInt(str.substring(37,40).trim());  // validity test
                        n = Integer.parseInt(str.substring(41,44).trim());  // validity test
                        n = Integer.parseInt(str.substring(45,48).trim());  // validity test
                        n = Integer.parseInt(str.substring(49,52).trim());  // validity test
                        n = Integer.parseInt(str.substring(53,56).trim());  // validity test
                        n = Integer.parseInt(str.substring(57,60).trim());  // validity test
                        Double d = Double.valueOf(str.substring(61,76));    // validity test
                        n = Integer.parseInt(str.substring(77,80).trim());  // validity test
                        n = Integer.parseInt(str.substring(81,84).trim());  // validity test
                        n = Integer.parseInt(str.substring(85,88).trim());  // validity test
                        n = Integer.parseInt(str.substring(89,92).trim());  // validity test
                    }
                    catch (NumberFormatException e) {                // format error
                        continue;                                    // skip the line
                    }
                }
                else if( str.length() == 101 ) {
                    // C3vsTDS
                    if( ! str.substring(24,26).equals("  ") ||
                        ! str.substring(33,34).equals(" ")  ||
                        ! str.substring(47,49).equals("% ") ||
                        ! str.substring(54,55).equals(" ")  ||
                        ! str.substring(68,70).equals("% ") ||
                        ! str.substring(85,86).equals(" ")     ) {   // format error
                        continue;                                    // skip the line
                    }
                    try {
                        cx = Double.valueOf(str.substring(0,15));    // Double for X
                        cy = Double.valueOf(str.substring(15,24));   // Double for Y
                        int n;
                        Double d;
                        d = Double.valueOf(str.substring(28,33).trim());    // validity test
                        n = Integer.parseInt(str.substring(36,40).trim());  // validity test
                        n = Integer.parseInt(str.substring(40,43).trim());  // validity test
                        n = Integer.parseInt(str.substring(43,47).trim());  // validity test
                        d = Double.valueOf(str.substring(49,54).trim());    // validity test
                        n = Integer.parseInt(str.substring(58,61).trim());  // validity test
                        n = Integer.parseInt(str.substring(61,64).trim());  // validity test
                        n = Integer.parseInt(str.substring(64,68).trim());  // validity test
                        d = Double.valueOf(str.substring(70,85));           // validity test
                        d = Double.valueOf(str.substring(86,101));          // validity test
                    }
                    catch (NumberFormatException e) {                // format error
                        continue;                                    // skip the line
                    }
                }
                else {
                    // TDS (97)
                    if( ! str.substring(24,26).equals("  ") ||
                        ! str.substring(31,32).equals(" ")  ||
                        ! str.substring(45,47).equals("% ") ||
                        ! str.substring(50,51).equals(" ")  ||
                        ! str.substring(64,66).equals("% ") ||
                        ! str.substring(81,82).equals(" ")     ) {   // format error
                        continue;                                    // skip the line
                    }
                    try {
                        cx = Double.valueOf(str.substring(0,15));    // Double for X
                        cy = Double.valueOf(str.substring(15,24));   // Double for Y
                        int n;
                        n = Integer.parseInt(str.substring(28,31).trim());  // validity test
                        n = Integer.parseInt(str.substring(35,38).trim());  // validity test
                        n = Integer.parseInt(str.substring(38,41).trim());  // validity test
                        n = Integer.parseInt(str.substring(41,45).trim());  // validity test
                        n = Integer.parseInt(str.substring(47,50).trim());  // validity test
                        n = Integer.parseInt(str.substring(54,57).trim());  // validity test
                        n = Integer.parseInt(str.substring(57,60).trim());  // validity test
                        n = Integer.parseInt(str.substring(60,64).trim());  // validity test
                        Double d;
                        d = Double.valueOf(str.substring(66,81));           // validity test
                        d = Double.valueOf(str.substring(82,97));           // validity test
                    }
                    catch (NumberFormatException e) {                // format error
                        continue;                                    // skip the line
                    }
                }
                alx.add(cx);                                         // keep data
                aly.add(cy);
            }
        }
        catch(IOException ioe) {                                     // IO error
            JOptionPane.showMessageDialog(null,"IO error while reading file"+lnsep+
                                               name                         +lnsep+
                                               ioe);
            return false;
        }
        finally {
            // close
            if (br != null) {
                try {
                    br.close();
                }
                catch (IOException ignored) {
                }
            }
        }
        // save data
        nbxy = alx.size();                                           // nb of points
        if( nbxy == 0 ) {
            // no point
            JOptionPane.showMessageDialog(null,"No valid data found in file"+lnsep+
                                               name);
            return false;
        }
        x    = new double[nbxy];
        y    = new double[nbxy];
        // sort
        XYPoint[] xypt = new XYPoint[nbxy];                          // compare X
        for(int i=0; i<nbxy; i++) {
            xypt[i] = new XYPoint( ((Double) alx.get(i)).doubleValue(),
                                   ((Double) aly.get(i)).doubleValue() );
        }
        Arrays.sort(xypt);                                           // sort
        // save data
        for(int i=0; i<nbxy; i++) {
            x[i]    = xypt[i].getX();
            y[i]    = xypt[i].getY();
        }

        return true;
    }

    /**
     * Get number of data points.
     */
    public int getNbxy() {

        return nbxy;
    }

    /**
     * Get X array.
     */
    public double[] getX() {

        return x;
    }

    /**
     * Get Y array.
     */
    public double[] getY() {

        return y;
    }

}
