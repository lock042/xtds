/*
 * Class job creation, called by JobPlay
 */

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;
import java.util.*;

/**
 * This panel allows to create different types of jobs for a given package.
 */
public class CreateJob extends JPanel
                       implements  ActionListener {

    private CJPFit         cjpfit;                                   // fit                             job panel
    private CJPXFit        cjpxfit;                                  // xfit                            job panel
    private CJPLevel       cjplevel;                                 // level                           job panel
    private CJPPara        cjppara;                                  // initial parameter file creation job panel
    private CJPSpect       cjpspect;                                 // spectrum                        job panel
    private CJPSpectStark  cjpspectstark;                            // STARK spectrum                  job panel
    private CJPSimul       cjpsimul;                                 // simulation                      job panel

    private JLabel       jlpack;                                     // package choice
    private JComboBox    jcbpack;                                    // package jcb
    private JLabel       jljob;                                      // job choice
    private ButtonGroup  bgroup;                                     // RadioButton group
    private JRadioButton jrbpfc;                                     // Parameter File Creation
    private JRadioButton jrblvc;                                     // Level Calculation
    private JRadioButton jrbspc;                                     // Spectrum Calculation
    private JRadioButton jrbspcD2hStark;                             // Spectrum Calculation (STARK)
    private JRadioButton jrbsic;                                     // Simulation Calculation
    private JRadioButton jrbfit;                                     // Fit Calculation
    private JRadioButton jrbxfit;                                    // Xfit Calculation
    private int          jobnb;                                      // job type #

    private Box            box;
    private String         nomf;                                     // various file names
    private String         str;                                      // string
    private BufferedReader br;                                       // and buffer for file reading

    // CJPanel components
    private JButton jbreset;                                         // various CJPanel reset button

    // package characteristics
    private String   playd;                                          // XTDS installation directory
    private String[] packdirs;                                       // package directories
    private String   npack;                                          // package name
    private int      nbmol;                                          // nb of molecules
    private String[] moldirs;                                        // molecule directories
    private int      mxpol;                                          // max nb of polyads
    private int      nbclab;                                         // nb of characters of a parameter label
    private int      nbvqn;                                          // nb of vibrational quantum numbers

    private String lnsep;                                            // line separator
    private String fisep;                                            // file separator

////////////////////////////////////////////////////////////

    /**
     * Constructs a new CreateJob.
     *
     * @param cpackdirs   packages directories
     */
    public CreateJob(String[] cpackdirs) {

        playd = System.getProperty("xtds.home");                     // XTDS installation directory
        packdirs = (String[]) cpackdirs.clone();                     // package directories

        lnsep = System.getProperty("line.separator");
        fisep = System.getProperty("file.separator");
        setName("CreateJob");                                        // for help files
        askPack("");                                                   // choose a package
    }

    // choose a package
    private void askPack( String cnpack ) {

        removeAll();                                                 // remove all panel components
        setLayout(new FlowLayout());                                 // layout choice

        npack = cnpack;                                              // chosen package
        jobnb = 0;                                                   // no chosen job
        // package
        jlpack = new JLabel("Choose a Package :");
        // create JComboBox
        jcbpack = new JComboBox();
        jcbpack.addItem("");                                         // add       ""
        for( int i=0; i<packdirs.length; i++ ) {                     // add directories
            jcbpack.addItem(packdirs[i]);
        }
        jcbpack.setBackground(Color.WHITE);
        jcbpack.setSelectedItem(npack);                              // defaut to cnpack
        jcbpack.addActionListener(this);                             // manage this jcb

        box = Box.createVerticalBox();                               // choice box
        box.add(Box.createVerticalStrut(100));
        box.add(jlpack);                                             // package choice
        box.add(Box.createVerticalStrut(10));
        box.add(jcbpack);

        add(box);                                                    // display all

        if( npack != "" ) {
            jljob = new JLabel("Choose the type of file to create :");   // action choice
            jrbpfc = new JRadioButton("Parameter File Creation Job");    // #1
            jrbpfc.setBackground(Color.WHITE);
            jrbpfc.addActionListener(this);
            jrblvc = new JRadioButton("Level Job");                      // #2
            jrblvc.setBackground(Color.WHITE);
            jrblvc.addActionListener(this);
            jrbspc = new JRadioButton("Spectrum Job");                   // #3
            jrbspc.setBackground(Color.WHITE);
            jrbspc.addActionListener(this);
            if( npack.equals("D2hTDS") ) {
                jrbspcD2hStark = new JRadioButton("Spectrum Job (STARK)");  // #31
                jrbspcD2hStark.setBackground(Color.WHITE);
                jrbspcD2hStark.addActionListener(this);
            }
            jrbsic = new JRadioButton("Simulation Job");                 // #4
            jrbsic.setBackground(Color.WHITE);
            jrbsic.addActionListener(this);
            jrbfit = new JRadioButton("Fit Job");                        // #5
            jrbfit.setBackground(Color.WHITE);
            jrbfit.addActionListener(this);
            jrbxfit = new JRadioButton("XFit Job");                      // #5
            jrbxfit.setBackground(Color.WHITE);
            jrbxfit.addActionListener(this);
            bgroup = new ButtonGroup();                                  // radiobutton group
            bgroup.add(jrbpfc);
            bgroup.add(jrblvc);
            bgroup.add(jrbspc);
            if( npack.equals("D2hTDS") ) {
                bgroup.add(jrbspcD2hStark);
            }
            bgroup.add(jrbsic);
            bgroup.add(jrbfit);
            bgroup.add(jrbxfit);

            box.add(Box.createVerticalStrut(50));                        // job choice
            box.add(jljob);
            box.add(Box.createVerticalStrut(10));
            box.add(jrbpfc);
            box.add(Box.createVerticalStrut(5));
            box.add(jrblvc);
            box.add(Box.createVerticalStrut(5));
            box.add(jrbspc);
            if( npack.equals("D2hTDS") ) {
                box.add(jrbspcD2hStark);
            }
            box.add(Box.createVerticalStrut(5));
            box.add(jrbsic);
            box.add(Box.createVerticalStrut(5));
            box.add(jrbfit);
            box.add(Box.createVerticalStrut(5));
            box.add(jrbxfit);
        }
        repaint();
    }

    /**
     * Processes the events.
     */
    public void actionPerformed(ActionEvent evt) {

        // CJxxx reset button
        if (evt.getSource() == jbreset) {
            setVisible(false);                                       // hide everything
            askPack("");                                               // restart from beginning
            return;
        }
        // package choice
        if (evt.getSource() == jcbpack ) {
            npack = (String) jcbpack.getSelectedItem();              // package name
            askPack(npack);
        }
        else if(evt.getSource() == jrbpfc) {
            jobnb = 1;                                               // job type #1
        }
        else if(evt.getSource() == jrblvc) {
            jobnb = 2;                                               // job type #2
        }
        else if(evt.getSource() == jrbspc) {
            jobnb = 3;                                               // job type #3
        }
        else if(evt.getSource() == jrbspcD2hStark) {
            jobnb = 31;                                              // job type #31
        }
        else if(evt.getSource() == jrbsic) {
            jobnb = 4;                                               // job type #4
        }
        else if(evt.getSource() == jrbfit) {
            jobnb = 5;                                               // job type #5
        }
        else if(evt.getSource() == jrbxfit) {
            jobnb = 6;                                               // job type #6
        }
        if(npack != "" && jobnb != 0) {                              // package and job chosen
            if(createCar()) {                                        // if characteristics are read
                removeAll();                                         // call one of these CJPanel
                setLayout(new BorderLayout());
                switch(jobnb) {
                    case 1 : {
                        cjppara = new CJPPara();                                  // instanciate CJPanel
                        cjppara.setBorder(BorderFactory.createTitledBorder(       // frame title
                            BorderFactory.createLineBorder(Color.BLUE,4),"Parameter File Creation for "+npack));
                        jbreset = cjppara.getJBreset();                           // set referent
                        cjppara.setBasicPara(npack,moldirs,mxpol,nbclab,nbvqn);   // set basic parameters
                        add(cjppara, "Center");                                   // display CJPanel
                        break;
                    }
                    case 2 : {
                        cjplevel = new CJPLevel();                                // instanciate CJPanel
                        cjplevel.setBorder(BorderFactory.createTitledBorder(      // frame title
                            BorderFactory.createLineBorder(Color.BLUE,4),"Level Job Creation for "+npack));
                        jbreset = cjplevel.getJBreset();                          // set referent
                        cjplevel.setBasicPara(npack,moldirs,mxpol,nbclab,nbvqn);  // set basic parameters
                        add(cjplevel, "Center");                                  // display CJPanel
                        break;
                    }
                    case 3 : {
                        cjpspect = new CJPSpect();                                // instanciate CJPanel
                        cjpspect.setBorder(BorderFactory.createTitledBorder(      // frame title
                            BorderFactory.createLineBorder(Color.BLUE,4),"Spectrum Job Creation for "+npack));
                        jbreset = cjpspect.getJBreset();                          // set referent
                        cjpspect.setBasicPara(npack,moldirs,mxpol,nbclab,nbvqn);  // set basic parameters
                        add(cjpspect, "Center");                                  // display CJPanel
                        break;
                    }
                    case 31 : {
                        cjpspectstark = new CJPSpectStark();                        // instanciate CJPanel
                        cjpspectstark.setBorder(BorderFactory.createTitledBorder(   // frame title
                            BorderFactory.createLineBorder(Color.BLUE,4),"STARK Spectrum Job Creation for "+npack));
                        jbreset = cjpspectstark.getJBreset();                       // set referent
                        cjpspectstark.setBasicPara(npack,moldirs,mxpol,nbclab,nbvqn);  // set basic parameters
                        add(cjpspectstark, "Center");                               // display CJPanel
                        break;
                    }
                    case 4 : {
                        cjpsimul = new CJPSimul();                                // instanciate CJPanel
                        cjpsimul.setBorder(BorderFactory.createTitledBorder(      // frame title
                            BorderFactory.createLineBorder(Color.BLUE,4),"Simulation Job Creation for "+npack));
                        jbreset = cjpsimul.getJBreset();                          // set referent
                        cjpsimul.setBasicPara(npack,moldirs,mxpol,nbclab,nbvqn);  // set basic parameters
                        add(cjpsimul, "Center");                                  // display CJPanel
                        break;
                    }
                    case 5 : {
                        cjpfit = new CJPFit();                                    // instanciate CJPanel
                        cjpfit.setBorder(BorderFactory.createTitledBorder(        // frame title
                            BorderFactory.createLineBorder(Color.BLUE,4),"Fit Jobs Creation for "+npack));
                        jbreset = cjpfit.getJBreset();                            // set referent
                        cjpfit.setBasicPara(npack,moldirs,mxpol,nbclab,nbvqn);    // set basic parameters
                        add(cjpfit, "Center");                                    // display CJPanel
                        break;
                    }
                    case 6 : {
                        cjpxfit = new CJPXFit();                                  // instanciate CJPanel
                        cjpxfit.setBorder(BorderFactory.createTitledBorder(       // frame title
                            BorderFactory.createLineBorder(Color.BLUE,4),"XFit Jobs Creation for "+npack));
                        jbreset = cjpxfit.getJBreset();                           // set referent
                        cjpxfit.setBasicPara(npack,moldirs,mxpol,nbclab,nbvqn);   // set basic parameters
                        add(cjpxfit, "Center");                                   // display CJPanel
                        break;
                    }
                    default : {
                    }
                }

                // manage CJPanel reset button
                jbreset.setText("Reset All");                        // reset button title
                jbreset.addActionListener (this);                    // listen to it
            }
            else {
                askPack("");                                         // restart from beginning
            }
        }
    }

    // read basic characteristics
    private boolean createCar() {

        // get package characteristics
        String[]  cmoldirs;
        int       cnbmol;
        boolean[] isdir;

        // molecules
        File paradir = new File(playd+fisep+"packages"+fisep+npack+fisep+"para");
        cmoldirs = paradir.list();
        cnbmol   = cmoldirs.length;
        isdir    = new boolean[cnbmol];
        nbmol    = 0;
        for( int i=0; i<cnbmol; i++ ) {
            isdir[i] = false;
            if( (new File(paradir, cmoldirs[i])).isDirectory() ) {
                isdir[i] = true;
                nbmol ++;
            }
        }
        moldirs = new String[nbmol];
        nbmol    = 0;
        for( int i=0; i<cnbmol; i++ ) {
            if( isdir[i] ) {
                moldirs[nbmol] = cmoldirs[i];
                nbmol ++;
            }
        }
        Arrays.sort(moldirs);                                        // sort
        //
        if( ! readTdsPF() ) {                                        // read mod_par_tds
            return false;
        }
        return true;
    }

    // read mod_par_tds
    private boolean readTdsPF() {

        mxpol  = 0;
        nbclab = 0;
        nbvqn  = 0;
        try {
            nomf = playd+fisep+"packages"+fisep+npack+fisep+"prog"+fisep+"mod"+fisep+"mod_par_tds.f90";
            File f = new File(nomf);
            br = new BufferedReader (new FileReader(f));             // open
            while ((str = br.readLine()) != null) {
               if(str.startsWith("      integer ,parameter  :: MXPOL  =")) {
                   str = str.substring(38,38+7);
                   str = str.trim();
                   mxpol  = Integer.parseInt(str);
               }
               if(str.startsWith("      integer ,parameter  :: NBCLAB =")) {
                   str = str.substring(38,38+7);
                   str = str.trim();
                   nbclab = Integer.parseInt(str);
               }
               if(str.startsWith("      integer ,parameter  :: NBVQN  =")) {
                   str = str.substring(38,38+7);
                   str = str.trim();
                   nbvqn  = Integer.parseInt(str);
               }
            }
        }
        catch(IOException ioe) {                                     // IO error
            JOptionPane.showMessageDialog(null,"IO error while reading file"+lnsep+
                                               nomf                         +lnsep+
                                               ioe);
            return false;
        }
        finally {                                                    // close file
            if (br !=null) {
                try {
                    br.close();
                }
                catch (IOException ignored) {
                }
            }
        }

        // happy end check
        if(mxpol  == 0 ) {                                           // not found
            JOptionPane.showMessageDialog(null,"MXPOL  not found in "+nomf);
            return false;
        }
        if(nbclab == 0 ) {                                           // not found
            JOptionPane.showMessageDialog(null,"NBCLAB not found in "+nomf);
            return false;
        }
        if(nbvqn  == 0 ) {                                           // not found
            JOptionPane.showMessageDialog(null,"NBVQN  not found in "+nomf);
            return false;
        }
        return true;
    }

}
