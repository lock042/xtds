/**
 * This class allows to sort component segment data.
 */
public class ComPoint implements Comparable {

    private int ip_deb;                                              // Ip_deb
    private int ip_fin;                                              // Ip_fin
    private int icol;                                                // Icol

    /**
     * Construct a new ComPoint.
     *
     * @param cip_deb  Ip_deb
     * @param cip_fin  Ip_fin
     * @param cicol    color index
     */
    public ComPoint(int cip_deb, int cip_fin, int cicol) {

        ip_deb = cip_deb;                                            // Ip_deb
        ip_fin = cip_fin;                                            // Ip_fin
        icol   = cicol;                                              // color index
    }

/////////////////////////////////////////////////////////////////////

    /**
     * Get Ip_deb.
     */
    public int getIp_deb() {

        return ip_deb;
    }

    /**
     * Get Ip_fin.
     */
    public int getIp_fin() {

        return ip_fin;
    }

    /**
     * Get Icol.
     */
    public int getIcol() {

        return icol;
    }

    /**
     * Compare Ip_fin-Ip_deb.
     *
     * @param ccp the ComPoint to compare to
     */
    public int compareTo(Object ccp) {

        if( ! (ccp instanceof ComPoint) ) {                          // not the right object
            throw new ClassCastException();
        }
        int delta = ( ((ComPoint)ccp).getIp_fin() - ((ComPoint)ccp).getIp_deb()) -
                    ( ip_fin                        - ip_deb                       );  // compare length
        if     ( delta < 0 ) {
            return  1;
        }
        else if( delta > 0 ) {
            return -1;
        }
        return 0;                                                    // equal
    }

}
