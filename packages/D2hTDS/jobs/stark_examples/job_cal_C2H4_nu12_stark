#! /bin/sh
 set -v
##
##  Calculation of the Stark spectrum
## of nu12 of C2H4
#
BASD=will be set by *tds_INSTALL
#
 SCRD=$BASD/prog/exe
 PARD=$BASD/para/C2H4
##
## Jmax values.
##
 JPH=10
 JPPol=8
 MSt=6
 Elec=13.5
###########################################################
## Parameter files.
##
 PARA=$PARD/Pa_000000000001m000000000000
 PARP=$PARD/Pa_000000000001m000000000000_raman
##
###########################################################
##
## GS.
##
## Zero-field Hamiltonian matrix elements.
##
 $SCRD/passx hmodel P0 N1 D4   0 0 0 0 0 0 0 0 0 0 0 0   \
                    Ir
 $SCRD/passx parchk P0    D4   $PARD $PARA
 $SCRD/passx rovbas P0 N1 D4   $JPH
 $SCRD/passx hmatri P0 N1 D4   $JPH
 $SCRD/passx hdi    P0 N1 D4   $JPH  $PARA
##
## Stark Hamiltonian matrix elements.
##
 $SCRD/passx polmod P0 N1   0 0 0 0 0 0 0 0 0 0 0 0   \
                    P0 N1   0 0 0 0 0 0 0 0 0 0 0 0   \
                    D0                                \
                    Ir
 $SCRD/passx parchk P0    D4  P0    D0  pol    $PARD  $PARP
 $SCRD/passx polmat P0 N1     P0 N1 D0  $JPPol
 $SCRD/passx smatri P0 N1     P0 N1 D0  $JPPol $PARP
 $SCRD/passx sdi    P0                  $JPPol $MSt   $Elec
##
###########################################################
##
## nu12.
##
## Zero-field Hamiltonian matrix elements.
##
 $SCRD/passx hmodel P1 N1 D4   0 0 0 0 0 0 0 0 0 0 0 0   \
                       N1 D8   0 0 0 0 0 0 0 0 0 0 0 1   \
                    Ir
 $SCRD/passx parchk P1    D48  $PARD $PARA
 $SCRD/passx rovbas P1 N1 D48  $JPH
 $SCRD/passx hmatri P1 N1 D48  $JPH
 $SCRD/passx hdi    P1 N1 D48  $JPH $PARA
##
## Stark Hamiltonian matrix elements.
##
 $SCRD/passx polmod P1 N1   0 0 0 0 0 0 0 0 0 0 0 0   \
                       N1   0 0 0 0 0 0 0 0 0 0 0 1   \
                    P1 N1   0 0 0 0 0 0 0 0 0 0 0 0   \
                       N1   0 0 0 0 0 0 0 0 0 0 0 1   \
                    D02                               \
                    Ir
 $SCRD/passx parchk P1    D48 P1    D02 pol    $PARD  $PARP
 $SCRD/passx polmat P1 N1     P1 N1 D02 $JPPol
 $SCRD/passx smatri P1 N1     P1 N1 D02 $JPPol $PARP
 $SCRD/passx sdi    P1                  $JPPol $MSt   $Elec
##
###########################################################
##
## Upper - lower level transition.
##
 $SCRD/passx dipmod P1 N1   0 0 0 0 0 0 0 0 0 0 0 0   \
                       N1   0 0 0 0 0 0 0 0 0 0 0 1   \
                    P0 N1   0 0 0 0 0 0 0 0 0 0 0 0   \
                    D2                                \
                    Ir
 $SCRD/passx parchk P1    D48 P0    D2 dip  $PARD $PARA
 $SCRD/passx dipmats P1 N1    P0 N1 D2 $MSt $PARA
##
###########################################################
##
## Spectrum calculation.
##
 $SCRD/passx spects P1 N1 P0 N1 D2 $MSt 1380.0  1510.0   \
                    300.0 300.0 0.0 fpvib 1.0 abund 1.0
##
 \rm DIS* EN_* FN_* HA_* MD_* ME_* MH_* MP_* PO_* VP_* VPS_*
