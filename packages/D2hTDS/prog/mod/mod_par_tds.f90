
      module mod_par_tds

      use mod_dppr
      IMPLICIT NONE

!
!
!  This file provides various limitating parameters concerning the different programs in the package.
!  A short description as well as involved programs are given for each parameter.
!  Note: some program specific parameters are still defined in the related program source.
!
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
! Program stamped with (*) provides maximum value of the concerned parameter
! written in the 'debug.t' file.
!
!
! MXBRA  = Maximum number of branches.
!          In           -> mod_com_branch
!                          mod_com_trm
!                          eq_int
!                          eq_tds
!                          spech
!                          spect
!                          xwf
!                          xwfa
!                          xws
!
! MXDIMI = JC block maximum dimension for lower polyad.
!          Dependancies -> polyad and Jmax.
!          In           -> mod_com_dipmat
!                          mod_com_dipomat
!                          mod_com_polmat
!                          mod_com_trm
!                          mod_main_dipmat
!                          mod_main_dipmats
!                          mod_main_polmat
!                          mod_main_trm
!                          mod_main_trmomt
!                          mod_main_xpafit
!                          dipmat (*)
!                          dipmats(*)
!                          polmat (*)
!                          trm
!                          trmomt
!                          xtrm
!                          xtro
!
! MXDIMS = JC block maximum dimension for upper polyad.
!          Dependancies -> polyad and Jmax.
!          In           -> mod_com_dipomat
!                          mod_com_hdic
!                          mod_com_hmatri
!                          mod_com_pgdh
!                          mod_com_rovbas
!                          mod_com_smatri
!                          mod_com_trm
!                          mod_com_xhdic
!                          mod_main_dipmat
!                          mod_main_dipmats
!                          mod_main_hdi
!                          mod_main_hdiag
!                          mod_main_hmatri
!                          mod_main_polmat
!                          mod_main_rovbas
!                          mod_main_smatri
!                          mod_main_trm
!                          mod_main_trmomt
!                          mod_main_xpafit
!                          asfn
!                          asha
!                          asvp
!                          asxvp
!                          diago
!                          dipmat (*)
!                          dipmats(*)
!                          dmspr
!                          hdi
!                          hdiag
!                          hmatri (*)
!                          polmat (*)
!                          rovbas
!                          smatri
!                          trm
!                          trmomt
!                          xhdi
!                          xhdiag
!                          xtrm
!                          xtro
!
! MXELMD = Maximum number of matrix elements for all H operators per JC block.
!          Dependancies -> polyad, development order and Jmax.
!          In           -> mod_com_pgdh
!                          mod_main_hdi
!                          mod_main_hdiag
!                          mod_main_xpafit
!                          hdi    (*)
!                          hdiag  (*)
!                          smatri
!                          xhdi   (*)
!                          xhdiag (*)
!
! MXELMH = Maximum number of matrix elements for one H operator per JC block.
!          Dependancies -> polyad, development order and Jmax.
!          In           -> mod_com_hmatri
!                          mod_com_sdi
!                          mod_main_hmatri
!                          mod_main_sdi
!                          asha
!                          hmatri (*)
!                          sdi
!
! MXELMT = Maximum number of matrix elements for one transition moment operator
!                          for a given upper JC block.
!          Dependancies -> polyad, development order and Jmax.
!          In           -> mod_com_dipomat
!                          mod_com_smatri
!                          mod_com_trm
!                          mod_main_dipmat
!                          mod_main_dipmats
!                          mod_main_polmat
!                          mod_main_smatri
!                          mod_main_trm
!                          mod_main_trmomt
!                          mod_main_xpafit
!                          asdi
!                          aspo
!                          calpo
!                          dipmat (*)
!                          dipmats(*)
!                          polmat (*)
!                          smatri
!                          trm
!                          trmomt
!                          xtrm
!                          xtro
!
! MXEMR  = Maximum number of vibrational reduced matrix elements.
!          Dependancies -> polyad, development order.
!          In           -> mod_com_dipomat
!                          mod_com_hmatri
!                          mod_com_model
!                          mod_main_dipmat
!                          mod_main_dipmats
!                          mod_main_dipmod
!                          mod_main_hmatri
!                          mod_main_hmodel
!                          mod_main_polmat
!                          mod_main_polmod
!                          dipmat
!                          dipmats
!                          dipmod (*)
!                          hmatri
!                          hmodel (*)
!                          polmat
!                          polmod (*)
!
! MXENI  = Maximum number of lower polyad energies.
!          Dependancies -> lower polyad and Jmax.
!          In           -> mod_com_eqint
!                          mod_com_tra
!                          mod_com_xwfa
!                          mod_com_xws
!                          mod_main_eqint
!                          mod_main_tra
!                          mod_main_xpafit
!                          eq_int
!                          tra    (*)
!                          xwfa   (*)
!                          xws    (*)
!
! MXJ    = Jmax.
!          Dependancies -> MXJ should be < or = to MXJG-2 for Raman,
!                                               to MXJG-1 for infrared.
!          In           -> mod_com_statf
!                          mod_com_stats
!                          dipmat
!                          dipmats
!                          eq_int
!                          hdi
!                          hdiag
!                          hmatri
!                          polmat
!                          smatri
!                          xpafit
!                          xws
!
! MXJG   = MXJ+1
!
! MXNCR  = Maximum number of rotational codes for a JC block.
!          Dependancies -> polyad and Jmax.
!          In           -> mod_com_rovbas
!                          mod_main_rovbas
!                          rovbas (*)
!
! MXNIV  = Maximum number of vibrational levels for a given polyad.
!          In           -> mod_com_dipomod
!                          mod_com_eqtds
!                          mod_com_hmodel
!                          mod_com_statf
!                          mod_com_xwf
!                          mod_main_dipmod
!                          mod_main_eqtds
!                          mod_main_hmodel
!                          mod_main_polmod
!                          mod_main_xpafit
!                          dipmod (*)
!                          eq_tds (*)
!                          hmodel (*)
!                          polmod (*)
!                          xwf    (*)
!
! MXOBS  = Maximum number of observed lines.
!          In           -> mod_com_cas
!                          mod_com_der
!                          mod_com_en
!                          mod_com_eqtds
!                          mod_com_levlst
!                          mod_com_precf
!                          mod_com_predlx
!                          mod_com_xassi
!                          mod_com_xwf
!                          mod_main_eqtds
!                          mod_main_levlst
!                          mod_main_precf
!                          mod_main_predlx
!                          mod_main_xctrmk
!                          mod_main_xpafit
!                          mod_par_x             -> MXOBZ,LWA
!                          eq_tds (*)
!                          levlst
!                          precf
!                          predlx
!                          xpafit (*)
!
! MXOCV  = Maximum number of rovibrational operators for a given vibrational block.
!          Dependancies -> polyad and development order.
!          In           -> mod_com_model
!                          mod_com_polmod
!                          mod_main_dipmod
!                          mod_main_hmodel
!                          mod_main_polmod
!                          dipmod (*)
!                          hmodel (*)
!                          polmod (*)
!
! MXOPH  = Maximum number of operators in H.
!          Dependancies -> polyad and development order.
!          In           -> mod_com_der
!                          mod_com_derv
!                          mod_com_eqfc
!                          mod_com_eqint
!                          mod_com_eqtds
!                          mod_com_hdic
!                          mod_com_hmatri
!                          mod_com_hmodel
!                          mod_com_paradj
!                          mod_com_pgde
!                          mod_com_pgdh
!                          mod_com_precf
!                          mod_com_xpara
!                          mod_com_xwfa
!                          mod_main_eqint
!                          mod_main_eqtds
!                          mod_main_hdi
!                          mod_main_hdiag
!                          mod_main_hmatri
!                          mod_main_hmodel
!                          mod_main_paradj
!                          mod_main_precf
!                          mod_main_xctrmk
!                          mod_main_xpafit
!                          mod_par_x             -> MXOP,MXOBZ,LWA
!                          ased
!                          asnorm
!                          asparv
!                          asxed
!                          eq_int
!                          eq_tds
!                          hdi
!                          hdiag
!                          hmatri
!                          hmodel (*)
!                          paradj
!                          precf
!                          smatri
!                          xctrmk
!                          xhdi
!                          xhdiag
!                          xpafit
!
! MXOPR  = Maximum number of rovibrational operators for each vibrational operator.
!          Dependancies -> polyad and development order.
!          In           -> mod_com_model
!                          mod_com_polmod
!                          mod_main_dipmod
!                          mod_main_hmodel
!                          mod_main_polmod
!                          dipmod (*)
!                          hmodel (*)
!                          polmod (*)
!
! MXOPT  = Maximum number of transition operators.
!          Dependancies -> polyad and development order.
!          In           -> mod_com_dipomat
!                          mod_com_dipomod
!                          mod_com_eqint
!                          mod_com_pa
!                          mod_com_polmat
!                          mod_com_polmod
!                          mod_com_trm
!                          mod_com_trmomt
!                          mod_com_xpara
!                          mod_com_xws
!                          mod_main_dipmat
!                          mod_main_dipmats
!                          mod_main_dipmod
!                          mod_main_eqint
!                          mod_main_polmat
!                          mod_main_polmod
!                          mod_main_trm
!                          mod_main_trmomt
!                          mod_main_xctrmk
!                          mod_main_xpafit
!                          mod_par_x             -> MXOP,MXOBZ,LWA
!                          astatd
!                          asxtro
!                          dipmat
!                          dipmats
!                          dipmod (*)
!                          eq_int
!                          polmat
!                          polmod (*)
!                          trm
!                          trmomt
!                          xctrmk
!                          xpafit
!                          xtrm
!                          xtro
!
! MXOPVH = Maximum number of vibrational operators (Hamiltonian).
!          Dependancies -> polyad and development order.
!          In           -> mod_com_hmatri
!                          mod_main_hmatri
!                          hmatri (*)
!                          hmodel (*)
!
! MXOPVT = Maximum number of vibrational operators (transition moment).
!          Dependancies -> polyad and development order.
!          In           -> mod_com_dipomat
!                          mod_main_dipmat
!                          mod_main_dipmats
!                          mod_main_polmat
!                          dipmat (*)
!                          dipmats(*)
!                          dipmod (*)
!                          polmat (*)
!                          polmod (*)
!
! MXSNV  = Maximum number of vibrational sublevels.
!          Dependancies -> polyad.
!          In           -> mod_com_dipomod
!                          mod_com_elmr
!                          mod_com_eqint
!                          mod_com_eqtds
!                          mod_com_hdic
!                          mod_com_hmodel
!                          mod_com_jener
!                          mod_com_levlst
!                          mod_com_matri
!                          mod_com_model
!                          mod_com_rovbas
!                          mod_com_spech
!                          mod_com_stats
!                          mod_com_tra
!                          mod_com_xhdic
!                          mod_com_xwf
!                          mod_com_xwfa
!                          mod_com_xws
!                          mod_main_dipmat
!                          mod_main_dipmats
!                          mod_main_dipmod
!                          mod_main_eqint
!                          mod_main_eqtds
!                          mod_main_hdi
!                          mod_main_hdiag
!                          mod_main_hmatri
!                          mod_main_hmodel
!                          mod_main_jener
!                          mod_main_levlst
!                          mod_main_polmat
!                          mod_main_polmod
!                          mod_main_rovbas
!                          mod_main_spech
!                          mod_main_tra
!                          mod_main_xpafit
!                          ased
!                          asen
!                          asxed
!                          calpo
!                          dipmat (*)
!                          dipmats(*)
!                          dipmod (*)
!                          elmr
!                          eq_int
!                          eq_tds
!                          hdi
!                          hdiag
!                          hmatri (*)
!                          hmodel (*)
!                          jener
!                          levlst
!                          polmat (*)
!                          polmod (*)
!                          rovbas
!                          smatri
!                          spech
!                          tra
!                          xhdi
!                          xhdiag
!                          xws
!
! MXSYM  = Maximum number of symmetries.
!          In           -> mod_com_dipmat        -> MDMJCI
!                          mod_com_polmat        -> MDMJCI
!                          mod_com_sdi
!                          mod_com_smatri
!                          mod_com_spin
!                          mod_com_sy
!                          mod_com_trm           -> MDMJCI
!                          asdi
!                          asfn
!                          asha
!                          asme
!                          aspo
!                          cal6c
!                          calpo
!                          dipmat
!                          dipmats
!                          dipmod
!                          eq_int
!                          eq_tds
!                          hdi
!                          hdiag
!                          hmatri
!                          hmodel
!                          nsym1
!                          parchk
!                          polmat
!                          polmod
!                          rovbas
!                          sdi
!                          smatri
!                          spects
!                          tra
!                          trm
!                          trmomt
!                          xhdi
!                          xhdiag
!                          xpafit
!                          xtrm
!                          xtro
!
! MXPOL  = Maximum number of polyads.
!          In           -> mod_com_dipomod
!                          mod_com_hmodel
!                          mod_com_xpoly
!                          dipmod
!                          hmodel
!                          polmod
!                          xasg0
!                          xctrmk
!                          xpafit
!
! MXNTR  = Maximum number of P*mP* transitions
!          MXNTR(MXPOL) = MXPOL+MXNTR(MXPOL-1)
!          55 for MXPOL=10
!          In           -> xwpaes
!
! MXATR  = Allowed number of P*mP* transitions ( MXATR <= MXNTR )
!          In           -> mod_com_xctrmk
!                          mod_com_xpafit
!                          mod_com_xpara
!                          mod_com_xtran
!                          mod_com_xws
!                          mod_main_xctrmk
!                          mod_main_xpafit
!                          mod_par_x             -> MXTRAD
!                          xctrmk
!                          xpafit (*)
!                          xwpaes
!                          xwpart
!
! MXNTRT = Maximum number of P*mP* transition types (DELTA+ISRAM)
!
! MXATRT = Allowed number of P*mP* transition types ( MXATRT <= MXNTRT )
!                                                   ( MXATRT <= MXATR  )
!          In           -> mod_com_xpara
!                          mod_com_xtran
!                          mod_main_xctrmk
!                          mod_main_xpafit
!                          mod_par_x             -> MXOP,MXOBZ,LWA
!                          xctrmk
!                          xpafit (*)
!                          xwpafi
!
! MXGAM  = Maximum rotational degree.
!
! MXDROT = MXGAM+1
!          Dependancies -> development order.
!
! MXFAC  = size of the factorial table.
!          2*(MXJG+1)+(MXGAM+1)+1     (for instance: 2*200     +10       +1 = 411)
!          In           -> mod_com_fa
!                          facto
!
! NBAM   = number of angular momenta
!          In           -> ctrpmk
!                          nulpar
!                          paradj
!                          parchk
!                          parmk
!                          xpafit
!                          xwpaes
!                          xwpafi
!                          xwpart
!
! NBCLAB = number of characters in the label defining a parameter.
!          In           -> mod_com_paradj
!                          mod_com_xpara
!                          ased
!                          asen
!                          asnorm
!                          astatd
!                          astptr
!                          astran
!                          asvp
!                          ctrpmk
!                          dipmat
!                          dipmats
!                          eq_int
!                          eq_tds
!                          hdi
!                          hdiag
!                          hmatri
!                          jener
!                          levlst
!                          nulpar
!                          paradj
!                          parchk
!                          parmk
!                          polmat
!                          precf
!                          smatri
!                          spect
!                          spects
!                          tra
!                          trm
!                          trmomt
!                          xctrmk
!                          xpafit
!                          xwf
!                          xwpaes
!                          xwpafi
!                          xwpart
!                          xws
!                          XTDS
!
! NBCTIT = number of characters of the TITRE string (parameter file header)
!          In           -> mod_com_paradj
!                          asdi
!                          ased
!                          asen
!                          asfn
!                          asha
!                          asnorm
!                          asparv
!                          aspo
!                          astatd
!                          astptr
!                          astran
!                          asvp
!                          asxed
!                          ctrpmk
!                          dipmat
!                          dipmats
!                          eq_int
!                          eq_tds
!                          hdi
!                          hdiag
!                          hmatri
!                          jener
!                          levlst
!                          nulpar
!                          paradj
!                          parchk
!                          parmk
!                          polmat
!                          precf
!                          rovbas
!                          smatri
!                          spech
!                          spect
!                          spects
!                          tra
!                          trm
!                          trmomt
!                          xctrmk
!                          xpafit
!                          xhdiag
!                          xwf
!                          xwfa
!                          xwpaes
!                          xwpafi
!                          xws
!
! NBVQN  = number of vibrationnal quantum numbers.
!          In           -> mod_com_dipomod
!                          mod_com_elmr
!                          mod_com_fp
!                          mod_com_hmodel
!                          mod_com_model
!                          mod_com_spech
!                          mod_com_xwf
!                          dipmod
!                          e1to12
!                          elmr
!                          eq_tds
!                          hmodel
!                          levlst
!                          parchk
!                          parmk
!                          polmod
!                          sigvi
!                          spech
!                          xwf
!                          XTDS
!
! NB6C   = number of 6C.
!          In           -> mod_com_com6c
!                          cal6c  (*)
!                          sxc
!
! MXNBES = Maximum number of spectral elements.
! MXNLF  = Maximum number of line files.
! MXNPV  = Maximum number of points in half Voigt profile (including center, should respect MXNPV = 101+10*I).
! MXNMI  = Maximum number of points in half apparatus function profile (2*MXNMI+1).
! MXNTRA = Maximum number of implied transitions for one spectrum point.
!          In           -> mod_com_simul
!                          mod_main_simul
!                          simul
!                          simuls
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! defined in mod_par_x
!
! MXOP   = MXOPH+MXOPT*MXATRT
!          In           -> mod_com_xpafit
!                          mod_com_xpara
!                          mod_main_xctrmk
!                          mod_main_xpafit
!                          mod_par_x             -> MXOBZ,LWA
!                          xpafit
!                          xpfitc
!                          xwpaes
!
! MXOBZ  = 2*MXOBS+MXOP
!          In           -> mod_com_xfjac
!                          mod_com_xfonc
!                          mod_com_xpafit
!                          mod_main_xctrmk
!                          mod_main_xpafit
!                          mod_par_x             -> LWA
!                          rmsh
!                          rmst
!                          xpafit
!                          xpfitc
!                          xwpaes
!                          xwpart
!
! LWA    = 5*MXOP+MXOBZ
!          In           -> mod_com_xpafit
!                          mod_main_xctrmk
!                          mod_main_xpafit
!                          mod_par_x
!                          lmder1
!                          xpafit
!
! MXTRAD = 2*MXATR
!          In           -> mod_com_xtran
!                          mod_main_xctrmk
!                          mod_main_xpafit
!                          mod_par_x
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! MDMJCI
!          In           -> mod_com_dipmat        ->  6*MXSYM
!                          mod_com_polmat        -> 10*MXSYM
!                          mod_com_trm           -> 10*MXSYM
!                          dipmat
!                          dipmats
!                          polmat
!                          trm
!                          trmomt
!                          xtrm
!                          xtro
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! NBLMAX
!          In           -> mod_com_tri
!                          mod_com_xtri
!                          mod_main_tri
!                          mod_main_xtri
!                          tri
!                          triasg
!                          xtrias
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! defined for STARK
!
! MXDIST = CM block maximum dimension.
!          In           -> mod_com_sdi
!                          sdi
!
! MXJST  = Maximum J for Stark (<= 99, cf. coding in dipmats and spects (ICODIM)).
!          In           -> mod_com_sdi
!                          mod_com_smatri
!                          mod_com_spects
!                          dipmats
!                          sdi
!                          smatri
!                          spects
!
! MXMST  = Maximum M for Stark ( MXMST <= MXJST-2 requested )
!          In           -> sdi
!                          spects
!
! MXOPAL = Maximum number of operators in polarizability.
!          In           -> mod_com_smatri
!
! MXISUM = maximum number of matrix elements of dipole moment
!          with the same label for lower polyad.
!          In           -> mod_com_spects
!                          spects
!
!.......................................................................
! defined in spects
!
! ICODIM  = 100000*MXSYM+  &
!           10000*2+       &                     ! 2 = # parity
!           100*MXJST+     &                     ! WARNING : MXJST < 99
!           (MXJST+1)/2
!          In           -> spects
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
      integer ,parameter  :: MXBRA  = 5                                                            !  do NOT change
      integer ,save       :: MXDIMI = 1                                                            !  23             25
      integer ,save       :: MXDIMS = 1                                                            !  89             98
      integer ,save       :: MXELMD = 1                                                            !  8305           9140
      integer ,save       :: MXELMH = 1                                                            !  89             98
      integer ,save       :: MXELMT = 1                                                            !  529            582
      integer ,save       :: MXEMR  = 1                                                            !  4              5
      integer ,save       :: MXENI  = 1                                                            !  2116           2330
      integer ,parameter  :: MXJ    = 198                                                          !  do NOT change
      integer ,parameter  :: MXJG   = MXJ+1                                                        !  do NOT change
      integer ,save       :: MXNCR  = 1                                                            !  23             25
      integer ,save       :: MXNIV  = 1                                                            !  4              5
      integer ,save       :: MXOBS  = 1                                                            !  1346           1490
      integer ,save       :: MXOCV  = 1                                                            !  35             39
      integer ,save       :: MXOPH  = 1                                                            !  219            241
      integer ,save       :: MXOPR  = 1                                                            !  35             39
      integer ,save       :: MXOPT  = 1                                                            !  12             13
      integer ,save       :: MXOPVH = 1                                                            !  17             19
      integer ,save       :: MXOPVT = 1                                                            !  7              8
      integer ,save       :: MXSNV  = 1                                                            !  4              5
      integer ,parameter  :: MXSYM  = 4                                                            !  do NOT change
      integer ,parameter  :: MXPOL  = 10                                                           !  do NOT change
      integer ,save       :: MXATR  = 1                                                            !  do NOT change
      integer ,parameter  :: MXNTRT = 2*MXPOL                                                      !  do NOT change
      integer ,save       :: MXATRT = 1                                                            !  do NOT change
      integer ,parameter  :: MXGAM  = 9                                                            !  do NOT change
      integer ,parameter  :: MXDROT = MXGAM+1                                                      !  do NOT change
      integer ,parameter  :: MXFAC  = 2*(MXJG+1)+(MXGAM+1)+1                                       !  do NOT change
      integer ,parameter  :: NBAM   = 1                                                            !  do NOT change
      integer ,parameter  :: NBCLAB = 51                                                           !  do NOT change
      integer ,parameter  :: NBCTIT = 140                                                          !  do NOT change
      integer ,parameter  :: NBVQN  = 12                                                           !  do NOT change
      integer ,parameter  :: NB6C   = 64                                                           !  do NOT change
      integer ,save       :: MXNBES = 1                                                            !                 2000000
      integer ,save       :: MXNLF  = 1                                                            !                 10
      integer ,save       :: MXNPV  = 111                                                          !                 1001
      integer ,save       :: MXNMI  = 1                                                            !                 500
      integer ,save       :: MXNTRA = 1                                                            !                 1000000
      integer ,parameter  :: MXDIST = 700                                                          !                 700
      integer ,parameter  :: MXJST  = 55                                                           !                 55
      integer ,parameter  :: MXMST  = 25                                                           !                 25
      integer ,parameter  :: MXOPAL = 6                                                            !  do NOT change
      integer ,parameter  :: MXISUM = 200                                                          !  do NOT change
      integer ,save       :: NBLMAX = 1                                                            !                     MXOBS


      end module mod_par_tds
