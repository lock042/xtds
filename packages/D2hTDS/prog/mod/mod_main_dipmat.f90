
      module mod_main_dipmat

      use mod_dppr
      use mod_par_tds
      use mod_com_dipmat
      use mod_com_dipomat
      use mod_com_matri


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine alloc_all
      IMPLICIT NONE

      call alloc_dipmat
      call alloc_dipomat
      call alloc_matri
!
      return
      end subroutine alloc_all

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXDIMI
      IMPLICIT NONE

      integer  :: M_DIMI

      M_DIMI = MXRES(MXDIMI)
      call RESIZE_MXDIMI_DIPMAT(M_DIMI)
      call RESIZE_MXDIMI_DIPOMAT(M_DIMI)
      MXDIMI = M_DIMI
!
      return
      end subroutine RESIZE_MXDIMI

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXDIMS
      IMPLICIT NONE

      integer  :: M_DIMS

      M_DIMS = MXRES(MXDIMS)
      call RESIZE_MXDIMS_DIPOMAT(M_DIMS)
      MXDIMS = M_DIMS
!
      return
      end subroutine RESIZE_MXDIMS

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXELMT
      IMPLICIT NONE

      integer  :: M_ELMT

      M_ELMT = MXRES(MXELMT)
      call RESIZE_MXELMT_DIPOMAT(M_ELMT)
      MXELMT = M_ELMT
!
      return
      end subroutine RESIZE_MXELMT

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXEMR
      IMPLICIT NONE

      integer  :: M_EMR

      M_EMR = MXRES(MXEMR)
      call RESIZE_MXEMR_DIPOMAT(M_EMR)
      MXEMR = M_EMR
!
      return
      end subroutine RESIZE_MXEMR

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXOPT
      IMPLICIT NONE

      integer  :: M_OPT

      M_OPT = MXRES(MXOPT)
      call RESIZE_MXOPT_DIPOMAT(M_OPT)
      MXOPT = M_OPT
!
      return
      end subroutine RESIZE_MXOPT

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXOPVT
      IMPLICIT NONE

      integer  :: M_OPVT

      M_OPVT = MXRES(MXOPVT)
      call RESIZE_MXOPVT_DIPOMAT(M_OPVT)
      MXOPVT = M_OPVT
!
      return
      end subroutine RESIZE_MXOPVT

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXSNV
      IMPLICIT NONE

      integer  :: M_SNV

      M_SNV = MXRES(MXSNV)
      call RESIZE_MXSNV_MATRI(M_SNV)
      MXSNV = M_SNV
!
      return
      end subroutine RESIZE_MXSNV

      end module mod_main_dipmat
