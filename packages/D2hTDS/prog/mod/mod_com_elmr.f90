
      module mod_com_elmr

      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      integer ,pointer ,dimension(:,:) ,save  :: IPK                                               ! (NBVQN+2,MXSNV)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_ELMR
      IMPLICIT NONE

      integer  :: ierr

      allocate(IPK(NBVQN+2,MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_ELMR_IPK')
      IPK = 0
!
      return
      end subroutine ALLOC_ELMR

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXSNV

      subroutine RESIZE_MXSNV_ELMR(C_SNV)
      IMPLICIT NONE
      integer :: C_SNV

      integer :: i,ierr

! IPK
      allocate(ipd2(NBVQN+2,C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_ELMR_IPK')
      ipd2 = 0
      do i=1,MXSNV
        ipd2(:,i) = IPK(:,i)
      ENDDO
      deallocate(IPK)
      IPK => ipd2
!
      return
      end subroutine RESIZE_MXSNV_ELMR

      end module mod_com_elmr
