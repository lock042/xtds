
      module mod_com_statf

      use mod_dppr
      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      real(kind=dppr) ,dimension(0:MXJ) ,save  :: ECU   = 0.d0
      real(kind=dppr) ,dimension(0:MXJ) ,save  :: SPCU  = 0.d0
      real(kind=dppr) ,dimension(0:MXJ) ,save  :: TNEGL = 0.d0

      integer         ,dimension(0:MXJ) ,save  :: NCU   = 0
!
      real(kind=dppr) ,pointer ,dimension(:,:,:) ,save  :: SMJ,SPOI,SPROR                          ! (0:MXJ,0:MXNIV,0:MXNIV)

      integer         ,pointer ,dimension(:,:,:) ,save  :: NFJ                                     ! (0:MXJ,0:MXNIV,0:MXNIV)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_STATF
      IMPLICIT NONE

      integer  :: ierr

      allocate(SMJ(0:MXJ,0:MXNIV,0:MXNIV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_STATF_SMJ')
      SMJ = 0.d0
      allocate(SPOI(0:MXJ,0:MXNIV,0:MXNIV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_STATF_SPOI')
      SPOI = 0.d0
      allocate(SPROR(0:MXJ,0:MXNIV,0:MXNIV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_STATF_SPROR')
      SPROR = 0.d0

      allocate(NFJ(0:MXJ,0:MXNIV,0:MXNIV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_STATF_NFJ')
      NFJ = 0
!
      return
      end subroutine ALLOC_STATF

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXNIV

      subroutine RESIZE_MXNIV_STATF(C_NIV)
      IMPLICIT NONE
      integer :: C_NIV

      integer :: i,ierr
      integer :: j

! SMJ
      allocate(rpd3(0:MXJ,0:C_NIV,0:C_NIV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXNIV_STATF_SMJ')
      rpd3 = 0.d0
      do j=0,MXNIV
        do i=0,MXNIV
          rpd3(:,i,j) = SMJ(:,i,j)
        enddo
      enddo
      deallocate(SMJ)
      SMJ => rpd3
! SPOI
      allocate(rpd3(0:MXJ,0:C_NIV,0:C_NIV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXNIV_STATF_SPOI')
      rpd3 = 0.d0
      do j=0,MXNIV
        do i=0,MXNIV
          rpd3(:,i,j) = SPOI(:,i,j)
        enddo
      enddo
      deallocate(SPOI)
      SPOI => rpd3
! SPROR
      allocate(rpd3(0:MXJ,0:C_NIV,0:C_NIV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXNIV_STATF_SPROR')
      rpd3 = 0.d0
      do j=0,MXNIV
        do i=0,MXNIV
          rpd3(:,i,j) = SPROR(:,i,j)
        enddo
      enddo
      deallocate(SPROR)
      SPROR => rpd3

! NFJ
      allocate(ipd3(0:MXJ,0:C_NIV,0:C_NIV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXNIV_STATF_NFJ')
      ipd3 = 0
      do j=0,MXNIV
        do i=0,MXNIV
          ipd3(:,i,j) = NFJ(:,i,j)
        enddo
      enddo
      deallocate(NFJ)
      NFJ => ipd3
!
      return
      end subroutine RESIZE_MXNIV_STATF

      end module mod_com_statf
