
      module mod_com_dipomod

      use mod_dppr
      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      integer         ,pointer ,dimension(:)     ,save  :: ICODRO,ICOSY1,ICOSY2                    ! (MXOPT)
      integer         ,pointer ,dimension(:,:)   ,save  :: ICODVA,ICODVC                           ! (MXOPT,4)
      integer         ,pointer ,dimension(:,:)   ,save  :: IFFI,IFFS                               ! (NBVQN+2,MXSNV)
      integer         ,pointer ,dimension(:,:,:) ,save  :: NVIBI,NVIBS                             ! (MXPOL,MXNIV,NBVQN)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_DIPOMOD
      IMPLICIT NONE

      integer  :: ierr

      allocate(ICODRO(MXOPT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DIPOMOD_ICODRO')
      ICODRO = 0
      allocate(ICOSY1(MXOPT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DIPOMOD_ICOSY1')
      ICOSY1 = 0
      allocate(ICOSY2(MXOPT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DIPOMOD_ICOSY2')
      ICOSY2 = 0
      allocate(ICODVA(MXOPT,4),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DIPOMOD_ICODVA')
      ICODVA = 0
      allocate(ICODVC(MXOPT,4),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DIPOMOD_ICODVC')
      ICODVC = 0
      allocate(IFFI(NBVQN+2,MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DIPOMOD_IFFI')
      IFFI = 0
      allocate(IFFS(NBVQN+2,MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DIPOMOD_IFFS')
      IFFS = 0
      allocate(NVIBI(MXPOL,MXNIV,NBVQN),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DIPOMOD_NVIBI')
      NVIBI = 0
      allocate(NVIBS(MXPOL,MXNIV,NBVQN),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DIPOMOD_NVIBS')
      NVIBS = 0
!
      return
      end subroutine ALLOC_DIPOMOD

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXNIV

      subroutine RESIZE_MXNIV_DIPOMOD(C_NIV)
      IMPLICIT NONE
      integer :: C_NIV

      integer :: i,ierr
      integer :: j

! NVIBI
      allocate(ipd3(MXPOL,C_NIV,NBVQN),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXNIV_DIPOMOD_NVIBI')
      ipd3 = 0
      do j=1,NBVQN
        do i=1,MXNIV
          ipd3(:,i,j) = NVIBI(:,i,j)
        enddo
      enddo
      deallocate(NVIBI)
      NVIBI => ipd3
! NVIBS
      allocate(ipd3(MXPOL,C_NIV,NBVQN),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXNIV_DIPOMOD_NVIBS')
      ipd3 = 0
      do j=1,NBVQN
        do i=1,MXNIV
          ipd3(:,i,j) = NVIBS(:,i,j)
        enddo
      enddo
      deallocate(NVIBS)
      NVIBS => ipd3
!
      return
      end subroutine RESIZE_MXNIV_DIPOMOD

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOPT

      subroutine RESIZE_MXOPT_DIPOMOD(C_OPT)
      IMPLICIT NONE
      integer :: C_OPT

      integer :: i,ierr

! ICODRO
      allocate(ipd1(C_OPT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPT_DIPOMOD_ICODRO')
      ipd1 = 0
      ipd1(1:MXOPT) = ICODRO(:)
      deallocate(ICODRO)
      ICODRO => ipd1
! ICOSY1
      allocate(ipd1(C_OPT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPT_DIPOMOD_ICOSY1')
      ipd1 = 0
      ipd1(1:MXOPT) = ICOSY1(:)
      deallocate(ICOSY1)
      ICOSY1 => ipd1
! ICOSY2
      allocate(ipd1(C_OPT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPT_DIPOMOD_ICOSY2')
      ipd1 = 0
      ipd1(1:MXOPT) = ICOSY2(:)
      deallocate(ICOSY2)
      ICOSY2 => ipd1
! ICODVA
      allocate(ipd2(C_OPT,4),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPT_DIPOMOD_ICODVA')
      ipd2 = 0
      do i=1,4
        ipd2(1:MXOPT,i) = ICODVA(:,i)
      enddo
      deallocate(ICODVA)
      ICODVA => ipd2
! ICODVC
      allocate(ipd2(C_OPT,4),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPT_DIPOMOD_ICODVC')
      ipd2 = 0
      do i=1,4
        ipd2(1:MXOPT,i) = ICODVC(:,i)
      enddo
      deallocate(ICODVC)
      ICODVC => ipd2
!
      return
      end subroutine RESIZE_MXOPT_DIPOMOD

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXSNV

      subroutine RESIZE_MXSNV_DIPOMOD(C_SNV)
      IMPLICIT NONE
      integer :: C_SNV

      integer :: i,ierr

! IFFI
      allocate(ipd2(NBVQN+2,C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_DIPOMOD_IFFI')
      ipd2 = 0
      do i=1,MXSNV
        ipd2(:,i) = IFFI(:,i)
      enddo
      deallocate(IFFI)
      IFFI => ipd2
! IFFS
      allocate(ipd2(NBVQN+2,C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_DIPOMOD_IFFS')
      ipd2 = 0
      do i=1,MXSNV
        ipd2(:,i) = IFFS(:,i)
      enddo
      deallocate(IFFS)
      IFFS => ipd2
!
      return
      end subroutine RESIZE_MXSNV_DIPOMOD

      end module mod_com_dipomod
