
      module mod_com_model

      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      real(kind=dppr) ,pointer ,dimension(:)   ,save  :: EMOP                                      ! (MXEMR)

      integer         ,pointer ,dimension(:)   ,save  :: IC,LI                                     ! (MXEMR)
      integer         ,pointer ,dimension(:,:) ,save  :: IKVA,IKVC                                 ! (NBVQN+2,MXSNV)
      integer         ,pointer ,dimension(:)   ,save  :: IDRO,IDSY1,IDSY2,IRDRE                    ! (MXOCV)
      integer         ,pointer ,dimension(:,:) ,save  :: IDVA,IDVC                                 ! (MXOCV,4)
      integer         ,pointer ,dimension(:)   ,save  :: IROCO                                     ! (MXOPR)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_MODEL
      IMPLICIT NONE

      integer  :: ierr

      allocate(EMOP(MXEMR),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_MODEL_EMOP')
      EMOP = 0.d0

      allocate(IC(MXEMR),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_MODEL_IC')
      IC = 0
      allocate(LI(MXEMR),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_MODEL_LI')
      LI = 0
      allocate(IKVA(NBVQN+2,MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_MODEL_IKVA')
      IKVA = 0
      allocate(IKVC(NBVQN+2,MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_MODEL_IKVC')
      IKVC = 0
      allocate(IDRO(MXOCV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_MODEL_IDRO')
      IDRO = 0
      allocate(IDSY1(MXOCV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_MODEL_IDSY1')
      IDSY1 = 0
      allocate(IDSY2(MXOCV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_MODEL_IDSY2')
      IDSY2 = 0
      allocate(IRDRE(MXOCV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_MODEL_IRDRE')
      IRDRE = 0
      allocate(IDVA(MXOCV,4),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_MODEL_IDVA')
      IDVA = 0
      allocate(IDVC(MXOCV,4),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_MODEL_IDVC')
      IDVC = 0
      allocate(IROCO(MXOPR),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_MODEL_IROCO')
      IROCO = 0
!
      return
      end subroutine ALLOC_MODEL

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXEMR

      subroutine RESIZE_MXEMR_MODEL(C_EMR)
      IMPLICIT NONE
      integer :: C_EMR

      integer :: ierr

! EMOP
      allocate(rpd1(C_EMR),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXEMR_MODEL_EMOP')
      rpd1 = 0.d0
      rpd1(1:MXEMR) = EMOP(:)
      deallocate(EMOP)
      EMOP => rpd1

! IC
      allocate(ipd1(C_EMR),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXEMR_MODEL_IC')
      ipd1 = 0
      ipd1(1:MXEMR) = IC(:)
      deallocate(IC)
      IC => ipd1
! LI
      allocate(ipd1(C_EMR),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXEMR_MODEL_LI')
      ipd1 = 0
      ipd1(1:MXEMR) = LI(:)
      deallocate(LI)
      LI => ipd1
!
      return
      end subroutine RESIZE_MXEMR_MODEL

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOCV

      subroutine RESIZE_MXOCV_MODEL(C_OCV)
      IMPLICIT NONE
      integer :: C_OCV

      integer :: i,ierr

! IDRO
      allocate(ipd1(C_OCV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOCV_MODEL_IDRO')
      ipd1 = 0
      ipd1(1:MXOCV) = IDRO(:)
      deallocate(IDRO)
      IDRO => ipd1
! IDSY1
      allocate(ipd1(C_OCV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOCV_MODEL_IDSY1')
      ipd1 = 0
      ipd1(1:MXOCV) = IDSY1(:)
      deallocate(IDSY1)
      IDSY1 => ipd1
! IDSY2
      allocate(ipd1(C_OCV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOCV_MODEL_IDSY2')
      ipd1 = 0
      ipd1(1:MXOCV) = IDSY2(:)
      deallocate(IDSY2)
      IDSY2 => ipd1
! IRDRE
      allocate(ipd1(C_OCV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOCV_MODEL_IRDRE')
      ipd1 = 0
      ipd1(1:MXOCV) = IRDRE(:)
      deallocate(IRDRE)
      IRDRE => ipd1
! IDVA
      allocate(ipd2(C_OCV,4),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOCV_MODEL_IDVA')
      ipd2 = 0
      do i=1,4
        ipd2(1:MXOCV,i) = IDVA(:,i)
      enddo
      deallocate(IDVA)
      IDVA => ipd2
! IDVC
      allocate(ipd2(C_OCV,4),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOCV_MODEL_IDVC')
      ipd2 = 0
      do i=1,4
        ipd2(1:MXOCV,i) = IDVC(:,i)
      enddo
      deallocate(IDVC)
      IDVC => ipd2
!
      return
      end subroutine RESIZE_MXOCV_MODEL

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOPR

      subroutine RESIZE_MXOPR_MODEL(C_OPR)
      IMPLICIT NONE
      integer :: C_OPR

      integer :: ierr

! IROCO
      allocate(ipd1(C_OPR),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPR_MODEL_IROCO')
      ipd1 = 0
      ipd1(1:MXOPR) = IROCO(:)
      deallocate(IROCO)
      IROCO => ipd1
!
      return
      end subroutine RESIZE_MXOPR_MODEL

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXSNV

      subroutine RESIZE_MXSNV_MODEL(C_SNV)
      IMPLICIT NONE
      integer :: C_SNV

      integer :: i,ierr

! IKVA
      allocate(ipd2(NBVQN+2,C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_MODEL_IKVA')
      ipd2 = 0
      do i=1,MXSNV
        ipd2(:,i) = IKVA(:,i)
      ENDDO
      deallocate(IKVA)
      IKVA => ipd2
! IKVC
      allocate(ipd2(NBVQN+2,C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_MODEL_IKVC')
      ipd2 = 0
      do i=1,MXSNV
        ipd2(:,i) = IKVC(:,i)
      ENDDO
      deallocate(IKVC)
      IKVC => ipd2
!
      return
      end subroutine RESIZE_MXSNV_MODEL

      end module mod_com_model
