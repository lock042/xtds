
      module mod_com_dipmat

      use mod_dppr
      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      integer ,parameter  :: MDMJCI = 6*MXSYM

      integer ,dimension(MDMJCI)  :: JICDIM = 0
!
      integer ,pointer ,dimension(:,:) ,save  :: NRCI,NVCI                                         ! (MXDIMI,MDMJCI)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_DIPMAT
      IMPLICIT NONE

      integer  :: ierr

      allocate(NRCI(MXDIMI,MDMJCI),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DIPMAT_NRCI')
      NRCI = 0
      allocate(NVCI(MXDIMI,MDMJCI),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DIPMAT_NVCI')
      NVCI = 0
!
      return
      end subroutine ALLOC_DIPMAT

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXDIMI

      subroutine RESIZE_MXDIMI_DIPMAT(C_DIMI)
      IMPLICIT NONE
      integer :: C_DIMI

      integer :: i,ierr

! NRCI
      allocate(ipd2(C_DIMI,MDMJCI),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXDIMI_DIPMAT_NRCI')
      ipd2 = 0
      do i=1,MDMJCI
        ipd2(1:MXDIMI,i) = NRCI(:,i)
      enddo
      deallocate(NRCI)
      NRCI => ipd2
! NVCI
      allocate(ipd2(C_DIMI,MDMJCI),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXDIMI_DIPMAT_NVCI')
      ipd2 = 0
      do i=1,MDMJCI
        ipd2(1:MXDIMI,i) = NVCI(:,i)
      enddo
      deallocate(NVCI)
      NVCI => ipd2
!
      return
      end subroutine RESIZE_MXDIMI_DIPMAT

      end module mod_com_dipmat
