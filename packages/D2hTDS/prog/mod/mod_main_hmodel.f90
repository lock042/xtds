
      module mod_main_hmodel

      use mod_dppr
      use mod_par_tds
      use mod_com_elmr
      use mod_com_hmodel
      use mod_com_model


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine alloc_all
      IMPLICIT NONE

      call alloc_elmr
      call alloc_hmodel
      call alloc_model
!
      return
      end subroutine alloc_all

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXEMR
      IMPLICIT NONE

      integer  :: M_EMR

      M_EMR = MXRES(MXEMR)
      call RESIZE_MXEMR_MODEL(M_EMR)
      MXEMR = M_EMR
!
      return
      end subroutine RESIZE_MXEMR

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXNIV
      IMPLICIT NONE

      integer  :: M_NIV

      M_NIV = MXRES(MXNIV)
      call RESIZE_MXNIV_HMODEL(M_NIV)
      MXNIV = M_NIV
!
      return
      end subroutine RESIZE_MXNIV

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXOCV
      IMPLICIT NONE

      integer  :: M_OCV

      M_OCV = MXRES(MXOCV)
      call RESIZE_MXOCV_MODEL(M_OCV)
      MXOCV = M_OCV
!
      return
      end subroutine RESIZE_MXOCV

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXOPH
      IMPLICIT NONE

      integer  :: M_OPH

      M_OPH = MXRES(MXOPH)
      call RESIZE_MXOPH_HMODEL(M_OPH)
      MXOPH = M_OPH
!
      return
      end subroutine RESIZE_MXOPH

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXOPR
      IMPLICIT NONE

      integer  :: M_OPR

      M_OPR = MXRES(MXOPR)
      call RESIZE_MXOPR_MODEL(M_OPR)
      MXOPR = M_OPR
!
      return
      end subroutine RESIZE_MXOPR

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXSNV
      IMPLICIT NONE

      integer  :: M_SNV

      M_SNV = MXRES(MXSNV)
      call RESIZE_MXSNV_ELMR(M_SNV)
      call RESIZE_MXSNV_HMODEL(M_SNV)
      call RESIZE_MXSNV_MODEL(M_SNV)
      MXSNV = M_SNV
!
      return
      end subroutine RESIZE_MXSNV

      end module mod_main_hmodel
