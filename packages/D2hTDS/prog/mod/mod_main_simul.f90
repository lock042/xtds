
      module mod_main_simul

      use mod_dppr
      use mod_par_tds
      use mod_com_simul


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine alloc_all
      IMPLICIT NONE

      call alloc_simul
!
      return
      end subroutine alloc_all

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXNBES
      IMPLICIT NONE

      integer  :: M_NBES

      M_NBES = MXRES(MXNBES)
      call RESIZE_MXNBES_SIMUL(M_NBES)
      MXNBES = M_NBES
!
      return
      end subroutine RESIZE_MXNBES

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXNLF
      IMPLICIT NONE

      integer  :: M_NLF

      M_NLF = MXRES(MXNLF)
      call RESIZE_MXNLF_SIMUL(M_NLF)
      MXNLF = M_NLF
!
      return
      end subroutine RESIZE_MXNLF

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXNMI
      IMPLICIT NONE

      integer  :: M_NMI

      M_NMI = MXRES(MXNMI)
      call RESIZE_MXNMI_SIMUL(M_NMI)
      MXNMI = M_NMI
!
      return
      end subroutine RESIZE_MXNMI

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXNPV  = Maximum number of points in half Voigt profile (including center, should respect MXNPV = 101+10*I).
      subroutine RESIZE_MXNPV
      IMPLICIT NONE

      integer  :: M_NPV
      integer  :: i

      i     = (MXNPV-101)/10
      i     = MXRES(i)
      M_NPV = 101+10*i
      call RESIZE_MXNPV_SIMUL(M_NPV)
      MXNPV = M_NPV
!
      return
      end subroutine RESIZE_MXNPV

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXNTRA
      IMPLICIT NONE

      integer  :: M_NTRA

      M_NTRA = MXRES(MXNTRA)
      call RESIZE_MXNTRA_SIMUL(M_NTRA)
      MXNTRA = M_NTRA
!
      return
      end subroutine RESIZE_MXNTRA

      end module mod_main_simul
