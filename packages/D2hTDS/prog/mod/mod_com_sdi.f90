
      module mod_com_sdi

      use mod_dppr
      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      real(kind=dppr) ,dimension(MXDIST,MXDIST)              ,save  :: HST  = 0.d0
      real(kind=dppr) ,dimension(MXDIST,MXDIST)              ,save  :: T    = 0.d0
      real(kind=dppr) ,dimension(MXDIST)                     ,save  :: HD   = 0.d0
      real(kind=dppr)                                        ,save  :: ELEC = 0.d0

      integer         ,dimension(0:MXJST,MXSYM,1:2)          ,save  :: INFB = 0
      integer         ,dimension(MXDIST)                     ,save  :: K    = 0
      integer         ,dimension(0:MXJST)                    ,save  :: IJ   = 0
      integer         ,dimension(0:MXJST)                    ,save  :: INC  = 0
      integer         ,dimension(-1:MXJST)                   ,save  :: ISUM = 0
      integer         ,dimension(MXSYM,1:2,0:MXJST,0:MXDIST) ,save  :: INDJ = 0
!
      real(kind=dppr) ,pointer ,dimension(:,:,:,:,:) ,save  :: H0                                  ! (MXSYM,2,0:MXJST,0:MXELMH,0:MXELMH)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_SDI
      IMPLICIT NONE

      integer  :: ierr

      allocate(H0(MXSYM,2,0:MXJST,0:MXELMH,0:MXELMH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_SDI_H0')
      H0 = 0.d0
!
      return
      end subroutine ALLOC_SDI

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXELMH

      subroutine RESIZE_MXELMH_SDI(C_ELMH)
      IMPLICIT NONE
      integer :: C_ELMH

      integer :: i,ierr
      integer :: j

! H0
      allocate(rpd5(MXSYM,2,0:MXJST,0:C_ELMH,0:C_ELMH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXELMH_SDI_H0')
      rpd5 = 0.d0
      do j=0,MXELMH
        do i=0,MXELMH
          rpd5(:,:,:,i,j) = H0(:,:,:,i,j)
        enddo
      enddo
      deallocate(H0)
      H0 => rpd5
!
      return
      end subroutine RESIZE_MXELMH_SDI

      end module mod_com_sdi
