
      module mod_main_tra

      use mod_dppr
      use mod_par_tds
      use mod_com_tra


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine alloc_all
      IMPLICIT NONE

      call alloc_tra
!
      return
      end subroutine alloc_all

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXENI
      IMPLICIT NONE

      integer  :: M_ENI

      M_ENI = MXRES(MXENI)
      call RESIZE_MXENI_TRA(M_ENI)
      MXENI = M_ENI
!
      return
      end subroutine RESIZE_MXENI

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXSNV
      IMPLICIT NONE

      integer  :: M_SNV

      M_SNV = MXRES(MXSNV)
      call RESIZE_MXSNV_TRA(M_SNV)
      MXSNV = M_SNV
!
      return
      end subroutine RESIZE_MXSNV

      end module mod_main_tra
