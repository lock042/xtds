
      module mod_main_precf

      use mod_dppr
      use mod_par_tds
      use mod_com_der
      use mod_com_derv
      use mod_com_eqfc
      use mod_com_precf


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine alloc_all
      IMPLICIT NONE

      call alloc_der
      call alloc_derv
      call alloc_eqfc
      call alloc_precf
!
      return
      end subroutine alloc_all

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXOBS
      IMPLICIT NONE

      integer  :: M_OBS

      M_OBS = MXRES(MXOBS)
      call RESIZE_MXOBS_DER(M_OBS)
      call RESIZE_MXOBS_PRECF(M_OBS)
      MXOBS = M_OBS
!
      return
      end subroutine RESIZE_MXOBS

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXOPH
      IMPLICIT NONE

      integer  :: M_OPH

      M_OPH = MXRES(MXOPH)
      call RESIZE_MXOPH_DER(M_OPH)
      call RESIZE_MXOPH_DERV(M_OPH)
      call RESIZE_MXOPH_EQFC(M_OPH)
      call RESIZE_MXOPH_PRECF(M_OPH)
      MXOPH = M_OPH
!
      return
      end subroutine RESIZE_MXOPH

      end module mod_main_precf
