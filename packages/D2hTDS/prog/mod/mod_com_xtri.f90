
      module mod_com_xtri

      use mod_dppr
      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      integer ,parameter  :: NBCMAX = 6
      integer ,parameter  :: NRE    = NBCMAX+1

      integer              ,dimension(NRE)    ,save  :: PCOL   = 0
      integer              ,dimension(NRE)    ,save  :: DCOL   = 0
      integer              ,dimension(NRE)    ,save  :: NBCOL  = 0
      integer                                 ,save  :: NBCHPS = 0
      integer                                 ,save  :: NBLGN  = 0

      character(len =   1) ,dimension(NBCMAX) ,save  :: TYPE   = ''
      character(len =   1) ,dimension(NBCMAX) ,save  :: VAR    = ''
      character(len =  20) ,dimension(NBCMAX) ,save  :: FOR    = ''
      character(len = 188) ,dimension(NBCMAX) ,save  :: TEST   = ''                                ! 152+36 cf. FORMAT 1002 of xasg0.f
      character(len = 188)                    ,save  :: LIGNE  = ''                                ! 152+36 cf. FORMAT 1002 of xasg0.f
!
      real(kind=dppr)      ,pointer ,dimension(:,:) ,save  :: DVAR                                 ! (NBCMAX,NBLMAX)

      integer              ,pointer ,dimension(:)   ,save  :: IND                                  ! (NBLMAX)

      character(len = 188) ,pointer ,dimension(:)   ,save  :: CHAINE                               ! (NBLMAX+1) ; 152+36 cf. FORMAT 1002 of xasg0.f


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_XTRI
      IMPLICIT NONE

      integer  :: ierr

      allocate(DVAR(NBCMAX,NBLMAX),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XTRI_DVAR')
      DVAR = 0.d0
      allocate(IND(NBLMAX),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XTRI_IND')
      IND = 0
      allocate(CHAINE(NBLMAX+1),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XTRI_CHAINE')
      CHAINE = ''
!
      return
      end subroutine ALLOC_XTRI

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! NBLMAX

      subroutine RESIZE_NBLMAX_XTRI(C_LMAX)
      IMPLICIT NONE
      integer :: C_LMAX

      integer :: i,ierr

      character(len = 188) ,pointer ,dimension(:)  :: cpd1_188

! DVAR
      allocate(rpd2(NBCMAX,C_LMAX),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_NBLMAX_XTRI_DVAR')
      rpd2 = 0.d0
      do i=1,NBCMAX
        rpd2(i,1:NBLMAX) = DVAR(i,:)
      enddo
      deallocate(DVAR)
      DVAR => rpd2
! IND
      allocate(ipd1(C_LMAX),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_NBLMAX_XTRI_IND')
      ipd1 = 0
      ipd1(1:NBLMAX) = IND(:)
      deallocate(IND)
      IND => ipd1
! CHAINE
      allocate(cpd1_188(C_LMAX+1),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_NBLMAX_XTRI_CHAINE')
      cpd1_188 = ''
      cpd1_188(1:NBLMAX+1) = CHAINE(:)
      deallocate(CHAINE)
      CHAINE => cpd1_188
!
      return
      end subroutine RESIZE_NBLMAX_XTRI

      end module mod_com_xtri
