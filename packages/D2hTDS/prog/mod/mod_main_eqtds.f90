
      module mod_main_eqtds

      use mod_dppr
      use mod_par_tds
      use mod_com_cas
      use mod_com_der
      use mod_com_derv
      use mod_com_en
      use mod_com_eqfc
      use mod_com_eqtds
      use mod_com_statf


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine alloc_all
      IMPLICIT NONE

      call alloc_cas
      call alloc_der
      call alloc_derv
      call alloc_en
      call alloc_eqfc
      call alloc_eqtds
      call alloc_statf
!
      return
      end subroutine alloc_all

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXNIV
      IMPLICIT NONE

      integer  :: M_NIV

      M_NIV = MXRES(MXNIV)
      call RESIZE_MXNIV_EQTDS(M_NIV)
      call RESIZE_MXNIV_STATF(M_NIV)
      MXNIV = M_NIV
!
      return
      end subroutine RESIZE_MXNIV

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXOBS
      IMPLICIT NONE

      integer  :: M_OBS

      M_OBS = MXRES(MXOBS)
      call RESIZE_MXOBS_CAS(M_OBS)
      call RESIZE_MXOBS_DER(M_OBS)
      call RESIZE_MXOBS_EN(M_OBS)
      call RESIZE_MXOBS_EQTDS(M_OBS)
      MXOBS = M_OBS
!
      return
      end subroutine RESIZE_MXOBS

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXOPH
      IMPLICIT NONE

      integer  :: M_OPH

      M_OPH = MXRES(MXOPH)
      call RESIZE_MXOPH_DER(M_OPH)
      call RESIZE_MXOPH_DERV(M_OPH)
      call RESIZE_MXOPH_EQFC(M_OPH)
      call RESIZE_MXOPH_EQTDS(M_OPH)
      MXOPH = M_OPH
!
      return
      end subroutine RESIZE_MXOPH

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXSNV
      IMPLICIT NONE

      integer  :: M_SNV

      M_SNV = MXRES(MXSNV)
      call RESIZE_MXSNV_EQTDS(M_SNV)
      MXSNV = M_SNV
!
      return
      end subroutine RESIZE_MXSNV

      end module mod_main_eqtds
