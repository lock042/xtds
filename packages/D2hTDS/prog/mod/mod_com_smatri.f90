
      module mod_com_smatri

      use mod_dppr
      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      real(kind=dppr) ,dimension(MXOPAL)          ,save  :: PARA = 0.d0

      integer         ,dimension(0:MXJST,MXSYM,2) ,save  :: INFB = 0
!
      real(kind=dppr) ,pointer ,dimension(:)             ,save  :: H                               ! (MXELMT)
      real(kind=dppr) ,pointer ,dimension(:,:,:,:,:,:,:) ,save  :: P2                              ! (0:1,0:MXJST,0:MXJST,MXSYM,2,0:MXDIMS,0:MXDIMS)
      real(kind=dppr) ,pointer ,dimension(:,:,:,:,:,:)   ,save  :: P0                              ! (0:MXJST,0:MXJST,MXSYM,2,0:MXDIMS,0:MXDIMS)

      integer         ,pointer ,dimension(:)             ,save  :: KO,LI                           ! (MXELMT)
      integer         ,pointer ,dimension(:)             ,save  :: ICROT,NRCOD,NVCOD               ! (MXDIMS)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_SMATRI
      IMPLICIT NONE

      integer  :: ierr

      allocate(H(MXELMT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_SMATRI_H')
      H = 0.d0
      allocate(P2(0:1,0:MXJST,0:MXJST,MXSYM,2,0:MXDIMS,0:MXDIMS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_SMATRI_P2')
      P2 = 0.d0
      allocate(P0(0:MXJST,0:MXJST,MXSYM,2,0:MXDIMS,0:MXDIMS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_SMATRI_P0')
      P0 = 0.d0

      allocate(KO(MXELMT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_SMATRI_KO')
      KO = 0
      allocate(LI(MXELMT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_SMATRI_LI')
      LI = 0
      allocate(ICROT(MXDIMS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_SMATRI_ICROT')
      ICROT = 0
      allocate(NRCOD(MXDIMS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_SMATRI_NRCOD')
      NRCOD = 0
      allocate(NVCOD(MXDIMS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_SMATRI_NVCOD')
      NVCOD = 0
!
      return
      end subroutine ALLOC_SMATRI

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXDIMS

      subroutine RESIZE_MXDIMS_SMATRI(C_DIMS)
      IMPLICIT NONE
      integer :: C_DIMS

      integer :: i,ierr
      integer :: j

! P2
      allocate(rpd7(0:1,0:MXJST,0:MXJST,MXSYM,2,0:C_DIMS,0:C_DIMS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXDIMS_SMATRI_P2')
      rpd7 = 0.d0
      do j=0,MXDIMS
        do i=0,MXDIMS
          rpd7(:,:,:,:,:,i,j) = P2(:,:,:,:,:,i,j)
        enddo
      enddo
      deallocate(P2)
      P2 => rpd7
! P0
      allocate(rpd6(0:MXJST,0:MXJST,MXSYM,2,0:C_DIMS,0:C_DIMS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXDIMS_SMATRI_P0')
      rpd6 = 0.d0
      do j=0,MXDIMS
        do i=0,MXDIMS
          rpd6(:,:,:,:,i,j) = P0(:,:,:,:,i,j)
        enddo
      enddo
      deallocate(P0)
      P0 => rpd6

! ICROT
      allocate(ipd1(C_DIMS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXDIMS_SMATRI_ICROT')
      ipd1 = 0
      ipd1(1:MXDIMS) = ICROT(:)
      deallocate(ICROT)
      ICROT => ipd1
! NRCOD
      allocate(ipd1(C_DIMS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXDIMS_SMATRI_NRCOD')
      ipd1 = 0
      ipd1(1:MXDIMS) = NRCOD(:)
      deallocate(NRCOD)
      NRCOD => ipd1
! NVCOD
      allocate(ipd1(C_DIMS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXDIMS_SMATRI_NVCOD')
      ipd1 = 0
      ipd1(1:MXDIMS) = NVCOD(:)
      deallocate(NVCOD)
      NVCOD => ipd1
!
      return
      end subroutine RESIZE_MXDIMS_SMATRI

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXELMT

      subroutine RESIZE_MXELMT_SMATRI(C_ELMT)
      IMPLICIT NONE
      integer :: C_ELMT

      integer :: ierr

! H
      allocate(rpd1(C_ELMT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXELMT_SMATRI_H')
      rpd1 = 0.d0
      rpd1(1:MXELMT) = H(:)
      deallocate(H)
      H => rpd1

! KO
      allocate(ipd1(C_ELMT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXELMT_SMATRI_KO')
      ipd1 = 0
      ipd1(1:MXELMT) = KO(:)
      deallocate(KO)
      KO => ipd1
! LI
      allocate(ipd1(C_ELMT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXELMT_SMATRI_LI')
      ipd1 = 0
      ipd1(1:MXELMT) = LI(:)
      deallocate(LI)
      LI => ipd1
!
      return
      end subroutine RESIZE_MXELMT_SMATRI

      end module mod_com_smatri
