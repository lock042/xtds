
      module mod_com_paradj

      use mod_dppr
      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      integer ,save  :: MXFI = 0                                                                   ! max number of normal equation files
!
      real(kind=dppr)            ,pointer ,dimension(:,:) ,save  :: ACBNC,ACBNI,ACBNS,  &          ! (MXOPH,MXOPH)
                                                                    ASTDC,ASTDI,ASTDS,  &
                                                                    CTRL,CTRLM1,        &
                                                                    VCBNS,VSTDI,VSTDS
      real(kind=dppr)            ,pointer ,dimension(:)   ,save  :: DPCBN,SOMP                     ! (MXOPH)
      real(kind=dppr)            ,pointer ,dimension(:)   ,save  :: VC,VCM1                        ! (MXOPH*MXOPH)
      real(kind=dppr)            ,pointer ,dimension(:,:) ,save  :: PARAFX,PREDFX                  ! (MXOPH,MXFI)
      real(kind=dppr)            ,pointer ,dimension(:,:) ,save  :: POIDS                          ! (MXOPH,0:MXFI)
      real(kind=dppr)            ,pointer ,dimension(:)   ,save  :: ETPCBN,ETPSTD                  ! (MXOPH)
      real(kind=dppr)            ,pointer ,dimension(:)   ,save  :: PCBN,PCBNAJ,PRAT               ! (MXOPH)
      real(kind=dppr)            ,pointer ,dimension(:)   ,save  :: PSTD,PSTDAJ,VALAT              ! (MXOPH)
      real(kind=dppr)            ,pointer ,dimension(:)   ,save  :: YCBNC,YCBNS,YSTDC              ! (MXOPH)
      real(kind=dppr)            ,pointer ,dimension(:,:) ,save  :: AIY                            ! (MXOPH,2*MXOPH+1)
      real(kind=dppr)            ,pointer ,dimension(:)   ,save  :: SPE2C,SPOBSC                   ! (0:MXFI)

      integer                    ,pointer ,dimension(:)   ,save  :: IC,ICM1,JC,JCM1                ! (MXOPH*MXOPH)
      integer                    ,pointer ,dimension(:)   ,save  :: ICNTRL,IXCL,LIBCBN,LIBSTD      ! (MXOPH)
      integer                    ,pointer ,dimension(:)   ,save  :: NFITC,JMAXC,NBOPAJ,NBOPFX      ! (MXFI)

      character(len = NBCLAB+10) ,pointer ,dimension(:)   ,save  :: COPAJ,COPAJS                   ! (MXOPH)
      character(len = NBCLAB+10) ,pointer ,dimension(:,:) ,save  :: CHAIFX                         ! (MXOPH,MXFI)
      character(len = NBCTIT)    ,pointer ,dimension(:,:) ,save  :: TITRAJ,TITRFX                  ! (7,MXFI)
      character(len = 120)       ,pointer ,dimension(:)   ,save  :: FPARA,NEQFI                    ! (MXFI)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_PARADJ
      IMPLICIT NONE

      integer  :: ierr

      MXFI = 1
      allocate(ACBNC(MXOPH,MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_ACBNC')
      ACBNC = 0.d0
      allocate(ACBNI(MXOPH,MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_ACBNI')
      ACBNI = 0.d0
      allocate(ACBNS(MXOPH,MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_ACBNS')
      ACBNS = 0.d0
      allocate(ASTDC(MXOPH,MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_ASTDC')
      ASTDC = 0.d0
      allocate(ASTDI(MXOPH,MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_ASTDI')
      ASTDI = 0.d0
      allocate(ASTDS(MXOPH,MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_ASTDS')
      ASTDS = 0.d0
      allocate(CTRL(MXOPH,MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_CTRL')
      CTRL = 0.d0
      allocate(CTRLM1(MXOPH,MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_CTRLM1')
      CTRLM1 = 0.d0
      allocate(VCBNS(MXOPH,MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_VCBNS')
      VCBNS = 0.d0
      allocate(VSTDI(MXOPH,MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_VSTDI')
      VSTDI = 0.d0
      allocate(VSTDS(MXOPH,MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_VSTDS')
      VSTDS = 0.d0
      allocate(DPCBN(MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_DPCBN')
      DPCBN = 0.d0
      allocate(SOMP(MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_SOMP')
      SOMP = 0.d0
      allocate(VC(MXOPH*MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_VC')
      VC = 0.d0
      allocate(VCM1(MXOPH*MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_VCM1')
      VCM1 = 0.d0
      allocate(PARAFX(MXOPH,MXFI),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_PARAFX')
      PARAFX = 0.d0
      allocate(PREDFX(MXOPH,MXFI),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_PREDFX')
      PREDFX = 0.d0
      allocate(POIDS(MXOPH,0:MXFI),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_POIDS')
      POIDS = 0.d0
      allocate(ETPCBN(MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_ETPCBN')
      ETPCBN = 0.d0
      allocate(ETPSTD(MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_ETPSTD')
      ETPSTD = 0.d0
      allocate(PCBN(MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_PCBN')
      PCBN = 0.d0
      allocate(PCBNAJ(MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_PCBNAJ')
      PCBNAJ = 0.d0
      allocate(PRAT(MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_PRAT')
      PRAT = 0.d0
      allocate(PSTD(MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_PSTD')
      PSTD = 0.d0
      allocate(PSTDAJ(MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_PSTDAJ')
      PSTDAJ = 0.d0
      allocate(VALAT(MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_VALAT')
      VALAT = 0.d0
      allocate(YCBNC(MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_YCBNC')
      YCBNC = 0.d0
      allocate(YCBNS(MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_YCBNS')
      YCBNS = 0.d0
      allocate(YSTDC(MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_YSTDC')
      YSTDC = 0.d0
      allocate(AIY(MXOPH,2*MXOPH+1),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_AIY')
      AIY = 0.d0
      allocate(SPE2C(0:MXFI),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_SPE2C')
      SPE2C = 0.d0
      allocate(SPOBSC(0:MXFI),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_SPOBSC')
      SPOBSC = 0.d0

      allocate(IC(MXOPH*MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_IC')
      IC = 0
      allocate(ICM1(MXOPH*MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_ICM1')
      ICM1 = 0
      allocate(JC(MXOPH*MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_JC')
      JC = 0
      allocate(JCM1(MXOPH*MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_JCM1')
      JCM1 = 0
      allocate(ICNTRL(MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_ICNTRL')
      ICNTRL = 0
      allocate(IXCL(MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_IXCL')
      IXCL = 0
      allocate(LIBCBN(MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_LIBCBN')
      LIBCBN = 0
      allocate(LIBSTD(MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_LIBSTD')
      LIBSTD = 0
      allocate(NFITC(MXFI),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_NFITC')
      NFITC = 0
      allocate(JMAXC(MXFI),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_JMAXC')
      JMAXC = 0
      allocate(NBOPAJ(MXFI),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_NBOPAJ')
      NBOPAJ = 0
      allocate(NBOPFX(MXFI),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_NBOPFX')
      NBOPFX = 0

      allocate(COPAJ(MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_COPAJ')
      COPAJ = ''
      allocate(COPAJS(MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_COPAJS')
      COPAJS = ''
      allocate(CHAIFX(MXOPH,MXFI),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_CHAIFX')
      CHAIFX = ''
      allocate(TITRAJ(7,MXFI),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_TITRAJ')
      TITRAJ = ''
      allocate(TITRFX(7,MXFI),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_TITRFX')
      TITRFX = ''
      allocate(FPARA(MXFI),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_FPARA')
      FPARA = ''
      allocate(NEQFI(MXFI),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PARADJ_NEQFI')
      NEQFI = ''
!
      return
      end subroutine ALLOC_PARADJ

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXFI

      subroutine RESIZE_MXFI_PARADJ(C_FI)
      IMPLICIT NONE
      integer :: C_FI

      integer :: i,ierr

      character(len = 120)       ,pointer ,dimension(:)    :: cpd1
      character(len = NBCLAB+10) ,pointer ,dimension(:,:)  :: cpd2
      character(len = NBCTIT)    ,pointer ,dimension(:,:)  :: cpd2t

! PARAFX
      allocate(rpd2(MXOPH,C_FI),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXFI_PARADJ_PARAFX')
      rpd2 = 0.d0
      do i=1,MXFI
        rpd2(:,i) = PARAFX(:,i)
      enddo
      deallocate(PARAFX)
      PARAFX => rpd2
! PREDFX
      allocate(rpd2(MXOPH,C_FI),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXFI_PARADJ_PREDFX')
      rpd2 = 0.d0
      do i=1,MXFI
        rpd2(:,i) = PREDFX(:,i)
      enddo
      deallocate(PREDFX)
      PREDFX => rpd2
! POIDS
      allocate(rpd2(MXOPH,0:C_FI),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXFI_PARADJ_POIDS')
      rpd2 = 0.d0
      do i=0,MXFI
        rpd2(:,i) = POIDS(:,i)
      enddo
      deallocate(POIDS)
      POIDS => rpd2
! SPE2C
      allocate(rpd1(0:C_FI),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXFI_PARADJ_SPE2C')
      rpd1 = 0.d0
      rpd1(0:MXFI) = SPE2C(:)
      deallocate(SPE2C)
      SPE2C => rpd1
! SPOBSC
      allocate(rpd1(0:C_FI),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXFI_PARADJ_SPOBSC')
      rpd1 = 0.d0
      rpd1(0:MXFI) = SPOBSC(:)
      deallocate(SPOBSC)
      SPOBSC => rpd1
! NFITC
      allocate(ipd1(C_FI),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXFI_PARADJ_NFITC')
      ipd1 = 0
      ipd1(1:MXFI) = NFITC(:)
      deallocate(NFITC)
      NFITC => ipd1
! JMAXC
      allocate(ipd1(C_FI),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXFI_PARADJ_JMAXC')
      ipd1 = 0
      ipd1(1:MXFI) = JMAXC(:)
      deallocate(JMAXC)
      JMAXC => ipd1
! NBOPAJ
      allocate(ipd1(C_FI),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXFI_PARADJ_NBOPAJ')
      ipd1 = 0
      ipd1(1:MXFI) = NBOPAJ(:)
      deallocate(NBOPAJ)
      NBOPAJ => ipd1
! NBOPFX
      allocate(ipd1(C_FI),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXFI_PARADJ_NBOPFX')
      ipd1 = 0
      ipd1(1:MXFI) = NBOPFX(:)
      deallocate(NBOPFX)
      NBOPFX => ipd1
! CHAIFX
      allocate(cpd2(MXOPH,C_FI),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXFI_PARADJ_CHAIFX')
      cpd2 = ''
      do i=1,MXFI
        cpd2(:,i) = CHAIFX(:,i)
      enddo
      deallocate(CHAIFX)
      CHAIFX => cpd2
! TITRAJ
      allocate(cpd2t(7,C_FI),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXFI_PARADJ_TITRAJ')
      cpd2t = ''
      do i=1,MXFI
        cpd2t(:,i) = TITRAJ(:,i)
      enddo
      deallocate(TITRAJ)
      TITRAJ => cpd2t
! TITRFX
      allocate(cpd2t(7,C_FI),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXFI_PARADJ_TITRFX')
      cpd2t = ''
      do i=1,MXFI
        cpd2t(:,i) = TITRFX(:,i)
      enddo
      deallocate(TITRFX)
      TITRFX => cpd2t
! FPARA
      allocate(cpd1(C_FI),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXFI_PARADJ_FPARA')
      cpd1 = ''
      cpd1(1:MXFI) = FPARA(:)
      deallocate(FPARA)
      FPARA => cpd1
! NEQFI
      allocate(cpd1(C_FI),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXFI_PARADJ_NEQFI')
      cpd1 = ''
      cpd1(1:MXFI) = NEQFI(:)
      deallocate(NEQFI)
      NEQFI => cpd1
!
      return
      end subroutine RESIZE_MXFI_PARADJ

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOPH

      subroutine RESIZE_MXOPH_PARADJ(C_OPH)
      IMPLICIT NONE
      integer :: C_OPH

      integer :: i,ierr

      character(len = NBCLAB+10) ,pointer ,dimension(:)    :: cpd1
      character(len = NBCLAB+10) ,pointer ,dimension(:,:)  :: cpd2

! ACBNC
      allocate(rpd2(C_OPH,C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_PARADJ_ACBNC')
      rpd2 = 0.d0
      do i=1,MXOPH
        rpd2(1:MXOPH,i) = ACBNC(:,i)
      enddo
      deallocate(ACBNC)
      ACBNC => rpd2
! ACBNI
      allocate(rpd2(C_OPH,C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_PARADJ_ACBNI')
      rpd2 = 0.d0
      do i=1,MXOPH
        rpd2(1:MXOPH,i) = ACBNI(:,i)
      enddo
      deallocate(ACBNI)
      ACBNI => rpd2
! ACBNS
      allocate(rpd2(C_OPH,C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_PARADJ_ACBNS')
      rpd2 = 0.d0
      do i=1,MXOPH
        rpd2(1:MXOPH,i) = ACBNS(:,i)
      enddo
      deallocate(ACBNS)
      ACBNS => rpd2
! ASTDC
      allocate(rpd2(C_OPH,C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_PARADJ_ASTDC')
      rpd2 = 0.d0
      do i=1,MXOPH
        rpd2(1:MXOPH,i) = ASTDC(:,i)
      enddo
      deallocate(ASTDC)
      ASTDC => rpd2
! ASTDI
      allocate(rpd2(C_OPH,C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_PARADJ_ASTDI')
      rpd2 = 0.d0
      do i=1,MXOPH
        rpd2(1:MXOPH,i) = ASTDI(:,i)
      enddo
      deallocate(ASTDI)
      ASTDI => rpd2
! ASTDS
      allocate(rpd2(C_OPH,C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_PARADJ_ASTDS')
      rpd2 = 0.d0
      do i=1,MXOPH
        rpd2(1:MXOPH,i) = ASTDS(:,i)
      enddo
      deallocate(ASTDS)
      ASTDS => rpd2
! CTRL
      allocate(rpd2(C_OPH,C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_PARADJ_CTRL')
      rpd2 = 0.d0
      do i=1,MXOPH
        rpd2(1:MXOPH,i) = CTRL(:,i)
      enddo
      deallocate(CTRL)
      CTRL => rpd2
! CTRLM1
      allocate(rpd2(C_OPH,C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_PARADJ_CTRLM1')
      rpd2 = 0.d0
      do i=1,MXOPH
        rpd2(1:MXOPH,i) = CTRLM1(:,i)
      enddo
      deallocate(CTRLM1)
      CTRLM1 => rpd2
! VCBNS
      allocate(rpd2(C_OPH,C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_PARADJ_VCBNS')
      rpd2 = 0.d0
      do i=1,MXOPH
        rpd2(1:MXOPH,i) = VCBNS(:,i)
      enddo
      deallocate(VCBNS)
      VCBNS => rpd2
! VSTDI
      allocate(rpd2(C_OPH,C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_PARADJ_VSTDI')
      rpd2 = 0.d0
      do i=1,MXOPH
        rpd2(1:MXOPH,i) = VSTDI(:,i)
      enddo
      deallocate(VSTDI)
      VSTDI => rpd2
! VSTDS
      allocate(rpd2(C_OPH,C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_PARADJ_VSTDS')
      rpd2 = 0.d0
      do i=1,MXOPH
        rpd2(1:MXOPH,i) = VSTDS(:,i)
      enddo
      deallocate(VSTDS)
      VSTDS => rpd2
! DPCBN
      allocate(rpd1(C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_PARADJ_DPCBN')
      rpd1 = 0.d0
      rpd1(1:MXOPH) = DPCBN(:)
      deallocate(DPCBN)
      DPCBN => rpd1
! SOMP
      allocate(rpd1(C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_PARADJ_SOMP')
      rpd1 = 0.d0
      rpd1(1:MXOPH) = SOMP(:)
      deallocate(SOMP)
      SOMP => rpd1
! VC
      allocate(rpd1(C_OPH*C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_PARADJ_VC')
      rpd1 = 0.d0
      rpd1(1:MXOPH*MXOPH) = VC(:)
      deallocate(VC)
      VC => rpd1
! VCM1
      allocate(rpd1(C_OPH*C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_PARADJ_VCM1')
      rpd1 = 0.d0
      rpd1(1:MXOPH*MXOPH) = VCM1(:)
      deallocate(VCM1)
      VCM1 => rpd1
! PARAFX
      allocate(rpd2(C_OPH,MXFI),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_PARADJ_PARAFX')
      rpd2 = 0.d0
      do i=1,MXFI
        rpd2(1:MXOPH,i) = PARAFX(:,i)
      enddo
      deallocate(PARAFX)
      PARAFX => rpd2
! PREDFX
      allocate(rpd2(C_OPH,MXFI),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_PARADJ_PREDFX')
      rpd2 = 0.d0
      do i=1,MXFI
        rpd2(1:MXOPH,i) = PREDFX(:,i)
      enddo
      deallocate(PREDFX)
      PREDFX => rpd2
! POIDS
      allocate(rpd2(C_OPH,0:MXFI),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_PARADJ_POIDS')
      rpd2 = 0.d0
      do i=0,MXFI
        rpd2(1:MXOPH,i) = POIDS(:,i)
      enddo
      deallocate(POIDS)
      POIDS => rpd2
! ETPCBN
      allocate(rpd1(C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_PARADJ_ETPCBN')
      rpd1 = 0.d0
      rpd1(1:MXOPH) = ETPCBN(:)
      deallocate(ETPCBN)
      ETPCBN => rpd1
! ETPSTD
      allocate(rpd1(C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_PARADJ_ETPSTD')
      rpd1 = 0.d0
      rpd1(1:MXOPH) = ETPSTD(:)
      deallocate(ETPSTD)
      ETPSTD => rpd1
! PCBN
      allocate(rpd1(C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_PARADJ_PCBN')
      rpd1 = 0.d0
      rpd1(1:MXOPH) = PCBN(:)
      deallocate(PCBN)
      PCBN => rpd1
! PCBNAJ
      allocate(rpd1(C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_PARADJ_PCBNAJ')
      rpd1 = 0.d0
      rpd1(1:MXOPH) = PCBNAJ(:)
      deallocate(PCBNAJ)
      PCBNAJ => rpd1
! PRAT
      allocate(rpd1(C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_PARADJ_PRAT')
      rpd1 = 0.d0
      rpd1(1:MXOPH) = PRAT(:)
      deallocate(PRAT)
      PRAT => rpd1
! PSTD
      allocate(rpd1(C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_PARADJ_PSTD')
      rpd1 = 0.d0
      rpd1(1:MXOPH) = PSTD(:)
      deallocate(PSTD)
      PSTD => rpd1
! PSTDAJ
      allocate(rpd1(C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_PARADJ_PSTDAJ')
      rpd1 = 0.d0
      rpd1(1:MXOPH) = PSTDAJ(:)
      deallocate(PSTDAJ)
      PSTDAJ => rpd1
! VALAT
      allocate(rpd1(C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_PARADJ_VALAT')
      rpd1 = 0.d0
      rpd1(1:MXOPH) = VALAT(:)
      deallocate(VALAT)
      VALAT => rpd1
! YCBNC
      allocate(rpd1(C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_PARADJ_YCBNC')
      rpd1 = 0.d0
      rpd1(1:MXOPH) = YCBNC(:)
      deallocate(YCBNC)
      YCBNC => rpd1
! YCBNS
      allocate(rpd1(C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_PARADJ_YCBNS')
      rpd1 = 0.d0
      rpd1(1:MXOPH) = YCBNS(:)
      deallocate(YCBNS)
      YCBNS => rpd1
! YSTDC
      allocate(rpd1(C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_PARADJ_YSTDC')
      rpd1 = 0.d0
      rpd1(1:MXOPH) = YSTDC(:)
      deallocate(YSTDC)
      YSTDC => rpd1
! AIY
      allocate(rpd2(C_OPH,2*C_OPH+1),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_PARADJ_AIY')
      rpd2 = 0.d0
      do i=1,2*MXOPH+1
        rpd2(1:MXOPH,i) = AIY(:,i)
      enddo
      deallocate(AIY)
      AIY => rpd2
! IC
      allocate(ipd1(C_OPH*C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_PARADJ_IC')
      ipd1 = 0
      ipd1(1:MXOPH*MXOPH) = IC(:)
      deallocate(IC)
      IC => ipd1
! ICM1
      allocate(ipd1(C_OPH*C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_PARADJ_ICM1')
      ipd1 = 0
      ipd1(1:MXOPH*MXOPH) = ICM1(:)
      deallocate(ICM1)
      ICM1 => ipd1
! JC
      allocate(ipd1(C_OPH*C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_PARADJ_JC')
      ipd1 = 0
      ipd1(1:MXOPH*MXOPH) = JC(:)
      deallocate(JC)
      JC => ipd1
! JCM1
      allocate(ipd1(C_OPH*C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_PARADJ_JCM1')
      ipd1 = 0
      ipd1(1:MXOPH*MXOPH) = JCM1(:)
      deallocate(JCM1)
      JCM1 => ipd1
! ICNTRL
      allocate(ipd1(C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_PARADJ_ICNTRL')
      ipd1 = 0
      ipd1(1:MXOPH) = ICNTRL(:)
      deallocate(ICNTRL)
      ICNTRL => ipd1
! IXCL
      allocate(ipd1(C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_PARADJ_IXCL')
      ipd1 = 0
      ipd1(1:MXOPH) = IXCL(:)
      deallocate(IXCL)
      IXCL => ipd1
! LIBCBN
      allocate(ipd1(C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_PARADJ_LIBCBN')
      ipd1 = 0
      ipd1(1:MXOPH) = LIBCBN(:)
      deallocate(LIBCBN)
      LIBCBN => ipd1
! LIBSTD
      allocate(ipd1(C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_PARADJ_LIBSTD')
      ipd1 = 0
      ipd1(1:MXOPH) = LIBSTD(:)
      deallocate(LIBSTD)
      LIBSTD => ipd1
! COPAJ
      allocate(cpd1(C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_PARADJ_COPAJ')
      cpd1 = ''
      cpd1(1:MXOPH) = COPAJ(:)
      deallocate(COPAJ)
      COPAJ => cpd1
! COPAJS
      allocate(cpd1(C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_PARADJ_COPAJS')
      cpd1 = ''
      cpd1(1:MXOPH) = COPAJS(:)
      deallocate(COPAJS)
      COPAJS => cpd1
! CHAIFX
      allocate(cpd2(C_OPH,MXFI),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_PARADJ_CHAIFX')
      cpd2 = ''
      do i=1,MXFI
        cpd2(1:MXOPH,i) = CHAIFX(:,i)
      enddo
      deallocate(CHAIFX)
      CHAIFX => cpd2
!
      return
      end subroutine RESIZE_MXOPH_PARADJ

      end module mod_com_paradj
