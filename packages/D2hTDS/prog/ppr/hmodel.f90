      PROGRAM HMODEL
!
!  6.7.88 FORTRAN 77 DU SUN4
!  REV 25 JAN 1990
!  REV    DEC 1992 T.GABARD
!  REV 29 SEP 1994
!  REV    JAN 1995 JPC,CW (PARAMETER)
!
!  MODIFICATION MARS 2002 W. RABALLAND ---> C2H4 / D2h.
!
!      CODAGE DES OPERATEURS ROVIBRATIONNELS DE L'HAMILTONIEN EFFECTIF
!     POUR UNE RESTRICTION DONNEE D'UNE POLYADE VIBRATIONNELLE TYPE CH4
!
!     CALCUL DES 6-C
!
!     CALCUL DES K JMKCUB = FIXE DANS LE PROGRAMME
!
! APPEL : hmodel Pn Nm  Dk  v1  v2  v3  v4  v5  v6   ...   w1  w2  w3  w4  w5  w6            |
!                   Nm' Dk' v1' v2' v3' v4' v5' v6'  ...   w1' w2' w3' w4' w5' w6'           | (n+1 lines)
!                   ...........................                                              |
!                Ir|IIr|IIIr
!
!  *****  LIMITATIONS DU PROGRAMME GEREES PAR PARAMETER.
!
!     MXOPH  = NB MAXIMUM D'OPERATEURS ROVIBRATIONNELS
!               (ICODRO,ICODVC,ICODVA,ICODSY)
!     MXOCV  = NB MAXIMUM D'OPERATEURS ROVIBRATIONNELS PAR CASE
!              VIBRATIONNELLE
!               (IRDRE,IDRO,IDVC,IDVA,IDSY)
!     MXEMR  = NB MAXIMUM D'E.M.R.V. NON NULS
!               (EMOP,LI,IC)
!     MXSNV  = NB MAXIMUM TOTAL DE SOUS-NIVEAUX VIBRATIONNELS
!               (IFF,IKVC,IKVA)
!     MXOPR  = NB MAXIMUM D'OPERATEURS ROVIBRATIONNELS PAR OPERATEUR
!              VIBRATIONNEL
!               (IROCO)
!     MXNIV  = NB MAXIMUM DE NIVEAUX VIBRATIONNELS DANS UNE POLYADE DONNEE
!               (NVIB)
!     MXPOL  = NB MAXIMUM DE POLYADES
!               (NIVI,NVIB)
!
      use mod_dppr
      use mod_par_tds
      use mod_com_fdate
      use mod_com_phase
      use mod_com_sy
      use mod_main_hmodel
      IMPLICIT NONE

! functions
      real(kind=dppr)  :: ELMR
      integer          :: MULGU,NSYM1

      real(kind=dppr)  :: EM,EM1,EM2,EN
      real(kind=dppr)  :: SP

      integer ,dimension(NBVQN)    :: ICF,ICP,ILA,IL,ILC
      integer ,dimension(NBVQN)    :: INA,INC,INUM,IV,IVA,IVC
      integer ,dimension(NBVQN+1)  :: ICA,ICC,IPA,IPC
      integer ,dimension(MXSYM)    :: IROT
      integer ,dimension(MXPOL)    :: IORH,NIVI
      integer          :: NFF_SUP,IOP_SUP,NROT_SUP,NBOPH_SUP
      integer          :: IROTA_SUP,NIV_SUP
      integer          :: I,IB
      integer          :: ID,IDEGV,IDK,IDUM
      integer          :: IDROT,IF,IF1,IF2,IG,IGAMA,II,IIIR,IJK,IK,IM,IMIA
      integer          :: IOA,IOA1,IOC,IOC1,IOP,IOPA,IOPC,IPGAMA,IPOL,IRO
      integer          :: IRO1,IRO2,IRO3,IRO4,IROTA,IU,IVI
      integer          :: J,JD,JJJ,JMKCUB
      integer          :: KNVIB,KNIVP,KPOL,KOP
      integer          :: L,LOP
      integer          :: NBOPH,NBVA,NBVC,NDUM,NFF
      integer          :: NPOL,NROT,NS

      character(len =   1)  :: AVI,AORH
      character(len =  11)  :: CHAINE
      character(len = 120)  :: IFICH
!
1000  FORMAT('   EFFECTIVE ROVIBRATIONAL HAMILTONIAN :'  , /,   &
             ' VIBRATIONAL REDUCED MATRIX ELEMENTS (RME)',//,   &
             A                                               )
1001  FORMAT(//)
1002  FORMAT(I6,'  |[',             &
             I2,A,A,' *',           &
             I2,A,A,' *',           &
             I2,A,A,' *',           &
             I2,A,A,' *',           &
             I2,A,A,' *',           &
             I2,A,A,' *',           &
             I2,A,A,' *',           &
             I2,A,A,' *',           &
             I2,A,A,' *',           &
             I2,A,A,' *',           &
             I2,A,A,' *',           &
             I2,A,A,' ]',A,A,' >')
1004  FORMAT(I4,' TH VIBRATIONAL OPERATOR',//,   &
             A,1X,' { ',                         &
             I2,A,A,' *',                        &
             I2,A,A,' *',                        &
             I2,A,A,' *',                        &
             I2,A,A,' *',                        &
             I2,A,A,' *',                        &
             I2,A,A,' *',                        &
             I2,A,A,' *',                        &
             I2,A,A,' *',                        &
             I2,A,A,' *',                        &
             I2,A,A,' *',                        &
             I2,A,A,' *',                        &
             I2,A,A,                             &
             ' } '                        , /,   &
             2X,' { ',                           &
             I2,A,A,' *',                        &
             I2,A,A,' *',                        &
             I2,A,A,' *',                        &
             I2,A,A,' *',                        &
             I2,A,A,' *',                        &
             I2,A,A,' *',                        &
             I2,A,A,' *',                        &
             I2,A,A,' *',                        &
             I2,A,A,' *',                        &
             I2,A,A,' *',                        &
             I2,A,A,' *',                        &
             I2,A,A,                             &
             ' } ',A,A                        )
1005  FORMAT(/,              &
             'HMODEL : ',A)
1025  FORMAT(I4,' ROVIBRATIONAL OPERATORS',/)
1026  FORMAT(/,            &
             1X,100('-'))
1027  FORMAT(/,                                          &
             ' HAMILTONIAN DEVELOPMENT ORDER :  ',10I2)
1028  FORMAT(A)
1029  FORMAT(/,                     &
             I5,' NON ZERO RME',/)
1030  FORMAT(2X,A1,'<',I5,'|',3X,'|',I5,'>',3X,F19.13)
1031  FORMAT(/,                               &
             I4,' ROTATIONAL OPERATOR(S)',/)
1032  FORMAT(I1)
1033  FORMAT(/,                         &
             ' POLYAD NUMBER : ',I2,/)
1034  FORMAT(5X,'(v1;v2;v3;v4;v5;v6;v7;v8;v9;v10;v11;v12) = (',11(I2,';'),I2,')')                  ! ATTENTION respecter cette regle dans tous les packages pour VAMDC
1035  FORMAT(I2)
1100  FORMAT(/,                                  &
             I4,' VIBRATIONAL SUBLEVEL(S) :',/)
1313  FORMAT(I4,I3,'(',I1,',',I1,A,A,')',1X,12I1,A,A,1X,12I1,A,A,1X,A,A,I8,I7)                     ! 12 = NBVQN
2001  FORMAT(' >> ',A)
8000  FORMAT(' !!! HMODEL : STOP ON ERROR')
8001  FORMAT(' !!! ERROR OPENING CONTROL FILE')
8002  FORMAT(' !!! INCOMPLETE CONTROL FILE')
8004  FORMAT(' !!! INVALID IIIR VALUE')
!
! ARRAY INITIAL ALLOCATION
!
      CALL ALLOC_ALL
!
      NFF_SUP   = -1
      IOP_SUP   = -1
      NROT_SUP  = -1
      NBOPH_SUP = -1
      IROTA_SUP = -1
      NIV_SUP   = -1
      OPEN(10,FILE=CTRLF,ERR=9998,STATUS='OLD')
      READ(10,1028,END=9997) FDATE
      PRINT 1005,            FDATE
!
! NUMERO DE LA POLYADE COURANTE
!
      READ(10,1028,END=9997) CHAINE
      READ (CHAINE(2:2),1032) IVI
      WRITE(AVI,1032)         IVI
!
! NOMBRE TOTAL DE POLYADES
!
      NPOL = IVI+1
!
! NOM DU FICHIER DE SORTIE
!
      IFICH = 'MH_P'//AVI//'_D'
!
! LECTURE DES NIVEAUX VIBRATIONNELS
!
      JMKCUB = 0
      DO KPOL=1,NPOL
        READ(10,1028,END=9997) CHAINE
        READ(CHAINE(2:3),1035) NIVI(KPOL)
        IF( NIVI(KPOL) .GT. NIV_SUP ) NIV_SUP = NIVI(KPOL)
        DO WHILE( NIVI(KPOL) .GT. MXNIV )
          CALL RESIZE_MXNIV
        ENDDO
        READ(10,1028,END=9997) CHAINE
        READ (CHAINE(2:2),1032) IORH(KPOL)
        WRITE(AORH,1032)        IORH(KPOL)
        IFICH = TRIM(IFICH)//AORH
        DO KNIVP=1,NIVI(KPOL)
          DO KNVIB=1,NBVQN
            READ(10,1032,END=9997) NVIB(KPOL,KNIVP,KNVIB)
            IF( KNVIB .GT. 2 ) JMKCUB = MAX(JMKCUB,NVIB(KPOL,KNIVP,KNVIB))
          ENDDO
        ENDDO
      ENDDO
      READ(10,1028,END=9997) CHAINE
      IF    ( CHAINE(1:4) .EQ. 'Ir  ' ) THEN
        IIIR = 1
      ELSEIF( CHAINE(1:4) .EQ. 'IIr ' ) THEN
        IIIR = 2
      ELSEIF( CHAINE(1:4) .EQ. 'IIIr' ) THEN
        IIIR = 3
      ELSE
        GOTO 9995
      ENDIF
      CLOSE(10)
      PRINT 2001, TRIM(IFICH)
!
! INITIALISATIONS
!
      CALL FACTO
      CALL CAL6C
      OPEN(20,FILE=IFICH,STATUS='UNKNOWN')
      WRITE(20,1000) FDATE
      WRITE(20,1033) IVI
      DO I=1,NIVI(NPOL)
        WRITE(20,1034) (NVIB(NPOL,I,J),J=1,NBVQN)
      ENDDO
!
! CODAGE DES SOUS-NIVEAUX VIBRATIONNELS
!
      NFF = NIVI(NPOL)
      DO WHILE( NFF .GT. MXSNV )
        CALL RESIZE_MXSNV
      ENDDO
      NFF_SUP = NFF
      DO IF1=1,NFF
        DO IF2=1,NBVQN
          IV(IF2) = NVIB(NPOL,IF1,IF2)
        ENDDO
        CALL SIGVI(IV,IFF,MXSNV,IF1,IIIR)
      ENDDO
      WRITE(20,1100) NFF
      DO IF=1,NFF
        DO I=1,NBVQN
          CALL VLNC(IFF(I,IF),IV(I),ICF(I),ICP(I))
          IL(I)   = 0
          INUM(I) = 0
        ENDDO
        IFF(NBVQN+1,IF) = 1
        IFF(NBVQN+2,IF) = 1
        DO I=1,NBVQN
          CALL MULD2H(IFF(NBVQN+1,IF),ICF(I),NDUM,IDUM)
          IFF(NBVQN+1,IF) = IDUM
          IFF(NBVQN+2,IF) = MULGU(IFF(NBVQN+2,IF),ICP(I))
        ENDDO
        WRITE(20,1002) IF,                                         &
                       (IV(I),SYM(ICF(I)),PAR(ICP(I)),I=1,NBVQN),  &
                       SYM(IFF(NBVQN+1,IF)),PAR(IFF(NBVQN+2,IF))
      ENDDO
      WRITE(20,1027) (IORH(IPOL),IPOL=1,NPOL)
      LOP   = 0
      NBOPH = 0
!
! BOUCLE SUR LES POLYADES
!
E36:  DO KPOL=1,NPOL
!
! DETERMINATION DES CREATEURS
!
E35:    DO IOC=1,NIVI(KPOL)
          DO IOC1=1,NBVQN
            IVC(IOC1) = NVIB(KPOL,IOC,IOC1)
          ENDDO
          NBVC = 1
          CALL SIGVI(IVC,IKVC,MXSNV,NBVC,IIIR)
!
! DETERMINATION DES ANNIHILATEURS
!
E33:      DO IOA=1,NIVI(KPOL)
            DO IOA1=1,NBVQN
              IVA(IOA1) = NVIB(KPOL,IOA,IOA1)
            ENDDO
            IF( IOA .LT. IOC ) CYCLE E33
            NBVA = 1
            CALL SIGVI(IVA,IKVA,MXSNV,NBVA,IIIR)
            IDEGV = 0
            DO IOP=1,NBVQN
              IDEGV = IVA(IOP)+IVC(IOP)+IDEGV
            ENDDO
            IDROT = IORH(KPOL)+2-IDEGV
            IF( IDROT .LT. 0 ) CYCLE E33
!
! COUPLAGE CREATEURS-ANNIHILATEURS
!
            IOPC = 1
            IMIA = 1
            IF( IOA .EQ. IOC ) IMIA = IOPC
            DO I=1,NBVQN
              CALL VLNC(IKVC(I,IOPC),IVC(I),ICC(I),IPC(I))
              ILC(I) = 0
              INC(I) = 0
            ENDDO
            IOPA = IMIA
            DO I=1,NBVQN
              CALL VLNC(IKVA(I,IOPA),IVA(I),ICA(I),IPA(I))
              ILA(I) = 0
              INA(I) = 0
            ENDDO
            ICA(NBVQN+1) = 1
            IPA(NBVQN+1) = 1
            DO I=1,NBVQN
              CALL MULD2H(ICA(NBVQN+1),ICA(I),NDUM,IDUM)
              ICA(NBVQN+1) = IDUM
              IPA(NBVQN+1) = MULGU(IPA(NBVQN+1),IPA(I))
            ENDDO
            IKVA(NBVQN+1,IOPA) = ICA(NBVQN+1)
            IKVA(NBVQN+2,IOPA) = IPA(NBVQN+1)
            ICC(NBVQN+1)       = 1
            IPC(NBVQN+1)       = 1
            DO I=1,NBVQN
              CALL MULD2H(ICC(NBVQN+1),ICC(I),NDUM,IDUM)
              ICC(NBVQN+1) = IDUM
              IPC(NBVQN+1) = MULGU(IPC(NBVQN+1),IPC(I))
            ENDDO
            IKVC(NBVQN+1,IOPC) = ICC(NBVQN+1)
            IKVC(NBVQN+2,IOPC) = IPC(NBVQN+1)
            CALL MULD2H(ICA(NBVQN+1),ICC(NBVQN+1),NDUM,IGAMA)
            IPGAMA = MULGU(IPA(NBVQN+1),IPC(NBVQN+1))
            IF( IPGAMA .EQ. 2 ) CYCLE E33
            NROT = 0
E733:       DO IU=1,2
              DO IJK=1,MXSYM
                IROT(IJK) = 0
              ENDDO
!
! DETERMINATION DES OPERATEURS ROTATIONNELS POSSIBLES
!
              IROTA = 0
              IF( IDROT .LT. IU-1 ) CYCLE E733
              DO ID=IU-1,IDROT,2
                CALL NBJC(ID,IRO1,IRO2,IRO3,IRO4)
                IROT(1) = IROT(1)+IRO1
                IROT(2) = IROT(2)+IRO2
                IROT(3) = IROT(3)+IRO3
                IROT(4) = IROT(4)+IRO4
E365:           DO IDK=ID,IU-1,-2
                  IF( NSYM1(IDK,IGAMA) .EQ. 0 ) CYCLE E365
                  IF( IDEGV .EQ. 0 .AND.             &
                      ID    .EQ. 0       ) CYCLE E365
                  DO NS=1,NSYM1(IDK,IGAMA)
                    IROTA = IROTA+1
                    IF( IROTA .GT. IROTA_SUP ) IROTA_SUP = IROTA
                    DO WHILE( IROTA .GT. MXOPR )
                      CALL RESIZE_MXOPR
                    ENDDO
                    IROCO(IROTA) = 1000*ID+100*IDK+10*IGAMA+NS-1
                  ENDDO
                ENDDO E365
              ENDDO
!
! CALCUL DU FACTEUR DE NORMALISATION
!      IU = 1, EPSILON = +
!      IU = 2, EPSILON = -
!
              IF( IROT(IGAMA) .EQ. 0 ) CYCLE E733
              SP = (-1)**(IU+1)
              EN = ELMR(IKVC(1,IOPC),IKVC(1,IOPC),IKVA(1,IOPA),IKVA(1,IOPA),IGAMA,IIIR)
              EN = ELMR(IKVC(1,IOPC),IKVA(1,IOPA),IKVC(1,IOPC),IKVA(1,IOPA),IGAMA,IIIR)*SP+EN
              IF( ABS(EN) .LT. 1.D-05 ) CYCLE E733
              LOP = LOP+1
              WRITE(20,1026)
              WRITE(20,1004)  LOP,AUG(IU),                                &
                             (IVC(J),SYM(ICC(J)),PAR(IPC(J)),J=1,NBVQN),  &
                             (IVA(J),SYM(ICA(J)),PAR(IPA(J)),J=1,NBVQN),  &
                             SYM(IGAMA),PAR(IPGAMA)
!
! CALCUL ET CODAGE DES E.M.R.V.
!
              IOP = 0
              DO IB=1,NFF
E5:             DO IK=IB,NFF
                  EM1 = ELMR(IFF(1,IB),IKVC(1,IOPC),IKVA(1,IOPA),IFF(1,IK),IGAMA,IIIR)
                  EM2 = ELMR(IFF(1,IB),IKVA(1,IOPA),IKVC(1,IOPC),IFF(1,IK),IGAMA,IIIR)*SP
                  EM  = EM1+EM2
                  IF( ABS(EM) .LT. 1.D-10 ) CYCLE E5
                  EM = EM/EN
                  IF( IU .EQ. 2 ) EM = -EM
! changement de convention pour les operateurs V dont les elements
! matriciels sont imaginaires (pour retablir, dans la majorite des cas,
! les anciens signes des parametres)     4 / 5 / 93
                  IOP = IOP+1
                  IF( IOP .GT. IOP_SUP ) IOP_SUP = IOP
                  IF( IOP .GT. MXEMR   ) CALL RESIZE_MXEMR
                  LI(IOP)   = IK
                  IC(IOP)   = IB
                  EMOP(IOP) = EM
                ENDDO E5
              ENDDO
              WRITE(20,1029) IOP
              DO KOP=1,IOP
                WRITE(20,1030) AIM(IU),IC(KOP),LI(KOP),EMOP(KOP)
              ENDDO
              WRITE(20,1031) IROTA
!
! CODAGE DES OPERATEURS ROVIBRATIONNELS
!
              DO IRO=1,IROTA
                NROT = NROT+1
                IF( NROT .GT. NROT_SUP ) NROT_SUP = NROT
                IF( NROT .GT. MXOCV    ) CALL RESIZE_MXOCV
                IDRO(NROT) = IROCO(IRO)
                DO II=0,3
                  IDVC(NROT,II+1) = IVC(1+3*II)*100+IVC(2+3*II)*10+IVC(3+3*II)
                  IDVA(NROT,II+1) = IVA(1+3*II)*100+IVA(2+3*II)*10+IVA(3+3*II)
                ENDDO
                IDSY1(NROT) = ICC(NBVQN+1)*100+ICA(NBVQN+1)*10+IGAMA
                IDSY2(NROT) = IPC(NBVQN+1)*100+IPA(NBVQN+1)*10+IPGAMA
                IDSY1(NROT) = IDSY1(NROT)+LOP*1000
              ENDDO
            ENDDO E733
!
! MISE EN ORDRE A L'INTERIEUR D'UNE CASE VIBRATIONNELLE DONNEE
!
            IF( NROT .EQ. 0 ) CYCLE E33
            CALL IRDER(NROT,IDRO,IRDRE,MXOCV)
            DO JJJ=1,NROT
              NBOPH = NBOPH+1
              IF( NBOPH .GT. NBOPH_SUP ) NBOPH_SUP = NBOPH
              IF( NBOPH .GT. MXOPH     ) CALL RESIZE_MXOPH
              IRO           = IRDRE(JJJ)
              ICODRO(NBOPH) = IDRO(IRO)
              DO I=1,4
                ICODVC(NBOPH,I) = IDVC(IRO,I)
              ENDDO
              DO I=1,4
                ICODVA(NBOPH,I) = IDVA(IRO,I)
              ENDDO
              ICOSY1(NBOPH) = IDSY1(IRO)
              ICOSY2(NBOPH) = IDSY2(IRO)
            ENDDO
          ENDDO E33
        ENDDO E35
      ENDDO E36
      CALL DEBUG( 'HMODEL => MXOPVH=',LOP)
!
! STOCKAGE DES CODES DES OPERATEURS ROVIBRATIONNELS
!
      WRITE(20,1001)
      WRITE(20,1025) NBOPH
      DO IRO=1,NBOPH
        IM = ICODRO(IRO)/1000
        IK = (ICODRO(IRO)-1000*IM)/100
        IG = (ICODRO(IRO)-1000*IM-100*IK)/10
        JD = (ICODRO(IRO)-1000*IM-100*IK-10*IG)
        CALL VLNC(MOD(ICOSY1(IRO),1000),ICC(NBVQN+1),ICA(NBVQN+1),IGAMA)
        CALL VLNC(ICOSY2(IRO),IPC(NBVQN+1),IPA(NBVQN+1),IPGAMA)
        DO I=0,3
          CALL VLNC(ICODVC(IRO,I+1),IVC(1+3*I),IVC(2+3*I),IVC(3+3*I))
        ENDDO
        DO I=0,3
          CALL VLNC(ICODVA(IRO,I+1),IVA(1+3*I),IVA(2+3*I),IVA(3+3*I))
        ENDDO
        WRITE(20,1313) IRO,IM,IK,JD,SYM(IGAMA),PAR(IPGAMA),                     &
                       (IVC(L),L=1,NBVQN),SYM(ICC(NBVQN+1)),PAR(IPC(NBVQN+1)),  &
                       (IVA(L),L=1,NBVQN),SYM(ICA(NBVQN+1)),PAR(IPA(NBVQN+1)),  &
                       SYM(IGAMA),PAR(IPGAMA),ICODRO(IRO),ICOSY1(IRO)
      ENDDO
      CLOSE(20)
      CALL DEBUG( 'HMODEL => MXSNV=',NFF_SUP)
      CALL DEBUG( 'HMODEL => MXEMR=',IOP_SUP)
      CALL DEBUG( 'HMODEL => MXOCV=',NROT_SUP)
      CALL DEBUG( 'HMODEL => MXOPH=',NBOPH_SUP)
      CALL DEBUG( 'HMODEL => MXOPR=',IROTA_SUP)
      CALL DEBUG( 'HMODEL => MXNIV=',NIV_SUP)
      GOTO 9000
!
9995  PRINT 8004
      GOTO  9999
9997  PRINT 8002
      CLOSE(10)
      GOTO  9999
9998  PRINT 8001
9999  PRINT 8000
!
9000  PRINT *
      END PROGRAM HMODEL
