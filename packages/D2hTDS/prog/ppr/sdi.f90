      PROGRAM SDI
!
!     2007 FORTRAN 77
!     04/2010 M.SANZHAROV ---> C2H4/D2h
!
! ***  CALCUL DES VALEURS PROPRES D'UN HAMILTONIEN STARK EFFECTIF ***
!
! APPEL : sdi    Pn  JPPOL MST  ELEC
!
!  Pn    : polyad
!  JPPOL : Polarizability Jmax
!  MST   : STARK          Mmax
!  ELEC  : Electric Field
!
      use mod_dppr
      use mod_par_tds
      use mod_com_fdate
      use mod_main_sdi
      IMPLICIT NONE

! functions
      integer          :: STABI

      real(kind=dppr)  :: E,ENERG
      real(kind=dppr)  :: ST,STARK

      integer          :: I,IA,IALB,IALK,IBX,IC,ICLK,ICNIV,ICOL,ICS,II,IL,ILIG,IM,IML,IMX
      integer          :: IP,IPK,IPNIV,IPS,ISTAB,ITEST
      integer          :: JB,JK,JLK,JM,JNIV,JPPOL
      integer          :: MST,MUNIV1,MUNIV2
      integer          :: NELMA,NFB

      character(len =  11) ,dimension(4)  :: CARG
      character(len =  15)  :: FOUXY
      character(len = 120)  :: FH0,FHST,FVPST
!
1000  FORMAT(A)
1020  FORMAT(/,            &
             'SDI    : ',A)
2000  FORMAT(' <  ',A)
2001  FORMAT(' >> ',A)
2006  FORMAT(I3,' ',I3,' ',I3,' ',I3)
2007  FORMAT(I3,' ',F18.12)
8000  FORMAT(' !!! SDI    : STOP ON ERROR')
8001  FORMAT(' !!! MXJST EXCEEDED : ',I8,' > ',I8)
8002  FORMAT(' !!! MXMST EXCEEDED : ',I8,' > ',I8)
8003  FORMAT(' !!! MST <= JPPOL REQUESTED : ',I8,' > ',I8)
!
! ARRAY INITIAL ALLOCATION
!
      CALL ALLOC_ALL
!
      OPEN(10,FILE=CTRLF,STATUS='OLD')
      READ(10,1000) FDATE
      PRINT 1020,   FDATE
      DO I=1,1
        READ(10,1000) CARG(I)
      ENDDO
      FH0   = 'ME_'//TRIM(CARG(1))//'_'
      FHST  = 'P'//TRIM(CARG(1))//'_st'
      FVPST = 'VPS_'//TRIM(CARG(1))
      FOUXY = 'ES_'//TRIM(CARG(1))
      PRINT 2000, TRIM(FH0)
      PRINT 2000, TRIM(FHST)
      PRINT 2001, TRIM(FVPST)
      PRINT 2001, TRIM(FOUXY)
      READ(10,*) JPPOL
      IF( JPPOL .GT. MXJST ) THEN
        PRINT 8001, JPPOL,MXJST
        GOTO  9999
      ENDIF
      READ(10,*) MST
      IF( MST .GT. JPPOL ) THEN
        PRINT 8003, MST,JPPOL
        GOTO  9999
      ENDIF
      IF( MST .GT. MXMST ) THEN
        PRINT 8002, MST,MXMST
        GOTO  9999
      ENDIF
      READ(10,*) ELEC
      CLOSE(10)
      ITEST = 0
      IF( MST .EQ. JPPOL ) ITEST = 1
      DO JLK=0,JPPOL
        DO ICLK=1,MXSYM
          DO IPK=1,2
            INFB(JLK,ICLK,IPK) = 0
          ENDDO
        ENDDO
      ENDDO
!
!     *** LECTURE DE L'HAMILTONIAN A CHAMP NUL ***
!
      OPEN(20,FILE=FH0,FORM='UNFORMATTED',STATUS='OLD')
      OPEN(50,FILE=FVPST,FORM='UNFORMATTED')
      WRITE(50) MST
      WRITE(50) ELEC
!
102   READ(20,END=4) JNIV,ICNIV,IPNIV,NELMA,NFB
      DO WHILE( NFB .GT. MXELMH )
        CALL RESIZE_MXELMH
      ENDDO
      INFB(JNIV,ICNIV,IPNIV) = NFB
      DO MUNIV1=1,NFB
        DO MUNIV2=MUNIV1,NFB
          READ(20) ENERG
          H0(ICNIV,IPNIV,JNIV,MUNIV1,MUNIV2) = ENERG
        ENDDO
      ENDDO
      GOTO 102
!
4     CLOSE(20)
!
!     *** L'ATTRIBUTION DE LA VALEUR DE J AU ***
!         NUMERO DE LA LIGNE DE LA MATRICE H
!
      DO IC=1,MXSYM
        DO IP=1,2
          DO JM=0,JPPOL
            IJ(0)    = JPPOL
            ISUM(-1) = 0
            DO II=0,JPPOL-JM
              ISUM(II) = ISUM(II-1)+INFB(IJ(II),IC,IP)
              IJ(II+1) = IJ(II)-1
            ENDDO
            IA = ISUM(JPPOL-JM)
            DO IML=0,IA
              DO IL=0,JPPOL
                IMX = IA-IML
                IF( IMX .LE. ISUM(IJ(IL))   .AND.         &
                    IMX .GT. ISUM(IJ(IL+1))       ) THEN
                  INDJ(IC,IP,JM,IML+1) = IL
                  INDJ(IC,IP,JM,0)     = -1
                ENDIF
              ENDDO
            ENDDO
          ENDDO
        ENDDO
      ENDDO
!
!     *** LECTURE DES ELEMENTS MATRICIELS STARK ***
!
      OPEN(30,FILE=FHST,FORM='UNFORMATTED',STATUS='OLD')
      IF( ITEST .EQ. 1 ) OPEN(40,FILE=FOUXY)
      E = ELEC*1.D+7
!
!     *** CALCUL DES VALEURS PROPRES DE L'HAMILTONIAN ***
!
E21:  DO ICS=1,MXSYM
E210:   DO IPS=1,2
E22:      DO IM=0,JPPOL
            ILIG = 0
            DO JB=IM,JPPOL
              DO IALB=1,INFB(JB,ICS,IPS)
                ILIG = ILIG+1
                ICOL = 0
                DO JK=IM,JPPOL
                  DO IALK=1,INFB(JK,ICS,IPS)
                    ICOL = ICOL+1
                    READ(30) ST
                    STARK = -0.5D0*ST*E*E
                    IF( JB .EQ. JK ) STARK = STARK+H0(ICS,IPS,JB,IALB,IALK)
                    HST(ILIG,ICOL) = STARK
                  ENDDO
                ENDDO
              ENDDO
            ENDDO
            IF( IM .GT. MST ) CYCLE E22
            CALL DIAGO(ILIG)
            DO I=1,ILIG
              HD(I) = HST(I,I)
            ENDDO
            CALL ORDER(ILIG,HD,K,MXDIST)
            IF( IM .EQ. 0 ) THEN
              ISTAB = STABI(ICS,IPS,MST)
            ELSE
              ISTAB = STABI(ICS,IPS,MST)-STABI(ICS,IPS,IM-1)
            ENDIF
            IF( ISTAB .NE. 0 ) THEN
              IF( ITEST .EQ. 1 ) THEN
                WRITE(40,2006) ICS,IPS,IM,ISTAB
              ENDIF
              WRITE(50) ICS,IPS,IM,ISTAB
            ENDIF
            DO IBX=1,ISTAB
              WHERE( INC .NE. 1 ) INC = 1
              IF( ITEST .EQ. 1 ) THEN
                WRITE(40,2007) IM,HD(K(IBX))
              ENDIF
              WRITE(50) IM,HD(K(IBX))
              DO IP=1,ISTAB
                IF( INDJ(ICS,IPS,IM,IP)   .EQ.         &
                    INDJ(ICS,IPS,IM,IP-1)      ) THEN
                  INC(INDJ(ICS,IPS,IM,IP)) = INC(INDJ(ICS,IPS,IM,IP))+1
                ENDIF
                WRITE(50) INDJ(ICS,IPS,IM,IP),INC(INDJ(ICS,IPS,IM,IP)),T(IP,K(IBX))
              ENDDO
            ENDDO
          ENDDO E22
        ENDDO E210
      ENDDO E21
      IF( ITEST .NE. 1 ) CLOSE(50)
      IF( ITEST .EQ. 1 ) CLOSE(40)
      CLOSE(30,STATUS='DELETE')
      GOTO 9000
!
9999  PRINT 8000
9000  PRINT *
      END PROGRAM SDI
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!*********************************************************************
!     DIAGONALISATION DE MATRICES SYMETRIQUES ET PLEINES : H(I,J)=H(J,I)
!
!     ENTREE => H    ;   SORTIE => H  ET  T  TEL QUE :
!     H=(T-1)*H*T    ;    VEC. PROP.= T(IB,IP)*VEC. DE BASE.
!*********************************************************************
!
!  V. BOUDON 2002 : UTILISATION PROGRAMME DMSPRO DE B. SARTAKOV
!
      SUBROUTINE DIAGO(N)
      use mod_dppr
      use mod_par_tds
      use mod_com_sdi
      IMPLICIT NONE
      integer          :: N

      real(kind=dppr) ,dimension((MXDIST*MXDIST+MXDIST)/2)  :: A
      real(kind=dppr) ,dimension(MXDIST*MXDIST)             :: B
      real(kind=dppr)  :: ACC

      integer          :: I
      integer          :: J
      integer          :: KS
!
      ACC = 1.D-16
      KS  = 1
      DO I=1,N
        DO J=1,I
          A(KS) = HST(J,I)
          KS    = KS+1
        ENDDO
      ENDDO
      CALL DMSPR(A,B,N,0,ACC)
      DO I=1,N
        DO J=1,N
          T(J,I) = B(N*(I-1)+J)
          IF( J .EQ. I )THEN
            HST(I,J) = A((J*J+J)/2)
          ELSE
            HST(I,J) = 0.D0
          ENDIF
        ENDDO
      ENDDO
!
      RETURN
      END SUBROUTINE DIAGO
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
      FUNCTION STABI(IC,IP,IM)
      use mod_com_sdi
      IMPLICIT NONE
      integer          :: STABI
      integer          :: IC,IP,IM

      integer          :: IJS,ISTAB
!
      ISTAB = 0
      DO IJS=0,IM
        ISTAB = ISTAB+INFB(IJS,IC,IP)
      ENDDO
      STABI = ISTAB
!
      RETURN
      END FUNCTION STABI
