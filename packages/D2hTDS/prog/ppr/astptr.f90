      PROGRAM ASTPTR
!
! REV    SEPT 2006  CW
!
!  lire un fichier binaire (ex. INFILE) de type TP_ ou TR_
!  et l'ecrire en ASCII dans INFILE'_ASC'
!
! APPEL : astptr
!
      use mod_dppr
      use mod_par_tds
      IMPLICIT NONE

      real(kind=dppr)  :: TM

      integer          :: ICK,ICL,IK,IKC,IL,ILC,IN,ISV
      integer          :: ICPL,ICPK
      integer          :: JK,JL
      integer          :: NBOPT,NFBK,NFBL,NSV

      character(len = NBCTIT)  :: TITRE
      character(len =  80)  :: NOM
      character(len = 150)  :: INFILE
      character(len = 160)  :: OUTFILE
!
1000  FORMAT(A)
1020  FORMAT(/,            &
             'ASTPTR : ')
2000  FORMAT(' <  ',A)
2001  FORMAT(' >> ',A)
7000  FORMAT('ENTER TP_ or TR_ TYPE FILE NAME :')
8000  FORMAT(' !!! ASTPTR : STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF FOR ',A)
!
      PRINT 1020
!
!  FICHIER D'ENTREE
!
      PRINT 7000
      READ(*,1000) INFILE
      PRINT 2000,  TRIM(INFILE)
      OPEN(81,FILE=TRIM(INFILE),FORM='UNFORMATTED',STATUS='OLD')
!
!  FICHIER DE SORTIE
!
      OUTFILE = TRIM(INFILE)//'_ASC'
      PRINT 2001,  TRIM(OUTFILE)
      OPEN(10,FILE=TRIM(OUTFILE))
!
! *** LECTURE DES PARAMETRES DE H
!
      CALL IOPBAS(81,10)
!
! *** LECTURE DES PARAMETRES DU MOMENT DE TRANSITION
!
      CALL IOPBAS(81,10)
      DO IN=1,2                                                                                    ! BOUCLE SUR LES DEUX NIVEAUX
        WRITE(10,*) '>>> NSV'
        READ (81,END=9003) NSV,TITRE
        WRITE(10,*)        NSV,TITRE
        DO ISV=1,NSV
          READ (81,END=9003) TITRE(:99)
          WRITE(10,*)        TITRE(:99)
        ENDDO
      ENDDO
      WRITE(10,*) '>>> NBOPT'
      READ (81,END=9003) NBOPT,NOM(:24)
      WRITE(10,*)        NBOPT,NOM(:24)
      WRITE(10,*) '>>> JK,ICK,ICPK,NFBK,JL,ICL,ICPL,NFBL'
      WRITE(10,*) '>>> IK,IL,TM'
!
3     READ (81,END=9000) JK,ICK,ICPK,NFBK,JL,ICL,ICPL,NFBL
      WRITE(10,*)        JK,ICK,ICPK,NFBK,JL,ICL,ICPL,NFBL
      DO IKC=1,NFBK
        DO ILC=1,NFBL
          READ (81,END=9003) IK,IL,TM
          WRITE(10,*)        IK,IL,TM
        ENDDO
      ENDDO
      GOTO 3
!
9003  PRINT 8003, TRIM(INFILE)
      PRINT 8000
!
9000  CLOSE(10)
      PRINT *
      END PROGRAM ASTPTR
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!     LECTURE ET RECOPIE DES PARAMETRES
!
      SUBROUTINE IOPBAS(LUI,LUO)
      use mod_dppr
      use mod_par_tds
      IMPLICIT NONE
      integer          :: LUI,LUO

      real(kind=dppr)  :: PARA,PREC

      integer          :: I,IP
      integer          :: NBOPH

      character(len = NBCLAB+10)  :: CHAINE
      character(len = NBCTIT)     :: TITRE
!
1000  FORMAT(A)
1001  FORMAT(I4,A)
1002  FORMAT(A,E18.11,E14.7)
8000  FORMAT(' !!! ASTPTR : STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF IN IOPBAS')
!
      DO I=1,4
        READ (LUI,END=2000) TITRE
        WRITE(LUO,1000)     TRIM(TITRE)
      ENDDO
      READ (LUI,END=2000) NBOPH,TITRE
      WRITE(LUO,1001)     NBOPH,TRIM(TITRE)
      DO I=1,2
        READ (LUI,END=2000) TITRE
        WRITE(LUO,1000)     TRIM(TITRE)
      ENDDO
      DO IP=1,NBOPH
        READ (LUI,END=2000) CHAINE,PARA,PREC
        WRITE(LUO,1002)     CHAINE,PARA,PREC
      ENDDO
      RETURN
!
2000  PRINT 8003
      PRINT 8000
      PRINT *
      STOP
!
      END SUBROUTINE IOPBAS
