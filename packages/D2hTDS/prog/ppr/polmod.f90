      PROGRAM POLMOD
!
!  14.12.88 FORTRAN 77 DU SUN4 REV 13 JAN 1989
!  REV 25 JAN 1990
!  AOU. 93 T.GABARD
! REV    JAN 1995 JPC,CW (PARAMETER)
! MODIFIE EN JANVIER 2003  W. RABALLAND ---> C2H4/D2h.
!
!   CODAGE DES OPERATEURS ROVIBRATIONNELS DU TENSEUR DE POLARISABILITE
!   ENTRE DES RESTRICTIONS DONNEES DE POLYADES VIBRATIONNELLES TYPE XY6
!
!     CALCUL DES K  JMKCUB = FIXE DANS LE PROGRAMME
!
! APPEL : comme dipmod
!
!  *****  LIMITATIONS DU PROGRAMME GEREES PAR PARAMETER.
!
!     MXOPT                                                             !ICODRO:ICODVC:ICODVA:ICODAN
! NB MAXIMUM D'OPERATEURS ROVIBRATIONNELS
!     MXOCV                                                             !IDANIS:IDRO:IDVC:IDVA:IDSY:IRDRE
! NB MAXIMUM D'OPERATEURS ROVIBRATIONNELS PAR CASE VIBRATIONNELLE
!     MXEMR                                                             !EMOP:LI:IC
! NB MAXIMUM D'E.M.R.V. NON NULS
!     MXSNV                                                             !IFFI:IFFS:IKVA:IKVC
! NB MAXIMUM TOTAL DE SOUS-NIVEAUX VIBRATIONNELS SUP ET INF
!     MXOPR                                                             !IROCO:ICODRA
! NB MAXIMUM D'OPERATEURS ROVIBRATIONNELS PAR OPERATEUR VIBRATIONNEL
!
      use mod_dppr
      use mod_par_tds
      use mod_com_fdate
      use mod_com_phase
      use mod_com_sy
      use mod_main_polmod
      IMPLICIT NONE

! functions
      real(kind=dppr)  :: ELMR
      integer          :: CTR,NSYM1
      integer          :: MULGU

      real(kind=dppr)  :: EM,EM1,EM2,EN
      real(kind=dppr)  :: SP

      integer ,dimension(NBVQN)    :: ICF,ICP,IL,ILA,ILC,INA,INC
      integer ,dimension(NBVQN)    :: INUM,IV,IVA,IVC
      integer ,dimension(NBVQN+1)  :: ICA,ICC,IPA,IPC
      integer ,dimension(MXSYM)    :: IROT
      integer ,dimension(MXPOL)    :: NIVII,NIVIS
      integer ,dimension(0:MXPOL)  :: IORTR
      integer ,dimension(6)        :: IRAM = (/ 1, 1, 1, 2, 3, 4 /)
      integer ,dimension(6)        :: LRAM = (/ 0, 2, 2, 2, 2, 2 /)
      integer ,dimension(6)        :: NRAM = (/ 0, 0, 1, 0, 0, 0 /)
      integer          :: NFF_SUP,IOP_SUP,NROT_SUP,NBOPT_SUP
      integer          :: IROTA_SUP,NIV_SUP
      integer          :: I,IB
      integer          :: ICANIS,ICROT
      integer          :: ID,IDEGV,IDK,IDROT,IDUM,IDXRAM,IF,IF1,IF2,IG,IGAMA,II,IIIR,IJK,IK
      integer          :: IM,IMIA,IOA,IOA1,IOC,IOC1,IOP,IOPA,IOPC,IPGAMA
      integer          :: IRO,IRO1,IRO2,IRO3,IRO4,IROTA,IU
      integer          :: IVI,IVS
      integer          :: J,JANIS,JD,JJJ,JMINF,JMKCUB,JMSUP
      integer          :: KNVIB,KNIVP,KOP,KPOL,KPOLI,KPOLS,KVI
      integer          :: L,LOP
      integer          :: NANIS,NBOPT,NBVA,NBVC,NDUM,NFFI,NFFS,NIC
      integer          :: NPOLI,NPOLS,NROT,NS

      character(len =   1) ,dimension(0:MXPOL)  :: AORTR
      character(len =   1)  :: AVI,AVS
      character(len =  10)  :: CHAINE
      character(len = 120)  :: IFICH
!
1000  FORMAT('    EFFECTIVE POLARIZABILITY OPERATOR :'   , /,   &
             ' VIBRATIONAL REDUCED MATRIX ELEMENTS (RME)',//,   &
             A                                               )
1001  FORMAT(//)
1002  FORMAT(I6,'  |[',            &
             I2,A,A,' *',          &
             I2,A,A,' *',          &
             I2,A,A,' *',          &
             I2,A,A,' *',          &
             I2,A,A,' *',          &
             I2,A,A,' *',          &
             I2,A,A,' *',          &
             I2,A,A,' *',          &
             I2,A,A,' *',          &
             I2,A,A,' *',          &
             I2,A,A,' *',          &
             I2,A,A,']',A,A,' >')
1004  FORMAT(I4,' TH VIBRATIONAL OPERATOR',//,   &
             A,1X,' { ',                         &
             I2,A,A,' *',                        &
             I2,A,A,' *',                        &
             I2,A,A,' *',                        &
             I2,A,A,' *',                        &
             I2,A,A,' *',                        &
             I2,A,A,' *',                        &
             I2,A,A,' *',                        &
             I2,A,A,' *',                        &
             I2,A,A,' *',                        &
             I2,A,A,' *',                        &
             I2,A,A,' *',                        &
             I2,A,A,                             &
             ' } '                        , /,   &
             2X,' { ',                           &
             I2,A,A,' *',                        &
             I2,A,A,' *',                        &
             I2,A,A,' *',                        &
             I2,A,A,' *',                        &
             I2,A,A,' *',                        &
             I2,A,A,' *',                        &
             I2,A,A,' *',                        &
             I2,A,A,' *',                        &
             I2,A,A,' *',                        &
             I2,A,A,' *',                        &
             I2,A,A,' *',                        &
             I2,A,A,                             &
             ' } ',A,A                        )
1006  FORMAT(I1)
1007  FORMAT(I2)
1010  FORMAT(A)
1029  FORMAT(/,                     &
             I5,' NON ZERO RME',/)
1030  FORMAT(2X,A1,'<',I5,'|',3X,'|',I5,'>',3X,F19.13)
1100  FORMAT(/,                                                  &
             I4,' VIBRATIONAL SUBLEVEL(S) OF UPPER POLYAD :',/)
1133  FORMAT(/,                                                  &
             I4,' VIBRATIONAL SUBLEVEL(S) OF LOWER POLYAD :',/)
1199  FORMAT(/,              &
             'POLMOD : ',A)
1205  FORMAT(' UPPER LEVEL ',I2,' : (v1;v2;v3;v4;v5;v6;v7;v8;v9;v10;v11;v12) = (',11(I2,';'),I2,')')
1206  FORMAT(/,                                     &
             ' POLYAD ',I2,'  MINUS POLYAD ',I2,/)
1207  FORMAT(' LOWER LEVEL ',I2,' : (v1;v2;v3;v4;v5;v6;v7;v8;v9;v10;v11;v12) = (',11(I2,';'),I2,')')
1215  FORMAT(/,                                              &
             ' POLARIZABILITY DEVELOPMENT ORDER :  ',7(I2))
1218  FORMAT(/,                               &
             I4,' ROTATIONAL OPERATOR(S)',/)
1219  FORMAT(/,           &
             1X,46('-'))
1220  FORMAT(I4,' ROVIBRATIONAL OPERATORS',/)
1313  FORMAT(I4,I3,'(',I1,',',I1,A,A,')',1X,12I1,A,A,1X,12I1,A,A,1X,A,A,I8,I7,I8)                  ! 12 = NBVQN
2001  FORMAT(' >> ',A)
8000  FORMAT(' !!! POLMOD : STOP ON ERROR')
8001  FORMAT(' !!! ERROR OPENING CONTROL FILE')
8002  FORMAT(' !!! INCOMPLETE CONTROL FILE')
8003  FORMAT(' !!! INCOMPATIBLE ARGUMENTS')
8004  FORMAT(' !!! INVALID IIIR VALUE')
!
! ARRAY INITIAL ALLOCATION
!
      CALL ALLOC_ALL
!
      NFF_SUP   = -1
      IOP_SUP   = -1
      NROT_SUP  = -1
      NBOPT_SUP = -1
      IROTA_SUP = -1
      NIV_SUP   = -1
      OPEN(10,FILE=CTRLF,ERR=9998,STATUS='OLD')
      READ(10,1010,END=9997) FDATE
      PRINT 1199,            FDATE
!
! NUMERO DE LA POLYADE SUPERIEURE
!
      READ(10,1010,END=9997) CHAINE
      AVS = CHAINE(2:2)
      READ(AVS,1006) IVS
      NPOLS = IVS+1
!
! LECTURE DES NIVEAUX VIBRATIONNELS
!
      JMSUP = 0
      DO KPOL=1,NPOLS
        READ(10,1010,END=9997) CHAINE
        READ(CHAINE(2:3),1007) NIVIS(KPOL)
        IF( NIVIS(KPOL) .GT. NIV_SUP ) NIV_SUP = NIVIS(KPOL)
        DO WHILE( NIVIS(KPOL) .GT. MXNIV )
          CALL RESIZE_MXNIV
        ENDDO
        DO KNIVP=1,NIVIS(KPOL)
          DO KNVIB=1,NBVQN
            READ(10,1006,END=9997) NVIBS(KPOL,KNIVP,KNVIB)
            IF( KNVIB .GT. 2 ) JMSUP = MAX(JMSUP,NVIBS(KPOL,KNIVP,KNVIB))
          ENDDO
        ENDDO
      ENDDO
!
! NUMERO DE LA POLYADE INFERIEURE
!
      READ(10,1010,END=9997) CHAINE
      AVI = CHAINE(2:2)
      READ(AVI,1006) IVI
      NPOLI = IVI+1
!
! LECTURE DES NIVEAUX VIBRATIONNELS
!
      JMINF = 0
      DO KPOL=1,NPOLI
        READ(10,1010,END=9997) CHAINE
        READ(CHAINE(2:3),1007) NIVII(KPOL)
        IF( NIVII(KPOL) .GT. NIV_SUP ) NIV_SUP = NIVII(KPOL)
        DO WHILE( NIVII(KPOL) .GT. MXNIV )
          CALL RESIZE_MXNIV
        ENDDO
        DO KNIVP=1,NIVII(KPOL)
          DO KNVIB=1,NBVQN
            READ(10,1006,END=9997) NVIBI(KPOL,KNIVP,KNVIB)
            IF( KNVIB .GT. 2 ) JMINF = MAX(JMINF,NVIBI(KPOL,KNIVP,KNVIB))
          ENDDO
        ENDDO
      ENDDO
      JMKCUB = MAX(JMINF,JMSUP)
!
! ORDRE DU DEVELOPPEMENT
!
      READ(10,1010,END=9997) CHAINE
      IF( LEN_TRIM(CHAINE) .NE. IVI+2 ) GOTO 9996
      DO KVI=0,IVI
        I          = KVI+2
        AORTR(KVI) = CHAINE(I:I)
        READ(AORTR(KVI),1006) IORTR(KVI)
      ENDDO
!
!  NOM DU FICHIER DE SORTIE
!
      IFICH = 'MP_P'//AVS//'mP'//AVI//'_D'
      DO KVI=0,IVI
        IFICH = TRIM(IFICH)//AORTR(KVI)
      ENDDO
      READ(10,1010,END=9997) CHAINE
      IF    ( CHAINE(1:4) .EQ. 'Ir  ' ) THEN
        IIIR = 1
      ELSEIF( CHAINE(1:4) .EQ. 'IIr ' ) THEN
        IIIR = 2
      ELSEIF( CHAINE(1:4) .EQ. 'IIIr' ) THEN
        IIIR = 3
      ELSE
        GOTO 9995
      ENDIF
      CLOSE(10)
      PRINT 2001, TRIM(IFICH)
!
! INITIALISATIONS
!
      CALL FACTO
      CALL CAL6C
      OPEN(20,FILE=IFICH,STATUS='UNKNOWN')
      WRITE(20,1000) FDATE
      WRITE(20,1206) IVS,IVI
      DO I=1,NIVIS(NPOLS)
        WRITE(20,1205) I,(NVIBS(NPOLS,I,J),J=1,NBVQN)
      ENDDO
      DO I=1,NIVII(NPOLI)
        WRITE(20,1207) I,(NVIBI(NPOLI,I,J),J=1,NBVQN)
      ENDDO
!
!     CODAGE DES SOUS-NIVEAUX VIBRATIONNELS
!
!     SOUS-NIVEAUX SUPERIEURS.
!
      NFFS = NIVIS(NPOLS)
      IF( NFFS .GT. NFF_SUP ) NFF_SUP = NFFS
      DO WHILE( NFFS .GT. MXSNV )
        CALL RESIZE_MXSNV
      ENDDO
      DO IF1=1,NFFS
        DO IF2=1,NBVQN
          IV(IF2) = NVIBS(NPOLS,IF1,IF2)
        ENDDO
        CALL SIGVI(IV,IFFS,MXSNV,IF1,IIIR)
      ENDDO
      WRITE(20,1100) NFFS
      DO IF=1,NFFS
        DO I=1,NBVQN
          CALL VLNC(IFFS(I,IF),IV(I),ICF(I),ICP(I))
          IL(I)   = 0
          INUM(I) = 0
        ENDDO
        IFFS(NBVQN+1,IF) = 1
        IFFS(NBVQN+2,IF) = 1
        DO I=1,NBVQN
          CALL MULD2H(IFFS(NBVQN+1,IF),ICF(I),NDUM,IDUM)
          IFFS(NBVQN+1,IF) = IDUM
          IFFS(NBVQN+2,IF) = MULGU(IFFS(NBVQN+2,IF),ICP(I))
        ENDDO
        WRITE(20,1002) IF,                                          &
                       (IV(I),SYM(ICF(I)),PAR(ICP(I)),I=1,NBVQN),   &
                       SYM(IFFS(NBVQN+1,IF)),PAR(IFFS(NBVQN+2,IF))
      ENDDO
!
!     SOUS-NIVEAUX INFERIEURS.
!
      NFFI = NIVII(NPOLI)
      IF( NFFI .GT. NFF_SUP ) NFF_SUP = NFFI
      DO WHILE( NFFI .GT. MXSNV )
        CALL RESIZE_MXSNV
      ENDDO
      DO IF1=1,NFFI
        DO IF2=1,NBVQN
          IV(IF2) = NVIBI(NPOLI,IF1,IF2)
        ENDDO
        CALL SIGVI(IV,IFFI,MXSNV,IF1,IIIR)
      ENDDO
      WRITE(20,1133) NFFI
      DO IF=1,NFFI
        DO I=1,NBVQN
          CALL VLNC(IFFI(I,IF),IV(I),ICF(I),ICP(I))
          IL(I)   = 0
          INUM(I) = 0
        ENDDO
        IFFI(NBVQN+1,IF) = 1
        IFFI(NBVQN+2,IF) = 1
        DO I=1,NBVQN
          CALL MULD2H(IFFI(NBVQN+1,IF),ICF(I),NDUM,IDUM)
          IFFI(NBVQN+1,IF) = IDUM
          IFFI(NBVQN+2,IF) = MULGU(IFFI(NBVQN+2,IF),ICP(I))
        ENDDO
        WRITE(20,1002) IF,                                          &
                       (IV(I),SYM(ICF(I)),PAR(ICP(I)),I=1,NBVQN),   &
                       SYM(IFFI(NBVQN+1,IF)),PAR(IFFI(NBVQN+2,IF))
      ENDDO
      WRITE(20,1215) (IORTR(I),I=0,IVI)
      LOP   = 0
      NBOPT = 0
!
!     BOUCLE SUR LES POLYADES INFERIEURES
!
E36:  DO KPOLI=1,NPOLI
!
!     DETERMINATION DES CREATEURS
!
E35:    DO IOC=1,NIVII(KPOLI)
          DO IOC1=1,NBVQN
            IVC(IOC1) = NVIBI(KPOLI,IOC,IOC1)
          ENDDO
          NBVC = 1
          CALL SIGVI(IVC,IKVC,MXSNV,NBVC,IIIR)
!
!     POLYADES SUPERIEURES
!
          KPOLS = KPOLI+IVS-IVI
!
!     DETERMINATION DES ANNIHILATEURS
!
E33:      DO IOA=1,NIVIS(KPOLS)
            DO IOA1=1,NBVQN
              IVA(IOA1) = NVIBS(KPOLS,IOA,IOA1)
            ENDDO
            IF( IVI .EQ. IVS .AND.              &
                IOA .LT. IOC       ) CYCLE E33
            NBVA = 1
            CALL SIGVI(IVA,IKVA,MXSNV,NBVA,IIIR)
            IDEGV = 0
            DO IOP=1,NBVQN
              IDEGV = IVA(IOP)+IVC(IOP)+IDEGV
            ENDDO
            IDROT = IORTR(KPOLI-1)+1-IDEGV
!
!     COUPLAGE CREATEURS-ANNIHILATEURS
!
            IOPC = 1
            IMIA = 1
            IF( IOA .EQ. IOC ) IMIA = IOPC
            DO I=1,NBVQN
              CALL VLNC(IKVC(I,IOPC),IVC(I),ICC(I),IPC(I))
              ILC(I) = 0
              INC(I) = 0
            ENDDO
            IOPA = IMIA
            DO I=1,NBVQN
              CALL VLNC(IKVA(I,IOPA),IVA(I),ICA(I),IPA(I))
              ILA(I) = 0
              INA(I) = 0
            ENDDO
            ICA(NBVQN+1) = 1
            IPA(NBVQN+1) = 1
            DO I=1,NBVQN
              CALL MULD2H(ICA(NBVQN+1),ICA(I),NDUM,IDUM)
              ICA(NBVQN+1) = IDUM
              IPA(NBVQN+1) = MULGU(IPA(NBVQN+1),IPA(I))
            ENDDO
            IKVA(NBVQN+1,IOPA) = ICA(NBVQN+1)
            IKVA(NBVQN+2,IOPA) = IPA(NBVQN+1)
            ICC(NBVQN+1)       = 1
            IPC(NBVQN+1)       = 1
            DO I=1,NBVQN
              CALL MULD2H(ICC(NBVQN+1),ICC(I),NDUM,IDUM)
              ICC(NBVQN+1) = IDUM
              IPC(NBVQN+1) = MULGU(IPC(NBVQN+1),IPC(I))
            ENDDO
            IKVC(NBVQN+1,IOPC) = ICC(NBVQN+1)
            IKVC(NBVQN+2,IOPC) = IPC(NBVQN+1)
            CALL MULD2H(ICA(NBVQN+1),ICC(NBVQN+1),NDUM,IGAMA)
            IPGAMA = MULGU(IPA(NBVQN+1),IPC(NBVQN+1))
            IF( IPGAMA .EQ. 2 ) CYCLE E33
            NROT = 0
E1:         DO IDXRAM=1,6
E733:         DO IU=1,2
                DO IJK=1,MXSYM
                  IROT(IJK) = 0
                ENDDO
!
!     DETERMINATION DES OPERATEURS ROTATIONNELS POSSIBLES
!
                IROTA = 0
                IF( IDROT .LT. IU-1 ) CYCLE E733
                DO ID=IU-1,IDROT,2
                  CALL NBJC(ID,IRO1,IRO2,IRO3,IRO4)
                  IROT(1) = IROT(1)+IRO1
                  IROT(2) = IROT(2)+IRO2
                  IROT(3) = IROT(3)+IRO3
                  IROT(4) = IROT(4)+IRO4
                  DO IDK=ID,IU-1,-2
E361:               DO ICROT=1,MXSYM
                      NIC = NSYM1(IDK,ICROT)
                      IF( NIC                           .EQ. 0 ) CYCLE E361
                      IF( CTR(IRAM(IDXRAM),ICROT,IGAMA) .EQ. 0 ) CYCLE E361
                      DO NS=1,NIC
                        IROTA = IROTA+1
                        IF( IROTA .GT. IROTA_SUP ) IROTA_SUP = IROTA
                        IF( IROTA .GT. MXOPR     ) CALL RESIZE_MXOPR
                        IROCO(IROTA)  = 1000*ID+100*IDK+10*ICROT+NS-1
                        ICODRA(IROTA) = LRAM(IDXRAM)*10000+NRAM(IDXRAM)*10+IRAM(IDXRAM)
                      ENDDO
                    ENDDO E361
                  ENDDO
                ENDDO
!
!    CALCUL DU FACTEUR DE NORMALISATION
!
                IF( IROTA .EQ. 0 ) CYCLE E733
                SP = (-1)**(IU+1)
                EN = ELMR(IKVC(1,IOPC),IKVC(1,IOPC),IKVA(1,IOPA),IKVA(1,IOPA),IGAMA,IIIR)
                EN = ELMR(IKVC(1,IOPC),IKVA(1,IOPA),IKVC(1,IOPC),IKVA(1,IOPA),IGAMA,IIIR)*SP+EN
                IF( ABS(EN) .LT. 1.D-05 ) CYCLE E733
                LOP = LOP+1
                WRITE(20,1219)
                WRITE(20,1004) LOP,AUG(IU),                                 &
                               (IVC(J),SYM(ICC(J)),PAR(IPC(J)),J=1,NBVQN),  &
                               (IVA(J),SYM(ICA(J)),PAR(IPA(J)),J=1,NBVQN),  &
                               SYM(IGAMA),PAR(IPGAMA)
!
!     CALCUL ET CODAGE DES E.M.R.V.
!
                IOP = 0
                DO IB=1,NFFI
E5:               DO IK=1,NFFS
                    EM1 = ELMR(IFFI(1,IB),IKVC(1,IOPC),IKVA(1,IOPA),IFFS(1,IK),IGAMA,IIIR)
                    EM2 = ELMR(IFFI(1,IB),IKVA(1,IOPA),IKVC(1,IOPC),IFFS(1,IK),IGAMA,IIIR)*SP
                    EM  = EM1+EM2
                    IF( EM*EM .LT. 1.D-10 ) CYCLE E5
                    EM = EM/EN
                    IF( IU .EQ. 2 ) EM = -EM
! changement de convention pour les operateurs V dont les elements
! matriciels sont imaginaires (pour retablir, dans la majorite des cas,
! les anciens signes des parametres)     4 / 5 / 93
                    IOP = IOP+1
                    IF( IOP .GT. IOP_SUP ) IOP_SUP = IOP
                    IF( IOP .GT. MXEMR   ) CALL RESIZE_MXEMR
                    LI(IOP)   = IK
                    IC(IOP)   = IB
                    EMOP(IOP) = EM
                  ENDDO E5
                ENDDO
                WRITE(20,1029) IOP
                DO KOP=1,IOP
                  WRITE(20,1030) AIM(IU),IC(KOP),LI(KOP),EMOP(KOP)
                ENDDO
                WRITE(20,1218) IROTA
!
!    CODAGE DES OPERATEURS ROVIBRATIONNELS
!
                DO IRO=1,IROTA
                  NROT = NROT+1
                  IF( NROT .GT. NROT_SUP ) NROT_SUP = NROT
                  IF( NROT .GT. MXOCV    ) CALL RESIZE_MXOCV
                  IDRO(NROT)   = IROCO(IRO)
                  IDANIS(NROT) = ICODRA(IRO)
                  DO II=0,3
                    IDVC(NROT,II+1) = IVC(1+3*II)*100+IVC(2+3*II)*10+IVC(3+3*II)
                    IDVA(NROT,II+1) = IVA(1+3*II)*100+IVA(2+3*II)*10+IVA(3+3*II)
                  ENDDO
                  IDSY1(NROT) = ICC(NBVQN+1)*100+ICA(NBVQN+1)*10+IGAMA
                  IDSY2(NROT) = IPC(NBVQN+1)*100+IPA(NBVQN+1)*10+IPGAMA
                  IDSY1(NROT) = IDSY1(NROT)+LOP*1000
                ENDDO
              ENDDO E733
            ENDDO E1
!
!    MISE EN ORDRE A L'INTERIEUR D'UNE CASE VIBRATIONNELLE DONNEE
!
            IF( NROT .EQ. 0 ) GOTO 124
            CALL IRDER(NROT,IDRO,IRDRE,MXOCV)
            DO JJJ=1,NROT
              NBOPT = NBOPT+1
              IF( NBOPT .GT. NBOPT_SUP ) NBOPT_SUP = NBOPT
              IF( NBOPT .GT. MXOPT     ) CALL RESIZE_MXOPT
              IRO           = IRDRE(JJJ)
              ICODRO(NBOPT) = IDRO(IRO)
              ICODAN(NBOPT) = IDANIS(IRO)
              DO I=1,4
                ICODVC(NBOPT,I) = IDVC(IRO,I)
              ENDDO
              DO I=1,4
                ICODVA(NBOPT,I) = IDVA(IRO,I)
              ENDDO
              ICOSY1(NBOPT) = IDSY1(IRO)
              ICOSY2(NBOPT) = IDSY2(IRO)
            ENDDO
!
124         CONTINUE
          ENDDO E33
        ENDDO E35
      ENDDO E36
      CALL DEBUG( 'POLMOD => MXOPVT=',LOP)
!
!     STOCKAGE DES CODES DES OPERATEURS ROVIBRATIONNELS
!
      WRITE(20,1001)
      WRITE(20,1220) NBOPT
      DO IRO=1,NBOPT
        IM     = ICODRO(IRO)/1000
        IK     = (ICODRO(IRO)-1000*IM)/100
        IG     = (ICODRO(IRO)-1000*IM-100*IK)/10
        JD     = (ICODRO(IRO)-1000*IM-100*IK-10*IG)
        JANIS  = ICODAN(IRO)/10000
        ICANIS = MOD(ICODAN(IRO),10)
        NANIS  = (ICODAN(IRO)-10000*JANIS-ICANIS)/10
        CALL VLNC(MOD(ICOSY1(IRO),1000),ICC(NBVQN+1),ICA(NBVQN+1),IGAMA)
        CALL VLNC(ICOSY2(IRO),IPC(NBVQN+1),IPA(NBVQN+1),IPGAMA)
        DO I=0,3
          CALL VLNC(ICODVC(IRO,I+1),IVC(1+3*I),IVC(2+3*I),IVC(3+3*I))
        ENDDO
        DO I=0,3
          CALL VLNC(ICODVA(IRO,I+1),IVA(1+3*I),IVA(2+3*I),IVA(3+3*I))
        ENDDO
        WRITE(20,1313) IRO,IM,IK,JD,SYM(IG),PAR(IPGAMA),                           &
                       (IVC(L),L=1,NBVQN),SYM(ICC(NBVQN+1)),PAR(IPC(NBVQN+1)),     &
                       (IVA(L),L=1,NBVQN),SYM(ICA(NBVQN+1)),PAR(IPA(NBVQN+1)),     &
                       SYM(IGAMA),PAR(IPGAMA),ICODRO(IRO),ICOSY1(IRO),ICODAN(IRO)
      ENDDO
      CALL DEBUG( 'POLMOD => MXSNV=',NFF_SUP)
      CALL DEBUG( 'POLMOD => MXEMR=',IOP_SUP)
      CALL DEBUG( 'POLMOD => MXOCV=',NROT_SUP)
      CALL DEBUG( 'POLMOD => MXOPT=',NBOPT_SUP)
      CALL DEBUG( 'POLMOD => MXOPR=',IROTA_SUP)
      CALL DEBUG( 'POLMOD => MXNIV=',NIV_SUP)
      CLOSE(20)
      GOTO 9000
!
9995  PRINT 8004
      GOTO  9999
9996  PRINT 8003
      GOTO  9999
9997  PRINT 8002
      GOTO  9999
9998  PRINT 8001
9999  PRINT 8000
!
9000  PRINT *
      END PROGRAM POLMOD
