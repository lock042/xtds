      PROGRAM ASVP
!
! REV    SEPT 2006  CW
!
!  lire un fichier binaire (ex. INFILE) de type VP_
!  et l'ecrire en ASCII dans INFILE'_ASC'
!
! APPEL : asvp
!
      use mod_dppr
      use mod_par_tds
      use mod_main_hdi
      IMPLICIT NONE

      integer          :: IC,IP,ISV
      integer          :: ICP
      integer          :: J,JB
      integer          :: NBOPH,NFB,NSV

      character(len = NBCTIT)  :: TITRE
      character(len = 150)  :: INFILE
      character(len = 160)  :: OUTFILE
!
1000  FORMAT(A)
1020  FORMAT(/,            &
             'ASVP   : ')
2000  FORMAT(' <  ',A)
2001  FORMAT(' >> ',A)
3000  FORMAT(E25.16)
3001  FORMAT(2I8,(E25.16))
7000  FORMAT('ENTER VP_ TYPE FILE NAME :')
8000  FORMAT(' !!! ASVP   : STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF FOR ',A)
!
! ARRAY INITIAL ALLOCATION
!
      CALL ALLOC_ALL
!
      PRINT 1020
!
!  FICHIER D'ENTREE
!
      PRINT 7000
      READ(*,1000) INFILE
      PRINT 2000,  TRIM(INFILE)
      OPEN(60,FILE=TRIM(INFILE),FORM='UNFORMATTED',STATUS='OLD')
!
!  FICHIER DE SORTIE
!
      OUTFILE = TRIM(INFILE)//'_ASC'
      PRINT 2001,  TRIM(OUTFILE)
      OPEN(10,FILE=TRIM(OUTFILE))
!
      CALL IOPBAS(60,10)
!
      WRITE(10,*) '>>> NSV'
      READ (60,END=9003) NSV,TITRE
      WRITE(10,*)        NSV,TITRE
      DO ISV=1,NSV
        READ (60,END=9003) TITRE(:99)
        WRITE(10,*)        TITRE(:99)
      ENDDO
      WRITE(10,*) '>>> NBOPH'
      READ (60,END=9003) NBOPH
      WRITE(10,*)        NBOPH
      WRITE(10,*) '>>> J,IC,ICP,NFB'
      WRITE(10,*) '>>> (HD(K(IP)),IP=1,NFB)'
      WRITE(10,*) '>>> NVCOD(JB),NRCOD(JB),(T(JB,K(IP)),IP=1,NFB)'
!
12    READ (60,END=9000) J,IC,ICP,NFB
      WRITE(10,*)        J,IC,ICP,NFB
      DO WHILE( NFB .GT. MXDIMS )
        CALL RESIZE_MXDIMS
      ENDDO
      IF( NFB .EQ. 0 ) GOTO 12
      READ (60,END=9003) (HD(IP),IP=1,NFB)
      WRITE(10,3000)     (HD(IP),IP=1,NFB)
      DO JB=1,NFB
        READ (60,END=9003) NVCOD(1),NRCOD(1),(T(IP,1),IP=1,NFB)
        WRITE(10,3001)     NVCOD(1),NRCOD(1),(T(IP,1),IP=1,NFB)
      ENDDO
      GOTO 12
!
9003  PRINT 8003, TRIM(INFILE)
      PRINT 8000
!
9000  CLOSE(10)
      PRINT *
      END PROGRAM ASVP
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!  LECTURE ET RECOPIE DES PARAMETRES
!
      SUBROUTINE IOPBAS(LUI,LUO)
      use mod_dppr
      use mod_par_tds
      IMPLICIT NONE
      integer          :: LUI,LUO

      real(kind=dppr)  :: PARA,PREC

      integer          :: I,IP
      integer          :: NBOPH

      character(len = NBCLAB+10)  :: CHAINE
      character(len = NBCTIT)     :: TITRE
!
1000  FORMAT(A)
1001  FORMAT(I4,A)
1002  FORMAT(A,E18.11,E14.7)
8000  FORMAT(' !!! ASVP   : STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF IN IOPBAS')
!
      DO I=1,4
        READ (LUI,END=2000) TITRE
        WRITE(LUO,1000)     TRIM(TITRE)
      ENDDO
      READ (LUI,END=2000) NBOPH,TITRE
      WRITE(LUO,1001)     NBOPH,TRIM(TITRE)
      DO I=1,2
        READ (LUI,END=2000) TITRE
        WRITE(LUO,1000)     TRIM(TITRE)
      ENDDO
      DO IP=1,NBOPH
        READ (LUI,END=2000) CHAINE,PARA,PREC
        WRITE(LUO,1002)     CHAINE,PARA,PREC
      ENDDO
      RETURN
!
2000  PRINT 8003
      PRINT 8000
      PRINT *
      STOP
!
      END SUBROUTINE IOPBAS
