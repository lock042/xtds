      PROGRAM SPECTS
!
!  04/2010 M. SANZHAROV ---> STARK INFRARED SPECTRA
!
! APPEL : spects  PS NS PI NI DO JMAXST FMIN FMAX TVIB TROT RINMI [fpvib FPVIB] [abund ABUND]
!
      use mod_dppr
      use mod_par_tds
      use mod_com_const
      use mod_com_fdate
      use mod_com_fp
      use mod_com_spects
      use mod_com_spin
      IMPLICIT NONE

      real(kind=dppr) ,allocatable ,dimension(:,:)  :: BIM,BIO,BIP,BST
      real(kind=dppr) ,allocatable ,dimension(:)    :: EINFO,EINFP,EINFM
      real(kind=dppr) ,allocatable ,dimension(:)    :: VPZM,VPXM
      real(kind=dppr) ,allocatable ,dimension(:)    :: VPZO,VPXO
      real(kind=dppr) ,allocatable ,dimension(:)    :: VPZP,VPXP
      real(kind=dppr)  :: BI
      real(kind=dppr)  :: EINF,EPI,EPS
      real(kind=dppr)  :: F1,F2,FRJMAX
      real(kind=dppr)  :: TRJMAX,TRM,TVIB

      integer         ,allocatable ,dimension(:,:)  :: JST,INCST
      integer         ,allocatable ,dimension(:,:)  :: JIM,ICIM
      integer         ,allocatable ,dimension(:,:)  :: JIO,ICIO
      integer         ,allocatable ,dimension(:,:)  :: JIP,ICIP
      integer         ,allocatable ,dimension(:)    :: ICODEM,ICODEO,ICODEP

      integer          :: ICODIM
      integer          :: I,I111,I112,I113,I203,I204,IBI,III,ILIGI,ILIGIM,ILIGIO,ILIGIP
      integer          :: IMI,IMIM,IMIO,IMIP,INCI,INCS,IPI,IPS,ISNV,ISV,ITEMPS
      integer          :: JI,JIMAX,JIMIN,JMAXI,JMAXS,JMAXST
      integer          :: NNIVI,NNIVS,NSVINF,NSVSUP,NTRANS

      character(len =  11) ,dimension(5)  :: CARG
      character(len = NBCTIT)  :: TITRE
      character(len =  10)  :: OPT
      character(len =  30)  :: PUNIT
      character(len = 120)  :: FCNI,FCNS,FSEM2,FSNINF,FSNSUP
!
1000  FORMAT(A)
1010  FORMAT(/,                 &
             'SPECTS    : ',A)
1012  FORMAT(I4,A)
1021  FORMAT('       Frequency   Intensity  C"  P" |M"| sig" C   P  |M|  sig   Lower Energy')
1121  FORMAT(/,                                 &
             I3,' Upper Vibrational States',/)
1122  FORMAT(/,                                 &
             I3,' Lower Vibrational States',/)
1230  FORMAT(/,                                                &
             'Spectroscopic Data Calculated through J = ',I3)
1231  FORMAT('Imposed Frequency Range:',F20.6,' - ',F20.6)
1232  FORMAT('Vibrational Temperature:',F8.2,' Kelvin')
1233  FORMAT('Rotational  Temperature:',F8.2,' Kelvin')
1234  FORMAT('Intensity Lower Limit:',E10.2,1X,A)
1237  FORMAT(/,                             &
             '  Calculated Transitions',/)
1251  FORMAT('Abundance: ',F9.4)
1252  FORMAT('Vibrational partition function: ',1PE12.4)
2000  FORMAT(' <  ',A)
2001  FORMAT(' >> ',A)
2002  FORMAT(I2)
8000  FORMAT(' !!! STOP ON ERROR')
8001  FORMAT(' !!! ERROR OPENING CONTROL FILE')
8002  FORMAT(' !!! INCOMPLETE CONTROL FILE')
8003  FORMAT(' !!! MXMST  EXCEEDED : ',I8,' > ',I8)
8004  FORMAT(' !!! BAD OPTION IN CONTROL FILE')
8013  FORMAT(' !!! 0. < FPVIB    REQUESTED')
8014  FORMAT(' !!! 0. < ABUND <=1.    REQUESTED')
8101  FORMAT(' !!! ERROR OPENING spectr_st.t file')
8102  FORMAT(' !!! ERROR OPENING spectr_st.xy file')
8111  FORMAT(' !!! UPPER AND LOWER ELECTRIC FIELDS DIFFER ')
8112  FORMAT(' !!! MXISUM EXCEEDED : ',I8,' > ',I8)
8113  FORMAT(' !!! ICODIM EXCEEDED : ',I8,' > ',I8)
8114  FORMAT(' !!! MXJST  EXCEEDED :'    ,/,   &
             ' !!!    J   = ',I8,' > ',I8,/,   &
             ' !!! OR INC = ',I8,' > ',I8   )

      ICODIM  = 100000*MXSYM+  &
                10000*2+       &                                                                   ! 2 = # parity
                100*MXJST+     &                                                                   ! WARNING : MXJST < 99
                (MXJST+1)/2                                                                        ! maximum label of JST
      ALLOCATE(ISUMS(ICODIM))
      ISUMS = 0
      ALLOCATE(ICODEMS(ICODIM,MXISUM))
      ICODEMS = 0
!
      OPEN(10,FILE=CTRLF,ERR=9998,STATUS='OLD')
      READ(10,1000,END=9997) FDATE
      PRINT 1010,            FDATE
      DO I=1,5
        READ(10,1000,END=9997) CARG(I)
      ENDDO
      READ(10,*,ERR=9997) JMAXST
      IF( JMAXST .GT. MXMST) THEN
        PRINT 8003, JMAXST,MXMST
        GOTO  9999
      ENDIF
      FSNINF = 'VPS_'//TRIM(CARG(3))
      FSNSUP = 'VPS_'//TRIM(CARG(1))
      FCNS   = 'DISN_'//TRIM(CARG(1))//'_'
      FCNI   = 'DISN_'//TRIM(CARG(3))//'_'
      FSEM2  = 'DIS_'//TRIM(CARG(1))//'m'//TRIM(CARG(3))//'_'//TRIM(CARG(5))//'_'
      OPEN(61,FILE=FSNSUP,FORM='UNFORMATTED')
      OPEN(51,FILE=FSNINF,FORM='UNFORMATTED')
      PRINT 2000, TRIM(FCNS)
      PRINT 2000, TRIM(FCNI)
      PRINT 2000, TRIM(FSNINF)
      PRINT 2000, TRIM(FSNSUP)
      PRINT 2000, TRIM(FSEM2)
      PRINT 2001, 'spectr_st.t'
      PRINT 2001, 'spectr_st.xy'
      READ(CARG(2)(2:3),2002) NNIVS
      READ(CARG(4)(2:3),2002) NNIVI
!
!  APPLICATION DES DIRECTIVES
!
      OPEN(20,FILE=FCNS,FORM='UNFORMATTED',STATUS='OLD')
      OPEN(30,FILE=FCNI,ACCESS='DIRECT',RECL=20)
      OPEN(40,FILE=FSEM2,ACCESS='DIRECT',RECL=20)
      OPEN(100,FILE='spectr_st.t',ERR=4081,STATUS='OLD')
      OPEN(101,FILE='spectr_st.xy',ERR=4082,STATUS='UNKNOWN')
!
! *** BASIC MOLECULAR CONSTANTS
!
      DO I=1,3
        READ(100,*)
      ENDDO
      READ(100,*) SPIN,SPINY,AA,BB,CC
      READ(100,*)
      READ(100,*) VIBNU,B0
      REWIND(100)
!
! *** READING INPUT FILE HEADINGS
!
      CALL IOPBA0(100,0)
!
! INTENSITY UNIT
!
      DO I=1,7
        READ(100,*)
      ENDDO
      READ(100,1012) III,PUNIT
      REWIND(100)
      CALL IOPBA0(100,0)
      READ(100,*)
      CALL IOPBA0(100,0)
      READ(100,*)
      READ(100,1012) NSVSUP,TITRE
      PRINT 1121,    NSVSUP
      READ(100,*)
      READ(100,1000) TITRE
      PRINT 1000,    TITRE
      DO ISV=1,NSVSUP
        READ(100,1000) TITRE(:99)
        PRINT    1000, TITRE(:99)
      ENDDO
      READ(100,*)
      READ(100,1012) NSVINF,TITRE
      PRINT 1122,    NSVINF
      READ(100,*)
      READ(100,1000) TITRE
      PRINT 1000,    TITRE
      DO ISV=1,NSVINF
        READ(100,1000) TITRE(:99)
        PRINT    1000, TITRE(:99)
      ENDDO
!
! *** ENTERING NEW OPTIONS FROM ARGUMENTS OF THE SPECT COMMAND
!
      WRITE(100,1230) JMAXST
      PRINT     1230, JMAXST
      READ(10,*,END=9997) FMIN
      READ(10,*,END=9997) FMAX
      PRINT     1231, FMIN,FMAX
      READ (10,*,END=9997) TVIB
      WRITE(100,1232)      TVIB
      PRINT     1232,      TVIB
      READ (10,*,END=9997) TROT
      WRITE(100,1233)      TROT
      PRINT     1233,      TROT
      READ (10,*,END=9997) RINMI
      WRITE(100,1234)      RINMI,TRIM(PUNIT)
      PRINT     1234,      RINMI,TRIM(PUNIT)
      ABUND = 1.D0                                                                                 ! abundance
      FPVIB = 0.D0                                                                                 ! no fpvib option
!
19    READ(10,1000,END=21) OPT
      IF( OPT .EQ. 'fpvib' ) THEN
        READ(10,*,END=9997) FPVIB
        IF( FPVIB .LE. 0. ) GOTO 9992
        GOTO 19
      ENDIF
      IF( OPT .EQ. 'abund' ) THEN
        READ(10,*,END=9997) ABUND
        IF( ABUND .LE. 0. .OR.              &
            ABUND .GT. 1.      ) GOTO 9991
        GOTO 19
      ENDIF
      GOTO 9996                                                                                    ! bad option
!
21    CLOSE(10)
!
! *** HEADINGS
!
      WRITE(100,1237)
      WRITE(100,1021)
!
! *** CALCULATING  CONSTANT FACTOR FROM PARTITION FUNCTION
!
      COEF = (8.D0*PI*PI*PI*CL)/(CLUM*PLANK)*1.D-36
      COEF = COEF*T0/TROT/FPART(TROT,TVIB)
      WRITE(100,1251) ABUND
      PRINT     1251, ABUND
      WRITE(100,1252) FPVIB
      PRINT     1252, FPVIB
!
! *** INITIAL VALUES FOR Fmin,Fmax,Imax,JImin,JImax,Ntrans
!
      F1     = 1.D10
      F2     = 0.D0
      TRM    = 0.D0
      TRJMAX = 0.D0
      FRJMAX = 0.D0
      JIMIN  = 200
      JIMAX  = 0
      NTRANS = 0
!
      READ(40,REC=1) ISNV
      DO I203=1,ICODIM
        ISUMS(I203) = 0
      ENDDO
      DO I204=2,ISNV
        READ(20) ITEMPS
        IF( ITEMPS .GT. ICODIM ) THEN
          PRINT 8113, ITEMPS,ICODIM
          GOTO  9999
        ENDIF
        ISUMS(ITEMPS) = ISUMS(ITEMPS)+1
        IF( ISUMS(ITEMPS) .GT. MXISUM ) THEN
          PRINT 8112, ISUMS(ITEMPS),MXISUM
          GOTO  9999
        ENDIF
        ICODEMS(ITEMPS,ISUMS(ITEMPS)) = I204
      ENDDO
      CALL DEBUG( 'SPECTS => MXISUM=',MAXVAL(ISUMS))
      READ(51) JMAXI
      READ(51) EPI
      READ(61) JMAXS
      READ(61) EPS
      IF ( EPI .NE. EPS ) THEN
        PRINT 8111
        GOTO  9999
      ENDIF
      ICI=1
!
2     READ(61,END=9000) ICS,ICPS,IMS,ILIGS
      IF( ICS .NE. ICI ) THEN
        DEALLOCATE(BIM,JIM,ICIM,EINFM,BIO,JIO,ICIO,EINFO)
        ILIGIM = 0
        ILIGIO = 0
        ILIGIP = 0
      ENDIF
      ALLOCATE(BST(ILIGS,ILIGS))
      ALLOCATE(JST(ILIGS,ILIGS))
      ALLOCATE(INCST(ILIGS,ILIGS))
E4:   DO IBS=1,ILIGS
        READ(61) IMS,ESUP
        DO IPS=1,ILIGS
          READ(61) JS,INCS,BS
          IF( JS   .GT. MXJST       .OR.         &
              INCS .GT. (MXJST+1)/2      ) THEN
            PRINT 8114, JS,MXJST,INCS,(MXJST+1)/2
            GOTO  9999
          ENDIF
          BST(IBS,IPS)   = BS
          JST(IBS,IPS)   = JS
          INCST(IBS,IPS) = INCS
        ENDDO
        IF( IBS .EQ. 1 ) THEN
          IF( IMS .EQ. 0 ) THEN
!
5           READ(51) ICI,ICPI,IMI,ILIGI
            IF( IMI-IMS .GT. 1 ) THEN
              BACKSPACE(51)
              GOTO 800
            ENDIF
            SELECT CASE( IMI-IMS )
              CASE( 0 )
                ALLOCATE(BIO(ILIGI,ILIGI))
                ALLOCATE(JIO(ILIGI,ILIGI))
                ALLOCATE(ICIO(ILIGI,ILIGI))
                ALLOCATE(EINFO(ILIGI))
                ILIGIO = ILIGI
              CASE( 1 )
                ALLOCATE(BIP(ILIGI,ILIGI))
                ALLOCATE(JIP(ILIGI,ILIGI))
                ALLOCATE(ICIP(ILIGI,ILIGI))
                ALLOCATE(EINFP(ILIGI))
                ILIGIP = ILIGI
            END SELECT
            DO IBI=1,ILIGI
              READ(51) IMI,EINF
              IF( IMI .EQ. 0 .AND.               &
                  IBI .EQ. 1 .AND.               &
                  ICI .EQ. 1       ) EN0 = EINF
              DO IPI=1,ILIGI
                READ(51) JI,INCI,BI
                IF( JI   .GT. MXJST       .OR.         &
                    INCI .GT. (MXJST+1)/2      ) THEN
                  PRINT 8114, JI,MXJST,INCI,(MXJST+1)/2
                  GOTO  9999
                ENDIF
                SELECT CASE( IMI-IMS )
                  CASE( 0 )
                    BIO(IBI,IPI)  = BI
                    JIO(IBI,IPI)  = JI
                    ICIO(IBI,IPI) = INCI
                    EINFO(IBI)    = EINF
                  CASE( 1 )
                    BIP(IBI,IPI)  = BI
                    JIP(IBI,IPI)  = JI
                    ICIP(IBI,IPI) = INCI
                    EINFP(IBI)    = EINF
                END SELECT
              ENDDO
            ENDDO
            GOTO 5
          ELSE
            IF( IMS .GT. 1 ) DEALLOCATE(BIM,JIM,ICIM,EINFM)
            ALLOCATE(BIM(ILIGIO,ILIGIO))
            ALLOCATE(JIM(ILIGIO,ILIGIO))
            ALLOCATE(ICIM(ILIGIO,ILIGIO))
            ALLOCATE(EINFM(ILIGIO))
            BIM    = BIO
            JIM    = JIO
            ICIM   = ICIO
            EINFM  = EINFO
            ILIGIM = ILIGIO
            DEALLOCATE(BIO,JIO,ICIO,EINFO)
            ALLOCATE(BIO(ILIGIP,ILIGIP))
            ALLOCATE(JIO(ILIGIP,ILIGIP))
            ALLOCATE(ICIO(ILIGIP,ILIGIP))
            ALLOCATE(EINFO(ILIGIP))
            BIO    = BIP
            JIO    = JIP
            ICIO   = ICIP
            EINFO  = EINFP
            ILIGIO = ILIGIP
            DEALLOCATE(BIP,JIP,ICIP,EINFP)
            IF( IMS .LT. JMAXS ) THEN
              READ(51) ICI,ICPI,IMI,ILIGI
              ALLOCATE(BIP(ILIGI,ILIGI))
              ALLOCATE(JIP(ILIGI,ILIGI))
              ALLOCATE(ICIP(ILIGI,ILIGI))
              ALLOCATE(EINFP(ILIGI))
              ILIGIP = ILIGI
              DO IBI=1,ILIGI
                READ(51) IMI,EINF
                EINFP(IBI) = EINF
                DO IPI=1,ILIGI
                  READ(51) JI,INCI,BI
                  IF( JI   .GT. MXJST       .OR.         &
                      INCI .GT. (MXJST+1)/2      ) THEN
                    PRINT 8114, JI,MXJST,INCI,(MXJST+1)/2
                    GOTO  9999
                  ENDIF
                  BIP(IBI,IPI)  = BI
                  JIP(IBI,IPI)  = JI
                  ICIP(IBI,IPI) = INCI
                ENDDO
              ENDDO
            ENDIF
          ENDIF
        ENDIF
!
800     CONTINUE
E110:   DO I110=1,ILIGS
          IF( I110 .EQ. 1 ) THEN
            ALLOCATE(VPZO(ILIGIO),VPXO(ILIGIO),ICODEO(ILIGIO))
            DO I111=1,ILIGIO
              VPZO(I111)   = 0.D0
              VPXO(I111)   = 0.D0
              ICODEO(I111) = 0
            ENDDO
            IF( IMS .GT. 0 ) THEN
              ALLOCATE(VPZM(ILIGIM),VPXM(ILIGIM),ICODEM(ILIGIM))
              DO I112=1,ILIGIM
                VPZM(I112)   = 0.D0
                VPXM(I112)   = 0.D0
                ICODEM(I112) = 0
              ENDDO
            ENDIF
            IF( IMS .LT. JMAXS ) THEN
              ALLOCATE(VPZP(ILIGIP),VPXP(ILIGIP),ICODEP(ILIGIP))
              DO I113=1,ILIGIP
                VPZP(I113)   = 0.D0
                VPXP(I113)   = 0.D0
                ICODEP(I113) = 0
              ENDDO
            ENDIF
          ENDIF
          IF( BST(IBS,I110) .EQ. 0     .AND.             &
              I110          .LT. ILIGS       ) CYCLE E110
          JS     = JST(IBS,I110)
          BS     = BST(IBS,I110)
          INCT   = INCST(IBS,I110)
          ICODES = 100000*ICS+10000*ICPS+100*JS+INCT
          IF( ISUMS(ICODES) .EQ. 0 ) CYCLE E110
          IF( IMS           .GT. 0 ) THEN
            IMIM = IMS-1
            CALL PROBA(IMIM,BIM,JIM,ICIM,ILIGIM,EINFM,VPZM,VPXM,ICODEM)
            IF( I110 .EQ. ILIGS ) DEALLOCATE(VPZM,VPXM,ICODEM)
          ENDIF
          IMIO = IMS
          CALL PROBA(IMIO,BIO,JIO,ICIO,ILIGIO,EINFO,VPZO,VPXO,ICODEO)
          IF( I110 .EQ. ILIGS ) DEALLOCATE(VPZO,VPXO,ICODEO)
          IF( IMS  .LT. JMAXS ) THEN
            IMIP = IMS+1
            CALL PROBA(IMIP,BIP,JIP,ICIP,ILIGIP,EINFP,VPZP,VPXP,ICODEP)
            IF( I110 .EQ. ILIGS ) DEALLOCATE(VPZP,VPXP,ICODEP)
          ENDIF
        ENDDO E110
      ENDDO E4
      DEALLOCATE(BST,JST,INCST)
      GOTO 2
!
4081  PRINT 8101
      GOTO  9999
4082  PRINT 8102
      GOTO  9999
9991  PRINT 8014
      GOTO  9999
9992  PRINT 8013
      GOTO  9999
9996  PRINT 8004
      GOTO  9999
9997  PRINT 8002
      CLOSE(10)
      GOTO  9999
9998  PRINT 8001
9999  PRINT 8000
9000  CLOSE(40)
      CLOSE(61)
      CLOSE(60)
      CLOSE(100)
      PRINT *
      END PROGRAM SPECTS
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!     READING AND COPYING PARAMETERS
!
      SUBROUTINE IOPBA0(LUI,LUO)
      use mod_dppr
      use mod_par_tds
      IMPLICIT NONE
      integer          :: LUI,LUO

      real(kind=dppr)  :: PARA,PREC

      integer          :: I,IP
      integer          :: NBOPH

      character(len = NBCLAB+10)  :: CHAINE
      character(len = NBCTIT)     :: TITRE
!
1000  FORMAT(A)
1001  FORMAT(I4,A)
1002  FORMAT(A,E18.11,E14.7)
8000  FORMAT(' !!! SPECTS : STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF IN IOPBA0')
!
      DO I=1,6
        READ(LUI,1000,END=2000)          TITRE
        IF( LUO .NE. 0 ) WRITE(LUO,1000) TRIM(TITRE)
      ENDDO
      READ(LUI,1001,END=2000)          NBOPH,TITRE
      IF( LUO .NE. 0 ) WRITE(LUO,1001) NBOPH,TRIM(TITRE)
      DO I=1,2
        READ(LUI,1000,END=2000)          TITRE
        IF( LUO .NE. 0 ) WRITE(LUO,1000) TRIM(TITRE)
      ENDDO
      DO IP=1,NBOPH
        READ(LUI,1002,END=2000)          CHAINE,PARA,PREC
        IF( LUO .NE. 0 ) WRITE(LUO,1002) CHAINE,PARA,PREC
      ENDDO
      RETURN
!
2000  PRINT 8003
      PRINT 8000
      PRINT *
      STOP
!
      END SUBROUTINE IOPBA0
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
      SUBROUTINE PROBA(IMI,BIT,JIT,ICIT,IT,EINF,VPZ,VPX,ICODEI2)
      use mod_dppr
      use mod_com_const
      use mod_com_fp
      use mod_com_spects
      use mod_com_spin
      IMPLICIT NONE
      real(kind=dppr) ,dimension(IT)     :: EINF,VPZ,VPX
      real(kind=dppr) ,dimension(IT,IT)  :: BIT
      integer         ,dimension(IT)     :: ICODEI2
      integer         ,dimension(IT,IT)  :: ICIT,JIT
      integer          :: IMI,IT

! functions
      real(kind=dppr)  :: TROIJ

      real(kind=dppr) ,dimension(IT)  :: ENINF,F,HHT,W0
      real(kind=dppr)  :: AS
      real(kind=dppr)  :: HT,HTRA
      real(kind=dppr)  :: TJ,TR

      integer          :: I1,I2,I3,ICODEI,ICODET,ICODET2,ILIG,INCIM,INCSM,ITEMPI
      integer          :: JI,JIM,JSM
!
1023  FORMAT(F12.6,1PE11.4)
1024  FORMAT(F18.9,1X,1PE9.2,0P,8(1X,I3),1X,F15.9,4(1X,I3))
!
      CALL FACTO
E1:   DO I1=1,IT
        ENINF(I1) = EINF(I1)
        F(I1)     = ESUP-ENINF(I1)
        IF( FMIN .GT. F(I1) .OR.           &
            FMAX .LT. F(I1)      ) CYCLE E1
        HT      = ENINF(I1)*HCOVRK/TROT
        W0(I1)  = EXP(-HT)*SPIN(ICI)
        HHT(I1) = F(I1)*HCOVRK/TROT
E2:     DO I2=1,IT
          IF( BIT(I1,I2) .EQ. 0 ) THEN
            IF( I2 .EQ. IT ) GOTO 4
            CYCLE E2
          ENDIF
          JI = JIT(I1,I2)
          IF( ABS(JS-JI) .GT. 1 ) THEN
            IF( I2 .EQ. IT ) GOTO 4
            CYCLE E2
          ENDIF
          ICODEI = 100000*ICI+10000*ICPI+100*JI+ICIT(I1,I2)
          DO I3=1,ISUMS(ICODES)
            ILIG = ICODEMS(ICODES,I3)
            READ(30,REC=ILIG) ITEMPI
            IF( ITEMPI .EQ. ICODEI ) THEN
              READ(40,REC=ILIG) HTRA
              IF( SQRT(BS*BIT(I1,I2)) .GT. 0.8D0 ) THEN
                ICODEI2(I1) = 1000000*JS+10000*INCT+100*JI+ICIT(I1,I2)
              ENDIF
              IF( IMS-IMI .EQ. 0 ) THEN
                TJ      = ( (-1)**(1+JI-IMI) )*TROIJ(JI,1,JS,-IMI,0,IMS)
                VPZ(I1) = VPZ(I1)+BS*TJ*BIT(I1,I2)*HTRA
              ELSE
                TJ      = ( (-1)**(JI+IMI) )*TROIJ(JI,1,JS,-IMI,IMI-IMS,IMS)
                VPX(I1) = VPX(I1)-BS*TJ*BIT(I1,I2)*HTRA
              ENDIF
            ENDIF
          ENDDO
        ENDDO E2
!
4       IF( I110 .EQ. ILIGS ) THEN
          AS = VPZ(I1)*VPZ(I1)+VPX(I1)*VPX(I1)
          TR = 2.D0*AS*W0(I1)*F(I1)*COEF*(1.D0-EXP(-HHT(I1)))
          IF( IMI .EQ. 0 .AND.                 &
              IMS .EQ. 0       ) TR = TR/2.D0
          TR = TR*ABUND
          IF( TR .GT. RINMI ) THEN
            JSM     = ICODEI2(I1)/1000000
            INCIM   = MOD(ICODEI2(I1),100)
            ICODET  = MOD(ICODEI2(I1),1000000)
            INCSM   = ICODET/10000
            ICODET2 = MOD(ICODET,10000)
            JIM     = ICODET2/100
            WRITE(100,1024) F(I1),TR,      &
                            ICS,ICPS,IMS,  &
                            IBS,           &
                            ICI,ICPI,IMI,  &
                            I1,ENINF(I1),  &
                            JIM,INCIM,     &
                            JSM,INCSM
            WRITE(101,1023) F(I1),TR
          ENDIF
        ENDIF
      ENDDO E1
!
      RETURN
      END SUBROUTINE PROBA
