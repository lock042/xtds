      PROGRAM DIPMATS
!
! 14.12.88 FORTRAN 77 POUR SUN4  REV 16 JAN 1989
! REV 25 JAN 1990
! REV 15 FEV 1990
! REV    DEC 1992 T.GABARD
! REV    JAN 1995 JPC,CW (PARAMETER)
! MODIFIE 09/02 W. RABALLAND ---> C2H4/D2h
! MODIFIE 04/10 M. SANZHAROV ---> STARK C2H4/D2h
!
! ***  MOMENT DIPOLAIRE EFFECTIF DES XY4
! ***  RESTRICTION VIBRATIONNELLE DE POLYADES DE XY4
! ***  CALCUL ET STOCKAGE DES ELEMENTS MATRICIELS NON NULS
! ***  A PARTIR DES DONNEES DU FICHIER ISSU DE DIPMOD
!
! APPEL : dipmat Pn Nm Pn' Nm' Dp Jmax
!
!  ******    LIMITATIONS DU PROGRAMME
!
! VALEUR MAXIMALE DE J
!     MXJ
!
! DIMENSION MAXIMALE D'UN BLOC J,C POL SUPERIEURE
!     MXDIMS              !NRCODS,NVCODS
!
! DIMENSION MAXIMALE D'UN BLOC J,C POL INFERIEURE
!     MXDIMI              !NRCODI,NVCODI
!
! NB MAXIMUM D'OPERATEURS ROVIBRATIONNELS
!     MXOPT               !ICODR:ICODV
!
! NB MAXIMUM D'OPERATEURS VIBRATIONNELS
!     MXOPVT              !LIR:KOR:EMRV
!
! NB MAXIMUM DE SOUS-NIVEAUX VIBRATIONNELS
!     MXSNV               !EMRD
!
! NB MAXIMUM D'ELEMENTS MATRICIELS NON NULS D'UN OPERATEUR
!     MXELMT              !H:LI:KO
!
! NB MAXIMUM D'ELEMENTS MATRICIELS REDUITS NON NULS D'UN OPERATEUR
! VIBRATIONNEL
!     MXEMR               !LIR:KOR:EMRV
!
!
      use mod_dppr
      use mod_par_tds
      use mod_com_fdate
      use mod_main_dipmats
      IMPLICIT NONE

      integer          :: NFB_INF,JCL_SUP,NBELM_SUP,NFB_SUP
      integer          :: I,ICI,ICLUI,ICLUS,ICOR,ICS
      integer          :: I150,ICODEI,ICODES,IO,IOS,ISV
      integer          :: ICPI,ICPLUI,ICPLUS,ICPS
      integer          :: IDJCI,IDJIC,IEL,IFBI,IGV,IK,IMR
      integer          :: IOP,IOV,ISVI,ISVS
      integer          :: JC,JI,JL,JLUI,JLUS,JMAX,JMTEMP,JS
      integer          :: NBOTR,NEL,NELMH,NFBI,NFBS,NNIVI,NNIVS
      integer          :: NSVI,NSVII,NSVS,NSVSS

      character(len = NBCTIT)  :: IDENT,TITRE
      character(len =  11) ,dimension(5)  :: CARG
      character(len =  40)  :: IDEMR
      character(len = 120)  :: FCFBPI,FCFBPS,FEMRV,FSEM2,FPARA,FCNS,FCNI
!
1000  FORMAT(A)
1001  FORMAT(I4,A)
1007  FORMAT(4X,I5,5X,I5,4X,F19.13)
1008  FORMAT(I4,A,I8,I7)                                                                           ! cf. 1313 de dipmod.f
1010  FORMAT(/,              &
             'DIPMATS : ',A)
1015  FORMAT(' DIPMATS -> J = ',I3,'/',I3)
1020  FORMAT('     #    v1     v2     v3     v4     v5     v6     v',   &
             '7     v8     v9     v10    v11    v12    Cv'           )
1031  FORMAT(/////,   &
             I5,/  )
1035  FORMAT(/,     &
             I4,/)
1036  FORMAT(I2)
1121  FORMAT(/,                                 &
             I3,' Upper Vibrational States',/)
1122  FORMAT(/,                                 &
             I3,' Lower Vibrational States',/)
1235  FORMAT('Hamiltonian Parameters in D2h Formalism',/)
1236  FORMAT(/,                                                  &
             'Transition Moment Parameters in D2h Formalism',/)
2000  FORMAT(' <  ',A)
2001  FORMAT(' >> ',A)
8000  FORMAT(' !!! DIPMATS : STOP ON ERROR')
8001  FORMAT(' !!! ERROR OPENING CONTROL FILE')
8002  FORMAT(' !!! INCOMPLETE CONTROL FILE')
8116  FORMAT(' !!! INCOMPATIBLE JMAX : ',I3)
8118  FORMAT(' !!! JMAX TOO LARGE'                ,/,   &
             ' !!! MXJST  EXCEEDED : ',I8,' > ',I8   )
8125  FORMAT(' !!! INCOMPATIBLE FUNCTION FILE AND OPERATOR FILE')
8128  FORMAT(' !!! UNEXPECTED EOF IN WAVEFUNCTION FILE')
8129  FORMAT(' !!! MAXIMUM LABEL OF JST EXCEEDED')
!
! ARRAY INITIAL ALLOCATION
!
      CALL ALLOC_ALL
!
      OPEN(10,FILE=CTRLF,ERR=9998,STATUS='OLD')
      READ(10,1000,END=9997) FDATE
      PRINT 1010,            FDATE
      DO I=1,5
        READ(10,1000,END=9997) CARG(I)
      ENDDO
      READ(CARG(2)(2:3),1036) NNIVS
      READ(CARG(4)(2:3),1036) NNIVI
      READ(10,*,ERR=9997) JMAX
      READ(10,1000,ERR=9997) FPARA
      FEMRV  = 'MD_'//TRIM(CARG(1))//'m'//TRIM(CARG(3))//'_'//CARG(5)
      FCFBPS = 'FN_'//TRIM(CARG(1))//'_'
      FCFBPI = 'FN_'//TRIM(CARG(3))//'_'
      CLOSE(10)
      PRINT 2000, TRIM(FEMRV)
      PRINT 2000, TRIM(FCFBPS)
      PRINT 2000, TRIM(FCFBPI)
      IF( JMAX .LT. 0 ) THEN
        PRINT 8116, JMAX
        GOTO  9999
      ENDIF
!
!  APPLICATION DES DIRECTIVES
!
      IF( JMAX .GT. MXJST ) THEN
        PRINT 8118, JMAX,MXJST
        GOTO  9999
      ENDIF
!
! FICHIER DES E.M.R.V.
!
      OPEN(20,STATUS='OLD',FILE=FEMRV,FORM='FORMATTED')
!
! FICHIER DE CODES DES FCTS DE BASE POL SUPERIEURE
!
      OPEN(30,STATUS='OLD',FILE=FCFBPS,FORM='UNFORMATTED')
!
! FICHIER DE CODES DES FCTS DE BASE POL INFERIEURE
!
      OPEN(31,STATUS='OLD',FILE=FCFBPI,FORM='UNFORMATTED')
!
! FICHIER DE STOCKAGE DES E.M.
!
      OPEN(40,FILE='spectr_st.t',STATUS='UNKNOWN')
      OPEN(50,IOSTAT=IOS,FILE=FPARA,STATUS='OLD')
      CALL CAL6C
      JMTEMP = MAX(5,JMAX+1)
      CALL FACTO
      WRITE(40,1235)
      CALL IOPAB(50,40,0)
      WRITE(40,1236)
      CALL IOPAB(50,40,1)
      CLOSE(50)
      FCNS  = 'DISN_'//TRIM(CARG(1))//'_'
      FCNI  = 'DISN_'//TRIM(CARG(3))//'_'
      FSEM2 = 'DIS_'//TRIM(CARG(1))//'m'//TRIM(CARG(3))//'_'//TRIM(CARG(5))//'_'
      OPEN(80,FILE=FCNS,FORM='UNFORMATTED',STATUS='UNKNOWN')
      OPEN(90,FILE=FCNI,ACCESS='DIRECT',RECL=20)
      OPEN(100,FILE=FSEM2,ACCESS='DIRECT',RECL=20)
      PRINT 2001, TRIM(FCNS)
      PRINT 2001, TRIM(FCNI)
      PRINT 2001, TRIM(FSEM2)
      ISV = 2
!
! *** REMISE A ZERO.
!
      DO IOP=1,MXOPVT
        DO IK=1,MXEMR
          KOR(IK,IOP)  = 0
          LIR(IK,IOP)  = 0
          EMRV(IK,IOP) = 0.D0
        ENDDO
      ENDDO
!
! LECTURE DES CARACTERISTIQUES GENERALES
!
      DO I=1,3
        READ(20,1000) TITRE
      ENDDO
      READ(20,1000) IDEMR
      DO I=1,4+NNIVI+NNIVS
        READ(20,1000) TITRE
      ENDDO
!
!  CARACTERISTIQUES DE LA POLYADE SUPERIEURE
!
      READ (20,1001) NSVS,TITRE
      WRITE(40,1121) NSVS
      DO WHILE( NSVS .GT. MXSNV )
        CALL RESIZE_MXSNV
      ENDDO
      WRITE(40,1020)
!
! SOUS-NIVEAUX VIB DE LA POLYADE SUPERIEURE
!
      READ(20,*,END=3994)
      DO ISVS=1,NSVS
        READ (20,1000) IDENT(:99)
        WRITE(40,1000) IDENT(:99)
      ENDDO
!
!  CARACTERISTIQUES DE LA POLYADE INFERIEURE
!
      READ(20,*,END=3994)
      READ (20,1001) NSVI,TITRE
      WRITE(40,1122) NSVI
      DO WHILE( NSVI .GT. MXSNV )
        CALL RESIZE_MXSNV
      ENDDO
      WRITE(40,1020)
!
! SOUS-NIVEAUX VIB DE LA POLYADE INFERIEURE
!
      READ(20,*,END=3994)
      DO ISVI=1,NSVI
        READ (20,1000) IDENT(:99)
        WRITE(40,1000) IDENT(:99)
      ENDDO
!
!  ORDRE DU DEVELOPPEMENT
!
      DO I=1,3
        READ(20,1000)
      ENDDO
!
! LECTURE DES ELEMENTS MATRICIELS REDUITS
!
      IOV = 0
!
5     READ(20,1000) TITRE
      IF( TITRE(1:2) .EQ. '  ' ) GOTO 2201
      IOV = IOV+1
      IF( IOV .GT. MXOPVT ) CALL RESIZE_MXOPVT
      READ(20,1031) NEL
      DO WHILE( NEL .GT. MXEMR )
        CALL RESIZE_MXEMR
      ENDDO
      DO IEL=1,NEL
        READ(20,1007) LIR(IEL,IOV),KOR(IEL,IOV),EMRV(IEL,IOV)
      ENDDO
      DO I=1,4
        READ(20,1000) TITRE
      ENDDO
      GOTO 5
!
2201  CONTINUE
      CALL DEBUG( 'DIPMATS => MXOPVT=',IOV)
!
! OPERATEURS DU MOMENT DIPOLAIRE
!
      READ(20,1035) NBOTR
      DO WHILE( NBOTR .GT. MXOPT )
        CALL RESIZE_MXOPT
      ENDDO
      DO IOP=1,NBOTR
        READ(20,1008) IO,IDENT(:NBCLAB-4),ICODR(IOP),ICODV(IOP)                                    ! cf. 1313 de dipmod.f
      ENDDO
!
!  POSITIONNEMENT APRES L'ENTETE DES FICHIERS DE FONCTIONS D'ONDE SUP ET INF
!
      CALL IPAR(30,NNIVS,NSVSS)
      IF( NSVSS .NE. NSVS ) THEN
        PRINT 8125
        GOTO  9999
      ENDIF
      NFB_INF = -1
      CALL IPAR(31,NNIVI,NSVII)
      IF( NSVII .NE. NSVI ) THEN
        PRINT 8125
        GOTO  9999
      ENDIF
!
!  BOUCLE SUR JS (POLYADE SUPERIEURE)
!
      JCL_SUP   = -1
      NBELM_SUP = -1
      NFB_SUP   = -1
      ISV       =  2
E400: DO JS=0,JMAX
        REWIND(31)
        CALL IPAR(31,NNIVI,NSVII)
!
! DIMENSION DES BLOCS J,C DE LA POLYADE INFERIEURE
! JI=JS-1,JS,JS+1 ; IC = 1,MXSYM ; ICP = 1,2
!
        DO IDJIC=1,MDMJCI
          JICDIM(IDJIC) = 0
        ENDDO
        DO JI=MAX(JS-1,0),JS+1
          DO ICI=1,MXSYM
            DO ICPI=1,2
!
4666          READ(31) JLUI,ICLUI,ICPLUI,NELMH,NFBI
              IF( JLUI .GT. JS+1    ) GOTO 467
              IF( NFBI .GT. NFB_INF ) NFB_INF = NFBI
              IF( NFBI .NE. 0       ) THEN
                DO WHILE( NFBI .GT. MXDIMI )
                  CALL RESIZE_MXDIMI
                ENDDO
                READ(31) (NVCODI(I),NRCODI(I),I=1,NFBI)
              ENDIF
              IF( JLUI .LT. JI ) GOTO 4666
              IDJCI         = (JLUI-JS+1)*MXSYM*2+(ICLUI-1)*2+ICPLUI
              JICDIM(IDJCI) = NFBI
              DO IFBI=1,NFBI
                NVCI(IFBI,IDJCI) = NVCODI(IFBI)
                NRCI(IFBI,IDJCI) = NRCODI(IFBI)
              ENDDO
            ENDDO
          ENDDO
        ENDDO
!
!  BOUCLE SUR CS (POLYADE SUPERIEURE)
!
467     CONTINUE
E1:     DO ICS=1,MXSYM
E503:     DO ICPS=1,2
            READ(30) JLUS,ICLUS,ICPLUS,NELMH,NFBS
            IF( NFBS .GT. NFB_SUP ) NFB_SUP = NFBS
            IF( NFBS .EQ. 0       ) CYCLE E503
            DO WHILE( NFBS .GT. MXDIMS )
              CALL RESIZE_MXDIMS
            ENDDO
            READ(30) (NVCODS(I),NRCODS(I),I=1,NFBS)
            ICI  = ICS
            ICPI = 1+MOD(ICPS,2)
!
!  BOUCLE SUR LES BRANCHES (POLYADE INFERIEURE)
!
E406:       DO JI=MAX(JS-1,0),JS+1
              IDJCI = (JI-JS+1)*MXSYM*2+(ICI-1)*2+ICPI
              NFBI  = JICDIM(IDJCI)
              IF( NFBI .EQ. 0 ) CYCLE E406
              DO IFBI=1,NFBI
                NVCODI(IFBI) = NVCI(IFBI,IDJCI)
                NRCODI(IFBI) = NRCI(IFBI,IDJCI)
              ENDDO
!
!  BOUCLE SUR LES OPERATEURS
!
E414:         DO IOP=1,NBOTR
                IF( PARAT(IOP) .EQ. 0.D0 ) CYCLE E414
                DO JL=1,MXSNV
                  DO JC=1,MXSNV
                    EMRD(JL,JC) = 0.D0
                  ENDDO
                ENDDO
                IOV = ICODV(IOP)/1000
                IGV = MOD(ICODV(IOP),10)
                DO IMR=1,MXEMR
                  IF( LIR(IMR,IOV) .EQ. 0 ) GOTO 639
                  JC = LIR(IMR,IOV)
                  JL = KOR(IMR,IOV)
                  IF( JC .GT. JCL_SUP ) JCL_SUP = JC
                  IF( JL .GT. JCL_SUP ) JCL_SUP = JL
                  EMRD(JL,JC) = EMRV(IMR,IOV)
                ENDDO
!
639             ICOR = ICODR(IOP)
                CALL CALDI(JS,ICS,NFBS,JI,ICI,NFBI,ICOR,IGV)
                IF( NBELM .GT. NBELM_SUP ) NBELM_SUP = NBELM
                IF( NBELM .EQ. 0         ) CYCLE E414
                IF( JS    .GT. JMAX      ) CYCLE E414
                IF( JI    .GT. JMAX      ) CYCLE E414
                DO I150=1,NBELM
                  IF( (KO(I150) .GT. (MXJST+1)/2)  .OR.         &
                      (LI(I150) .GT. (MXJST+1)/2)       ) THEN
                    PRINT 8129
                    GOTO  9999
                  ENDIF
                  ICODES = 100000*ICS+10000*ICPS+100*JS+KO(I150)
                  WRITE(80) ICODES
                  ICODEI = 100000*ICI+10000*ICPI+100*JI+LI(I150)
                  WRITE(90,REC=ISV) ICODEI
                  WRITE(100,REC=ISV) H(I150)*PARAT(IOP)
                  ISV = ISV+1
                ENDDO
              ENDDO E414
            ENDDO E406
          ENDDO E503
        ENDDO E1
      ENDDO E400
      ISV = ISV-1
      WRITE(100,REC=1) ISV
      CLOSE(80)
      CLOSE(90)
      CLOSE(100)
      CALL DEBUG( 'DIPMATS => MXSNV=',JCL_SUP)
      CALL DEBUG( 'DIPMATS => MXELMT=',NBELM_SUP)
      CALL DEBUG( 'DIPMATS => MXDIMI=',NFB_INF)
      CALL DEBUG( 'DIPMATS => MXDIMS=',NFB_SUP)
      CLOSE(20)
      CLOSE(30)
      CLOSE(31)
      CLOSE(40)
      PRINT 1015, JS-1,JMAX
      GOTO  9000
!
3994  PRINT 8128
      GOTO  9999
9997  PRINT 8002
      CLOSE(10)
      GOTO  9999
9998  PRINT 8001
9999  PRINT 8000
9000  PRINT *
      END PROGRAM DIPMATS
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!     LECTURE ET RECOPIE DES PARAMETRES
!
      SUBROUTINE IOPAB(LUI,LUO,IS)
      use mod_dppr
      use mod_par_tds
      use mod_main_dipmats
      IMPLICIT NONE
      integer          :: LUI,LUO,IS

      real(kind=dppr)  :: PARA,PREC

      integer          :: I,IP,MBOPT

      character(len = NBCTIT)  :: TITRE
      character(len = NBCLAB+10)  :: CHAINE
!
1000  FORMAT(A)
1001  FORMAT(I4,A)
1002  FORMAT(A,E18.11,E14.7)
8000  FORMAT(' !!! STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF IN IOPAB')
!
      DO I=1,4
        READ(LUI,1000,END=2000)          TITRE
        IF( LUO .NE. 0 ) WRITE(LUO,1000) TRIM(TITRE)
      ENDDO
      READ(LUI,1001,END=2000)          MBOPT,TITRE
      IF( LUO .NE. 0 ) WRITE(LUO,1001) MBOPT,TRIM(TITRE)
      DO WHILE( MBOPT .GT. MXOPT )
        CALL RESIZE_MXOPT
      ENDDO
      DO I=1,2
        READ(LUI,1000,END=2000)          TITRE
        IF( LUO .NE. 0 ) WRITE(LUO,1000) TRIM(TITRE)
      ENDDO
      DO IP=1,MBOPT
        READ(LUI,1002,END=2000)          CHAINE,PARA,PREC
        IF( IS  .EQ. 1 ) PARAT(IP) = PARA
        IF( LUO .NE. 0 ) WRITE(LUO,1002) CHAINE,PARA,PREC
      ENDDO
      IF( IS .EQ. 1 ) NBOPT = MBOPT
      RETURN
!
2000  PRINT 8003
      PRINT 8000
      PRINT *
      STOP
!
      END SUBROUTINE IOPAB
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ***  CALCULE LES ELEM. MAT. DES OP. DU M. DIP.
!
! SMIL J.P.C., J.M.J. , G.P.    DEC.88
! MOD. T.GABARD MAR. 93
! REV    JAN 1995 JPC,CW (PARAMETER)
! MODIFIE 09/02 W. RABALLAND ---> C2H4/D2h.
!
      SUBROUTINE CALDI(JS,ICS,NFBS,JI,ICI,NFBI,ICOR,IGV)
      use mod_dppr
      use mod_par_tds
      use mod_main_dipmats
      IMPLICIT NONE
      integer          :: JS,ICS,NFBS,JI,ICI,NFBI,ICOR,IGV

! functions
      real(kind=dppr)  :: EMRRO
      integer          :: CTR,MULGU,NSYM1

      real(kind=dppr)  :: CK1,CKK,COEF
      real(kind=dppr)  :: ELM
      real(kind=dppr)  :: TERM1,TERM11,TERM2,TERM22

      integer          :: IB,IBRA,ICRB,ICRK,ICSUM,IG,IGC,IGRV
      integer          :: IK,IKET,IO,IPB,IPK,IPT,IVB,IVK
      integer          :: KR
      integer          :: NDUM,NG,NMB,NRB,NRCB,NRCK,NRK
      integer          :: NSSUM,NVB,NVK
!
! OMEGA
!
      IO = ICOR/1000
!
! KR
!
      KR = (ICOR-IO*1000)/100
!
! GAMMA R
!
      IG = (ICOR-IO*1000-KR*100)/10
!
! N R
!
      NG    = ICOR-IO*1000-KR*100-IG*10
      NBELM = 0
!
! POLYADE INFERIEURE
!
E3:   DO IBRA=1,NFBI
        NVB = NVCODI(IBRA)
!
! NUMERO DU SOUS-NIVEAU VIBRATIONNEL INF.
!
        IB = NVB/100
!
! C'V
!
        IPB  = NVB-100*IB
        IVB  = IPB/10
        NRCB = NRCODI(IBRA)
!
! PARITE DE C'V
!
        IPB = IPB-10*IVB
!
! N'
!
        NRB = NRCB/10
!
! C'R
!
        ICRB = NRCB-10*NRB
!
!   POLYADE SUPERIEURE
!
E4:     DO IKET=1,NFBS
          NVK = NVCODS(IKET)
!
! NUMERO DU SOUS-NIVEAU VIBRATIONNEL SUP.
!
          IK = NVK/100
          IF( EMRD(IK,IB) .EQ. 0.D0 ) CYCLE E4
!
! CV
!
          IPK  = NVK-100*IK
          IVK  = IPK/10
          NRCK = NRCODS(IKET)
!
! PARITE DE CV
!
          IPK = IPK-10*IVK
          IPT = MULGU(IPB,IPK)
          IF( IPT .EQ. 1 ) CYCLE E4
!
! N
!
          NRK = NRCK/10
!
! CR
!
          ICRK = NRCK-10*NRK
!
! SYMETRIE DE M
!
          CALL MULD2H(IG,IGV,NDUM,IGRV)
          CALL MULD2H(IGRV,ICS,NDUM,IGC)
          ELM = 0.D0
          IF( ICI .NE. ICS ) GOTO 401
!
!    PREMIERE SOMMATION
!
          TERM1 = 0.D0
E5:       DO ICSUM=1,MXSYM
            NSSUM = NSYM1(JS,ICSUM)
            IF( NSSUM                               .EQ. 0 ) CYCLE E5
            IF( CTR(IG,ICRK,ICSUM)*CTR(IGV,IVK,IVB) .EQ. 0 ) CYCLE E5
            TERM11 = 0.D0
            DO NMB=0,NSSUM-1
              CALL KCUBU(1,JS,JI,0,NMB,NRB,IGRV,ICSUM,ICRB,CK1)
              CALL KCUBU(KR,JS,JS,NG,NRK,NMB,IG,ICRK,ICSUM,CKK)
              TERM11 = CK1*CKK+TERM11
            ENDDO
            TERM1 = TERM1+( (-1)**(ICRK+ICSUM) )*TERM11
          ENDDO E5
          TERM1 = TERM1*EMRRO(IO,KR,JS)
!
!    DEUXIEME SOMMATION
!
          TERM2 = 0.D0
E6:       DO ICSUM=1,MXSYM
            NSSUM = NSYM1(JI,ICSUM)
            IF( NSSUM                               .EQ. 0 ) CYCLE E6
            IF( CTR(IG,ICSUM,ICRB)*CTR(IGV,IVK,IVB) .EQ. 0 ) CYCLE E6
            TERM22 = 0.D0
            DO NMB=0,NSSUM-1
              CALL KCUBU(1,JS,JI,0,NRK,NMB,IGRV,ICRK,ICSUM,CK1)
              CALL KCUBU(KR,JI,JI,NG,NMB,NRB,IG,ICSUM,ICRB,CKK)
              TERM22 = CK1*CKK+TERM22
            ENDDO
            TERM2 = TERM2+( (-1)**(ICRK+ICSUM) )*TERM22
          ENDDO E6
          TERM2 = TERM2*EMRRO(IO,KR,JI)
!
!    SOMME DES 2 TERMES
!
          COEF = EMRD(IK,IB)/2.D0
          ELM  = (TERM1+TERM2)*COEF
          ELM  = ELM*SQRT(DBLE((2*JI+1)*(2*JS+1)))
!
401       NBELM = NBELM+1
          IF( NBELM .GT. MXELMT ) CALL RESIZE_MXELMT
          LI(NBELM) = IBRA
          KO(NBELM) = IKET
          H(NBELM)  = ELM
        ENDDO E4
      ENDDO E3
!
      RETURN
      END SUBROUTINE CALDI
