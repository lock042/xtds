      PROGRAM ASXEN
!
! 2008 AUGUST
! V.BOUDON, CH.WENGER
!
!  lire un fichier binaire (ex. INFILE) de type XEN_
!  et l'ecrire en ASCII dans INFILE'_ASC'
!
! APPEL : asxen
!
      use mod_dppr
      IMPLICIT NONE

      real(kind=dppr)  :: HD

      integer          :: IC,IP
      integer          :: ICP
      integer          :: J
      integer          :: NFB

      character(len = 150)  :: INFILE
      character(len = 160)  :: OUTFILE
!
1000  FORMAT(A)
1020  FORMAT(/,            &
             'ASXEN  : ')
2000  FORMAT(' <  ',A)
2001  FORMAT(' >> ',A)
7000  FORMAT('ENTER XEN_ TYPE FILE NAME :')
8000  FORMAT(' !!! ASXEN  : STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF FOR ',A)
!
      PRINT 1020
!
!  FICHIER D'ENTREE
!
      PRINT 7000
      READ(*,1000) INFILE
      PRINT 2000,  TRIM(INFILE)
      OPEN(50,FILE=TRIM(INFILE),FORM='UNFORMATTED',STATUS='OLD')
!
!  FICHIER DE SORTIE
!
      OUTFILE = TRIM(INFILE)//'_ASC'
      PRINT 2001,  TRIM(OUTFILE)
      OPEN(10,FILE=TRIM(OUTFILE))
      WRITE(10,*) '>>> J,IC,ICP,NFB'
      WRITE(10,*) '>>> HD(K(IP))'
!
12    READ (50,END=9000) J,IC,ICP,NFB
      WRITE(10,*)        J,IC,ICP,NFB
      DO IP=1,NFB
        READ (50,END=9003) HD
        WRITE(10,*)        HD
      ENDDO
      GOTO 12
!
9003  PRINT 8003, TRIM(INFILE)
      PRINT 8000
!
9000  CLOSE(10)
      PRINT *
      END PROGRAM ASXEN
