      PROGRAM XASG0
!
! READ      standard input
! EXTRACT   lines containing the string at the right position
! SET       units, temperature, raman, polarization
! TRUNCATE  lines
! WRITE OUT ASG_EXP
!
! WARNING : MAX LINE LENGTH (TRUNCATED ON INPUT) IS 152
!
! CALL    : xasg0 'string' Pn Pm [MHz|GHz] [TEMP [pol [Rxxx]] [fpvib FPVIB] [abund ABUND]]
!
!
      use mod_dppr
      use mod_com_fdate
      use mod_com_fp
      IMPLICIT NONE

      real(kind=dppr)  :: TEMPC

      integer          :: ILOW,IRAC,IUPP
      integer          :: LUX
      integer          :: NBEXTR

      character(len =   1)  :: UNIT                                                                ! C,M,G
      character(len =   2)  :: PLOW,PUPP                                                           ! 2 DUE TO MXPOL
      character(len =   4)  :: POLST
      character(len =  30)  :: CARG,CHAINE
      character(len = 152)  :: LIGNE

      logical          :: LTEMP
!
1000  FORMAT(A)
1001  FORMAT(I1)
1002  FORMAT(A,1X,F7.2,1X,I1,1X,A,1P,E12.4,0P,F9.5)
1011  FORMAT(' XASG0  -> NUMBER OF EXTRACTED LINES : ',I7)
2001  FORMAT(' >> ',A)
8000  FORMAT(' !!! XASG0  : STOP ON ERROR')
8001  FORMAT(' !!! ERROR OPENING CONTROL FILE')
8002  FORMAT(' !!! INCOMPLETE CONTROL FILE')
8007  FORMAT(' !!! TEMPERATURE has to be set before ',A,' option')
8008  FORMAT(' !!! ERROR READING INPUT')
8010  FORMAT(' !!! ERROR WRITING OUTPUT FILE')
8012  FORMAT(' !!! INVALID TEMP: ',A)
8013  FORMAT(' !!! INVALID FPVIB: ',A)
8014  FORMAT(' !!! INVALID ABUND: ',A)
8015  FORMAT(' !!! INVALID POLYAD(S): ',A,2X,A)
!
      OPEN(10,FILE=CTRLF,ERR=9998,STATUS='OLD')                                                    ! command line arguments
      READ(10,1000,END=9997)
      READ(10,1000,END=9997) CHAINE
      READ(10,1000,END=9997) PUPP
      READ(10,1000,END=9997) PLOW
      IF( PUPP(1:1) .NE. 'P' .OR.              &
          PLOW(1:1) .NE. 'P'      ) GOTO 9990                                                      ! validity test
      READ (PUPP(2:2),1001,ERR=9989) IUPP                                                          ! validity test
      WRITE(PUPP(2:2),1001)          IUPP
      READ (PLOW(2:2),1001,ERR=9988) ILOW                                                          ! validity test
      WRITE(PLOW(2:2),1001)          ILOW
      UNIT  = 'C'                                                                                  ! default
      TEMPC = 0.D0                                                                                 ! default
      IRAC  = 1                                                                                    ! default
      POLST = 'R111'                                                                               ! default
      FPVIB = 0.D0                                                                                 ! default
      ABUND = 1.D0                                                                                 ! default
      LTEMP = .FALSE.                                                                              ! TEMP NOT already set
!
5     READ(10,1000,END=6) CARG
      IF    ( CARG .EQ. 'MHz'   ) THEN
        UNIT = 'M'
      ELSEIF( CARG .EQ. 'GHz'   ) THEN
        UNIT = 'G'
      ELSEIF( CARG .EQ. 'pol'   ) THEN
        IF( .NOT. LTEMP ) GOTO 9993                                                                ! TEMP first
        IRAC = 0
        READ(10,1000,END=6) CARG
        IF( CARG .EQ. 'R111' .OR.         &
            CARG .EQ. 'R110' .OR.         &
            CARG .EQ. 'R001'      ) THEN
          POLST = TRIM(CARG)
        ELSE
          BACKSPACE(10)
          GOTO 5
        ENDIF
      ELSEIF( CARG .EQ. 'fpvib' ) THEN
        IF( .NOT. LTEMP ) GOTO 9993                                                                ! TEMP first
        READ(10,1000,END=9997) CARG
        READ(CARG,*,ERR=9992) FPVIB
        IF( FPVIB .LE. 0.D0 ) GOTO 9992
      ELSEIF( CARG .EQ. 'abund' ) THEN
        IF( .NOT. LTEMP ) GOTO 9993                                                                ! TEMP first
        READ(10,1000,END=9997) CARG
        READ(CARG,*,ERR=9991) ABUND
        IF( ABUND .LE. 0.D0 .OR.              &
            ABUND .GT. 1.D0      ) GOTO 9991
      ELSE
        READ(CARG,*,ERR=9995) TEMPC
        IF( TEMPC .LE.    0.D0 .OR.              &
            TEMPC .GT. 1000.D0      ) GOTO 9995
        LTEMP = .TRUE.                                                                             ! TEMP set
      ENDIF
      GOTO 5
!
6     CLOSE(10)
      PRINT 2001, 'ASG_EXP'                                                                        ! output file
      LUX = 20
      OPEN(LUX,FILE='ASG_EXP',STATUS='UNKNOWN')
!
! PROCESS
!
      NBEXTR = 0                                                                                   ! nb of extracted lines
!
1     READ(*,1000,ERR=9994,END=2) LIGNE
      IF( LIGNE(74:103) .NE. CHAINE ) GOTO 1                                                       ! right string at right place ?
      LIGNE(1:1) = UNIT                                                                            ! set unit
      LIGNE      = LIGNE(:72)//' '//PUPP//'m'//PLOW                                                ! truncate and format
      WRITE(LUX,1002,ERR=9996) TRIM(LIGNE),TEMPC,IRAC,POLST,FPVIB,ABUND
      NBEXTR = NBEXTR+1
      GOTO 1
!
2     PRINT 1011, NBEXTR                                                                           ! nb of extracted lines
      GOTO 9000
!
9988  PRINT 8015, PLOW
      GOTO  9999
9989  PRINT 8015, PUPP
      GOTO  9999
9990  PRINT 8015, PUPP,PLOW
      GOTO  9999
9991  PRINT 8014, TRIM(CARG)
      GOTO  9999
9992  PRINT 8013, TRIM(CARG)
      GOTO  9999
9993  PRINT 8007, TRIM(CARG)
      GOTO  9999
9994  PRINT 8008
      GOTO  9999
9995  PRINT 8012, TRIM(CARG)
      GOTO  9999
9996  PRINT 8010
      GOTO  9999
9997  PRINT 8002
      CLOSE(10)
      GOTO  9999
9998  PRINT 8001
9999  PRINT 8000
!
9000  CLOSE(LUX)
      END PROGRAM XASG0
