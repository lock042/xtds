      PROGRAM SIMULS
!
!  ADAPTE DE DES_THEO       (TRANSMISSION)
!            DES_THEO_RAMAN (RAMAN)
!            DES_ABS        (ABSORBANCE)
!  POUR SORTIE DANS LE FICHIER DE POINTS simul.xy
!  INFORMATIONS COMPLEMENTAIRES DANS     simul.log
!
!  TRACE DE SPECTRE THEORIQUE PAR FENETRE
!
! APPEL : simul trans|raman|absor|coabs UMIN UMAX T RES PAF SEUIL
!                    LINEFILE1 CDAE RM UK PTTC [PPTC [CLEN]] [MHz|GHz]
!               more LINEFILE2 CDAE RM UK PTTC [PPTC [CLEN]] [MHz|GHz]
!               ............................................................
!               end  [NFAP] [MHz|GHz]
!
! absor : absorbance
! coabs : coefficient d'absorption
!
! UMIN     : frequence mini de la fenetre            (cm-1)
! UMAX     : frequence maxi de la fenetre            (cm-1)
! T        : temperature                             (K)
! RES      : resolution                              (cm-1)
! PAF      : pas de dessin                           (cm-1)
! SEUIL    : seuil d'intensite mini                  (cm-2/atm)
! LINEFILE : fichier de raies                        (FORMAT 2000)
! CDAE     : coefficient de demi auto-elargissement  (cm-1.atm-1)
! RM       : masse molaire
! UK       : facteur multiplicatif d'intensite
! PTTC     : pression totale                         (Torr)
! PPTC     : pression partielle                      (Torr)
! CLEN     : longueur de cuve                        (cm)
! NFAP     : fonction d'appareil                     (dirac|sinc|sinc2|gauss)
!
! PPTC requis pour les options trans,        absor, coabs
! CLEN requis pour les options trans,        absor
! NFAP requis pour les options trans, raman, absor
!
! unite par defaut = cm-1
! peut etre specifique en entree : [MHz|GHz] pour LINEFILE*
! peut etre different  en sortie : [MHz|GHz] pour 'end'
! unite de UMIN, UMAX, RES et PAF = unite en sortie.
!
!  MXNBES   = NB MAXI D'ELEMENTS SPECTRAUX DANS LA FENETRE
!  MXNLF    = NB MAXI DE FICHIERS DE RAIES (ASCII SEQ.)
!  MXNPV    = NB MAXI DE PTS DU 1/2 PROF. VOIGT ; CENTRE COMPRIS.
!             DOIT VERIFIER : MXNPV = 101+10*I
!  MXNMI    = NB MAXI DE PTS DE LA FCT D'APP. (2*MXNMI+1)
!  MXNTRA   = NB MAXI DE TRANSITIONS CONCERNEES
!
!
      use mod_dppr
      use mod_par_tds
      use mod_com_const
      use mod_com_fdate
      use mod_main_simul
      IMPLICIT NONE

! functions
      real(kind=dppr)  :: YYCAL

      real(kind=dppr)  :: CMGHZW,CMID,CPAS
      real(kind=dppr)  :: FNOE,FREQA
      real(kind=dppr)  :: PAF,PAS,PASG,PDLMH,PDLMHF
      real(kind=dppr)  :: Q1,Q2,Q3
      real(kind=dppr)  :: REFL,REFLC,RES,RISC
      real(kind=dppr)  :: SD,SEUIL,SGP,SL
      real(kind=dppr)  :: T,TINTA,TJMAX
      real(kind=dppr)  :: UMAX,UMIN
      real(kind=dppr)  :: VCMIN,VCMAX,VDELTA
      real(kind=dppr)  :: WI,WR,WRM
      real(kind=dppr)  :: XLIM,XSDX,XV
      real(kind=dppr)  :: YV,YY

      integer          :: I,ICNUI,IG
      integer          :: J,J1,J2,JMAX
      integer          :: L
      integer          :: NBES,NEC,NFF,NMI,NPVS,NUF,NUI,NUSP

      character(len =   4) :: SMGHZW
      character(len =   5) :: NFAP,NTYP
      character(len =   9) :: PDFORM                                                               ! cf. FORMAT 2003
      character(len =  10) :: CARG
      character(len =  20) :: YYFORM                                                               ! cf. FORMAT 2002
!
1000  FORMAT(A)
1001  FORMAT(/,                        &
             '** Wait , Please **',/)
2000  FORMAT(F12.6,1PE11.4)
2001  FORMAT(F12.6,1X,E13.5E3)
2002  FORMAT(E20.12E3)
2003  FORMAT(F9.1)                                                                                 ! cf. FORMAT 4003
3000  FORMAT('SIMULATION PARAMETERS'                             ,//,   &
             'Frequency Range        :', F8.1,' to ',F8.1,' cm-1', /,   &
             'Temperature            :', F8.1,12X,' K'           , /,   &
             'Resolution             :',F12.5, 8X,' cm-1'        , /,   &
             'Drawing Steps Size     :',F12.5, 8X,' cm-1'        , /,   &
             'Intensity Threshold    :',F11.4, 9X,' cm-2/atm'    , / )
3001  FORMAT(' LINE FILE : ',A)
3002  FORMAT('  Press Broadening Coeff   :',F10.3,6X,' cm-1.atm-1',/,   &
             '  Molar Mass               :', F8.1                 ,/,   &
             '  Intensity scaling factor :',E15.4                 ,/,   &
             '  Total   Pressure         :',F10.3,6X,' Torr'         )
3003  FORMAT('  Partial Pressure         :',F10.3,6X,' Torr')
3004  FORMAT('  Path Length              :', F8.1,8X,' cm')
3005  FORMAT('  Voigt half profil extent :',F11.4,5X,' cm-1',/)
3006  FORMAT(' OUTPUT UNIT               : ',A)
4001  FORMAT('First Line      :',F9.2                        ,/,   &
             'Strongest Line  :',F9.2,'  (',E8.2,' cm-2/atm)',/,   &
             'Last Line       :',F9.2                        ,/ )
4003  FORMAT(/,                                                       &
             'NUMBER OF DRAWING    STEPS  IN A   FWHM   : ',F9.1,/,   &
             'NUMBER OF DRAWING    POINTS IN THE WINDOW : ',I7  ,/,   &
             'NUMBER OF CALCULATED POINTS IN THE WINDOW : ',I7     )
8000  FORMAT(' !!! SIMULS : STOP ON ERROR')
8001  FORMAT(' !!! ERROR OPENING CONTROL FILE')
8002  FORMAT(' !!! INCOMPLETE CONTROL FILE')
8005  FORMAT(' !!! WRONG APPARATUS FUNCTION : ',A)
8006  FORMAT(' !!! SAMPLING TOO SPARSE'                     ,/,   &
             ' !!! NUMBER OF DRAWING STEPS IN A FWHM: ',F9.1,/,   &
             ' !!! MUST BE >=  3.'                             )
8007  FORMAT(/,                                                 &
             ' WARNING: SAMPLING TOO DENSE'               ,/,   &
             '   NUMBER OF DRAWING STEPS IN A FWHM: ',F9.1,/,   &
             '   SHOULD BE <  20.'                           )
8009  FORMAT(' !!! WRONG SPECTRUM TYPE : ',A)
8010  FORMAT(' !!! NO LINES')
8013  FORMAT(' !!! WRONG TAG ON COMMAND LINE: ',A)
8014  FORMAT(' !!! WRONG APPARATUS FUNCTION: ',A)
!
! ARRAY INITIAL ALLOCATION
!
      CALL ALLOC_ALL
!
!  VALEURS INITIALES
!
      NF    = 0                                                                                    ! NB DE FICHIERS DE RAIES
      NTRA  = 0                                                                                    ! NB DE RAIES LUES (+EXTREMITES)
      TRANS = .FALSE.
      RAMAN = .FALSE.
      ABSOR = .FALSE.
      COABS = .FALSE.
!
!  CONSTANTES UTILES
!
      Q1 = SQRT(LOG(2.D0))
      Q2 = 1.D0/SQRT(PI)
!
!  LECTURE DES PARAS D'ENTREE
!
      OPEN(10,FILE=CTRLF,ERR=9998,STATUS='OLD')
!
!  PARAS INDEPENDANTS DES FICHIERS DE RAIES
!
      READ(10,1000,END=9997) FDATE
      READ(10,1000,END=9997) NTYP
      IF    ( NTYP .EQ. 'trans' ) THEN
        TRANS = .TRUE.
      ELSEIF( NTYP .EQ. 'raman' ) THEN
        RAMAN = .TRUE.
      ELSEIF( NTYP .EQ. 'absor' ) THEN
        ABSOR = .TRUE.
      ELSEIF( NTYP .EQ. 'coabs' ) THEN
        COABS = .TRUE.
      ELSE
        PRINT 8009, NTYP
        GOTO  9999
      ENDIF
      READ(10,*,END=9997) UMIN                                                                     ! FREQ.MINI
      READ(10,*,END=9997) UMAX                                                                     ! FREQ.MAXI
      READ(10,*,END=9997) T                                                                        ! TEMPERATURE
      READ(10,*,END=9997) RES                                                                      ! RESOLUTION
      READ(10,*,END=9997) PAF                                                                      ! PAS FINAL (DU TRACE)
      READ(10,*,END=9997) SEUIL                                                                    ! SEUIL DES RAIES THEORIQUES
!
!  LECTURE DES FICHIERS DE RAIES ET DE LEURS PARAS
!
5     NF = NF+1
      IF( NF .GT. MXNLF ) CALL RESIZE_MXNLF
      READ(10,1000,END=9997) NOM(NF)                                                               ! NOM DU FICHIER DE RAIES
      READ(10,*,END=9997) CDAE(NF)
      READ(10,*,END=9997) RM(NF)
      READ(10,*,END=9997) UK(NF)
      READ(10,*,END=9997) PTTC(NF)
      PTA(NF) = PTTC(NF)/760.D0                                                                    ! TORR -> ATM.
      IF( .NOT. RAMAN ) THEN                                                                       ! trans, absor, coabs
        READ(10,*,END=9997) PPTC(NF)
        PPA(NF) = PPTC(NF)/760.D0                                                                  ! TORR -> ATM.
        IF( .NOT. COABS ) THEN                                                                     ! trans, absor
          READ(10,*,END=9997) CLEN(NF)
        ENDIF
      ENDIF
      READ(10,1000,END=9997) CARG                                                                  ! ENCORE? more OU AUTRE(par ex. end)
! unite d'entree
      CMGHZ(NF) = 1.D0                                                                             ! cm-1
      IF    ( CARG .EQ. 'MHz' ) THEN
        CMGHZ(NF) = CMHZ                                                                           ! MHz
        READ(10,1000,END=9997) CARG                                                                ! suivant
      ELSEIF( CARG .EQ. 'GHz' ) THEN
        CMGHZ(NF) = CGHZ                                                                           ! GHz
        READ(10,1000,END=9997) CARG                                                                ! suivant
      ENDIF
      IF( CARG .EQ. 'more' ) GOTO 5
      IF( CARG .NE. 'end'  ) THEN
        PRINT 8013, CARG
        GOTO  9999
      ENDIF
      IF( COABS ) THEN
        NFAP = 'dirac'
      ELSE
        READ(10,1000,END=9997) NFAP                                                                ! FCT D'APP. 5 CARAS
        IF( NFAP .NE. 'dirac' .AND.         &
            NFAP .NE. 'sinc'  .AND.         &
            NFAP .NE. 'sinc2' .AND.         &
            NFAP .NE. 'gauss'       ) THEN
          PRINT 8014, NFAP
          GOTO  9999
        ENDIF
      ENDIF
! unite de sortie
      CMGHZW = 1.D0                                                                                ! cm-1
      SMGHZW = 'cm-1'
      READ(10,1000,END=100) CARG
      IF    ( CARG .EQ. 'MHz' ) THEN
        CMGHZW = CMHZ                                                                              ! MHz
        SMGHZW = 'MHz'
      ELSEIF( CARG .EQ. 'GHz' ) THEN
        CMGHZW = CGHZ                                                                              ! GHz
        SMGHZW = 'GHz'
      ENDIF
!
100   CLOSE(10)
! conversion en cm-1
      UMIN = UMIN/CMGHZW                                                                           ! en cm-1
      UMAX = UMAX/CMGHZW                                                                           ! en cm-1
      RES  = RES/CMGHZW                                                                            ! en cm-1
      PAF  = PAF/CMGHZW                                                                            ! en cm-1
!
! lecture des donnees
      DO I=1,NF
        OPEN(20,FILE=NOM(I),STATUS='OLD')
!
8       CONTINUE
        READ(20,2000,ERR=8,END=10) FREQA,TINTA                                                     ! FREQUENCE,INTENSITE
        IF( FREQA .LT. (UMIN-1.D0) ) GOTO 8                                                        ! 1.CM-1 DE MARGE
        IF( FREQA .GT. (UMAX+1.D0) ) GOTO 8                                                        ! 1.CM-1 DE MARGE
        IF( TINTA .LT. SEUIL       ) GOTO 8                                                        ! intensite sous seuil
        NTRA = NTRA+1                                                                              ! nouvelle transition
        IF( NTRA .GT. MXNTRA ) CALL RESIZE_MXNTRA
        FREQ(NTRA) = FREQA/CMGHZ(I)                                                                ! TABLEAU DES FREQUENCES (cm-1)
        TINT(NTRA) = TINTA                                                                         ! TABLEAU DES INTENSITES
        INC(NTRA)  = I                                                                             ! TABLEAU DES NUMEROS DE FICHIER
        GOTO 8
!
10      CLOSE(20)
      ENDDO
      IF( NTRA .EQ. 0 ) THEN                                                                       ! PAS DE RAIES
        PRINT 8010
        GOTO  9999
      ENDIF
!
! determiner pas du calcul
!
      NUI  = 5                                                                                     ! NB DE PASU PAR INTERVALLE (PAF)
      PASU = PAF/NUI                                                                               ! PAS ULTIME (DU CALCUL)
      IF( PASU .LT. RES*.1D0 ) THEN
        PASU = RES*.1D0                                                                            ! PASU >= RES/10 requis
        NUI  = INT(PAF/PASU)
        IF( MOD(PAF,PASU) .NE. 0.D0 ) NUI = NUI+1
        PASU = PAF/NUI                                                                             ! AJUSTEMENT DE PASU
      ENDIF
!
!  PRISE EN COMPTE DE LA FONCTION D'APPAREIL
!
      NMI = 0                                                                                      ! support de FAP = 2*NMI+1
      IF    ( NFAP .EQ. 'dirac'      ) THEN
        RES  = 0.D0
        PASU = PAF
        NUI  = 1
        GOTO 22
      ELSEIF( NFAP .EQ. 'sinc'  .OR.         &
              NFAP .EQ. 'sinc2'      ) THEN                                                        ! cas de sinc/sinc2
        FNOE = 25.D0                                                                               ! LARGEUR DE SINC/SINC2 (NB DE NOEUDS)
        NMI  = INT(FNOE*RES/PASU)
        DO WHILE( NMI .GT. MXNMI )
          CALL RESIZE_MXNMI
        ENDDO
        IF( NMI .EQ. 0     ) GOTO 22                                                               ! pas de fonction d'appareil
        NEC    = 2*NMI+1                                                                           ! NB TOTAL DE PASU DE LA FCT.APP.
        J1     = NMI+1
        J2     = NMI+2
        FA(J1) = 1.D0
        PAS    = FNOE*PI/NMI
        IF( NFAP .EQ. 'sinc2' ) PAS = PAS*.5D0                                                     ! SINC2 DIFFERENT DE SINC
        CPAS = 0.D0
        DO I=J2,NEC
          CPAS  = CPAS+PAS
          FA(I) = DSIN(CPAS)/CPAS
        ENDDO
        IF( NFAP .EQ. 'sinc2' ) THEN
          DO I=J2,NEC                                                                              ! F=F*F
            FA(I) = FA(I)*FA(I)
          ENDDO
        ENDIF
      ELSEIF( NFAP .EQ. 'gauss'      ) THEN                                                        ! cas de gauss
        XLIM = SQRT(-LOG(1.D-4))*RES/(2.D0*Q1)                                                     ! distance au centre pour intensite a 1.D-4
        NMI  = XLIM/PASU                                                                           ! nb de PASU correspondant
        IF( MOD(XLIM,PASU) .NE. 0.D0 ) NMI = NMI+1
        DO WHILE( NMI .GT. MXNMI )
          CALL RESIZE_MXNMI
        ENDDO
        IF( NMI .EQ. 0     ) GOTO 22                                                               ! pas de fonction d'appareil
        NEC    = 2*NMI+1                                                                           ! NB TOTAL DE PASU DE LA FCT.APP.
        J1     = NMI+1
        J2     = NMI+2
        FA(J1) = 1.D0
        Q3     = -4.D0*LOG(2.D0)
        DO I=J2,NEC
          XSDX  = DBLE(I-J2+1)*PASU/RES
          FA(I) = EXP(Q3*XSDX*XSDX)
        ENDDO
      ELSE                                                                                         ! mauvais NFAP
        PRINT 8005, NFAP
        GOTO  9999
      ENDIF
      RISC = 0.D0                                                                                  ! INTEGRALE PARTIELLE
      DO I=J2,NEC
        RISC = RISC+FA(I)
      ENDDO
      RISC = FA(J1)+2.D0*RISC
      DO I=J1,NEC                                                                                  ! NORMALISATION: somme des points (FA) = 1
        FA(I) = FA(I)/RISC
      ENDDO
      DO I=1,NMI                                                                                   ! TOUT LE PROFIL
        FA(I) = FA(NEC+1-I)
      ENDDO
! mise en ordre des donnees
!
22    NTRA       = NTRA+1
      FREQ(NTRA) = 1.7D+38                                                                         ! FAUSSE RAIE DE FIN DE TABLEAU
      TINT(NTRA) = 0.D0
      INC(NTRA)  = 1
      PRINT 1001
      CALL ORDER(NTRA,FREQ,K,MXNTRA+1)                                                             ! MISE EN ORDRE
! 1ere frequence non nulle
      DO I=1,NTRA-1
        IF( FREQ(K(I)) .NE. 0.D0 ) GOTO 305
      ENDDO
! la plus grande intensite
305   TJMAX = 0.D0
E303: DO J=1,NTRA-1
        IF( TINT(J)*UK(INC(J)) .LT. TJMAX ) CYCLE E303
        TJMAX = TINT(J)*UK(INC(J))
        JMAX  = J
      ENDDO E303
      PRINT 4001, FREQ(K(I)),FREQ(JMAX),TJMAX,FREQ(K(NTRA-1))
!
!  CALCUL DU SPECTRE
!
! determiner VCMIN, VCMAX
      VCMIN  = UMIN                                                                                ! VRAI CMIN
      NFF    = INT((UMAX-VCMIN)/PAF)                                                               ! NB D'INTERV. PAF        DE LA FENETRE
      NUF    = NFF*NUI                                                                             ! NB D'INTERV. PASU       DE LA FENETRE
      NBES   = NFF+1                                                                               ! NB D'ELEMENTS SPECTRAUX DE LA FENETRE
      VDELTA = NFF*PAF                                                                             ! LARGEUR                 DE LA FENETRE
      VCMAX  = VCMIN+VDELTA                                                                        ! VRAI CMAX
! profil de voigt
!
! determination du support pour chaque fichier
      CMID = (UMIN+UMAX)/2.D0                                                                      ! frequence moyenne
      NPVS = 0                                                                                     ! SUPPORT VOIGT LE + GRAND
      DO I=1,NF
        PRINT 3001, TRIM(NOM(I))
        SL = CDAE(I)*PTA(I)                                                                        ! LARGEUR LORENTZ
        SD = 3.581097D-7*SQRT(T/RM(I))*CMID                                                        ! LARGEUR DOPPLER
        IF( RAMAN ) SD = SQRT(SD*SD+RES*RES)
        SGP    = Q1/SD
        YV     = SL*SGP
        NPV(I) = 101
        CALL CPF12(0.D0,YV,WRM,WI)                                                                 ! VALEUR RELATIVE AU CENTRE
!
12      XV = (NPV(I)-1)*PASU*SGP
        CALL CPF12(XV,YV,WR,WI)                                                                    ! VALEUR RELATIVE EN BOUT
        IF( WR .LT. WRM*1.D-4 ) THEN                                                               ! SAUT = 1/10000E DU MAXI
          PRINT 3005, (NPV(I)-1)*PASU
          GOTO  13
        ENDIF
        NPV(I) = NPV(I)+10
        DO WHILE( NPV(I) .GT. MXNPV )
          CALL RESIZE_MXNPV
        ENDDO
        GOTO 12
!
13      IF( NPV(I) .GT. NPVS ) NPVS = NPV(I)
      ENDDO
! calcul du profil pour chaque fichier
      REFL = 1.7D+38                                                                               ! LARGEUR A MI-HAUTEUR de reference
      DO I=1,NF                                                                                    ! PROFILS DE VOIGT
        SL = CDAE(I)*PTA(I)
        SD = 3.581097D-7*SQRT(T/RM(I))*CMID
        IF( RAMAN ) SD = SQRT(SD*SD+RES*RES)
        REFLC = MAX(SL,SD)                                                                         ! REFL pour ce fichier
        IF( REFLC .LT. REFL ) REFL = REFLC                                                         ! garder le plus petit
        SGP  = Q1/SD
        YV   = SL*SGP
        PASG = PASU*SGP
        RISC = 0.D0
        DO L=1,NPV(I)
          J  = L-1
          XV = J*PASG
          CALL CPF12(XV,YV,WR,WI)
          PV(L,I) = WR
          RISC    = RISC+2.D0*PV(L,I)
        ENDDO
        RISC = (RISC-PV(1,I))*PASU                                                                 ! centre compte 2 fois
! normalisation: somme des points (PV) = 1
        DO L=1,NPV(I)
          PV(L,I) = PV(L,I)/RISC
        ENDDO
      ENDDO
      REFL = 2.D0*REFL
      IF( RES  .GT. REFL    .AND.               &
          NFAP .NE. 'dirac'       ) REFL = RES                                                     ! REFL >= RES requis
      PDLMH = REFL/PAF                                                                             ! nb de PAF dans REFL
      PRINT 4003, PDLMH,NBES,NBES*NUI
      WRITE(PDFORM,2003,ERR=46) PDLMH                                                              ! pour rendre coherents test et affichage
      PDLMHF = PDLMH
      READ (PDFORM,2003,ERR=46) PDLMHF
!
46    IF( PDLMHF .LT.  3.D0 ) THEN                                                                 ! cf. FORMAT 8006
        PRINT 8006 ,PDLMH                                                                          ! SAMPLING TOO SPARSE
        GOTO  9999
      ENDIF
      IF( PDLMHF .GE. 20.D0 ) PRINT 8007 ,PDLMH                                                    ! SAMPLING TOO DENSE, cf. FORMAT 8007
      DO WHILE( NBES .GT. MXNBES )
        CALL RESIZE_MXNBES
      ENDDO
      OPEN(11,FILE='simul.log',STATUS='UNKNOWN')
      WRITE(11,3000) UMIN,UMAX,T,RES,PAF,SEUIL
      DO I=1,NF
        WRITE(11,3001) TRIM(NOM(I))
        WRITE(11,3002) CDAE(I),RM(I),UK(I),PTTC(I)
        IF( .NOT. RAMAN ) THEN
          WRITE(11,3003) PPTC(I)
          IF( .NOT. COABS ) THEN
            WRITE(11,3004) CLEN(I)
          ENDIF
        ENDIF
        WRITE(11,3005) (NPV(I)-1)*PASU
      ENDDO
      WRITE(11,3006) SMGHZW
      CLOSE(11)
!
!  FREQUENCES ET INTENSITES
!
! initialisations
      IG = 0                                                                                       ! INDICE DE YCAL
      IF( NMI .NE. 0 ) THEN                                                                        ! appliquer la fonction d'appareil
        DO I=1,NEC                                                                                 ! initialisation du support
          SP(I) = 1.D0
        ENDDO
      ENDIF
      L0   = 1                                                                                     ! 1ERE RAIE A PRENDRE EN COMPTE
      NUSP = -NMI-1                                                                                ! NO DU PASU pour alimenter SP
!                                                                                                  ! commencer a 1/2 FAP avant le 1er point a dessiner
      ICNUI = NUSP-NMI                                                                             ! compteur cyclique (0 -NUI), =0 si on est sur un point a dessiner (PAF)
      XU    = VCMIN+NUSP*PASU                                                                      ! frequence du point courant
      XUMIS = XU-(NPVS-1)*PASU                                                                     ! limite inf du champ maxi
      XUMAS = XU+(NPVS-1)*PASU                                                                     ! limite sup du champ maxi
      DO I=1,NF                                                                                    ! pour chaque fichier
        XUMI(I) = XU-(NPV(I)-1)*PASU                                                               ! limite inf du champ du fichier
        XUMA(I) = XU+(NPV(I)-1)*PASU                                                               ! limite sup du champ du fichier
      ENDDO
!
! boucle des points du spectre
!
! incrementer les compteurs
24    NUSP  = NUSP+1                                                                               ! <NMI avant le 1er point a dessiner
      ICNUI = ICNUI+1                                                                              ! <0   avant le 1er point a dessiner
      IF( ICNUI .EQ. NUI ) ICNUI = 0                                                               ! ICNUI = 0 : on est sur un point a dessiner
      YY = YYCAL()
      IF( NMI .NE. 0 ) THEN                                                                        ! translation de SP
        SP(NEC+1) = YY
        DO I=1,NEC
          SP(I) = SP(I+1)
        ENDDO
      ENDIF
      IF( NUSP .LT. NMI ) GOTO 24                                                                  ! SP pas encore rempli, continuer jusqu'a etre a 1/2 FAP apres
!                                                                                                  ! le 1er point a dessiner,     aller au prochain point de calcul
      IF( ICNUI .NE. 0 ) GOTO 24                                                                   ! pas sur un point a dessiner, aller au prochain point de calcul
! on est sur un point a dessiner
      IF( NMI .NE. 0 ) THEN                                                                        ! appliquer fonction d'appareil
        YY = 0.D0
        DO I=1,NEC
          YY = YY+FA(I)*SP(I)                                                                      ! valide car somme des points (FA) = 1
        ENDDO
      ENDIF
      IF( ABSOR ) THEN
        WRITE(YYFORM,2002,ERR=208) YY                                                              ! reduire le nb de chiffres significatifs pour LOG (pb autour de 1.)
        READ (YYFORM,2002,ERR=208) YY
!
208     YY = -LOG(YY)
      ENDIF
      IG       = IG+1
      YCAL(IG) = YY
      IF( IG .NE. NBES ) GOTO 24                                                                   ! encore des points a dessiner a calculer
! fenetre entierement calculee
!
!  ECRITURE DU FICHIER *.xy
!
      OPEN(10,FILE='simul.xy',STATUS='UNKNOWN')
      DO I=1,NBES
        WRITE(10,2001) (VCMIN+(I-1)*PAF)*CMGHZW,YCAL(I)
      ENDDO
      CLOSE(10)
      GOTO 9000
!
9997  PRINT 8002
      GOTO  9999
9998  PRINT 8001
9999  PRINT 8000
      CLOSE(10)
      CLOSE(11)
      CLOSE(20)
!
9000  PRINT *
      END PROGRAM SIMULS
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
! calculer YY
!
      FUNCTION YYCAL()
      use mod_dppr
      use mod_par_tds
      use mod_com_simul
      IMPLICIT NONE
      real(kind=dppr)  :: YYCAL

      real(kind=dppr)  :: COEF
      real(kind=dppr)  :: FREQC
      real(kind=dppr)  :: PVC
      real(kind=dppr)  :: X
      real(kind=dppr)  :: Y

      integer          :: I,IKL
      integer          :: L,LVO
      integer          :: NFC
!
      XU    = XU+PASU                                                                              ! frequence du point ultime courant
      XUMIS = XUMIS+PASU                                                                           ! limite inf du champ maxi
      XUMAS = XUMAS+PASU                                                                           ! limite sup du champ maxi
      DO I=1,NF                                                                                    ! pour chaque fichier
        XUMI(I) = XUMI(I)+PASU                                                                     ! limite inf du champ du fichier
        XUMA(I) = XUMA(I)+PASU                                                                     ! limite sup du champ du fichier
      ENDDO
! cumuler l'influence de chaque raie
      Y = 0.D0                                                                                     ! intensite (primaire)
!
25    CONTINUE
E27:  DO L=L0,NTRA                                                                                 ! BOUCLE DES RAIES concernees POUR UN POINT (PASU)
        IKL   = K(L)                                                                               ! indice ordonne
        FREQC = FREQ(IKL)                                                                          ! FREQUENCE DE LA RAIE
        IF( FREQC .LT. XUMIS ) THEN                                                                ! raie hors champ maxi (<)
          L0 = L0+1                                                                                ! raie suivante
          GOTO 25                                                                                  ! redemarrer la boucle
        ENDIF
        IF( FREQC .GT. XUMAS ) GOTO 28                                                             ! pas d'autres raies dans le champ maxi (>)
        NFC = INC(IKL)                                                                             ! FICHIER de cette raie
        IF( FREQC .LT. XUMI(NFC) .OR.              &
            FREQC .GT. XUMA(NFC)      ) CYCLE E27                                                  ! raie hors champ du fichier
        X   = ABS(FREQC-XU)                                                                        ! distance a la raie
        LVO = 1+INT(X/PASU)                                                                        ! indice correspondant du profil de voigt
! influence de la raie
        IF    ( RAMAN      ) THEN
          COEF = 1.D0
        ELSEIF( TRANS .OR.         &
                ABSOR      ) THEN
          COEF = PPA(NFC)*CLEN(NFC)
        ELSE                                                                                       ! COABS
          COEF = PPA(NFC)
        ENDIF
        PVC = PV(LVO,NFC)                                                                          ! contribution du profil de Voigt
        IF( LVO .NE. NPV(NFC) ) THEN                                                               ! interpolation
          PVC = PVC+(PV(LVO+1,NFC)-PV(LVO,NFC))*DMOD(X,PASU)/PASU
        ENDIF
        Y = Y+COEF*UK(NFC)*TINT(IKL)*PVC                                                           ! valide car somme des points (PV) = 1
      ENDDO E27
!
! YYCAL : intensite finale
28    IF( TRANS .OR.         &
          ABSOR      ) THEN                                                                        ! VALEUR SANS FCT.APP.
        YYCAL = EXP(-Y)
      ELSE                                                                                         ! RAMAN, COABS
        YYCAL = Y
      ENDIF
!
      RETURN
      END FUNCTION YYCAL
