      PROGRAM LEVLST
!
!   SEPTEMBRE 2005 CW VB
!   mars      2008
!
! ############################################################################
! ##                                                                        ##
! ##  LECTURE DES ENERGIES ET DE LEURS DERIVEES PAR RAPPORT AUX PARAMETRES  ##
! ##                                                                        ##
! ############################################################################
!
!  ******    LIMITATIONS DU PROGRAMME
!
! DIMENSION MAXIMALE D'UN BLOC J,C
!     MAXDIM      !NVCOD:NRCOD:K:T:H:HL:HC:HD:TC:TL:HI
!
! NB MAXIMUM D'OPERATEURS ROVIBRATIONNELS
!     MXOPH      !PARA
!
! NB MAXIMUM D'ELEMENTS MATRICIELS NON NULS D'UN BLOC J,C
!     MAXELM      !LI:KO:EL
!
! VALEUR MAXIMALE DE J
!     MAXJ
!
!
!  APPEL  : levlst Pn Jmin Jmax [assign Pred_mixfile] [comp MH_file] [database SPINX]
!  infile  ->   ED_Pn_
!  outfile -> Ener_Pn
!          -> Ener_assign_Pn        if [assign Pred_mixfile]
!          -> components_Pn         if [comp MH_file]
!  Pred_mixfile may be one of:
!                              eq_tds   prediction_mix.t format
!                              xwf    f_prediction_mix.t format
!  database: to be used with the same option for hdiag
!            Ener_Pn.s added
!
!  Ener[_assign]_Pn file may be visualized by XTDS
!                                               -> Visualize Results
!                                                    -> Ener[_assign]_Pn file
!  with help of components_Pn file.
!
      use mod_dppr
      use mod_par_tds
      use mod_com_fdate
      use mod_com_spin
      use mod_com_sy
      use mod_main_levlst
      IMPLICIT NONE

      real(kind=dppr)  :: GHITRAN,SPINX
      real(kind=dppr)  :: BETA0
      real(kind=dppr)  :: CDERI
      real(kind=dppr)  :: ENERG
      real(kind=dppr)  :: GAMA0
      real(kind=dppr)  :: PI0

      integer          :: CJS,CNSUP
      integer          :: I,IC,ICP,IOP,IP,ISV
      integer          :: J,JMIN,JMAX
      integer          :: LPCMAX
      integer          :: N1,N2,NBFTRC,NBOBS,NBOPH,NBSNV,NFB
      integer          :: NSV,NSVSUP

      character(len = NBCTIT)  :: TITRE
      character(len =   1)  :: CFAS,CPASUP,CPOLS
      character(len =   2)  :: CARG,CSYSUP
      character(len =  10)  :: OPT
      character(len = 100)  :: CCOMP,PCOMP                                                         ! cf. NBVQN
      character(len = 120)  :: MHF,NOM,PREDF

      logical          :: LASSIGN,LCOMP,LXWF,LDB
!
1000  FORMAT(A)
1001  FORMAT(/,              &
             'LEVLST : ',A)
1009  FORMAT(3(1X,E18.11),4X,'(BETA0,GAMA0,PI0)')                                                  ! cf. 1002 de iopba
1010  FORMAT(I4,A)
1011  FORMAT(1X,A,3X,I3,1X,2A,I3,1X,F15.6)
1012  FORMAT((10X,20(2I4,'%')))
1014  FORMAT(I4,2X,I4)
2000  FORMAT(' <  ',A)
2001  FORMAT(' >> ',A)
2005  FORMAT(1X,A,61X,I3,1X,2A,I3)                                                                 ! cf. 2005 de xwf et eq_tds
2101  FORMAT(/,   &                                                                                ! cf. 2100 et 2101 de xwf
             I4)
2103  FORMAT(I4,/)                                                                                 ! cf. 2103 de xwf
2106  FORMAT(/,       &                                                                            ! cf. 2106 de xwf
             1X,A,/)
2109  FORMAT(I8,/)                                                                                 ! cf. 2109 de xwf
8000  FORMAT(' !!! LEVLST : STOP ON ERROR')
8001  FORMAT(' !!! ERROR OPENING CONTROL FILE')
8002  FORMAT(' !!! INCOMPLETE CONTROL FILE')
8003  FORMAT(' !!! ERROR OPENING UPPER EIGENVALUES AND EIGENVECTORS FILE')
8004  FORMAT(' !!! ERROR OPENING OUTPUT FILE')
8006  FORMAT(' !!! PREDICTION FILE NAME MISSING AFTER assign OPTION')
8007  FORMAT(' !!! ERROR OPENING PREDICTION FILE: ',A)
8008  FORMAT(' !!! BAD OPTION IN CONTROL FILE: ',A)
8009  FORMAT(' !!! MH_ FILE NAME MISSING AFTER comp OPTION')
8010  FORMAT(' !!! ERROR OPENING MH_ FILE: ',A)
8011  FORMAT(' !!! ERROR OPENING COMPONENT FILE: ',A)
8012  FORMAT(' !!! ERROR READING MH_ FILE: ',A)
8013  FORMAT(' !!! ERROR READING SPINX')
8014  FORMAT(' !!! ERROR OPENING XSAMS FILE: ',A)
!
! ARRAY INITIAL ALLOCATION
!
      CALL ALLOC_ALL
!
      OPEN(10,FILE=CTRLF,ERR=9998,STATUS='OLD')
      READ(10,1000,END=9997) FDATE
      PRINT 1001,            FDATE
      READ(10,1000,END=9997) CARG
      NOM = 'ED_P'//CARG(2:2)//'_'
      PRINT 2000,           TRIM(NOM)
      OPEN(50,ERR=9996,FILE=TRIM(NOM),STATUS='OLD',FORM='UNFORMATTED')                             ! FICHIER D'ENERGIES
      CALL INBGP(50,BETA0,GAMA0,PI0)
      READ(10,*,END=9997) JMIN
      READ(10,*,END=9997) JMAX
      LASSIGN = .FALSE.                                                                            ! PAS D'ATTRIBUTIONS
      NOM     = 'Ener_P'//CARG(2:2)
      LCOMP   = .FALSE.                                                                            ! ne pas creer de fichier de composantes
      LDB     = .FALSE.                                                                            ! ne pas creer de fichier pour XSAMS
!
10    READ(10,1000,END=5) OPT
      IF    ( OPT .EQ. 'comp'     ) THEN
        LCOMP = .TRUE.                                                                             ! creer un fichier de composantes
        READ(10,1000,END=9990) MHF
        GOTO 10                                                                                    ! prochaine option
      ELSEIF( OPT .EQ. 'assign'   ) THEN
        LASSIGN = .TRUE.                                                                           ! CONSIDERER LES ATTRIBUTIONS
        READ(10,1000,END=9992) PREDF
        PRINT 2000,           TRIM(PREDF)
        OPEN(30,ERR=9994,FILE=TRIM(PREDF),STATUS='OLD')                                            ! FICHIER DE PREDICTIONS
        NOM  = 'Ener_assign_P'//CARG(2:2)
        LXWF = .FALSE.                                                                             ! NOT xwf f_prediction_mix.t format
        READ(30,1000) TITRE
! CHECK FOR LXWF
        IF( TITRE(1:1) .EQ. 'P' ) THEN                                                             ! polyad -> xwf f_prediction_mix.t format
          LXWF = .TRUE.
!
110       READ(30,2101) NSVSUP
          DO I=1,NSVSUP+3                                                                          ! 3 = cf. 2102 de xwf
            READ(30,1000)
          ENDDO
          READ(30,1000) TITRE
          IF( TITRE(1:1) .EQ. 'P' ) GOTO 110                                                       ! an other polyad
          BACKSPACE(30)
        ELSE                                                                                       ! eq_tds prediction_mix.t format
          REWIND(30)
          READ(30,1010) NSVSUP
          DO I=1,NSVSUP
            READ(30,1000)
          ENDDO
          READ(30,1010) LPCMAX
          READ(30,1000)
        ENDIF
! INITIALIZE
        NBOBS = 0
! READ ASSIGNMENTS
12      IF( LXWF ) THEN
          READ(30,2106,END=11) CPOLS                                                               ! upper polyad number for this transition
          READ(30,2109) NBFTRC
          IF( NBFTRC .EQ. 0 ) GOTO 12                                                              ! next transition
          READ(30,2103) LPCMAX
          IF( CPOLS .NE. CARG(2:2) ) THEN                                                          ! not the right polyad
!
13          READ(30,1000,END=11) TITRE                                                             ! skip assignments, cf. 2005 et 2006 de xwf
            IF( TITRE .NE. '' ) GOTO 13
            BACKSPACE(30)
            GOTO 12                                                                                ! next transition
          ENDIF
        ENDIF
!
7       IF( LXWF ) THEN
          READ(30,1000,END=11) TITRE
          IF( TITRE .EQ. '' ) THEN
            BACKSPACE(30)
            GOTO 12                                                                                ! next transition
          ENDIF
          READ(TITRE,2005)     CFAS,CJS,CSYSUP,CPASUP,CNSUP
        ELSE
          READ(30,2005,END=11) CFAS,CJS,CSYSUP,CPASUP,CNSUP
        ENDIF
        DO I=1,(LPCMAX+19)/20                                                                      ! cf. 2006 de xwf
          READ(30,1000)
        ENDDO
        IF( CFAS .EQ. '-' ) GOTO 7                                                                 ! NON ATTRIBUEE
        NBOBS = NBOBS+1
        IF( NBOBS .GT. MXOBS ) CALL RESIZE_MXOBS
        FAS(NBOBS)   = CFAS
        JS(NBOBS)    = CJS
        SYSUP(NBOBS) = CSYSUP
        PASUP(NBOBS) = CPASUP
        NSUP(NBOBS)  = CNSUP
        GOTO 7                                                                                     ! continuer la lecture
!
11      CLOSE(30)                                                                                  ! fichier d attribution lu
        GOTO 10                                                                                    ! prochaine option
      ELSEIF( OPT .EQ. 'database' ) THEN                                                           ! creer un fichier pour XSAMS
        LDB = .TRUE.
        READ(10,*,ERR=9985) SPINX
        READ(50)
        READ(50) TITRE
        READ(TITRE,*) SPIN
        REWIND(50)
        OPEN(61,ERR=9986,FILE=TRIM(NOM)//'.s',FORM='UNFORMATTED',STATUS='UNKNOWN')
      ELSE
        GOTO 9991
      ENDIF
!
5     OPEN(60,ERR=9995,FILE=TRIM(NOM),STATUS='UNKNOWN')                                            ! FICHIER DE SORTIE
      PRINT 2001, TRIM(NOM)
      CLOSE(10)
!
! *** LECTURE DES PARAMETRES DE H
!
      CALL IOPBA(50,60)
      WRITE(60,1009)      BETA0,GAMA0,PI0
      IF( LDB ) WRITE(61) BETA0,GAMA0,PI0
      READ (50)           NSV,TITRE
      WRITE(60,1010)      NSV,TITRE
      IF( LDB ) WRITE(61) NSV
      DO WHILE( NSV .GT. MXSNV )
        CALL RESIZE_MXSNV
      ENDDO
      DO ISV=1,NSV
        READ(50)
      ENDDO
      READ(50) NBOPH
!
! ***  LECTURE ET ECRITURE
!
2     READ(50,END=4) J,IC,ICP,NFB
      IF( J   .LT. JMIN .OR.           &
          NFB .EQ. 0         ) GOTO 2
      IF( J   .GT. JMAX      ) GOTO 4
      DO IP=1,NFB
        IF( LDB ) THEN
          READ(50) ENERG,(CDERI,IOP=1,NBOPH),(PCENT(ISV),ISV=1,NSV)
          DO ISV=1,NSV
            ICENT(ISV) = 100.D0*PCENT(ISV)+0.5D0
          ENDDO
          CALL ORDER(NSV,PCENT,KCENT,MXSNV)
        ELSE
          READ(50) ENERG,(CDERI,IOP=1,NBOPH),(ICENT(ISV),ISV=1,NSV)
          CALL IRDER(NSV,ICENT,KCENT,MXSNV)
        ENDIF
        CFAS = '-'
        IF( LASSIGN ) THEN
          DO I=1,NBOBS
            IF( JS(I)    .EQ. J        .AND.         &
                SYSUP(I) .EQ. SYM(IC)  .AND.         &
                PASUP(I) .EQ. PAR(ICP) .AND.         &
                NSUP(I)  .EQ. IP             ) THEN
              CFAS = FAS(I)
              GOTO 9
            ENDIF
          ENDDO
!
9         CONTINUE
        ENDIF
        WRITE(60,1011) CFAS,J,SYM(IC),PAR(ICP),IP,ENERG
        WRITE(60,1012) (KCENT(ISV),ICENT(KCENT(ISV)),ISV=NSV,1,-1)
        IF( LDB ) THEN
          WRITE(61) CFAS,J,SYM(IC),PAR(ICP),IP,ENERG
          WRITE(61) (KCENT(ISV),REAL(PCENT(KCENT(ISV))),ISV=NSV,1,-1)
          GHITRAN = ( (2.D0*SPINX+1.D0)**2 )*(2.D0*J+1.D0)*SPIN(IC)
          WRITE(61) INT(GHITRAN)
        ENDIF
      ENDDO
      GOTO 2
!
4     CLOSE(60)                                                                                    ! FIN
      IF( LDB ) CLOSE(61)
      IF( LCOMP ) THEN
        PRINT 2000,           TRIM(MHF)
        OPEN(20,ERR=9989,FILE=TRIM(MHF),STATUS='OLD')                                              ! fichier FEMRV
        NOM = 'components_P'//CARG(2:2)
        OPEN(60,ERR=9988,FILE=TRIM(NOM),STATUS='UNKNOWN')                                          ! fichier de composantes
!
112     READ(20,1000,ERR=9987,END=113) TITRE
        IF( TITRE(5:30) .NE. ' VIBRATIONAL SUBLEVEL(S) :' ) GOTO 112
        READ(20,1000)
        READ(TITRE,1010) NBSNV                                                                     ! nb de sous-niveaux vibrationnels
        PCOMP = ''                                                                                 ! precedente composante
        N1    = 0                                                                                  ! 1er  snv de la composante
        N2    = 0                                                                                  ! 2eme snv de la composante
E14:    DO I=1,NBSNV                                                                               ! pour chaque sous-niveau
          READ(20,1000,ERR=9987) TITRE
          CCOMP        = ''                                                                        ! composante courante
          CCOMP(1:1)   = TITRE(12:12)
          CCOMP(2:2)   = TITRE(19:19)
          CCOMP(3:3)   = TITRE(26:26)
          CCOMP(4:4)   = TITRE(33:33)
          CCOMP(5:5)   = TITRE(40:40)
          CCOMP(6:6)   = TITRE(47:47)
          CCOMP(7:7)   = TITRE(54:54)
          CCOMP(8:8)   = TITRE(61:61)
          CCOMP(9:9)   = TITRE(68:68)
          CCOMP(10:10) = TITRE(75:75)
          CCOMP(11:11) = TITRE(82:82)
          CCOMP(12:12) = TITRE(89:89)
          IF( CCOMP .NE. PCOMP ) THEN
            PCOMP = CCOMP
            IF( N1 .EQ. 0 ) THEN
              N1 = 1
              CYCLE E14
            ELSE
              N2 = I-1
              WRITE(60,1014) N1,N2
              N1 = N2+1
            ENDIF
          ENDIF
        ENDDO E14
        N2 = NBSNV
        WRITE(60,1014) N1,N2
!
113     CLOSE(60)
        CLOSE(20)
      ENDIF
      GOTO 9000
!
9985  PRINT 8013
      GOTO  9999
9986  PRINT 8014, TRIM(NOM)//'.s'
      GOTO  9999
9987  PRINT 8012, MHF
      GOTO  9999
9988  PRINT 8011, NOM
      GOTO  9999
9989  PRINT 8010, MHF
      GOTO  9999
9990  PRINT 8009
      GOTO  9999
9991  PRINT 8008, OPT
      GOTO  9999
9992  PRINT 8006
      GOTO  9999
9994  PRINT 8007, PREDF
      GOTO  9999
9995  PRINT 8004
      GOTO  9999
9996  PRINT 8003
      GOTO  9999
9997  PRINT 8002
      CLOSE(10)
      GOTO  9999
9998  PRINT 8001
9999  PRINT 8000
!
9000  PRINT *
      END PROGRAM LEVLST
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! ++  LECTURE ET RECOPIE DES PARAMETRES HAMILTONIEN OU DE TRANSITION  ++
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
      SUBROUTINE IOPBA(LUI,LUO)
      use mod_dppr
      use mod_par_tds
      IMPLICIT NONE
      integer          :: LUI,LUO

      real(kind=dppr)  :: PARA,PREC

      integer          :: I,IP
      integer          :: NBOPH

      character(len = NBCLAB+10)  :: CHAINE
      character(len = NBCTIT)     :: TITRE
!
1000  FORMAT(A)
1001  FORMAT(I4,A)
1002  FORMAT(A,E18.11,E14.7)
8000  FORMAT(' !!! LEVLST : STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF IN IOPBA')
!
      DO I=1,4
        READ (LUI,END=2000) TITRE
        WRITE(LUO,1000)     TITRE
      ENDDO
      READ (LUI,END=2000) NBOPH,TITRE
      WRITE(LUO,1001)     NBOPH,TITRE
      DO I=1,2
        READ (LUI,END=2000) TITRE
        WRITE(LUO,1000)     TITRE
      ENDDO
      DO IP=1,NBOPH
        READ (LUI,END=2000) CHAINE,PARA,PREC
        WRITE(LUO,1002)     CHAINE,PARA,PREC
      ENDDO
      RETURN
!
2000  PRINT 8003
      PRINT 8000
      PRINT *
      STOP
!
      END SUBROUTINE IOPBA
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! ++  DETERMINATION de BETA0, GAMA0 et PI0
! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!
      SUBROUTINE INBGP(LUI,BETA0,GAMA0,PI0)
      use mod_dppr
      use mod_par_tds
      IMPLICIT NONE
      real(kind=dppr)  :: BETA0,GAMA0,PI0
      integer          :: LUI

      real(kind=dppr) ,dimension(3)  :: VAR
      real(kind=dppr)  :: PARA,PREC

      integer          :: I,I1,I2,IP
      integer          :: J
      integer          :: NBOPH

      character(len = NBCLAB+10) :: CHAINE
      character(len = NBCTIT)    :: TITRE
!
8000  FORMAT(' !!! LEVLST : STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF IN INBGP')
!
      DO I=1,4
        READ(LUI,END=2000) TITRE
      ENDDO
      READ(LUI,END=2000) NBOPH,TITRE
      DO I=1,2
        READ(LUI,END=2000) TITRE
      ENDDO
      READ(LUI,END=2000) CHAINE
      TITRE = ' '
      DO I=1,NBVQN
        TITRE = TRIM(TITRE)//'0'                                                                   ! la chaine de 0
      ENDDO
      I1 = INDEX(CHAINE,TITRE(:NBVQN))                                                             ! ou la chercher
      I2 = INDEX(CHAINE(I1+NBVQN:),TITRE(:NBVQN))
      I2 = I1+NBVQN-1+I2
      BACKSPACE(LUI)
      J = 0
      DO IP=1,NBOPH                                                                                ! trouver les 3
        READ(LUI,END=2000) CHAINE,PARA,PREC
        IF( CHAINE(9:9)           .EQ. '0'           .AND.         &
            CHAINE(I1:I1+NBVQN-1) .EQ. TITRE(:NBVQN) .AND.         &
            CHAINE(I2:I2+NBVQN-1) .EQ. TITRE(:NBVQN)       ) THEN
          J      = J+1
          VAR(J) = PARA
          IF( J .EQ. 3 ) GOTO 101
        ENDIF
      ENDDO
      DO I=J+1,3                                                                                   ! mettre a 0 les absents
        VAR(I) = 0
      ENDDO
!
101   BETA0 =  VAR(1)
      GAMA0 = -VAR(2)
      PI0   =  VAR(3)
      REWIND(LUI)
      RETURN
!
2000  PRINT 8003
      PRINT 8000
      PRINT *
      STOP
!
      END SUBROUTINE INBGP
