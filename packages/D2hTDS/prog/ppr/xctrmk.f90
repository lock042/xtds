      PROGRAM XCTRMK
!
! 2008 JUL
! V. BOUDON, Ch. WENGER
!
! PARAMETER CONTROL FILE CREATION
! BASED UPON TRANSITION PARAMETER FILES
!
! CALL  : xctrmk trans  P*mP* para_file [Dp [pol]]
!                ...    ...   ...
!                trans  P*mP* para_file [Dp [pol]]
!                end    para_control
!
! command line directly related to xpafit command line
! WARNING : 'polyad', 'trans' AND 'end' ARE NOT ALLOWED FOR para_file NAME.
!
      use mod_dppr
      use mod_par_tds
      use mod_com_fdate
      use mod_main_xctrmk
      IMPLICIT NONE

      real(kind=dppr)  :: PARAC,PREC

      integer          :: I,IDEB,IDELC,IDELTA,IPI,IPS,IRA,IRAC
      integer          :: ITR,ITRT
      integer          :: KTRTC
      integer          :: LTR
      integer          :: N,NBH,NBOPHC,NBOPTC

      character(len = NBCLAB+10)  :: COPAJC
      character(len = 120)  :: CARG                                                                ! 120 due to para_file
      character(len = 120)  :: FCLX

      logical          :: LHPF,LTPF
!
1000  FORMAT(A)
1002  FORMAT(A,E18.11,E14.7)
1020  FORMAT(/,              &
             'XCTRMK : ',A)
2000  FORMAT(' <  ',A)
2001  FORMAT(' >> ',A)
3001  FORMAT(I1)
3004  FORMAT(////,   &
             I4,//)
3010  FORMAT(I10)
8000  FORMAT(' !!! XCTRMK : STOP ON ERROR')
8001  FORMAT(' !!! ERROR OPENING CONTROL FILE')
8002  FORMAT(' !!! UNEXPECTED EOF OF CONTROL FILE')
8009  FORMAT(' !!! BAD TAG IN CONTROL FILE'                      ,/,   &
             '     polyad, trans OR end EXPECTED, BUT FOUND : ',A   )
8010  FORMAT(' !!! BAD DEVELOPMENT ORDER: ',A)
8011  FORMAT(' !!! ERROR OPENING PARAMETER CONTROL FILE : ',A)
8012  FORMAT(' !!! ERROR OPENING PARAMETER FILE : ',A)
8013  FORMAT(' !!! ERROR READING PARAMETER FILE : ',A)
8014  FORMAT(' !!! UNEXPECTED EOF IN PARAMETER FILE : ',A)
8016  FORMAT(' !!! NO PARAMETER FILE DEFINED')
8017  FORMAT(' !!! NO TRANSITION DEFINED')
8200  FORMAT(' !!! TRANSITIONS HAVE TO BE DECLARED IN'      ,/,   &
             ' !!! LOWER THEN UPPER POLYAD INCREASING ORDER'   )
8204  FORMAT(' !!! SAME P*mP* TRANSITIONS MUST HAVE SAME DEVELOPPMENT ORDER')
8208  FORMAT(' !!! INCONSISTENT HAMILTONIAN PARAMETERS :',/,   &
             '     ',A,E18.11,' : ',A                    ,/,   &
             '     ',A,E18.11,' : ',A                       )
8209  FORMAT(' !!! INCONSISTENT TRANSITION  PARAMETERS :',/,   &
             '     ',A,E18.11,' : ',A                    ,/,   &
             '     ',A,E18.11,' : ',A                       )
8215  FORMAT(' !!! ALL PARAMETER FILES MUST BE OF THE SAME TYPE:',/,   &
             '     H or T'                                          )
8216  FORMAT(' !!! A SINGLE H PARAMETER FILE IS REQUIRED')
8300  FORMAT(' !!! PARAMETER FILE MISSING OR INVALID NAME',/,   &
             '     polyad, trans OR end : ',A                )
!
! ARRAY INITIAL ALLOCATION
!
      CALL ALLOC_ALL
!
      NCL1 = NBCLAB+3
!
! ***************************************************
! *** GET PROGRAM ARGUMENTS
! ***************************************************
!
      OPEN(10,FILE=CTRLF,ERR=9998,STATUS='OLD')                                                    ! command line arguments
      READ(10,1000,END=9997) FDATE
      PRINT 1020,            FDATE
      NTR = 0                                                                                      ! number of transitions
!
1     READ(10,1000,END=9997) CARG                                                                  ! read an argument
      IF    ( CARG .EQ. 'trans' ) THEN
        NTR = NTR+1                                                                                ! one more transition
        IF( NTR .GT. MXATR ) CALL RESIZE_MXATR
        READ(10,1000,END=9997) CARG
        READ(CARG(2:2),3001,ERR=9997) INPS(NTR)
        READ(CARG(5:5),3001,ERR=9997) INPI(NTR)
!      ---> IRA = 1 IF INFRARED ; IRA = 0 IF ISOTROPIC RAMAN
        CDVO(NTR)  = '-'                                                                           ! default = H only, development order only for Ts
        FPARA(NTR) = ''                                                                            ! default = no para_file
        IRAC       = 1                                                                             ! default = infrared
        READ(10,1000,END=9997) CARG
        IF( CARG .EQ. 'polyad' .OR.         &
            CARG .EQ. 'trans'  .OR.         &
            CARG .EQ. 'end'         ) THEN
          PRINT 8300, TRIM(CARG)
          GOTO  9999
        ELSE                                                                                       ! more arguments for this 'trans'
          FPARA(NTR) = CARG                                                                        ! para_file
          READ(10,1000,END=9997) CARG
          IF( CARG(1:1) .EQ. 'D' ) THEN                                                            ! development order
            CDVO(NTR) = CARG(2:)
            READ(CDVO(NTR),3010,ERR=9996) N                                                        ! must be integer
            READ(10,1000,END=9997) CARG
            IF( CARG .EQ. 'pol' )  THEN                                                            ! isotropic raman
              IRAC = 0
            ELSE                                                                                   ! CARG was not 'pol'
              BACKSPACE(10)                                                                        ! later processed
            ENDIF
          ELSE                                                                                     ! CARG was not 'D...'
            BACKSPACE(10)                                                                          ! later processed
          ENDIF
        ENDIF
        IF( IRAC .EQ. 0 ) THEN
          ISRAM(NTR) = .TRUE.                                                                      ! is     raman
        ELSE
          ISRAM(NTR) = .FALSE.                                                                     ! is not raman
        ENDIF
        GOTO 1                                                                                     ! next argument
      ELSEIF( CARG .EQ. 'end'   ) THEN                                                             ! last tag
        GOTO 25
      ELSE                                                                                         ! bad tag
        PRINT 8009, CARG
        GOTO  9999
      ENDIF
!
25    IF( NTR .EQ. 0 ) THEN
        PRINT 8017
        GOTO  9999
      ENDIF
! CHECK TRANSITIONS CONSISTENCY
      LHPF = .FALSE.                                                                               ! there is NO H parameter file
      LTPF = .FALSE.                                                                               ! there is NO T parameter file
E500: DO ITR=1,NTR
        IF( FPARA(ITR) .EQ. ''  ) CYCLE E500                                                       ! no related parameter file
        IF( CDVO(ITR)  .EQ. '-' ) THEN
          LHPF = .TRUE.                                                                            ! some H
        ELSE
          LTPF = .TRUE.                                                                            ! some T
        ENDIF
        IF( LHPF .AND. LTPF ) THEN                                                                 ! both, not allowed
          PRINT 8215
          GOTO  9999
        ENDIF
      ENDDO E500
      IF( NTR .GT. 1 ) THEN                                                                        ! transitions to compare with
        DO ITR=1,NTR-1                                                                             ! each transition except last one
          DO LTR=ITR+1,NTR                                                                         ! all remaining transitions
            IF( INPS(ITR) .EQ. INPS(LTR) .AND.         &
                INPI(ITR) .EQ. INPI(LTR)       ) THEN                                              ! same P*mP*
              IF( CDVO(ITR) .NE. CDVO(LTR) ) THEN                                                  ! different CDVO
                PRINT 8204                                                                         ! not allowed
                GOTO  9999
              ENDIF
            ENDIF
          ENDDO
        ENDDO
        DO ITR=2,NTR
          IF(  INPI(ITR) .LT. INPI(ITR-1)        .OR.         &                                    ! increasing order Pinf then Psup required
              (INPI(ITR) .EQ. INPI(ITR-1) .AND.               &
               INPS(ITR) .LT. INPS(ITR-1)      )      ) THEN
            PRINT 8200
            GOTO  9999
          ENDIF
        ENDDO
      ENDIF
! LAST ARGUMENTS
      READ(10,1000,END=9997) FCLX                                                                  ! parameter control file
      CLOSE(10)
!
! READ PARAMETER FILES
!
! ONLY ONE H PARAMETER FILE ALLOWED
      IF( LHPF ) THEN
        NBH = 0                                                                                    ! nb of declared parameter files
E501:   DO ITR=1,NTR
          FPARAC = FPARA(ITR)
          IF( FPARAC .EQ. '' ) CYCLE E501                                                          ! no related parameter file
          NBH = NBH+1
        ENDDO E501
        IF( NBH .NE. 1 ) THEN
          PRINT 8216
          GOTO  9999
        ENDIF
      ENDIF
!
! SET NBOHT(0) (H) AND THE RELATED PARAMETER FILE
! 1ST FILE WITH THE GREATER NBOHC
!
      NBOHT(0) = 0                                                                                 ! nb of hamiltonian operators
      ITRHT(0) = 0                                                                                 ! index of related transition
E350: DO ITR=1,NTR                                                                                 ! for each transition
        FPARAC = FPARA(ITR)
        IF( FPARAC .EQ. '' ) CYCLE E350                                                            ! no related parameter file
        OPEN(70,ERR=9994,FILE=TRIM(FPARAC),STATUS='OLD')
        READ(70,3004,ERR=9993) NBOPHC
        IF( NBOPHC .GT. NBOHT(0) ) THEN                                                            ! greater NBOPHC file
          NBOHT(0) = NBOPHC
          ITRHT(0) = ITR                                                                           ! the transition referencing H
        ENDIF
        CLOSE(70)
      ENDDO E350
      IF( NBOHT(0) .EQ. 0 ) THEN                                                                   ! parameter file not found
        PRINT 8016
        GOTO  9999
      ENDIF
! READ H PARAMETERS
      FPARAC = FPARA(ITRHT(0))                                                                     ! related parameter file
      PRINT 2000,           TRIM(FPARAC)
      OPEN(70,ERR=9994,FILE=TRIM(FPARAC),STATUS='OLD')
      CALL IOPH(70)                                                                                ! read H parameters
      CLOSE(70)
! CHECK H PARAMETERS CONSISTENCY FOR ALL PARAMETER FILES
E351: DO ITR=1,NTR
        IF( ITR .EQ. ITRHT(0) ) CYCLE E351                                                         ! no self test
        FPARAC = FPARA(ITR)                                                                        ! current parameter file
        IF( FPARAC .EQ. '' ) CYCLE E351                                                            ! no parameter file
        OPEN(70,ERR=9994,FILE=TRIM(FPARAC),STATUS='OLD')
        READ(70,3004,ERR=9993) NBOPHC                                                              ! current nb of H operators
        DO I=1,NBOPHC                                                                              ! for each operator
          READ(70,1002,END=9992) COPAJC,PARAC,PREC
! COPAJ (OPERATOR DEFINITION) AND PARAO (ORIGINAL PARAMETER VALUE) SET IN IOPH
          IF( COPAJC .NE. COPAJ(I) .OR.         &                                                  ! test definition
              PARAC  .NE. PARAO(I)      ) THEN                                                     ! and  value
            PRINT 8208, COPAJC,PARAC,          &                                                   ! not consistent
                        TRIM(FPARAC),          &
                        COPAJ(I),PARAO(I),     &
                        TRIM(FPARA(ITRHT(0)))
            GOTO  9999
          ENDIF
        ENDDO
        CLOSE(70)
      ENDDO E351
! SET TYPE OF EACH TRANSITION
! A TYPE IS UNIQUE AND DEPENDS ON DELTA AND ISRAM
! DELTA = i-j FOR PimPj TRANSITION
! ISRAM = RAMAN (0) OR NOT (1)
!
! TYPE IS 0 FOR H, NOT NULL (1,NTRT) FOR Ts
      DO ITR=1,NTR                                                                                 ! for each transition
        IF( CDVO(ITR) .EQ. '-' ) THEN                                                              ! means H
          KTRT(ITR)  = -1                                                                          ! dummy type
          INTRT(ITR) =  0                                                                          ! dummy index of transition type
        ELSE
          IPS   = INPS(ITR)                                                                        ! T case
          IPI   = INPI(ITR)
          IDELC = IPS-IPI                                                                          ! current DELTA (0,MXPOL-1)
          IF( ISRAM(ITR) ) THEN                                                                    ! set current IRA (0,1)
            IRAC = 0
          ELSE
            IRAC = 1
          ENDIF
          KTRT(ITR) = IDELC*10+IRAC                                                                ! type code (0->(MXPOL-1)*10+1)
        ENDIF
      ENDDO
! SET THE NUMBER OF DIFFERENT TYPES
! AND SET THE UNIQUE TYPE # FOR EACH TRANSITION
      NTRT = 0                                                                                     ! nb of transition types
      DO IDELTA=0,MXPOL-1
E355:   DO IRA=0,1                                                                                 ! 0 if ISRAM=TRUE
          KTRTC = IDELTA*10+IRA                                                                    ! type code
          DO ITR=1,NTR                                                                             ! for each transition
            IF( KTRTC .EQ. KTRT(ITR) ) THEN                                                        ! new type
              NTRT = NTRT+1                                                                        ! nb of unique types
              IF( NTRT .GT. MXATRT ) CALL RESIZE_MXATRT
              DO LTR=1,NTR
                IF( KTRTC .EQ. KTRT(LTR) ) THEN
                  INTRT(LTR) = NTRT                                                                ! type # of this transition
                ENDIF
              ENDDO
              CYCLE E355                                                                           ! next possible type
            ENDIF
          ENDDO
        ENDDO E355
      ENDDO
!
! SET NBOHT() FOR EACH (T) TYPE
      DO ITRT=1,NTRT                                                                               ! Ts
        NBOHT(ITRT) = 0
        ITRHT(ITRT) = 0
E359:   DO ITR=1,NTR                                                                               ! for each transition
          IF( ITRT .NE. INTRT(ITR) ) CYCLE E359                                                    ! not of this type
          FPARAC = FPARA(ITR)                                                                      ! para_file necesseraly present (T)
          OPEN(70,ERR=9994,FILE=TRIM(FPARAC),STATUS='OLD')
          READ(70,3004,ERR=9993) NBOPHC                                                            ! current nb of H operators
          DO I=1,NBOPHC                                                                            ! skip  H operators
            READ(70,1002,END=9992)
          ENDDO
          READ(70,3004,ERR=9993) NBOPTC                                                            ! current nb of T operators
          CLOSE(70)
          IF( NBOPTC .GT. NBOHT(ITRT) ) THEN
            NBOHT(ITRT) = NBOPTC                                                                   ! update nb of T operators for this type
            ITRHT(ITRT) = ITR                                                                      ! the transition related to this type
          ENDIF
        ENDDO E359
      ENDDO
! READ Ts PARAMETERS
      DO ITRT=1,NTRT                                                                               ! Ts
        FPARAC = FPARA(ITRHT(ITRT))
        PRINT 2000, TRIM(FPARAC)
        OPEN(70,ERR=9994,FILE=TRIM(FPARAC),STATUS='OLD')
        CALL IOPT(70,ITRT)                                                                         ! read T parameters
        CLOSE(70)
! CHECK T CONSISTENCY FOR ALL TRANSITION PARAMETER FILES OF THIS TYPE
        IDEB = INPHT(ITRT)                                                                         ! INPHT set in IOPT
E362:   DO ITR=1,NTR                                                                               ! for each transition
          IF( ITR  .EQ. ITRHT(ITRT) .OR.               &                                           ! no self test
              ITRT .NE. INTRT(ITR)       ) CYCLE E362                                              ! different type
          FPARAC = FPARA(ITR)
          OPEN(70,ERR=9994,FILE=TRIM(FPARAC),STATUS='OLD')
          READ(70,3004,ERR=9993) NBOPHC                                                            ! current nb of H operators
          DO I=1,NBOPHC                                                                            ! skip  H operators
            READ(70,1000,END=9992)
          ENDDO
          READ(70,3004,ERR=9993) NBOPTC                                                            ! current nb of T operators
          DO I=IDEB+1,IDEB+NBOPTC                                                                  ! for each operator
            READ(70,1002,END=9992) COPAJC,PARAC,PREC
! COPAJ (OPERATOR DEFINITION) AND PARAO (ORIGINAL PARAMETER VALUE) SET IN IOPT
            IF( COPAJC .NE. COPAJ(I) .OR.         &                                                ! test definition
                PARAC  .NE. PARAO(I)      ) THEN                                                   ! and  value
              PRINT 8209, COPAJC,PARAC,             &                                              ! not consistent
                          TRIM(FPARAC),             &
                          COPAJ(I),PARAO(I),        &
                          TRIM(FPARA(ITRHT(ITRT)))
              GOTO  9999
            ENDIF
          ENDDO
          CLOSE(70)
        ENDDO E362
      ENDDO
!
! CREATE PARAMETER CONTROL FILE
!
      PRINT 2001, TRIM(FCLX)
      OPEN(12,ERR=9995,FILE=TRIM(FCLX),STATUS='UNKNOWN')
      CALL SETCH                                                                                   ! hamiltonian       parameters
      DO ITRT=1,NTRT                                                                               ! Ts
        CALL SETCT(ITRT)                                                                           ! transition moment parameters
      ENDDO
      CLOSE(12)
      GOTO 9000                                                                                    ! end
!
9992  PRINT 8014, FPARAC
      GOTO  9999
9993  PRINT 8013, FPARAC
      GOTO  9999
9994  PRINT 8012, FPARAC
      GOTO  9999
9995  PRINT 8011, FCLX
      GOTO  9999
9996  PRINT 8010, CARG
      GOTO  9999
9997  PRINT 8002
      CLOSE(10)
      GOTO  9999
9998  PRINT 8001
9999  PRINT 8000
!
9000  PRINT *
      END PROGRAM XCTRMK
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!  CREER UN FICHIER DE CONTROLE DE PARAMETRES (PARTIE FREQUENCES)
!  A PARTIR DU FICHIER DE PARAMETRES
!
      SUBROUTINE SETCH
      use mod_par_tds
      use mod_com_fdate
      use mod_com_xctrmk
      use mod_com_xtran
      IMPLICIT NONE

      integer          :: I
      integer          :: NBOP

      character(len = NBCTIT)  :: TITRE
!
1000  FORMAT(A)
1002  FORMAT(////,   &
             I4,/ )
1005  FORMAT(A,19X,'Hmn  Frdm         Attac/cm-1  St.Dev./cm-1')
1006  FORMAT(A,5X,'1',1X,E18.11,E14.7)
8000  FORMAT(' !!! XCTRMK : STOP ON ERROR')
8003  FORMAT(' !!! ERROR OPENING PARAMETER FILE')
8007  FORMAT(' !!! ERROR READING PARAMETER FILE')
8008  FORMAT(' !!! ERROR WRITING PARAMETER CONTROL FILE')
!
      FPARAC = FPARA(ITRHT(0))
      OPEN(11,FILE=FPARAC,ERR=9996,STATUS='OLD')
!
!  LIRE ET COPIER
!
      READ (11,1002,ERR=9994) NBOP
      WRITE(12,1002,ERR=9993) NBOP
      READ(11,1000,ERR=9994)
      WRITE(12,1005,ERR=9993) FDATE
      DO I=1,NBOP
        READ (11,1000,ERR=9994) TITRE(:NCL1)
        WRITE(12,1006,ERR=9993) TITRE(:NCL1),0.D0,0.D0
      ENDDO
      CLOSE(11)
      RETURN
!
9993  PRINT 8008
      GOTO  9999
9994  PRINT 8007
      GOTO  9999
9996  PRINT 8003
      GOTO  9999
9999  PRINT 8000
      PRINT *
      STOP
!
      END SUBROUTINE SETCH
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!  CREER UN FICHIER DE CONTROLE DE PARAMETRES (PARTIE INTENSITES)
!  A PARTIR DU FICHIER DE PARAMETRES
!
      SUBROUTINE SETCT(ITRT)
      use mod_par_tds
      use mod_com_fdate
      use mod_com_xctrmk
      use mod_com_xtran
      IMPLICIT NONE
      integer          :: ITRT

      integer          :: I
      integer          :: NBOP

      character(len = NBCTIT)  :: TITRE
!
1000  FORMAT(A)
1001  FORMAT(////,   &
             I4,//)
1003  FORMAT(///,    &
             A ,/,   &
             I4,/ )
1005  FORMAT(A,19X,'Hmn  Frdm         Attac/cm-1  St.Dev./cm-1')
1006  FORMAT(A,5X,'1',1X,E18.11,E14.7)
8000  FORMAT(' !!! XCTRMK : STOP ON ERROR')
8003  FORMAT(' !!! ERROR OPENING PARAMETER FILE')
8007  FORMAT(' !!! ERROR READING PARAMETER FILE')
8008  FORMAT(' !!! ERROR WRITING PARAMETER CONTROL FILE')
!
      FPARAC = FPARA(ITRHT(ITRT))
      OPEN(11,FILE=FPARAC,ERR=9996,STATUS='OLD')
!
! PASSER LES FREQUENCES
!
      READ(11,1001,ERR=9994) NBOP
      DO I=1,NBOP
        READ(11,1000,ERR=9994)
      ENDDO
!
!  LIRE ET COPIER
!
      READ (11,1003,ERR=9994) TITRE      ,NBOP
      WRITE(12,1003,ERR=9993) TRIM(TITRE),NBOP
      READ(11,1000,ERR=9994)
      WRITE(12,1005,ERR=9993) FDATE
      DO I=1,NBOP
        READ (11,1000,ERR=9994) TITRE(:NCL1)
        WRITE(12,1006,ERR=9993) TITRE(:NCL1),0.D0,0.D0
      ENDDO
      CLOSE(11)
      RETURN
!
9993  PRINT 8008
      GOTO  9999
9994  PRINT 8007
      GOTO  9999
9996  PRINT 8003
      GOTO  9999
9999  PRINT 8000
      PRINT *
      STOP
!
      END SUBROUTINE SETCT
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
! READ HAMILTONIAN PARAMETERS
!
      SUBROUTINE IOPH(LUI)
      use mod_dppr
      use mod_par_tds
      use mod_main_xctrmk
      IMPLICIT NONE
      integer          :: LUI

      real(kind=dppr)  :: PREC

      integer          :: I,IP
      integer          :: NBOPHC

      character(len = NBCTIT)  :: TITRE
!
1000  FORMAT(A)
1001  FORMAT(I4)
1002  FORMAT(A,E18.11,E14.7)
8000  FORMAT(' !!! XCTRMK : STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF IN IOPH')
!
      READ(LUI,1000,END=2000)
      READ(LUI,1000,END=2000) TITRE
      READ(LUI,1000,END=2000)
      READ(LUI,1000,END=2000) TITRE
      READ(LUI,1001,END=2000) NBOPHC
      DO WHILE( NBOPHC .GT. MXOPH )
        CALL RESIZE_MXOPH
      ENDDO
      INPHT(0) = 0                                                                                 ! starting index (-1) of H parameters
      NBOHT(0) = NBOPHC                                                                            ! current number of H operators
      DO I=1,2
        READ(LUI,1000,END=2000) TITRE
      ENDDO
      DO IP=INPHT(0)+1,INPHT(0)+NBOHT(0)
        READ(LUI,1002,END=2000) COPAJ(IP),PARAO(IP),PREC                                           ! operator definition, original parameter value
      ENDDO
      RETURN
!
2000  PRINT 8003
      PRINT 8000
      PRINT *
      STOP
!
      END SUBROUTINE IOPH
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
! READ TRANSITION PARAMETERS
!
      SUBROUTINE IOPT(LUI,ITRT)
      use mod_dppr
      use mod_par_tds
      use mod_main_xctrmk
      IMPLICIT NONE
      integer          :: LUI,ITRT

      real(kind=dppr)  :: PREC

      integer          :: IDEB,IP
      integer          :: NBOPHC,NBOPTC
!
1000  FORMAT(A)
1002  FORMAT(A,E18.11,E14.7)
3004  FORMAT(////,   &
             I4,//)
8000  FORMAT(' !!! XCTRMK : STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF IN IOPT')
!
      READ(LUI,3004,END=2000) NBOPHC
      DO IP=1,NBOPHC
        READ(LUI,1000,END=2000)
      ENDDO
      READ(LUI,3004,END=2000) NBOPTC
      DO WHILE( NBOPTC .GT. MXOPT )
        CALL RESIZE_MXOPT
      ENDDO
      INPHT(ITRT) = INPHT(ITRT-1)+NBOHT(ITRT-1)                                                    ! starting index (-1) of this type of T parameters
      NBOHT(ITRT) = NBOPTC                                                                         ! current number of T operators
      IDEB        = INPHT(ITRT)
      DO IP=IDEB+1,IDEB+NBOHT(ITRT)
        READ(LUI,1002,END=2000) COPAJ(IP),PARAO(IP),PREC                                           ! operator definition, original parameter value
      ENDDO
      RETURN
!
2000  PRINT 8003
      PRINT 8000
      PRINT *
      STOP
!
      END SUBROUTINE IOPT
