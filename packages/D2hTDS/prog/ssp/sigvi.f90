!
! *** CHERCHE POUR UN ENSEMBLE DONNE DE VI: (IV(I),I=1,12)
! *** IKNC(I,ISN), I=1,12 modes et ISN=1,... niveaux {VI}
!     centaines=Vi ;
!     dizaines=RI de D2 (1=A,2=B1,3=B2,4=B3) ;
!     unites=parite (1=g ou 2=u).
!     IIIR = 1:Ir, 2:IIr, 3:IIIr
!
!  REV 29 SEP 94
!  MODIFIE MARS 02 ---> C2H4/D2h (W. RABALLAND).
!
      SUBROUTINE SIGVI(IV,IKNC,MDMKNC,ISN,IIIR)
      use mod_par_tds
      IMPLICIT NONE
      integer ,dimension(NBVQN)           :: IV
      integer ,dimension(NBVQN+2,MDMKNC)  :: IKNC
      integer          :: MDMKNC,ISN,IIIR

      integer ,dimension(3)  :: ID
      integer          :: I
!
      DO I=0,2
        ID(I+1) = 10*(2+MOD(IIIR+I,3))
      ENDDO
!
!     Convention d'axes selon Herzberg
!
!     Ag+Ag+Ag+Au
!
      IKNC(1,ISN) = 100*IV(1)+11
      IKNC(2,ISN) = 100*IV(2)+11
      IKNC(3,ISN) = 100*IV(3)+11
      IKNC(4,ISN) = 100*IV(4)+MOD(IV(4),2)+11
!
! n = 2:Ir, 3:IIr, 1:IIIr
!     Bng+Bng+Bnu
!
      IKNC(5,ISN) = 100*IV(5)+MOD(IV(5),2)*(ID(1)+1)+(1-MOD(IV(5),2))*11
      IKNC(6,ISN) = 100*IV(6)+MOD(IV(6),2)*(ID(1)+1)+(1-MOD(IV(6),2))*11
      IKNC(7,ISN) = 100*IV(7)+MOD(IV(7),2)*(ID(1)+2)+(1-MOD(IV(7),2))*11
!
! m = 3:Ir, 1:IIr, 2:IIIr
!     Bmg+Bmu+Bmu
!
      IKNC(8,ISN)  = 100*IV(8)+MOD(IV(8),2)*(ID(2)+1)+(1-MOD(IV(8),2))*11
      IKNC(9,ISN)  = 100*IV(9)+MOD(IV(9),2)*(ID(2)+2)+(1-MOD(IV(9),2))*11
      IKNC(10,ISN) = 100*IV(10)+MOD(IV(10),2)*(ID(2)+2)+(1-MOD(IV(10),2))*11
!
! l = 1:Ir, 2:IIr, 3:IIIr
!     B3u+B3u
!
      IKNC(11,ISN) = 100*IV(11)+MOD(IV(11),2)*(ID(3)+2)+(1-MOD(IV(11),2))*11
      IKNC(12,ISN) = 100*IV(12)+MOD(IV(12),2)*(ID(3)+2)+(1-MOD(IV(12),2))*11
!
      RETURN
      END SUBROUTINE SIGVI
