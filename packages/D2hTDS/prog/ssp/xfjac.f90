      SUBROUTINE XFJAC(M,N,X,FVEC,FJAC,LDFJAC)
!
! 2008 AUGUST
! JACOBIAN CALCULUS FOR FCN OF xpafit.f90
!
! BASED UPON eq_tds.f90 AND eq_int.f90
!
      use mod_dppr
      use mod_par_tds
      use mod_par_x
      use mod_com_const
      use mod_com_cas
      use mod_com_der
      use mod_com_derv
      use mod_com_en
      use mod_com_spin
      use mod_com_trmomt
      use mod_com_xassi
      use mod_com_xfjac
      use mod_com_xfonc
      use mod_com_xpara
      use mod_com_xpoly
      use mod_com_xtran
      use mod_com_xvari
      IMPLICIT NONE
      real(kind=dppr) ,dimension(N)         :: X
      real(kind=dppr) ,dimension(M)         :: FVEC
      real(kind=dppr) ,dimension(LDFJAC,N)  :: FJAC
      integer          :: M,N,LDFJAC

! functions
      real(kind=dppr)  :: DPMPAR

      real(kind=dppr)  :: A2
      real(kind=dppr)  :: COEF1
      real(kind=dppr)  :: DIFSA,DIFSR,DTR,DTRC
      real(kind=dppr)  :: EN,EPS,EPSMCH
      real(kind=dppr)  :: F
      real(kind=dppr)  :: H,HHT,HT
      real(kind=dppr)  :: PARAC
      real(kind=dppr)  :: TM,TR
      real(kind=dppr)  :: W0
      real(kind=dppr)  :: XTMP

      integer          :: I,IB,IC,ICI,ICS,IDEB,IFLAG,II
      integer          :: ICP,ICPS,ICPI
      integer          :: IOBS,IOBSC,IOBZ,IOP,IPOL
      integer          :: IS,ISOBZC,ISV,ITR,ITRT,IX,IXOP
      integer          :: J,JC,JCI,JCIOBS,JCS,JCSOBS,JI,JS
      integer          :: K
      integer          :: NBOPHC,NBOPTC,NBSTRC,NFB,NFBC,NFBI,NFBS
      integer          :: NINF,NSUP,NSV

      character(len = 120)  :: FXED,FXTRO
!
8000  FORMAT(' !!! XFJAC  : STOP ON ERROR')
8050  FORMAT(' !!! ERROR OPENING ENERGY FILE : ',A)
8105  FORMAT(' !!! ERROR OPENING TRANSITION MOMENT FILE : ',A)
8107  FORMAT(' !!! UNEXPECTED EOF FOR : ',A)
!8203  FORMAT(' !!! ',I8,' FREQUENCY ASSIGNMENT SOLVED, ',I8,' EXPECTED',/,   &
!             ' !!! ',I8,' INTENSITY ASSIGNMENT SOLVED, ',I8,' EXPECTED',/,   &
!             ' !!! FOR TRANSITION MOMENT FILE : ',A                       )
!
! INITIALIZATION
!
      DO IXOP=1,N
        DO IOBZ=1,M
          FJAC(IOBZ,IXOP) = 0.D0                                                                   ! initialize to 0
        ENDDO
      ENDDO
      DO J=1,NBOHT(0)
        DO I=1,NBOBS
          DERINF(I,J) = 0.D0
          DERSUP(I,J) = 0.D0
        ENDDO
      ENDDO
!
! STORE ENERGIES AND DERIVATIVES
!
      DO IPOL=1,NBPOL
        CALL XHDIAG(IPOL)                                                                          ! energies and derivatives
        FXED = 'XED_P'//CPOL(IPOL)//'_'                                                            ! polyad energy file
        OPEN(50,ERR=4050,FILE=FXED,STATUS='OLD',FORM='UNFORMATTED')
        READ(50) NSV
        DO ISV=1,NSV
          READ(50)
        ENDDO
        READ(50) NBOPHC
!
1       READ(50,END=2) J,IC,ICP,NFB
        JC = J*100+IC*10+ICP
        DO NFBC=1,NFB
          READ(50,END=4150) EN
          READ(50,END=4150) (DERV(IOP), IOP=1,NBOPHC)
          READ(50,END=4150)
E4:       DO IOBS=1,NBOBS
            IF( CPOLI(IOBS) .NE. CPOL(IPOL) ) GOTO 5
            JCIOBS = JIOBS(IOBS)*100+ICIOBS(IOBS)*10+IPIOBS(IOBS)                                  ! inf
            IF( JCIOBS      .EQ. JC   .AND.         &
                NIOBS(IOBS) .EQ. NFBC       ) THEN
              ENINF(IOBS) = EN
              DO IOP=1,NBOPHC
                DERINF(IOBS,IOP) = DERV(IOP)
              ENDDO
            ENDIF
!
5           IF( CPOLS(IOBS) .NE. CPOL(IPOL) ) CYCLE E4
            JCSOBS = JSOBS(IOBS)*100+ICSOBS(IOBS)*10+IPSOBS(IOBS)                                  ! sup
            IF( JCSOBS      .EQ. JC   .AND.         &
                NSOBS(IOBS) .EQ. NFBC       ) THEN
              ENSUP(IOBS) = EN
              DO IOP=1,NBOPHC
                DERSUP(IOBS,IOP) = DERV(IOP)
              ENDDO
            ENDIF
          ENDDO E4
        ENDDO
        GOTO 1
!
2       CLOSE(50)
      ENDDO
!
! FREQUENCIES
!
      IF( NBFOBZ .NE. 0 ) THEN
!
! FREQUENCIES / H OPERATORS DERIVATIVES
!
        DO IOBZ=1,NBFOBZ
          IX = 0                                                                                   ! stocker dans xpafit ???
          DO J=1,NBOHT(0)                                                                          ! stocker NBOPHC pour chaque polyade ???
            IF( ICNTRL(J) .EQ. 2 .OR.         &
                ICNTRL(J) .EQ. 4      ) THEN
              IX            = IX+1
              IOBSC         = INDOBS(IOBZ)
              FJAC(IOBZ,IX) = -(DERSUP(IOBSC,J)-DERINF(IOBSC,J))*ZWGT(IOBZ)
            ENDIF
          ENDDO
        ENDDO
!
! FREQUENCIES / T OPERATORS DERIVATIVES
! = 0 , ALREADY INITIALIZED
!
      ENDIF
!
! INTENSITIES
!
      IF( NBSOBZ .NE. 0 ) THEN
!
! INTENSITIES / H OPERATORS DERIVATIVES
! FORWARD-DIFFERENCE APPROXIMATION
!
        EPSMCH = DPMPAR(1)                                                                         ! machine precision
!       EPS    = SQRT(MAX(EPSFCN,EPSMCH))                                                          ! cf. FDJAC2
        EPS    = SQRT(EPSMCH)                                                                      ! EPSFCN=0 in LMDIF1
        IONLY  = .TRUE.
        IFLAG  = 3                                                                                 ! as 1 but does not increment FCN counter
        DO J=1,NBXOHT(0)
          XTMP = X(J)
          H    = EPS*ABS(XTMP)
          IF( H .EQ. 0.D0 ) H = EPS
          X(J) = XTMP+H
          IX   = 0                                                                                 ! index of fitted parameter
          DO I=1,NBOP                                                                              ! for each parameter
            IF( ICNTRL(I) .EQ. 2 .OR.         &
                ICNTRL(I) .EQ. 4      ) THEN                                                       ! fitted
              IX      = IX+1
              PARA(I) = X(IX)                                                                      ! calculus used parameter
            ENDIF
          ENDDO
!
! TAKE CONTRIBUTIONS INTO ACCOUNT
!
          DO I=1,NBOHT(0)                                                                          ! H
            IF( ICNTRL(I) .EQ. 5 ) THEN                                                            ! 5 -> contribution
              PARAC = 0.D0
              DO K=1,NBOHT(0)                                                                      ! for each H operator
                IF( CTRIBH(I,K) .NE. 0.D0 ) PARAC = PARAC+CTRIBH(I,K)*PARA(K)                      ! add contribution
              ENDDO
              PARA(I) = PARAC                                                                      ! calculus used parameter
            ENDIF
          ENDDO
          DO ITRT=1,NTRT                                                                           ! Ts
            IDEB = INPHT(ITRT)                                                                     ! starting index (-1)
            DO I=IDEB+1,IDEB+NBOHT(ITRT)
              IF( ICNTRL(I) .EQ. 5 ) THEN                                                          ! 5 -> contribution
                PARAC = 0.D0
                IB    = I-IDEB                                                                     ! for CTRIBT
                DO K=1,NBOHT(ITRT)
                  IF( CTRIBT(ITRT,IB,K) .NE. 0.D0 ) THEN
                    PARAC = PARAC+CTRIBT(ITRT,IB,K)*PARA(IDEB+K)                                   ! add contribution
                  ENDIF
                ENDDO
                PARA(I) = PARAC                                                                    ! calculus used parameter
              ENDIF
            ENDDO
          ENDDO
          CALL XOBMCA(M,WB,IFLAG)
          X(J) = XTMP
          DO I=NBFOBZ+1,NBFOBZ+NBSOBZ
            FJAC(I,J) = (WB(I)-FVEC(I))/H                                                          ! FVEC est-il a jour ???
          ENDDO
        ENDDO
        IONLY = .FALSE.
!
! INTENSITIES / T OPERATORS DERIVATIVES
!
E200:   DO ITR=1,NTR                                                                               ! for each transition
          NBSTRC = 0                                                                               ! current number of intensity data assigned to this transition
          IF( NBSTR(ITR) .NE. 0 ) THEN
            CALL XTRO(ITR)
            FXTRO = 'XTRO_'//CTR(ITR)                                                              ! transition moment file
            OPEN(80,ERR=4080,FILE=FXTRO,STATUS='OLD',FORM='UNFORMATTED')
            READ(80) NBOPTC
!
! ASSIGNMENT JC CODES
!
11          READ(80,END=13) JS,ICS,ICPS,NFBS,  &
                            JI,ICI,ICPI,NFBI
            JCS = JS*100+ICS*10+ICPS
            JCI = JI*100+ICI*10+ICPI
E14:        DO IS=1,NFBS
E15:          DO II=1,NFBI
                READ(80,END=4180) NSUP,NINF,TM,(DERITR(IOP),IOP=1,NBOPTC)
! LOOK FOR AN ASSIGNMENT
E12:            DO IOBS=1,NBOBS
                  IF( JTR(IOBS)   .NE. ITR .OR.              &
                      SASOT(IOBS) .NE. '+'      ) CYCLE E12
                  JCIOBS = JIOBS(IOBS)*100+ICIOBS(IOBS)*10+IPIOBS(IOBS)                            ! right transition
                  JCSOBS = JSOBS(IOBS)*100+ICSOBS(IOBS)*10+IPSOBS(IOBS)
                  IF( JCIOBS      .EQ. JCI .AND.         &
                      NIOBS(IOBS) .EQ. II  .AND.         &
                      JCSOBS      .EQ. JCS .AND.         &
                      NSOBS(IOBS) .EQ. IS        ) THEN
                    A2  = TM*TM
                    HT  = ENINF(IOBS)*HCOVRK/TEMP(IOBS)
                    W0  = EXP(-HT)*SPIN(ICI)
                    TR  = A2*W0
                    DTR = 2.D0*TM*W0
                    IF( .NOT. ISRAM(ITR) )  THEN
                      F     = ENSUP(IOBS)-ENINF(IOBS)                                              ! frequency
                      HHT   = F*HCOVRK/TEMP(IOBS)
                      COEF1 = F*COEF(IOBS)*(1.D0-EXP(-HHT))
                      TR    = TR*COEF1
                      DTR   = DTR*COEF1
                    ENDIF
                    DIFSA = SOBS(IOBS)-TR                                                          ! absolute delta
                    DIFSR = 100.D0*DIFSA/TR                                                        ! relative delta
                    IF( ABS(DIFSR) .GT. RMXOMC ) GOTO 111                                          ! RMXOMC exceeded => 0.
                    ISOBZC = ISOBZ(IOBS)
                    IDEB   = INPHT(INTRT(ITR))
                    IX     = INXPHT(INTRT(ITR))
                    DO J=IDEB+1,IDEB+NBOPTC                                                        ! ??? NBOHT(INTRT(ITR))
                      IF( ICNTRL(J) .EQ. 2 .OR.         &
                          ICNTRL(J) .EQ. 4      ) THEN
                        IX              = IX+1
                        DTRC            = DTR*DERITR(J-IDEB)                                       ! TR derivative
                        FJAC(ISOBZC,IX) = -DTRC*ZWGT(ISOBZC)
                      ENDIF
                    ENDDO
!
111                 CONTINUE
                    NBSTRC = NBSTRC+1
                    IF( NBSTRC .EQ. NBSTR(ITR) ) GOTO 13
                  ENDIF
                ENDDO E12
              ENDDO E15
            ENDDO E14
            GOTO 11                                                                                ! next calculated transition
!
13          CLOSE(80)
          ENDIF
! CONSISTENCY CHECK
!16       IF( NBFTRC .NE. NBFTR(ITR) .OR.         &
!             NBSTRC .NE. NBSTR(ITR)      ) THEN                                                    ! ne tester que les intensites ???
!           PRINT 8203, NBFTRC,NBFTR(ITR),  &
!                       NBSTRC,NBSTR(ITR),  &
!                       FXTRM
!           GOTO  9999
!         ENDIF
        ENDDO E200
      ENDIF
!
! VIRTUAL (TYPE 2) DATA DERIVATIVES
!
      IOBZ = NBROBZ
      IF( IOBZ .NE. NBOBZ ) THEN
        IXOP = 0
        DO IOP=1,NBOP
          IF( ICNTRL(J) .EQ. 2 .OR.                  &
              ICNTRL(J) .EQ. 4      ) IXOP = IXOP+1
          IF( ICNTRL(J) .EQ. 2      ) THEN
            IOBZ            = IOBZ+1
            FJAC(IOBZ,IXOP) = -1.D0/PRAT(IOP)
          ENDIF
        ENDDO
      ENDIF
      RETURN
!
4050  PRINT 8050, FXED
      GOTO  9999
4150  PRINT 8107, FXED
      GOTO  9999
4080  PRINT 8105, FXTRO
      GOTO  9999
4180  PRINT 8107, FXTRO
      GOTO  9999
9999  PRINT 8000
      PRINT *
      STOP
!
      END SUBROUTINE XFJAC
