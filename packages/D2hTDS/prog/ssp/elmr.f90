!
! SMIL G.PIERRE MAI 82
! MOD. T.GABARD DEC 92
! REV. 6/10/94
! REV    JAN 1995 JPC,CW (PARAMETER)
! MODIFIE 03/02 W. RABALLAND ---> C2H4/D2h.
!
! *** COUPLAGE DES OPERATEURS CREATIONS ET ANNHILATIONS
!
! ***   < IB !! IC * IA !! IK > =
!
!       ( ICC   ICA   IGAMA)
! ***   (                  ) < IB !! IC !! IS >< IS !! IA !! IK >
!       ( ICB   ICK    IS  )
!
! ***   IS=ICA*ICB
! ***   AVEC IX(I) = 10000*VI+1000*LI+100*NI+10*CI+PI  POUR I=1,NBVQN
! ***   IX PEUT ETRE  IB,IA,IC OU IK
!     IIIR = 1:Ir, 2:IIr, 3:IIIr
!
      FUNCTION ELMR(IB,IC,IA,IK,IGAMA,IIIR)
      use mod_dppr
      use mod_par_tds
      use mod_com_elmr
      use mod_com_sy
      IMPLICIT NONE
      real(kind=dppr)  :: ELMR
      integer ,dimension(NBVQN+2)  :: IB,IC,IA,IK
      integer          :: IGAMA,IIIR

! functions
      real(kind=dppr)  :: E1TO12,SXC
      integer          :: MULGU

      real(kind=dppr)  :: EM1,EM2
      real(kind=dppr)  :: SIX

      integer ,dimension(NBVQN)          :: IPB
      integer          :: I,IDUM,IPKC,IPKP,IPKV
      integer          :: NB,NDUM
!
      ELMR = 0.D0
      DO I=1,NBVQN
        IPK(I,1) = IB(I)/100-IC(I)/100
        IPB(I)   = IK(I)/100-IA(I)/100
        IF( IPB(I)   .LT. 0      ) RETURN
        IF( IPK(I,1) .NE. IPB(I) ) RETURN
      ENDDO
      NB = 1
      CALL SIGVI(IPB,IPK,MXSNV,NB,IIIR)
      IPK(NBVQN+1,1) = 1
      IPK(NBVQN+2,1) = 1
      DO I=1,NBVQN
        CALL VLNC(IPK(I,1),IPKV,IPKC,IPKP)
        CALL MULD2H(IPK(NBVQN+1,1),IPKC,NDUM,IDUM)
        IPK(NBVQN+1,1) = IDUM
        IPK(NBVQN+2,1) = MULGU(IPK(NBVQN+2,1),IPKP)
      ENDDO
      DO I=1,NB
        EM1  = E1TO12(1,IB,IC,IPK(1,I))
        EM2  = E1TO12(-1,IPK(1,I),IA,IK)
        SIX  = SXC(IC(NBVQN+1),IA(NBVQN+1),IGAMA,IK(NBVQN+1),IB(NBVQN+1),IPK(NBVQN+1,1))
        ELMR = ELMR+SIX*EM1*EM2
      ENDDO
!
      RETURN
      END FUNCTION ELMR
