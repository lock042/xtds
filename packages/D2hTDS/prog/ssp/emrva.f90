!
! SMIL CHAMPION MAI 82
!
! MODIFIE 03/02 W. RABALLAND ---> C2H4/D2h.
!
!     ELEMENTS MATRICIELS REDUITS DES OPERATEURS VIBRATIONNELS
!     ELEMENTAIRES, LES OPERATEURS SONT CARACTERISES PAR :
!                 IPM     = +1 POUR UN OPERATEUR CREATION
!                         = -1 POUR UN OPERATEUR ANNIHILATION
!                 OMEGA   = DEGRE VIBRATIONNEL < OU = 3
!                 K       = OMEGA , OMEGA-2 , ... , 1 OU 0
!                 C       = SYMETRIE DANS D2 (A,B1,B2,B3)
!
      FUNCTION EMRVA(VP,CP,IPM,OMEGA,V,C)
      use mod_dppr
      use mod_com_sy
      IMPLICIT NONE
      real(kind=dppr)  :: EMRVA
      integer          :: VP,CP,IPM,OMEGA,V,C

      real(kind=dppr)  :: DVM

      integer          :: I
!
      EMRVA = 0.D0
      IF( IPM*(VP-V) .NE. OMEGA ) RETURN
      IF( OMEGA      .NE. 0     ) GOTO 10
      IF( C-CP       .NE. 0     ) RETURN
      EMRVA = 1.D0
      RETURN
!
10    IF( OMEGA .LT. 0 ) RETURN
      DVM   = MAX(VP,V)
      EMRVA = 1.D0
      DO I=0,(OMEGA-1)
        EMRVA = EMRVA*(DVM-I*1.0D0)
      ENDDO
      EMRVA = SQRT(EMRVA)
!
      RETURN
      END FUNCTION EMRVA
