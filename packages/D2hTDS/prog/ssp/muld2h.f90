!
! *** MULTIPLICATION DANS D2.
!
!  SMIL CHAMPION DEC 78
!
!  MODIFIE MARS 02 ---> C2H4/D2h (W. RABALLAND).
!
      SUBROUTINE MULD2H(IC1,IC2,N,IC)
      use mod_par_tds
      IMPLICIT NONE
      integer          :: IC1,IC2,N,IC

!
      N = 1
      IF    ( IC1     .EQ. IC2) THEN
        IC = 1
      ELSEIF( IC1     .EQ. 1  ) THEN
        IC = IC2
      ELSEIF( IC2     .EQ. 1  ) THEN
        IC = IC1
      ELSEIF( IC1*IC2 .EQ. 6  ) THEN
        IC = 4
      ELSEIF( IC1*IC2 .EQ. 8  ) THEN
        IC = 3
      ELSEIF( IC1*IC2 .EQ. 12 ) THEN
        IC = 2
      ENDIF
!
      RETURN
      END SUBROUTINE MULD2H
