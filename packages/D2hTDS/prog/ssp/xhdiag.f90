      SUBROUTINE XHDIAG(IHF)
!
! 2008 AUGUST
! V.BOUDON, CH.WENGER
!
! ***  EFFECTIVE HAMILTONIAN EIGENVALUES AND VECTORS
!      AND VIBRATIONAL COMPOSITION CALCULATION
!
! BASED UPON xhdi.f90 AND hdiag.f90
!
      use mod_dppr
      use mod_par_tds
      use mod_com_xpoly
      use mod_main_xpafit
      IMPLICIT NONE
      integer          :: IHF

      real(kind=dppr)  :: TT

      integer          :: I,IBASE,IC,ICLU,ICP,ICPLU,IFB,IOP,IOPH,IOPLU
      integer          :: IP,ISV,IV
      integer          :: J,JB,JFB,JLU
      integer          :: LMD_SUP
      integer          :: NBELM,NBOPHC,NELMA,NFB,NSV

      character(len = NBCTIT)  :: TITRE
      character(len = 120)  :: FEM,FXED,FXVP
!
8000  FORMAT(' !!! XHDIAG : STOP ON ERROR')
8128  FORMAT(' !!! MXOPH  EXCEEDED : ',I8,' > ',I8)
!
      FEM  = 'HA_P'//CPOL(IHF)//'_D'//TRIM(CDEV(IHF))//'_'
      FXED = 'XED_P'//CPOL(IHF)//'_'
!
! FILES
!
      OPEN(40,FILE=FEM,STATUS='OLD',FORM='UNFORMATTED')                                            ! matrix elements
      OPEN(50,FILE=FXED,FORM='UNFORMATTED',STATUS='UNKNOWN')                                       ! energies & derivatives
      IF( NBSOBZ .NE. 0 ) THEN
        FXVP = 'XVP_P'//CPOL(IHF)//'_'
        OPEN(60,FILE=FXVP,FORM='UNFORMATTED',STATUS='UNKNOWN')                                     ! eigenvalues and vectors
      ENDIF
!
! GENERAL CHARACTERISTICS
!
      DO I=1,8+NNIV(IHF)
        READ(40)
      ENDDO
      READ (40) NSV
      WRITE(50) NSV
      DO WHILE( NSV .GT. MXSNV )
        CALL RESIZE_MXSNV
      ENDDO
      READ(40)
      DO ISV=1,NSV
        READ (40) TITRE(:99)
        WRITE(50) TITRE(:99)
      ENDDO
      DO I=1,4
        READ(40)
      ENDDO
      READ(40)  NBOPHC
      IF( NBOPHC .GT. MXOPH ) THEN
        PRINT 8128, NBOPHC,MXOPH
        GOTO  9999
      ENDIF
      WRITE(50) NBOPHC
!
! ***  J LOOP
!
      LMD_SUP = -1
E12:  DO J=0,JMAX(IHF)
!
! ***  IC LOOP
!
E1:     DO IC=1,MXSYM
E13:      DO ICP=1,2
            READ(40) JLU,ICLU,ICPLU,NELMA,NFB
            DO WHILE( NFB .GT. MXDIMS )
              CALL RESIZE_MXDIMS
            ENDDO
            IF( NFB .EQ. 0 ) THEN
              IF( NBSOBZ .NE. 0 ) THEN
                GOTO 104
              ELSE
                CYCLE E13
              ENDIF
            ENDIF
            DO IFB=1,NFB
              DO JFB=1,NFB
                H(IFB,JFB) = 0.D0
              ENDDO
            ENDDO
            READ(40) (NVCOD(IP),NRCOD(IP),IP=1,NFB)
!
! ***  HAMILTONIAN OPERATORS LOOP
!
            NUMI(1) = 1
E14:        DO IOPH=1,NBOPHC
              READ(40) JLU,ICLU,ICPLU,NELMA,NFB,IOPLU,NBELM
              NUMI(IOPH+1) = NUMI(IOPH)+NBELM
              IF( NUMI(IOPH+1)-1 .GT. LMD_SUP ) LMD_SUP = NUMI(IOPH+1)-1
              DO WHILE( NUMI(IOPH+1)-1 .GT. MXELMD )
                CALL RESIZE_MXELMD
              ENDDO
              IF( NBELM .EQ. 0 ) CYCLE E14
              READ(40) (LI(I),KO(I),EL(I),I=NUMI(IOPH),NUMI(IOPH+1)-1)
E211:         DO I=NUMI(IOPH),NUMI(IOPH+1)-1
                H(LI(I),KO(I)) = H(LI(I),KO(I))+EL(I)*PARA(IOPH)
                IF( LI(I) .EQ. KO(I) ) CYCLE E211
                H(KO(I),LI(I)) = H(KO(I),LI(I))+EL(I)*PARA(IOPH)
              ENDDO E211
            ENDDO E14
!
! ***  DIAGONALIZE THEN WRITE OUT EIGENVALUES AND VECTORS
!
            CALL DIAGO(NFB)
            DO I=1,NFB
              HD(I) = H(I,I)
            ENDDO
            CALL ORDER(NFB,HD,K,MXDIMS)
            WRITE(50) J,IC,ICP,NFB
!
104         IF( NBSOBZ .NE. 0 ) THEN
              WRITE(60) J,IC,ICP,NFB
              IF( NFB .EQ. 0 ) CYCLE E13
              WRITE(60) ( HD(K(IP)),IP=1,NFB)
              DO JB=1,NFB
                WRITE(60) NVCOD(JB),NRCOD(JB),(T(JB,K(IP)),IP=1,NFB)
              ENDDO
            ENDIF
            DO IP=1,NFB
!
! VIBRATIONAL COMPOSITION
!
              DO ISV=1,NSV
                PCENT(ISV) = 0.D0
              ENDDO
              DO IBASE=1,NFB
                IV        = NVCOD(IBASE)/100
                TT        = T(IBASE,K(IP))
                PCENT(IV) = PCENT(IV)+TT*TT
              ENDDO
              DO ISV=1,NSV
                ICENT(ISV) = 100.D0*PCENT(ISV)+0.5D0
              ENDDO
              CALL DERIP(K(IP),NBOPHC)
              WRITE(50) HD(K(IP))
              WRITE(50) (DERI(IOP),IOP=1,NBOPHC)
              WRITE(50) (ICENT(ISV),ISV=1,NSV)
            ENDDO
          ENDDO E13
        ENDDO E1
      ENDDO E12
      CALL DEBUG( 'XHDIAG => MXELMD=',LMD_SUP)
      IF( NBSOBZ .NE. 0 ) CLOSE(60)
      CLOSE(50)
      CLOSE(40)
      RETURN
!
9999  PRINT 8000
      PRINT *
      STOP
!
      END SUBROUTINE XHDIAG
