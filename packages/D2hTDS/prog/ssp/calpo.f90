!
! ***  CALCULE LES ELEMENTS MATRICIELS DU TENSEUR DE POLARISABILITE
!
! SMIL J.P.C., J.M.J. , G.P.    DEC.88
! MOD. T.GABARD MAR. 93
!      T.GABARD AOU. 93
! REV    JAN 1995 JPC,CW (PARAMETER)
! MODIFIE 01/03 W. RABALLAND ---> C2H4/D2h
!
      SUBROUTINE CALPO(JS,ICS,NFBS,JI,ICI,NFBI,ICOR,IGV,ILG,ING,IGAM)
      use mod_dppr
      use mod_par_tds
      use mod_com_sy
      use mod_main_polmat
      IMPLICIT NONE
      integer          :: JS,ICS,NFBS,JI,ICI,NFBI,ICOR,IGV,ILG,ING,IGAM

! functions
      real(kind=dppr)  :: EMRRO
      integer          :: CTR,MULGU,NSYM1

      real(kind=dppr)  :: CK1,CKK,COEF
      real(kind=dppr)  :: ELM
      real(kind=dppr)  :: TERM1,TERM11,TERM2,TERM22

      integer          :: IB,IBRA,ICRB,ICRK,ICSUM
      integer          :: IG,IK,IKET,IO,IPB,IPK,IPT,IVB,IVK
      integer          :: KR
      integer          :: NG,NMB,NRB,NRCB,NRCK,NRK
      integer          :: NSSUM,NVB,NVK
!
! OMEGA
!
      IO = ICOR/1000
!
! KR
!
      KR = (ICOR-IO*1000)/100
!
! GAMMA R
!
      IG = (ICOR-IO*1000-KR*100)/10
!
! N R
!
      NG    = ICOR-IO*1000-KR*100-IG*10
      NBELM = 0
!
! POLYADE INFERIEURE
!
E3:   DO IBRA=1,NFBI
        NVB = NVCODI(IBRA)
!
! NUMERO DU SOUS-NIVEAU VIBRATIONNEL INF.
!
        IB = NVB/100
!
! C'V
!
        IPB  = NVB-100*IB
        IVB  = IPB/10
        NRCB = NRCODI(IBRA)
!
! PARITE DE C'V
!
        IPB = IPB-10*IVB
!
! N'
!
        NRB = NRCB/10
!
! C'R
!
        ICRB = NRCB-10*NRB
!
!   POLYADE SUPERIEURE
!
E4:     DO IKET=1,NFBS
          NVK = NVCODS(IKET)
!
! NUMERO DU SOUS-NIVEAU VIBRATIONNEL SUP.
!
          IK = NVK/100
          IF( EMRD(IK,IB) .EQ. 0.D0 ) CYCLE E4
!
! CV
!
          IPK  = NVK-100*IK
          IVK  = IPK/10
          NRCK = NRCODS(IKET)
!
! PARITE DE CV
!
          IPK = IPK-10*IVK
          IPT = MULGU(IPB,IPK)
          IF( IPT .EQ. 2 ) CYCLE E4
!
! N
!
          NRK = NRCK/10
!
! CR
!
          ICRK = NRCK-10*NRK
!
! SYMETRIE DE M
!
          ELM = 0.D0
          IF( ICI .NE. ICS             ) GOTO 401
          IF( ILG .LT. ABS(JI-JS) .OR.             &
              ILG .GT. ABS(JI+JS)      ) GOTO 401
!
!    PREMIERE SOMMATION
!
          TERM1 = 0.D0
E5:       DO ICSUM=1,MXSYM
            NSSUM = NSYM1(JS,ICSUM)
            IF( NSSUM                               .EQ. 0 ) CYCLE E5
            IF( CTR(IG,ICRK,ICSUM)*CTR(IGV,IVK,IVB) .EQ. 0 ) CYCLE E5
            TERM11 = 0.D0
            DO NMB=0,NSSUM-1
              CALL KCUBU(ILG,JS,JI,ING,NMB,NRB,IGAM,ICSUM,ICRB,CK1)
              CALL KCUBU(KR,JS,JS,NG,NRK,NMB,IG,ICRK,ICSUM,CKK)
              TERM11 = CK1*CKK+TERM11
            ENDDO
            TERM1 = TERM1+( (-1)**(ICRK+ICSUM) )*TERM11
          ENDDO E5
          TERM1 = TERM1*EMRRO(IO,KR,JS)
!
!    DEUXIEME SOMMATION
!
          TERM2 = 0.D0
E6:       DO ICSUM=1,MXSYM
            NSSUM = NSYM1(JI,ICSUM)
            IF( NSSUM                               .EQ. 0 ) CYCLE E6
            IF( CTR(IG,ICSUM,ICRB)*CTR(IGV,IVK,IVB) .EQ. 0 ) CYCLE E6
            TERM22 = 0.D0
            DO NMB=0,NSSUM-1
              CALL KCUBU(ILG,JS,JI,ING,NRK,NMB,IGAM,ICRK,ICSUM,CK1)
              CALL KCUBU(KR,JI,JI,NG,NMB,NRB,IG,ICSUM,ICRB,CKK)
              TERM22 = CK1*CKK+TERM22
            ENDDO
            TERM2 = TERM2+( (-1)**(ICRK+ICSUM) )*TERM22
          ENDDO E6
          TERM2 = TERM2*EMRRO(IO,KR,JI)
!
!    SOMME DES 2 TERMES
!
! RACINE DE LA DIMENSION DE LA REPRESENTATION DU COSINUS DIRECTEUR
! EN PLUS PAR RAPPORT A MA FORMULE
!
          COEF = EMRD(IK,IB)/2.D0
          ELM  = (TERM1+TERM2)*COEF
          ELM  = ELM*SQRT(DBLE((2*JI+1)*(2*JS+1)))
!
401       NBELM = NBELM+1
          IF( NBELM .GE. MXELMT ) CALL RESIZE_MXELMT
          LI(NBELM) = IBRA
          KO(NBELM) = IKET
! THESE L. TOUZANI P. 24 (POLARISABILITE ANISOTROPE)
! OLD        H(NBELM)=ELM
          H(NBELM) = ELM*SQRT(DC(IGAM))
        ENDDO E4
      ENDDO E3
!
      RETURN
      END SUBROUTINE CALPO
