!
! *** MULTIPLICITES DE A,B1,B2,B3 DANS D**J(U OU G)
!
! MODIFIE MARS 02 ---> C2H4/D2h (W. RABALLAND).
!
      SUBROUTINE NBJC(IR,NA,NB1,NB2,NB3)
      IMPLICIT NONE
      integer          :: IR,NA,NB1,NB2,NB3

      integer          :: P
      integer          :: Q
!
      P   = IR/2
      Q   = MOD(IR,2)
      NA  = P+1-Q
      NB1 = P+Q
      NB2 = NB1
      NB3 = NB2
!
      RETURN
      END SUBROUTINE NBJC
