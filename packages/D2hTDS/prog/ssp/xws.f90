      SUBROUTINE XWS
!
! 2008 AUGUST
! V. BOUDON, Ch. WENGER
!
! WRITE OUT INTENSITY FILES
!   s_prediction.t
!   s_prediction_mix.t
!   s_statistics.t
!
! BASED UPON eq_int.f90
!
      use mod_dppr
      use mod_par_tds
      use mod_com_branch
      use mod_com_const
      use mod_com_fdate
      use mod_com_fp
      use mod_com_spin
      use mod_com_sy
      use mod_com_xpoly
      use mod_com_xvari
      use mod_main_xpafit
      IMPLICIT NONE

      real(kind=dppr)  :: A2
      real(kind=dppr)  :: CODER,COEF0,COEF1,COENO,COL
      real(kind=dppr)  :: DDT,DIFF,DIFS,DIFSA
      real(kind=dppr)  :: ECTPJ,ECUM,ENIN,ENSU
      real(kind=dppr)  :: FCAL,FEXP
      real(kind=dppr)  :: HT,HHT
      real(kind=dppr)  :: PF1,PF2,POIC,POIDIR
      real(kind=dppr)  :: SCAL,SDFCAL,SDFEXP,SDFMOC,SDFMOD,SDSCAL,SDSEXP
      real(kind=dppr)  :: SDSMOD,SEXP
      real(kind=dppr)  :: TEMPC,TM,TNEGLC,TNO
      real(kind=dppr)  :: VACOII,VACOIJ
      real(kind=dppr)  :: W0,WNO

      integer          :: I,IBS,IC,ICINF,ICI,ICNTCL,ICS,ICSUP,IDEB,IFOBZC,II,IM,IN,IOP
      integer          :: ICPI,ICPINF,ICPS,ICPSUP
      integer          :: IOBS,IP,IPCINFS,IPOL,IS,ISOBZC,ISV,ITR,ITRT
      integer          :: J,JCI,JI,JINF,JJ,JMACS,JOP,JS,JSUP
      integer          :: KCENT
      integer          :: LPC,LPCI,LPCMAC,LTR
      integer          :: N,NBENI,NBINF,NBOPHC,NBOPTR,NBOTR,NBSTRC
      integer          :: NFBI,NFBS,NFBSUP,NINF,NSUP,NSV,NSVINF,NSVSUP
      integer          :: NUNS,NUS,NUSFIN

      character(len = NBCTIT)  :: TITRE
      character(len =   1)  :: FASO,ISP,SASO
      character(len =   1)  :: PAINF,PASUP
      character(len =   2)  :: BR,SYINF,SYSUP
      character(len =   5)  :: NUO
      character(len = 120)  :: FPARAC,FEM,FXTRO,FXEDS,FXEDI

      logical          :: INTRA
      logical          :: LASSNF
!
1000  FORMAT(A)
2001  FORMAT(' >> ',A)
2005  FORMAT(2A,3F11.4,2E13.4,F6.1,'% ',A,2(I3,1X,2A,I3,1X))
2006  FORMAT((10X,20(2I4,'%')))
2020  FORMAT(F13.6,2X,2A,2F7.3,F11.3,F13.6,F8.3,I5,1X,2A,I3,F13.6,I3,I4,'%',2X,A,1X,   &
             2(I3,1X,2A,I3,1X)                                                      )
2021  FORMAT(E13.4,2X,2A,F7.3,'%',F6.3,'%',F10.3,'%',E12.4,F8.3,'%',I4,1X,2A,I3,F13.6,I3,I4,'%')
2100  FORMAT(A,'    POLYAD ***********************************',/)
2101  FORMAT(I4,' VIBRATIONAL SUBLEVEL(S)')
2102  FORMAT(/,                                &
             I4,' HAMILTONIAN OPERATOR(S)',/)
2103  FORMAT(I4,' GREATEST VIBRATIONAL PERCENTAGE(S)',/)
2104  FORMAT(/,                                                           &
             '* POSITION :'                                         ,/,   &
             '      FEXP       CODE   SDEXP  SDTHEO  EXP-CALC      ',     &
             'FCAL      SDCAL  ( J C    N      E  )SUP         ....',     &
             ' ASSIGNMENT  ....'                                       )
2105  FORMAT('* STRENGTH :'                                         ,/,   &
             '      SEXP       CODE  %SDEXP %SDTHEO %EXP-CALC      ',     &
             'SCAL     %SDCAL  ( J C    N      E  )INF'                )
2106  FORMAT(/,                                         &
             A,' TRANSITION **********************',/)
2107  FORMAT(11X,I3,' TRANSITION OPERATOR(S)',//)
2108  FORMAT(/,                                                                       &
             A,1X,'TRANSITION  FITTED OPERATOR(S) ******************************',/)
2109  FORMAT(I8,' ASSIGNMENTS FOR THIS TRANSITION',/)
2110  FORMAT(/,                                                                      &
             15X,'HAMILTONIAN FITTED OPERATOR(S) ******************************',/)
3003  FORMAT(I3,I8,F15.6,6X,I6,4X,3(F11.2,1X,'%'))
3013  FORMAT(//,                      &
             ' PARTIAL STATISTICS ')
3014  FORMAT(//,                                          &
             I6,' DATA REACHING THE UPPER SUBLEVEL ',I3)
3015  FORMAT(//,                                                                                        &
             '      NUMBER OF   THEORETICAL   CUMULATIVE      PARTIAL      MEAN       CUMULATIVE', /,   &
             'Jsup    DATA       PRECISION    NB OF DATA     STD. DEV.     DEV.       STD. DEV.' ,// )
3016  FORMAT(//,                     &
             ' GLOBAL STATISTICS ')
8000  FORMAT(' !!! XWS    : STOP ON ERROR')
8003  FORMAT(' !!! INCOMPATIBLE'                         ,/,   &
             ' !!!     TRANSITION MOMENT        JCP:',3I4,/,   &
             ' !!! AND LOWER POLYAD HAMILTONIAN JCP:',3I4   )
8012  FORMAT(' !!! ERROR OPENING PARAMETER FILE : ',A)
8101  FORMAT(' !!! DIPOLE MOMENT FILE => UNEXPECTED EOF')
8102  FORMAT(' !!! UPPER LEVEL FILE   => UNEXPECTED EOF')
8103  FORMAT(' !!! LOWER LEVEL FILE   => UNEXPECTED EOF')
8104  FORMAT(' !!! ERROR OPENING TRANSITION MOMENT FILE : ',A)
8107  FORMAT(' !!! ERROR OPENING MATRIX ELEMENT FILE : ',A)
8109  FORMAT(' !!! ERROR OPENING s_statistics.t FILE')
8111  FORMAT(' !!! ERROR OPENING s_prediction_mix.t FILE')
8112  FORMAT(' !!! ERROR OPENING s_prediction.t FILE')
8203  FORMAT(' !!! ',I8,' INTENSITY ASSIGNMENT SOLVED, ',I8,' EXPECTED',/,   &
             ' !!! FOR TRANSITION MOMENT FILE : ',A                       )
!
      COEF0 = (8.D0*PI*PI*PI*CL)/(CLUM*PLANK)*1.D-36*T0
      TNO   = 296.D0                                                                               ! temperature des raies non observees
      COENO = COEF0/TNO/FPART(TNO,TNO)
      DO ITR=1,NTR
        IF( ISRAM(ITR) ) THEN
          DIPPOLS(ITR) = 'pol'
          POLSTC(ITR)  = POLST(ITR)
        ELSE
          DIPPOLS(ITR) = ''
          POLSTC(ITR)  = ''
        ENDIF
      ENDDO
!
! PREDICTIONS
!
      PRINT 2001,           's_prediction.t'
      OPEN(91,ERR=4080,FILE='s_prediction.t',STATUS='UNKNOWN')
      PRINT 2001,           's_prediction_mix.t'
      OPEN(95,ERR=4091,FILE='s_prediction_mix.t',STATUS='UNKNOWN')
!
! STATISTICS
!
      PRINT 2001,           's_statistics.t'
      OPEN(93,ERR=4093,FILE='s_statistics.t',STATUS='UNKNOWN')
      FPARAC = FPARA(ITRHT(0))                                                                     ! the transition referencing H
      FPARAC = TRIM(FPARAC)//'_adj'                                                                ! fitted parameter file
      OPEN(70,ERR=9994,FILE=TRIM(FPARAC),STATUS='OLD')
!
! ******************************
! *** READ AND COPY HEADERS  ***
! ******************************
!
! READ H PARAMETERS
!
      WRITE(91,2110)
      CALL IOAAS(70,91)                                                                            ! hamiltonian parameters
      CLOSE(70)
      WRITE(91,1000)
      DO ITRT=1,NTRT
        ITR = ITRHT(ITRT)
        WRITE(91,2108) 'P'//CPOL(INPS(ITR))//'mP'//CPOL(INPI(ITR))//' '//DIPPOLS(ITR)//' '//POLSTC(ITR)
        DO LTR=1,NTR
          IF( INTRT(LTR) .EQ. ITRT .AND.         &
              LTR        .NE. ITR        ) THEN
            WRITE(91,1000) 'P'//CPOL(INPS(LTR))//'mP'//CPOL(INPI(LTR))//' '//DIPPOLS(LTR)//' '//POLSTC(LTR)
          ENDIF
        ENDDO
        FPARAC = FPARA(ITR)
        FPARAC = TRIM(FPARAC)//'_adj'                                                              ! Ts fitted parameter file
        OPEN(70,ERR=9994,FILE=TRIM(FPARAC),STATUS='OLD')
        CALL IOAAX(70,91,ITRT)                                                                     ! transition  parameters
        CLOSE(70)
      ENDDO
      WRITE(91,1000)
      DO IPOL=1,NBPOL                                                                              ! for each polyad
        FEM = 'HA_P'//CPOL(IPOL)//'_D'//TRIM(CDEV(IPOL))//'_'
        WRITE(91,2100) 'P'//CPOL(IPOL)
        WRITE(93,2100) 'P'//CPOL(IPOL)
        WRITE(95,2100) 'P'//CPOL(IPOL)
        OPEN(40,ERR=4040,FILE=TRIM(FEM),STATUS='OLD',FORM='UNFORMATTED')                           ! matrix elements
!
! VIBRATIONAL SUBLEVELS
!
        DO I=1,8+NNIV(IPOL)
          READ(40)
        ENDDO
        READ (40)      NSV
        WRITE(91,2101) NSV
        WRITE(93,2101) NSV
        WRITE(95,2101) NSV
        READ(40)
        DO ISV=1,NSV
          READ (40)      TITRE(:99)
          WRITE(91,1000) TITRE(:99)
          WRITE(93,1000) TITRE(:99)
          WRITE(95,1000) TITRE(:99)
        ENDDO
!
! NUMBER OF OPERATORS OF THE CURRENT LEVEL
!
        DO I=1,4
          READ(40)
        ENDDO
        READ (40)      NBOPHC
        WRITE(91,2102) NBOPHC
        WRITE(93,2102) NBOPHC
        WRITE(95,2102) NBOPHC
        CLOSE(40)
        CALL XHDIAG(IPOL)                                                                          ! XED_ files
      ENDDO
      WRITE(91,1000) FDATE
      WRITE(93,1000) FDATE
      WRITE(91,2104)
      WRITE(91,2105)
!
! TRANSITION LOOP
!
E1:   DO ITR=1,NTR
        WRITE(91,2106) 'P'//CPOL(INPS(ITR))//'mP'//CPOL(INPI(ITR))//' '//DIPPOLS(ITR)//' '//POLSTC(ITR)
        WRITE(93,2106) 'P'//CPOL(INPS(ITR))//'mP'//CPOL(INPI(ITR))//' '//DIPPOLS(ITR)//' '//POLSTC(ITR)
        WRITE(95,2106) 'P'//CPOL(INPS(ITR))//'mP'//CPOL(INPI(ITR))//' '//DIPPOLS(ITR)//' '//POLSTC(ITR)
        WRITE(91,2109) NBSTR(ITR)
        WRITE(93,2109) NBSTR(ITR)
        WRITE(95,2109) NBSTR(ITR)
        IF( CDVO(ITR) .EQ. '-' ) THEN                                                              ! H only, skip it
          CYCLE E1
        ENDIF
        JMACS = JMAX(INPS(ITR))
        FXEDS = 'XED_P'//CPOL(INPS(ITR))//'_'                                                      ! upper polyad energy file
        FXEDI = 'XED_P'//CPOL(INPI(ITR))//'_'                                                      ! lower polyad energy file
        IF( FXEDS .EQ. FXEDI ) THEN
          INTRA = .TRUE.
        ELSE
          INTRA = .FALSE.
        ENDIF
        OPEN(50,ERR=4050,FILE=FXEDS,STATUS='OLD',FORM='UNFORMATTED')
        IF( INTRA ) THEN                                                                           ! partial copy of FXEDS in 51
          OPEN(51,STATUS='SCRATCH',FORM='UNFORMATTED')
          READ(50) NSVINF                                                                          ! no copy
          DO I=1,NSVINF
            READ(50)                                                                               ! no copy
          ENDDO
          READ(50)
!
4001      READ (50,END=4003) JINF,ICINF,ICPINF,NBINF
          WRITE(51)          JINF,ICINF,ICPINF,NBINF
          DO IP=1,NBINF
            READ (50,END=4161) ENIN
            WRITE(51)          ENIN
            READ(50,END=4161)
            WRITE(51)
            READ (50,END=4161) (ICENT(ISV),ISV=1,NSVINF)
            WRITE(51)          (ICENT(ISV),ISV=1,NSVINF)
          ENDDO
          GOTO 4001
!
4003      REWIND(50)
          ENDFILE(51)
          REWIND(51)
        ELSE
          OPEN(51,ERR=4051,FILE=FXEDI,STATUS='OLD',FORM='UNFORMATTED')
          READ(51) NSVINF
          DO ISV=1,NSVINF                                                                          ! FXEDI set position
            READ(51)
          ENDDO
          READ(51)
        ENDIF
!
! NSVSUP UPPER VIBRATIONAL SUBLEVELS
!
        READ(50) NSVSUP
        LPCMAC = MIN(NSVSUP,LPCMAX)                                                                ! reduce LPCMAX if needed
        WRITE(91,2103) LPCMAC
        IF( NBSTR(ITR) .NE. 0 ) THEN
          WRITE(93,2103) LPCMAC
          WRITE(95,2103) LPCMAC
        ENDIF
        DO I=1,NSVSUP                                                                              ! FXEDS set position
          READ(50)
        ENDDO
        READ(50)
!
! TRANSITION MOMENT FILE
!
        CALL XTRO(ITR)
        FXTRO = 'XTRO_'//CTR(ITR)                                                                  ! transition moment file and derivatives
        OPEN(80,ERR=4081,FILE=FXTRO,STATUS='OLD',FORM='UNFORMATTED')
        READ (80)      NBOTR                                                                       ! NUMBER OF TRANSITION OPERATORS
        WRITE(91,2107) NBOTR
        IF( NBSTR(ITR) .NE. 0 ) THEN
          WRITE(93,2107) NBOTR
          WRITE(95,2107) NBOTR
        ENDIF
!
! INITIALIZATION
!
        DO J=0,MXJ
          DO ISV=0,MXSNV
            NFJS(J,ISV)   = 0
            SMJS(J,ISV)   = 0.D0
            SPOIS(J,ISV)  = 0.D0
            SPRORS(J,ISV) = 0.D0
          ENDDO
        ENDDO
!
! set ICNTCL,IXCLS
!
        ITRT   = INTRT(ITR)                                                                        ! transition type
        NBOPTR = NBOHT(ITRT)                                                                       ! real    nb of T operators for this type
        IDEB   = INPHT(ITRT)                                                                       ! starting index (-1) (all parameters) for this type
        ICNTCL = 0                                                                                 ! nb of fitted operators
        DO I=IDEB+1,IDEB+NBOPTR
          IF( ICNTRL(I) .GE. 2 ) THEN
            ICNTCL        = ICNTCL+1                                                               ! current index of fitted operators
            IXCLS(ICNTCL) = I-IDEB                                                                 ! index of real operator
          ENDIF
        ENDDO
!
! *******************************
! *** READ LOWER ENERGY FILE  ***
! *******************************
!
        NBENI = 0
!
6       READ(51,END=8) JINF,ICINF,ICPINF,NBINF
        JCI = JINF*100+ICINF*10+ICPINF
        DO IP=1,NBINF
          NBENI = NBENI+1
          IF( NBENI .GT. MXENI ) CALL RESIZE_MXENI
          JIC(NBENI) = JCI
          READ(51,END=4160) ENINFC(NBENI)
          READ(51,END=4160)
          READ(51,END=4160) (ICENT(ISV),ISV=1,NSVINF)
          KCENT = 0
E9:       DO IC=1,NSVINF
            IF( KCENT .GT. ICENT(IC) ) CYCLE E9
            KCENT = ICENT(IC)
            IM    = IC
          ENDDO E9
          JCENTI(NBENI) = IM+10*KCENT
        ENDDO
        GOTO 6
!
8       CLOSE(51)
        CALL DEBUG( 'XWS    => MXENI=',NBENI)
!
! ****************************************
! *** THEORETICAL TRANSITIONS CALCULUS ***
! ****************************************
        NBSTRC = 0
!
! TRANSITION INDEX
!
10      READ(80,END=90) JS,ICS,ICPS,NFBS,  &
                        JI,ICI,ICPI,NFBI
        JCI = JI*100+ICI*10+ICPI
!
! SEARCH FOR UPPER LEVEL
!
11      READ(50,END=4161) JSUP,ICSUP,ICPSUP,NFBSUP
        IF( JS   .EQ. JSUP   .AND.            &
            ICS  .EQ. ICSUP  .AND.            &
            ICPS .EQ. ICPSUP       ) GOTO 14
        DO I=1,3*NFBSUP
          READ(50,END=4161)
        ENDDO
        GOTO 11
!
! SEARCH FOR LOWER LEVEL
!
14      CONTINUE
E15:    DO IN=1,NBENI
          IF( JCI .GT. JIC(IN) ) CYCLE E15
          IF( JCI .EQ. JIC(IN) ) GOTO 16
        ENDDO E15
! NOT FOUND
        JINF   =  JIC(IN)/100
        ICINF  = (JIC(IN)-JINF*100)/10
        ICPINF =  JIC(IN)-JINF*100-ICINF*10
        PRINT 8003, JI,ICI,ICPI,       &
                    JINF,ICINF,ICPINF
        GOTO  9999
!
16      CONTINUE
E18:    DO IS=1,NFBS
          READ(50,END=4161) ENSU
          READ(50,END=4161)
          READ(50,END=4161) (ICENT(ISV),ISV=1,NSVSUP)
E17:      DO II=1,NFBI
            N   = IN+II-1
            HT  = ENINFC(N)*HCOVRK/TNO
            WNO = EXP(-HT)*SPIN(ICI)
            READ(80,END=4150) NSUP,NINF,TM,(DERT(IOP),IOP=1,NBOTR)
            A2 = TM*TM
            DO LPC=1,LPCMAC
              IPCSUPS(LPC) = 0
              NUSVSUS(LPC) = 0
E702:         DO IC=1,NSVSUP                                                                       ! for each sublevel
                DO LPCI=1,LPC-1
                  IF( NUSVSUS(LPCI) .EQ. IC ) CYCLE E702                                           ! IC already used
                ENDDO
                IF( IPCSUPS(LPC) .GT. ICENT(IC) ) CYCLE E702                                       ! not a bigger ICENT
                IPCSUPS(LPC) = ICENT(IC)                                                           ! bigger => save
                NUSVSUS(LPC) = IC
              ENDDO E702
            ENDDO
            IPCINFS = JCENTI(N)/10
            NUSFIN  = JCENTI(N)-10*IPCINFS
!
! ASSIGNMENT LOOP
!
            JINF   = JI
            SYINF  = SYM(ICI)
            PAINF  = PAR(ICPI)
            NINF   = II
            JSUP   = JS
            SYSUP  = SYM(ICS)
            PASUP  = PAR(ICPS)
            NSUP   = IS
            BR     = BRANCH(JSUP-JINF+MXBRA-2)
            SDFMOC = 1.D0
            IF( IMEGA(ITR) .NE. 0 ) SDFMOC = DBLE(JSUP)**IMEGA(ITR)
            SDFMOC = PNEG(ITR)*SDFMOC
            SDSMOD = 0.D0
            LASSNF = .TRUE.                                                                        ! assignment not found
            IF( NBSTR(ITR) .EQ. 0 ) GOTO 110                                                       ! no assignment for this transition
E217:       DO IOBS=1,NBOBS
              IF( JTR(IOBS)    .NE. ITR  .OR.                &
                  SASOT(IOBS)  .NE. '+'        ) CYCLE E217                                        ! not selected
              IF( JIOBS(IOBS)  .EQ. JI   .AND.         &
                  ICIOBS(IOBS) .EQ. ICI  .AND.         &
                  IPIOBS(IOBS) .EQ. ICPI .AND.         &
                  NIOBS(IOBS)  .EQ. II   .AND.         &
                  JSOBS(IOBS)  .EQ. JS   .AND.         &
                  ICSOBS(IOBS) .EQ. ICS  .AND.         &
                  IPSOBS(IOBS) .EQ. ICPS .AND.         &
                  NSOBS(IOBS)  .EQ. IS         ) THEN
                LASSNF = .FALSE.                                                                   ! assignment     found
                ISP    = ISPT(IOBS)
                NUO    = NUOT(IOBS)
                FEXP   = FOBS(IOBS)
                FASO   = FASOT(IOBS)
                SEXP   = SOBS(IOBS)
                SASO   = SASOZ(ISOBZ(IOBS)-NBFOBZ)                                                 ! selected / internally unselected
                SDFEXP = SDFOBS(IOBS)
                SDSEXP = SDSOBS(IOBS)
                IFOBZC = IFOBZ(IOBS)                                                               ! fitted frequency ?
                IF( IFOBZC .NE.  0 ) THEN
                  DIFF   = FOMC(IFOBZC)/ZWGT(IFOBZC)
                  FCAL   = FEXP-DIFF
                  SDFCAL = SDZCAL(IFOBZC)
                ELSE
                  FCAL   = ENSU-ENINFC(N)                                                          ! unfitted frequency
                  DIFF   = FEXP-FCAL
                  SDFCAL = 0.D0                                                                    ! no precision
                ENDIF
                TEMPC = TEMP(IOBS)
                COEF1 = COEF0/TEMPC/FPART(TEMPC,TEMPC)
                HT    = ENINFC(N)*HCOVRK/TEMPC
                W0    = EXP(-HT)*SPIN(ICI)
                IF( ISRAM(ITR) ) THEN
                  SCAL = W0*A2
                ELSE
                  HHT  = FCAL*HCOVRK/TEMPC
                  COL  = W0*FCAL*COEF1*(1.D0-EXP(-HHT))
                  SCAL = COL*A2
                ENDIF
                ISOBZC = ISOBZ(IOBS)
                DIFSA  = SEXP-SCAL
                IF( SCAL .EQ. 0.D0 ) THEN
                  DIFS   = 999.999D0
                  SDSCAL = 999.999D0
                ELSE
                  DIFS   = 100.D0*DIFSA/SCAL
                  SDSCAL = (SDZCAL(ISOBZC)/SCAL)*100.D0
                  DIFS   = MIN(DIFS  ,999.999D0)                                                   ! cut off
                  SDSCAL = MIN(SDSCAL,999.999D0)                                                   ! cut off
                ENDIF
                IF( ABS(DIFS) .GT. RMXOMC ) GOTO 326                                               ! RMXOMC taken into account
!
! STATISTICS
!
                NUS            = NUSVSUS(1)
                POIC           = ZWGT(ISOBZC)*ZWGT(ISOBZC)
                POIDIR         = POIC*DIFS*DIFS
                NFJS(JS,NUS)   = NFJS(JS,NUS)+1
                NFJS(JS,0)     = NFJS(JS,0)+1
                SMJS(JS,NUS)   = SMJS(JS,NUS)+POIC*DIFS
                SMJS(JS,0)     = SMJS(JS,0)+POIC*DIFS
                SPOIS(JS,NUS)  = SPOIS(JS,NUS)+POIC
                SPOIS(JS,0)    = SPOIS(JS,0)+POIC
                SPRORS(JS,NUS) = SPRORS(JS,NUS)+POIDIR
                SPRORS(JS,0)   = SPRORS(JS,0)+POIDIR
!
326             IF    ( ISP .EQ. 'M' ) THEN
                  FEXP   = FEXP*CMHZ
                  SDFEXP = SDFEXP*CMHZ
                  SDFMOD = SDFMOC*CMHZ
                  DIFF   = DIFF*CMHZ
                  FCAL   = FCAL*CMHZ
                  SDFCAL = SDFCAL*CMHZ
                ELSEIF( ISP .EQ. 'G' ) THEN
                  FEXP   = FEXP*CGHZ
                  SDFEXP = SDFEXP*CGHZ
                  SDFMOD = SDFMOC*CGHZ
                  DIFF   = DIFF*CGHZ
                  FCAL   = FCAL*CGHZ
                  SDFCAL = SDFCAL*CGHZ
                ELSE
                  SDFMOD = SDFMOC
                ENDIF
                SDFEXP = SDFEXP*1.D+3
                SDFMOD = SDFMOD*1.D+3
                IF( IFOBZC .EQ.  0 ) THEN
                  DIFF   = 9999.999D0                                                              ! whatever the unit
                  SDFCAL = 9999.999D0
                ELSE
                  DIFF   = DIFF*1.D+3
                  SDFCAL = SDFCAL*1.D+3
                  DIFF   = MIN(DIFF  ,9999.999D0)                                                  ! cut off
                  SDFCAL = MIN(SDFCAL,9999.999D0)                                                  ! cut off
                ENDIF
!
! WRITE OUT OUTPUT FILES
!
                WRITE(91,2020) FEXP,FASO,ISP//NUO,SDFEXP,SDFMOD,DIFF,FCAL,SDFCAL,  &
                               JSUP,SYSUP,PASUP,NSUP,                              &
                               ENSU,NUSVSUS(1),IPCSUPS(1),BR,                      &
                               JINF,SYINF,PAINF,NINF,                              &
                               JSUP,SYSUP,PASUP,NSUP
                WRITE(95,2005) ISP,SASO,               &
                               FEXP,FCAL,DIFF,         &
                               SEXP,SCAL,DIFS,         &
                               BR,                     &
                               JINF,SYINF,PAINF,NINF,  &
                               JSUP,SYSUP,PASUP,NSUP
                WRITE(95,2006) (NUSVSUS(JJ),IPCSUPS(JJ),JJ=1,LPCMAC)
                WRITE(91,2021) SEXP,SASO,ISP//NUO,SDSEXP,SDSMOD,DIFS,SCAL,SDSCAL,  &
                               JINF,SYINF,PAINF,NINF,                              &
                               ENINFC(N),NUSFIN,IPCINFS
                NBSTRC = NBSTRC+1
              ENDIF
            ENDDO E217
!
110         IF( LASSNF ) THEN                                                                      ! assignment not found
              ISP    = 'C'
              NUO    = ''
              FEXP   = 0.D0
              FASO   = ''
              SEXP   = 0.D0
              SASO   = ''
              SDFEXP = 0.D0
              SDSEXP = 0.D0
!  FREQUENCE
              FCAL = ENSU-ENINFC(N)
              IF( FCAL .LE. 0.D0 ) CYCLE E17
!  INTENSITE
              IF( ISRAM(ITR) ) THEN
                SCAL = WNO*A2
                DO IOP=1,NBOTR
                  DERS(IOP) = WNO*DERT(IOP)
                ENDDO
              ELSE
                HHT   = FCAL*HCOVRK/TNO
                COL   = WNO*FCAL*COENO*(1.D0-EXP(-HHT))
                SCAL  = COL*A2
                CODER = 2.D0*TM*COL
                DO IOP=1,NBOTR
                  DERS(IOP) = CODER*DERT(IOP)
                ENDDO
              ENDIF
              PF1 = 0.D0
              PF2 = 0.D0
E773:         DO IOP=1,ICNTCL
                DDT    = DERS(IXCLS(IOP))
                VACOII = COV(IOP,IOP)
                PF1    = PF1+DDT*DDT*VACOII
                IF( IOP .EQ. ICNTCL ) CYCLE E773
                DO JOP=IOP+1,ICNTCL
                  VACOIJ = COV(JOP,IOP)
                  PF2    = PF2+DDT*DERS(IXCLS(JOP))*VACOIJ
                ENDDO
              ENDDO E773
              DIFS = 999.999D0
              IF( SCAL .EQ. 0.D0 ) THEN
                SDSCAL = 999.999D0
              ELSE
                SDSCAL = SQRT(PF1+PF2*2.D0)/SCAL*100.D0
                SDSCAL = MIN(SDSCAL,999.999D0)
              ENDIF
              SDFMOD = SDFMOC
              SDFEXP = SDFEXP*1.D+3
              SDFMOD = SDFMOD*1.D+3
              DIFF   = 9999.999D0
              SDFCAL = 9999.999D0
!
! WRITE OUT OUTPUT FILES
!
              WRITE(91,2020) FEXP,FASO,ISP//NUO,SDFEXP,SDFMOD,DIFF,FCAL,SDFCAL,  &
                             JSUP,SYSUP,PASUP,NSUP,                              &
                             ENSU,NUSVSUS(1),IPCSUPS(1),BR,                      &
                             JINF,SYINF,PAINF,NINF,                              &
                             JSUP,SYSUP,PASUP,NSUP
              WRITE(91,2021) SEXP,SASO,ISP//NUO,SDSEXP,SDSMOD,DIFS,SCAL,SDSCAL,  &
                             JINF,SYINF,PAINF,NINF,                              &
                             ENINFC(N),NUSFIN,IPCINFS
            ENDIF
          ENDDO E17
        ENDDO E18
        DO IBS=1,3*NFBSUP+1
          BACKSPACE(50)
        ENDDO
        GOTO 10
!
90      IF( NBSTRC .NE. NBSTR(ITR) ) THEN
          PRINT 8203, NBSTRC,NBSTR(ITR),FXTRO
          GOTO  9999
        ENDIF
        CLOSE(50)
        CLOSE(80)
        IF( NBSTR(ITR) .EQ. 0 ) CYCLE E1                                                           ! no statistics
!
! ********************
! ***  STATISTICS  ***
! ********************
!
        WRITE(93 ,3013)
E105:   DO NUNS=1,NSVSUP
          DO J=0,JMACS
            NCUS(J)  = NFJS(J,NUNS)
            SPCUS(J) = SPOIS(J,NUNS)
            ECUS(J)  = SPRORS(J,NUNS)
            NCU0(J)  = NCU0(J)+NCUS(J)
            SPCU0(J) = SPCU0(J)+SPCUS(J)
            ECU0(J)  = ECU0(J)+ECUS(J)
          ENDDO
          DO J=1,JMACS
            NCUS(J)  = NCUS(J-1)+NCUS(J)
            SPCUS(J) = SPCUS(J-1)+SPCUS(J)
            ECUS(J)  = ECUS(J-1)+ECUS(J)
          ENDDO
          IF( NCUS(JMACS) .EQ. 0 ) CYCLE E105
          WRITE(93,3014) NCUS(JMACS),NUNS
          WRITE(93,3015)
E104:     DO J=0,JMACS
            IF( NFJS(J,NUNS) .EQ. 0 ) CYCLE E104
            SMJS(J,NUNS) = SMJS(J,NUNS)/SPOIS(J,NUNS)
            ECTPJ        = SQRT(SPRORS(J,NUNS)/SPOIS(J,NUNS))
            ECUM         = SQRT(ECUS(J)/SPCUS(J))
            TNEGLC       = 0.D0                                                                    ! theoretical precision
            WRITE(93,3003) J,NFJS(J,NUNS),TNEGLC,NCUS(J),ECTPJ,SMJS(J,NUNS),ECUM
          ENDDO E104
        ENDDO E105
        DO J=1,JMACS
          NCU0(J)  = NCU0(J-1)+NCU0(J)
          SPCU0(J) = SPCU0(J-1)+SPCU0(J)
          ECU0(J)  = ECU0(J-1)+ECU0(J)
        ENDDO
        WRITE(93 ,3016)
        WRITE(93 ,3015)
E107:   DO J=0,JMACS
          IF( NFJS(J,0) .EQ. 0 ) CYCLE E107
          SMJS(J,0) = SMJS(J,0)/SPOIS(J,0)
          ECTPJ     = SQRT(SPRORS(J,0)/SPOIS(J,0))
          ECUM      = SQRT(ECU0(J)/SPCU0(J))
          TNEGLC    = 0.D0                                                                         ! theoretical precision
          WRITE(93,3003) J,NFJS(J,0),TNEGLC,NCU0(J),ECTPJ,SMJS(J,0),ECUM
        ENDDO E107
      ENDDO E1
      CLOSE(91)
      CLOSE(93)
      CLOSE(95)
      RETURN
!
4080  PRINT 8112
      GOTO  9999
4091  PRINT 8111
      GOTO  9999
4093  PRINT 8109
      GOTO  9999
4040  PRINT 8107, FEM
      GOTO  9999
4050  PRINT 8107, FXEDS
      GOTO  9999
4051  PRINT 8107, FXEDI
      GOTO  9999
4081  PRINT 8104, FXTRO
      GOTO  9999
4160  PRINT 8103
      GOTO  9999
4161  PRINT 8102
      GOTO  9999
4150  PRINT 8101
      GOTO  9999
9994  PRINT 8012, FPARAC
      GOTO  9999
9999  PRINT 8000
      PRINT *
      STOP
!
      END SUBROUTINE XWS
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
! READ AND COPY PARAMETERS
!
      SUBROUTINE IOAAX(LUI,LUO,ITRT)
      use mod_dppr
      use mod_par_tds
      use mod_com_xpara
      IMPLICIT NONE
      integer          :: LUI,LUO,ITRT

      real(kind=dppr)  :: PARAC,PREC

      integer          :: I,IDEB,IP
      integer          :: NBOPHC,NBOPTC

      character(len = NBCLAB+10)  :: CHAINE
      character(len = NBCTIT)     :: TITRE
!
1000  FORMAT(A)
1001  FORMAT(I4,A)
1002  FORMAT(A,E18.11,E14.7)
3004  FORMAT(////,   &
             I4,//)
8000  FORMAT(' !!! XWS    : STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF IN IOAAX')
!
! READ H
      READ(LUI,3004,END=2000) NBOPHC
      DO IP=1,NBOPHC
        READ(LUI,1000,END=2000)
      ENDDO
! COPY T
      DO I=1,4
        READ (LUI,1000,END=2000) TITRE
        WRITE(LUO,1000)          TRIM(TITRE)
      ENDDO
      READ (LUI,1001,END=2000)       NBOPTC,TITRE
      WRITE(LUO,1001)          NBXOHT(ITRT),TRIM(TITRE)
      DO I=1,2
        READ (LUI,1000,END=2000) TITRE
        WRITE(LUO,1000)          TRIM(TITRE)
      ENDDO
      IDEB = INPHT(ITRT)
      DO IP=IDEB+1,IDEB+NBOHT(ITRT)
        READ(LUI,1002,END=2000) CHAINE,PARAC,PREC
        IF( ICNTRL(IP) .EQ. 2 .OR.         &
            ICNTRL(IP) .EQ. 4      ) THEN                                                          ! fitted
          WRITE(LUO,1002)       CHAINE,PARAC,PREC
        ENDIF
      ENDDO
      RETURN
!
2000  PRINT 8003
      PRINT 8000
      PRINT *
      STOP
!
      END SUBROUTINE IOAAX
