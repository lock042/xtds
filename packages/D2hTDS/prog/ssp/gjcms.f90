!
! *** SYMBOLES G DE SO(3)
!
! SMIL CHAMPION DEC 78
! REV    JAN 1995 JPC,CW (PARAMETER)
!
! MODIFIE 03/2002 W. RABALLAND ---> D2H/C2H4
!
      SUBROUTINE GJCMS(J,M,N,IC,G)
      use mod_dppr
      IMPLICIT NONE
      real(kind=dppr)  :: G
      integer          :: J,M,N,IC

      integer          :: IM,IP
      integer          :: JP
      integer          :: MP
!
      G  = 0.D0
      IP = 0
      IM = ABS(M)
      JP = MOD(J,2)
      MP = MOD(IM,2)
      IF( IM .NE. N ) RETURN
      IF( IM .EQ. 0 ) GOTO 20
      G = 1.D0/SQRT(2.D0)
      IF( M  .LT. 0 ) G = G*( (-1)**(JP+MP+IC+1) )
      IF( MP .NE. 0 ) GOTO 22
      IF( IC .GT. 2 ) G = 0.D0
      RETURN
!
22    IF( IC .LT. 3 ) G = 0.D0
      RETURN
!
20    IF( ((IC.EQ.1).AND.(JP.EQ.0)) .OR.             &
          ((IC.EQ.2).AND.(JP.EQ.1))      ) G = 1.D0
!
      RETURN
      END SUBROUTINE GJCMS
