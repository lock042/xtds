!
! *** MULTIPLICITE DE IC DANS D**J(U OU G)
!
! MODIFIE MARS 02 ---> C2H4/D2h (W. RABALLAND).
!
      FUNCTION NSYM1(J,IC)
      use mod_par_tds
      IMPLICIT NONE
      integer          :: NSYM1
      integer          :: J,IC

      integer ,dimension(MXSYM)  :: N
      integer          :: N1,N2,N3,N4
!
      CALL NBJC(J,N1,N2,N3,N4)
      N(1)  = N1
      N(2)  = N2
      N(3)  = N3
      N(4)  = N4
      NSYM1 = N(IC)
!
      RETURN
      END FUNCTION NSYM1
