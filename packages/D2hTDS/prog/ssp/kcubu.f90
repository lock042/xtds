!
! *** FACTEURS ISOSCALAIRES  O(3)-TD
!
! SMIL CHAMPION DEC 78
!
! MODIFIE 03/2002 W. RABALLAND ---> D2H/C2H4
!
      SUBROUTINE KCUBU(J1,J2,J3,N1,N2,N3,IC1,IC2,IC3,AK)
      use mod_dppr
      IMPLICIT NONE
      real(kind=dppr)  :: AK
      integer          :: J1,J2,J3,N1,N2,N3,IC1,IC2,IC3

      real(kind=dppr)  :: F
!
      CALL FCUBU(J1,J2,J3,N1,N2,N3,IC1,IC2,IC3,F)
      AK = F
!
      RETURN
      END SUBROUTINE KCUBU
