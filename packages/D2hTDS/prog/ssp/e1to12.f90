!
! SMIL G.PIERRE JUIN 82
! MOD. T.GABARD DEC 92
! MODIFIE 03/02 W. RBABALLAND---> C2H4/D2h.
!
!     IPM=+1 POUR UN OPERATEUR CREATION
!     IPM=-1 POUR UN OPERATEUR ANNIHILATION
!
      FUNCTION E1TO12(IPM,IB,IA,IK)
      use mod_dppr
      use mod_par_tds
      IMPLICIT NONE
      real(kind=dppr)  :: E1TO12
      integer ,dimension(NBVQN+2)  :: IB,IA,IK
      integer          :: IPM

! functions
      real(kind=dppr)  :: EMRVA

      integer          :: I
      integer          :: IAC,IAP,IAV
      integer          :: IBC,IBP,IBV
      integer          :: IKC,IKP,IKV
!
      E1TO12 = 1.D0
      DO I=1,NBVQN
        CALL VLNC(IB(I),IBV,IBC,IBP)
        CALL VLNC(IA(I),IAV,IAC,IAP)
        CALL VLNC(IK(I),IKV,IKC,IKP)
        E1TO12 = E1TO12*EMRVA(IBV,IBC,IPM,IAV,IKV,IKC)
      ENDDO
!
      RETURN
      END FUNCTION E1TO12
