!
!  CALCUL DES K, EXTRAITS PAR LES FONCTIONS DKUB, ODKUB.
!  D COMME DIAG, OD COMME OFF DIAG.
!  POUR UTILISATION DANS LE PROGRAMME DIPMAT.F
!
! REV    JAN 1995 JPC,CW (PARAMETER)
!
      SUBROUTINE CALKD(JM,IORDEV)
      use mod_par_tds
      use mod_com_sy
      use mod_main_dipmat
      IMPLICIT NONE
      integer          :: JM,IORDEV

! functions
      integer          :: CTR,NSYM1

      integer          :: I,IC1,IC2,IC3,INC
      integer          :: J1,J2,J3
      integer          :: N1,N2,N3
!
!  OFF DIAG
!
      J1  = 1
      IC1 = 4
      INC = 1
      N1  = 0
      DO J2=0,JM-1
        J3         = J2+1
        INJODK(J3) = INC
        DO IC3=1,MXSYM
E3:       DO IC2=1,MXSYM
            IF( CTR(IC1,IC2,IC3) .EQ. 0 ) CYCLE E3
            DO N3=0,NSYM1(J3,1,IC3)-1
              DO N2=0,NSYM1(J2,1,IC2)-1
                IODKX(INC) = 100000*IC3+10000*IC2+100*N3+N2
                CALL KCUBU(J1,J2,J3,1,1,1,N1,N2,N3,IC1,IC2,IC3,VODK(INC),I)
                INC = INC+1
                IF( INC .GT. MXODK ) CALL RESIZE_MXODK
              ENDDO
            ENDDO
          ENDDO E3
        ENDDO
      ENDDO
      CALL DEBUG( 'CALKD  => J=',JM)
      CALL DEBUG( 'CALKD  => MXODK=',INC-1)
      INJODK(JM+1) = INC
!
!  DIAG
!
      INC = 1
      DO J2=0,JM
        J3        = J2
        INJDK(J2) = INC
        DO J1=0,IORDEV+1
E103:     DO IC1=1,MXSYM
            IF( NSYM1(J1,1,IC1) .EQ. 0 ) CYCLE E103
            DO IC3=1,MXSYM
E105:         DO IC2=IC3,MXSYM
                IF( CTR(IC1,IC2,IC3) .EQ. 0 ) CYCLE E105
                DO N3=0,NSYM1(J3,1,IC3)-1
E107:             DO N2=0,NSYM1(J2,1,IC2)-1
                    IF( J1 .EQ. 0 ) THEN
                      IF( N2  .NE. N3  .OR.               &
                          IC2 .NE. IC3      ) CYCLE E107
                      VDK(INC) = ( (-1)**J2 )*SQRT(DC(IC2)/(2*J2+1))
                    ELSE
                      CALL KCUBU(J1,J2,J3,1,1,1,N1,N2,N3,IC1,IC2,IC3,VDK(INC),I)
                    ENDIF
                    IDKX(INC) = 10000000*J1+1000000*IC1+100000*IC3+10000*IC2+100*N3+N2
                    INC = INC+1
                    IF( INC .GT. MXDK ) CALL RESIZE_MXDK
                  ENDDO E107
                ENDDO
              ENDDO E105
            ENDDO
          ENDDO E103
        ENDDO
      ENDDO
      CALL DEBUG( 'CALKD  => J=',JM)
      CALL DEBUG( 'CALKD  => MXDK=',INC-1)
      INJDK(JM+1) = INC
!
      RETURN
      END SUBROUTINE CALKD
