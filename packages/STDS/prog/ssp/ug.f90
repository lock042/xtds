!
! *** TRANSFORMATION U-G
!
! SMIL CHAMPION DEC 78
!
      SUBROUTINE UG(ICC,ISS,IC,IS,S)
      use mod_dppr
      IMPLICIT NONE
      real(kind=dppr)  :: S
      integer          :: ICC,ISS,IC,IS

!
      IF( ICC             .EQ. 3 ) GOTO 5
      IF( (ICC-2)*(ICC-5) .NE. 0 ) GOTO 3
      IC = ICC-1
      GOTO 4
!
3     IC = ICC+1
!
4     IS = ISS
      S  = 1.D0
      RETURN
!
5     IC = ICC
      IF( ISS .GT. 1 ) GOTO 7
      IS =  2
      S  = -1.D0
      RETURN
!
7     IS = 1
      S  = 1.D0
!
      RETURN
      END SUBROUTINE UG
