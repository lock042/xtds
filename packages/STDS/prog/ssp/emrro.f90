!
!     ELEM. MAT. REDUIT DU ROTATEUR SPHERIQUE.
!
! SMIL CHAMPION DEC 78
! MOD. 5 / 12 / 1992 T.GABARD : ANCIEN ELEMENT MULTIPLIE PAR
! UNE PHASE (-1)**(KR)
!
      FUNCTION EMRRO(IMEGA,KR,J)
      use mod_dppr
      use mod_com_fa
      IMPLICIT NONE
      real(kind=dppr)  :: EMRRO
      integer          :: IMEGA,KR,J

      real(kind=dppr)  :: A
      real(kind=dppr)  :: B
      real(kind=dppr)  :: C
      real(kind=dppr)  :: D

      integer          :: I,IA
      integer          :: J2MK,J2P2,J2PK1
      integer          :: K,K2M1
!
      J2P2  = 2*J+2
      J2PK1 = 2*J+KR+1
      K2M1  = 2*KR-1
      J2MK  = 2*J-KR
      C     = 1.D0
      B     = 1.D0
      IF( J2MK .LE. 1 ) GOTO 2
      A  = FACT(J2PK1)/FACT(J2MK)
      IA = KFAC(J2PK1)-KFAC(J2MK)
      GOTO 3
!
2     A  = FACT(J2PK1)
      IA = KFAC(J2PK1)
!
3     A = A*( (10.D0)**IA )
      IF( KR .LE. 1 ) GOTO 6
      DO K=1,K2M1,2
        B = B*DBLE(K)
      ENDDO
      C = FACT(KR)*( (10.D0)**KFAC(KR) )
!
6     D = SQRT(A/B*C)
      I = (IMEGA-KR)/2
      IF( I .GT. 0 ) GOTO 8
      EMRRO = D
      EMRRO = EMRRO*( (-1)**KR )
      RETURN
!
8     EMRRO = D*( (-2.D0*DBLE(J*J2P2)/SQRT(3.D0))**I )
      EMRRO = EMRRO*( (-1)**KR )
!
      RETURN
      END FUNCTION EMRRO
