!
! *** CALCUL DES 6C , STOCKAGE DES CODES ET DES VALEURS
!
      SUBROUTINE CAL6C
      use mod_dppr
      use mod_par_tds
      use mod_com_com6c
      IMPLICIT NONE

! functions
      real(kind=dppr)  :: SXC1
      integer          :: CTR

      integer          :: IC1,IC2,IC3,IC4,IC5,IC6
      integer          :: N
!
      N = 0
      DO IC1=1,MXSYM
        DO IC2=1,MXSYM
E3:       DO IC3=1,MXSYM
            IF( CTR(IC1,IC2,IC3) .EQ. 0 ) CYCLE E3
            DO IC4=1,MXSYM
E5:           DO IC5=1,MXSYM
                IF( CTR(IC4,IC5,IC3) .EQ. 0 ) CYCLE E5
E6:             DO IC6=1,MXSYM
                  IF( CTR(IC4,IC2,IC6)*CTR(IC1,IC5,IC6) .EQ. 0 ) CYCLE E6
                  N      = N+1
                  I6C(N) = 100000*IC1+10000*IC2+1000*IC3+100*IC4+10*IC5+IC6
                  V6C(N) = SXC1(IC1,IC2,IC3,IC4,IC5,IC6)
                ENDDO E6
              ENDDO E5
            ENDDO
          ENDDO E3
        ENDDO
      ENDDO
      CALL DEBUG( 'CAL6C  => NB6C=',N)
!
      RETURN
      END SUBROUTINE CAL6C
