!
!  EXTRAIRE, PAR TRI DICHOTOMIQUE, UN K CALCULE PAR CALKP.
!  S'IL N'EST PAS DANS LE TABLEAU, ON LE RESTITUE COMME 0.
!  POUR UTILISATION DANS LE PROGRAMME POL_MATRIX.F
!
! REV    JAN 1995 JPC,CW (PARAMETER)
!
      FUNCTION ODKP(J2,J3,N2,N3,IC1,IC2,IC3)
      use mod_dppr
      use mod_com_ckdipo
      use mod_com_ckpo
      use mod_com_sy
      IMPLICIT NONE
      real(kind=dppr)  :: ODKP
      integer          :: J2,J3,N2,N3,IC1,IC2,IC3

      real(kind=dppr)  :: PHAS

      integer          :: I,I1,I2,ICR2,ICR3,IODKXC
      integer          :: ISYM
      integer          :: JDIFF,JR3
      integer          :: NR2,NR3
!
8000  FORMAT(' !!! ODKP   : STOP ON ERROR')
8100  FORMAT(' !!! ODKP : SYMMETRY ERROR')
8101  FORMAT(' !!! ODKP : DELTA J ERROR')
!
      IF    ( IC1 .EQ. 3 ) THEN
        ISYM = 1
      ELSEIF( IC1 .EQ. 5 ) THEN
        ISYM = 2
      ELSE
        GOTO 9998
      ENDIF
      JDIFF = ABS(J2-J3)
      IF( JDIFF .NE. 1 .AND.              &
          JDIFF .NE. 2       ) GOTO 9997
      IF( J2    .GT. J3      ) THEN
        JR3  = J2
        NR2  = N3
        NR3  = N2
        ICR2 = IC3
        ICR3 = IC2
! J1 = 2, IC1 = E OU F2
        PHAS = ( (-1)**(J2+J3) )*PC(IC2)*PC(IC3)
      ELSE
        JR3  = J3
        NR2  = N2
        NR3  = N3
        ICR2 = IC2
        ICR3 = IC3
        PHAS = 1.D0
      ENDIF
      ODKP   = 0.D0
      IODKXC = 100000*ICR3+10000*ICR2+100*NR3+NR2
      I1     = INJODK(JDIFF,ISYM,JR3)
      I2     = INJODK(JDIFF,ISYM,JR3+1)-1
      IF( I2     .EQ. 0                    ) RETURN
      IF( IODKXC .LT. IODKX(JDIFF,ISYM,I1) ) RETURN
      IF( IODKXC .GT. IODKX(JDIFF,ISYM,I2) ) RETURN
      IF( IODKXC .EQ. IODKX(JDIFF,ISYM,I2) ) GOTO 4
      IF( I1     .EQ. I2                   ) RETURN
!
1     I = (I1+I2)/2
      IF    ( IODKXC .EQ. IODKX(JDIFF,ISYM,I) ) THEN
        GOTO 5
      ELSEIF( IODKXC .GT. IODKX(JDIFF,ISYM,I) ) THEN
        GOTO 3
      ENDIF
      IF( I2-I1 .EQ. 1 ) RETURN
      I2 = I
      GOTO 1
!
3     IF( I2-I1 .EQ. 1 ) RETURN
      I1 = I
      GOTO 1
!
4     I    = I2
!
5     ODKP = VODK(JDIFF,ISYM,I)*PHAS
      RETURN
!
9997  PRINT 8101
      GOTO  9999
9998  PRINT 8100
9999  PRINT 8000
      PRINT *
      STOP
      END FUNCTION ODKP
