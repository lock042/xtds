!
! SMIL CHAMPION MAI 82, MOD JAN. 83
! MOD. DEC 92 T.GABARD
!
      FUNCTION A23(IPM,VP,LP,CP,K,GAMA,V,L,C)
      use mod_dppr
      use mod_com_sy
      IMPLICIT NONE
      real(kind=dppr)  :: A23
      integer          :: IPM,VP,LP,CP,K,GAMA,V,L,C

! functions
      real(kind=dppr)  :: A21,A22,SXC
      integer          :: IPSI

      real(kind=dppr)  :: E

      integer          :: CS
      integer          :: GAMAS
      integer          :: LD,LS
      integer          :: VS
!
      A23 = 0.D0
      IF( IPSI(2,3,K,0,GAMA) .EQ. 0 ) RETURN
      IF( IPM*(VP-V)         .NE. 3 ) RETURN
      VS = V+IPM
      DO LD=-1,1,2
        LS = L+LD
E1:     DO CS=1,3
          IF( IPSI(2,VS,LS,0,CS) .EQ. 0 ) CYCLE E1
          GAMAS = K
          E     = A22(IPM,VP,LP,CP,GAMAS,VS,LS,CS)*A21(IPM,VS,LS,CS,V,L,C)
          IF( E .EQ. 0.D0 ) CYCLE E1
          A23 = A23+E*PC(C)*PC(CP)*PC(GAMAS)*SXC(GAMAS,3,GAMA,C,CP,CS)
        ENDDO E1
      ENDDO
      A23 = A23*SQRT(DC(GAMA))
!
      RETURN
      END FUNCTION A23
