!
! SMIL CHAMPION MAI 82 ,MOD. JAN 83
! MOD. DEC 92 T.GABARD
!
      FUNCTION A22(IPM,VP,LP,CP,GAMA,V,L,C)
      use mod_dppr
      use mod_com_sy
      IMPLICIT NONE
      real(kind=dppr)  :: A22
      integer          :: IPM,VP,LP,CP,GAMA,V,L,C

! functions
      real(kind=dppr)  :: A21,SXC
      integer          :: IPSI

      real(kind=dppr)  :: E

      integer          :: CS
      integer          :: LD,LS
      integer          :: VS
!
      A22 = 0.D0
      IF( GAMA       .NE. 1 .AND.           &
          GAMA       .NE. 3       ) RETURN
      IF( IPM*(VP-V) .NE. 2       ) RETURN
      VS = V+IPM
E2:   DO LD=-1,1,2
        LS = L+LD
        IF( ABS(LP-LS) .NE. 1 ) CYCLE E2
E1:     DO CS=1,3
          IF( IPSI(2,VS,LS,0,CS) .EQ. 0 ) CYCLE E1
          E = A21(IPM,VP,LP,CP,VS,LS,CS)*A21(IPM,VS,LS,CS,V,L,C)
          IF( E .EQ. 0.D0 ) CYCLE E1
          A22 = A22+E*PC(C)*PC(CP)*SXC(3,3,GAMA,C,CP,CS)
        ENDDO E1
      ENDDO E2
      A22 = A22*SQRT(DC(GAMA))
!
      RETURN
      END FUNCTION A22
