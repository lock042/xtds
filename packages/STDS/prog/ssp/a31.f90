!
! SMIL CHAMPION  MAI 82,MOD JAN.83
! MOD. DEC 92 T.GABARD
!
!   ELEMENTS MATRICIELS REDUITS DE L'OSCILLATEUR TRIPLEMENT DEGENERE
!     IO3TD   =  0    ELEMENT MATRICIEL REDUIT DANS TD
!             =  1    ELEMENT MATRICIEL REDUIT DANS O(3)
!
      FUNCTION A31(IPM,VVP,LLP,NNP,CCP,VV,LL,NN,CC,IO3TD)
      use mod_dppr
      use mod_com_sy
      IMPLICIT NONE
      real(kind=dppr)  :: A31
      integer          :: IPM,VVP,LLP,NNP,CCP,VV,LL,NN,CC,IO3TD

! functions
      real(kind=dppr)  :: XMKUB
      integer          :: IPSI

      real(kind=dppr)  :: AK,AVA31

      integer          :: C,CP
      integer          :: IUG,IUGP
      integer          :: L,LP
      integer          :: N,NP
      integer          :: V,VP
!
      A31 = 0.D0
      IF( IPM .NE. -1 ) GOTO 10
      VP = VVP
      LP = LLP
      NP = NNP
      CP = CCP
      V  = VV
      L  = LL
      N  = NN
      C  = CC
      GOTO 20
!
10    IF( IPM .NE. 1 ) RETURN
      VP = VV
      LP = LL
      NP = NN
      CP = CC
      V  = VVP
      L  = LLP
      N  = NNP
      C  = CCP
!
20    IF( V-VP                                .NE. 1 ) RETURN
      IF( IPSI(3,VP,LP,NP,CP)*IPSI(3,V,L,N,C) .EQ. 0 ) RETURN
      IF( L-LP                                .NE. 1 ) GOTO 30
      AVA31 = (V+L+1)*L
      A31   = -SQRT(AVA31)
      GOTO 40
!
30    IF( L-LP .NE. -1 ) RETURN
      AVA31 = (V-L)*(L+1)
      A31   = -SQRT(AVA31)
!
40    IF( IPM   .EQ. 1 ) A31 = -A31
      IF( IO3TD .NE. 0 ) RETURN
      IUG  = MOD(L,2)+1
      IUGP = MOD(LP,2)+1
      AK   = XMKUB(1,L,LP,2,IUG,IUGP,0,N,NP,5,C,CP)
      A31  = ( (-1)**LP )*AK*A31
      IF( IPM .EQ. 1 ) A31 = -A31*PC(C)*PC(CP)
!
      RETURN
      END FUNCTION A31
