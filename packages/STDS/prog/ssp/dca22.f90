!
!                                            (C1,C2 ,F2 ,A2 )
! *** CALCUL DES SYMBOLES 12C DU TYPE        (C5,C6 ,C7 ,C8 )
!                                            (C9,A1 ,F1 ,C12)
!
! *** METHODE ' ELLIOTT '
!
      FUNCTION DCA22(C1,C2,C5,C6,C7,C8,C9,C12)
      use mod_dppr
      use mod_com_sy
      IMPLICIT NONE
      real(kind=dppr)  :: DCA22
      integer          :: C1,C2,C5,C6,C7,C8,C9,C12

! functions
      real(kind=dppr)  :: SXC
      integer          :: CTR

      real(kind=dppr)  :: B
      real(kind=dppr)  :: SIGN

      integer          :: CS
!
!  INDICE DE SOMMATION
!
      CS = 3+3*(C8/3)+3*(C8/4)-C8
!
!  RELATIONS TRIANGULAIRES DU 12C
!
      IF( C2            .NE. C6  ) GOTO 100
      IF( C8            .NE. C12 ) GOTO 100
      IF( CS            .NE. C7  ) GOTO 100
      IF( CTR(C1,C2,5)  .EQ. 0   ) GOTO 100
      IF( CTR(C5,C6,C7) .EQ. 0   ) GOTO 100
      IF( CTR(C1,C5,C9) .EQ. 0   ) GOTO 100
      IF( CTR(C9,4,C8)  .EQ. 0   ) GOTO 100
!
!  RELATIONS TRIANGULAIRES LIEES A LA METHODE ELLIOTT
!
      IF( CTR(CS,C9,5)  .EQ. 0 ) GOTO 100
      IF( CTR(CS,C2,C5) .EQ. 0 ) GOTO 100
      B     = DC(CS)*SXC(C2,C5,CS,C9,5,C1)*SXC(C9,5,CS,2,C8,4)
      SIGN  = PC(C1)*PC(C6)*PC(C7)*PC(C9)*PC(C12)*PC(CS)
      SIGN  = -SIGN
      DCA22 = B*SIGN/SQRT(DC(C2)*DC(C8))/DC(CS)
      RETURN
!
100   DCA22 = 0.D0
!
      RETURN
      END FUNCTION DCA22
