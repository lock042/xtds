!
!  EXTRAIRE, PAR TRI DICHOTOMIQUE DOUBLE, UN K CALCULE PAR CALKM.
!  S'IL N'EST PAS DANS LE TABLEAU, ON LE RESTITUE COMME 0.
!     POUR UTILISATION DANS HMODEL
!     JMAXK LIMITE A 9 (CF. CALKM.F)
!
!  REV 29 SEP 94
! REV    JAN 1995 JPC,CW (PARAMETER)
!
      FUNCTION XMKUB(J1I,J2I,J3I,IUG1I,IUG2I,IUG3I,N1I,N2I,N3I,IC1I,IC2I,IC3I)
      use mod_dppr
      use mod_com_ckmo
      use mod_com_sy
      IMPLICIT NONE
      real(kind=dppr)  :: XMKUB
      integer          :: J1I,J2I,J3I,IUG1I,IUG2I,IUG3I,N1I,N2I,N3I,IC1I,IC2I,IC3I
!
! INDICE NLI POUR J ET IUG
! INDICE NCO POUR N ET IC
!
      real(kind=dppr)  :: PHASE,PHASE1

      integer ,dimension(3,4)  :: IRD
      integer          :: I,I1,I2,IC1,IC2,IC3,IKJCC,IKNCC,IPERM
      integer          :: IUG1,IUG2,IUG3
      integer          :: J1,J2,J3
      integer          :: N1,N2,N3,NLI
!
      XMKUB    = 0.D0
      PHASE1   = (-1)**(J1I + J2I + J3I)
      PHASE1   = PHASE1*PC(IC1I)*PC(IC2I)*PC(IC3I)
      IRD(1,1) = J1I
      IRD(2,1) = J2I
      IRD(3,1) = J3I
      IRD(1,2) = IUG1I
      IRD(2,2) = IUG2I
      IRD(3,2) = IUG3I
      IRD(1,3) = N1I
      IRD(2,3) = N2I
      IRD(3,3) = N3I
      IRD(1,4) = IC1I
      IRD(2,4) = IC2I
      IRD(3,4) = IC3I
      CALL RENGE(IRD,3,4,IPERM)
      J1   = IRD(1,1)
      J2   = IRD(2,1)
      J3   = IRD(3,1)
      IUG1 = IRD(1,2)
      IUG2 = IRD(2,2)
      IUG3 = IRD(3,2)
      N1   = IRD(1,3)
      N2   = IRD(2,3)
      N3   = IRD(3,3)
      IC1  = IRD(1,4)
      IC2  = IRD(2,4)
      IC3  = IRD(3,4)
      IF( IPERM .EQ. -1 ) THEN
        PHASE = PHASE1
      ELSE
        PHASE = 1.D0
      ENDIF
!
! CODAGES
!
      IKJCC = 100000*J1 +10000*J2 +1000*J3 +100*IUG1+10*IUG2+IUG3
      IKNCC = 100000*IC1+10000*IC2+1000*IC3+100*N1  +10*N2  +N3
      I1    = 1
      I2    = NBKLI
      IF( IKJCC .LT. IKJUG(I1) ) RETURN
      IF( IKJCC .GT. IKJUG(I2) ) RETURN
      IF( IKJCC .EQ. IKJUG(I2) ) GOTO 4
!
1     I = (I1+I2)/2
      IF    ( IKJCC .EQ. IKJUG(I) ) THEN
        GOTO 5
      ELSEIF( IKJCC .GT. IKJUG(I) ) THEN
        GOTO 3
      ENDIF
      IF( I2-I1 .EQ. 1 ) RETURN
      I2 = I
      GOTO 1
!
3     IF( I2-I1 .EQ. 1 ) RETURN
      I1 = I
      GOTO 1
!
4     I = I2
!
5     NLI = I
      I1  = 1
      I2  = NBKCO
      IF( IKNCC .LT. IKINIC(I1) ) RETURN
      IF( IKNCC .GT. IKINIC(I2) ) RETURN
      IF( IKNCC .EQ. IKINIC(I2) ) GOTO 14
!
11    I = (I1+I2)/2
      IF    ( IKNCC .EQ. IKINIC(I) ) THEN
        GOTO 15
      ELSEIF( IKNCC .GT. IKINIC(I) ) THEN
        GOTO 13
      ENDIF
      IF( I2-I1 .EQ. 1 ) RETURN
      I2 = I
      GOTO 11
!
13    IF( I2-I1 .EQ. 1 ) RETURN
      I1 = I
      GOTO 11
!
14    I = I2
!
15    XMKUB = PHASE*VK(NLI,I)
!
      RETURN
      END FUNCTION XMKUB
