!
! SMIL CHAMPION  MAI 82,MOD. JAN. 83
! MOD. DEC 92 T.GABARD
!
      FUNCTION A33(IPM,VP,LP,NP,CP,K,GAMA,V,L,N,C,IO3TD)
      use mod_dppr
      use mod_par_tds
      IMPLICIT NONE
      real(kind=dppr)  :: A33
      integer          :: IPM,VP,LP,NP,CP,K,GAMA,V,L,N,C,IO3TD

! functions
      real(kind=dppr)  :: A31,A32,SXJ,XMKUB
      integer          :: IPSI

      real(kind=dppr)  :: AK
      real(kind=dppr)  :: E

      integer          :: CS
      integer          :: GAMAI
      integer          :: IUG,IUGP
      integer          :: KI
      integer          :: LD,LS
      integer          :: VS
!
      A33 = 0.D0
      IF( IO3TD              .NE. 0 .AND.           &
          IPSI(3,3,K,0,GAMA) .EQ. 0       ) RETURN
      IF( IPM*(VP-V)         .NE. 3       ) RETURN
      KI    = K-1
      GAMAI = K
      VS    = V+IPM
E2:   DO LD=-1,1,2
        LS = L+LD
        DO CS=1,MXSYM
          IF( IPSI(3,VS,LS,0,CS) .NE. 0 ) GOTO 20
        ENDDO
        CYCLE E2
!
20      E = A32(IPM,VP,LP,NP,CP,KI,GAMAI,VS,LS,0,CS,1)*  &
            A31(IPM,VS,LS,0,CS,V,L,N,C,1)
        IF( E .EQ. 0.D0 ) CYCLE E2
        A33 = A33-E*( (-1)**(L+LP+K+KI+1) )*SXJ(KI,1,K,L,LP,LS)
      ENDDO E2
      A33 = A33*SQRT(2.D0*K+1.D0)
      IF( IO3TD .NE. 0 ) RETURN
      IUG  = MOD(L,2)+1
      IUGP = MOD(LP,2)+1
      AK   = XMKUB(K,L,LP,2,IUG,IUGP,0,N,NP,GAMA,C,CP)
      A33  = ( (-1)**LP )*AK*A33
!
      RETURN
      END FUNCTION A33
