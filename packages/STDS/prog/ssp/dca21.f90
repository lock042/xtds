!
!                                            (C1,C2 ,F2 ,A2 )
! *** CALCUL DES SYMBOLES 12C DU TYPE        (C5,A1 ,C7 ,C8 )
!                                            (C9,C10,F1 ,C12)
!
! *** METHODE ' ELLIOTT '
!
      FUNCTION DCA21(C1,C2,C5,C7,C8,C9,C10,C12)
      use mod_dppr
      use mod_com_sy
      IMPLICIT NONE
      real(kind=dppr)  :: DCA21
      integer          :: C1,C2,C5,C7,C8,C9,C10,C12

! functions
      real(kind=dppr)  :: SXC
      integer          :: CTR

      real(kind=dppr)  :: B
      real(kind=dppr)  :: SIGN

      integer          :: CS
!
!  INDICE DE SOMMATION
!
      CS = 3+3*(C8/3)+3*(C8/4)-C8
!
!  RELATIONS TRIANGULAIRES DU 12C
!
      IF( C5              .NE. C7  ) GOTO 100
      IF( C2              .NE. C10 ) GOTO 100
      IF( CTR(C1,C2,5)    .EQ. 0   ) GOTO 100
      IF( CTR(C1,C5,C9)   .EQ. 0   ) GOTO 100
      IF( CTR(C9,4,C8)    .EQ. 0   ) GOTO 100
      IF( CTR(C8,C12,C10) .EQ. 0   ) GOTO 100
!
!  RELATIONS TRIANGULAIRES LIEES A LA METHODE ELLIOTT
!
      IF( CTR(CS,C7,C10) .EQ. 0 ) GOTO 100
      IF( CTR(CS,C9,5)   .EQ. 0 ) GOTO 100
      IF( CTR(CS,C2,C5)  .EQ. 0 ) GOTO 100
      B     = DC(CS)*SXC(C2,C5,CS,C9,5,C1)*  &
                     SXC(C9,5,CS,2,C8,4)*    &
                     SXC(2,C8,CS,C10,C7,C12)
      SIGN  = PC(C1)*PC(C2)*PC(C5)*PC(C8)*PC(C9)*PC(C12)
      DCA21 = B*SIGN/SQRT(DC(C10)*DC(C7))
      RETURN
!
100   DCA21 = 0.D0
!
      RETURN
      END FUNCTION DCA21
