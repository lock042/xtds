      PROGRAM PARADJ
!
!  AJUSTEMENT DES PARAMETRES A PARTIR D'UN JEU D'EQUATIONS NORMALES
!  TOUS TYPES DE PARAMETRES POSSIBLES
!  CONTRAINTES LINEAIRES POSSIBLES
!      DANS CE CAS
!  DES CONTRIBUTIONS SUIVENT EVENTUELLEMENT LES PARAMETRES SUR LA OU LES LIGNES SUIVANTES .
!  LES  COMBINAISONS DOIVENT ETRE STRUCTUREES DE LA FACON SUIVANTE :
!  COEF*NO_PARA COEF*NO_PARA ...
!
!  GLOSSAIRE :
!    STD  : PARAMETRES STANDARDS
!    CBN  : PARAMETRES COMBINES
!    AJ   : PARAMETRES AJUSTABLES H -> 1ER  JEU
!                                 T -> 2EME JEU
!    FX   : PARAMETRES FIXES      H -> 2EME JEU
!                                 T -> 1ER  JEU
!    MXFI : NB MAX DE FICHIERS (EQUATIONS NORMALES)
!      ATTENTION  -> FORMAT 1008, FORMAT 1013 ET TITRE, ASSOCIES A MXFI.
!
!  REV  9  FEVRIER  1990   C.WENGER  J.P.CHAMPION
! REV    JAN 1995 JPC,CW (PARAMETER)
! REV    DEC 2002 V. BOUDON : UTILISATION DE CL_eqtds ET CL_eqint VENANT DE EQ_TDS ET EQ_INT
!
! APPEL : paradj  H/T  NQ1 FPARA1 [NQ2 FPARA2 ... NQ10 FPARA10]
!
      use mod_dppr
      use mod_par_tds
      use mod_com_arond
      use mod_com_fdate
      use mod_main_paradj
      IMPLICIT NONE

      real(kind=dppr)  :: ES
      real(kind=dppr)  :: POIDSP,PSTDC
      real(kind=dppr)  :: SIGMA2,SIGMAN,SPE2,SPOBS
      real(kind=dppr)  :: XLIB
      real(kind=dppr)  :: VP

      integer ,dimension(9)  :: ICOR,NUCOR                                                         ! cf. FORMAT 1011
      integer                :: I,ICO,IEXP,II,IIPA,IPCBN,IPORG,IPSTD,IXCLT
      integer                :: J,JJ,JMAX,JPCBN,JPSTD
      integer                :: K
      integer                :: L
      integer                :: NBC,NBCNST,NBEC,NBECM1,NBFI,NBOP,NBOPC,NBOPHI
      integer                :: NBOPM,NCL1,NCL2,NCO,NDEB,NFIN,NFIT,NOAJM
      integer                :: NOFI,NPARAV,NSTAR,NUMPI,NUMPN

      character(len = NBCLAB+10)  :: CHAINE,COPAJC
      character(len = NBCTIT)     :: TITRE
      character(len =   8)        :: CTRP
      character(len = 120)        :: FPARI,NOM
      character(len = 124)        :: NEQFIA

      logical          :: TRANS
!
1000  FORMAT(A)
1001  FORMAT(A,E18.11,E14.7)
1002  FORMAT(I5,' Data ; Jmax ',I3,' ; St Dev previous',F8.3,' , predicted',F8.3)
1003  FORMAT(I4,A)
1004  FORMAT(A,    'Hmn  Frdm         Value/cm-1  St.Dev./cm-1')
1005  FORMAT(I2,4X,I5,' Data ; Jmax ',I3,' ; R.M.S. =',F8.3,4X,A)
1006  FORMAT(A,3X,F7.1,' %   bound within ',E18.11,' +/-',E14.7)
1007  FORMAT(A,3X,F7.1,' %   damped')
1008  FORMAT(A,4X,A,I5,100(F6.1))                                                                  ! 100 CAR MXFI inconnu
1009  FORMAT(A,'     100.0 %   fixed to     ',E18.11)
1010  FORMAT(A,'    ( ',F16.10,22X,') x10',I3)
1011  FORMAT(A,7X,9(I5,I4,'%'))                                                                    ! cf. dimension de ICOR,NUCOR
1012  FORMAT(1X,I2,4X,I5,' Data ; Jmax ',I3,' ; R.M.S. =',F8.3,4X,A)
1013  FORMAT(/,                                                                                        &
             'PARAMETER ESTIMATES'                                                               ,/,   &
             13X,'Parameter',29X,'Value(St.Dev)    Variation',10X,'Frdm Relative weights of Data',/,   &
              90X,100(I6)                                                                        ,/ )        ! 100 CAR MXFI inconnu
1014  FORMAT('No type 2(bound)/3(damped) constraint',/)
1015  FORMAT(A,'     100.0 %   fixed to 0')
1016  FORMAT(A,'     100.0 %   fixed to inital value')
1020  FORMAT(/,              &
             'PARADJ : ',A)
1100  FORMAT('DATA DETAILS',/)
1101  FORMAT(/,                        &
             'PARAMETER CONSTRAINTS')
1102  FORMAT(43X,'Constraint')
1103  FORMAT(13X,'Parameter',18X,'Weight   Description',/)
1104  FORMAT(/,                                            &
             I5,' Data fitted : Standard Deviation',F8.3)
1105  FORMAT(E9.2)
1106  FORMAT(I3)
1107  FORMAT(/,                                               &
             'Predicted Standard Deviation of the fit',F8.3)
1108  FORMAT(/,                          &
             'Correlations ( >85% )',/)
1109  FORMAT(////,   &
             I4,//)
1110  FORMAT(I4)
1111  FORMAT(I1)
2000  FORMAT(' <  ',A)
2001  FORMAT(' >> ',A)
8000  FORMAT(' !!! PARADJ : STOP ON ERROR')
8001  FORMAT(' !!! ERROR OPENING CONTROL FILE')
8002  FORMAT(' !!! INCOMPLETE CONTROL FILE')
8003  FORMAT(' !!! NO ASSIGNMENT IN ',A)
8122  FORMAT(' !!! NBOPC TOO LARGE'               ,/,   &
             ' !!! NBOP   EXCEEDED : ',I8,' > ',I8,/,   &
             ' !!! IN ',A                            )
8123  FORMAT(' !!! NBOP : ',I3,' > ',I3,' : MAX NBOP OF NORMAL EQUATIONS')
8124  FORMAT(' !!! INCOMPATIBLE PARAMETERS TO FIT IN : ',A)
!
! ARRAY INITIAL ALLOCATION
!
      CALL ALLOC_ALL
!
      NOAJM = 0                                                                                    ! INITIALISATION PAR DEFAUT
      NCL1  = NBCLAB+1+NBAM+1                                                                      ! Hmn  included
      NCL2  = NBCLAB+1+NBAM+1+6                                                                    ! Frdm included
!
!     INITIALISATIONS
!
      SPOBSC(0) = 0.D0
      SPE2C(0)  = 0.D0
      DO IIPA=1,MXOPH
        IXCL(IIPA) = 0
      ENDDO
      OPEN(10,FILE=CTRLF,ERR=9998,STATUS='OLD')
      READ(10,1000,END=9997) FDATE
      PRINT 1020,            FDATE
!
!  LIRE LE TYPE DE PARAS A AJUSTER
!
      READ(10,1000,END=9997) NOM
      IF    ( NOM .EQ. 'T' ) THEN
        TRANS = .TRUE.                                                                             ! PARAS TRANSITION
        CTRP  = 'CL_eqint'
      ELSEIF( NOM .EQ. 'H' ) THEN
        TRANS = .FALSE.                                                                            ! PARAS HAMILTONIEN
        CTRP  = 'CL_eqtds'
      ELSE
        GOTO 9997
      ENDIF
!
!  LIRE LE FICHIER DE CONTROLE DES PARAMETRES
!
      PRINT 2000,  TRIM(CTRP)
      OPEN(15,FILE=TRIM(CTRP),STATUS='OLD')
! CTRP : codes de controle > 1
      READ(15,1109) NBOP
      DO IIPA=1,NBOP
        READ(15,1110) IXCLT
        DO WHILE( IXCLT .GT. MXOPH )
          CALL RESIZE_MXOPH
        ENDDO
        IXCL(IXCLT) = IIPA
        READ(15,1000,END=6002) TITRE
        NSTAR = INDEX(TRIM(TITRE),'*')
        IF( NSTAR .LT. 1 ) THEN
          BACKSPACE(15)
        ENDIF
6002    CONTINUE
      ENDDO
      REWIND(15)
      READ(15,1109) NBOP
      DO WHILE( NBOP .GT. MXOPH )
        CALL RESIZE_MXOPH
      ENDDO
      DO I=1,NBOP
        PCBN(I)    = 0.D0
        PSTDAJ(I)  = 0.D0
        ETPSTD(I)  = 0.D0
        POIDS(I,0) = 0.D0
        DO J=1,NBOP
          ASTDS(I,J) = 0.D0
          VSTDS(I,J) = 0.D0
          ASTDI(I,J) = 0.D0
          VSTDI(I,J) = 0.D0
        ENDDO
      ENDDO
E1:   DO IPCBN=1,NBOP                                                                              ! POUR CHAQUE PARA
        READ(15,1001) COPAJ(IPCBN),VALAT(IPCBN),PRAT(IPCBN)
        READ(COPAJ(IPCBN)(NCL2:NCL2),1111) ICNTRL(IPCBN)                                           ! ETAT DU PARA
        DO IPSTD=1,NBOP                                                                            ! INITIALISER CTRL(CBN,STD)
          CTRL(IPCBN,IPSTD) = 0.D0
        ENDDO
        CTRL(IPCBN,IPCBN) = 1.D0
!
11      READ(15,1000,END=1) TITRE                                                                  ! LIRE UNE LIGNE EVENTUELLE DE DEPENDANCES
        NFIN  = LEN_TRIM(TITRE)
        NSTAR = INDEX(TITRE(:NFIN),'*')
        IF( NSTAR .LT. 1 ) THEN                                                                    ! PAS UNE LIGNE DE CONTRIBUTION
          BACKSPACE(15)
          CYCLE E1
        ENDIF
        OPEN(11,STATUS='SCRATCH')
        NDEB = 1
        GOTO 12
!
3       NSTAR = NDEB-1+INDEX(TITRE(NDEB:NFIN),'*')
        IF( NSTAR .LT. NDEB ) THEN                                                                 ! PAS D AUTRE DEPENDANCE SUR CETTE LIGNE
          CLOSE(11)
          GOTO 11
        ENDIF
!
12      REWIND(11)
        WRITE(11,1000) TITRE(NSTAR+1:NFIN)
        WRITE(11,1000) TITRE(NDEB:NSTAR-1)
        REWIND(11)
        READ(11,*) II                                                                              ! # DU PARA
        READ(11,*) CTRL(IPCBN,IXCL(II))                                                            ! COEFFICIENT
        NDEB = NSTAR+INDEX(TITRE(NSTAR+1:),' ')                                                    ! DEBUT DE LA CONTRIBUTION SUIVANTE
        IF( NDEB .LT. NFIN ) GOTO 3                                                                ! SUITE
        CLOSE(11)
1       CONTINUE
      ENDDO E1
!
!  CREATION DE LA MATRICE CTRL REDUITE
!
      NBEC = 0                                                                                     ! NB D ELEMENTS NON-NULS DE CTRL
      DO IPCBN=1,NBOP
        DO IPSTD=1,NBOP
          IF( CTRL(IPCBN,IPSTD) .NE. 0.D0 ) THEN
            NBEC     = NBEC+1
            IC(NBEC) = IPCBN
            JC(NBEC) = IPSTD
            VC(NBEC) = CTRL(IPCBN,IPSTD)
          ENDIF
        ENDDO
      ENDDO
      CALL GAUJD(NBOP,CTRL,YSTDC,CTRLM1,DPCBN)                                                     ! INVERSER CTRL(CBN,STD) -> CTRLM1(STD,CBN)
!
!  CREATION DE LA MATRICE CTRLM1 REDUITE
!
      NBECM1 = 0                                                                                   ! NB D ELEMENTS NON-NULS DE CTRL
      DO IPSTD=1,NBOP
        DO IPCBN=1,NBOP
          IF( CTRLM1(IPSTD,IPCBN) .NE. 0.D0 ) THEN
            NBECM1       = NBECM1+1
            ICM1(NBECM1) = IPSTD
            JCM1(NBECM1) = IPCBN
            VCM1(NBECM1) = CTRLM1(IPSTD,IPCBN)
          ENDIF
        ENDDO
      ENDDO
!
!  LECTURE DES FICHIERS D'EQUATIONS NORMALES PARTIELLES
!
      DO IPCBN=1,NBOP                                                                              ! YCBNS=0. , ACBNS=0.
        YCBNS(IPCBN) = 0.D0
        DO JPCBN=1,NBOP
          ACBNS(IPCBN,JPCBN) = 0.D0
        ENDDO
      ENDDO
      NBOPM = 0                                                                                    ! NBOP MAX DES DIFFERENTS OP. A AJUSTER.
!
! A AJUSTER
!
      NOFI = 0
5       READ(10,1000,END=404) NOM                                                                  ! POUR TOUS LES FICHIERS
        NOFI = NOFI + 1
        IF( NOFI .GT. MXFI ) CALL RESIZE_MXFI
        NEQFI(NOFI) = NOM
        PRINT 2000,  TRIM(NOM)
        OPEN(12,FILE=TRIM(NOM),STATUS='OLD',FORM='UNFORMATTED')
        READ(10,1000,END=9997) NOM
        PRINT 2000,            TRIM(NOM)
        FPARA(NOFI) = NOM
        IF( TRANS ) THEN
          DO I=1,4
            READ(12)
          ENDDO
          READ(12) NBOPC
          DO I=1,NBOPC+2
            READ(12)
          ENDDO
        ENDIF
        DO I=1,4                                                                                   ! SAUVER L ENTETE
          READ(12) TITRAJ(I,NOFI)
        ENDDO
        READ(12) NBOPC,TITRAJ(5,NOFI)
        IF( NBOPC .GT. NBOP ) THEN                                                                 ! VALIDER NBOPC
          PRINT 8122, NBOPC,NBOP,TRIM(NOM)
          GOTO  9999
        ENDIF
        DO L=6,7
          READ(12) TITRAJ(L,NOFI)
        ENDDO
        DO IPSTD=1,NBOPC
          READ(12) CHAINE,PSTDC
          IF( IPSTD .GT. NBOPM ) THEN
            PSTD(IPSTD)   = PSTDC                                                                  ! STOCKER LES PARAS
            COPAJS(IPSTD) = CHAINE
          ELSE
            IF( PSTD(IPSTD)          .NE. PSTDC         ) GOTO 9996                                ! VALIDER LES VALEURS DES PARAS
            IF( COPAJS(IPSTD)(:NCL1) .NE. CHAINE(:NCL1) ) GOTO 9996                                ! VALIDER LES NOMS DES PARAS
          ENDIF
        ENDDO
        IF( NBOPC .GT. NBOPM ) THEN
          NBOPM = NBOPC
          NOAJM = NOFI                                                                             ! # DU FICHIER AU NBOPM
        ENDIF
        NBOPAJ(NOFI) = NBOPC
!
!    FIXES
!
        IF( TRANS ) REWIND(12)
        DO L=1,4                                                                                   ! LIRE LES INFOS FIXES ET LES STOCKER
          READ(12) TITRFX(L,NOFI)
        ENDDO
        READ(12) NBOPFX(NOFI),TITRFX(5,NOFI)
        DO WHILE( NBOPFX(NOFI) .GT. MXOPH )
          CALL RESIZE_MXOPH
        ENDDO
        DO L=6,7
          READ(12) TITRFX(L,NOFI)
        ENDDO
        DO L=1,NBOPFX(NOFI)
          READ(12) CHAIFX(L,NOFI),PARAFX(L,NOFI),PREDFX(L,NOFI)
        ENDDO
!
!    LIRE LES EQUATIONS NORMALES PARTIELLES
!
        IF( TRANS ) THEN
          DO I=1,4
            READ(12)
          ENDDO
          READ(12) NBOPC
          DO I=1,NBOPC+2
            READ(12)
          ENDDO
        ENDIF
        DO IPSTD=1,NBOP                                                                            ! YSTDC = ASTDC = 0.
          YSTDC(IPSTD) = 0.D0
          DO JPSTD=1,NBOP
            ASTDC(JPSTD,IPSTD) = 0.D0
          ENDDO
        ENDDO
        READ(12) NFITC(NOFI),JMAXC(NOFI),SPOBSC(NOFI),SPE2C(NOFI)
        IF( NFITC(NOFI) .EQ. 0 ) GOTO 9995
        READ(12) (YSTDC(IPSTD),IPSTD=1,NBOPC)
        DO JPSTD=1,NBOPC
          READ(12) (ASTDC(IPSTD,JPSTD),IPSTD=JPSTD,NBOPC)
          DO IPSTD=JPSTD,NBOPC
            ASTDC(JPSTD,IPSTD) = ASTDC(IPSTD,JPSTD)                                                ! REMPLIR LA 2EME MOITIE
          ENDDO
        ENDDO
        CLOSE(12)
!
!    TRANSFORMER LES EQUATIONS NORMALES PARTIELLES
!
!
!  ACBN  = TRANSP(CTRLM1)*ASTD*CTRLM1
!          ^^^^^^^^^^^^^^^^^^^
!  ACBN  =         ACBNI      *CTRLM1
!
!  ACBNI(IPCBN,IPSTD) =   S    CTRLM1(JPSTD,IPCBN)*ASTD(JPSTD,IPSTD)
!                       JPSTD
!
!  ACBN(IPCBN,JPCBN)  =   S    ACBNI(IPCBN,IPSTD)*CTRLM1(IPSTD,JPCBN)
!                       IPSTD
!
        DO IPCBN=1,NBOP                                                                            ! ACBNI=ACBNC=0.
          DO JPCBN=1,NBOP
            ACBNI(IPCBN,JPCBN) = 0.D0
            ACBNC(IPCBN,JPCBN) = 0.D0
          ENDDO
        ENDDO
        DO IPSTD=1,NBOP
          DO I=1,NBECM1
            JJ              = JCM1(I)
            ACBNI(JJ,IPSTD) = ACBNI(JJ,IPSTD)+VCM1(I)*ASTDC(ICM1(I),IPSTD)
          ENDDO
        ENDDO
        DO IPCBN=1,NBOP
          DO I=1,NBECM1
            JJ              = JCM1(I)
            ACBNC(IPCBN,JJ) = ACBNC(IPCBN,JJ)+ACBNI(IPCBN,ICM1(I))*VCM1(I)
          ENDDO
        ENDDO
        DO IPCBN=1,NBOP                                                                            ! STOCKER LES ELEMENTS DIAGONAUX
          POIDS(IPCBN,NOFI) = ACBNC(IPCBN,IPCBN)
        ENDDO
!
!  YCBN = TRANSP(CTRL)*YSTD
!
!  YCBN(IPCBN) =   S   CTRLM1(JPSTD,IPCBN)*YSTD(JPSTD)
!                JPSTD
!
        DO IPCBN=1,NBOP
          YCBNC(IPCBN) = 0.D0
        ENDDO
        DO I=1,NBECM1
          JJ        = JCM1(I)
          YCBNC(JJ) = YCBNC(JJ)+VCM1(I)*YSTDC(ICM1(I))
        ENDDO
!
!    AJOUTER LES CONTRIBUTIONS PARTIELLES AUX EQUATIONS NORMALES GLOBALES
!
        DO IPCBN=1,NBOP                                                                            ! AJOUTER LES CONTRIBUTIONS A YCBNS ET ACBNS
          YCBNS(IPCBN) = YCBNS(IPCBN)+YCBNC(IPCBN)
          DO JPCBN=1,NBOP
            ACBNS(IPCBN,JPCBN) = ACBNS(IPCBN,JPCBN)+ACBNC(IPCBN,JPCBN)
          ENDDO
        ENDDO
      GOTO 5                                                                                       ! FICHIER SUIVANT
!
404   NBFI = NOFI
      CLOSE(10)
      IF( NBOPM .NE. NBOP ) THEN
        PRINT 8123, NBOP,NBOPM
        GOTO  9999
      ENDIF
!
!  VALEURS INITIALES DES PARAS TRANSFORMES
!  PCBN SUPPOSEE = 0.
!
      DO I=1,NBEC
        II       = IC(I)
        PCBN(II) = PCBN(II)+VC(I)*PSTD(JC(I))
      ENDDO
!
!  PRISE EN COMPTE DES CONTRAINTES SUR LES PARAS TRANSFORMES
!
      NPARAV = NBOP                                                                                ! NB DE PARAS VARIABLES
      DO IPCBN=1,NBOP
        SELECT CASE( ICNTRL(IPCBN) )
          CASE( 0,1 )
            DO JPCBN=1,NBOP
              ACBNS(JPCBN,IPCBN) = 0.D0
              ACBNS(IPCBN,JPCBN) = 0.D0
            ENDDO
            ACBNS(IPCBN,IPCBN) = 1.D0
            YCBNS(IPCBN)       = 0.D0
            NPARAV             = NPARAV-1
          CASE( 2 )
            POIDSP             = 1.D0/(PRAT(IPCBN)*PRAT(IPCBN))
            SPOBSC(0)          = SPOBSC(0)+POIDSP
            VP                 = VALAT(IPCBN)-PCBN(IPCBN)
            SPE2C(0)           = SPE2C(0)+POIDSP*VP*VP
            YCBNS(IPCBN)       = YCBNS(IPCBN)+POIDSP*VP
            ACBNS(IPCBN,IPCBN) = ACBNS(IPCBN,IPCBN)+POIDSP
            POIDS(IPCBN,0)     = POIDSP
          CASE( 3 )
            ACBNS(IPCBN,IPCBN) = ACBNS(IPCBN,IPCBN)*(1.D0+PRAT(IPCBN))
            POIDS(IPCBN,0)     = ACBNS(IPCBN,IPCBN)*PRAT(IPCBN)
            SPOBSC(0)          = SPOBSC(0)+POIDS(IPCBN,0)
        END SELECT                                                                                 ! 4 = rien
      ENDDO
!
!  VARIATIONS DES PARAS TRANSFORMES
!
      CALL GAUJD(NBOP,ACBNS,YCBNS,VCBNS,DPCBN)
      DO IPCBN=1,NBOP
        IF( ICNTRL(IPCBN) .LT. 2 ) VCBNS(IPCBN,IPCBN) = 0.D0
      ENDDO
!
!  NOUVELLES VALEURS DES PARAS TRANSFORMES
!
      DO IPCBN=1,NBOP
        PCBNAJ(IPCBN) = PCBN(IPCBN)+DPCBN(IPCBN)
      ENDDO
!
!  NOUVELLES VALEURS DES PARAS STANDARD
!  PSTDAJ SUPPOSEE = 0.
!
      DO I=1,NBECM1
        II         = ICM1(I)
        PSTDAJ(II) = PSTDAJ(II)+VCM1(I)*PCBNAJ(JCM1(I))
      ENDDO
!
!  CARACTERISTIQUES GLOBALES DE L'AJUSTEMENT
!
      NFIT  = 0
      JMAX  = 0
      SPOBS = SPOBSC(0)
      SPE2  = SPE2C(0)
      DO NOFI=1,NBFI
        NFIT = NFIT+NFITC(NOFI)
        IF( JMAXC(NOFI) .GT. JMAX ) JMAX = JMAXC(NOFI)
        SPOBS = SPOBS+SPOBSC(NOFI)
        SPE2  = SPE2+SPE2C(NOFI)
      ENDDO
!
!  VARIANCE DE L'AJUSTEMENT
!
      SIGMA2 = SPE2/DBLE(NFIT-NPARAV)
!
! VARIANCE PREDITE APRES AJUSTEMENT
!
      ES = 0.D0
      DO JPCBN=1,NBOP
        ES = ES+YCBNS(JPCBN)*DPCBN(JPCBN)
      ENDDO
      SIGMAN = SIGMA2-ES/DBLE(NFIT-NPARAV)
!
!  VARIANCE DES PARAS TRANSFORMES
!
      DO IPCBN=1,NBOP
        IF( ICNTRL(IPCBN) .LT. 2 ) THEN
          LIBCBN(IPCBN) = 0
        ELSE
          ETPCBN(IPCBN) = SQRT(VCBNS(IPCBN,IPCBN)*SIGMA2)
          XLIB          = 1.D0/SQRT(ACBNS(IPCBN,IPCBN)*VCBNS(IPCBN,IPCBN))
          LIBCBN(IPCBN) = NINT(XLIB*1000.D0)
        ENDIF
      ENDDO
!
!  VARIANCE DES PARAS STANDARD
!  ASTDI,VSTDI,ASTDS,VSTDS SUPPOSEES = 0.
!
!  ASTD = TRANSP(CTRL)*ACBN*CTRL
!         ^^^^^^^^^^^^^^^^^
!  ASTD =      ASTDI       *CTRL
!
!  ASTDI(IPSTD,IPCBN) =   S   CTRL(JPCBN,IPSTD)*ACBN(JPCBN,IPCBN)
!                       JPCBN
!
!  ASTD(IPSTD,JPSTD)  =   S   ASTDI(IPSTD,IPCBN)*CTRL(IPCBN,JPSTD)
!                       IPCBN
!
!  IDEM POUR VSTD (VSTDI,VCBN)
!
      DO IPCBN=1,NBOP
        DO I=1,NBEC
          II              = IC(I)
          JJ              = JC(I)
          ASTDI(JJ,IPCBN) = ASTDI(JJ,IPCBN)+VC(I)*ACBNS(II,IPCBN)
          VSTDI(JJ,IPCBN) = VSTDI(JJ,IPCBN)+VC(I)*VCBNS(II,IPCBN)
        ENDDO
      ENDDO
      DO IPSTD=1,NBOP
        DO I=1,NBEC
          II              = IC(I)
          JJ              = JC(I)
          ASTDS(IPSTD,JJ) = ASTDS(IPSTD,JJ)+ASTDI(IPSTD,II)*VC(I)
          VSTDS(IPSTD,JJ) = VSTDS(IPSTD,JJ)+VSTDI(IPSTD,II)*VC(I)
        ENDDO
      ENDDO
      DO IPSTD=1,NBOP
        IF( VSTDS(IPSTD,IPSTD) .EQ. 0.D0 ) THEN
          LIBSTD(IPSTD) = 0
        ELSE
          ETPSTD(IPSTD) = SQRT(VSTDS(IPSTD,IPSTD)*SIGMA2)
          XLIB          = 1.D0/SQRT(ASTDS(IPSTD,IPSTD)*VSTDS(IPSTD,IPSTD))
          LIBSTD(IPSTD) = NINT(XLIB*1000.D0)
        ENDIF
      ENDDO
!
!  FICHIERS DE SORTIE
!
!
!  ECRITURE DE LA MATRICE DE COVARIANCE DES PARAMETRES
!
      PRINT 2001,  'para_variance.t'
      OPEN(12,FILE='para_variance.t',FORM='UNFORMATTED',STATUS='UNKNOWN')
      WRITE(TITRE,1002) NFIT,JMAX,SQRT(SIGMA2),SQRT(SIGMAN)                                        ! AJUSTES
      WRITE(12) TITRE
      DO J=1,3
        WRITE(12) TITRAJ(J,NOAJM)
      ENDDO
      WRITE(12) NBOP,TITRAJ(4,NOAJM)
      WRITE(12) TITRAJ(5,NOAJM)
      TITRE = FDATE
      WRITE(12) TITRE
      DO IPSTD=1,NBOP
        WRITE(COPAJS(IPSTD)(NCL2-3:NCL2),'(I4)') LIBSTD(IPSTD)
        WRITE(12) COPAJS(IPSTD),PSTDAJ(IPSTD),ETPSTD(IPSTD)
      ENDDO
      DO IPSTD=1,NBOP
        WRITE(12) (SIGMA2*VSTDS(K,IPSTD),K=IPSTD,NBOP)
      ENDDO
      CLOSE(12)
!
!  SORTIE DANS LES FICHIERS DE PARAMETRES AJUSTES
!
E22:  DO I=1,NBFI                                                                                  ! POUR CHAQUE FICHIER D ENTREE
        NEQFIA = TRIM(NEQFI(I))//'_adj'
        FPARI  = TRIM(FPARA(I))
        PRINT 2001,  TRIM(NEQFIA)
        OPEN(12,FILE=TRIM(NEQFIA),STATUS='UNKNOWN')
        OPEN(13,FILE=FPARI,STATUS='OLD')
        IF( TRANS ) GOTO 5002
!
5003    DO II=1,4
          READ(13,1000) TITRE
        ENDDO
        READ(13,1003) NBOPHI,TITRE
        DO II=1,2
          READ(13,1000) TITRE
        ENDDO
        WRITE(12,1002) NFIT,JMAX,SQRT(SIGMA2),SQRT(SIGMAN)                                         ! AJUSTES
        DO J=2,4
          WRITE(12,1000) TRIM(TITRAJ(J,I))
        ENDDO
        WRITE(12,1003) NBOPHI,TRIM(TITRAJ(5,I))
        WRITE(12,1000) TRIM(TITRAJ(6,I))
        WRITE(12,1004) FDATE(:31)
E3:     DO IPORG=1,NBOPHI
          READ(13,1000) TITRE
          READ(TITRE(1:4),1110) NUMPI
          DO IPSTD=1,NBOPAJ(I)
            READ(COPAJS(IPSTD)(1:4),1110) NUMPN
            IF( NUMPN .EQ. NUMPI ) THEN
              WRITE(12,1001) COPAJS(IPSTD),PSTDAJ(IPSTD),ETPSTD(IPSTD)
              CYCLE E3
            ENDIF
          ENDDO
          WRITE(12,1000) TRIM(TITRE)
        ENDDO E3
        IF( TRANS ) GOTO 5004
!
5002    DO J=1,4                                                                                   ! FIXES
          WRITE(12,1000) TRIM(TITRFX(J,I))
        ENDDO
        WRITE(12,1003) NBOPFX(I),TRIM(TITRFX(5,I))
        DO J=6,7
          WRITE(12,1000) TRIM(TITRFX(J,I))
        ENDDO
        DO J=1,NBOPFX(I)
          WRITE(12,1001) CHAIFX(J,I),PARAFX(J,I),PREDFX(J,I)
        ENDDO
        IF( TRANS ) THEN
          DO II=1,4
            READ(13,1000) TITRE
          ENDDO
          READ(13,1003) NBOPHI,TITRE
          DO II=1,2
            READ(13,1000) TITRE
          ENDDO
          DO II=1,NBOPHI
            READ(13,1000) TITRE
          ENDDO
          GOTO 5003
        ENDIF
!
5004    CLOSE(13)
        CLOSE(12)
      ENDDO E22
!
!  SORTIE DANS LE FICHIER LISTE
!
      PRINT 2001,  'para_estimates.t'
      OPEN(12,FILE='para_estimates.t',STATUS='UNKNOWN')
      WRITE(12,1100)
      DO NOFI=1,NBFI
        WRITE(12,1005) NOFI,NFITC(NOFI),JMAXC(NOFI),SQRT(SPE2C(NOFI)/NFITC(NOFI)),TRIM(NEQFI(NOFI))
        PRINT    1012, NOFI,NFITC(NOFI),JMAXC(NOFI),SQRT(SPE2C(NOFI)/NFITC(NOFI)),TRIM(NEQFI(NOFI))
      ENDDO
      WRITE(12,1101)
      WRITE(12,1102)
      WRITE(12,1103)
! CTRP : codes de controle = 0
      READ(15,1000)
      NBC = 0
!
421   READ(15,1000)  COPAJC
      IF( COPAJC .EQ. '' ) GOTO 422
      NBC = NBC+1
      WRITE(12,1015) COPAJC(:NCL1)
      GOTO 421
!
! CTRP : codes de controle = 1
422   IF( NBC .NE. 0 ) WRITE(12,1000)
      NBC = 0
!
423   READ(15,1000,END=424) COPAJC
      NBC = NBC+1
      WRITE(12,1016)        COPAJC(:NCL1)
      GOTO 423
!
424   IF( NBC .NE. 0 ) WRITE(12,1000)
      CLOSE(15)
      NBCNST = 0
      DO IPCBN=1,NBOP
        IF( ICNTRL(IPCBN) .EQ. 0 .OR.         &                                                    ! en fait, 0 et 1 absents de CTRP
            ICNTRL(IPCBN) .EQ. 1      ) THEN
          WRITE(12,1009) COPAJ(IPCBN)(:NCL1),PCBNAJ(IPCBN)
        ELSE
          SOMP(IPCBN) = POIDS(IPCBN,0)
          DO NOFI=1,NBFI
            SOMP(IPCBN) = SOMP(IPCBN)+POIDS(IPCBN,NOFI)
          ENDDO
          IF    ( ICNTRL(IPCBN) .EQ. 2 ) THEN
            WRITE(12,1006) COPAJ(IPCBN)(:NCL1),100.D0*POIDS(IPCBN,0)/SOMP(IPCBN),VALAT(IPCBN),PRAT(IPCBN)
            NBCNST = NBCNST+1
          ELSEIF( ICNTRL(IPCBN) .EQ. 3 ) THEN
            WRITE(12,1007) COPAJ(IPCBN)(:NCL1),100.D0*PRAT(IPCBN)
            NBCNST = NBCNST+1
          ENDIF
        ENDIF
      ENDDO
      IF( NBCNST .EQ. 0 ) WRITE(12,1014)
      WRITE(12,1104) NFIT,SQRT(SIGMA2)
      WRITE(12,1013) (NOFI,NOFI=1,NBFI)
      DO IPCBN=1,NBOP
        IF    ( ICNTRL(IPCBN) .EQ. 1 ) THEN
          WRITE(CHAINE,1105) PCBNAJ(IPCBN)
          READ(CHAINE(7:9),1106) IEXP
          WRITE(12,1010) COPAJ(IPCBN)(:NCL1),PCBNAJ(IPCBN)/10.D0**(IEXP-1),IEXP-1
        ELSEIF( ICNTRL(IPCBN) .NE. 0 ) THEN
          CALL AROND(PCBNAJ(IPCBN),ETPCBN(IPCBN),DPCBN(IPCBN),CARON)
          WRITE(12,1008) COPAJ(IPCBN)(:NCL1),CARON,LIBCBN(IPCBN),            &
                         (100.D0*POIDS(IPCBN,NOFI)/SOMP(IPCBN),NOFI=1,NBFI)
        ENDIF
      ENDDO
      WRITE(12,1107) SQRT(SIGMAN)
      WRITE(12,1108)
      DO IPCBN=1,NBOP
        NCO = 0
E20:    DO JPCBN=1,NBOP
          IF( IPCBN         .EQ. JPCBN      ) CYCLE E20
          IF( ICNTRL(IPCBN) .LE. 2     .OR.              &
              ICNTRL(JPCBN) .LE. 2          ) CYCLE E20
          ICO = NINT(100.D0*VCBNS(IPCBN,JPCBN)/SQRT(VCBNS(IPCBN,IPCBN)*VCBNS(JPCBN,JPCBN)))
          IF( ABS(ICO) .LT. 85 ) CYCLE E20
          NCO = NCO+1
          READ(COPAJ(JPCBN)(1:4),1110) NUCOR(NCO)
          ICOR(NCO) = ICO
          IF( NCO .EQ. 9 ) GOTO 29
        ENDDO E20
!
29      IF( NCO .NE. 0 ) WRITE(12,1011) COPAJ(IPCBN)(:NCL1),(NUCOR(K),ICOR(K),K=1,NCO)
      ENDDO
      CLOSE(12)
      GOTO 9000
!
9995  PRINT 8003, NEQFI(NOFI)
      GOTO  9999
9996  PRINT 8124, NOM
      GOTO  9999
9997  PRINT 8002
      GOTO  9999
9998  PRINT 8001
9999  PRINT 8000
!
9000  PRINT *
      END PROGRAM PARADJ
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
! RESOLUTION D'UN SYSTEME LINEAIRE (Y=A*X) DE RANG N
! PAR L'ALGORITHME DE GAUSS JORDAN AVEC CALCUL DE L'INVERSE DE A
!
      SUBROUTINE GAUJD(N,A,Y,AM1,X)
      use mod_dppr
      use mod_par_tds
      use mod_com_paradj
      IMPLICIT NONE
      real(kind=dppr) ,dimension(MXOPH,MXOPH)      :: A,AM1
      real(kind=dppr) ,dimension(MXOPH)            :: Y,X
      integer          :: N

      real(kind=dppr)  :: FACTC
      real(kind=dppr)  :: H
      real(kind=dppr)  :: PIVOT

      integer          :: J
      integer          :: K
      integer          :: L,LI,LP
      integer          :: NBCOL
!
8000  FORMAT(' !!! PARADJ : STOP ON ERROR')
8100  FORMAT(' !!! DIMENSION = ',I4,' EXCEEDING MXOPH = ',I8)
8101  FORMAT(' !!! THE MATRIX IS SINGULAR TO WORKING PRECISION')
!
      IF( N .GT. MXOPH ) THEN
        PRINT 8100, N,MXOPH
        PRINT 8000
        PRINT *
        STOP
!
      ENDIF
!
! INITIALISATION DE AIY
!
      NBCOL = 2*N+1
      DO L=1,N
        DO K=1,N
          AIY(L,K)   = A(L,K)
          AIY(L,K+N) = 0.D0
        ENDDO
        AIY(L,NBCOL) = Y(L)
        AIY(L,L+N)   = 1.D0
      ENDDO
!
! BOUCLE SUR LES PIVOTS
!
      DO LP=1,N
        LI = LP
!
110     PIVOT = AIY(LI,LP)
        IF( PIVOT .NE. 0.D0 ) GOTO 200
        LI = LI+1
        IF( LI .LE. N ) GOTO 110
        PRINT 8101
        PRINT 8000
        PRINT *
        STOP
!
! PERMUTATION DES LIGNES LI ET LP
!
200     DO J=LP,NBCOL
          H         = AIY(LP,J)
          AIY(LP,J) = AIY(LI,J)
          AIY(LI,J) = H
        ENDDO
!
! DIVISION DE LA LIGNE LP PAR LE PIVOT
!
        DO K=LP,NBCOL
          AIY(LP,K) = AIY(LP,K)/PIVOT
        ENDDO
!
! ANNULATION DES ELEMENTS DE LA COLONNE LP
!
E5:     DO L=1,N
          IF( L .EQ. LP ) CYCLE E5
          FACTC = AIY(L,LP)
          DO K=LP,NBCOL
            AIY(L,K) = AIY(L,K)-FACTC*AIY(LP,K)
          ENDDO
        ENDDO E5
      ENDDO
!
! SOLUTION
!
      DO L=1,N
        X(L) = AIY(L,NBCOL)
        DO K=1,N
          AM1(L,K) = AIY(L,K+N)
        ENDDO
      ENDDO
!
      RETURN
      END SUBROUTINE GAUJD
