      PROGRAM ASXTRM
!
! 2008 AUGUST
! V.BOUDON, CH.WENGER
!
!  lire un fichier binaire (ex. INFILE) de type XTRM_
!  et l'ecrire en ASCII dans INFILE'_ASC'
!
! APPEL : asxtrm
!
      use mod_dppr
      IMPLICIT NONE

      real(kind=dppr)  :: TM

      integer          :: ICK,ICL,IK,IKC,IL,ILC
      integer          :: JK,JL
      integer          :: NBOTR,NFBK,NFBL

      character(len = 150)  :: INFILE
      character(len = 160)  :: OUTFILE
!
1000  FORMAT(A)
1020  FORMAT(/,            &
             'ASXTRM : ')
2000  FORMAT(' <  ',A)
2001  FORMAT(' >> ',A)
7000  FORMAT('ENTER XTRM_ TYPE FILE NAME :')
8000  FORMAT(' !!! ASXTRM : STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF FOR ',A)
!
      PRINT 1020
!
!  FICHIER D'ENTREE
!
      PRINT 7000
      READ(*,1000) INFILE
      PRINT 2000,  TRIM(INFILE)
      OPEN(81,FILE=TRIM(INFILE),FORM='UNFORMATTED',STATUS='OLD')
!
!  FICHIER DE SORTIE
!
      OUTFILE = TRIM(INFILE)//'_ASC'
      PRINT 2001,  TRIM(OUTFILE)
      OPEN(10,FILE=TRIM(OUTFILE))
      WRITE(10,*) '>>> NBOTR'
      READ (81,END=9003) NBOTR
      WRITE(10,*)        NBOTR
      WRITE(10,*) '>>> JK,ICK,NFBK,JL,ICL,NFBL'
      WRITE(10,*) '>>> IK,IL,TM'
!
3     READ (81,END=9000) JK,ICK,NFBK,JL,ICL,NFBL
      WRITE(10,*)        JK,ICK,NFBK,JL,ICL,NFBL
      DO IKC=1,NFBK
        DO ILC=1,NFBL
          READ (81,END=9003) IK,IL,TM
          WRITE(10,*)        IK,IL,TM
        ENDDO
      ENDDO
      GOTO 3
!
9003  PRINT 8003, TRIM(INFILE)
      PRINT 8000
!
9000  CLOSE(10)
      PRINT *
      END PROGRAM ASXTRM
