      PROGRAM POLMOD
!
!  14.12.88 FORTRAN 77 DU SUN4 REV 13 JAN 1989
!  REV 25 JAN 1990
!  AOU. 93 T.GABARD
! REV    JAN 1995 JPC,CW (PARAMETER)
! MODIF 01/99 V. BOUDON ---> SCHEMA DE POLYADE QUELCONQUE.
!
!   CODAGE DES OPERATEURS ROVIBRATIONNELS DU TENSEUR DE POLARISABILITE
!   ENTRE DES RESTRICTIONS DONNEES DE POLYADES VIBRATIONNELLES TYPE CH4
!
!     LECTURE DES G  MXDROT
!
!     CALCUL DES K  JMKCUB = FIXE DANS LE PROGRAMME
!
! APPEL : comme dipmod
!
!  *****  LIMITATIONS DU PROGRAMME GEREES PAR PARAMETER.
!
!     MXOPT                                                             !ICODVI:ICODRO:ICODVC:ICODVA:ICODSY:ICODAN
! NB MAXIMUM D'OPERATEURS ROVIBRATIONNELS
!     MXOCV                                                             !IDANIS:IDVI:IDRO:IDVC:IDVA:IDSY:IRDRE
! NB MAXIMUM D'OPERATEURS ROVIBRATIONNELS PAR CASE VIBRATIONNELLE
!     MXEMR                                                             !EMOP:LI:IC
! NB MAXIMUM D'E.M.R.V. NON NULS
!     MXSNV                                                             !IFFI:IFFS
! NB MAXIMUM TOTAL DE SOUS-NIVEAUX VIBRATIONNELS SUP ET INF
!     MXSNB                                                             !IKVC:IKVA
! NB MAXIMUM DE SOUS-NIVEAUX VIBRATIONNELS POUR UN ENSEMBLE DONNE DES VI
!     MXOPR                                                             !IROCO:ICODRA
! NB MAXIMUM D'OPERATEURS ROVIBRATIONNELS PAR OPERATEUR VIBRATIONNEL
!
! NB MAXIMUM D'OPERATEURS ROVIBRATIONNELS PAR OPERATEUR VIBRATIONNEL
!     MXPOL                                                             ! NVIBI:NVIBS:NVIII:NIVIS
! NB MAXIMUM DE POLYADES
!
      use mod_dppr
      use mod_par_tds
      use mod_com_fdate
      use mod_com_lg
      use mod_com_phase
      use mod_com_sy
      use mod_main_polmod
      IMPLICIT NONE

! functions
      real(kind=dppr)  :: ELMR
      integer          :: CTR,NSYM1

      real(kind=dppr)  :: EM,EM1,EM2,EN
      real(kind=dppr)  :: SP

      integer ,dimension(NBVQN+1)  :: IBELMR,ICELMR,IAELMR,IKELMR
      integer ,dimension(NBVQN)    :: ICA,ICC,ICF,IL,ILA,ILC,INA,INC
      integer ,dimension(NBVQN)    :: INUM,IV,IVA,IVC
      integer ,dimension(MDMIGA)   :: IGAMA
      integer ,dimension(MXSYM)    :: IROT
      integer ,dimension(MXPOL)    :: NIVII,NIVIS
      integer ,dimension(0:MXPOL)  :: IORTR
      integer          :: NFS_SUP,NFFS_SUP,IOP_SUP,NROT_SUP,NBOPT_SUP
      integer          :: IROTA_SUP,NIV_SUP
      integer          :: I,IB,IC1,IC2,IC23,IC234,ICA1,ICA2,ICA23
      integer          :: ICA34,ICANIS,ICC1,ICC2,ICC23,ICC34,ICROT
      integer          :: ID,IDEGV,IDK,IDROT,IF,IF1,IF2,IG,IGA,IJK,IK
      integer          :: IM,IMIA,IOA,IOA1,IOC,IOC1,IOP,IOPA,IOPC
      integer          :: IRAM,IRO,IRO1,IRO2,IRO3,IRO4,IRO5,IROTA,IU
      integer          :: IVI,IVS
      integer          :: J,JANIS,JD,JJJ,JMINF,JMKCUB,JMSUP
      integer          :: KNVIB,KNIVP,KOP,KPOL,KPOLI,KPOLS,KVI
      integer          :: L,LOP
      integer          :: NBOPT,NBVA,NBVC,NFFI,NFFS,NFI,NFS,NIC,NGAMA
      integer          :: NPOLI,NPOLS,NROT,NS

      character(len =   1) ,dimension(0:MXPOL)  :: AORTR
      character(len =   1)  :: AVI,AVS
      character(len =  10)  :: CHAINE
      character(len = 120)  :: IFICH
!
1000  FORMAT('    EFFECTIVE POLARIZABILITY OPERATOR :'   , /,   &
             ' VIBRATIONAL REDUCED MATRIX ELEMENTS (RME)',//,   &
             A                                               )
1001  FORMAT(//)
1002  FORMAT(I6,'  |[[',                      &
             I2,'(',I1,',',I1,A,')*',         &
             I2,'(',I1,',',I1,A,')*',         &
             I2,'(',I1,',',I1,A,')]',A,'*',   &
             I2,'(',I1,',',I1,A,')]',A,       &
             ' >'                          )
1004  FORMAT(I4,' TH VIBRATIONAL OPERATOR',//,   &
             A,1X,' {[[[',                       &
             I1,'(',I1,',',A,')*',               &
             I1,'(',I1,',',A,')]*',              &
             I1,'(',I1,',',A,')]',A,'*',         &
             I1,'(',I1,',',A,')]',A,             &
             '} '                         , /,   &
             2X,' {[[[',                         &
             I1,'(',I1,',',A,')*',               &
             I1,'(',I1,',',A,')]*',              &
             I1,'(',I1,',',A,')]',A,'*',         &
             I1,'(',I1,',',A,')]',A,             &
             '} ',A)
1006  FORMAT(I1)
1007  FORMAT(I2)
1010  FORMAT(A)
1029  FORMAT(/,                     &
             I5,' NON ZERO RME',/)
1030  FORMAT(2X,A1,'<',I5,'|',3X,'|',I5,'>',3X,F19.13)
1100  FORMAT(/,                                                  &
             I4,' VIBRATIONAL SUBLEVEL(S) OF UPPER POLYAD :',/)
1133  FORMAT(/,                                                  &
             I4,' VIBRATIONAL SUBLEVEL(S) OF LOWER POLYAD :',/)
1199  FORMAT(/,              &
             'POLMOD : ',A)
1205  FORMAT(' UPPER LEVEL ',I2,' : (v1;v2;v3;v4) = (',3(I2,';'),I2,')')
1206  FORMAT(/,                                     &
             ' POLYAD ',I2,'  MINUS POLYAD ',I2,/)
1207  FORMAT(' LOWER LEVEL ',I2,' : (v1;v2;v3;v4) = (',3(I2,';'),I2,')')
1215  FORMAT(/,                                              &
             ' POLARIZABILITY DEVELOPMENT ORDER :  ',7(I2))
1218  FORMAT(/,                               &
             I4,' ROTATIONAL OPERATOR(S)',/)
1219  FORMAT(/,           &
             1X,46('-'))
1220  FORMAT(I4,' ROVIBRATIONAL OPERATORS',/)
1313  FORMAT(I4,I3,'(',I1,',',I1,A,')',1X,4I1,A,1X,4I1,A,1X,A,I8,I7,I3,I1)                         ! 4 = NBVQN
2001  FORMAT(' >> ',A)
8000  FORMAT(' !!! POLMOD : STOP ON ERROR')
8001  FORMAT(' !!! ERROR OPENING CONTROL FILE')
8002  FORMAT(' !!! INCOMPLETE CONTROL FILE')
8003  FORMAT(' !!! INCOMPATIBLE ARGUMENTS')
!
! ARRAY INITIAL ALLOCATION
!
      CALL ALLOC_ALL
!
      NFS_SUP   = -1
      NFFS_SUP  = -1
      IOP_SUP   = -1
      NROT_SUP  = -1
      NBOPT_SUP = -1
      IROTA_SUP = -1
      NIV_SUP   = -1
      OPEN(10,FILE=CTRLF,ERR=9998,STATUS='OLD')
      READ(10,1010,END=9997) FDATE
      PRINT 1199,            FDATE
!
! NUMERO DE LA POLYADE SUPERIEURE
!
      READ(10,1010,END=9997) CHAINE
      AVS = CHAINE(2:2)
      READ(AVS,1006) IVS
      NPOLS = IVS+1
!
! LECTURE DES NIVEAUX VIBRATIONNELS
!
      JMSUP = 0
      DO KPOL=1,NPOLS
        READ(10,1010,END=9997) CHAINE
        READ(CHAINE(2:3),1007) NIVIS(KPOL)
        IF( NIVIS(KPOL) .GT. NIV_SUP ) NIV_SUP = NIVIS(KPOL)
        DO WHILE( NIVIS(KPOL) .GT. MXNIV )
          CALL RESIZE_MXNIV
        ENDDO
        DO KNIVP=1,NIVIS(KPOL)
          DO KNVIB=1,NBVQN
            READ(10,1006,END=9997) NVIBS(KPOL,KNIVP,KNVIB)
            IF( KNVIB .GT. 2 ) JMSUP = MAX(JMSUP,NVIBS(KPOL,KNIVP,KNVIB))
          ENDDO
        ENDDO
      ENDDO
!
! NUMERO DE LA POLYADE INFERIEURE
!
      READ(10,1010,END=9997) CHAINE
      AVI = CHAINE(2:2)
      READ(AVI,1006) IVI
      NPOLI = IVI+1
!
! LECTURE DES NIVEAUX VIBRATIONNELS
!
      JMINF = 0
      DO KPOL=1,NPOLI
        READ(10,1010,END=9997) CHAINE
        READ(CHAINE(2:3),1007) NIVII(KPOL)
        IF( NIVII(KPOL) .GT. NIV_SUP ) NIV_SUP = NIVII(KPOL)
        DO WHILE( NIVII(KPOL) .GT. MXNIV )
          CALL RESIZE_MXNIV
        ENDDO
        DO KNIVP=1,NIVII(KPOL)
          DO KNVIB=1,NBVQN
            READ(10,1006,END=9997) NVIBI(KPOL,KNIVP,KNVIB)
            IF( KNVIB .GT. 2 ) JMINF = MAX(JMINF,NVIBI(KPOL,KNIVP,KNVIB))
          ENDDO
        ENDDO
      ENDDO
      JMKCUB = MAX(JMINF,JMSUP)
!
! ORDRE DU DEVELOPPEMENT
!
      READ(10,1010,END=9997) CHAINE
      IF( LEN_TRIM(CHAINE) .NE. IVI+2 ) GOTO 9996
      DO KVI=0,IVI
        I          = KVI+2
        AORTR(KVI) = CHAINE(I:I)
        READ(AORTR(KVI),1006) IORTR(KVI)
      ENDDO
!
!  NOM DU FICHIER DE SORTIE
!
      IFICH = 'MP_P'//AVS//'mP'//AVI//'_D'
      DO KVI=0,IVI
        IFICH = TRIM(IFICH)//AORTR(KVI)
      ENDDO
      CLOSE(10)
      PRINT 2001, TRIM(IFICH)
!
! INITIALISATIONS
!
      CALL LECTG(MXDROT)
      CALL FACTO
      CALL CAL6C
      CALL CALKM(JMKCUB)
      OPEN(20,FILE=IFICH,STATUS='UNKNOWN')
      WRITE(20,1000) FDATE
      WRITE(20,1206) IVS,IVI
      DO I=1,NIVIS(NPOLS)
        WRITE(20,1205) I,(NVIBS(NPOLS,I,J),J=1,NBVQN)
      ENDDO
      DO I=1,NIVII(NPOLI)
        WRITE(20,1207) I,(NVIBI(NPOLI,I,J),J=1,NBVQN)
      ENDDO
!
!     CODAGE DES SOUS-NIVEAUX VIBRATIONNELS
!
!     SOUS-NIVEAUX SUPERIEURS.
!
      NFFS = 0
E65:  DO IF1=1,NIVIS(NPOLS)
        DO IF2=1,NBVQN
          IV(IF2) = NVIBS(NPOLS,IF1,IF2)
        ENDDO
        CALL SIGVI(IV,'IFFS',NFFS,NFS)
        NFFS = NFFS+NFS
        IF( NFS  .GT. NFS_SUP  ) NFS_SUP  = NFS
        IF( NFFS .GT. NFFS_SUP ) NFFS_SUP = NFFS
      ENDDO E65
      WRITE(20,1100) NFFS
      DO IF=1,NFFS
        DO I=1,NBVQN
          CALL VLNC(IFFS(I,IF),IV(I),IL(I),INUM(I),ICF(I))
        ENDDO
        CALL VLNC(IFFS(NBVQN+1,IF),IC1,IC2,IC23,IC234)
        WRITE(20,1002) IF,                                                  &
                       (IV(I),IL(I),INUM(I),SYM(ICF(I)),I=1,3),SYM(IC23) ,  &
                        IV(4),IL(4),INUM(4),SYM(ICF(4))       ,SYM(IC234)
      ENDDO
!
!     SOUS-NIVEAUX INFERIEURS.
!
      NFFI = 0
      DO IF1=1,NIVII(NPOLI)
        DO IF2=1,NBVQN
          IV(IF2) = NVIBI(NPOLI,IF1,IF2)
        ENDDO
        CALL SIGVI(IV,'IFFI',NFFI,NFI)
        NFFI = NFFI+NFI
      ENDDO
      WRITE(20,1133) NFFI
      DO IF=1,NFFI
        DO I=1,NBVQN
          CALL VLNC(IFFI(I,IF),IV(I),IL(I),INUM(I),ICF(I))
        ENDDO
        CALL VLNC(IFFI(NBVQN+1,IF),IC1,IC2,IC23,IC234)
        WRITE(20,1002) IF,                                                  &
                       (IV(I),IL(I),INUM(I),SYM(ICF(I)),I=1,3),SYM(IC23) ,  &
                        IV(4),IL(4),INUM(4),SYM(ICF(4))       ,SYM(IC234)
      ENDDO
      WRITE(20,1215) (IORTR(I),I=0,IVI)
      LOP   = 0
      NBOPT = 0
!
!     BOUCLE SUR LES POLYADES INFERIEURES
!
E36:  DO KPOLI=1,NPOLI
!
!     DETERMINATION DES CREATEURS
!
E35:    DO IOC=1,NIVII(KPOLI)
          DO IOC1=1,NBVQN
            IVC(IOC1) = NVIBI(KPOLI,IOC,IOC1)
          ENDDO
          CALL SIGVI(IVC,'IKVC',0,NBVC)
!
!     POLYADES SUPERIEURES
!
          KPOLS = KPOLI+IVS-IVI
!
!     DETERMINATION DES ANNIHILATEURS
!
E33:      DO IOA=1,NIVIS(KPOLS)
            DO IOA1=1,NBVQN
              IVA(IOA1) = NVIBS(KPOLS,IOA,IOA1)
            ENDDO
            IF( IVI .EQ. IVS .AND.              &
                IOA .LT. IOC       ) CYCLE E33
            CALL SIGVI(IVA,'IKVA',0,NBVA)
            IDEGV = 0
            DO IOP=1,NBVQN
              IDEGV = IVA(IOP)+IVC(IOP)+IDEGV
            ENDDO
            IDROT = IORTR(KPOLI-1)+1-IDEGV
!
!     COUPLAGE CREATEURS-ANNIHILATEURS
!
E1:         DO IOPC=1,NBVC
              IMIA = 1
              IF( IOA .EQ. IOC ) IMIA = IOPC
              DO I=1,NBVQN
                CALL VLNC(IKVC(I,IOPC),IVC(I),ILC(I),INC(I),ICC(I))
              ENDDO
              CALL VLNC(IKVC(NBVQN+1,IOPC),ICC1,ICC2,ICC23,ICC34)
E2:           DO IOPA=IMIA,NBVA
                DO I=1,NBVQN
                  CALL VLNC(IKVA(I,IOPA),IVA(I),ILA(I),INA(I),ICA(I))
                ENDDO
                CALL VLNC(IKVA(NBVQN+1,IOPA),ICA1,ICA2,ICA23,ICA34)
                CALL MULTD(ICC34,ICA34,NGAMA,IGAMA)
                NROT = 0
E111:           DO I=1,NGAMA
E3:               DO IRAM=1,5,2
E733:               DO IU=1,2
                      DO IJK=1,MXSYM
                        IROT(IJK) = 0
                      ENDDO
!
!     DETERMINATION DES OPERATEURS ROTATIONNELS POSSIBLES
!
                      IROTA = 0
                      IF( IDROT .LT. IU-1 ) CYCLE E733
                      DO ID=IU-1,IDROT,2
                        CALL NBJC(ID,1,IRO1,IRO2,IRO3,IRO4,IRO5)
                        IROT(1) = IROT(1)+IRO1
                        IROT(2) = IROT(2)+IRO2
                        IROT(3) = IROT(3)+IRO3
                        IROT(4) = IROT(4)+IRO4
                        IROT(5) = IROT(5)+IRO5
                        DO IDK=ID,0,-2
E361:                     DO ICROT=1,MXSYM
                            NIC = NSYM1(IDK,1,ICROT)
                            IF( NIC                      .EQ. 0 ) CYCLE E361
                            IF( CTR(IRAM,ICROT,IGAMA(I)) .EQ. 0 ) CYCLE E361
                            DO NS=1,NIC
                              IROTA = IROTA+1
                              IF( IROTA .GT. IROTA_SUP ) IROTA_SUP = IROTA
                              IF( IROTA .GT. MXOPR     ) CALL RESIZE_MXOPR
                              IROCO(IROTA)  = 1000*ID+100*IDK+10*ICROT+NS-1
                              ICODRA(IROTA) = 20*(IRAM/3)+IRAM
                            ENDDO
                          ENDDO E361
                        ENDDO
                      ENDDO
!
!    CALCUL DU FACTEUR DE NORMALISATION
!
                      IF( IROTA .EQ. 0 ) CYCLE E733
                      SP        = (-1)**(IU+ICA34+ICC34+IGAMA(I))
                      IBELMR(:) = IKVC(:,IOPC)
                      ICELMR(:) = IKVC(:,IOPC)
                      IAELMR(:) = IKVA(:,IOPA)
                      IKELMR(:) = IKVA(:,IOPA)
                      EN        = ELMR(IBELMR,ICELMR,IAELMR,IKELMR,IGAMA(I))
                      IBELMR(:) = IKVC(:,IOPC)
                      ICELMR(:) = IKVA(:,IOPA)
                      IKELMR(:) = IKVC(:,IOPC)
                      IKELMR(:) = IKVA(:,IOPA)
                      EN        = ELMR(IBELMR,ICELMR,IAELMR,IKELMR,IGAMA(I))*SP+EN
                      IF( ABS(EN) .LT. 1.D-05 ) CYCLE E733
                      LOP = LOP+1
                      WRITE(20,1219)
                      WRITE(20,1004) LOP,AUG(IU),                                   &
                                     (IVC(J),ILC(J),SYM(ICC(J)),J=1,3),SYM(ICC23),  &
                                      IVC(4),ILC(4),SYM(ICC(4)),SYM(ICC34),         &
                                     (IVA(J),ILA(J),SYM(ICA(J)),J=1,3),SYM(ICA23),  &
                                      IVA(4),ILA(4),SYM(ICA(4)),SYM(ICA34),         &
                                      SYM(IGAMA(I))
!
!     CALCUL ET CODAGE DES E.M.R.V.
!
                      IOP = 0
                      DO IB=1,NFFI
E5:                     DO IK=1,NFFS
                          IBELMR(:) = IFFI(:,IB)
                          ICELMR(:) = IKVC(:,IOPC)
                          IAELMR(:) = IKVA(:,IOPA)
                          IKELMR(:) = IFFS(:,IK)
                          EM1       = ELMR(IBELMR,ICELMR,IAELMR,IKELMR,IGAMA(I))
                          IBELMR(:) = IFFI(:,IB)
                          ICELMR(:) = IKVA(:,IOPA)
                          IAELMR(:) = IKVC(:,IOPC)
                          IKELMR(:) = IFFS(:,IK)
                          EM2       = ELMR(IBELMR,ICELMR,IAELMR,IKELMR,IGAMA(I))*SP
                          EM        = EM1+EM2
                          IF( EM*EM .LT. 1.D-10 ) CYCLE E5
                          EM = EM/EN
                          IF( IU .EQ. 2 ) EM = -EM
! changement de convention pour les operateurs V dont les elements
! matriciels sont imaginaires (pour retablir, dans la majorite des cas,
! les anciens signes des parametres)     4 / 5 / 93
                          IOP = IOP+1
                          IF( IOP .GT. IOP_SUP ) IOP_SUP = IOP
                          IF( IOP .GT. MXEMR   ) CALL RESIZE_MXEMR
                          LI(IOP)   = IK
                          IC(IOP)   = IB
                          EMOP(IOP) = EM
                        ENDDO E5
                      ENDDO
                      WRITE(20,1029) IOP
                      DO KOP=1,IOP
                        WRITE(20,1030) AIM(IU),IC(KOP),LI(KOP),EMOP(KOP)
                      ENDDO
                      WRITE(20,1218) IROTA
!
!    CODAGE DES OPERATEURS ROVIBRATIONNELS
!
                      DO IRO=1,IROTA
                        NROT = NROT+1
                        IF( NROT .GT. NROT_SUP ) NROT_SUP = NROT
                        IF( NROT .GT. MXOCV    ) CALL RESIZE_MXOCV
                        IDRO(NROT)   = IROCO(IRO)
                        IDANIS(NROT) = ICODRA(IRO)
                        IDVC(NROT)   = IVC(1)*1000+IVC(2)*100+IVC(3)*10+IVC(4)
                        IDVA(NROT)   = IVA(1)*1000+IVA(2)*100+IVA(3)*10+IVA(4)
                        IDSY(NROT)   = ICC34*100+ICA34*10+IGAMA(I)
                        IDVI(NROT)   = LOP*10+IGAMA(I)
                      ENDDO
                    ENDDO E733
                  ENDDO E3
                ENDDO E111
!
!    MISE EN ORDRE A L'INTERIEUR D'UNE CASE VIBRATIONNELLE DONNEE
!
                IF( NROT .EQ. 0 ) GOTO 124
                CALL IRDER(NROT,IDRO,IRDRE,MXOCV)
                DO JJJ=1,NROT
                  NBOPT = NBOPT+1
                  IF( NBOPT .GT. NBOPT_SUP ) NBOPT_SUP = NBOPT
                  IF( NBOPT .GT. MXOPT     ) CALL RESIZE_MXOPT
                  IRO           = IRDRE(JJJ)
                  ICODRO(NBOPT) = IDRO(IRO)
                  ICODAN(NBOPT) = IDANIS(IRO)
                  ICODVC(NBOPT) = IDVC(IRO)
                  ICODVA(NBOPT) = IDVA(IRO)
                  ICODSY(NBOPT) = IDSY(IRO)
                  ICODVI(NBOPT) = IDVI(IRO)
                ENDDO
!
124             CONTINUE
              ENDDO E2
            ENDDO E1
          ENDDO E33
        ENDDO E35
      ENDDO E36
      CALL DEBUG( 'POLMOD => MXOPVT=',LOP)
!
!     STOCKAGE DES CODES DES OPERATEURS ROVIBRATIONNELS
!
      WRITE(20,1001)
      WRITE(20,1220) NBOPT
      DO IRO=1,NBOPT
        IM     = ICODRO(IRO)/1000
        IK     = (ICODRO(IRO)-1000*IM)/100
        IG     = (ICODRO(IRO)-1000*IM-100*IK)/10
        JD     = (ICODRO(IRO)-1000*IM-100*IK-10*IG)
        JANIS  = ICODAN(IRO)/10
        ICANIS = ICODAN(IRO)-JANIS*10
        CALL VLNC(ICODVC(IRO),IVC(1),IVC(2),IVC(3),IVC(4))
        CALL VLNC(ICODVA(IRO),IVA(1),IVA(2),IVA(3),IVA(4))
        CALL VLNC(ICODSY(IRO),J,ICC34,ICA34,IGA)
        WRITE(20,1313) IRO,IM,IK,JD,SYM(IG),                          &
                       (IVC(L),L=1,NBVQN),SYM(ICC34),                 &
                       (IVA(L),L=1,NBVQN),SYM(ICA34),                 &
                       SYM(IGA),ICODRO(IRO),ICODVI(IRO),JANIS,ICANIS
      ENDDO
      CALL DEBUG( 'POLMOD => MXSNB=',NFS_SUP)
      CALL DEBUG( 'POLMOD => MXSNV=',NFFS_SUP)
      CALL DEBUG( 'POLMOD => MXEMR=',IOP_SUP)
      CALL DEBUG( 'POLMOD => MXOCV=',NROT_SUP)
      CALL DEBUG( 'POLMOD => MXOPT=',NBOPT_SUP)
      CALL DEBUG( 'POLMOD => MXOPR=',IROTA_SUP)
      CALL DEBUG( 'POLMOD => MXNIV=',NIV_SUP)
      CLOSE(20)
      GOTO 9000
!
9996  PRINT 8003
      GOTO  9999
9997  PRINT 8002
      GOTO  9999
9998  PRINT 8001
9999  PRINT 8000
!
9000  PRINT *
      END PROGRAM POLMOD
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! CALCUL DES K, EXTRAITS PAR LA FONCTION XMKUB.
!     POUR UTILISATION DANS HAM_MODEL
!     JMAXK LIMITE A 9
!
!  REV 29 SEP 94
! REV    JAN 1995 JPC,CW (PARAMETER)
!
      SUBROUTINE CALKM(JMAXK)
      use mod_dppr
      use mod_par_tds
      use mod_main_polmod
      IMPLICIT NONE
      integer          :: JMAXK

! functions
      integer          :: CTR,IUGTR,NSYM1,NTR

      real(kind=dppr)  :: VKC

      integer          :: I,IC1,IC2,IC3,ICO,IKNCOD,IKNT,ILI
      integer          :: ITEST1,ITEST2,ITEST3,IUG1,IUG2,IUG3
      integer          :: J1,J2,J3
      integer          :: N1,N2,N3,NCO,NCO0,NCO1,NLI
!
8000  FORMAT(' !!! CALKM  : STOP ON ERROR')
8100  FORMAT(' !!! CALKM  : CODING ERROR')
8103  FORMAT(' !!! CALKM  : JMAXK TOO LARGE ',I5,' > ',I5)
!
      IF( JMAXK .GT. 9 ) THEN
        PRINT 8103, JMAXK,9
        GOTO  9999
      ENDIF
      NCO1      = 0                                                                                ! INITIALISATION PAR DEFAUT
      NCO0      = 1
      IKINIC(1) = 0
      NLI       = 0
E1:   DO J1=0,JMAXK
E2:     DO J2=J1,JMAXK
E3:       DO J3=J2,JMAXK
            IF( NTR(J1,J2,J3) .EQ. 0 ) CYCLE E3
E4:         DO IUG1=1,2
E5:           DO IUG2=1,2
E6:             DO IUG3=1,2
                  IF( IUGTR(IUG1,IUG2,IUG3) .EQ. 0 ) CYCLE E6
                  NLI = NLI+1
                  IF( NLI .GT. MXKLI ) CALL RESIZE_MXKLI
                  IKJUG(NLI) = 100000*J1+10000*J2+1000*J3+100*IUG1+10*IUG2+IUG3
E7:               DO IC1=1,MXSYM
                    ITEST1 = NSYM1(J1,IUG1,IC1)
                    IF( ITEST1 .EQ. 0 ) CYCLE E7
E8:                 DO IC2=1,MXSYM
                      ITEST2 = NSYM1(J2,IUG2,IC2)
                      IF( ITEST2 .EQ. 0 ) CYCLE E8
E9:                   DO IC3=1,MXSYM
                        ITEST3 = NSYM1(J3,IUG3,IC3)
                        IF( ITEST3           .EQ. 0 ) CYCLE E9
                        IF( CTR(IC1,IC2,IC3) .EQ. 0 ) CYCLE E9
E10:                    DO N1=0,ITEST1-1
E11:                      DO N2=0,ITEST2-1
E12:                        DO N3=0,ITEST3-1
                              CALL KCUBU(J1,J2,J3,IUG1,IUG2,IUG3,N1,N2,N3,IC1,IC2,IC3,VKC,I)
                              IF( ABS(VKC) .LT. 1.D-10 ) CYCLE E12
                              IKNCOD = 100000*IC1+10000*IC2+1000*IC3+100*N1+10*N2+N3
                              IF( NCO0 .EQ. 1 ) THEN
!
! POUR INITIALISATION
!
                                IF( NLI .EQ. 1 ) THEN
                                  IKINIC(2)    = IKNCOD
                                  NCO0         = 2
                                  VK(NLI,NCO0) = VKC
                                  CYCLE E12
                                ENDIF
                              ELSE
!
! POSITIONNEMENT DANS LA LISTE DE CODES, D APRES LA VALEUR COURANTE
!
E13:                            DO NCO=1,NCO0
                                  IKNT = IKNCOD-IKINIC(NCO)
                                  IF    ( IKNT .EQ. 0 ) THEN
                                    GOTO 15
                                  ELSEIF( IKNT .GT. 0 ) THEN
                                    GOTO 16
                                  ENDIF
                                  IF( IKNCOD .LE. IKINIC(NCO-1) ) THEN
                                    PRINT 8100
                                    GOTO  9999
                                  ENDIF
                                  DO ICO=NCO0,NCO,-1
                                    IKINIC(ICO+1) = IKINIC(ICO)
                                    DO ILI=1,NLI
                                      VK(ILI,ICO+1) = VK(ILI,ICO)
                                    ENDDO
                                  ENDDO
                                  IKINIC(NCO) = IKNCOD
                                  VK(NLI,NCO) = VKC
                                  DO ILI=1,NLI-1
                                    VK(ILI,NCO) = 0.D0
                                  ENDDO
                                  NCO1 = NCO0+1
                                  GOTO 20
!
15                                VK(NLI,NCO) = VKC
                                  NCO1        = NCO0
                                  GOTO 20
!
16                                IF( NCO .LT. NCO0 ) THEN
                                    CYCLE E13
                                  ELSE
                                    VK(NLI,NCO0+1) = VKC
                                    IKINIC(NCO0+1) = IKNCOD
                                    NCO1           = NCO0+1
                                    GOTO 20
                                  ENDIF
                                ENDDO E13
                              ENDIF
!
20                            NCO0 = NCO1
                              IF( NCO0 .GE. MXKCO-1 ) CALL RESIZE_MXKCO
                            ENDDO E12
                          ENDDO E11
                        ENDDO E10
                      ENDDO E9
                    ENDDO E8
                  ENDDO E7
                ENDDO E6
              ENDDO E5
            ENDDO E4
          ENDDO E3
        ENDDO E2
      ENDDO E1
      NBKLI = NLI
      NBKCO = NCO0
      CALL DEBUG( 'CALKM  => JMAXK=',JMAXK)
      CALL DEBUG( 'CALKM  => MXKLI=',NBKLI)
      CALL DEBUG( 'CALKM  => MXKCO=',NBKCO)
      RETURN
!
9999  PRINT 8000
      PRINT *
      STOP
!
      END SUBROUTINE CALKM
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! SMIL G.PIERRE MAI 82
! MOD. T.GABARD DEC 92
! REV. 6/10/94
! REV    JAN 1995 JPC,CW (PARAMETER)
!
! *** COUPLAGE DES OPERATEURS CREATIONS ET ANNHILATIONS
!
! ***   < IB !! IC * IA !! IK > = (-1)**(ICB+ICK+IC+IA) * SQRT(IGAMA)
!
!                        ( ICC , ICA ,IGAMA)
! ***   SOMME SUR IBK DE (                 ) <IB!!IC!!IBK><IBK!!IA!!IK>
!                        ( ICB , ICK , ICBK)
!
! ***   AVEC IX(I)       = 1000*VI+100*LI+10*NI+CI     POUR I=1,NBVQN
! ***   ET   IX(NBVQN+1) = 1000*C1+100*C2+10*C23+C234
! ***   AVEC C23=C2*C3   ET   C234=C23*C4
! ***   IX PEUT ETRE  IB,IA,IC OU IK
!
      FUNCTION ELMR(IB,IC,IA,IK,IGAMA)
      use mod_dppr
      use mod_par_tds
      use mod_com_sigvi
      use mod_com_sy
      IMPLICIT NONE
      real(kind=dppr)  :: ELMR
      integer ,dimension(NBVQN+1)  :: IB,IC,IA,IK
      integer          :: IGAMA

! functions
      real(kind=dppr)  :: E1234,SXC
      integer          :: IPSI

      real(kind=dppr)  :: EM1,EM2
      real(kind=dppr)  :: P
      real(kind=dppr)  :: SIX

      integer ,dimension(NBVQN)          :: IPB
      integer ,dimension(NBVQN,4)        :: IBCAK
      integer          :: I,ICA,ICB,ICC,ICK,ICPK,II,IL,IV
      integer          :: J
      integer          :: N,NB
!
      ELMR = 0.D0
!
! *** TEST SUR LES FONCTIONS ET LES OPERATEURS.
!
      DO I=1,NBVQN
        IBCAK(I,1) = IB(I)
        IBCAK(I,2) = IC(I)
        IBCAK(I,3) = IA(I)
        IBCAK(I,4) = IK(I)
      ENDDO
!
! TEST DE LA COHERENCE DE L'ENSEMBLE DES NOMBRES QUANTIQUES
!
      DO J=1,4
        DO I=1,NBVQN
          CALL VLNC(IBCAK(I,J),IV,IL,N,ICC)
          II = I
          IF( I                    .EQ. 4 ) II = 3
          IF( IPSI(II,IV,IL,N,ICC) .EQ. 0 ) RETURN
        ENDDO
      ENDDO
!
! *** TEST SUR LES VI ET LES OMEGAI
!
      DO I=1,NBVQN
        IPK(I,1) = IB(I)/1000-IC(I)/1000
        IPB(I)   = IK(I)/1000-IA(I)/1000
        IF( IPB(I)   .LT. 0      ) RETURN
        IF( IPK(I,1) .NE. IPB(I) ) RETURN
      ENDDO
      ICB = IB(NBVQN+1)-10*(IB(NBVQN+1)/10)
      ICK = IK(NBVQN+1)-10*(IK(NBVQN+1)/10)
      ICC = IC(NBVQN+1)-10*(IC(NBVQN+1)/10)
      ICA = IA(NBVQN+1)-10*(IA(NBVQN+1)/10)
      P   = PC(ICC)*PC(ICA)*PC(ICB)*PC(ICK)*SQRT(DC(IGAMA))
!
! *** SOMMATION SUR LES ETATS INTERMEDIAIRES.
!
      CALL SIGVI(IPB,'IPK',0,NB)
      DO I=1,NB
        EM1  = E1234(1,IB,IC,IPK(1,I))
        EM2  = E1234(-1,IPK(1,I),IA,IK)
        ICPK = IPK(NBVQN+1,I)-10*(IPK(NBVQN+1,I)/10)
        SIX  = SXC(ICC,ICA,IGAMA,ICK,ICB,ICPK)
        ELMR = ELMR+P*SIX*EM1*EM2
      ENDDO
!
      RETURN
      END FUNCTION ELMR
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! DIPMOD, POLMOD version
!
! *** CHERCHE POUR UN ENSEMBLE DONNE DE VI : (IV(I),I=1,4)
! *** LES NB INDICES DE LA SIGNATURE VIBRATIONNELLE
! *** IKNC(I,K), I=1,5 ET K=ISN+1,ISN+NB
!
!  REV 29 SEP 94
!
! ATTENTION : SIGVI peut redimensionner les tableaux dependants de MXC
!
      SUBROUTINE SIGVI(IV,MXC,ISN,NB)                                                              ! SMIL G.PIERRE JUIN 82
      use mod_par_tds
      use mod_main_polmod
      IMPLICIT NONE
      integer            ,dimension(NBVQN)  :: IV
      character(len = *)                    :: MXC
      integer                               :: ISN,NB

      integer ,parameter  :: MDMIB2 = 7
      integer ,parameter  :: MDMIB3 = 22
      integer ,parameter  :: MDMIB4 = 22

      integer ,pointer ,dimension(:,:)     :: IKNC
      integer          ,dimension(MDMIB2)  :: IB2
      integer          ,dimension(MDMIB3)  :: IB3
      integer          ,dimension(MDMIB4)  :: IB4
      integer          ,dimension(MDMIGA)  :: IC23
      integer          ,dimension(MDMIGA)  :: IC234
      integer                              :: I2,I23,I234,I3,I4
      integer                              :: IC2,IC3,IC4
      integer                              :: K
      integer                              :: MDMKNC
      integer                              :: N23,N234
      integer                              :: NB2,NB3,NB4
!
8000  FORMAT(' !!! SIGVI  : STOP ON ERROR'   ,/,   &
             '              ',A,' UNEXPECTED'   )
!
      SELECT CASE( MXC )
        CASE( 'IFFI' )
          MDMKNC = MXSNV
          IKNC => IFFI
        CASE( 'IFFS' )
          MDMKNC = MXSNV
          IKNC => IFFS
        CASE( 'IKVA' )
          MDMKNC = MXSNB
          IKNC => IKVA
        CASE( 'IKVC' )
          MDMKNC = MXSNB
          IKNC => IKVC
        CASE( 'IPK' )
          MDMKNC = MXSNB
          IKNC => IPK
        CASE DEFAULT
          PRINT 8000, MXC
          STOP
!
      END SELECT
!
      K = ISN
      CALL TCUBE(2,IV(2),NB2,IB2,MDMIB2)
      CALL TCUBE(3,IV(3),NB3,IB3,MDMIB3)
      CALL TCUBE(4,IV(4),NB4,IB4,MDMIB4)
      DO I2=1,NB2
        IC2 = IB2(I2)-10*(IB2(I2)/10)
        DO I3=1,NB3
          IC3 = IB3(I3)-10*(IB3(I3)/10)
          CALL MULTD(IC2,IC3,N23,IC23)
          DO I23=1,N23
            DO I4=1,NB4
              IC4 = IB4(I4)-10*(IB4(I4)/10)
              CALL MULTD(IC23(I23),IC4,N234,IC234)
E2:           DO I234=1,N234
                K = K+1
                IF( K .GT. MDMKNC ) THEN
                  SELECT CASE( MXC )
                    CASE( 'IFFI' )
                      CALL RESIZE_MXSNV
                      MDMKNC = MXSNV
                      IKNC => IFFI
                    CASE( 'IFFS' )
                      CALL RESIZE_MXSNV
                      MDMKNC = MXSNV
                      IKNC => IFFS
                    CASE( 'IKVA' )
                      CALL RESIZE_MXSNB
                      MDMKNC = MXSNB
                      IKNC => IKVA
                    CASE( 'IKVC' )
                      CALL RESIZE_MXSNB
                      MDMKNC = MXSNB
                      IKNC => IKVC
                    CASE( 'IPK' )
                      CALL RESIZE_MXSNB
                      MDMKNC = MXSNB
                      IKNC => IPK
                  END SELECT
                ENDIF
                IKNC(1,K) = 1000*IV(1)+1
                IKNC(2,K) = IB2(I2)
                IKNC(3,K) = IB3(I3)
                IKNC(4,K) = IB4(I4)
                IKNC(5,K) = 1000+100*IC2+10*IC23(I23)+IC234(I234)
              ENDDO E2
            ENDDO
          ENDDO
        ENDDO
      ENDDO
      NB = K-ISN
!
      RETURN
      END SUBROUTINE SIGVI
