      PROGRAM HDI
!
!  12.7.88 FORTRAN 77 POUR SUN4  REV 20 MARS 1989
!  REV 25 JAN 1990 REV 8 DEC 93
! REV    JAN 1995 JPC,CW (PARAMETER)
! MODIF 01/99 V. BOUDON ---> SCHEMA DE POLYADE QUELCONQUE.
!
! ***  CALCUL DES VALEURS ET VECTEURS PROPRES D'UN HAMILTONIEN EFFECTIF
!
! APPEL : hdi    Pn Nm Dkk'...k" Jmax para_file_name
!
!  ******    LIMITATIONS DU PROGRAMME
!
! DIMENSION MAXIMALE D'UN BLOC J,C
!     MXDIMS      !NVCOD:NRCOD:K:T:H:HL:HC:HD:TC:TL
!
! NB MAXIMUM D'OPERATEURS ROVIBRATIONNELS
!     MXOPH       !PARA
!
! NB MAXIMUM D'ELEMENTS MATRICIELS NON NULS D'UN BLOC J,C
!     MXELMD      !LI:KO:EL
!
! VALEUR MAXIMALE DE J
!     MXJ
! NB MAXIMUM DE SOUS-NIVEAUX VIBRATIONNELS
!     MXSNV
!
!
      use mod_dppr
      use mod_par_tds
      use mod_com_fdate
      use mod_main_hdi
      IMPLICIT NONE

      real(kind=dppr)  :: TT

      integer          :: I,IBASE,IC,ICLU,IFB,IOPH,IOPLU,IP,ISV,IV
      integer          :: IFB1,IFB2
      integer          :: J,JB,JFB,JLU,JMAX
      integer          :: LMD_SUP
      integer          :: NBELM,NBOPH,NELMA,NFB,NNIV,NSV

      character(len = NBCTIT)  :: TITRE
      character(len =  11) ,dimension(3)  :: CARG
      character(len = 120)  :: FED,FEM,FPARA,FVVPS
      character(len = 120)  :: FES
!
1000  FORMAT(A)
1020  FORMAT(/,              &
             'HDI    : ',A)
1021  FORMAT(' HDI    -> J = ',I3,'/',I3)
2000  FORMAT(' <  ',A)
2001  FORMAT(' >> ',A)
3000  FORMAT(I2)
8000  FORMAT(' !!! HDI    : STOP ON ERROR')
8001  FORMAT(' !!! ERROR OPENING CONTROL FILE')
8002  FORMAT(' !!! INCOMPLETE CONTROL FILE')
8125  FORMAT(' !!! JMAX TOO LARGE'                ,/,   &
             ' !!! MXJ+2  EXCEEDED : ',I8,' > ',I8   )
8128  FORMAT(' !!! MXOPH  EXCEEDED : ',I8,' > ',I8)
!
! ARRAY INITIAL ALLOCATION
!
      CALL ALLOC_ALL
!
      OPEN(10,FILE=CTRLF,ERR=9998,STATUS='OLD')
      READ(10,1000,END=9997) FDATE
      PRINT 1020,            FDATE
      DO I=1,3
        READ(10,1000,END=9997) CARG(I)
      ENDDO
      READ(10,*,END=9997) JMAX
      READ(10,1000,END=9997) FPARA
      CLOSE(10)
      FEM   = 'HA_'//TRIM(CARG(1))//'_'//TRIM(CARG(3))//'_'
      FED   = 'EN_'//TRIM(CARG(1))//'_'
      FES   = 'ME_'//TRIM(CARG(1))//'_'
      FVVPS = 'VP_'//TRIM(CARG(1))//'_'
      PRINT 2000, TRIM(FEM)
      PRINT 2000, TRIM(FPARA)
      PRINT 2001, TRIM(FED)
      PRINT 2001, TRIM(FVVPS)
      PRINT 2001, TRIM(FES)
      READ(CARG(2)(2:3),3000) NNIV
!
!  ***  APPLICATION DES DIRECTIVES
!
      IF( JMAX .GT. MXJ+2 ) THEN
        PRINT 8125, JMAX,MXJ+2
        GOTO  9999
      ENDIF
      OPEN(40,FILE=FEM,STATUS='OLD',FORM='UNFORMATTED')                                            ! ELEMENTS MATRICIELS
      OPEN(70,FILE=FPARA,STATUS='OLD')                                                             ! PARAMETRES
      OPEN(50,FILE=FED,FORM='UNFORMATTED',STATUS='UNKNOWN')                                        ! ENERGIES
      OPEN(60,FILE=FVVPS,FORM='UNFORMATTED',STATUS='UNKNOWN')                                      ! VALEURS ET VECTEURS PROPRES
      OPEN(80,FILE=FES,FORM='UNFORMATTED',STATUS='UNKNOWN')
!
!     LECTURE DES PARAMETRES
!
      CALL IOPAB(70,60)
      REWIND(70)
      CALL IOPAB(70,50)
      CLOSE(70)
!
! LECTURE DES CARACTERISTIQUES GENERALES
!
      DO I=1,3
        READ(40)
      ENDDO
      READ(40)
      DO I=1,4+NNIV
        READ(40)
      ENDDO
      READ (40) NSV,TITRE
      WRITE(60) NSV,TITRE
      WRITE(50) NSV,TITRE
      DO WHILE( NSV .GT. MXSNV )
        CALL RESIZE_MXSNV
      ENDDO
      READ(40)
      DO ISV=1,NSV
        READ (40) TITRE(:58)
        WRITE(50) TITRE(:58)
        WRITE(60) TITRE(:58)
      ENDDO
      DO I=1,3
        READ(40)
      ENDDO
      READ(40) TITRE(:67)
      READ (40) NBOPH
      WRITE(50) NBOPH
      WRITE(60) NBOPH
      IF( NBOPH .GT. MXOPH ) THEN
        PRINT 8128, NBOPH,MXOPH
        GOTO  9999
      ENDIF
!
! ***  BOUCLE J
!
      LMD_SUP = -1
E12:  DO J=0,JMAX
!
! ***  BOUCLE IC
!
E13:    DO IC=1,MXSYM
          READ (40) JLU,ICLU,NELMA,NFB
          WRITE(80) JLU,ICLU,NELMA,NFB
          DO WHILE( NFB .GT. MXDIMS )
            CALL RESIZE_MXDIMS
          ENDDO
          IF( NFB .EQ. 0 ) CYCLE E13
          DO IFB=1,NFB
            DO JFB=1,NFB
              H(IFB,JFB) = 0.D0
            ENDDO
          ENDDO
          READ(40) (NVCOD(IP),NRCOD(IP),IP=1,NFB)
!
! ***  BOUCLE OP H.
!
          NUMI(1) = 1
E14:      DO IOPH=1,NBOPH
            READ(40) JLU,ICLU,NELMA,NFB,IOPLU,NBELM
            NUMI(IOPH+1) = NUMI(IOPH)+NBELM
            IF( NUMI(IOPH+1)-1 .GT. LMD_SUP ) LMD_SUP = NUMI(IOPH+1)-1
            DO WHILE( NUMI(IOPH+1)-1 .GT. MXELMD )
              CALL RESIZE_MXELMD
            ENDDO
            IF( NBELM .EQ. 0 ) CYCLE E14
            READ(40) (LI(I),KO(I),EL(I),I=NUMI(IOPH),NUMI(IOPH+1)-1)
E211:       DO I=NUMI(IOPH),NUMI(IOPH+1)-1
              H(LI(I),KO(I)) = H(LI(I),KO(I))+EL(I)*PARA(IOPH)
              IF( LI(I) .EQ. KO(I) ) CYCLE E211
              H(KO(I),LI(I)) = H(KO(I),LI(I))+EL(I)*PARA(IOPH)
            ENDDO E211
          ENDDO E14
!
! ***  ECRITURE DE HAMILTONIAN
!
          DO IFB1=1,NFB
            DO IFB2=IFB1,NFB
              WRITE(80) H(IFB1,IFB2)
            ENDDO
          ENDDO
!
! ***  DIAGONALISATION ET STOKAGE DES VALEURS ET VECTEURS PROPRES
!
          CALL DIAGO(NFB)
          DO I=1,NFB
            HD(I) = H(I,I)
          ENDDO
          CALL ORDER(NFB,HD,K,MXDIMS)
          WRITE(60) J,IC,NFB
          WRITE(50) J,IC,NFB
          WRITE(60) (HD(K(IP)),IP=1,NFB)
          DO JB=1,NFB
            WRITE(60) NVCOD(JB),NRCOD(JB),(T(JB,K(IP)),IP=1,NFB)
          ENDDO
!
! ***  STOCKAGE DES ENERGIES
!
          DO IP=1,NFB
!
!     COMPOSITION VIBRATIONNELLE
!
            DO ISV=1,NSV
              PCENT(ISV) = 0.D0
            ENDDO
            DO IBASE=1,NFB
              IV        = NVCOD(IBASE)/10
              TT        = T(IBASE,K(IP))
              PCENT(IV) = PCENT(IV)+TT*TT
            ENDDO
            DO ISV=1,NSV
              ICENT(ISV) = 100.D0*PCENT(ISV)+0.5D0
            ENDDO
            WRITE(50) HD(K(IP)),(ICENT(ISV),ISV=1,NSV)
          ENDDO
        ENDDO E13
      ENDDO E12
      CALL DEBUG( 'HDI    => MXELMD=',LMD_SUP)
      CLOSE(80)
      CLOSE(60)
      CLOSE(50)
      CLOSE(40)
      PRINT 1021, J-1,JMAX
      GOTO  9000
!
9997  PRINT 8002
      CLOSE(10)
      GOTO  9999
9998  PRINT 8001
9999  PRINT 8000
      PRINT 1021, J-1,JMAX
!
9000  PRINT *
      END PROGRAM HDI
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!  LECTURE ET RECOPIE DES PARAMETRES
!
      SUBROUTINE IOPAB(LUI,LUO)
      use mod_dppr
      use mod_par_tds
      use mod_main_hdi
      IMPLICIT NONE
      integer          :: LUI,LUO

      real(kind=dppr)  :: PREC

      integer          :: I,IP
      integer          :: NBOPH

      character(len = NBCLAB+10)  :: CHAINE
      character(len = NBCTIT)     :: TITRE
!
1000  FORMAT(A)
1001  FORMAT(I4,A)
1002  FORMAT(A,E18.11,E14.7)
8000  FORMAT(' !!! HDI    : STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF IN IOPAB')
!
      DO I=1,4
        READ (LUI,1000,END=2000) TITRE
        WRITE(LUO)               TITRE
      ENDDO
      READ (LUI,1001,END=2000) NBOPH,TITRE
      WRITE(LUO)               NBOPH,TITRE
      DO WHILE( NBOPH .GT. MXOPH )
        CALL RESIZE_MXOPH
      ENDDO
      DO I=1,2
        READ (LUI,1000,END=2000) TITRE
        WRITE(LUO)               TITRE
      ENDDO
      DO IP=1,NBOPH
        READ (LUI,1002,END=2000) CHAINE,PARA(IP),PREC
        WRITE(LUO)               CHAINE,PARA(IP),PREC
      ENDDO
      RETURN
!
2000  PRINT 8003
      PRINT 8000
      PRINT *
      STOP
!
      END SUBROUTINE IOPAB
