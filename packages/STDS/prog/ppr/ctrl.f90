      PROGRAM CTRL
!
! REV    JAN 1995 JPC,CW (PARAMETER)
! REV    AVR 1995 JPC,CW (NIVEAUX ET STARK)
!
! APPEL : par STDS
!
      use mod_dppr
      use mod_par_tds
      use mod_com_fdate
      use mod_com_fp
      use mod_main_ctr
      IMPLICIT NONE

      integer ,parameter  :: MDLMAX = 100                                                          ! LONGUEUR MAX DE MOLDIR

      real(kind=dppr)  :: FMAX,FMIN
      real(kind=dppr)  :: SMIN
      real(kind=dppr)  :: TROT,TVIB

      integer          :: I,INDSA,INDSR
      integer          :: J,JMAX,JMAXI
      integer          :: K
      integer          :: N
      integer          :: M

      character(len =   2)  :: V1I,V1S,V2I,V2S,V3I,V3S,V4I,V4S
      character(len =   3)  :: NC,NI,NS
      character(len =   3)  :: OPTION
      character(len =   3)  :: PI,PS
      character(len =   4)  :: POLST
      character(len =  10)  :: CARG
      character(len =  12)  :: DI,DS,DT
      character(len = 100)  :: MOLDIR
      character(len = 120)  :: PARAFI,PARAFO                                                       ! CF. MDLMAX

      logical          :: DEFABU = .FALSE.
      logical          :: DEFFPV = .FALSE.
      logical          :: DEFPS  = .FALSE.                                                         ! etat polarisation defini
      logical          :: NBARG2 = .TRUE.
      logical          :: NIV    = .FALSE.
!
1000  FORMAT(I1)
1001  FORMAT(20A)
1020  FORMAT(/,              &
             'CTRL   : ',A)
8000  FORMAT(' !!! CTRL   : STOP ON ERROR')
8001  FORMAT(' !!! ERROR OPENING CONTROL FILE')
8002  FORMAT(' !!! INCOMPLETE CONTROL FILE')
8003  FORMAT(' !!! PARAMETER FILE ERROR')
8004  FORMAT(' !!! OUTPUT CONTROL FILE ERROR')
8005  FORMAT(' !!! PARAMETER FILE DIRECTORY NAME TOO LONG',/,   &
             ' !!! MDLMAX EXCEEDED : ',I8,' > ',I8           )
8006  FORMAT(' !!! NEITHER dip NOR pol NOR str')
!
! ARRAY INITIAL ALLOCATION
!
      CALL ALLOC_ALL
!
!  LECTURE DU FICHIER 'control_argu'
!
      OPEN(20,FILE='control_argu',ERR=9998,STATUS='OLD')
      READ(20,1001,END=9997) FDATE
      PRINT 1020,            FDATE
      READ(20,1001,END=9997) PARAFO
      READ(20,1001,END=9997) PARAFI
      READ(20,*,END=9997) JMAX
      READ(20,*,END=2000) FMIN
      READ(20,*,END=9997) FMAX
      READ(20,*,END=9997) TVIB
      READ(20,*,END=9997) TROT
      READ(20,*,END=9997) SMIN
      NBARG2 = .FALSE.
      READ(20,1001,END=2000) POLST
      IF( POLST(1:1) .EQ. 'R' ) THEN
        DEFPS = .TRUE.
      ELSE
        BACKSPACE(20)
      ENDIF
      READ(20,1001,END=2000) CARG
      IF( CARG .EQ. 'fpvib' ) THEN
        DEFFPV = .TRUE.
        READ(20,*,END=9997) FPVIB
      ELSE
        BACKSPACE(20)
      ENDIF
      READ(20,1001,END=2000) CARG
      IF( CARG .EQ. 'abund' ) THEN
        DEFABU = .TRUE.
        READ(20,*,END=9997) ABUND
      ELSE
        BACKSPACE(20)
      ENDIF
!
2000  CONTINUE
!
!  DETERMINATION DU REPERTOIRE DU FICHIER DE PARAMETRES
!
      INDSA = 0                                                                                    ! INDEX ABSOLU DU DERNIER /
!
3000  INDSR = INDEX(PARAFO(INDSA+1:),'/')
      IF( INDSR .NE. 0 ) THEN                                                                      ! IL Y A ENCORE UN / DANS LA CHAINE
        INDSA = INDSA+INDSR
        GOTO 3000
      ENDIF
      INDSA = INDSA-1
      IF( INDSA .GT. MDLMAX ) THEN                                                                 ! TAILLE DE MOLDIR
        PRINT 8005, INDSA,MDLMAX
        GOTO  9999
      ENDIF
      MOLDIR = PARAFO(:INDSA)
!
!  LECTURE DE LA TROISIEME LIGNE DU FICHIER DE PARAMETRES
!
      OPEN(10,FILE=PARAFI,ERR=9996,STATUS='OLD')
      READ(10,*)
      READ(10,*)
      READ(10,1001,ERR=9996) PS,DS,V1S,V2S,V3S,V4S,  &
                             PI,DI,V1I,V2I,V3I,V4I,  &
                             DT,OPTION
      CLOSE(10)
      IF( OPTION .NE. 'dip' .AND.         &
          OPTION .NE. 'pol' .AND.         &
          OPTION .NE. 'str'       ) THEN
        PRINT 8006
        GOTO  9999
      ENDIF
      IF( OPTION .EQ. 'dip' .OR.         &
          OPTION .EQ. 'str'      ) THEN
        JMAXI = JMAX+1
      ELSE
        JMAXI = JMAX+2
      ENDIF
!
!  transfert d'infos a stds
!
      REWIND(20,ERR=9997)
      IF( PS .EQ. PI ) THEN
        WRITE(20,1001,ERR=9997) 'intra ',OPTION
      ELSE
        WRITE(20,1001,ERR=9997) 'inter ',OPTION
      ENDIF
      IF( NBARG2            .AND.         &
          OPTION .NE. 'str'       ) THEN
        NIV = .TRUE.
      ENDIF
      CLOSE(20,ERR=9997)
!
!  FABRICATION DES FICHIERS DE CONTROLE
!
      READ(PS(2:2),1000) N
      CALL CV1234(N,V1S,V2S,V3S,V4S,NS,M)
      READ(PI(2:2),1000) N
      CALL CV1234(N,V1I,V2I,V3I,V4I,NI,M)
!
!  POLYADE SUP OU CALCUL DES NIVEAUX
!
      IF(  PS .NE. PI .OR.         &
           NIV             ) THEN
!
! HMODEL POLYADE SUP
!
        OPEN(10,FILE='control_hmodel_sup',ERR=9995,STATUS='UNKNOWN')
        WRITE(10,1001) FDATE
        WRITE(10,1001) PS
        READ(PS(2:2),1000) N
        DO I=0,N
          CALL CV1234(I,V1S,V2S,V3S,V4S,NC,M)
          WRITE(10,1001) NC
          WRITE(10,1001) DS(1:1)//DS(I+2:I+2)
          DO J=1,M
            DO K=1,4
              WRITE(10,1001) V1234(K,J)
            ENDDO
          ENDDO
        ENDDO
        NS = NC
        CLOSE(10)
!
! PARCHK
!
        OPEN(10,FILE='control_parchk_h',ERR=9995,STATUS='UNKNOWN')
        WRITE(10,1001) FDATE
        WRITE(10,1001) PS
        WRITE(10,1001) DS
        WRITE(10,1001) MOLDIR
        WRITE(10,1001) PARAFI
        CLOSE(10)
!
! HRED POLYADE SUP
!
        OPEN(10,FILE='control_hred_sup',ERR=9995,STATUS='UNKNOWN')
        WRITE(10,1001) FDATE
        WRITE(10,1001) PS
        WRITE(10,1001) NS
        WRITE(10,1001) DS
        WRITE(10,1001) PARAFI
        CLOSE(10)
!
! ROVBAS POLYADE SUP
!
        OPEN(10,FILE='control_rovbas_sup',ERR=9995,STATUS='UNKNOWN')
        WRITE(10,1001) FDATE
        WRITE(10,1001) PS
        WRITE(10,1001) NS
        WRITE(10,1001) DS
        WRITE(10,*) JMAX
        CLOSE(10)
!
! HMATRI POLYADE SUP
!
        OPEN(10,FILE='control_hmatri_sup',ERR=9995,STATUS='UNKNOWN')
        WRITE(10,1001) FDATE
        WRITE(10,1001) PS
        WRITE(10,1001) NS
        WRITE(10,1001) DS
        WRITE(10,*) JMAX
        CLOSE(10)
!
! HDIAG POLYADE SUP
!
        OPEN(10,FILE='control_hdiag_sup',ERR=9995,STATUS='UNKNOWN')
        WRITE(10,1001) FDATE
        WRITE(10,1001) PS
        WRITE(10,1001) NS
        WRITE(10,1001) DS
        WRITE(10,*) JMAX
        WRITE(10,1001) PARAFI
        CLOSE(10)
! NIVEAUX
        IF( NIV ) GOTO 9000
      ENDIF
!
!  POLYADE INF
!
! HMODEL POLYADE INF
!
      OPEN(10,FILE='control_hmodel_inf',ERR=9995,STATUS='UNKNOWN')
      WRITE(10,1001) FDATE
      WRITE(10,1001) PI
      READ(PI(2:2),1000) N
      DO I=0,N
        CALL CV1234(I,V1I,V2I,V3I,V4I,NC,M)
        WRITE(10,1001) NC
        WRITE(10,1001) DI(1:1)//DI(I+2:I+2)
        DO J=1,M
          DO K=1,4
            WRITE(10,1001) V1234(K,J)
          ENDDO
        ENDDO
      ENDDO
      NI = NC
      CLOSE(10)
!
! PARCHK
!
      IF( PS .EQ. PI ) THEN
        OPEN(10,FILE='control_parchk_h',ERR=9995,STATUS='UNKNOWN')
        WRITE(10,1001) FDATE
        WRITE(10,1001) PI
        WRITE(10,1001) DI
        WRITE(10,1001) MOLDIR
        WRITE(10,1001) PARAFI
        CLOSE(10)
      ENDIF
      OPEN(10,FILE='control_parchk_tm',ERR=9995,STATUS='UNKNOWN')
      WRITE(10,1001) FDATE
      WRITE(10,1001) PS
      WRITE(10,1001) DS
      WRITE(10,1001) PI
      WRITE(10,1001) DT
      WRITE(10,1001) OPTION
      WRITE(10,1001) MOLDIR
      WRITE(10,1001) PARAFI
      CLOSE(10)
!
! HRED POLYADE INF
!
      OPEN(10,FILE='control_hred_inf',ERR=9995,STATUS='UNKNOWN')
      WRITE(10,1001) FDATE
      WRITE(10,1001) PI
      WRITE(10,1001) NI
      WRITE(10,1001) DI
      WRITE(10,1001) PARAFI
      CLOSE(10)
!
! ROVBAS POLYADE INF
!
      OPEN(10,FILE='control_rovbas_inf',ERR=9995,STATUS='UNKNOWN')
      WRITE(10,1001) FDATE
      WRITE(10,1001) PI
      WRITE(10,1001) NI
      WRITE(10,1001) DI
      WRITE(10,*) JMAXI
      CLOSE(10)
!
! HMATRI POLYADE INF
!
      OPEN(10,FILE='control_hmatri_inf',ERR=9995,STATUS='UNKNOWN')
      WRITE(10,1001) FDATE
      WRITE(10,1001) PI
      WRITE(10,1001) NI
      WRITE(10,1001) DI
      WRITE(10,*) JMAXI
      CLOSE(10)
!
! HDIAG POLYADE INF
!
      OPEN(10,FILE='control_hdiag_inf',ERR=9995,STATUS='UNKNOWN')
      WRITE(10,1001) FDATE
      WRITE(10,1001) PI
      WRITE(10,1001) NI
      WRITE(10,1001) DI
      WRITE(10,*) JMAXI
      WRITE(10,1001) PARAFI
      CLOSE(10)
!
! DIPMOD OU POLMOD
!
      IF( OPTION .EQ. 'pol' ) THEN
        OPEN(10,FILE='control_polmod',ERR=9995,STATUS='UNKNOWN')
      ELSE
        OPEN(10,FILE='control_dipmod',ERR=9995,STATUS='UNKNOWN')
      ENDIF
      WRITE(10,1001) FDATE
      WRITE(10,1001) PS
      READ(PS(2:2),1000) N
      DO I=0,N
        CALL CV1234(I,V1S,V2S,V3S,V4S,NC,M)
        WRITE(10,1001) NC
        DO J=1,M
          DO K=1,4
            WRITE(10,1001) V1234(K,J)
          ENDDO
        ENDDO
      ENDDO
      WRITE(10,1001) PI
      READ(PI(2:2),1000) N
      DO I=0,N
        CALL CV1234(I,V1I,V2I,V3I,V4I,NC,M)
        WRITE(10,1001) NC
        DO J=1,M
          DO K=1,4
            WRITE(10,1001) V1234(K,J)
          ENDDO
        ENDDO
      ENDDO
      WRITE(10,1001) DT
      CLOSE(10)
!
! DIPMAT OU POLMAT
!
      IF( OPTION .EQ. 'pol' ) THEN
        OPEN(10,FILE='control_polmat',ERR=9995,STATUS='UNKNOWN')
      ELSE
        OPEN(10,FILE='control_dipmat',ERR=9995,STATUS='UNKNOWN')
      ENDIF
      WRITE(10,1001) FDATE
      WRITE(10,1001) PS
      WRITE(10,1001) NS
      WRITE(10,1001) PI
      WRITE(10,1001) NI
      WRITE(10,1001) DT
      WRITE(10,*) JMAX
      CLOSE(10)
!
      IF( OPTION .EQ. 'str' ) THEN
!
!  STARKL
!
        OPEN(10,FILE='control_starkl',ERR=9995,STATUS='UNKNOWN')
        WRITE(10,1001) FDATE
        WRITE(10,1001) PS
        WRITE(10,1001) NS
        WRITE(10,1001) PI
        WRITE(10,1001) NI
        WRITE(10,1001) DT
        WRITE(10,*) JMAX
        WRITE(10,1001) PARAFI
        CLOSE(10)
      ELSE
!
! TRMOMT
!
        OPEN(10,FILE='control_trmomt',ERR=9995,STATUS='UNKNOWN')
        WRITE(10,1001) FDATE
        WRITE(10,1001) PS
        WRITE(10,1001) NS
        WRITE(10,1001) PI
        WRITE(10,1001) NI
        WRITE(10,1001) DT
        WRITE(10,*) JMAX
        WRITE(10,1001) PARAFI
        WRITE(10,1001) OPTION
        IF( DEFPS ) WRITE(10,1001) POLST
        CLOSE(10)
!
! TRANS
!
        OPEN(10,FILE='control_trans',ERR=9995,STATUS='UNKNOWN')
        WRITE(10,1001) FDATE
        WRITE(10,1001) PS
        WRITE(10,1001) PI
        WRITE(10,1001) DT
        WRITE(10,*) JMAX
        WRITE(10,1001) 'range'
        WRITE(10,*) FMIN
        WRITE(10,*) FMAX
        IF( OPTION .EQ. 'pol' ) WRITE(10,1001) OPTION
        CLOSE(10)
!
! SPECT
!
        OPEN(10,FILE='control_spect',ERR=9995,STATUS='UNKNOWN')
        WRITE(10,1001) FDATE
        WRITE(10,*) FMIN
        WRITE(10,*) FMAX
        WRITE(10,*) TVIB
        WRITE(10,*) TROT
        WRITE(10,*) SMIN
        IF( DEFFPV ) THEN
          WRITE(10,1001) 'fpvib'
          WRITE(10,*)     FPVIB
        ENDIF
        IF( DEFABU ) THEN
          WRITE(10,1001) 'abund'
          WRITE(10,*)     ABUND
        ENDIF
        CLOSE(10)
      ENDIF
      GOTO 9000
!
9995  PRINT 8004
      GOTO  9999
9996  PRINT 8003
      GOTO  9999
9997  PRINT 8002
      GOTO  9999
9998  PRINT 8001
      GOTO  9999
9999  PRINT 8000
!
9000  PRINT *
      END PROGRAM CTRL
