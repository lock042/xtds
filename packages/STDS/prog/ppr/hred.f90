      PROGRAM HRED
!
!      5 JAN 1995 JPC,CW
!
! MODIF 01/99 V. BOUDON ---> SCHEMA DE POLYADE QUELCONQUE.
!
! APPEL : par STDS, STDH
!
! NB MAXIMUM D'OPERATEURS ROVIBRATIONNELS
!     MXOPH               !NOCOD
!
!
      use mod_dppr
      use mod_par_tds
      use mod_com_fdate
      use mod_main_hred
      IMPLICIT NONE

      real(kind=dppr)  :: PARA,PREC

      integer          :: I,IEL,IOPH,IP,IROTA,ISV
      integer          :: NBOMOD,NBOMR,NBOPAR,NBOPR,NBOPT,NEL,NNIV,NSV

      character(len = NBCLAB+10)  :: CHAINE
      character(len = NBCTIT)     :: TITRE,TITR1
      character(len =  11) ,dimension(3)  :: CARG
      character(len =   9)  :: FOUT   = 'hred_temp'                                                ! NOM DU FICHIER DE SORTIE
      character(len = 120)  :: FEMRV,FPARA
      character(len = 130)  :: FEMRVT,FPARAT
!
1000  FORMAT(A)
1001  FORMAT(I4,A)
1003  FORMAT(/,       &
             I4,A,/)
1005  FORMAT(A,E18.11,E14.7)
1010  FORMAT(/,              &
             'HRED   : ',A)
1031  FORMAT(/,       &
             I5,A,/)
2000  FORMAT(' <  ',A)
2001  FORMAT(' >> ',A)
2002  FORMAT(I2)
8000  FORMAT(' !!! HRED   : STOP ON ERROR')
8001  FORMAT(' !!! ERROR OPENING CONTROL FILE')
8002  FORMAT(' !!! INCOMPLETE CONTROL FILE')
!
! ARRAY INITIAL ALLOCATION
!
      CALL ALLOC_ALL
!
      OPEN(10,FILE=CTRLF,ERR=9998,STATUS='OLD')
      READ(10,1000,END=9997) FDATE
      PRINT 1010,            FDATE
      DO I=1,3
        READ(10,1000,END=9997) CARG(I)
      ENDDO
      READ(10,1000,END=9997) FPARA
      CLOSE(10)
      FPARAT = TRIM(FPARA)//'_hred_temp'
      FEMRV  = 'MH_'//TRIM(CARG(1))//'_'//CARG(3)                                                  ! EMRV IN
      FEMRVT = TRIM(FEMRV)//'_hred_temp'                                                           ! EMRV OUT
      PRINT 2000, TRIM(FPARA)
      PRINT 2000, TRIM(FEMRV)
      PRINT 2001, TRIM(FPARAT)
      PRINT 2001, TRIM(FEMRVT)
      PRINT 2001, FOUT
      READ(CARG(2)(2:3),2002) NNIV
!
! RECOPIE DU DEBUT DU FICHIER DES EMRV
! ET DETERMINATION DE SON NB D'OPERATEURS (NBOMOD)
!
      OPEN(20,STATUS='OLD',FILE=FEMRV)
      OPEN(21,FILE=FEMRVT,STATUS='UNKNOWN')
      DO I=1,7+NNIV
        READ(20,1000)    TITRE
        IF( LEN_TRIM(TITRE) .EQ. 0 ) THEN
          WRITE(21,1000)
        ELSE
          WRITE(21,1000) TRIM(TITRE)
        ENDIF
      ENDDO
      READ (20,1003) NSV,TITRE
      WRITE(21,1003) NSV,TRIM(TITRE)
      DO ISV=1,NSV
        READ (20,1000) TITRE(:58)
        WRITE(21,1000) TITRE(:58)
      ENDDO
      DO I=1,3
        READ(20,1000)    TITRE
        IF( LEN_TRIM(TITRE) .EQ. 0 ) THEN
          WRITE(21,1000)
        ELSE
          WRITE(21,1000) TRIM(TITRE)
        ENDIF
      ENDDO
!
! LECTURE DES ELEMENTS MATRICIELS REDUITS
!
1     READ(20,1000)    TITRE
      IF( LEN_TRIM(TITRE) .EQ. 0 ) THEN
        WRITE(21,1000)
      ELSE
        WRITE(21,1000) TRIM(TITRE)
      ENDIF
      IF( TITRE(1:3) .EQ. '   ' ) GOTO 2201
      DO I=1,4
        READ(20,1000)    TITRE
        IF( LEN_TRIM(TITRE) .EQ. 0 ) THEN
          WRITE(21,1000)
        ELSE
          WRITE(21,1000) TRIM(TITRE)
        ENDIF
      ENDDO
      READ (20,1031) NEL,TITRE
      WRITE(21,1031) NEL,TRIM(TITRE)
!
!      KOR <=> < | BRA
!      LIR <=> | > KET
!
      DO IEL=1,NEL
        READ (20,1000) TITRE
        WRITE(21,1000) TRIM(TITRE)
      ENDDO
      READ (20,1003) IROTA,TITRE
      WRITE(21,1003) IROTA,TRIM(TITRE)
      READ(20,1000)
      WRITE(21,1000)
      GOTO 1
!
! OPERATEURS DE L'HAMILTONIEN
!
2201  READ (20,1003) NBOMOD,TITR1
      DO WHILE( NBOMOD .GT. MXOPH )
        CALL RESIZE_MXOPH
      ENDDO
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
! FICHIER DE PARAMETRES
! DETERMINATION DES PARAS NON NULS        (TABLEAU NOCOD)
! ET DE LEUR NOMBRE POUR FICHIER DES EMRV (NBOMR)
!                ET POUR FICHIER DE PARAS (NBOPR)
!
      OPEN(30,STATUS='OLD',FILE=FPARA)                                                             ! PARAMETRES IN
      DO I=1,4
        READ(30,1000) TITRE
      ENDDO
      READ(30,1001) NBOPAR,TITRE
      DO WHILE( NBOPAR .GT. MXOPH )
        CALL RESIZE_MXOPH
      ENDDO
      DO I=1,2
        READ(30,1000) TITRE
      ENDDO
      NBOMR = 0
      NBOPR = 0
      DO IP=1,NBOPAR
        READ(30,1005) CHAINE,PARA,PREC
        IF( PARA .EQ. 0.D0 ) THEN
          NOCOD(IP) = 1
        ELSE
          NOCOD(IP) = 0
          IF( IP .LE. NBOMOD ) NBOMR = NBOMR+1
          NBOPR = NBOPR+1
        ENDIF
      ENDDO
      REWIND(30)
!
! RECOPIE DU FICHIER DE PARAS REDUIT
!
      OPEN(31,FILE=FPARAT,STATUS='UNKNOWN')                                                        ! PARAMETRES OUT
      DO I=1,4
        READ(30,1000)    TITRE
        IF( LEN_TRIM(TITRE) .EQ. 0 ) THEN
          WRITE(31,1000)
        ELSE
          WRITE(31,1000) TRIM(TITRE)
        ENDIF
      ENDDO
      READ (30,1001) NBOPAR,TITRE
      WRITE(31,1001) NBOPR ,TRIM(TITRE)
      DO I=1,2
        READ(30,1000)    TITRE
        IF( LEN_TRIM(TITRE) .EQ. 0 ) THEN
          WRITE(31,1000)
        ELSE
          WRITE(31,1000) TRIM(TITRE)
        ENDIF
      ENDDO
E1:   DO IP=1,NBOPAR
        READ(30,1005)  CHAINE,PARA,PREC
        IF( PARA .EQ. 0.D0 ) CYCLE E1
        WRITE(31,1005) CHAINE,PARA,PREC
      ENDDO E1
      DO I=1,4
        READ(30,1000)    TITRE
        IF( LEN_TRIM(TITRE) .EQ. 0 ) THEN
          WRITE(31,1000)
        ELSE
          WRITE(31,1000) TRIM(TITRE)
        ENDIF
      ENDDO
      READ (30,1001) NBOPT,TITRE
      WRITE(31,1001) NBOPT,TRIM(TITRE)
      DO I=1,2
        READ(30,1000)    TITRE
        IF( LEN_TRIM(TITRE) .EQ. 0 ) THEN
          WRITE(31,1000)
        ELSE
          WRITE(31,1000) TRIM(TITRE)
        ENDIF
      ENDDO
      DO IP=1,NBOPT
        READ (30,1005) CHAINE,PARA,PREC
        WRITE(31,1005) CHAINE,PARA,PREC
      ENDDO
      CLOSE(30)
      CLOSE(31)
!
! RECOPIE DES OPERATEURS DU FICHIER DES EMRV
!
      WRITE(21,1003) NBOMR,TRIM(TITR1)
E2:   DO IOPH=1,NBOMOD
        READ(20,1000)  TITRE
        IF( NOCOD(IOPH) .EQ. 1 ) CYCLE E2
        WRITE(21,1000) TRIM(TITRE)
      ENDDO E2
      CLOSE(20)
      CLOSE(21)
!
! FICHIER POUR mv DE STDS
!
      OPEN(11,FILE=FOUT,STATUS='UNKNOWN')
      WRITE(11,1000) FPARA
      WRITE(11,1000) FEMRV
      CLOSE(11)
      GOTO 9000
!
9997  PRINT 8002
      CLOSE(10)
      GOTO  9999
9998  PRINT 8001
9999  PRINT 8000
!
9000  PRINT *
      END PROGRAM HRED
