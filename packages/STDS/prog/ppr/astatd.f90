      PROGRAM ASTATD
!
! REV    SEPT 2006  CW
!
!  lire un fichier binaire (ex. INFILE) de type TA_ ou TD_
!  et l'ecrire en ASCII dans INFILE'_ASC'
!
! APPEL : astatd
!
      use mod_dppr
      use mod_par_tds
      use mod_main_trmomt
      IMPLICIT NONE

      real(kind=dppr)  :: TM

      integer          :: ICK,ICL,IK,IKC,IL,ILC,IN,IOP,ISV
      integer          :: JK,JL
      integer          :: NFBK,NFBL,NSV

      character(len = NBCTIT)  :: TITRE
      character(len =  80)  :: NOM
      character(len = 150)  :: INFILE
      character(len = 160)  :: OUTFILE
!
1000  FORMAT(A)
1020  FORMAT(/,            &
             'ASTATD : ')
2000  FORMAT(' <  ',A)
2001  FORMAT(' >> ',A)
7000  FORMAT('ENTER TA_ or TD_ TYPE FILE NAME :')
8000  FORMAT(' !!! ASTATD : STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF FOR ',A)
!
! ARRAY INITIAL ALLOCATION
!
      CALL ALLOC_ALL
!
      PRINT 1020
!
!  FICHIER D'ENTREE
!
      PRINT 7000
      READ(*,1000) INFILE
      PRINT 2000,  TRIM(INFILE)
      OPEN(81,FILE=TRIM(INFILE),FORM='UNFORMATTED',STATUS='OLD')
!
!  FICHIER DE SORTIE
!
      OUTFILE = TRIM(INFILE)//'_ASC'
      PRINT 2001,  TRIM(OUTFILE)
      OPEN(10,FILE=TRIM(OUTFILE))
!
! *** LECTURE DES PARAMETRES DE H
!
      CALL IOPBAS(81,10)
!
! *** LECTURE DES PARAMETRES DU MOMENT DE TRANSITION
!
      CALL IOPBAS(81,10)
      DO IN=1,2                                                                                    ! BOUCLE SUR LES DEUX NIVEAUX
        WRITE(10,*) '>>> NSV'
        READ (81,END=9003) NSV,TITRE
        WRITE(10,*)        NSV,TITRE
        DO ISV=1,NSV
          READ (81,END=9003) TITRE(:58)
          WRITE(10,*)        TITRE(:58)
        ENDDO
      ENDDO
      WRITE(10,*) '>>> NBOPT'
      READ (81,END=9003) NBOPT,NOM(:24)
      WRITE(10,*)        NBOPT,NOM(:24)
      DO WHILE( NBOPT .GT. MXOPT )
        CALL RESIZE_MXOPT
      ENDDO
      WRITE(10,*) '>>> JK,ICK,NFBK,JL,ICL,NFBL'
      WRITE(10,*) '>>> IK,IL,TM,(DERITR(IOP),IOP=1,NBOPT)'
!
3     READ (81,END=9000) JK,ICK,NFBK,JL,ICL,NFBL
      WRITE(10,*)        JK,ICK,NFBK,JL,ICL,NFBL
      DO IKC=1,NFBK
        DO ILC=1,NFBL
          READ (81,END=9003) IK,IL,TM,(DERITR(IOP),IOP=1,NBOPT)
          WRITE(10,*)        IK,IL,TM,(DERITR(IOP),IOP=1,NBOPT)
        ENDDO
      ENDDO
      GOTO 3
!
9003  PRINT 8003, TRIM(INFILE)
      PRINT 8000
!
9000  CLOSE(10)
      PRINT *
      END PROGRAM ASTATD
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!     LECTURE ET RECOPIE DES PARAMETRES
!
      SUBROUTINE IOPBAS(LUI,LUO)
      use mod_dppr
      use mod_par_tds
      IMPLICIT NONE
      integer          :: LUI,LUO

      real(kind=dppr)  :: PARA,PREC

      integer          :: I,IP
      integer          :: NBOPH

      character(len = NBCLAB+10)  :: CHAINE
      character(len = NBCTIT)     :: TITRE
!
1000  FORMAT(A)
1001  FORMAT(I4,A)
1002  FORMAT(A,E18.11,E14.7)
8000  FORMAT(' !!! ASTATD : STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF IN IOPBAS')
!
      DO I=1,4
        READ (LUI,END=2000) TITRE
        WRITE(LUO,1000)     TRIM(TITRE)
      ENDDO
      READ (LUI,END=2000) NBOPH,TITRE
      WRITE(LUO,1001)     NBOPH,TRIM(TITRE)
      DO I=1,2
        READ (LUI,END=2000) TITRE
        WRITE(LUO,1000)     TRIM(TITRE)
      ENDDO
      DO IP=1,NBOPH
        READ (LUI,END=2000) CHAINE,PARA,PREC
        WRITE(LUO,1002)     CHAINE,PARA,PREC
      ENDDO
      RETURN
!
2000  PRINT 8003
      PRINT 8000
      PRINT *
      STOP
!
      END SUBROUTINE IOPBAS
