      PROGRAM ASXTRO
!
! 2008 AUGUST
! V.BOUDON, CH.WENGER
!
!  lire un fichier binaire (ex. INFILE) de type XTRO_
!  et l'ecrire en ASCII dans INFILE'_ASC'
!
! APPEL : asxtro
!
      use mod_dppr
      use mod_par_tds
      use mod_main_trmomt
      IMPLICIT NONE

      real(kind=dppr)  :: TM

      integer          :: ICK,ICL,IK,IKC,IL,ILC,IOP
      integer          :: JK,JL
      integer          :: NBOTR,NFBK,NFBL

      character(len = 150)  :: INFILE
      character(len = 160)  :: OUTFILE
!
1000  FORMAT(A)
1020  FORMAT(/,            &
             'ASXTRO : ')
2000  FORMAT(' <  ',A)
2001  FORMAT(' >> ',A)
7000  FORMAT('ENTER XTRO_ TYPE FILE NAME :')
8000  FORMAT(' !!! ASXTRO : STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF FOR ',A)
!
! ARRAY INITIAL ALLOCATION
!
      CALL ALLOC_ALL
!
      PRINT 1020
!
!  FICHIER D'ENTREE
!
      PRINT 7000
      READ(*,1000) INFILE
      PRINT 2000,  TRIM(INFILE)
      OPEN(81,FILE=TRIM(INFILE),FORM='UNFORMATTED',STATUS='OLD')
!
!  FICHIER DE SORTIE
!
      OUTFILE = TRIM(INFILE)//'_ASC'
      PRINT 2001,  TRIM(OUTFILE)
      OPEN(10,FILE=TRIM(OUTFILE))
      WRITE(10,*) '>>> NBOTR'
      READ (81,END=9003) NBOTR
      WRITE(10,*)        NBOTR
      DO WHILE( NBOTR .GT. MXOPT )
        CALL RESIZE_MXOPT
      ENDDO
      WRITE(10,*) '>>> JK,ICK,NFBK,JL,ICL,NFBL'
      WRITE(10,*) '>>> IK,IL,TM,(DERITR(IOP),IOP=1,NBOTR)'
!
3     READ (81,END=9000) JK,ICK,NFBK,JL,ICL,NFBL
      WRITE(10,*)        JK,ICK,NFBK,JL,ICL,NFBL
      DO IKC=1,NFBK
        DO ILC=1,NFBL
          READ (81,END=9003) IK,IL,TM,(DERITR(IOP),IOP=1,NBOTR)
          WRITE(10,*)        IK,IL,TM,(DERITR(IOP),IOP=1,NBOTR)
        ENDDO
      ENDDO
      GOTO 3
!
9003  PRINT 8003, TRIM(INFILE)
      PRINT 8000
!
9000  CLOSE(10)
      PRINT *
      END PROGRAM ASXTRO
