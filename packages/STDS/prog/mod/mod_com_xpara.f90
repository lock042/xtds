
      module mod_com_xpara                                                                         ! parameter related COMMON

      use mod_dppr
      use mod_par_tds
      use mod_par_x
      use mod_alloc
      IMPLICIT NONE

      integer ,save  :: NBOP  = 0
      integer ,save  :: NBXOP = 0
!
      real(kind=dppr)            ,pointer ,dimension(:)     ,save  :: PARA                         ! (MXOP)
      real(kind=dppr)            ,pointer ,dimension(:)     ,save  :: PARAO                        ! (MXOP)
      real(kind=dppr)            ,pointer ,dimension(:)     ,save  :: PARAR                        ! (MXOP)
      real(kind=dppr)            ,pointer ,dimension(:)     ,save  :: PRAT                         ! (MXOP)
      real(kind=dppr)            ,pointer ,dimension(:)     ,save  :: VALAT                        ! (MXOP)
      real(kind=dppr)            ,pointer ,dimension(:)     ,save  :: XPARA                        ! (MXOP)
      real(kind=dppr)            ,pointer ,dimension(:,:)   ,save  :: CTRIBH                       ! (MXOPH,MXOPH)
      real(kind=dppr)            ,pointer ,dimension(:,:,:) ,save  :: CTRIBT                       ! (MXATRT,MXOPT,MXOPT)
      real(kind=dppr)            ,pointer ,dimension(:,:)   ,save  :: COV                          ! (MXOP,MXOP)
      real(kind=dppr)            ,pointer ,dimension(:,:)   ,save  :: POIDF                        ! (MXOP,MXATR)
      real(kind=dppr)            ,pointer ,dimension(:,:)   ,save  :: POIDS                        ! (MXOP,MXATR)

      integer                    ,pointer ,dimension(:)     ,save  :: ICNTRL                       ! (MXOP)
      integer                    ,pointer ,dimension(:)     ,save  :: LIB                          ! (MXOP)
      integer                    ,pointer ,dimension(:)     ,save  :: INPHT                        ! (0:MXATRT)
      integer                    ,pointer ,dimension(:)     ,save  :: INXPHT                       ! (0:MXATRT)
      integer                    ,pointer ,dimension(:)     ,save  :: NBOHT                        ! (0:MXATRT)
      integer                    ,pointer ,dimension(:)     ,save  :: NBXOHT                       ! (0:MXATRT)

      character(len = NBCLAB+10) ,pointer ,dimension(:)     ,save  :: COPAJ                        ! (MXOP)


      contains

!
! ARRAY ALLOCATION
!
      subroutine ALLOC_XPARA
      IMPLICIT NONE

      integer  :: ierr

      allocate(PARA(MXOP),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XPARA_PARA')
      PARA = 0.d0
      allocate(PARAO(MXOP),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XPARA_PARAO')
      PARAO = 0.d0
      allocate(PARAR(MXOP),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XPARA_PARAR')
      PARAR = 0.d0
      allocate(PRAT(MXOP),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XPARA_PRAT')
      PRAT = 0.d0
      allocate(VALAT(MXOP),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XPARA_VALAT')
      VALAT = 0.d0
      allocate(XPARA(MXOP),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XPARA_XPARA')
      XPARA = 0.d0
      allocate(CTRIBH(MXOPH,MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XPARA_CTRIBH')
      CTRIBH = 0.d0
      allocate(CTRIBT(MXATRT,MXOPT,MXOPT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XPARA_CTRIBT')
      CTRIBT = 0.d0
      allocate(COV(MXOP,MXOP),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XPARA_COV')
      COV = 0.d0
      allocate(POIDF(MXOP,MXATR),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XPARA_POIDF')
      POIDF = 0.d0
      allocate(POIDS(MXOP,MXATR),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XPARA_POIDS')
      POIDS = 0.d0

      allocate(ICNTRL(MXOP),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XPARA_ICNTRL')
      ICNTRL = 0
      allocate(LIB(MXOP),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XPARA_LIB')
      LIB = 0
      allocate(INPHT(0:MXATRT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XPARA_INPHT')
      INPHT = 0
      allocate(INXPHT(0:MXATRT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XPARA_INXPHT')
      INXPHT = 0
      allocate(NBOHT(0:MXATRT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XPARA_NBOHT')
      NBOHT = 0
      allocate(NBXOHT(0:MXATRT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XPARA_NBXOHT')
      NBXOHT = 0

      allocate(COPAJ(MXOP),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XPARA_COPAJ')
      COPAJ = ''
!
      return
      end subroutine ALLOC_XPARA

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXATR

      subroutine RESIZE_MXATR_XPARA(C_ATR)
      IMPLICIT NONE
      integer :: C_ATR

      integer :: i,ierr

! POIDF
      allocate(rpd2(MXOP,C_ATR),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXATR_XPARA_POIDF')
      rpd2 = 0.d0
      do i=1,MXATR
        rpd2(:,i) = POIDF(:,i)
      enddo
      deallocate(POIDF)
      POIDF => rpd2
! POIDS
      allocate(rpd2(MXOP,C_ATR),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXATR_XPARA_POIDS')
      rpd2 = 0.d0
      do i=1,MXATR
        rpd2(:,i) = POIDS(:,i)
      enddo
      deallocate(POIDS)
      POIDS => rpd2
!
      return
      end subroutine RESIZE_MXATR_XPARA

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXATRT

      subroutine RESIZE_MXATRT_XPARA(C_ATRT)
      IMPLICIT NONE
      integer :: C_ATRT

      integer :: i,ierr
      integer :: j

! CTRIBT
      allocate(rpd3(C_ATRT,MXOPT,MXOPT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXATRT_XPARA_CTRIBT')
      rpd3 = 0.d0
      do j=1,MXOPT
        do i=1,MXOPT
          rpd3(1:MXATRT,i,j) = CTRIBT(:,i,j)
        enddo
      enddo
      deallocate(CTRIBT)
      CTRIBT => rpd3
! INPHT
      allocate(ipd1(0:C_ATRT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXATRT_XPARA_INPHT')
      ipd1 = 0
      ipd1(0:MXATRT) = INPHT(:)
      deallocate(INPHT)
      INPHT => ipd1
! INXPHT
      allocate(ipd1(0:C_ATRT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXATRT_XPARA_INXPHT')
      ipd1 = 0
      ipd1(0:MXATRT) = INXPHT(:)
      deallocate(INXPHT)
      INXPHT => ipd1
! NBOHT
      allocate(ipd1(0:C_ATRT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXATRT_XPARA_NBOHT')
      ipd1 = 0
      ipd1(0:MXATRT) = NBOHT(:)
      deallocate(NBOHT)
      NBOHT => ipd1
! NBXOHT
      allocate(ipd1(0:C_ATRT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXATRT_XPARA_NBXOHT')
      ipd1 = 0
      ipd1(0:MXATRT) = NBXOHT(:)
      deallocate(NBXOHT)
      NBXOHT => ipd1
!
      return
      end subroutine RESIZE_MXATRT_XPARA

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOP

      subroutine RESIZE_MXOP_XPARA(C_OP)
      IMPLICIT NONE
      integer :: C_OP

      integer :: i,ierr

      character(len = NBCLAB+10) ,pointer ,dimension(:)  :: cpd1

! PARA
      allocate(rpd1(C_OP),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOP_XPARA_PARA')
      rpd1 = 0.d0
      rpd1(1:MXOP) = PARA(:)
      deallocate(PARA)
      PARA => rpd1
! PARAO
      allocate(rpd1(C_OP),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOP_XPARA_PARAO')
      rpd1 = 0.d0
      rpd1(1:MXOP) = PARAO(:)
      deallocate(PARAO)
      PARAO => rpd1
! PARAR
      allocate(rpd1(C_OP),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOP_XPARA_PARAR')
      rpd1 = 0.d0
      rpd1(1:MXOP) = PARAR(:)
      deallocate(PARAR)
      PARAR => rpd1
! PRAT
      allocate(rpd1(C_OP),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOP_XPARA_PRAT')
      rpd1 = 0.d0
      rpd1(1:MXOP) = PRAT(:)
      deallocate(PRAT)
      PRAT => rpd1
! VALAT
      allocate(rpd1(C_OP),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOP_XPARA_VALAT')
      rpd1 = 0.d0
      rpd1(1:MXOP) = VALAT(:)
      deallocate(VALAT)
      VALAT => rpd1
! XPARA
      allocate(rpd1(C_OP),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOP_XPARA_XPARA')
      rpd1 = 0.d0
      rpd1(1:MXOP) = XPARA(:)
      deallocate(XPARA)
      XPARA => rpd1
! COV
      allocate(rpd2(C_OP,C_OP),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOP_XPARA_COV')
      rpd2 = 0.d0
      do i=1,MXOP
        rpd2(1:MXOP,i) = COV(:,i)
      enddo
      deallocate(COV)
      COV => rpd2
! POIDF
      allocate(rpd2(C_OP,MXATR),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOP_XPARA_POIDF')
      rpd2 = 0.d0
      do i=1,MXATR
        rpd2(1:MXOP,i) = POIDF(:,i)
      enddo
      deallocate(POIDF)
      POIDF => rpd2
! POIDS
      allocate(rpd2(C_OP,MXATR),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOP_XPARA_POIDS')
      rpd2 = 0.d0
      do i=1,MXATR
        rpd2(1:MXOP,i) = POIDS(:,i)
      enddo
      deallocate(POIDS)
      POIDS => rpd2

! ICNTRL
      allocate(ipd1(C_OP),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOP_XPARA_ICNTRL')
      ipd1 = 0
      ipd1(1:MXOP) = ICNTRL(:)
      deallocate(ICNTRL)
      ICNTRL => ipd1
! LIB
      allocate(ipd1(C_OP),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOP_XPARA_LIB')
      ipd1 = 0
      ipd1(1:MXOP) = LIB(:)
      deallocate(LIB)
      LIB => ipd1

! COPAJ
      allocate(cpd1(C_OP),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOP_XPARA_COPAJ')
      cpd1 = ''
      cpd1(1:MXOP) = COPAJ(:)
      deallocate(COPAJ)
      COPAJ => cpd1
!
      return
      end subroutine RESIZE_MXOP_XPARA

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOPH

      subroutine RESIZE_MXOPH_XPARA(C_OPH)
      IMPLICIT NONE
      integer :: C_OPH

      integer :: i,ierr

! CTRIBH
      allocate(rpd2(C_OPH,C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_XPARA_CTRIBH')
      rpd2 = 0.d0
      do i=1,MXOPH
        rpd2(1:MXOPH,i) = CTRIBH(:,i)
      enddo
      deallocate(CTRIBH)
      CTRIBH => rpd2
!
      return
      end subroutine RESIZE_MXOPH_XPARA

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOPT

      subroutine RESIZE_MXOPT_XPARA(C_OPT)
      IMPLICIT NONE
      integer :: C_OPT

      integer :: i,ierr
      integer :: j

! CTRIBT
      allocate(rpd3(MXATRT,C_OPT,C_OPT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPT_XPARA_CTRIBT')
      rpd3 = 0.d0
      do j=1,MXOPT
        do i=1,MXOPT
          rpd3(:,i,j) = CTRIBT(:,i,j)
        enddo
      enddo
      deallocate(CTRIBT)
      CTRIBT => rpd3
!
      return
      end subroutine RESIZE_MXOPT_XPARA

      end module mod_com_xpara
