
      module mod_com_hred

      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      integer ,pointer ,dimension(:) ,save  :: NOCOD                                               ! (MXOPH)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_HRED
      IMPLICIT NONE

      integer  :: ierr

      allocate(NOCOD(MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_HRED_NOCOD')
      NOCOD = 0
!
      return
      end subroutine ALLOC_HRED

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOPH

      subroutine RESIZE_MXOPH_HRED(C_OPH)
      IMPLICIT NONE
      integer :: C_OPH

      integer :: ierr

! NOCOD
      allocate(ipd1(C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_HRED_NOCOD')
      ipd1 = 0
      ipd1(1:MXOPH) = NOCOD(:)
      deallocate(NOCOD)
      NOCOD => ipd1
!
      return
      end subroutine RESIZE_MXOPH_HRED

      end module mod_com_hred
