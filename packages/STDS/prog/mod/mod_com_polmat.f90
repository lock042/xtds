
      module mod_com_polmat

      use mod_dppr
      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      integer ,parameter  :: MDMJCI = 5*MXSYM

      integer ,pointer ,dimension(:)   ,save  :: ICODPO                                            ! (MXOPT)
      integer ,pointer ,dimension(:)   ,save  :: JICDIM                                            ! (MDMJCI)
      integer ,pointer ,dimension(:,:) ,save  :: NRCI,NVCI                                         ! (MXDIMI,MDMJCI)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_POLMAT
      IMPLICIT NONE

      integer  :: ierr

      allocate(ICODPO(MXOPT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_POLMAT_ICODPO')
      ICODPO = 0
      allocate(JICDIM(MDMJCI),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_POLMAT_JICDIM')
      JICDIM = 0
      allocate(NRCI(MXDIMI,MDMJCI),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_POLMAT_NRCI')
      NRCI = 0
      allocate(NVCI(MXDIMI,MDMJCI),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_POLMAT_NVCI')
      NVCI = 0
!
      return
      end subroutine ALLOC_POLMAT

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXDIMI

      subroutine RESIZE_MXDIMI_POLMAT(C_DIMI)
      IMPLICIT NONE
      integer :: C_DIMI

      integer :: i,ierr

! NRCI
      allocate(ipd2(C_DIMI,MDMJCI),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXDIMI_POLMAT_NRCI')
      ipd2 = 0
      do i=1,MDMJCI
        ipd2(1:MXDIMI,i) = NRCI(:,i)
      enddo
      deallocate(NRCI)
      NRCI => ipd2
! NVCI
      allocate(ipd2(C_DIMI,MDMJCI),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXDIMI_POLMAT_NVCI')
      ipd2 = 0
      do i=1,MDMJCI
        ipd2(1:MXDIMI,i) = NVCI(:,i)
      enddo
      deallocate(NVCI)
      NVCI => ipd2
!
      return
      end subroutine RESIZE_MXDIMI_POLMAT

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOPT

      subroutine RESIZE_MXOPT_POLMAT(C_OPT)
      IMPLICIT NONE
      integer :: C_OPT

      integer :: ierr

! ICODPO
      allocate(ipd1(C_OPT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPT_POLMAT_ICODPO')
      ipd1 = 0
      ipd1(1:MXOPT) = ICODPO(:)
      deallocate(ICODPO)
      ICODPO => ipd1
!
      return
      end subroutine RESIZE_MXOPT_POLMAT

      end module mod_com_polmat
