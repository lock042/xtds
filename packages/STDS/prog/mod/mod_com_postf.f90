
      module mod_com_postf

      use mod_dppr
      IMPLICIT NONE

      real(kind=dppr) ,save  :: FNORMF = 0.d0
      real(kind=dppr) ,save  :: FNORMI = 0.d0
      real(kind=dppr) ,save  :: RMSHF  = 0.d0
      real(kind=dppr) ,save  :: RMSHI  = 0.d0
      real(kind=dppr) ,save  :: RMSTF  = 0.d0
      real(kind=dppr) ,save  :: RMSTI  = 0.d0
      real(kind=dppr) ,save  :: STDF   = 0.d0
      real(kind=dppr) ,save  :: STDI   = 0.d0

      end module mod_com_postf
