
      module mod_com_ctr

      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      character(len =   4) ,pointer ,dimension(:,:) ,save  :: V1234                                ! (4,MXNIV)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_CTR
      IMPLICIT NONE

      integer  :: ierr

      allocate(V1234(4,MXNIV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_CTR_V1234')
      V1234 = ''
!
      return
      end subroutine ALLOC_CTR

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXNIV

      subroutine RESIZE_MXNIV_CTR(C_NIV)
      IMPLICIT NONE
      integer :: C_NIV

      integer :: i,ierr

      character(len =   4) ,pointer ,dimension(:,:)  :: cpd2_4

! V1234
      allocate(cpd2_4(4,C_NIV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXNIV_CTR_V1234')
      cpd2_4 = ''
      do i=1,4
        cpd2_4(i,1:MXNIV) = V1234(i,:)
      enddo
      deallocate(V1234)
      V1234 => cpd2_4
!
      return
      end subroutine RESIZE_MXNIV_CTR

      end module mod_com_ctr
