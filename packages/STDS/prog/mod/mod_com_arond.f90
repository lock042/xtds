
      module mod_com_arond

      IMPLICIT NONE

      character(len =  48) ,save  :: CARON


      contains

!
! WRITE CLEANLY IN A STRING (47 CHARS MINI)
! A VALUE , IT'S STANDARD DEVIATION AND VARIATION
!
      subroutine AROND(VAL,ECT,VAR,CHAINE)
      use mod_dppr
      IMPLICIT NONE
      real(kind=dppr)       :: VAL,ECT,VAR
      character(len =   *)  :: CHAINE                                                              !!!CHARACTER*(*) CHAINE

      integer               :: IECT,IECTX,IVALX
      integer               :: JEXP
      integer               :: KEXP
      integer               :: NBDEC

      character(len =  80)  :: FOR
!
1000  FORMAT(E9.2)
1001  FORMAT(I3)
1002  FORMAT('(''( '',F16.',I2,',''('',I2,'') '',F16.',I2,','' ) X10'',I3)')
1003  FORMAT(I2)
1004  FORMAT('( ',F16.1,'(',F3.1,') ',F15.1,' ) X10',I3)
1005  FORMAT('(',14X,'***(',F3.1,') ',F15.1,' ) X10',I3)
1006  FORMAT('( ',F16.1,'(',F3.1,') ',F15.1,' )')
1007  FORMAT('(''( '',F16.',I2,',''('',I2,'') '',F16.',I2,','' )'')')
1008  FORMAT('(',E10.3E3,'(',E10.3E3,')',E10.3E3,')')
!
      WRITE(FOR,1000) ECT
      READ(FOR(4:5),1003,ERR=1) IECT
      READ(FOR(7:9),1001,ERR=1) IECTX
      WRITE(FOR,1000) VAL
      READ(FOR(7:9),1001,ERR=1) IVALX
      IF( IVALX .LE. 0                        .OR.         &
          ( IVALX .GT. 0 .AND. IECTX .GT. 1 )      ) THEN
        JEXP = IVALX-1
        IF    ( IVALX .GT. IECTX ) THEN
          NBDEC = 2+JEXP-IECTX
          WRITE(FOR,1002) NBDEC,NBDEC
          WRITE(CHAINE,FOR) VAL/( (10.D0)**JEXP ),IECT,VAR/( (10.D0)**JEXP ),JEXP
        ELSEIF( IVALX .EQ. IECTX ) THEN
          WRITE(CHAINE,1004) VAL/( (10.D0)**JEXP ),DBLE(IECT)/10.D0,VAR/( (10.D0)**JEXP ),JEXP
        ELSE
          KEXP = IECTX-1
          WRITE(CHAINE,1005) DBLE(IECT)/10.D0,VAR/( (10.D0)**KEXP ),KEXP
        ENDIF
      ELSE
        IF( IECTX .EQ. 1 ) THEN
          WRITE(CHAINE,1006) VAL,ECT,VAR
        ELSE
          NBDEC = 2-IECTX
          WRITE(FOR,1007) NBDEC,NBDEC
          WRITE(CHAINE,FOR) VAL,IECT,VAR
        ENDIF
      ENDIF
      RETURN
!
1     WRITE(CHAINE,1008) VAL,ECT,VAR
!
      RETURN
      end subroutine AROND

      end module mod_com_arond
