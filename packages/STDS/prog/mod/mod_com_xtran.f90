
      module mod_com_xtran                                                                         ! transition related COMMON

      use mod_dppr
      use mod_par_tds
      use mod_par_x
      use mod_alloc
      IMPLICIT NONE

      integer ,save  :: NAD  = 0
      integer ,save  :: NTR  = 0
      integer ,save  :: NTRT = 0
!
      real(kind=dppr)      ,pointer ,dimension(:) ,save  :: PNEG                                   ! (MXATR)

      integer              ,pointer ,dimension(:) ,save  :: INPI,INPS,INTRT                        ! (MXTRAD)
      integer              ,pointer ,dimension(:) ,save  :: IMEGA,NBFTR,NBSTR                      ! (MXATR)
      integer              ,pointer ,dimension(:) ,save  :: ITRHT                                  ! (0:MXATRT)

      logical              ,pointer ,dimension(:) ,save  :: ISRAM                                  ! (MXTRAD)

      character(len =   3) ,pointer ,dimension(:) ,save  :: CTR                                    ! (MXATR)
      character(len =   4) ,pointer ,dimension(:) ,save  :: POLST,POLSTC                           ! (MXATR)
      character(len =  10) ,pointer ,dimension(:) ,save  :: CDVO                                   ! (MXATR)
      character(len = 120) ,pointer ,dimension(:) ,save  :: FPARA                                  ! (MXTRAD)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_XTRAN
      IMPLICIT NONE

      integer  :: ierr

      allocate(PNEG(MXATR),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XTRAN_PNEG')
      PNEG = 0.d0

      allocate(INPI(MXTRAD),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XTRAN_INPI')
      INPI = 0
      allocate(INPS(MXTRAD),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XTRAN_INPS')
      INPS = 0
      allocate(INTRT(MXTRAD),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XTRAN_INTRT')
      INTRT = 0
      allocate(IMEGA(MXATR),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XTRAN_IMEGA')
      IMEGA = 0
      allocate(NBFTR(MXATR),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XTRAN_NBFTR')
      NBFTR = 0
      allocate(NBSTR(MXATR),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XTRAN_NBSTR')
      NBSTR = 0
      allocate(ITRHT(0:MXATRT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XTRAN_ITRHT')
      ITRHT = 0

      allocate(ISRAM(MXTRAD),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XTRAN_ISRAM')
      ISRAM = .false.

      allocate(CTR(MXATR),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XTRAN_CTR')
      CTR = ''
      allocate(POLST(MXATR),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XTRAN_POLST')
      POLST = ''
      allocate(POLSTC(MXATR),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XTRAN_POLSTC')
      POLSTC = ''
      allocate(CDVO(MXATR),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XTRAN_CDVO')
      CDVO = ''
      allocate(FPARA(MXTRAD),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XTRAN_FPARA')
      FPARA = ''
!
      return
      end subroutine ALLOC_XTRAN

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXATR

      subroutine RESIZE_MXATR_XTRAN(C_ATR)
      IMPLICIT NONE
      integer :: C_ATR

      integer :: ierr

      character(len =   3) ,pointer ,dimension(:)  :: cpd1_3
      character(len =   4) ,pointer ,dimension(:)  :: cpd1_4
      character(len =  10) ,pointer ,dimension(:)  :: cpd1_10

! PNEG
      allocate(rpd1(C_ATR),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXATR_XTRAN_PNEG')
      rpd1 = 0.d0
      rpd1(1:MXATR) = PNEG(:)
      deallocate(PNEG)
      PNEG => rpd1

! IMEGA
      allocate(ipd1(C_ATR),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXATR_XTRAN_IMEGA')
      ipd1 = 0
      ipd1(1:MXATR) = IMEGA(:)
      deallocate(IMEGA)
      IMEGA => ipd1
! NBFTR
      allocate(ipd1(C_ATR),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXATR_XTRAN_NBFTR')
      ipd1 = 0
      ipd1(1:MXATR) = NBFTR(:)
      deallocate(NBFTR)
      NBFTR => ipd1
! NBSTR
      allocate(ipd1(C_ATR),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXATR_XTRAN_NBSTR')
      ipd1 = 0
      ipd1(1:MXATR) = NBSTR(:)
      deallocate(NBSTR)
      NBSTR => ipd1

! CTR
      allocate(cpd1_3(C_ATR),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXATR_XTRAN_CTR')
      cpd1_3 = ''
      cpd1_3(1:MXATR) = CTR(:)
      deallocate(CTR)
      CTR => cpd1_3
! POLST
      allocate(cpd1_4(C_ATR),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXATR_XTRAN_POLST')
      cpd1_4 = ''
      cpd1_4(1:MXATR) = POLST(:)
      deallocate(POLST)
      POLST => cpd1_4
! POLSTC
      allocate(cpd1_4(C_ATR),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXATR_XTRAN_POLSTC')
      cpd1_4 = ''
      cpd1_4(1:MXATR) = POLSTC(:)
      deallocate(POLSTC)
      POLSTC => cpd1_4
! CDVO
      allocate(cpd1_10(C_ATR),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXATR_XTRAN_CDVO')
      cpd1_10 = ''
      cpd1_10(1:MXATR) = CDVO(:)
      deallocate(CDVO)
      CDVO => cpd1_10
!
      return
      end subroutine RESIZE_MXATR_XTRAN

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXATRT

      subroutine RESIZE_MXATRT_XTRAN(C_ATRT)
      IMPLICIT NONE
      integer :: C_ATRT

      integer :: ierr

! ITRHT
      allocate(ipd1(0:C_ATRT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXATRT_XTRAN_ITRHT')
      ipd1 = 0
      ipd1(0:MXATRT) = ITRHT(:)
      deallocate(ITRHT)
      ITRHT => ipd1
!
      return
      end subroutine RESIZE_MXATRT_XTRAN

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXTRAD

      subroutine RESIZE_MXTRAD_XTRAN(C_TRAD)
      IMPLICIT NONE
      integer :: C_TRAD

      integer :: ierr

      character(len = 120) ,pointer ,dimension(:)  :: cpd1

! INPI
      allocate(ipd1(C_TRAD),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXTRAD_XTRAN_INPI')
      ipd1 = 0
      ipd1(1:MXTRAD) = INPI(:)
      deallocate(INPI)
      INPI => ipd1
! INPS
      allocate(ipd1(C_TRAD),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXTRAD_XTRAN_INPS')
      ipd1 = 0
      ipd1(1:MXTRAD) = INPS(:)
      deallocate(INPS)
      INPS => ipd1
! INTRT
      allocate(ipd1(C_TRAD),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXTRAD_XTRAN_INTRT')
      ipd1 = 0
      ipd1(1:MXTRAD) = INTRT(:)
      deallocate(INTRT)
      INTRT => ipd1

! ISRAM
      allocate(lpd1(C_TRAD),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXTRAD_XTRAN_ISRAM')
      lpd1 = .false.
      lpd1(1:MXTRAD) = ISRAM(:)
      deallocate(ISRAM)
      ISRAM => lpd1

! FPARA
      allocate(cpd1(C_TRAD),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXTRAD_XTRAN_FPARA')
      cpd1 = ''
      cpd1(1:MXTRAD) = FPARA(:)
      deallocate(FPARA)
      FPARA => cpd1
!
      return
      end subroutine RESIZE_MXTRAD_XTRAN

      end module mod_com_xtran
