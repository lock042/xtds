
      module mod_com_ckdipo

      use mod_dppr
      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      real(kind=dppr) ,pointer ,dimension(:) ,save  :: VDK                                         ! (MXDK)

      integer         ,pointer ,dimension(:) ,save  :: IDKX                                        ! (MXDK)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_CKDIPO
      use mod_alloc
      IMPLICIT NONE

      integer  :: ierr

      allocate(VDK(MXDK),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_CKDIPO_VDK')
      VDK = 0.d0

      allocate(IDKX(MXDK),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_CKDIPO_IDKX')
      IDKX = 0
!
      return
      end subroutine ALLOC_CKDIPO

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXDK

      subroutine RESIZE_MXDK_CKDIPO(C_DK)
      IMPLICIT NONE
      integer :: C_DK

      integer :: ierr

! VDK
      allocate(rpd1(C_DK),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXDK_CKDIPO_VDK')
      rpd1 = 0.d0
      rpd1(1:MXDK) = VDK(:)
      deallocate(VDK)
      VDK => rpd1
! IDKX
      allocate(ipd1(C_DK),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXDK_CKDIPO_IDKX')
      ipd1 = 0
      ipd1(1:MXDK) = IDKX(:)
      deallocate(IDKX)
      IDKX => ipd1
!
      return
      end subroutine RESIZE_MXDK_CKDIPO

      end module mod_com_ckdipo
