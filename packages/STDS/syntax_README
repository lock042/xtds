
This file explains the syntax of the calling parameters of the different programs used in the *TDS jobs.
This is illustrated through different examples taken from HTDS and STDS.

1) Spectrum calculation jobs:
-----------------------------

We detail here the job for the calculation of nu2+nu6 of 32SF6. We have put comments
at the end of some lines:

          >  corresponds to lines that should not be modified by the user.
          >> corresponds to lines that can be modified by the user.


------------------------------ File: HTDS/jobs/32SF6/job_cal_nu2+nu6 ------------------------------------------------------

#! /bin/sh
 set -v
##
## Calculation of the absorption spectrum
## of nu2+nu6 of SF6
##
BASD=/.../.../HTDS                                                  >  HTDS install directory.
#
 SCRD=$BASD/prog/exe
 PARD=$BASD/para/32SF6
##
## Jmax values.
##
 JPlow=36                                                           >> Maximum J value for the lower level.
 JPupp=35                                                           >> Maximum J value for the upper level.
 JPupp_low=35                                                       >> Maximum J value for the calculated spectrum.
##
## Parameter file.
##
 PARA=$PARD/Pa_010001m000000                                        >> Hamiltonian and dipole moment (or polarisability)
##                                                                     parameter file.
##
#################################################
##
## Hamiltonian matrix elements.
##
## Lower Polyad.
##
 $SCRD/passx hmodel P0 N1 D2   0 0 0 0 0 0                          >> Hamiltonian model for lower polyad.
 $SCRD/passx rovbas P0 N1 D2 $JPlow                                 >> Rovibrational basis for lower polyad.
 $SCRD/passx hmatri P0 N1 D2 $JPlow                                 >> Hamiltonian matrix elements for lower polyad.
 $SCRD/passx hdi    P0 N1 D2 $JPlow $PARA                           >> Diagonalization.
##
## Upper Polyad.
##
 $SCRD/passx hmodel P3 N1 D2   0 0 0 0 0 0   \                      >> Hamiltonian model for upper polyad.
                       N1 D3   0 1 0 0 0 0   \
                       N1 D3   0 0 0 0 0 1   \
                       N1 D3   0 1 0 0 0 1
 $SCRD/passx parchk P3    D2333 $PARD $PARA                         >> Check validity of the parameter file.
 $SCRD/passx rovbas P3 N1 D2333 $JPupp                              >> Rovibrational basis for upper polyad.
 $SCRD/passx hmatri P3 N1 D2333 $JPupp                              >> Hamiltonian matrix elements for upper polyad.
 $SCRD/passx hdi    P3 N1 D2333 $JPupp $PARA                        >> Diagonalization.
 $SCRD/passx jener  P3                                              >> Eigenvalues calculation.
##
## Dipole moment matrix elements.
##
## Upper - lower level transition.
##
 $SCRD/passx dipmod P3 N1   0 0 0 0 0 0   \                         >> Dipole moment model.
                       N1   0 1 0 0 0 0   \
                       N1   0 0 0 0 0 1   \
                       N1   0 1 0 0 0 1   \
                    P0 N1   0 0 0 0 0 0   \
                    D1
 $SCRD/passx parchk P3    D2333 P0    D1 dip $PARD $PARA            >> Check validity of the parameter file.
 $SCRD/passx dipmat P3 N1       P0 N1 D1 $JPupp_low                 >> Dipole moment matrix elements.
##
## Transition moment.
##
 $SCRD/passx trm    P3 N1       P0 N1 D1 $JPupp_low $PARA dip       >> Transition moments.
 $SCRD/passx tra    P3    P0          D1 $JPupp_low                 >> Frequencies and transition moments.
##
## Spectrum calculation.
##
 $SCRD/passx spect  986.0  996.0  30.0  30.0 1.0                    >> Spectrum calculation.
##
 rm trans.t
 rm DI* EN* FN* HA* MD* MH* TR* VP*                                 >  Intermediate files removal.

---------------------------------------------------------------------------------------------------------------------------

Description of the command lines to call the different programs:

NOTE: The job passx is used to read the command line arguments and to send them to the program through a
      file named control.
      \ is used as line continuation character.

                                                                                  \
   - hmodel Pn Nm  Dk  v1  v2  v3  v4  v5  v6   ...   w1  w2  w3  w4  w5  w6  \    |
               Nm' Dk' v1' v2' v3' v4' v5' v6'  ...   w1' w2' w3' w4' w5' w6' \     > (n+1 lines)
               .............................................................  \    |
               .............................................................      /
            plus an extra parameter in D2hTDS case: Ir|IIr|IIIr

            With: n is the running number of the polyad in the vibrational extrapolation scheme
                  considered.
                  m, m' are the number of vibrational levels in the polyads P0, ..., Pn (one value
                  per line) if P0 is the lower polyad.
                  k, k' are order of development the P0, ..., Pn part of the hamiltonian.
                  v1 ... v6 etc. are the m vibrational quantum number sets for each level of
                  each polyad.
                  Ir, IIr or IIIr is mandatory in D2hTDS case to specify the axis representation.

            First  example: HTDS, 32SF6, nu2+nu6 level.

                  hmodel P3 N1 D2   0 0 0 0 0 0   \                 >> Ground state, order 2.
                            N1 D3   0 1 0 0 0 0   \                 >> nu2         , order 3.
                            N1 D3   0 0 0 0 0 1   \                 >> nu6         , order 3.
                            N1 D3   0 1 0 0 0 1                     >> nu2+nu6     , order 3.

            Second example: HTDS, 32SF6, nu3, nu2+nu6 interacting dyad.

                  hmodel P3 N1 D2   0 0 0 0 0 0   \                 >> Ground state, order 2.
                            N1 D3   0 1 0 0 0 0   \                 >> nu2         , order 3.
                            N1 D3   0 0 0 0 0 1   \                 >> nu6         , order 3.
                            N2 D3   0 0 1 0 0 0     0 1 0 0 0 1     >> nu3, nu2+nu6, order 3.

            Third  example: D2hTDS, C2H4, nu12 level.

                   hmodel P1 N1  D4  0 0 0 0 0 0 0 0 0 0 0 0 \      >> Ground state, order 4.
                             N1  D8  0 0 0 0 0 0 0 0 0 0 0 1 \      >> nu12        , order 8.
                          Ir                                        >> axis representation

   - parchk                                                         >> see below after dipmod

   - rovbas Pn Nm Dk Jmax                                           >> Basis rovibrational functions up to Jmax.

   - hmatri Pn Nm Dk Jmax                                           >> Hamiltonian matrix up to Jmax.

   - hdi    Pn Nm Dkk'...k" Jmax para_file_name                     >> Diagonalization using parameters in file
                                                                       para_file_name.

   - dipmod Pn  Nm  v1  v2  v3  v4  v5  v6   ...   w1  w2  w3  w4  w5  w6  \      \
                Nm' v1' v2' v3' v4' v5' v6'  ...   w1' w2' w3' w4' w5' w6' \       > (n+1 lines)
                ...........................                                \      /
            Pn' Nm" v1" v2" v3" v4" v5" v6"  ...   w"1 w"2 w"3 w"4 w"5 w"6 \      \
                ...........................                                \       > (n'+1 lines)
                ...........................                                \      /
            Dp  (plus an extra parameter in D2hTDS case: Ir|IIr|IIIr)

            With: Pn is the upper polyad and Pn' the lower polyad.
                  p is the dipole moment development order.
                  The other arguments have the same meaning as in hmodel.
                  Ir, IIr or IIIr is mandatory in D2hTDS case to specify the axis representation.
            NOTE: For Raman transitions, one should use polmod instead of dipmod (same arguments).

   - parchk Pn Dkk'...k" [Pn' Dp trans_type] para_file_directory para_file_name
                                                                    >> Check validity of the parameter file,
                                                                       using para_file_directory/Pa_skel (parameter
                                                                       skeleton file) and MH_*, MD_* or MP_*

   - dipmat Pn Nm Pn' Nm' Dp Jmax                                   >> Dipole moment matrix up to Jmax.

            NOTE: Use polmat instead of dipmat for Raman transitions..

   - trm    Pn Nm Pn' Nm' Dp Jmax para_file_name trans_type [pol_state]
                                                                    >> Transition moment using parameters in file
                                                                       para_file_name.
                                                                       trans_type = dip or pol.
                                                                       if trans_type=pol,
                                                                       pol_state=  R111   |   R110   |   R001
                                                                       means      without | parallel | perpendicular

   - tra    Pn    Pn'     Dp Jmax [pol] [MHz|GHz] [range FMIN FMAX] >> Writes, in file trans.t, the list of frequencies
                                                                       and transition moments.
                                                                       pol option in case of Raman.
                                                                       MHz|GHz option to specify frequency unit
                                                                       (default is cm-1).
                                                                       range option to limit the output between
                                                                       FMIN and FMAX frequencies.

   - spect fmin fmax Trot Tvib Imin [fpvib FPVIB] [abund ABUND]     >> Calculates the spectrum between fmin and fmax,
                                                                       for rotational and vibrational temperatures Trot
                                                                       and Tvib (in K). The intensity threshold is Imin.
                                                                       Vibrational partition function value FPVIB can be
                                                                       specified through fpvib option.
                                                                       Isotopic abundance ABUND can be specified through
                                                                       abund option.
                                                                       The result is written in files spectr.t (detailed
                                                                       form) and spectr.xy (two columns: frequency,
                                                                       intensity).

2) Parameter fitting jobs:
--------------------------

  Three different kind of jobs can be used to fit experimental data:

    a) jobs using eq_tds and paradj to fit line positions only.
    b) jobs using eq_int and paradj to fit line intensities only.
    c) jobs using xpafit to fit line positions and/or intensities.

    A job for calculation of hamiltonian matrix elements (job_hme_xxx) has to be run
    before fit. It is built on the same principles as the jobs described in the preceeding paragraph.

  2a) frequency parameter fitting using eq_tds:
  ---------------------------------------------

    We give below some explanations concerning one example of nu2+nu6 of 32SF6 (HTDS).

------------------------------ File: HTDS/jobs/fit_examples/job_fit_32SF6_nu2+nu6 -----------------------------------------

#! /bin/sh
 set -v
##
## Example of parameter fitting :
## nu2+nu6 of 32SF6.
##
BASD=/.../.../HTDS                                                  >  HTDS install directory.
#
 SCRD=$BASD/prog/exe
 PARD=$BASD/para/32SF6
 EXPD=$BASD/exp/32SF6
 CTRD=$BASD/ctrp/32SF6
##
## Jmax values.
##
 JPlow=29
 JPupp=28
 JPupp_low=28
##
## Parameter file.
##
 SPARA=Pa_010001
 WPARA=$SPARA"_work"
##
## Assignment file.
##
 ASG=$EXPD/dip/ASG_Herman                                           >> Experimental file with assignments.
##
## Parameter constrain file.
##
 CLF=$CTRD/010001                                                   >> File for parameter fitting control (see below).
##
 PARA=$SPARA"_nulpar"                                               >> Reduced parameter file.
 $SCRD/passx nulpar H $CLF $WPARA $PARA                             >> Remove parameters fixed to 0.
###############################################################
##
## Hamiltonian diagonalization.
##
## 000000 level.
##
 $SCRD/passx hdiag  P0 N1 D2 $JPlow $PARA                           >> Like hdi (see above) with derivated
##                                                                     energy calculation.
## 010001 level.
##
 $SCRD/passx hdiag  P3 N1 D2224 $JPupp $PARA
##
## nu2+nu6 fit.
##
 $SCRD/exasg '010001m000000' $ASG                                   >> Extracts assignment lines containing the specified
                                                                       string.
 \cp ASG_EXP ASG_010001mP0.t
 \mv ASG_EXP assignments.t
##
 \cp $CLF CL_H_010001
##
 $SCRD/passx eq_tds P3 P0 $JPupp_low $PARA CL_H_010001 prec         >> Calculates normal equations.
 \mv normal_eq.t         NQ_H_010001_010001mP0                      >  Normal equation file (output of eq_tds).
 \mv prediction.t        Pred_010001mP0                             >  File for comparison between observed and
                                                                       calculated frequencies.
 \mv prediction_mix.t    Pred_mix_010001mP0                         >  As above with level mixings.
 \mv statistics.t        Stat_010001mP0                             >  Observed - calculated statistics for each J.
##
## New Hamiltonian parameter estimates.
##
 $SCRD/passx paradj H NQ_H_010001_010001mP0 $PARA                   >> New parameter estimation.
 \rm NQ_H_010001_010001mP0
##
 \rm assignments.t
 \rm CL* ED*     VP*
###
### The new parameter file is :
###
###                 NQ_H_010001_010001mP0_adj
###
#\mv NQ_H_010001_010001mP0_adj $WPARA

---------------------------------------------------------------------------------------------------------------------------

Description of the command lines to call the different programs:

   - nulpar H|T CLF FOPAR FNPAR                                     >> creates a FNPAR parameter file by removing parameter
                                                                       fixed to 0 (in CLF) from FOPAR.
                                                                       H or T specified for
                                                                       Hamiltonian or transition parameters.

   - eq_tds Pn Pn' Jmax para_file control_file [prec] [MHz|GHz] [mix N]
                                                                    >> Calculates normal equations for the
                                                                       transition Pn-Pn' up to Jmax (upper level),
                                                                       using parameters in para_file with parameter status
                                                                       in control_file.
                                                                       prec option used to output precision on calculated
                                                                       line positions.
                                                                       MHz|GHz option to specify frequency unit
                                                                       (default is cm-1).
                                                                       mix option to specify the number of greatest
                                                                       vibrational components of the eigenbasis
                                                                       decomposition on the initial basis set.

   - paradj H normal_equation_file para_file           \            >> Fit the Hamiltonian parameters using constraints
                                                                       described in control_file.
              .................... .........                           Multiple pairs of normal_equation and parameter_file
                                                                       can be given.


The file for parameter fitting control contains line in the form :

                                              (a)        (b)             (c)

   1  2(0,0A1g) 000000A1g 000000A1g A1g 02     1                    0.0000000E+00
   2  4(0,0A1g) 000000A1g 000000A1g A1g 04     2 -0.63369998149E-08 0.6000000E-08
   3  4(4,0A1g) 000000A1g 000000A1g A1g 04     3  0.18196943510E-09 0.2000000E-09
   4  0(0,0A1g) 000001F2u 000001F2u A1g 20     1                    0.0000000E+00
   5  1(1,0F1g) 000001F2u 000001F2u F1g 21     1                    0.0000000E+00
   6  2(0,0A1g) 000001F2u 000001F2u A1g 22     4                    0.0000000E+00
   7  2(2,0E g) 000001F2u 000001F2u E g 22     0                    0.0000000E+00

             .........................

The first columns describe the parameter. Column (a) contains the type of constrain:

   0: Parameter fixed to 0.
   1: Parameter fixed to its current value.
   2: Parameter bound to the value in column (b) +/- value in column (c).
   3: Parameter bound to its current value with maximum relative deviation given in column (c).
   4: Free parameter.

  2b) intensity parameter fitting using eq_int:
  ---------------------------------------------

    We give below some explanations concerning the example of Pentad of 12CH4 (STDS).
    The use of eq_int is very similar to that of eq_tds.

------------------------------ File: STDS/jobs/fit_examples/job_fit_12CH4_Pentad ------------------------------------------

#! /bin/sh
 set -v
##
## Example of intensity parameter fitting :
## Pentad band of 12CH4.
##
BASD="/.../.../STDS"                                                >   STDS install directory.
##
 SCRD=$BASD/prog/exe
 PARD=$BASD/para/12CH4
 EXPD=$BASD/exp/12CH4
 CTRD=$BASD/ctrp/12CH4
##
## Jmax values.
##
 JPlow=22
 JPupp=21
 JPupp_low=21
##
## Parameter file.
##
 SPARA=Pa_PENTADmGS
 WPARA=$SPARA"_work"
##
## Assignment files.
##
 ASG1=$EXPD/dip/ASG_Brown
 ASG2=$EXPD/dip/ASG_PR_JQSRT97_sca
 ASG3=$EXPD/dip/ASG_Q_JCP92_sca
 ASG4=$EXPD/dip/ASG_JOSA76_sca
 ASG5=$EXPD/dip/ASG_JMS79
 ASG6=$EXPD/dip/ASG_calib
##
## Parameter constraint file.
##
 CLF=$CTRD/Pentad
##
 PARA=$SPARA"_nulpar"
 $SCRD/passx nulpar T $CLF $WPARA $PARA
#########################################################
##
## Hamiltonian diagonalization and transition moments.
##
## GS and Pentad levels.
##
 $SCRD/passx hdiag  P0 N1 D6       $JPlow     $PARA
 $SCRD/passx hdiag  P2 N5 D665     $JPupp     $PARA
 $SCRD/passx trmomt P2 N5 P0 N1 D3 $JPupp_low $PARA dip             >> Like trm (see above) with derivated
##                                                                     transition moment calculation.
## Pentad fit.
##
 \cp $CLF CL_T_P2mP0
##
 $SCRD/exasg 'P2mP0' $ASG1
 \cp ASG_EXP ASG_Brown.t
 \mv ASG_EXP assignments.t
##
 $SCRD/passx eq_int P2 P0 D3 296 1.D-4 $JPupp_low $PARA CL_T_P2mP0 prec
                                                                    >> Calculates normal equations.
 \mv normal_eq.t       NQ_T_P2mP0_1
 \mv prediction.t      Pred_P2mP0_1
 \mv prediction_mix.t  Pred_mix_P2mP0_1
 \mv statistics.t      Stat_P2mP0_1
##
 $SCRD/exasg 'P2mP0' $ASG2
 \cp ASG_EXP ASG_PR_JQSRT97.t
 \mv ASG_EXP assignments.t
##
 $SCRD/passx eq_int P2 P0 D3 296 1.D-4 $JPupp_low $PARA CL_T_P2mP0 prec
 \mv normal_eq.t       NQ_T_P2mP0_2
 \mv prediction.t      Pred_P2mP0_2
 \mv prediction_mix.t  Pred_mix_P2mP0_2
 \mv statistics.t      Stat_P2mP0_2
##
 $SCRD/exasg 'P2mP0' $ASG3
 \cp ASG_EXP ASG_Q_JCP92_sca.t
 \mv ASG_EXP assignments.t
##
 $SCRD/passx eq_int P2 P0 D3 296 1.D-4 $JPupp_low $PARA CL_T_P2mP0 prec
 \mv normal_eq.t       NQ_T_P2mP0_3
 \mv prediction.t      Pred_P2mP0_3
 \mv prediction_mix.t  Pred_mix_P2mP0_3
 \mv statistics.t      Stat_P2mP0_3
##
 $SCRD/exasg 'P2mP0' $ASG4
 \cp ASG_EXP ASG_JOSA76_sca.t
 \mv ASG_EXP assignments.t
##
 $SCRD/passx eq_int P2 P0 D3 295 1.D-4 $JPupp_low $PARA CL_T_P2mP0 prec
 \mv normal_eq.t       NQ_T_P2mP0_4
 \mv prediction.t      Pred_P2mP0_4
 \mv prediction_mix.t  Pred_mix_P2mP0_4
 \mv statistics.t      Stat_P2mP0_4
##
 $SCRD/exasg 'P2mP0' $ASG5
 \cp ASG_EXP ASG_JMS79.t
 \mv ASG_EXP assignments.t
##
 $SCRD/passx eq_int P2 P0 D3 294 1.D-4 $JPupp_low $PARA CL_T_P2mP0 prec
 \mv normal_eq.t       NQ_T_P2mP0_5
 \mv prediction.t      Pred_P2mP0_5
 \mv prediction_mix.t  Pred_mix_P2mP0_5
 \mv statistics.t      Stat_P2mP0_5
##
 $SCRD/exasg 'P2mP0' $ASG6
 \cp ASG_EXP ASG_calib.t
 \mv ASG_EXP assignments.t
##
 $SCRD/passx eq_int P2 P0 D3 296 1.D-4 $JPupp_low $PARA CL_T_P2mP0 prec
 \mv normal_eq.t       NQ_T_P2mP0_6
 \mv prediction.t      Pred_P2mP0_6
 \mv prediction_mix.t  Pred_mix_P2mP0_6
 \mv statistics.t      Stat_P2mP0_6
##
## New intensity parameter estimates.
##
 $SCRD/passx paradj T NQ_T_P2mP0_1 $PARA  \
                      NQ_T_P2mP0_2 $PARA  \
                      NQ_T_P2mP0_3 $PARA  \
                      NQ_T_P2mP0_4 $PARA  \
                      NQ_T_P2mP0_5 $PARA  \
                      NQ_T_P2mP0_6 $PARA
 \rm NQ_T_P2mP0_1 NQ_T_P2mP0_2 NQ_T_P2mP0_3 NQ_T_P2mP0_4 NQ_T_P2mP0_5 NQ_T_P2mP0_6
##
 \rm assignments.t
 \rm     TD*
 \rm CL* ED*     VP*
###
### The new parameter files are :
###
###                 NQ_T_P2mP0_*_adj
###

---------------------------------------------------------------------------------------------------------------------------

Description of the command lines to call the different programs:

   - eq_int Pn Pn' Dp TEMP RINMI Jmax para_file control_file [prec] [pol] [MHz|GHz] [mix N] [thres T] [fpvib FPVIB] [abund ABUND]
                                                                    >> Calculates normal equations for the
                                                                       transition Pn-Pn' up to Jmax (upper level),
                                                                       at temperature TEMP, with intensity threshold RINMI,
                                                                       using parameters in para_file with parameter status
                                                                       in control_file.
                                                                       prec option used to output precision on calculated
                                                                       line intensities.
                                                                       pol option in case of Raman.
                                                                       MHz|GHz option to specify frequency unit
                                                                       (default is cm-1).
                                                                       mix option to specify the number of greatest
                                                                       vibrational components of the eigenbasis
                                                                       decomposition on the initial basis set.
                                                                       thres option to eliminate from fit the calculated
                                                                       intensities whose relative deviation from
                                                                       experiment is bigger than T.
                                                                       Vibrational partition function value FPVIB can be
                                                                       specified through fpvib option.
                                                                       Isotopic abundance ABUND can be specified through
                                                                       abund option.

  2c) frequency and/or intensity parameter fitting using xpafit:
  --------------------------------------------------------------

    We give below some explanations concerning the example of Dyad of 12CH4 (STDS).

------------------------------ File: STDS/jobs/fit_examples/job_xfit_12CH4_Dyad -------------------------------------------

#! /bin/sh
 set -v
##
## Frequency and Intensity parameter fitting
## P0mP0 P1mP0 P1mP1 of 12CH4.
##
BASD="/.../.../STDS"
##
 SCRD=$BASD/prog/exe
 EXPD=$BASD/exp/12CH4
 CTRD=$BASD/ctrp/12CH4
 PARD=$BASD/para/12CH4
##
## Jmax values.
##
 JP0=25
 JP1=24
##
## Assignment files.
##
 ASG_1_P0mP0_1=$EXPD/dip/ASG_freq_Dyad
 ASG_2_P1mP0_1=$EXPD/dip/ASG_freq_Dyad
 ASG_2_P1mP0_2=$EXPD/dip/ASG_int_Dyad_GS
 ASG_3_P1mP1_1=$EXPD/dip/ASG_freq_Dyad
##
## Parameter constraint file.
##
 CLF=$CTRD/CL_Dyad
##
#################################################
##
## Fit.
##
## 1_P0mP0
##
 $SCRD/xasg 'CH4 P0mP0' "$ASG_1_P0mP0_1" P0 P0 GHz                  >> Extracts assignment lines containing the specified
                                                                       string.
 \mv ASG_EXP ASG_1_P0mP0_1.t
##
##
## 2_P1mP0
##
 $SCRD/xasg 'CH4 P1mP0' "$ASG_2_P1mP0_1" P1 P0
 \mv ASG_EXP ASG_2_P1mP0_1.t
##
 $SCRD/xasg 'CH4 P1mP0' "$ASG_2_P1mP0_2" P1 P0     296.0
 \mv ASG_EXP ASG_2_P1mP0_2.t
##
##
## 3_P1mP1
##
 $SCRD/xasg 'CH4 P1mP1' "$ASG_3_P1mP1_1" P1 P1 GHz
 \mv ASG_EXP ASG_3_P1mP1_1.t
##
 $SCRD/passx xtrias ASG_1_P0mP0_1.t \                               >> Sorts assignments.
                    ASG_2_P1mP0_1.t \
                    ASG_2_P1mP0_2.t \
                    ASG_3_P1mP1_1.t
 \cp ASG_EXP ASG_P0mP0_P1mP0_P1mP1.t
 \mv ASG_EXP assignments.t
##
 $SCRD/passx xpafit polyad P0 N1  D6 $JP0 \                         >> Fits the parameters.
                    polyad P1 N2  D66 $JP1 \
                    trans  P0mP0 \
                    trans  P1mP0 $PARD/Pa_DYADmGS D2 \
                    trans  P1mP1 \
                    end    $CLF
##
 \rm XTRM_* XTRO_* XVP_*
 \rm XED_*  XEN_*

---------------------------------------------------------------------------------------------------------------------------

Description of the command lines to call the different programs:

   - xasg 'string'  'files'              Pup Plo [MHz|GHz] [TEMP [pol [Rxxx]] [fpvib FPVIB] [abund ABUND]]
                                                                    >> Extracts from 'files' the lines containing 'string'.
                                                                       Pup Plo: upper and lower polyads.
                                                                       MHz|GHz option to specify frequency unit
                                                                       (default is cm-1).
                                                                       TEMP: temperature.
                                                                       pol option  in case of Raman (default polarization
                                                                       state: R111).
                                                                       If pol is specified,
                                                                       pol_state=  R111   |   R110   |   R001
                                                                       means      without | parallel | perpendicular
                                                                       Vibrational partition function value FPVIB can be
                                                                       specified through fpvib option.
                                                                       Isotopic abundance ABUND can be specified through
                                                                       abund option.

   - xtrias INFILE(S)                                               >> Sorts assignments in quantic number increasing order
                                                                       for lower then upper polyad:
                                                                       Pinf Psup JCsup JCinf Nsup Ninf.

   - xpafit polyad Pi Nk Dmm'...m'' JPi                \            >> Hamiltonian and/or transition parameter fit
            ...... ....................                                using Levenberg-Marquardt method
            polyad Pj Nl Dnn'...n'' JPj                \               thanks to MINPACK Project implementation.
            trans  P*mP* [para_file [Dp [pol [Rxxx]]]] \
            .....  .....
            trans  P*mP* [para_file [Dp [pol [Rxxx]]]] \
           [adj    P*mP*  para_file     [pol]          \]
            .....  .....
           [adj    P*mP*  para_file     [pol]          \]
            end    para_control [mix N] [rmxomc X] [mxiter M]


     TAGS:
     -----
     polyad : defines a polyad Pi, with k levels, mm'...m''orders of development and maximum J JPi.
     trans  : defines a transition between two polyads.
              Assignments of this transition are taken into account in the fit.
              If para_file is specified, parameters in this file are fitted.
                When fitting Hamiltonian parameters only, there must be only one parameter file
                defined through the trans tags, without any Dp option.
                When fitting transition moment parameters (with or without Hamiltonian parameters)
                there may be multiple parameter files defined,
                each with a mandatory development order Dp option.
              pol option in case of Raman (default polarization state: R111).
              If pol is specified,  R111|R110|R001 means respectively:  without|parallel|perpendicular.
     adj    : defines some additional parameter files to be updated.
              The concerned parameters should have been fitted using parameter files defined through trans tags.
                Transition type (P*mP* para_file pol) defined through trans and adj tags has to be unique.
     end    : allows to define the last parameters
              para_control is the parameter status file.
              Cf. end of 2a section for constraint type definitions. With xpafit constraint type 3 is ignored.
              mix option to specify the number of greatest vibrational components
              of the eigenbasis decomposition on the initial basis set.
              rmxomc option to eliminate from fit the calculated intensities whose relative deviation
              from experiment is bigger than X.
              mxiter to specify the maximum number of iterations.
                xpafit can also be cleanly stopped while running if the user creates a 'xpafit_BREAKNOW' file
                in the working directory. xpafit deletes this file before fit beginning.


3) Simulation of synthetic spectra:
-----------------------------------

In the same way, jobs can be created in order to simulate synthetic spectra under
various physical conditions.

   - simul trans|raman|absor UMIN UMAX T RES PAF THRES        \     >> Outputs the simulated spectra in simul.xy
                LINEFILE1 CDAE RM UK PTTC [PPTC CL] [MHz|GHz] \        with supplementary informations in simul.log.
           more LINEFILE2 CDAE RM UK PTTC [PPTC CL] [MHz|GHz] \
           ..................................................
           end  APPF [MHz|GHz]

where the parameters have the following meaning:

trans|raman|absor for transmission, Raman or absorbance spectra.
UMIN      : Minimum wavenumber.
UMAX      : Maximum wavenumber.
T         : Temperature in K.
RES       : Resolution in cm-1.
PAF       : Drawing step in cm-1.
THRES     : Intensity threshold in cm-2.atm-1.
LINEFILE1 : Stick spectrum file, typically spectr.xy (output of spectrum calculation job).
CDAE      : Collisional broadening in cm-1.atm-1.
RM        : Molecular mass in atomic mass units.
UK        : Intensity correction coefficient (dimentionless).
PTTC      : Total pressure in torrs.
PPTC      : Partial pressure in torrs.
CL        : Optical path length in cm.
APPF      : Apparatus function (dirac, sinc, sinc2, or gauss).

Line position default unit is cm-1.
It can be changed to MHz or GHz for input files through each line file definition.
It can be changed to MHz or GHz for ouput files as last option.
Output unit is used for UMIN, UMAX, RES and PAF.

