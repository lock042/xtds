
! *** DIMENSIONS, PARITES ET DESIGNATIONS DES REPRESENTATIONS DE TD

      module mod_com_sy
      use mod_dppr
      use mod_par_tds
      IMPLICIT NONE

      real(kind=dppr)      ,dimension(MXSYM) ,parameter  :: DC  = (/  1.D0,  1.D0,  2.D0,  3.D0,  3.D0 /)
      real(kind=dppr)      ,dimension(MXSYM) ,parameter  :: PC  = (/  1.D0, -1.D0,  1.D0, -1.D0,  1.D0 /)

      character(len =   1) ,dimension(2)     ,parameter  :: PAR = (/ 'g' , 'u'                    /)
      character(len =   2) ,dimension(MXSYM) ,parameter  :: SYM = (/ 'A1', 'A2', 'E ', 'F1', 'F2' /)

      end module mod_com_sy
