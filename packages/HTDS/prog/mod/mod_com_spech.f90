
      module mod_com_spech

      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      integer              ,pointer ,dimension(:,:) ,save   :: IVIBI,IVIBS                         ! (MXSNV,NBVQN)
      integer              ,pointer ,dimension(:)   ,save   :: IVIBNI,IVIBNS                       ! (MXSNV)

      character(len =   1) ,pointer ,dimension(:)   ,save   :: PARIC,PARSC                         ! (MXSNV)
      character(len =   2) ,pointer ,dimension(:)   ,save   :: VIBIC,VIBSC                         ! (MXSNV)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_SPECH
      IMPLICIT NONE

      integer  :: ierr

      allocate(IVIBI(MXSNV,NBVQN),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_SPECH_IVIBI')
      IVIBI = 0
      allocate(IVIBS(MXSNV,NBVQN),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_SPECH_IVIBS')
      IVIBS = 0
      allocate(IVIBNI(MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_SPECH_IVIBNI')
      IVIBNI = 0
      allocate(IVIBNS(MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_SPECH_IVIBNS')
      IVIBNS = 0
      allocate(PARIC(MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_SPECH_PARIC')
      PARIC = ''
      allocate(PARSC(MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_SPECH_PARSC')
      PARSC = ''
      allocate(VIBIC(MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_SPECH_VIBIC')
      VIBIC = ''
      allocate(VIBSC(MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_SPECH_VIBSC')
      VIBSC = ''
!
      return
      end subroutine ALLOC_SPECH

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXSNV

      subroutine RESIZE_MXSNV_SPECH(C_SNV)
      IMPLICIT NONE
      integer :: C_SNV

      integer  :: i,ierr

      character(len =   1) ,pointer ,dimension(:)  :: cpd1_1
      character(len =   2) ,pointer ,dimension(:)  :: cpd1_2

! IVIBI
      allocate(ipd2(C_SNV,NBVQN),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_SPECH_IVIBI')
      ipd2 = 0
      do i=1,NBVQN
        ipd2(1:MXSNV,i) = IVIBI(:,i)
      enddo
      deallocate(IVIBI)
      IVIBI => ipd2
! IVIBS
      allocate(ipd2(C_SNV,NBVQN),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_SPECH_IVIBS')
      ipd2 = 0
      do i=1,NBVQN
        ipd2(1:MXSNV,i) = IVIBS(:,i)
      enddo
      deallocate(IVIBS)
      IVIBS => ipd2
! IVIBNI
      allocate(ipd1(C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_SPECH_IVIBNI')
      ipd1 = 0
      ipd1(1:MXSNV) = IVIBNI(:)
      deallocate(IVIBNI)
      IVIBNI => ipd1
! IVIBNS
      allocate(ipd1(C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_SPECH_IVIBNS')
      ipd1 = 0
      ipd1(1:MXSNV) = IVIBNS(:)
      deallocate(IVIBNS)
      IVIBNS => ipd1
! PARIC
      allocate(cpd1_1(C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_SPECH_PARIC')
      cpd1_1 = ''
      cpd1_1(1:MXSNV) = PARIC(:)
      deallocate(PARIC)
      PARIC => cpd1_1
! PARSC
      allocate(cpd1_1(C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_SPECH_PARSC')
      cpd1_1 = ''
      cpd1_1(1:MXSNV) = PARSC(:)
      deallocate(PARSC)
      PARSC => cpd1_1
! VIBIC
      allocate(cpd1_2(C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_SPECH_VIBIC')
      cpd1_2 = ''
      cpd1_2(1:MXSNV) = VIBIC(:)
      deallocate(VIBIC)
      VIBIC => cpd1_2
! VIBSC
      allocate(cpd1_2(C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_SPECH_VIBSC')
      cpd1_2 = ''
      cpd1_2(1:MXSNV) = VIBSC(:)
      deallocate(VIBSC)
      VIBSC => cpd1_2
!
      return
      end subroutine RESIZE_MXSNV_SPECH

      end module mod_com_spech
