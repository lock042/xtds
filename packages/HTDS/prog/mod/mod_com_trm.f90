
      module mod_com_trm

      use mod_dppr
      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      integer ,parameter  :: MDMJCI = 10*MXSYM

      integer ,dimension(MDMJCI) ,save  :: JDIM  = 0
      integer                    ,save  :: ICKRO = 0
      integer                    ,save  :: ICLRO = 0
!
      real(kind=dppr) ,pointer ,dimension(:,:,:,:) ,save  :: DIP                                   ! (MXDIMS,MXDIMI,MXBRA,MXOPT)
      real(kind=dppr) ,pointer ,dimension(:)       ,save  :: HTR                                   ! (MXELMT)
      real(kind=dppr) ,pointer ,dimension(:)       ,save  :: HDK                                   ! (MXDIMS)
      real(kind=dppr) ,pointer ,dimension(:)       ,save  :: HDL                                   ! (MXDIMI)
      real(kind=dppr) ,pointer ,dimension(:,:)     ,save  :: HDLJ                                  ! (MXDIMI,MDMJCI)
      real(kind=dppr) ,pointer ,dimension(:,:)     ,save  :: TK                                    ! (MXDIMS,MXDIMS)
      real(kind=dppr) ,pointer ,dimension(:,:)     ,save  :: TL                                    ! (MXDIMI,MXDIMI)
      real(kind=dppr) ,pointer ,dimension(:,:,:)   ,save  :: TLJ                                   ! (MXDIMI,MXDIMI,MDMJCI)

      integer         ,pointer ,dimension(:)       ,save  :: ICOEFI                                ! (MXOPT)
      integer         ,pointer ,dimension(:)       ,save  :: KOTR,LITR                             ! (MXELMT)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_TRM
      IMPLICIT NONE

      integer  :: ierr

      allocate(DIP(MXDIMS,MXDIMI,MXBRA,MXOPT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_TRM_DIP')
      DIP = 0.d0
      allocate(HTR(MXELMT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_TRM_HTR')
      HTR = 0.d0
      allocate(HDK(MXDIMS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_TRM_HDK')
      HDK = 0.d0
      allocate(HDL(MXDIMI),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_TRM_HDL')
      HDL = 0.d0
      allocate(HDLJ(MXDIMI,MDMJCI),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_TRM_HDLJ')
      HDLJ = 0.d0
      allocate(TK(MXDIMS,MXDIMS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_TRM_TK')
      TK = 0.d0
      allocate(TL(MXDIMI,MXDIMI),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_TRM_TL')
      TL = 0.d0
      allocate(TLJ(MXDIMI,MXDIMI,MDMJCI),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_TRM_TLJ')
      TLJ = 0.d0

      allocate(ICOEFI(MXOPT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_TRM_ICOEFI')
      ICOEFI = 0
      allocate(KOTR(MXELMT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_TRM_KOTR')
      KOTR = 0
      allocate(LITR(MXELMT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_TRM_LITR')
      LITR = 0
!
      return
      end subroutine ALLOC_TRM

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXDIMI

      subroutine RESIZE_MXDIMI_TRM(C_DIMI)
      IMPLICIT NONE
      integer :: C_DIMI

      integer :: i,ierr
      integer :: j
      integer :: k

! DIP
      allocate(rpd4(MXDIMS,C_DIMI,MXBRA,MXOPT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXDIMI_TRM_DIP')
      rpd4 = 0.d0
      do k=1,MXOPT
        do j=1,MXBRA
          do i=1,MXDIMS
            rpd4(i,1:MXDIMI,j,k) = DIP(i,:,j,k)
          enddo
        enddo
      enddo
      deallocate(DIP)
      DIP => rpd4
! HDL
      allocate(rpd1(C_DIMI),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXDIMI_TRM_HDL')
      rpd1 = 0.d0
      rpd1(1:MXDIMI) = HDL(:)
      deallocate(HDL)
      HDL => rpd1
! HDLJ
      allocate(rpd2(C_DIMI,MDMJCI),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXDIMI_TRM_HDLJ')
      rpd2 = 0.d0
      do i=1,MDMJCI
        rpd2(1:MXDIMI,i) = HDLJ(:,i)
      enddo
      deallocate(HDLJ)
      HDLJ => rpd2
! TL
      allocate(rpd2(C_DIMI,C_DIMI),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXDIMI_TRM_TL')
      rpd2 = 0.d0
      do i=1,MXDIMI
        rpd2(1:MXDIMI,i) = TL(:,i)
      enddo
      deallocate(TL)
      TL => rpd2
! TLJ
      allocate(rpd3(C_DIMI,C_DIMI,MDMJCI),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXDIMI_TRM_TLJ')
      rpd3 = 0.d0
      do j=1,MDMJCI
        do i=1,MXDIMI
          rpd3(1:MXDIMI,i,j) = TLJ(:,i,j)
        enddo
      enddo
      deallocate(TLJ)
      TLJ => rpd3
!
      return
      end subroutine RESIZE_MXDIMI_TRM

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXDIMS

      subroutine RESIZE_MXDIMS_TRM(C_DIMS)
      IMPLICIT NONE
      integer :: C_DIMS

      integer :: i,ierr
      integer :: j
      integer :: k

! DIP
      allocate(rpd4(C_DIMS,MXDIMI,MXBRA,MXOPT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXDIMS_TRM_DIP')
      rpd4 = 0.d0
      do k=1,MXOPT
        do j=1,MXBRA
          do i=1,MXDIMI
            rpd4(1:MXDIMS,i,j,k) = DIP(:,i,j,k)
          enddo
        enddo
      enddo
      deallocate(DIP)
      DIP => rpd4
! HDK
      allocate(rpd1(C_DIMS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXDIMS_TRM_HDK')
      rpd1 = 0.d0
      rpd1(1:MXDIMS) = HDK(:)
      deallocate(HDK)
      HDK => rpd1
! TK
      allocate(rpd2(C_DIMS,C_DIMS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXDIMS_TRM_TK')
      rpd2 = 0.d0
      do i=1,MXDIMS
        rpd2(1:MXDIMS,i) = TK(:,i)
      enddo
      deallocate(TK)
      TK => rpd2
!
      return
      end subroutine RESIZE_MXDIMS_TRM

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXELMT

      subroutine RESIZE_MXELMT_TRM(C_ELMT)
      IMPLICIT NONE
      integer :: C_ELMT

      integer :: ierr

! HTR
      allocate(rpd1(C_ELMT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXELMT_TRM_HTR')
      rpd1 = 0.d0
      rpd1(1:MXELMT) = HTR(:)
      deallocate(HTR)
      HTR => rpd1
! KOTR
      allocate(ipd1(C_ELMT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXELMT_TRM_KOTR')
      ipd1 = 0
      ipd1(1:MXELMT) = KOTR(:)
      deallocate(KOTR)
      KOTR => ipd1
! LITR
      allocate(ipd1(C_ELMT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXELMT_TRM_LITR')
      ipd1 = 0
      ipd1(1:MXELMT) = LITR(:)
      deallocate(LITR)
      LITR => ipd1
!
      return
      end subroutine RESIZE_MXELMT_TRM

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOPT

      subroutine RESIZE_MXOPT_TRM(C_OPT)
      IMPLICIT NONE
      integer :: C_OPT

      integer :: i,ierr
      integer :: j
      integer :: k

! DIP
      allocate(rpd4(MXDIMS,MXDIMI,MXBRA,C_OPT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPT_TRM_DIP')
      rpd4 = 0.d0
      do k=1,MXBRA
        do j=1,MXDIMI
          do i=1,MXDIMS
            rpd4(i,j,k,1:MXOPT) = DIP(i,j,k,:)
          enddo
        enddo
      enddo
      deallocate(DIP)
      DIP => rpd4

! ICOEFI
      allocate(ipd1(C_OPT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPT_TRM_ICOEFI')
      ipd1 = 0
      ipd1(1:MXOPT) = ICOEFI(:)
      deallocate(ICOEFI)
      ICOEFI => ipd1
!
      return
      end subroutine RESIZE_MXOPT_TRM

      end module mod_com_trm
