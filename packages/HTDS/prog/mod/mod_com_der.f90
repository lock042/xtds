
      module mod_com_der

      use mod_dppr
      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      real(kind=dppr) ,pointer ,dimension(:,:) ,save  :: DERINF,DERSUP                             ! (MXOBS,MXOPH)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_DER
      IMPLICIT NONE

      integer  :: ierr

      allocate(DERINF(MXOBS,MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DER_DERINF')
      DERINF = 0.d0
      allocate(DERSUP(MXOBS,MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DER_DERSUP')
      DERSUP = 0.d0
!
      return
      end subroutine ALLOC_DER

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOBS

      subroutine RESIZE_MXOBS_DER(C_OBS)
      IMPLICIT NONE
      integer :: C_OBS

      integer :: i,ierr

! DERINF
      allocate(rpd2(C_OBS,MXOPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_DER_DERINF')
      rpd2 = 0.d0
      do i=1,MXOPH
        rpd2(1:MXOBS,i) = DERINF(:,i)
      enddo
      deallocate(DERINF)
      DERINF => rpd2
! DERSUP
      allocate(rpd2(C_OBS,MXOPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_DER_DERSUP')
      rpd2 = 0.d0
      do i=1,MXOPH
        rpd2(1:MXOBS,i) = DERSUP(:,i)
      enddo
      deallocate(DERSUP)
      DERSUP => rpd2
!
      return
      end subroutine RESIZE_MXOBS_DER

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOPH

      subroutine RESIZE_MXOPH_DER(C_OPH)
      IMPLICIT NONE
      integer :: C_OPH

      integer :: i,ierr

! DERINF
      allocate(rpd2(MXOBS,C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_DER_DERINF')
      rpd2 = 0.d0
      do i=1,MXOBS
        rpd2(i,1:MXOPH) = DERINF(i,:)
      enddo
      deallocate(DERINF)
      DERINF => rpd2
! DERSUP
      allocate(rpd2(MXOBS,C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_DER_DERSUP')
      rpd2 = 0.d0
      do i=1,MXOBS
        rpd2(i,1:MXOPH) = DERSUP(i,:)
      enddo
      deallocate(DERSUP)
      DERSUP => rpd2
!
      return
      end subroutine RESIZE_MXOPH_DER

      end module mod_com_der
