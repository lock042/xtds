
      module mod_com_precf

      use mod_dppr
      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      integer              ,pointer ,dimension(:)   ,save  :: JI,JS                                ! (MXOBS)
      integer              ,pointer ,dimension(:)   ,save  :: NI,NS                                ! (MXOBS)
      integer              ,pointer ,dimension(:)   ,save  :: IXCL                                 ! (MXOPH) ; index des operateurs fittes

      character(len =   1) ,pointer ,dimension(:)   ,save  :: PARI,PARS                            ! (MXOBS)
      character(len =   2) ,pointer ,dimension(:)   ,save  :: SYI,SYS                              ! (MXOBS)

      logical              ,pointer ,dimension(:)   ,save  :: FOUND                                ! (MXOBS)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_PRECF
      IMPLICIT NONE

      integer  :: ierr

      allocate(JI(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PRECF_JI')
      JI = 0
      allocate(JS(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PRECF_JS')
      JS = 0
      allocate(NI(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PRECF_NI')
      NI = 0
      allocate(NS(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PRECF_NS')
      NS = 0
      allocate(IXCL(MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PRECF_IXCL')
      IXCL = 0

      allocate(PARI(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PRECF_PARI')
      PARI = ''
      allocate(PARS(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PRECF_PARS')
      PARS = ''
      allocate(SYI(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PRECF_SYI')
      SYI = ''
      allocate(SYS(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PRECF_SYS')
      SYS = ''

      allocate(FOUND(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_PRECF_FOUND')
      FOUND = .false.
!
      return
      end subroutine ALLOC_PRECF

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOBS

      subroutine RESIZE_MXOBS_PRECF(C_OBS)
      IMPLICIT NONE
      integer :: C_OBS

      integer :: ierr

      character(len =   1) ,pointer ,dimension(:)  :: cpd1_1
      character(len =   2) ,pointer ,dimension(:)  :: cpd1_2

! JI
      allocate(ipd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_PRECF_JI')
      ipd1 = 0
      ipd1(1:MXOBS) = JI(:)
      deallocate(JI)
      JI => ipd1
! JS
      allocate(ipd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_PRECF_JS')
      ipd1 = 0
      ipd1(1:MXOBS) = JS(:)
      deallocate(JS)
      JS => ipd1
! NI
      allocate(ipd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_PRECF_NI')
      ipd1 = 0
      ipd1(1:MXOBS) = NI(:)
      deallocate(NI)
      NI => ipd1
! NS
      allocate(ipd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_PRECF_NS')
      ipd1 = 0
      ipd1(1:MXOBS) = NS(:)
      deallocate(NS)
      NS => ipd1

! PARI
      allocate(cpd1_1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_PRECF_PARI')
      cpd1_1 = ''
      cpd1_1(1:MXOBS) = PARI(:)
      deallocate(PARI)
      PARI => cpd1_1
! PARS
      allocate(cpd1_1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_PRECF_PARS')
      cpd1_1 = ''
      cpd1_1(1:MXOBS) = PARS(:)
      deallocate(PARS)
      PARS => cpd1_1
! SYI
      allocate(cpd1_2(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_PRECF_SYI')
      cpd1_2 = ''
      cpd1_2(1:MXOBS) = SYI(:)
      deallocate(SYI)
      SYI => cpd1_2
! SYS
      allocate(cpd1_2(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_PRECF_SYS')
      cpd1_2 = ''
      cpd1_2(1:MXOBS) = SYS(:)
      deallocate(SYS)
      SYS => cpd1_2

! FOUND
      allocate(lpd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_PRECF_FOUND')
      lpd1 = .false.
      lpd1(1:MXOBS) = FOUND(:)
      deallocate(FOUND)
      FOUND => lpd1
!
      return
      end subroutine RESIZE_MXOBS_PRECF

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOPH

      subroutine RESIZE_MXOPH_PRECF(C_OPH)
      IMPLICIT NONE
      integer :: C_OPH

      integer :: ierr

! IXCL
      allocate(ipd1(C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_PRECF_IXCL')
      ipd1 = 0
      ipd1(1:MXOPH) = IXCL(:)
      deallocate(IXCL)
      IXCL => ipd1
!
      return
      end subroutine RESIZE_MXOPH_PRECF

      end module mod_com_precf
