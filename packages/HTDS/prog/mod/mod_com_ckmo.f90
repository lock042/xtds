
      module mod_com_ckmo

      use mod_dppr
      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      integer ,save  :: NBKCO = 0
      integer ,save  :: NBKLI = 0
!
      real(kind=dppr) ,pointer ,dimension(:,:) ,save  :: VK                                        ! (MXKLI,MXKCO)

      integer         ,pointer ,dimension(:)   ,save  :: IKINIC                                    ! (MXKCO)
      integer         ,pointer ,dimension(:)   ,save  :: IKJUG                                     ! (MXKLI)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_CKMO
      use mod_alloc
      IMPLICIT NONE

      integer  :: ierr

      allocate(VK(MXKLI,MXKCO),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_CKMO_VK')
      VK = 0.d0

      allocate(IKINIC(MXKCO),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_CKMO_IKINIC')
      IKINIC = 0
      allocate(IKJUG(MXKLI),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_CKMO_IKJUG')
      IKJUG = 0
!
      return
      end subroutine ALLOC_CKMO

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXKCO

      subroutine RESIZE_MXKCO_CKMO(C_KCO)
      IMPLICIT NONE
      integer :: C_KCO

      integer :: i,ierr

! VK
      allocate(rpd2(MXKLI,C_KCO),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXKCO_CKMO_VK')
      rpd2 = 0.d0
      do i=1,MXKCO
        rpd2(1:MXKLI,i) = VK(:,i)
      enddo
      deallocate(VK)
      VK => rpd2
! IKINIC
      allocate(ipd1(C_KCO),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXKCO_CKMO_IKINIC')
      ipd1 = 0
      ipd1(1:MXKCO) = IKINIC(:)
      deallocate(IKINIC)
      IKINIC => ipd1
!
      return
      end subroutine RESIZE_MXKCO_CKMO

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXKLI

      subroutine RESIZE_MXKLI_CKMO(C_KLI)
      IMPLICIT NONE
      integer :: C_KLI

      integer :: i,ierr

! VK
      allocate(rpd2(C_KLI,MXKCO),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXKLI_CKMO_VK')
      rpd2 = 0.d0
      do i=1,MXKCO
        rpd2(1:MXKLI,i) = VK(:,i)
      enddo
      deallocate(VK)
      VK => rpd2
! IKJUG
      allocate(ipd1(C_KLI),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXKLI_CKMO_IKJUG')
      ipd1 = 0
      ipd1(1:MXKLI) = IKJUG(:)
      deallocate(IKJUG)
      IKJUG => ipd1
!
      return
      end subroutine RESIZE_MXKLI_CKMO

      end module mod_com_ckmo
