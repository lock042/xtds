      PROGRAM CTRPMK
!
!  CREER UN FICHIER DE CONTROLE DE PARAMETRES (INTENSITES OU FREQUENCES)
!  A PARTIR DU FICHIER DE PARAMETRES
!
!  MAR 2005 CW VB
!
! APPEL : ctrpmk ParaFileName CtrpFileName int|freq
!
!
      use mod_par_tds
      use mod_com_fdate
      IMPLICIT NONE

      integer          :: I
      integer          :: NBOP,NCL1

      character(len = NBCTIT)  :: TITRE
      character(len =   4)  :: OPT
      character(len = 120)  :: FCTRP,FPARA

      logical               :: FREQ = .FALSE.                                                      ! INTENSITES PAR DEFAUT
      logical               :: REP
!
1000  FORMAT(A)
1001  FORMAT(////,   &
             I4,//)
1002  FORMAT(////,   &
             I4,/ )
1003  FORMAT(///,    &
             A ,/,   &
             I4,/ )
1005  FORMAT(A, 7X,'Hmn  Frdm         Attac/cm-1  St.Dev./cm-1')
1006  FORMAT(A,5X,'1',1X,E18.11,E14.7)
1010  FORMAT(/,              &
             'CTRPMK : ',A)
2000  FORMAT(' <  ',A)
2001  FORMAT(' >> ',A)
8000  FORMAT(' !!! CTRPMK : STOP ON ERROR')
8001  FORMAT(' !!! ERROR OPENING CONTROL FILE')
8002  FORMAT(' !!! INCOMPLETE CONTROL FILE')
8003  FORMAT(' !!! ERROR OPENING PARAMETER FILE')
8004  FORMAT(' !!! PARAMETER CONTROL FILE SHOULD NOT EXIST')
8005  FORMAT(' !!! ERROR OPENING PARAMETER CONTROL FILE')
8006  FORMAT(' !!! NEITHER int NOR freq')
8007  FORMAT(' !!! ERROR READING PARAMETER FILE')
8008  FORMAT(' !!! ERROR WRITING PARAMETER CONTROL FILE')
!
      NCL1 = NBCLAB+1+NBAM+1                                                                       ! Hmn included
      OPEN(10,FILE=CTRLF,ERR=9998,STATUS='OLD')
      READ(10,1000,END=9997) FDATE
      PRINT 1010,            FDATE
      READ(10,1000,END=9997) FPARA
      READ(10,1000,END=9997) FCTRP
      READ(10,1000,END=9997) OPT
      CLOSE(10)
      PRINT 2000,  FPARA
      OPEN(11,FILE=FPARA,ERR=9996,STATUS='OLD')
      INQUIRE(FILE=FCTRP,EXIST=REP)
      IF( REP ) THEN
        PRINT 8004
        GOTO  9999
      ENDIF
      PRINT 2001,  FCTRP
      OPEN(12,FILE=FCTRP,ERR=9995,STATUS='NEW')
      IF    ( OPT .EQ. 'freq' ) THEN
        FREQ = .TRUE.
      ELSEIF( OPT .NE. 'int'  ) THEN
        PRINT 8006
        GOTO  9999
      ENDIF
!
! PASSER LES FREQUENCES SI INTENSITES
!
      IF( .NOT. FREQ ) THEN
        READ(11,1001,ERR=9994) NBOP
        DO I=1,NBOP
          READ(11,1000,ERR=9994)
        ENDDO
      ENDIF
!
!  LIRE ET COPIER
!
      IF( FREQ ) THEN
        READ (11,1002,ERR=9994) NBOP
        WRITE(12,1002,ERR=9993) NBOP
      ELSE
        READ (11,1003,ERR=9994) TITRE      ,NBOP
        WRITE(12,1003,ERR=9993) TRIM(TITRE),NBOP
      ENDIF
      READ(11,1000,ERR=9994)
      WRITE(12,1005,ERR=9993) FDATE
      DO I=1,NBOP
        READ (11,1000,ERR=9994) TITRE(:NCL1)
        WRITE(12,1006,ERR=9993) TITRE(:NCL1),0.D0,0.D0
      ENDDO
      CLOSE(12)
      CLOSE(11)
      GOTO 9000
!
9993  PRINT 8008
      GOTO  9999
9994  PRINT 8007
      GOTO  9999
9995  PRINT 8005
      GOTO  9999
9996  PRINT 8003
      GOTO  9999
9997  PRINT 8002
      CLOSE(10)
      GOTO  9999
9998  PRINT 8001
9999  PRINT 8000
!
9000  PRINT *
      END PROGRAM CTRPMK
