      PROGRAM EQINT
!
!  21.03.89 FORTRAN 77 POUR SUN4 MODIFIE LE 12.10.89  J.M.JOUVARD
!  REV 9 FEVRIER 90  J.C.HILICO  J.P.CHAMPION
!  REV 19 OCTOBRE 99 J.P. CHAMPION
!  REV          VB : XY6/Oh
!  REV JAN 2003 VB : EQ. NOMRM. QUE POUR PARA. FITES !!!
!
! ####################################################################
! ##                                                                ##
! ## - CALCUL DU SPECTRE THEORIQUE (FREQUENCE ET INTENSITE)         ##
! ## - COMPARAISON AVEC LES RAIES EXPERIMENTALES                    ##
! ## - CALCUL DES EQUATIONS NORMALES   (INTENSITE)                  ##
! ## - STATISTIQUES                    (INTENSITE)                  ##
! ## - OPTION % NIVEAUX                                             ##
! ##                                                                ##
! ####################################################################
!
! APPEL : eq_int Pn Pn' Dp TEMP RINMI JMAX FPARA CLx [prec] [pol] [MHz/GHz] [mix N] [para FICHIER] [thres T] [fpvib FPVIB] [abund ABUND]
! LES OPTIONS PEUVENT ETRE INDIQUEES DANS UN ORDRE QUELCONQUE .
!
! *** LIMITATIONS DU PROGRAMME
!
! NOMBRE MAXIMUM D'OPERATEURS DE L'HAMILTONIEN
!     MXOPH                ! DERSUP, DERINF
!
! NOMBRE MAXIMUM D'OPERATEURS DU MOMENT DE TRANSITION
!     MXOPT                ! DERT,DERS
!
! NOMBRE MAXIMUM DE NIVEAUX D'ENERGIE J,C,N
!     MXENI                ! JC,ENINF,DERINF
!
! NOMBRE MAXIMUM DE SYMETRIES ROVIBRATIONNELLES
!     MXSYM                ! SYM,SPIN
!
! NOMBRE MAXIMUM DE BRANCHES
!     MXBRA                ! BRANCH
!
!
      use mod_dppr
      use mod_par_tds
      use mod_com_branch
      use mod_com_const
      use mod_com_fdate
      use mod_com_fp
      use mod_com_spin
      use mod_com_sy
      use mod_main_eqint
      IMPLICIT NONE

      real(kind=dppr) ,dimension(0:MXJ)  :: TNEGL
      real(kind=dppr)  :: A2
      real(kind=dppr)  :: CMGHZ,CODER,COEF,COL
      real(kind=dppr)  :: DDT,DIFF,DIFS,DIFSA
      real(kind=dppr)  :: ECTPJ,ECUM,ENINFC,ENSUP
      real(kind=dppr)  :: FCAL,FCAO,FEXP,FOBS
      real(kind=dppr)  :: HT
      real(kind=dppr)  :: PF1,PF2,PNEG,POIDIF,POIDIR,POIDS
      real(kind=dppr)  :: RINMI
      real(kind=dppr)  :: SCAL,SD2,SDFCAL,SDFEXP,SDFOBS,SDFMOD
      real(kind=dppr)  :: SDFMOO,SDSCAL,SDSEXA,SDSEXP,SDSMOD,SDSOBS
      real(kind=dppr)  :: SEXP,SOBS,SS0,SS1
      real(kind=dppr)  :: TEMP,THRES,TM
      real(kind=dppr)  :: VACOII,VACOIJ,VV
      real(kind=dppr)  :: W0

      integer          :: I,IBS,IC,ICI,ICIL,ICINF,ICNTCL,ICNTRL
      integer          :: ICPI,ICPIL,ICPINF,ICPS,ICPSUP
      integer          :: ICOEFU,ICS,ICSUP,II,III,IM,IMEGA,IN,INF,INFF
      integer          :: IPCINF,IOP,IOPT,IP,IPCBN,IRA,IS,ISV
      integer          :: J,JI,JIC,JIICI,JIL,JINF,JJ,JMAX,JOBS,JOBS0
      integer          :: JOP,JOPT,JS,JSUP
      integer          :: K,KCENT
      integer          :: LPC,LPCI,LPCMAX
      integer          :: N,NBINF,NBOM,NBOPC,NBOPHI,NBOPHS,NBOTR,NCL2
      integer          :: NFBI,NFBS,NFBSUP,NFIT,NINF,NOBS,NOBS0
      integer          :: NSTAR,NSUP,NSVI,NSVINF,NSVS,NSVSUP,NTM
      integer          :: NTOTIN,NUNS,NUS,NUSFIN
      integer          :: MVAR

      character(len = NBCLAB+10)  :: COPAJ
      character(len = NBCTIT)     :: TITRE
      character(len =  11) ,dimension(6)  :: CARG
      character(len =   1)  :: FAS,FASO,ISP,PAOBS,PAOBS0,SAS,SASO
      character(len =   1)  :: PAINF,PASUP
      character(len =   2)  :: BR
      character(len =   2)  :: SYINF
      character(len =   2)  :: SYOBS,SYOBS0,SYSUP
      character(len =   5)  :: NUO
      character(len =   6)  :: IDENT
      character(len =   8)  :: CTRP  = 'CL_eqint'
      character(len =  10)  :: OPT
      character(len = 120)  :: FEDI,FEDS,FEMMTD,FPARA,FPARAT,NOM

      logical          :: BSP90 = .FALSE.
      logical          :: EOF90 = .FALSE.
      logical          :: INTRA = .FALSE.
      logical          :: REP
!
1001  FORMAT(A)
2000  FORMAT(' <  ',A)
2001  FORMAT(' >> ',A)
2005  FORMAT(2A,3F11.4,2E13.4,F6.1,'% ',A,2(I3,1X,2A,I3,1X))
2006  FORMAT((10X,20(2I4,'%')))
2007  FORMAT(I4,' UPPER VIBRATIONAL SUBLEVEL(S)')
2008  FORMAT(I4,' LOWER VIBRATIONAL SUBLEVEL(S)')
2009  FORMAT(I4,' GREATER VIBRATIONAL PERCENTAGE(S)',/)
2010  FORMAT(/,              &
             'EQ_INT : ',A)
2011  FORMAT(' EQ_INT -> J = ',I3,'/',I3)
2012  FORMAT(' EQ_INT -> NB ASG+ =',I6)
2020  FORMAT(F13.6,2X,2A,2F7.3,F11.3,F13.6,F8.3,I5,1X,2A,I3,F13.6,I3,I4,'%',2X,A,1X,   &
             2(I3,1X,2A,I3,1X)                                                      )
2021  FORMAT(E13.4,2X,2A,F7.3,'%',F6.3,'%',F10.3,'%',E12.4,F8.3,'%',I4,   &
             1X,2A,I3,F13.6,I3,I4,'%'                                  )
2022  FORMAT(F13.6,2X,2A,2F7.3,11X,F13.6,F8.3,I5,1X,2A,I3,F13.6,I3,I4,'%',2X,A,1X,   &
             2(I3,1X,2A,I3,1X)                                                    )
3000  FORMAT(2A,F12.6,1X,A,E11.4,1X,A,F10.6,F6.1,2X,2(I3,1X,2A,I3,1X))
3003  FORMAT(I3,I8,F15.6,6X,I6,4X,3(F11.2,1X,'%'))
3004  FORMAT(////,   &
             I4,//)
3007  FORMAT('     UPPER LEVEL : ',I3,' OPERATORS',6X,'LOWER LEVEL : ',I3,' OPERATORS')
3008  FORMAT(18X,I3,' TRANSITION OPERATORS',//)
3009  FORMAT('* POSITION :')
3010  FORMAT('      FEXP       CODE   SDEXP  SDTHEO  EXP-CALC      ',   &
             'FCAL      SDCAL  ( J C    N      E  )SUP         ....',   &
             ' ASSIGNMENT  ....'                                     )
3011  FORMAT('* STRENGTH :')
3012  FORMAT('      SEXP       CODE  %SDEXP %SDTHEO %EXP-CALC      ',   &
             'SCAL     %SDCAL  ( J C    N      E  )INF'              )
3013  FORMAT(//,                      &
             ' PARTIAL STATISTICS ')
3014  FORMAT(//,                                          &
             I6,' DATA REACHING THE UPPER SUBLEVEL ',I3)
3015  FORMAT(//,                                                                                        &
             '      NUMBER OF   THEORETICAL   CUMULATIVE      PARTIAL      MEAN       CUMULATIVE', /,   &
             'Jsup    DATA       PRECISION    NB OF DATA     STD. DEV.     DEV.       STD. DEV.' ,// )
3016  FORMAT(//,                     &
             ' GLOBAL STATISTICS ')
3017  FORMAT(////,   &
             I4   )
3018  FORMAT(I1)
8000  FORMAT(' !!! EQ_INT : STOP ON ERROR')
8001  FORMAT(' !!! ERROR OPENING CONTROL FILE')
8002  FORMAT(' !!! INCOMPLETE CONTROL FILE')
8003  FORMAT(' !!! INCOMPATIBLE'                        ,/,   &
             ' !!!     TRANSITION MOMENT        JC:',2I4,/,   &
             ' !!! AND LOWER POLYAD HAMILTONIAN JC:',2I4   )
8004  FORMAT(' !!! BAD OPTION IN CONTROL FILE')
8005  FORMAT(' !!! INCOMPATIBLE'                          ,/,   &
             ' !!! UPPER POLYAD TRANSITION MOMENT    :',I4,/,   &
             ' !!!          AND HAMILTONIAN SUBLEVELS:',I4   )
8006  FORMAT(' !!! INCOMPATIBLE'                          ,/,   &
             ' !!! LOWER POLYAD TRANSITION MOMENT    :',I4,/,   &
             ' !!!          AND HAMILTONIAN SUBLEVELS:',I4   )
8013  FORMAT(' !!! 0. < FPVIB    REQUESTED')
8014  FORMAT(' !!! 0. < ABUND <=1.    REQUESTED')
8101  FORMAT(' !!! DIPOLE MOMENT FILE => UNEXPECTED EOF')
8102  FORMAT(' !!! UPPER LEVEL FILE => UNEXPECTED EOF')
8103  FORMAT(' !!! LOWER LEVEL FILE => UNEXPECTED EOF')
8104  FORMAT(' !!! ERROR OPENING DIPOLE MOMENT FILE: ',A)
8105  FORMAT(' !!! ERROR OPENING EXPERIMENTAL FILE: assignments.t')
8106  FORMAT(' !!! ERROR OPENING UPPER EIGENVAL. AND VECT. FILE: ',A)
8107  FORMAT(' !!! ERROR OPENING LOWER EIGENVAL. AND VECT. FILE: ',A)
8108  FORMAT(' !!! ERROR OPENING VARIANCE / COVARIANCE FILE: para_variance.t')
8109  FORMAT(' !!! ERROR OPENING statistics.t FILE')
8110  FORMAT(' !!! ERROR OPENING normal_eq.t FILE')
8111  FORMAT(' !!! ERROR OPENING prediction_mix.t FILE')
8112  FORMAT(' !!! ERROR OPENING prediction.t FILE')
8113  FORMAT(' !!! NO valid line in assignments.t')
8124  FORMAT(' !!! NB OF OPERATORS MUST BE EQUAL IN'   ,/,   &
             '         PARAMETER CONTROL FILE:',I8,2X,A,/,   &
             '     AND PARAMETER         FILE:',I8,2X,A   )
8125  FORMAT(' !!! BAD CONTROL VALUE FOR A PARAMETER WITH DEPENDANCY LINE; MUST BE 0 OR 1:',/,   &
             A                                                                                )
!
! ARRAY INITIAL ALLOCATION
!
      CALL ALLOC_ALL
!
      MVAR  = 0                                                                                    ! INITIALISATION PAR DEFAUT
      IM    = 0                                                                                    ! INITIALISATION PAR DEFAUT
      DIFSA = 0.D0                                                                                 ! INITIALISATION PAR DEFAUT
      OPEN(10,FILE=CTRLF,ERR=9998,STATUS='OLD')
      READ(10,1001,END=9997) FDATE
      PRINT 2010,            FDATE
      DO I=1,3                                                                                     ! ARGUMENTS OBLIGATOIRES
        READ(10,1001,END=9997) CARG(I)
      ENDDO
      READ(10,*,END=9997) TEMP
      READ(10,*,END=9997) RINMI
      READ(10,*,END=9997) JMAX
      READ(10,1001,END=9997) FPARA
      READ(10,1001,END=9997) NOM                                                                   ! FICHIER DE CONTROLE DES PARA
      PRINT 2000,  TRIM(NOM)
      OPEN(12,FILE=TRIM(NOM),STATUS='OLD')
      READ(12,3004) NBOPC
      DO WHILE( NBOPC .GT. MXOPT )
        CALL RESIZE_MXOPT
      ENDDO
      OPEN(13,FILE=FPARA,STATUS='OLD')
      READ(13,3017) NBOM
      DO I=1,2+NBOM
        READ(13,1001)
      ENDDO
      READ(13,3017) NBOM
      IF( NBOM .NE. NBOPC ) THEN
        PRINT 8124, NBOPC,TRIM(NOM),NBOM,TRIM(FPARA)
        GOTO  9999
      ENDIF
      CLOSE(13)
      ICNTCL = 0                                                                                   ! nb reduit de paras fittes
      NCL2   = NBCLAB+9                                                                            ! Frmd included
      DO IPCBN=1,NBOPC                                                                             ! POUR CHAQUE PARA
!  lecture des caracteristiques du parametre
        READ(12,1001) COPAJ
        READ(COPAJ(NCL2:NCL2),3018) ICNTRL                                                         ! ETAT DU PARA
        ISFIT(IPCBN) = ICNTRL
!  lecture d'une eventuelle unique ligne de dependances
        NSTAR = 0
        READ(12,1001,END=300) TITRE
        NSTAR = INDEX(TRIM(TITRE),'*')
        IF( NSTAR .LT. 1 ) THEN                                                                    ! pas une ligne de dependances
          BACKSPACE(12)                                                                            ! se repositionner pour le parametre suivant
        ENDIF
!
300     CONTINUE                                                                                   ! EOF
! traitement des differents cas
        IF( NSTAR .LT. 1 ) THEN                                                                    ! pas une ligne de dependances
          IF( ICNTRL .GT. 1 ) THEN                                                                 ! SI PARA FITTE ...
            ICNTCL       = ICNTCL+1
            IXCL(ICNTCL) = IPCBN                                                                   ! ... STOCKER SON NUMERO ...
          ENDIF
        ELSE                                                                                       ! ligne de dependances
          IF( ICNTRL .LE. 1 ) THEN                                                                 ! SI PARA FITTE ...
            ISFIT(IPCBN) = 5                                                                       ! marque les dependances
            ICNTCL       = ICNTCL+1
            IXCL(ICNTCL) = IPCBN
          ELSE
            PRINT 8125, TRIM(COPAJ)                                                                ! autre que 0,1 : interdit
            GOTO  9999
          ENDIF
        ENDIF
      ENDDO
! ecriture du fichier CTRP
! fichier de controle des paramateres non fixes a 0.
      PRINT 2001,  TRIM(CTRP)
      OPEN(15,FILE=TRIM(CTRP),STATUS='UNKNOWN')                                                    ! FICHIER DE CONTROLE REDUIT
! CTRP : codes de controle > 1
      REWIND(12)
      READ(12,3004) III
      WRITE(15,3004) ICNTCL
      DO IPCBN=1,NBOPC
        READ(12,1001)    TITRE
        IF( ISFIT(IPCBN) .GT. 1 ) THEN                                                             ! SI PARA FITTE ...
          WRITE(15,1001) TITRE                                                                     ! ... LE RECOPIER
          IF( ISFIT(IPCBN) .EQ. 5 ) THEN                                                           ! ligne de dependances
            READ (12,1001) TITRE
            WRITE(15,1001) TITRE
          ENDIF
        ENDIF
      ENDDO
! CTRP : codes de controle = 0
      REWIND(12)
      READ(12,3004) III
      WRITE(15,1001)                                                                               ! ligne vide, de separation
      DO IPCBN=1,NBOPC
        READ(12,1001)    COPAJ
        IF    ( ISFIT(IPCBN) .EQ. 0 ) THEN                                                         ! SI PARA FIXE ...
          WRITE(15,1001) COPAJ                                                                     ! ... LE RECOPIER
        ELSEIF( ISFIT(IPCBN) .EQ. 5 ) THEN                                                         ! ligne de dependances
          READ(12,1001)                                                                            ! la lire
        ENDIF
      ENDDO
! CTRP : codes de controle = 1
      REWIND(12)
      READ(12,3004) III
      WRITE(15,1001)                                                                               ! ligne vide, de separation
      DO IPCBN=1,NBOPC
        READ(12,1001)    COPAJ
        IF    ( ISFIT(IPCBN) .EQ. 1 ) THEN                                                         ! SI PARA FIXE ...
          WRITE(15,1001) COPAJ                                                                     ! ... LE RECOPIER
        ELSEIF( ISFIT(IPCBN) .EQ. 5 ) THEN                                                         ! ligne de dependances
          READ(12,1001)                                                                            ! la lire
        ENDIF
      ENDDO
      CLOSE(15)
      CLOSE(12)
!
!      ---> MVAR  =1 SI ON VEUT CALCULER LA PRECISION SUR LES INTENSITES
!                      ATTENTION : AJOUTER FICHIER VARIANCE/COVARIANCE
!      ---> IRA   =1 SI IR ; IRA=0 SI RAMAN ISOTROPE
!      ---> ICOEFU=1 CONVERTIT LES DONNEES DE MHZ EN CM-1
!      ---> ICOEFU=2 CONVERTIT LES DONNEES DE GHZ EN CM-1
!      ---> LPCMAX = LES LPC PLUS GRANDS POURCENTAGES VIBRATIONNELS
!      ---> NTM   =1 SI RECALCULER TM
!
      IRA    =  1
      ICOEFU =  0
      NTM    =  0
      LPCMAX =  1
      THRES  = 50.D0
      ABUND  =  1.D0                                                                               ! abundance
      FPVIB  =  0.D0                                                                               ! no fpvib option
!
19    READ(10,1001,END=21) OPT
      IF    ( OPT .EQ. 'prec'  ) THEN
        INQUIRE(FILE='para_variance.t',EXIST=REP)
        IF( REP ) MVAR = 1
      ELSEIF( OPT .EQ. 'pol'   ) THEN
        IRA = 0
      ELSEIF( OPT .EQ. 'MHz'   ) THEN
        IF( ICOEFU .NE. 0 ) GOTO 9997
        ICOEFU = 1
      ELSEIF( OPT .EQ. 'GHz'   ) THEN
        IF( ICOEFU .NE. 0 ) GOTO 9997
        ICOEFU = 2
      ELSEIF( OPT .EQ. 'mix'   ) THEN
        READ(10,*,END=9997) LPCMAX
      ELSEIF( OPT .EQ. 'para'  ) THEN
        READ(10,1001,END=9997) FPARAT
        NTM = 1
      ELSEIF( OPT .EQ. 'thres' ) THEN
        READ(10,*,END=9997) THRES
      ELSEIF( OPT .EQ. 'fpvib' ) THEN
        READ(10,*,END=9997) FPVIB
        IF( FPVIB .LE. 0.D0 ) GOTO 9992
      ELSEIF( OPT .EQ. 'abund' ) THEN
        READ(10,*,END=9997) ABUND
        IF( ABUND .LE. 0.D0 .OR.              &
            ABUND .GT. 1.D0      ) GOTO 9991
      ELSE
        GOTO 9996                                                                                  ! OPTION NON RECONNUE
      ENDIF
      GOTO 19
!
21    CLOSE(10)
      FEDS = 'ED_'//TRIM(CARG(1))//'_'
      FEDI = 'ED_'//TRIM(CARG(2))//'_'
      IF( FEDS .EQ. FEDI ) INTRA = .TRUE.
      FEMMTD = 'TD_'//TRIM(CARG(1))//'m'//TRIM(CARG(2))//'_'//TRIM(CARG(3))//'_'
      IF( IRA .EQ. 0 ) THEN
        FEMMTD = 'TA_'//TRIM(CARG(1))//'m'//TRIM(CARG(2))//'_'//TRIM(CARG(3))//'_'
      ENDIF
      IF( NTM .EQ. 1 ) THEN
        PRINT 2000,  TRIM(FPARAT)
        OPEN(82,FILE=TRIM(FPARAT),STATUS='OLD')
      ENDIF
!
!   FICHIER D'ENERGIES ET DERIVEES SUP
!
      PRINT 2000,           TRIM(FEDS)
      OPEN(50,ERR=4050,FILE=TRIM(FEDS),STATUS='OLD',FORM='UNFORMATTED')
      OPEN(10,STATUS='SCRATCH')
      READ(50)
      READ (50)      TITRE
      WRITE(10,1001) TITRE
      READ(50)
      READ (50)      TITRE
      WRITE(10,1001) TITRE
      READ (50)    I,TITRE
      WRITE(10,1001) TITRE
      REWIND(50)
      REWIND(10)
      READ(10,*) SPIN,SPINY
      READ(10,*) VIBNU,B0,D0
      READ(10,*) PNEG,IMEGA
      CLOSE(10)
!
!   FICHIER D'ENERGIES ET DERIVEES INF
!
      PRINT 2000, TRIM(FEDI)
      IF( INTRA ) THEN
        OPEN(51,STATUS='SCRATCH',FORM='UNFORMATTED')
        CALL IOPBA0(50,0)
        READ(50) NSVINF
        DO WHILE( NSVINF .GT. MXSNV )
          CALL RESIZE_MXSNV
        ENDDO
        DO I=1,NSVINF
          READ (50) TITRE(:96)
          WRITE(51) TITRE(:96)
        ENDDO
        READ (50) NBOPHI
        WRITE(51) NBOPHI
        DO WHILE( NBOPHI .GT. MXOPH )
          CALL RESIZE_MXOPH
        ENDDO
!
4001    READ(50,END=4003) JINF,ICINF,ICPINF,NBINF
        IF( JINF .GT. JMAX+2 ) GOTO 4003
        WRITE(51)         JINF,ICINF,ICPINF,NBINF
        DO IP=1,NBINF
          READ (50,END=4160) ENINFC,(DERBID(IOP),IOP=1,NBOPHI),(ICENT(ISV),ISV=1,NSVINF)
          WRITE(51)          ENINFC,(DERBID(IOP),IOP=1,NBOPHI),(ICENT(ISV),ISV=1,NSVINF)
        ENDDO
        GOTO 4001
!
4003    REWIND(50)
        REWIND(51)
      ELSE
        OPEN(51,ERR=4051,FILE=TRIM(FEDI),STATUS='OLD',FORM='UNFORMATTED')
        CALL IOPBA0(51,0)
        READ(51) NSVINF
        DO WHILE( NSVINF .GT. MXSNV )
          CALL RESIZE_MXSNV
        ENDDO
      ENDIF
!
!   FICHIER D'ELEMENTS MATRICIELS M.T. & DERIVEES
!
      PRINT 2000,           TRIM(FEMMTD)
      OPEN(81,ERR=4081,FILE=TRIM(FEMMTD),STATUS='OLD',FORM='UNFORMATTED')
!
!   FICHIER D'OBSERVEES
!
      PRINT 2000,           'assignments.t'
      OPEN(90,ERR=4090,FILE='assignments.t',STATUS='OLD')
!
!   FICHIER DE VARIANCE/COVARIANCE
!
      IF( MVAR .EQ. 1 ) THEN
        PRINT 2000,           'para_variance.t'
        OPEN(94,ERR=4094,FILE='para_variance.t',STATUS='OLD',FORM='UNFORMATTED')
      ENDIF
!
!   FICHIERS DE SORTIE :
!   TRANSITIONS OBSERVEES - CALCULEES
!
      PRINT 2001,           'prediction.t'
      OPEN(91,ERR=4080,FILE='prediction.t',STATUS='UNKNOWN')
      PRINT 2001,           'prediction_mix.t'
      OPEN(95,ERR=4091,FILE='prediction_mix.t',STATUS='UNKNOWN')
!
!   EQUATION NORMALE : Y=AX
!
      PRINT 2001,           'normal_eq.t'
      OPEN(92,ERR=4092,FILE='normal_eq.t',FORM='UNFORMATTED',STATUS='UNKNOWN')
!
!   STATISTIQUES
!
      PRINT 2001,           'statistics.t'
      OPEN(93,ERR=4093,FILE='statistics.t',STATUS='UNKNOWN')
!
!   INITIALISATION
!
      NFIT = 0
      SS0  = 0.D0
      SS1  = 0.D0
      DO J=0,JMAX
        TNEGL(J) = 0.D0
        DO ISV=0,MXSNV
          NFJS(J,ISV)   = 0
          SMJS(J,ISV)   = 0.D0
          SPOIS(J,ISV)  = 0.D0
          SPRORS(J,ISV) = 0.D0
        ENDDO
      ENDDO
      DO IOPT=1,ICNTCL
        YS(IOPT) = 0.D0
        DO JOPT=1,ICNTCL
          AS(JOPT,IOPT) = 0.D0
        ENDDO
      ENDDO
      CMGHZ = 1.D0
      IF( ICOEFU .EQ. 1 ) CMGHZ = CMHZ
      IF( ICOEFU .EQ. 2 ) CMGHZ = CGHZ
      COEF = (8.D0*PI*PI*PI*CL)/(CLUM*PLANK)*1.D-36
      COEF = COEF*T0/TEMP/FPART(TEMP,TEMP)
!
! ******************************************************
! ***   LECTURE , RECOPIE ET POSITIONNEMENT           ***
! ******************************************************
!
!   LECTURE DES PARAMETRES DE H
!
      WRITE(91,1001)
      CALL IOBA0B(50,91,92)
      CALL IOPBA0(81,0)
      IF( MVAR .EQ. 1 ) CALL IOPBA0(94,0)
      WRITE(91,1001)
!
!   LECTURE DES PARAMETRES DU M.D. ET STOCKAGE EN MEMOIRE SI NTM=1
!
      IF( NTM .EQ. 1 ) THEN
        CALL IOPA0(82)
        CALL IOPAAB(82,91,92,ICNTCL)
        CLOSE(82)
        CALL IOPBA0(81,0)
      ELSE
        CALL IOBA0C(81,91,92,ICNTCL)
      ENDIF
      WRITE(91,1001)
!
!   NSVSUP SOUS-NIVEAUX VIBRATIONNELS SUP
!
      READ (50)      NSVSUP,TITRE
      WRITE(91,2007) NSVSUP
      WRITE(93,2007) NSVSUP
      WRITE(95,2007) NSVSUP
      DO WHILE( NSVSUP .GT. MXSNV )
        CALL RESIZE_MXSNV
      ENDDO
      LPCMAX = MIN(NSVSUP,LPCMAX)                                                                  ! reduce LPCMAX if needed
      DO ISV=1,NSVSUP
        READ (50)      TITRE(:96)
        WRITE(91,1001) TRIM(TITRE)
        WRITE(93,1001) TRIM(TITRE)
        WRITE(95,1001) TRIM(TITRE)
      ENDDO
      WRITE(91,2009) LPCMAX
      WRITE(93,2009) LPCMAX
      WRITE(95,2009) LPCMAX
!
!   NSVINF SOUS-NIVEAUX VIBRATIONNELS INF
!
      WRITE(91,2008) NSVINF
      DO ISV=1,NSVINF
        READ (51)      TITRE(:96)
        WRITE(91,1001) TRIM(TITRE)
      ENDDO
!
!   TEST DE COHERENCE
!
      READ(81) NSVS,TITRE
      IF( NSVS .NE. NSVSUP ) THEN
        PRINT 8005, NSVS,NSVSUP
        GOTO  9999
      ENDIF
      DO ISV=1,NSVSUP
        READ(81) TITRE(:96)
      ENDDO
      READ(81) NSVI,TITRE
      IF( NSVI .NE. NSVINF ) THEN
        PRINT 8006, NSVI,NSVINF
        GOTO  9999
      ENDIF
      DO ISV=1,NSVINF
        READ(81) TITRE(:96)
      ENDDO
!
!   NBOPHS : NOMBRE D'OPERATEURS DU NIVEAU SUP
!   NBOPHI : NOMBRE D'OPERATEURS DU NIVEAU INF
!   NBOTR  : NOMBRE D'OPERATEURS DE TRANSITION
!
      WRITE(91,1001)
      READ(50) NBOPHS
      DO WHILE( NBOPHS .GT. MXOPH )
        CALL RESIZE_MXOPH
      ENDDO
      READ(51) NBOPHI
      DO WHILE( NBOPHI .GT. MXOPH )
        CALL RESIZE_MXOPH
      ENDDO
      READ(81) NBOTR
      DO WHILE( NBOTR .GT. MXOPT )
        CALL RESIZE_MXOPT
      ENDDO
      WRITE(91,3007) NBOPHS,NBOPHI
      WRITE(91,3008) NBOTR
      WRITE(91,1001) FDATE
      WRITE(91,1001)
      WRITE(93,1001) FDATE
      WRITE(93,1001)
      WRITE(91,3009)
      WRITE(91,3010)
      WRITE(91,3011)
      WRITE(91,3012)
      WRITE(91,1001)
!
!   LECTURE DE LA PREMIERE RAIE EXPERIMENTALE
!
5     CONTINUE
      READ(90,3000,ERR=5,END=700) ISP,NUO,FOBS,FASO,SOBS,SASO,SDFOBS,SDSOBS,  &
                                  JOBS0,SYOBS0,PAOBS0,NOBS0,                  &
                                  JOBS,SYOBS,PAOBS,NOBS
      FOBS   = FOBS/CMGHZ                                                                          ! -> CM-1
      SOBS   = SOBS/ABUND
      SDFOBS = SDFOBS/CMGHZ                                                                        ! -> CM-1
!
! ******************************************************
! *** LECTURE ET STOCKAGE DU FICHIER D'ENERGIES INF  ***
! ******************************************************
!
      INF  = 0
      INFF = 0
!
6     INFF = INFF+1
      READ(51,END=8) JINF,ICINF,ICPINF,NBINF
      JIC = JINF*100+ICINF*10+ICPINF
      DO IP=1,NBINF
        INF = INF+1
        IF( INF .GT. MXENI ) CALL RESIZE_MXENI
        JC(INF) = JIC
        READ(51,END=4160) ENINF(INF),(DERBID(IOP),IOP=1,NBOPHI),(ICENT(ISV),ISV=1,NSVINF)
        KCENT = 0
E9:     DO IC=1,NSVINF
          IF( KCENT .GT. ICENT(IC) ) CYCLE E9
          KCENT = ICENT(IC)
          IM    = IC
        ENDDO E9
        JCENTI(INF) = IM+10*KCENT
      ENDDO
      GOTO 6
!
8     NTOTIN = INF
!
! ******************************************************
! ***      LECTURE ET STOCKAGE DE LA MATRICE         ***
! ***   DE VARIANCE/COVARIANCE DES PARAMETRES DE H   ***
! ******************************************************
!
      IF( MVAR .EQ. 1 ) THEN
        DO J=1,ICNTCL
          READ(94) (VACO(K,J),K=J,ICNTCL)
        ENDDO
      ENDIF
!
! ********************************************
! *** CALCUL DES TRANSITIONS THEORIQUES ***
! ********************************************
!
!   INDICE DE LA TRANSITION
!
10    READ(81,END=90) JS,ICS,ICPS,NFBS,  &
                      JI,ICI,ICPI,NFBI
      IF( JS .GT. JMAX ) GOTO 90
      JIICI = 100*JI+ICI*10+ICPI
!
!   RECHERCHE DU NIVEAU SUP
!
11    READ(50,END=90) JSUP,ICSUP,ICPSUP,NFBSUP
      IF( JS   .EQ. JSUP   .AND.            &
          ICS  .EQ. ICSUP  .AND.            &
          ICPS .EQ. ICPSUP       ) GOTO 14
      DO I=1,NFBSUP
        READ(50,END=4161)
      ENDDO
      GOTO 11
!
!   RECHERCHE DU NIVEAU INF
!
14    CONTINUE
E15:  DO IN=1,NTOTIN
        IF( JIICI .GT. JC(IN) ) CYCLE E15
        IF( JIICI .EQ. JC(IN) ) GOTO 16
      ENDDO E15
      JIL   = JC(IN)/100
      ICIL  = (JC(IN)-JIL*100)/10
      ICPIL = JC(IN)-JIL*100-ICIL*10
      PRINT 8003, JI,ICI,JIL,ICIL
      GOTO  9999
!
16    CONTINUE
E18:  DO IS=1,NFBS
        READ(50,END=4161) ENSUP,(DERBID(IOP),IOP=1,NBOPHS),(ICENT(ISV),ISV=1,NSVSUP)
E17:    DO II=1,NFBI
          N = IN+II-1
          READ(81,END=4150) NSUP,NINF,TM,(DERT(IOP),IOP=1,NBOTR)
          IF( NTM .EQ. 1 ) THEN                                                                    ! NOUVEAU TM (OPTION 'PARA')
            TM = 0.D0
            DO IOP=1,NBOTR
              TM = TM+PARAT(IOP)*DERT(IOP)
            ENDDO
          ENDIF
          DO LPC=1,LPCMAX
            IPCSUP(LPC) = 0
            NUSVSU(LPC) = 0
E22:        DO IC=1,NSVSUP
              DO LPCI=1,LPC-1
                IF( IC .EQ. NUSVSU(LPCI) ) CYCLE E22
              ENDDO
              IF( IPCSUP(LPC) .GT. ICENT(IC) ) CYCLE E22
              IPCSUP(LPC) = ICENT(IC)
              NUSVSU(LPC) = IC
            ENDDO E22
          ENDDO
!
!   NOMBRE D'ONDE
!
          FCAL = ENSUP-ENINF(N)
          IF( FCAL .LE. 0.D0 ) CYCLE E17
          IPCINF = JCENTI(N)/10
          NUSFIN = JCENTI(N)-10*IPCINF
!
!   INTENSITE
!
          A2 = TM
          A2 = A2*A2
          HT = ABS(ENINF(N)*HCOVRK/TEMP)
          W0 = EXP(-HT)*SPIN(ICI)
!
!   INTENSITE IR
!
          COL   = W0*FCAL*COEF*(1.D0-EXP(-FCAL*HCOVRK/TEMP))
          SCAL  = COL*A2
          CODER = 2.D0*TM*COL
          DO IOP=1,NBOTR
            DERS(IOP) = CODER*DERT(IOP)
!
! FRI OCT 15 09:46:26 WETDST 1993 - T. GABARD / L. TOUZANI
! *** MODIF. DU CALCUL DE LA DERIVEE DANS TRMOMT.F -> CALCUL
!     INTERMEDIAIRE DEJA EFFECTUES.
!
            IF( IRA .EQ. 0 ) DERS(IOP) = W0*DERT(IOP)
          ENDDO
!
!    INTENSITE RAMAN ISOTROPE
!
          IF( IRA .EQ. 0 ) SCAL = A2*W0
!
! ********************************************************
! *** CALCUL DE L'ECART TYPE DE L' INTENSITE THEORIQUE ***
! ********************************************************
!
          IF( MVAR .EQ. 1 ) THEN
            PF1 = 0.D0
            PF2 = 0.D0
E773:       DO IOP=1,ICNTCL
              DDT    = DERS(IXCL(IOP))
              VACOII = VACO(IOP,IOP)
              PF1    = PF1+DDT*DDT*VACOII
              IF( IOP .EQ. ICNTCL ) CYCLE E773
              DO JOP=IOP+1,ICNTCL
                VACOIJ = VACO(JOP,IOP)
                PF2    = PF2+DDT*DERS(IXCL(JOP))*VACOIJ
              ENDDO
            ENDDO E773
            IF( SCAL .EQ. 0.D0 ) THEN
              SDSCAL = 999.999D0
            ELSE
              SDSCAL = SQRT(PF1+PF2*2.D0)/SCAL*100.D0
            ENDIF
          ELSE
            SDSCAL = 0.D0
          ENDIF
          SDFMOD = 1.D0
          IF( IMEGA .NE. 0 ) SDFMOD = DBLE(JS)**IMEGA
          SDFMOD = PNEG*SDFMOD
          SDFMOO = SDFMOD*1.D+3*CMGHZ
          SDFCAL = 0.D0
          FCAO   = FCAL*CMGHZ
!
! *******************************************
! *** RECHERCHE DES TRANSITIONS OBSERVEES ***
! *******************************************
!
!   INITIALISATION
!
          FEXP   = 0.D0
          SEXP   = 0.D0
          SDFEXP = 0.D0
          SDFMOD = 0.D0
          SDSMOD = 0.D0
          SDSEXP = 0.D0
          SYSUP  = SYM(ICS)
          SYINF  = SYM(ICI)
          PASUP  = PAR(ICPS)
          PAINF  = PAR(ICPI)
          BR     = BRANCH(JS-JI+MXBRA-2)
          IDENT  = '      '
          FAS    = ' '
          SAS    = ' '
!
321       IF    ( JOBS .LT. JS ) THEN
            GOTO 314
          ELSEIF( JOBS .GT. JS ) THEN
            GOTO 316
          ENDIF
          IF( SYOBS .LT. SYSUP ) GOTO 314
          IF( SYOBS .GT. SYSUP ) GOTO 316
          IF( PAOBS .LT. PASUP ) GOTO 314
          IF( PAOBS .GT. PASUP ) GOTO 316
          IF    ( JOBS0 .LT. JI ) THEN
            GOTO 314
          ELSEIF( JOBS0 .GT. JI ) THEN
            GOTO 316
          ENDIF
          IF( SYOBS0 .LT. SYINF ) GOTO 314
          IF( SYOBS0 .GT. SYINF ) GOTO 316
          IF( PAOBS0 .LT. PAINF ) GOTO 314
          IF( PAOBS0 .GT. PAINF ) GOTO 316
          IF    ( NOBS .LT. NSUP ) THEN
            GOTO 314
          ELSEIF( NOBS .GT. NSUP ) THEN
            GOTO 316
          ENDIF
          IF    ( NOBS0 .EQ. NINF ) THEN
            GOTO 315
          ELSEIF( NOBS0 .GT. NINF ) THEN
            GOTO 316
          ENDIF
!
!   LECTURE SUIVANTE
!
314       CONTINUE
          IF( EOF90 ) GOTO 326
          READ(90,3000,ERR=314,END=701) ISP,NUO,FOBS,FASO,SOBS,SASO,SDFOBS,SDSOBS,  &
                                        JOBS0,SYOBS0,PAOBS0,NOBS0,                  &
                                        JOBS,SYOBS,PAOBS,NOBS
          FOBS   = FOBS/CMGHZ                                                                      ! -> CM-1
          SOBS   = SOBS/ABUND
          SDFOBS = SDFOBS/CMGHZ                                                                    ! -> CM-1
          GOTO 321
!
701       EOF90 = .TRUE.                                                                           ! marquer la fin de fichier
          GOTO 326
!
!   TRANSITION NON OBSERVEE
!
316       BACKSPACE(90)
          BSP90 = .TRUE.
          GOTO 322
!
!   TRANSITION OBSERVEE
!
315       IDENT  = ISP(1:1)//NUO(1:5)
          FEXP   = FOBS
          SDFEXP = SDFOBS
          IF( FOBS .EQ. 0.D0 ) SDFEXP = 0.D0
          IF( FASO .NE. '+'  ) SDFEXP = 0.D0
          FAS    = FASO
          SEXP   = SOBS
          SDSEXP = SDSOBS
          IF( SOBS .EQ. 0.D0 ) SDSEXP = 0.D0
          IF( SASO .NE. '+'  ) SDSEXP = 0.D0
          SAS = SASO
!
322       DIFF  = FEXP-FCAL
          DIFSA = SEXP-SCAL
          IF( SCAL .EQ. 0.D0 ) THEN
            DIFS = 999.999D0
          ELSE
            DIFS = 100.D0*DIFSA/SCAL
          ENDIF
          IF( BSP90 ) THEN
            BSP90 = .FALSE.
            GOTO 326
          ENDIF
          IF( ABS(DIFS) .GT. THRES ) GOTO 326
!
!
! ********************************************
! ***    CALCUL DES EQUATIONS NORMALES     ***
! ********************************************
!
          IF( SEXP   .EQ. 0.D0 ) GOTO 326
          IF( SDSEXP .EQ. 0.D0 ) GOTO 326
          SDSEXA = SEXP*SDSEXP/100.D0
          SD2    = SDSEXA*SDSEXA
          IF( SD2 .LE. 1.D-16 ) GOTO 326
          POIDS = 1.D0/SD2
          NUS   = NUSVSU(1)
!
!   STATISTIQUES
!
          NFIT           = NFIT+1
          POIDIF         = POIDS*DIFSA*DIFSA
          POIDIR         = POIDS*DIFS*DIFS
          SS0            = SS0+POIDS
          SS1            = SS1+POIDIF
          NFJS(JS,NUS)   = NFJS(JS,NUS)+1
          NFJS(JS,0)     = NFJS(JS,0)+1
          SMJS(JS,NUS)   = SMJS(JS,NUS)+POIDS*DIFS
          SMJS(JS,0)     = SMJS(JS,0)+POIDS*DIFS
          SPOIS(JS,NUS)  = SPOIS(JS,NUS)+POIDS
          SPOIS(JS,0)    = SPOIS(JS,0)+POIDS
          SPRORS(JS,NUS) = SPRORS(JS,NUS)+POIDIR
          SPRORS(JS,0)   = SPRORS(JS,0)+POIDIR
!
!   EQUATIONS NORMALES
!
          DO I=1,ICNTCL
            VV    = POIDS*DERS(IXCL(I))
            YS(I) = YS(I)+DIFSA*VV
            DO K=I,ICNTCL
              AS(K,I) = AS(K,I)+VV*DERS(IXCL(K))
            ENDDO
          ENDDO
!
! ********************************************
! ***  ECRITURE DANS LE FICHIER DE SORTIE  ***
! ********************************************
!
326       IF( ICOEFU .NE. 0 ) THEN
            FEXP   = FEXP*CMGHZ
            SDFEXP = SDFEXP*CMGHZ
            DIFF   = DIFF*CMGHZ
          ENDIF
          DIFF   = DIFF*1.D+3
          SDFEXP = SDFEXP*1.D+3
          IF( FEXP .NE. 0.D0 ) THEN
            WRITE(91,2020) FEXP,FAS,IDENT,SDFEXP,SDFMOO,DIFF,FCAO,SDFCAL,  &
                           JSUP,SYSUP,PASUP,NSUP,                          &
                           ENSUP,NUSVSU(1),IPCSUP(1),BR,                   &
                           JI,SYINF,PAINF,NINF,                            &
                           JSUP,SYSUP,PASUP,NSUP
            WRITE(95,2005) ISP,SAS,FEXP,FCAO,DIFF,SEXP,SCAL,DIFS,BR,  &
                           JI,SYINF,PAINF,NINF,                       &
                           JSUP,SYSUP,PASUP,NSUP
            WRITE(95,2006) (NUSVSU(JJ),IPCSUP(JJ),JJ=1,LPCMAX)
            GOTO 327
          ELSE
            IF( SCAL .GE. RINMI ) THEN
              WRITE(91,2022) FEXP,FAS,IDENT,SDFEXP,SDFMOO,FCAO,SDFCAL,  &
                             JSUP,SYSUP,PASUP,NSUP,                     &
                             ENSUP,NUSVSU(1),IPCSUP(1),BR,              &
                             JI,SYINF,PAINF,NINF,                       &
                             JSUP,SYSUP,PASUP,NSUP
              GOTO 327
            ENDIF
          ENDIF
          GOTO 317
!
327       WRITE(91,2021) SEXP,SAS,IDENT,SDSEXP,SDSMOD,DIFS,SCAL,SDSCAL,  &
                         JI,SYINF,PAINF,NINF,                            &
                         ENINF(N),NUSFIN,IPCINF
!
!   LECTURE SUIVANTE
!
317       IF( EOF90 ) CYCLE E17
          READ(90,3000,ERR=317,END=702) ISP,NUO,FOBS,FASO,SOBS,SASO,SDFOBS,SDSOBS,  &
                                        JOBS0,SYOBS0,PAOBS0,NOBS0,                  &
                                        JOBS,SYOBS,PAOBS,NOBS
          FOBS   = FOBS/CMGHZ                                                                      ! -> CM-1
          SOBS   = SOBS/ABUND
          SDFOBS = SDFOBS/CMGHZ                                                                    ! -> CM-1
          IF( JOBS   .NE. JS    .OR.              &
              SYOBS  .NE. SYSUP .OR.              &
              PAOBS  .NE. PASUP .OR.              &
              NOBS   .NE. NSUP       ) CYCLE E17
          IF( JI     .NE. JOBS0 .OR.              &
              SYOBS0 .NE. SYINF .OR.              &
              PAOBS0 .NE. PAINF .OR.              &
              NOBS0  .NE. NINF       ) CYCLE E17
!
!   IL Y A UNE AUTRE OBSERVEE CORRESPONDANT A LA MEME TRANSITION
!
          GOTO 315
!
!   FIN DE FICHIER
!
702       EOF90 = .TRUE.                                                                           ! marquer la fin de fichier
!
!   CALCUL DE LA TRANSITION THEORIQUE SUIVANTE
!
        ENDDO E17
      ENDDO E18
      DO IBS=1,NFBSUP+1
        BACKSPACE(50)
      ENDDO
      GOTO 10
!
! ********************************************
! ***      STOCKAGE DE AS ET YS            ***
! ********************************************
!
90    WRITE(92) NFIT,JMAX,SS0,SS1
      PRINT 2011, JS-1,JMAX
      PRINT 2012, NFIT
      WRITE(92) (YS(I),I=1,ICNTCL)
      DO I=1,ICNTCL
        WRITE(92) (AS(K,I),K=I,ICNTCL)
      ENDDO
!
! ********************************************
! ***   STATISTIQUES                       ***
! ********************************************
!
      WRITE(93,3013)
E105: DO NUNS=1,NSVSUP
        DO J=0,JMAX
          NCUS(J)  = NFJS(J,NUNS)
          SPCUS(J) = SPOIS(J,NUNS)
          ECUS(J)  = SPRORS(J,NUNS)
          NCU0(J)  = NCU0(J)+NCUS(J)
          SPCU0(J) = SPCU0(J)+SPCUS(J)
          ECU0(J)  = ECU0(J)+ECUS(J)
        ENDDO
        DO J=1,JMAX
          NCUS(J)  = NCUS(J-1)+NCUS(J)
          SPCUS(J) = SPCUS(J-1)+SPCUS(J)
          ECUS(J)  = ECUS(J-1)+ECUS(J)
        ENDDO
        IF( NCUS(JMAX) .EQ. 0 ) CYCLE E105
        WRITE(93,3014) NCUS(JMAX),NUNS
        WRITE(93,3015)
E104:   DO J=0,JMAX
          SMJS(J,NUNS) = SMJS(J,NUNS)/SPOIS(J,NUNS)
          ECTPJ        = SQRT(SPRORS(J,NUNS)/SPOIS(J,NUNS))
          ECUM         = SQRT(ECUS(J)/SPCUS(J))
          IF( NFJS(J,NUNS) .EQ. 0 ) CYCLE E104
          WRITE(93,3003) J,NFJS(J,NUNS),TNEGL(J),NCUS(J),ECTPJ,SMJS(J,NUNS),ECUM
        ENDDO E104
      ENDDO E105
      DO J=1,JMAX
        NCU0(J)  = NCU0(J-1)+NCU0(J)
        SPCU0(J) = SPCU0(J-1)+SPCU0(J)
        ECU0(J)  = ECU0(J-1)+ECU0(J)
      ENDDO
      WRITE(93,3016)
      WRITE(93,3015)
E107: DO J=0,JMAX
        SMJS(J,0) = SMJS(J,0)/SPOIS(J,0)
        ECTPJ     = SQRT(SPRORS(J,0)/SPOIS(J,0))
        ECUM      = SQRT(ECU0(J)/SPCU0(J))
        IF( NFJS(J,0) .EQ. 0 ) CYCLE E107
        WRITE(93,3003) J,NFJS(J,0),TNEGL(J),NCU0(J),ECTPJ,SMJS(J,0),ECUM
      ENDDO E107
      GOTO 9000
!
700   PRINT 8113
      GOTO  9999
4080  PRINT 8112
      GOTO  9999
4091  PRINT 8111
      GOTO  9999
4092  PRINT 8110
      GOTO  9999
4093  PRINT 8109
      GOTO  9999
4094  PRINT 8108
      GOTO  9999
4051  PRINT 8107, FEDI
      GOTO  9999
4050  PRINT 8106, FEDS
      GOTO  9999
4090  PRINT 8105
      GOTO  9999
4081  PRINT 8104, FEMMTD
      GOTO  9999
4160  PRINT 8103
      GOTO  9999
4161  PRINT 8102
      GOTO  9999
4150  PRINT 8101
      GOTO  9999
9991  PRINT 8014
      GOTO  9999
9992  PRINT 8013
      GOTO  9999
9996  PRINT 8004
      GOTO  9999
9997  PRINT 8002
      GOTO  9999
9998  PRINT 8001
9999  PRINT 8000
!
9000  CLOSE(50)
      CLOSE(51)
      CLOSE(81)
      CLOSE(90)
      CLOSE(91)
      CLOSE(92)
      CLOSE(93)
      IF( MVAR .EQ. 1 ) CLOSE(94)
      CLOSE(95)
      PRINT *
      END PROGRAM EQINT
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!  LECTURE ET RECOPIE DES PARAMETRES
!
      SUBROUTINE IOPBA0(LUI,LUO)
      use mod_dppr
      use mod_par_tds
      IMPLICIT NONE
      integer          :: LUI,LUO

      real(kind=dppr)  :: PARA,PREC

      integer          :: I,IP
      integer          :: NBOPH

      character(len = NBCLAB+10)  :: CHAINE
      character(len = NBCTIT)     :: TITRE
!
1000  FORMAT(A)
1001  FORMAT(I4,A)
1002  FORMAT(A,E18.11,E14.7)
8000  FORMAT(' !!! EQ_INT : STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF IN IOPBA0')
!
      DO I=1,4
        READ(LUI,END=2000)               TITRE
        IF( LUO .NE. 0 ) WRITE(LUO,1000) TITRE
      ENDDO
      READ(LUI,END=2000)               NBOPH,TITRE
      IF( LUO .NE. 0 ) WRITE(LUO,1001) NBOPH,TITRE
      DO I=1,2
        READ(LUI,END=2000)               TITRE
        IF( LUO .NE. 0 ) WRITE(LUO,1000) TITRE
      ENDDO
      DO IP=1,NBOPH
        READ(LUI,END=2000)               CHAINE,PARA,PREC
        IF( LUO .NE. 0 ) WRITE(LUO,1002) CHAINE,PARA,PREC
      ENDDO
      RETURN
!
2000  PRINT 8003
      PRINT 8000
      PRINT *
      STOP
!
      END SUBROUTINE IOPBA0
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!  LECTURE ET RECOPIE DES PARAMETRES
!
      SUBROUTINE IOPA0(LUI)
      IMPLICIT NONE
      integer          :: LUI

      integer          :: I,IP
      integer          :: NBOPH
!
1000  FORMAT(A)
1001  FORMAT(I4,A)
1002  FORMAT(A,E18.11,E14.7)
8000  FORMAT(' !!! EQ_INT : STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF IN IOPA0')
!
      DO I=1,4
        READ(LUI,1000,END=2000)
      ENDDO
      READ(LUI,1001,END=2000) NBOPH
      DO I=1,2
        READ(LUI,1000,END=2000)
      ENDDO
      DO IP=1,NBOPH
        READ(LUI,1002,END=2000)
      ENDDO
      RETURN
!
2000  PRINT 8003
      PRINT 8000
      PRINT *
      STOP
!
      END SUBROUTINE IOPA0
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!  LECTURE ET RECOPIE DES PARAMETRES
!
      SUBROUTINE IOBA0B(LUI,LUO,LUU)
      use mod_dppr
      use mod_par_tds
      IMPLICIT NONE
      integer          :: LUI,LUO,LUU

      real(kind=dppr)  :: PARA,PREC

      integer          :: I,IP
      integer          :: NBOPH

      character(len = NBCLAB+10)  :: CHAINE
      character(len = NBCTIT)     :: TITRE
!
1000  FORMAT(A)
1001  FORMAT(I4,A)
1002  FORMAT(A,E18.11,E14.7)
8000  FORMAT(' !!! EQ_INT : STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF IN IOBA0B')
!
      DO I=1,4
        READ (LUI,END=2000)              TITRE
        WRITE(LUU)                       TITRE
        IF( LUO .NE. 0 ) WRITE(LUO,1000) TRIM(TITRE)
      ENDDO
      READ (LUI,END=2000)              NBOPH,TITRE
      WRITE(LUU)                       NBOPH,TITRE
      IF( LUO .NE. 0 ) WRITE(LUO,1001) NBOPH,TRIM(TITRE)
      DO I=1,2
        READ (LUI,END=2000)              TITRE
        WRITE(LUU)                       TITRE
        IF( LUO .NE. 0 ) WRITE(LUO,1000) TRIM(TITRE)
      ENDDO
      DO IP=1,NBOPH
        READ (LUI,END=2000)              CHAINE,PARA,PREC
        WRITE(LUU)                       CHAINE,PARA,PREC
        IF( LUO .NE. 0 ) WRITE(LUO,1002) CHAINE,PARA,PREC
      ENDDO
      RETURN
!
2000  PRINT 8003
      PRINT 8000
      PRINT *
      STOP
!
      END SUBROUTINE IOBA0B
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!  LECTURE , STOCKAGE ET RECOPIE DES PARAMETRES
!
      SUBROUTINE IOPAAB(LUI,LUO,LUU,ICNTCL)
      use mod_dppr
      use mod_par_tds
      use mod_main_eqint
      IMPLICIT NONE
      integer          :: LUI,LUO,LUU,ICNTCL

      real(kind=dppr)  :: PARA,PREC

      integer          :: I,IP
      integer          :: NBOPTC

      character(len = NBCLAB+10)  :: CHAINE
      character(len = NBCTIT)  :: TITRE
!
1000  FORMAT(A)
1001  FORMAT(I4,A)
1002  FORMAT(A,E18.11,E14.7)
8000  FORMAT(' !!! EQ_INT : STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF IN IOPAAB')
!
      DO I=1,4
        READ (LUI,1000,END=2000) TITRE
        WRITE(LUU)               TITRE
        WRITE(LUO,1000)          TRIM(TITRE)
      ENDDO
      READ(LUI,1001,END=2000) NBOPTC,TITRE
      DO WHILE( NBOPTC .GT. MXOPT )
        CALL RESIZE_MXOPT
      ENDDO
      WRITE(LUU)              ICNTCL,TITRE
      WRITE(LUO,1001)         ICNTCL,TRIM(TITRE)
      DO I=1,2
        READ (LUI,1000,END=2000) TITRE
        WRITE(LUU)               TITRE
        WRITE(LUO,1000)          TRIM(TITRE)
      ENDDO
      DO IP=1,NBOPTC
        READ(LUI,1002,END=2000) CHAINE,PARA,PREC
        PARAT(IP) = PARA
        IF( ISFIT(IP) .GT. 1 ) THEN
          WRITE(LUU)            CHAINE,PARA,PREC
          WRITE(LUO,1002)       CHAINE,PARA,PREC
        ENDIF
      ENDDO
      RETURN
!
2000  PRINT 8003
      PRINT 8000
      PRINT *
      STOP
!
      END SUBROUTINE IOPAAB
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!  LECTURE ET RECOPIE DES PARAMETRES
!
      SUBROUTINE IOBA0C(LUI,LUO,LUU,ICNTCL)
      use mod_dppr
      use mod_par_tds
      use mod_com_eqint
      IMPLICIT NONE
      integer          :: LUI,LUO,LUU,ICNTCL

      real(kind=dppr)  :: PARA,PREC

      integer          :: I,IP
      integer          :: NBOPTC

      character(len = NBCLAB+10)  :: CHAINE
      character(len = NBCTIT)  :: TITRE
!
1000  FORMAT(A)
1001  FORMAT(I4,A)
1002  FORMAT(A,E18.11,E14.7)
8000  FORMAT(' !!! EQ_INT : STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF IN IOBA0C')
!
      DO I=1,4
        READ (LUI,END=2000)              TITRE
        WRITE(LUU)                       TITRE
        IF( LUO .NE. 0 ) WRITE(LUO,1000) TRIM(TITRE)
      ENDDO
      READ(LUI,END=2000)               NBOPTC,TITRE
      WRITE(LUU)                       ICNTCL,TITRE
      IF( LUO .NE. 0 ) WRITE(LUO,1001) ICNTCL,TRIM(TITRE)
      DO I=1,2
        READ (LUI,END=2000)              TITRE
        WRITE(LUU)                       TITRE
        IF( LUO .NE. 0 ) WRITE(LUO,1000) TRIM(TITRE)
      ENDDO
      DO IP=1,NBOPTC
        READ(LUI,END=2000)                 CHAINE,PARA,PREC
        IF( ISFIT(IP) .GT. 1 ) THEN
          WRITE(LUU)                       CHAINE,PARA,PREC
          IF( LUO .NE. 0 ) WRITE(LUO,1002) CHAINE,PARA,PREC
        ENDIF
      ENDDO
      RETURN
!
2000  PRINT 8003
      PRINT 8000
      PRINT *
      STOP
!
      END SUBROUTINE IOBA0C
