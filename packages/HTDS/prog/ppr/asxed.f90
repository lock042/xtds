      PROGRAM ASXED
!
! 2008 AUGUST
! V.BOUDON, CH.WENGER
!
!  lire un fichier binaire (ex. INFILE) de type XED_
!  et l'ecrire en ASCII dans INFILE'_ASC'
!
! APPEL : asxed
!
      use mod_dppr
      use mod_par_tds
      use mod_main_xpafit
      IMPLICIT NONE

      integer          :: IC,IOP,IP,ISV
      integer          :: ICP
      integer          :: J
      integer          :: NBOPHC,NFB,NSV

      character(len = NBCTIT)  :: TITRE
      character(len = 150)  :: INFILE
      character(len = 160)  :: OUTFILE
!
1000  FORMAT(A)
1020  FORMAT(/,            &
             'ASXED  : ')
2000  FORMAT(' <  ',A)
2001  FORMAT(' >> ',A)
7000  FORMAT('ENTER XED_ TYPE FILE NAME :')
8000  FORMAT(' !!! ASXED  : STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF FOR ',A)
!
! ARRAY INITIAL ALLOCATION
!
      CALL ALLOC_ALL
!
      PRINT 1020
!
!  FICHIER D'ENTREE
!
      PRINT 7000
      READ(*,1000) INFILE
      PRINT 2000,  TRIM(INFILE)
      OPEN(50,FILE=TRIM(INFILE),FORM='UNFORMATTED',STATUS='OLD')
!
!  FICHIER DE SORTIE
!
      OUTFILE = TRIM(INFILE)//'_ASC'
      PRINT 2001,  TRIM(OUTFILE)
      OPEN(10,FILE=TRIM(OUTFILE))
!
      WRITE(10,*) '>>> NSV'
      READ (50,END=9003) NSV
      WRITE(10,*)        NSV
      DO WHILE( NSV .GT. MXSNV )
        CALL RESIZE_MXSNV
      ENDDO
      DO ISV=1,NSV
        READ (50,END=9003) TITRE(:96)
        WRITE(10,*)        TITRE(:96)
      ENDDO
      WRITE(10,*) '>>> NBOPHC'
      READ (50,END=9003) NBOPHC
      WRITE(10,*)        NBOPHC
      DO WHILE( NBOPHC .GT. MXOPH )
        CALL RESIZE_MXOPH
      ENDDO
      WRITE(10,*) '>>> J,IC,ICP,NFB'
      WRITE(10,*) '>>> HD(K(IP))'
      WRITE(10,*) '>>> (DERI(IOP),IOP=1,NBOPHC)'
      WRITE(10,*) '>>> (ICENT(ISV),ISV=1,NSV)'
!
12    READ (50,END=9000) J,IC,ICP,NFB
      WRITE(10,*)        J,IC,ICP,NFB
      DO IP=1,NFB
        READ (50,END=9003) HD(1)
        WRITE(10,*)        HD(1)
        READ (50,END=9003) (DERI(IOP),IOP=1,NBOPHC)
        WRITE(10,*)        (DERI(IOP),IOP=1,NBOPHC)
        READ (50,END=9003) (ICENT(ISV),ISV=1,NSV)
        WRITE(10,*)        (ICENT(ISV),ISV=1,NSV)
      ENDDO
      GOTO 12
!
9003  PRINT 8003, TRIM(INFILE)
      PRINT 8000
!
9000  CLOSE(10)
      PRINT *
      END PROGRAM ASXED
