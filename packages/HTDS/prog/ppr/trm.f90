      PROGRAM TRM
!
!  26.01.89 FORTRAN 77 POUR SUN4 MODIFIE LE 14.03.89   J.M.JOUVARD
!  REV 25 JAN 1990
!  REV. AOU 1993  REV 8 DEC 93
! REV    JAN 1995 JPC,CW (PARAMETER)
! MODIFIE 03/98 V. Boudon ---> XY6/Oh.
!
! ##################################################################################
! ##                                                                              ##
! ##  CALCUL DES ELEMENTS MATRICIELS DU MOMENT DE TRANSITION DANS LA BASE PROPRE  ##
! ##                                                                              ##
! ##################################################################################
!
! APPEL : trm    Pn Nm Pn' Nm' Dp Jmax para_file_name trans_type [pol_state (if trans_type=pol)]
! pol_state :  R111   |   R110   |   R001
!  means    : without | parallel | perpendicular
!
! *** LIMITATIONS DU PROGRAMME
!
! DIMENSION MAXIMUM D'UN BLOC SUPERIEUR.
!     MXDIMS               ! HDK:TK:DIP
!
! DIMENSION MAXIMUM D'UN BLOC INFERIEUR.
!     MXDIMI               ! HDL:TL:DIP:HDLJ:TLJ
!
!
      use mod_dppr
      use mod_par_tds
      use mod_com_fdate
      use mod_main_trm
      IMPLICIT NONE

      real(kind=dppr)  :: A
      real(kind=dppr)  :: COEFAN,COEFIS
      real(kind=dppr)  :: EMPRO
      real(kind=dppr)  :: TM,TMA,TMI

      integer          :: I,ICK,ICK1,ICL,ICL1,ICLUK,ICLUL,IDJ
      integer          :: ICPK,ICPK1,ICPL,ICPL1,ICPLUK,ICPLUL
      integer          :: II,IK,IK1,IL,IL1,IN,IOP,IOP1,IS,ISV
      integer          :: J,JCL,JK,JK1,JL,JL1,JLUK,JLUL,JMAX
      integer          :: KK,KL
      integer          :: LG
      integer          :: NBELM,NBR,NFBK,NFBL,NICK,NICL,NK1
      integer          :: NL1,NNIVI,NNIVS,NS,NSV,NSVI,NSVS

      character(len = NBCTIT)  :: TITRE
      character(len =  11) ,dimension(5)  :: CARG
      character(len =   3)  :: MT
      character(len =   4)  :: POLST
      character(len = 120)  :: FMTR,FNINF,FNSUP,FPARA,FSORT
      character(len = 120)  :: NOM

      logical          :: INTRA = .FALSE.
!
1000  FORMAT(A)
1010  FORMAT(/,              &
             'TRM    : ',A)
1011  FORMAT(' TRM    -> J = ',I3,'/',I3)
2000  FORMAT(' <  ',A)
2001  FORMAT(' >> ',A)
2002  FORMAT(I2)
8000  FORMAT(' !!! TRM    : STOP ON ERROR')
8001  FORMAT(' !!! ERROR OPENING CONTROL FILE')
8002  FORMAT(' !!! INCOMPLETE CONTROL FILE')
8100  FORMAT(' !!! WRONG POLARIZATION STATE : ',A)
8101  FORMAT(' !!! ERROR OPENING OUTPUT FILE: ',A)
8102  FORMAT(' !!! ERROR OPENING LOWER EIGENVAL. AND VECT. FILE: ',A)
8103  FORMAT(' !!! ERROR OPENING UPPER EIGENVAL. AND VECT. FILE: ',A)
8104  FORMAT(' !!! ERROR OPENING TRANSITION MOMENT PARAMETER FILE: ',A)
8105  FORMAT(' !!! ERROR OPENING DIPOLE MOMENT FILE: ',A)
8106  FORMAT(' !!! LOWER LEVEL FILE => UNEXPECTED EOF')
8107  FORMAT(' !!! UPPER LEVEL FILE => UNEXPECTED EOF')
8108  FORMAT(' !!! DIPOLE MOMENT FILE => UNEXPECTED EOF')
8109  FORMAT(' !!! J,C,P ERROR')
!
! ARRAY INITIAL ALLOCATION
!
      CALL ALLOC_ALL
!
      OPEN(10,FILE=CTRLF,ERR=9998,STATUS='OLD')
      READ(10,1000,END=9997) FDATE
      PRINT 1010,            FDATE
      DO I=1,5
        READ(10,1000,END=9997) CARG(I)
      ENDDO
      READ(10,*,ERR=9997) JMAX
      READ(10,1000,ERR=9997) FPARA
      READ(10,1000,ERR=9997) MT
      FNINF = 'VP_'//TRIM(CARG(3))//'_'
      FNSUP = 'VP_'//TRIM(CARG(1))//'_'
      IF( FNINF .EQ. FNSUP ) INTRA = .TRUE.
      IF( MT    .EQ. 'pol' ) THEN
!     COEFFICIENTS POUR L'INTENSITE RAMAN
        POLST = 'R111'                                                                             ! PAR DEFAUT : PAS DE POLARISEUR
        READ(10,1000,END=400) POLST
!
400     IF    ( POLST .EQ. 'R111' ) THEN
          COEFIS = SQRT(1.D0/3.D0)
          COEFAN = SQRT(7.D0/30.D0)
        ELSEIF( POLST .EQ. 'R110' ) THEN
          COEFIS = SQRT(1.D0/3.D0)
          COEFAN = SQRT(4.D0/30.D0)
        ELSEIF( POLST .EQ. 'R001' ) THEN
          COEFIS = 0.D0
          COEFAN = SQRT(3.D0/30.D0)
        ELSE
          PRINT 8100, POLST
          GOTO  9999
        ENDIF
        FMTR  = 'PO_'//TRIM(CARG(1))//'m'//TRIM(CARG(3))//'_'//TRIM(CARG(5))//'_'
        FSORT = 'TP_'//TRIM(CARG(1))//'m'//TRIM(CARG(3))//'_'//TRIM(CARG(5))//'_'
        NBR   = 2
      ELSE
        FMTR  = 'DI_'//TRIM(CARG(1))//'m'//TRIM(CARG(3))//'_'//TRIM(CARG(5))//'_'
        FSORT = 'TR_'//TRIM(CARG(1))//'m'//TRIM(CARG(3))//'_'//TRIM(CARG(5))//'_'
        NBR   = 1
      ENDIF
      CLOSE(10)
      PRINT 2000, TRIM(FNINF)
      PRINT 2000, TRIM(FNSUP)
      PRINT 2000, TRIM(FPARA)
      PRINT 2000, TRIM(FMTR)
      PRINT 2001, TRIM(FSORT)
      READ(CARG(2)(2:3),2002) NNIVS
      READ(CARG(4)(2:3),2002) NNIVI
!
!  APPLICATION DES DIRECTIVES
!
      OPEN(81,ERR=4081,FILE=FSORT,STATUS='UNKNOWN',FORM='UNFORMATTED')                             ! FICHIER DE SORTIE
      IF( INTRA ) THEN
        OPEN(60,STATUS='SCRATCH',FORM='UNFORMATTED')
      ELSE
        OPEN(60,ERR=4060,FILE=FNINF,STATUS='OLD',FORM='UNFORMATTED')                               ! FICHIER DE NIV INF
      ENDIF
      OPEN(61,ERR=4061,FILE=FNSUP,STATUS='OLD',FORM='UNFORMATTED')                                 ! FICHIER DE NIV SUP
      OPEN(80,ERR=4080,FILE=FPARA,STATUS='OLD')                                                    ! FICHIER DE PARAMETRES DU MOMENT DE TRANSITIONS
      OPEN(40,ERR=4040,FILE=FMTR,STATUS='OLD',FORM='UNFORMATTED')                                  ! FICHIER DU MOMENT DE TRANS
!
! *** LECTURE DES PARAMETRES DE H
!     DANS LE FICHIER VECTEURS PROPRES / VALEURS PROPRES
!     DE POLYADE SUP ET RECOPIE DANS LE FICHIER DE SORTIE
!
      CALL IOPBB0(61,81)
      READ(61,END=4161) NSVS
      DO ISV=1,NSVS
        READ(61)
      ENDDO
      READ(61)
      IF( INTRA ) THEN
!
5000    READ(61,END=5002) JLUL,ICLUL,ICPLUL,NFBL
        IF( JLUL .GT. JMAX+2 ) GOTO 5002
        WRITE(60)         JLUL,ICLUL,ICPLUL,NFBL
        DO WHILE( NFBL .GT. MXDIMI )
          CALL RESIZE_MXDIMI
        ENDDO
        IF( NFBL .EQ. 0 ) GOTO 5000
        READ (61) (HDL(I),I=1,NFBL)
        WRITE(60) (HDL(I),I=1,NFBL)
        DO IL=1,NFBL
          READ (61) NICL,ICLRO,(TL(IL,I),I=1,NFBL)
          WRITE(60) NICL,ICLRO,(TL(IL,I),I=1,NFBL)
        ENDDO
        GOTO 5000
!
5002    REWIND(61)
        CALL IOPBB0(61,0)
        READ(61) NSVS
        DO ISV=1,NSVS
          READ(61)
        ENDDO
        READ(61)
      ENDIF
!
! *** LECTURE DES PARAMETRES DU MOMENT DE TRANSITION
!     DANS LE FICHIER DE PARAMETRES H+D
!     STOCKAGE EN MEMOIRE ET RECOPIE DANS LE FICHIER DE SORTIE
!
      CALL IOPAB(80,0,0)                                                                           ! LECTURE DES PARAMETRES DE H
      CALL IOPAB(80,81,1)                                                                          ! LECTURE DES PARAMETRES DU M.D. ET STOCKAGE EN MEMOIRE
!
! *** LECTURE DU DEBUT DU FICHIER DE MOMENT DIPOLAIRE
!
      DO I=1,3
        READ(40)
      ENDDO
      READ(40)
      DO I=1,4+NNIVI+NNIVS
        READ(40)
      ENDDO
      NS = 0
      DO IN=1,2                                                                                    ! BOUCLE SUR LES DEUX NIVEAUX
        READ (40) NSV,TITRE
        WRITE(81) NSV,TITRE
        DO ISV=1,NSV
          READ (40) TITRE(:96)
          WRITE(81) TITRE(:96)
        ENDDO
        NS = NS+NSV
      ENDDO
      DO I=1,3
        READ(40)
      ENDDO
      READ(40)
      READ (40) NBOPT,NOM(:24)
      WRITE(81) NBOPT,NOM(:24)
      DO WHILE( NBOPT .GT. MXOPT )
        CALL RESIZE_MXOPT
      ENDDO
!
! *** BOUCLE SUR LES NIVEAUX SUP (K=SUP)
! *** BOUCLE SUR JSUP
!
E3:   DO JK=0,JMAX
!
! *** REPOSITIONNEMENT SUR LE FICHIER DE V.PROPRES DE LA POLYADE INF
!
        REWIND(60)                                                                                 ! RELECTURE DE L'ENTETE DU FICHIER INF
        IF( .NOT. INTRA ) THEN
          CALL IOPBA0(60,0)
          READ(60,END=4160) NSVI
          DO ISV=1,NSVI
            READ(60,END=4160)
          ENDDO
          READ(60,END=4160)
        ENDIF
!
! *** STOCKAGE DES DIMENSIONS ET DES V.PROPRES ET VA.PROPRES DE LA POLYADE INF
!     POUR LES 3 OU 5 BRANCHES *MXSYM*2 VALEURS DE ICINF,ICPINF
!
        DO I=1,MDMJCI
          JDIM(I) = 0
        ENDDO
        DO JL=MAX(JK-NBR,0),JK+NBR
          DO ICL=1,MXSYM
            DO ICPL=1,2
!
6666          READ(60,END=67) JLUL,ICLUL,ICPLUL,NFBL
              DO WHILE( NFBL .GT. MXDIMI )
                CALL RESIZE_MXDIMI
              ENDDO
              IF( JLUL .GT. JK+NBR ) GOTO 67
              IF( NFBL .NE. 0      ) THEN
                READ(60,END=4160) (HDL(I),I=1,NFBL)
                DO IL=1,NFBL
                  READ(60,END=4160) NICL,ICLRO,(TL(IL,I),I=1,NFBL)
                ENDDO
              ENDIF
              IF( JLUL .LT. JL ) GOTO 6666
              JCL       = (JLUL-JK+NBR)*MXSYM*2+(ICLUL-1)*2+ICPLUL
              JDIM(JCL) = NFBL
              DO I=1,NFBL
                HDLJ(I,JCL) = HDL(I)
                DO J=1,NFBL
                  TLJ(J,I,JCL) = TL(J,I)
                ENDDO
              ENDDO
            ENDDO
          ENDDO
        ENDDO
!
! *** BOUCLE SUR ICSUP
!
67      DO ICK=1,MXSYM
E103:     DO ICPK=1,2
!
! *** STOCKAGE DE LA DIMENSION ET DES V./VA. PROPRES DE LA POLYADE SUP
!     POUR LE BLOC JSUP,ICSUP,ICPSUP
!
            READ(61,END=4161) JLUK,ICLUK,ICPLUK,NFBK
            DO WHILE( NFBK .GT. MXDIMS )
              CALL RESIZE_MXDIMS
            ENDDO
            READ(40,END=4140) JK1,ICK1,ICPK1,NK1
            IF( JK   .NE. JK1    .OR.              &
                ICK  .NE. ICK1   .OR.              &
                ICPK .NE. ICPK1       ) GOTO 4151
            IF( NFBK .EQ. 0           ) CYCLE E103
            IF( JK   .NE. JLUK   .OR.         &
                ICK  .NE. ICLUK  .OR.         &
                ICPK .NE. ICPLUK      ) THEN
              BACKSPACE(61)
              CYCLE E103
            ENDIF
            READ(61,END=4161) (HDK(I),I=1,NFBK)
            DO IK=1,NFBK
              READ(61,END=4161) NICK,ICKRO,(TK(IK,I),I=1,NFBK)
            ENDDO
!
! ******************************************************
! *** MATRICE DE TRANSITION DANS LES FONCTIONS DE BASE
! ******************************************************
!
! *** STOCKAGE DES E.M. DANS LES FONCTIONS DE BASE
!
E6:         DO JL=MAX(JK-NBR,0),JK+NBR
              READ(40,END=4140) JK1,ICK1,ICPK1,NK1,  &
                                JL1,ICL1,ICPL1,NL1
              ICL  = ICL1
              ICPL = ICPL1
              JCL  = (JL-JK+NBR)*MXSYM*2+(ICL-1)*2+ICPL
              IF( JL  .NE. JL1 ) GOTO 4151
              IF( NL1 .EQ. 0   ) CYCLE E6
              IDJ = JL-JK+NBR+1
!
! *** INITIALISATION DE DIP(IK1,IL1,IDG,IOP)
!
              DO IOP=1,NBOPT
                DO IS=1,MXDIMS
                  DO II=1,MXDIMI
                    DIP(IS,II,IDJ,IOP) = 0.D0
                  ENDDO
                ENDDO
              ENDDO
!
! *** BOUCLE SUR LES OPERATEURS
!
              SELECT CASE( NBR )
!
! *** MOMENT DIPOLAIRE
!
                CASE( 1 )
E301:             DO IOP=1,NBOPT
!
146                 READ(40,END=4140) JK1,ICK1,ICPK1,NK1,  &
                                      JL1,ICL1,ICPL1,NL1,  &
                                      IOP1,NBELM
                    DO WHILE( NBELM .GT. MXELMT )
                      CALL RESIZE_MXELMT
                    ENDDO
                    IF( JL    .EQ. JL1   .AND.             &
                        JK    .EQ. JK1   .AND.             &
                        ICK   .EQ. ICK1  .AND.             &
                        ICPK  .EQ. ICPK1 .AND.             &
                        IOP   .EQ. IOP1        ) GOTO 150
                    IF( NBELM .NE. 0           ) READ(40,END=4140)
                    GOTO 146
!
150                 IF( NBELM .EQ. 0 ) CYCLE E301
                    READ(40,END=4140) (LITR(I),KOTR(I),HTR(I),I=1,NBELM)
                    DO I=1,NBELM
                      IK1                  = KOTR(I)
                      IL1                  = LITR(I)
                      DIP(IK1,IL1,IDJ,IOP) = HTR(I)
                    ENDDO
                  ENDDO E301
!
! *** POLARISABILITE
!
                CASE( 2 )
E351:             DO IOP=1,NBOPT
!
246                 READ(40,END=4140) JK1,ICK1,ICPK1,NK1,  &
                                      JL1,ICL1,ICPL1,NL1,  &
                                      IOP1,NBELM,LG
                    DO WHILE( NBELM .GT. MXELMT )
                      CALL RESIZE_MXELMT
                    ENDDO
                    IF( JL    .EQ. JL1   .AND.             &
                        JK    .EQ. JK1   .AND.             &
                        ICK   .EQ. ICK1  .AND.             &
                        ICPK  .EQ. ICPK1 .AND.             &
                        IOP   .EQ. IOP1        ) GOTO 250
                    IF( NBELM .NE. 0           ) READ(40,END=4140)
                    GOTO 246
!
250                 IF( NBELM .EQ. 0 ) CYCLE E351
                    ICOEFI(IOP) = 1
                    IF( LG .EQ. 2 ) ICOEFI(IOP) = 2
                    READ(40,END=4140) (LITR(I),KOTR(I),HTR(I),I=1,NBELM)
                    DO I=1,NBELM
                      IK1                  = KOTR(I)
                      IL1                  = LITR(I)
                      DIP(IK1,IL1,IDJ,IOP) = HTR(I)
                    ENDDO
                  ENDDO E351
              END SELECT
!
! ******************************************************
! *** MATRICE DE TRANSITION DANS LES FONCTIONS PROPRES
! ******************************************************
!
! *** BOUCLE (O) P Q R (S) SUR LES NIV INF (L=INF)
!
              NFBL = JDIM(JCL)
              IF( NFBL .EQ. 0 ) CYCLE E6
              DO I=1,NFBL
                HDL(I) = HDLJ(I,JCL)
                DO J=1,NFBL
                  TL(J,I) = TLJ(J,I,JCL)
                ENDDO
              ENDDO
              IDJ = JL-JK+NBR+1
              WRITE(81) JK,ICK,ICPK,NFBK,  &
                        JL,ICL,ICPL,NFBL
!
! *** BOUCLE SUR LE NIVEAU SUPERIEUR (H=SUP)
!
              DO IK=1,NFBK
!
! *** BOUCLE SUR LE NIVEAU INFERIEUR.
!
                DO IL=1,NFBL
                  TM  = 0.D0
                  TMI = 0.D0
                  TMA = 0.D0
!
! *** BOUCLE SUR LES OPERATEURS
!
                  DO IOP=1,NBOPT
                    EMPRO = 0.D0
                    DO KL = 1,NFBL
                      A = 0.D0
                      DO KK = 1,NFBK
                        A = A+DIP(KK,KL,IDJ,IOP)*TK(KK,IK)
                      ENDDO
                      EMPRO = EMPRO+A*TL(KL,IL)
                    ENDDO
                    IF( MT .EQ. 'pol' ) THEN
                      IF( ICOEFI(IOP) .EQ. 1 ) THEN
                        TMI = TMI+EMPRO*PARAT(IOP)*COEFIS
                      ELSE
                        TMA = TMA+EMPRO*PARAT(IOP)*COEFAN
                      ENDIF
                    ELSE
                      TM = TM+EMPRO*PARAT(IOP)                                                     ! --> ELT. MATR. DU M.T.
                    ENDIF
                  ENDDO                                                                            ! BOUCLE SUR LES OPERATEURS
                  IF( MT .EQ. 'pol' ) THEN
                    TM = SQRT(TMI*TMI+TMA*TMA)
                  ENDIF
                  WRITE(81) IK,IL,TM
                ENDDO                                                                              ! BOUCLE SUR LE NIV. INF.
              ENDDO                                                                                ! BOUCLE SUR NIV. SUP. (NFB)
            ENDDO E6                                                                               ! BOUCLE PQR SUR NIV. INF.
          ENDDO E103                                                                               ! BOUCLE SUR NIV. SUP. (P)
        ENDDO                                                                                      ! BOUCLE SUR NIV. SUP. (C)
      ENDDO E3                                                                                     ! BOUCLE SUR NIV. SUP. (J)
      PRINT 1011, JK-1,JMAX
      CLOSE(80)
      CLOSE(81)
      GOTO 9000
!
4081  PRINT 8101, FSORT
      GOTO  9999
4060  PRINT 8102, FNINF
      GOTO  9999
4061  PRINT 8103, FNSUP
      GOTO  9999
4080  PRINT 8104, FPARA
      GOTO  9999
4040  PRINT 8105, FMTR
      GOTO  9999
4160  PRINT 8106
      GOTO  9999
4161  PRINT 8107
      GOTO  9999
4140  PRINT 8108
      GOTO  9999
4151  PRINT 8109
      GOTO  9999
9997  PRINT 8002
      CLOSE(10)
      GOTO  9999
9998  PRINT 8001
9999  PRINT 8000
!
9000  CLOSE(40)
      CLOSE(60)
      CLOSE(61)
      PRINT *
      END PROGRAM TRM
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!     LECTURE ET RECOPIE DES PARAMETRES
!
      SUBROUTINE IOPAB(LUI,LUO,IS)
      use mod_dppr
      use mod_par_tds
      use mod_main_trm
      IMPLICIT NONE
      integer          :: LUI,LUO,IS

      real(kind=dppr)  :: PARA,PREC

      integer          :: I,IP
      integer          :: MBOPT

      character(len = NBCLAB+10)  :: CHAINE
      character(len = NBCTIT)     :: TITRE
!
1000  FORMAT(A)
1001  FORMAT(I4,A)
1002  FORMAT(A,E18.11,E14.7)
8000  FORMAT(' !!! TRM    : STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF IN IOPAB')
!
      DO I=1,4
        READ(LUI,1000,END=2000)     TITRE
        IF( LUO .NE. 0 ) WRITE(LUO) TITRE
      ENDDO
      READ(LUI,1001,END=2000)     MBOPT,TITRE
      IF( LUO .NE. 0 ) WRITE(LUO) MBOPT,TITRE
      DO WHILE( MBOPT .GT. MXOPT )
        CALL RESIZE_MXOPT
      ENDDO
      DO I=1,2
        READ(LUI,1000,END=2000)     TITRE
        IF( LUO .NE. 0 ) WRITE(LUO) TITRE
      ENDDO
      DO IP=1,MBOPT
        READ(LUI,1002,END=2000)     CHAINE,PARA,PREC
        IF( LUO .NE. 0 ) WRITE(LUO) CHAINE,PARA,PREC
        IF( IS  .EQ. 1 ) PARAT(IP) = PARA
      ENDDO
      IF( IS .EQ. 1 ) NBOPT = MBOPT
      RETURN
!
2000  PRINT 8003
      PRINT 8000
      PRINT *
      STOP
!
      END SUBROUTINE IOPAB
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!     LECTURE ET RECOPIE DES PARAMETRES
!
      SUBROUTINE IOPBA0(LUI,LUO)
      use mod_dppr
      use mod_par_tds
      IMPLICIT NONE
      integer          :: LUI,LUO

      real(kind=dppr)  :: PARA,PREC

      integer          :: I,IP
      integer          :: NBOPH

      character(len = NBCLAB+10)  :: CHAINE
      character(len = NBCTIT)     :: TITRE
!
1000  FORMAT(A)
1001  FORMAT(I4,A)
1002  FORMAT(A,E18.11,E14.7)
8000  FORMAT(' !!! TRM    : STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF IN IOPBA0')
!
      DO I=1,4
        READ(LUI,END=2000)               TITRE
        IF( LUO .NE. 0 ) WRITE(LUO,1000) TITRE
      ENDDO
      READ(LUI,END=2000)               NBOPH,TITRE
      IF( LUO .NE. 0 ) WRITE(LUO,1001) NBOPH,TITRE
      DO I=1,2
        READ(LUI,END=2000)               TITRE
        IF( LUO .NE. 0 ) WRITE(LUO,1000) TITRE
      ENDDO
      DO IP=1,NBOPH
        READ(LUI,END=2000)               CHAINE,PARA,PREC
        IF( LUO .NE. 0 ) WRITE(LUO,1002) CHAINE,PARA,PREC
      ENDDO
      RETURN
!
2000  PRINT 8003
      PRINT 8000
      PRINT *
      STOP
!
      END SUBROUTINE IOPBA0
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!     LECTURE ET RECOPIE DES PARAMETRES
!
      SUBROUTINE IOPBB0(LUI,LUO)
      use mod_dppr
      use mod_par_tds
      IMPLICIT NONE
      integer          :: LUI,LUO

      real(kind=dppr)  :: PARA,PREC

      integer          :: I,IP
      integer          :: NBOPH

      character(len = NBCLAB+10)  :: CHAINE
      character(len = NBCTIT)     :: TITRE
!
8000  FORMAT(' !!! TRM    : STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF IN IOPBB0')
!
      DO I=1,4
        READ(LUI,END=2000)          TITRE
        IF( LUO .NE. 0 ) WRITE(LUO) TITRE
      ENDDO
      READ(LUI,END=2000)          NBOPH,TITRE
      IF( LUO .NE. 0 ) WRITE(LUO) NBOPH,TITRE
      DO I=1,2
        READ(LUI,END=2000)          TITRE
        IF( LUO .NE. 0 ) WRITE(LUO) TITRE
      ENDDO
      DO IP=1,NBOPH
        READ(LUI,END=2000)          CHAINE,PARA,PREC
        IF( LUO .NE. 0 ) WRITE(LUO) CHAINE,PARA,PREC
      ENDDO
      RETURN
!
2000  PRINT 8003
      PRINT 8000
      PRINT *
      STOP
!
      END SUBROUTINE IOPBB0
