      PROGRAM ASDI
!
! REV    SEPT 2006  CW
!
!  lire un fichier binaire (ex. INFILE) de type DI_
!  et l'ecrire en ASCII dans INFILE'_ASC'
!
! APPEL : asdi
!
      use mod_par_tds
      use mod_main_dipmat
      IMPLICIT NONE

      integer          :: I,ICI,ICS,ICSC,IOP,IOPC,ISVI,ISVS
      integer          :: ICPI,ICPS,ICPSC
      integer          :: JI,JIC,JS
      integer          :: NBOTR,NFBI,NFBS,NSVI,NSVS

      character(len = NBCTIT)  :: IDENT,TITRE
      character(len =  40)  :: IDEMR
      character(len = 150)  :: INFILE
      character(len = 160)  :: OUTFILE
!
1000  FORMAT(A)
1020  FORMAT(/,            &
             'ASDI   : ')
2000  FORMAT(' <  ',A)
2001  FORMAT(' >> ',A)
7000  FORMAT('ENTER DI_ TYPE FILE NAME :')
8000  FORMAT(' !!! ASDI  : STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF FOR ',A)
!
! ARRAY INITIAL ALLOCATION
!
      CALL ALLOC_ALL
!
      PRINT 1020
!
!  FICHIER D'ENTREE
!
      PRINT 7000
      READ(*,1000) INFILE
      PRINT 2000,  TRIM(INFILE)
      OPEN(40,FILE=TRIM(INFILE),FORM='UNFORMATTED',STATUS='OLD')
!
!  FICHIER DE SORTIE
!
      OUTFILE = TRIM(INFILE)//'_ASC'
      PRINT 2001,  TRIM(OUTFILE)
      OPEN(10,FILE=TRIM(OUTFILE))
!
! LECTURE DES CARACTERISTIQUES GENERALES
!
      DO I=1,3
        READ (40,END=9003) TITRE
        WRITE(10,*)        TITRE
      ENDDO
      READ (40,END=9003) IDEMR
      WRITE(10,*)        IDEMR
      DO I=1,3
        READ (40,END=9003) TITRE
        WRITE(10,*)        TITRE
      ENDDO
!
5     READ(40,END=9003) TITRE
      IF( TITRE(5:13) .NE. 'ER LEVEL ' ) THEN
        BACKSPACE(40)
        GOTO 4
      ENDIF
      WRITE(10,*)       TITRE
      GOTO 5
!
4     READ (40,END=9003) TITRE
      WRITE(10,*)        TITRE
!
!  CARACTERISTIQUES DE LA POLYADE SUPERIEURE
!
      WRITE(10,*) '>>> NSVS'
      READ (40,END=9003) NSVS,TITRE
      WRITE(10,*)        NSVS,TITRE
      DO ISVS=1,NSVS
        READ (40,END=9003) IDENT(:96)
        WRITE(10,*)        IDENT(:96)
      ENDDO
!
!  CARACTERISTIQUES DE LA POLYADE INFERIEURE
!
      WRITE(10,*) '>>> NSVI'
      READ (40,END=9003) NSVI,TITRE
      WRITE(10,*)        NSVI,TITRE
      DO ISVI=1,NSVI
        READ (40,END=9003) IDENT(:96)
        WRITE(10,*)        IDENT(:96)
      ENDDO
!
!  ORDRE DU DEVELOPPEMENT
!
      DO I=1,3
        READ (40,END=9003) TITRE
        WRITE(10,*)        TITRE
      ENDDO
      READ (40,END=9003) TITRE(:67)
      WRITE(10,*)        TITRE(:67)
      WRITE(10,*) '>>> NBOTR'
      READ (40,END=9003) NBOTR,TITRE(:24)
      WRITE(10,*)        NBOTR,TITRE(:24)
      WRITE(10,*) '>>> JS,ICS,ICPS,NFBS'
      WRITE(10,*) '>>> JS,ICS,ICPS,NFBS,JI,ICI,ICPI,NFBI'
      WRITE(10,*) '>>> JS,ICS,ICPS,NFBS,JI,ICI,ICPI,NFBI,IOP,NBELM'
      WRITE(10,*) '>>> (LI(I),KO(I),H(I),I=1,NBELM)'
!
400   DO ICSC=1,MXSYM
E503:   DO ICPSC=1,2
          READ (40,END=9000) JS,ICS,ICPS,NFBS
          WRITE(10,*)        JS,ICS,ICPS,NFBS
          IF( NFBS .EQ. 0 ) CYCLE E503
E406:     DO JIC=MAX(JS-1,0),JS+1
            READ (40,END=9003) JS,ICS,ICPS,NFBS,JI,ICI,ICPI,NFBI
            WRITE(10,*)        JS,ICS,ICPS,NFBS,JI,ICI,ICPI,NFBI
            IF( NFBI .EQ. 0 ) CYCLE E406
E414:       DO IOPC=1,NBOTR
              READ (40,END=9003) JS,ICS,ICPS,NFBS,  &
                                 JI,ICI,ICPI,NFBI,  &
                                 IOP,NBELM
              WRITE(10,*)        JS,ICS,ICPS,NFBS,  &
                                 JI,ICI,ICPI,NFBI,  &
                                 IOP,NBELM
              DO WHILE( NBELM .GT. MXELMT )
                CALL RESIZE_MXELMT
              ENDDO
              IF( NBELM .EQ. 0 ) CYCLE E414
              READ (40,END=9003) (LI(I),KO(I),H(I),I=1,NBELM)
              WRITE(10,*)        (LI(I),KO(I),H(I),I=1,NBELM)
            ENDDO E414
          ENDDO E406
        ENDDO E503
      ENDDO
      GOTO 400
!
9003  PRINT 8003, TRIM(INFILE)
      PRINT 8000
!
9000  CLOSE(10)
      PRINT *
      END PROGRAM ASDI
