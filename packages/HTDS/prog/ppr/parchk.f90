      PROGRAM PARCHK
!
!  VALIDER UN FICHIER DE PARAMETRE sans [ou avec] para d'intensite
!  A PARTIR DE MH_xxx [et MD_xxx ou MP_xxx]
!
!  DEC 2004 CW VB
!  AVR 2010 CW VB test de H ou T, surnombre de paras possible
!
! APPEL : parchk Pnsup Dksup [Pninf Dktrm dip|pol]     ParaMolDir ParaFileName
!
!
      use mod_dppr
      use mod_par_tds
      use mod_com_fdate
      IMPLICIT NONE

      real(kind=dppr) ,dimension(MXSYM+1)  :: SS,SSC
      real(kind=dppr) ,dimension(NBVQN)    :: FREC,FREQ

      integer ,dimension(NBVQN)  :: IVA,IVC
      integer          :: I,III
      integer          :: J
      integer          :: NBOPH,NBOPHC,NBOPT,NBOPTC,NBP
      integer          :: NCL1,NSVQN

      character(len = NBCTIT)  :: TITRE,TITREC
      character(len =  11) ,dimension(5)      :: CARG
      character(len =   3)  :: OPTION
      character(len =   3)  :: PI,PS
      character(len =  12)  :: DS,DT
      character(len = 100)  :: MOLDIR
      character(len = 120)  :: FEMRV,FPARA

      logical          :: LINT
!
1000  FORMAT(A)
1001  FORMAT(I4,A)
1002  FORMAT( 6I1)                                                                                 ! 6  = NBVQN
1003  FORMAT(1X,I1,1A)                                                                             ! 1A = NBAM, cf. FORMAT 1003 DE PARMK
1010  FORMAT(/,              &
             'PARCHK : ',A)
2000  FORMAT(' <  ',A)
8000  FORMAT(' !!! PARCHK : STOP ON ERROR')
8001  FORMAT(' !!! ERROR OPENING CONTROL FILE')
8002  FORMAT(' !!! INCOMPLETE CONTROL FILE')
8003  FORMAT(' !!! ERROR OPENING VIBRATIONAL REDUCED MATRIX ELEMENTS FILE (HAMILTONIAN)')
8004  FORMAT(' !!! ERROR OPENING SKELETON PARAMETER FILE')
8005  FORMAT(' !!! ERROR OPENING CHECKED PARAMETER FILE')
8006  FORMAT(' !!! ERROR READING VIBRATIONAL REDUCED MATRIX ELEMENTS FILE (HAMILTONIAN)')
8007  FORMAT(' !!! NEITHER dip NOR pol')
8008  FORMAT(' !!! ERROR READING SKELETON PARAMETER FILE')
8009  FORMAT(' !!! ERROR READING CHECKED PARAMETER FILE')
8010  FORMAT(' !!! BAD NUMBER OF LINES IN CONTROL FILE')
8011  FORMAT(' !!! ERROR OPENING VIBRATIONAL REDUCED MATRIX ELEMENTS FILE (TRANSITION MOMENT)')
8012  FORMAT(' !!! ERROR READING VIBRATIONAL REDUCED MATRIX ELEMENTS FILE (TRANSITION MOMENT)')
8013  FORMAT(' !!! INVALID SPIN VALUE IN PARAMETER FILE (LINE #2)')
8014  FORMAT(' !!! INVALID FREQUENCY VALUE IN PARAMETER FILE (LINE #4)')
8015  FORMAT(' !!! INVALID NUMBER OF HAMILTONIAN OPERATORS IN PARAMETER FILE (LINE #5)')
8016  FORMAT(' !!! INVALID OPERATOR DESCRIPTOR IN PARAMETER FILE (LINE #',I4,')')
8017  FORMAT(' !!! INVALID NUMBER OF TRANSITION MOMENT OPERATORS IN PARAMETER FILE (LINE #',I4,')')
8019  FORMAT(' !!! INVALID CODE FOR INTENSITY UNITS IN PARAMETER FILE (LINE #',I4,')',/,   &
             A                                                                          )
!
      NCL1 = NBCLAB+1+NBAM+1                                                                       ! Hmn  included
      OPEN(10,FILE=CTRLF,ERR=9998,STATUS='OLD')
      NBP = 0                                                                                      ! nb total de paras
!
6     READ(10,1000,END=7)
      NBP = NBP+1
      GOTO 6
!
7     REWIND(10)
      NBP = NBP-3                                                                                  ! nb reduit de paras
      IF    ( NBP .EQ. 2 ) THEN
        LINT = .FALSE.                                                                             ! tester uniquement paras H
      ELSEIF( NBP .EQ. 5 ) THEN
        LINT = .TRUE.                                                                              ! tester uniquement paras T
      ELSE
        PRINT 8010
        GOTO  9999
      ENDIF
      READ(10,1000,END=9997) FDATE
      PRINT 1010,            FDATE
      DO I=1,NBP
        READ(10,1000,END=9997) CARG(I)
      ENDDO
      READ(10,1000,END=9997) MOLDIR
      READ(10,1000,END=9997) FPARA
      CLOSE(10)
      PS = TRIM(CARG(1))                                                                           ! upper polyad number
      DS = CARG(2)                                                                                 ! upper polyad development order
      IF( LINT ) THEN                                                                              ! lower polyad characteristics
        PI     = TRIM(CARG(3))
        DT     = CARG(4)
        OPTION = TRIM(CARG(5))
      ENDIF
      FEMRV = 'MH_'//TRIM(PS)//'_'//DS
      PRINT 2000,  TRIM(FEMRV)
      OPEN(11,FILE=TRIM(FEMRV),ERR=9996,STATUS='OLD')
!
!  VALIDATION PAR L'ENTETE DE Pa_skel
!
      PRINT 2000,  TRIM(MOLDIR)//'/Pa_skel'
      OPEN(12,FILE=TRIM(MOLDIR)//'/Pa_skel',ERR=9995,STATUS='OLD')
      PRINT 2000,  TRIM(FPARA)
      OPEN(13,FILE=TRIM(FPARA),ERR=9994,STATUS='OLD')
      READ(12,1000,ERR=9992)
      READ(13,1000,ERR=9991)
      READ(12,*,ERR=9992) SS
      READ(13,*,ERR=9991) SSC
      DO I=1,MXSYM+1                                                                               ! TEST DES SPIN
        IF( SS(I) .NE. SSC(I) ) THEN
          PRINT *, 'Expected : ',SS
          PRINT *, 'Found    : ',SSC
          PRINT 8013
        ENDIF
      ENDDO
      READ(12,1000,ERR=9992)
      READ(13,1000,ERR=9991)
      READ(12,*,ERR=9992) FREQ
      READ(13,*,ERR=9991) FREC
      DO I=1,NBVQN                                                                                 ! TEST DES FREQUENCES
        IF( FREQ(I) .NE. FREC(I) ) THEN
          PRINT *, 'Expected : ',FREQ
          PRINT *, 'Found    : ',FREC
          PRINT 8014
        ENDIF
      ENDDO
      CLOSE(12)
!
!  PARAMETRES H
!
3     READ(11,1000,ERR=9993) TITRE                                                                 ! CHERCHER NBOPH
      IF( TITRE(5:28) .NE. ' ROVIBRATIONAL OPERATORS' ) GOTO 3                                     ! FORMAT 1025 DE HMODEL
      READ(TITRE(:4),1001) NBOPH
      READ(13,1001,ERR=9991) NBOPHC
      IF( .NOT. LINT ) THEN                                                                        ! validation des paras H
        IF( NBOPH .GT. NBOPHC ) THEN                                                               ! TEST DE NBOPH
          PRINT *, 'Expected : ',NBOPH
          PRINT *, 'Found    : ',NBOPHC
          PRINT 8015
          GOTO  9999
        ENDIF
        READ(11,1000,ERR=9993)
        DO I=1,2
          READ(13,1000,ERR=9991)
        ENDDO
        DO I=1,NBOPH
          READ(11,1000,ERR=9993) TITRE
          READ(TITRE(17:17+NBVQN-1),1002) IVC                                                      ! FORMAT 1313 DE HMODEL
          READ(TITRE(27:27+NBVQN-1),1002) IVA                                                      ! FORMAT 1313 DE HMODEL
          NSVQN = 0
          DO J=1,NBVQN
            NSVQN = NSVQN+IVC(J)+IVA(J)
          ENDDO
          WRITE(TITRE(NCL1-1-NBAM:NCL1),1003) NSVQN,TITRE(7:7)
          READ(13,1000,ERR=9991) TITREC
          IF( TITRE(:NCL1) .NE. TITREC(:NCL1) ) THEN                                               ! TEST DE LA DESCRIPTION
            PRINT *, 'Expected : ',TITRE(:NCL1)
            PRINT *, 'Found    : ',TITREC(:NCL1)
            PRINT 8016, 7+I
          ENDIF
        ENDDO
        CLOSE(11)
        CLOSE(13)
        GOTO 9000                                                                                  ! ne pas tester les paras T
      ENDIF
!
! PARAS T
!
! positionnement apres les paras H
!
      DO I=1,NBOPH+1
        READ(11,1000,ERR=9993)
      ENDDO
      DO I=1,NBOPHC+2
        READ(13,1000,ERR=9991)
      ENDDO
      IF    ( OPTION .EQ. 'dip' ) THEN
        FEMRV = 'MD_'//TRIM(PS)//'m'//TRIM(PI)//'_'//DT
      ELSEIF( OPTION .EQ. 'pol' ) THEN
        FEMRV = 'MP_'//TRIM(PS)//'m'//TRIM(PI)//'_'//DT
      ELSE
        PRINT 8007
        GOTO  9999
      ENDIF
      PRINT 2000,  TRIM(FEMRV)
      OPEN(11,FILE=TRIM(FEMRV),ERR=9990,STATUS='OLD')
!
! VALIDATION DES PARAS T
!
      DO I=1,3
        READ(13,1000,ERR=9991)
      ENDDO
      READ(13,1000,ERR=9991) TITRE
      READ(TITRE(:4),1001,ERR=9988) III                                                            ! code unites d'intensites
!
9     READ(11,1000,ERR=9989) TITRE                                                                 ! CHERCHER NBOPT
      IF( TITRE(5:28) .NE. ' ROVIBRATIONAL OPERATORS' ) GOTO 9                                     ! FORMAT 1220 DIPMOD POLMOD
      READ(TITRE(:4),1001) NBOPT
      READ(13,1001,ERR=9991) NBOPTC
      IF( NBOPT .GT. NBOPTC ) THEN                                                                 ! TEST DE NBOPT
        PRINT *, 'Expected : ',NBOPT
        PRINT *, 'Found    : ',NBOPTC
        PRINT 8017, 7+NBOPHC+5
        GOTO  9999
      ENDIF
      READ(11,1000,ERR=9989)
      DO I=1,2
        READ(13,1000,ERR=9991)
      ENDDO
      DO I=1,NBOPT
        READ(11,1000,ERR=9989) TITRE
        READ(TITRE(17:17+NBVQN-1),1002) IVC                                                        ! FORMAT 1313 DIPMOD POLMOD
        READ(TITRE(27:27+NBVQN-1),1002) IVA                                                        ! FORMAT 1313 DIPMOD POLMOD
        NSVQN = 0
        DO J=1,NBVQN
          NSVQN = NSVQN+IVC(J)+IVA(J)
        ENDDO
        WRITE(TITRE(NCL1-1-NBAM:NCL1),1003) NSVQN,TITRE(7:7)
        READ(13,1000,ERR=9991) TITREC
        IF( TITRE(:NCL1) .NE. TITREC(:NCL1) ) THEN                                                 ! TEST DE LA DESCRIPTION
          PRINT *, 'Expected : ',TITRE(:NCL1)
          PRINT *, 'Found    : ',TITREC(:NCL1)
          PRINT 8016, 7+NBOPH+7+I
        ENDIF
      ENDDO
      CLOSE(11)
      CLOSE(13)
      GOTO 9000
!
9988  PRINT 8019, 7+NBOPHC+4,TRIM(TITRE)
      GOTO  9999
9989  PRINT 8012
      GOTO  9999
9990  PRINT 8011
      GOTO  9999
9991  PRINT 8009
      GOTO  9999
9992  PRINT 8008
      GOTO  9999
9993  PRINT 8006
      GOTO  9999
9994  PRINT 8005
      GOTO  9999
9995  PRINT 8004
      GOTO  9999
9996  PRINT 8003
      GOTO  9999
9997  PRINT 8002
      CLOSE(10)
      GOTO  9999
9998  PRINT 8001
9999  PRINT 8000
!
9000  PRINT *
      END PROGRAM PARCHK
