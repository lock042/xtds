      PROGRAM TRA
!
!
!  MODIFIE LE 31.01.89 MODIFIE JPC 11.02.91 REV 8 DEC 93
! REV    JAN 1995 JPC,CW (PARAMETER)
! MODIFIE 03/1998 V. BOUDON ---> XY6/Oh.
!
! ###########################################################################
! ##                                                                       ##
! ##          CALCUL DE SPECTRE : FREQUENCE ET MOMENTS DE TRANSITION       ##
! ##                                                                       ##
! ###########################################################################
!
! APPEL : tra  Pn Pn' Dp Jmax [pol] [MHz/GHz] [range FMIN FMAX]
!
!  LES OPTIONS PEUVENT ETRE INDIQUEES DANS UN ORDRE QUELCONQUE .
!
! *** LIMITATIONS DU PROGRAMME
!
! NOMBRE MAXIMUM DE NIVEAUX D'ENERGIE J,C,N
!     MXENI                ! JC,JCENTI,ENINF
!
! NOMBRE MAXIMUM DE SYMETRIES ROVIBRATIONNELLES
!     MXSYM                ! SYM
!
      use mod_dppr
      use mod_par_tds
      use mod_com_const
      use mod_com_fdate
      use mod_com_sy
      use mod_main_tra
      IMPLICIT NONE

      real(kind=dppr)  :: A2
      real(kind=dppr)  :: CMGHZ
      real(kind=dppr)  :: ENSUP
      real(kind=dppr)  :: F,FMAX,FMIN
      real(kind=dppr)  :: TM

      integer          :: I,IBS,IC,ICI,ICINF,ICOEFU,ICS,ICSUP,II,IM
      integer          :: ICPI,ICPINF,ICPS,ICPSUP
      integer          :: IN,INF,IP,IPCINF,IPCSUP,IRA,IS,ISV
      integer          :: JCC,JCENT1,JI,JIC,JIICI,JINF,JMAX,JS
      integer          :: JSICS,JSUP
      integer          :: LUINF
      integer          :: N,NBOTR,NFBI,NFBINF,NFBS,NFBSUP,NINF,NSUP
      integer          :: NSVI,NSVINF,NSVS,NSVSUP,NTOTI,NUSVI,NUSVS

      character(len = NBCTIT)                :: TITRE
      character(len =  11)    ,dimension(3)  :: CARG
      character(len =  60)                   :: OPT
      character(len = 120)                   :: FEDI,FEDS,FEMMTD

      logical           :: INTRA = .FALSE.
!
1001  FORMAT(A)
1020  FORMAT(/,              &
             'TRA    : ',A)
2000  FORMAT(' <  ',A)
2001  FORMAT(' >> ',A)
8000  FORMAT(' !!! TRA    : STOP ON ERROR')
8001  FORMAT(' !!! ERROR OPENING CONTROL FILE')
8002  FORMAT(' !!! INCOMPLETE CONTROL FILE')
8101  FORMAT(' !!! LOWER LEVEL OF ',I3,1X,2A,'-',I3,1X,2A,' TRANSITION NOT FOUND IN:',/,   &
             A                                                                          )
8102  FORMAT(' !!! ERROR OPENING trans.t FILE')
8103  FORMAT(' !!! ERROR OPENING LOWER EIGENVAL. AND VECT. FILE: ',A)
8104  FORMAT(' !!! ERROR OPENING UPPER EIGENVAL. AND VECT. FILE: ',A)
8105  FORMAT(' !!! ERROR OPENING DIPOLE MOMENT FILE: ',A)
8106  FORMAT(' !!! LOWER LEVEL FILE => UNEXPECTED EOF')
8107  FORMAT(' !!! UPPER LEVEL FILE => UNEXPECTED EOF')
8108  FORMAT(' !!! DIPOLE MOMENT FILE => UNEXPECTED EOF')
8109  FORMAT(' !!! INCOMPATIBLE SUBLEVEL NUMBERS')
8110  FORMAT(' !!! UPPER LEVEL OF ',I3,1X,2A,'-',I3,1X,2A,' TRANSITION NOT FOUND IN:',/,   &
             A                                                                          )
8111  FORMAT(' !!! INCONSISTENT NUMBER OF LEVELS (',2I4,')'                    ,/,   &
             ' !!! FOR UPPER LEVEL OF ',I3,1X,2A,'-',I3,1X,2A,' TRANSITION IN:',/,   &
             A                                                                 ,/,   &
             'AND ',I3,1X,2A,' IN:'                                            ,/,   &
             A                                                                    )
8112  FORMAT(' !!! INCONSISTENT NUMBER OF LEVELS (',2I4,')'                    ,/,   &
             ' !!! FOR LOWER LEVEL OF ',I3,1X,2A,'-',I3,1X,2A,' TRANSITION IN:',/,   &
             A                                                                 ,/,   &
             'AND ',I3,1X,2A,' IN:'                                            ,/,   &
             A                                                                    )
!
! ARRAY INITIAL ALLOCATION
!
      CALL ALLOC_ALL
!
      IM = 0                                                                                       ! INITIALISATION PAR DEFAUT
      OPEN(10,FILE=CTRLF,ERR=9998,STATUS='OLD')
      READ(10,1001,END=9997) FDATE
      PRINT 1020,            FDATE
      DO I=1,3                                                                                     ! ARGUMENTS OBLIGATOIRES
        READ(10,1001,END=9997) CARG(I)
      ENDDO
      READ(10,*,END=9997) JMAX
!
!      ---> IRA   =1 SI IR ; IRA=0 SI RAMAN ISOTROPE
!      ---> ICOEFU=1 CONVERTIT LES DONNEES DE MHZ EN CM-1
!      ---> ICOEFU=2 CONVERTIT LES DONNEES DE GHZ EN CM-1
!
      IRA    = 1
      ICOEFU = 0
      FMIN   = 0.D0
      FMAX   = 1.D+10
!
19    READ(10,1001,END=21) OPT
      IF    ( OPT .EQ. 'pol'   ) THEN
        IRA = 0
      ELSEIF( OPT .EQ. 'MHz'   ) THEN
        IF( ICOEFU .NE. 0 ) GOTO 9997
        ICOEFU = 1
      ELSEIF( OPT .EQ. 'GHz'   ) THEN
        IF( ICOEFU .NE. 0 ) GOTO 9997
        ICOEFU = 2
      ELSEIF( OPT .EQ. 'range' ) THEN
        READ(10,*,END=9997) FMIN
        READ(10,*,END=9997) FMAX
      ELSE
        GOTO 9997
      ENDIF
      GOTO 19
!
21    CLOSE(10)
      FEDS = 'EN_'//TRIM(CARG(1))//'_'
      FEDI = 'EN_'//TRIM(CARG(2))//'_'
      IF( FEDS .EQ. FEDI ) INTRA = .TRUE.
      FEMMTD = 'TR_'//TRIM(CARG(1))//'m'//TRIM(CARG(2))//'_'//TRIM(CARG(3))//'_'
      IF( IRA .EQ. 0 ) THEN
        FEMMTD = 'TP_'//TRIM(CARG(1))//'m'//TRIM(CARG(2))//'_'//TRIM(CARG(3))//'_'
      ENDIF
      PRINT 2000, TRIM(FEDS)
      PRINT 2000, TRIM(FEDI)
      PRINT 2000, TRIM(FEMMTD)
      PRINT 2001, 'trans.t'
!
!   FICHIER D'ENERGIES SUP
!
      OPEN(50,ERR=4050,FILE=FEDS,STATUS='OLD',FORM='UNFORMATTED')
!
!   FICHIER D'ENERGIES INF
!
      IF( .NOT. INTRA ) THEN
        OPEN(51,ERR=4051,FILE=FEDI,STATUS='OLD',FORM='UNFORMATTED')
      ENDIF
!
!   FICHIER D'ELEMENTS MATRICIELS M.T. & DERIVEES
!
      OPEN(81,ERR=4081,FILE=FEMMTD,STATUS='OLD',FORM='UNFORMATTED')
!
!    FICHIER DE SORTIE
!
      OPEN(91,ERR=4091,FILE='trans.t',FORM='UNFORMATTED',STATUS='UNKNOWN')
!
      CMGHZ = 1.D0
      IF( ICOEFU .EQ. 1 ) CMGHZ = CMHZ
      IF( ICOEFU .EQ. 2 ) CMGHZ = CGHZ
!
!    LECTURE DES PARAMETRES DE H
!
      CALL IOPBB0(50,91)
      CALL IOPBB0(81,0)
!
!    LECTURE DES PARAMETRES DU M.D. ET STOCKAGE EN MEMOIRE
!
      CALL IOPBB0(81,91)
!
!   NSVSUP SOUS-NIVEAUX VIBRATIONNELS SUP
!
      READ (50) NSVSUP,TITRE
      WRITE(91) NSVSUP
      DO WHILE( NSVSUP .GT. MXSNV )
        CALL RESIZE_MXSNV
      ENDDO
      DO ISV=1,NSVSUP
        READ (50) TITRE(:96)
        WRITE(91) TITRE(:96)
      ENDDO
!
!    NSVINF SOUS-NIVEAUX VIBRATIONNELS INF
!
      IF( INTRA ) THEN
        LUINF = 50
        DO I=1,NSVSUP+1
          BACKSPACE(LUINF)
        ENDDO
      ELSE
        LUINF = 51
        CALL IOPBB0(LUINF,0)
      ENDIF
      READ (LUINF) NSVINF,TITRE
      WRITE(91)    NSVINF
      DO WHILE( NSVINF .GT. MXSNV )
        CALL RESIZE_MXSNV
      ENDDO
      DO ISV=1,NSVINF
        READ (LUINF) TITRE(:96)
        WRITE(91)    TITRE(:96)
      ENDDO
      READ(81) NSVS,TITRE
      IF( NSVS .NE. NSVSUP ) GOTO 9996
      DO ISV=1,NSVSUP
        READ(81) TITRE(:96)
      ENDDO
      READ(81) NSVI,TITRE
      IF( NSVI .NE. NSVINF ) GOTO 9996
      DO ISV=1,NSVINF
        READ(81) TITRE(:96)
      ENDDO
!
!    NBOPHS : NOMBRE D'OPERATEURS DU NIVEAU SUP
!
      READ(50)
!
!    NBOPHI : NOMBRE D'OPERATEURS DU NIVEAU INF
!
      IF( .NOT. INTRA ) READ(LUINF)
!
!    NBOTR : NOMBRE D'OPERATEURS DE TRANSITION
!
      READ(81)  NBOTR
      WRITE(91) JMAX
      WRITE(91) IRA
!
! ******************************************************
! *** LECTURE ET STOCKAGE DU FICHIER D'ENERGIES INF  ***
! ******************************************************
!
      INF = 0
!
6     READ(LUINF,END=8) JINF,ICINF,ICPINF,NFBINF
      JIC = JINF*100+ICINF*10+ICPINF
      DO IP=1,NFBINF
        INF = INF+1
        IF( INF .GT. MXENI ) CALL RESIZE_MXENI
        JC(INF)  = JIC
        NFB(INF) = NFBINF
        READ(LUINF,END=4160) ENINF(INF),(ICENT(ISV),ISV=1,NSVINF)
        JCENT1 = 0
E9:     DO IC=1,NSVINF
          IF( JCENT1 .GT. ICENT(IC) ) CYCLE E9
          JCENT1 = ICENT(IC)
          IM     = IC
        ENDDO E9
        JCENTI(INF) = IM+10*JCENT1
      ENDDO
      GOTO 6
!
8     NTOTI = INF
      CALL DEBUG( 'TRA    => MXENI=',INF)
      IF( INTRA ) THEN
        REWIND(LUINF)
        CALL IOPBB0(LUINF,0)
        DO I=1,NSVSUP+2
          READ(LUINF)
        ENDDO
      ELSE
        CLOSE(LUINF)
      ENDIF
!
! **********************************
! *** RECHERCHE DES TRANSITIONS  ***
! **********************************
!
!    INDICES DE LA TRANSITION
!
11    READ(81,END=9000) JS,ICS,ICPS,NFBS,  &
                        JI,ICI,ICPI,NFBI
      IF( JS .GT. JMAX ) GOTO 9000
      JSICS = JS*100+ICS*10+ICPS
      JIICI = JI*100+ICI*10+ICPI
!
!    RECHERCHE DU NIVEAU SUP
!
12    READ(50,END=112) JSUP,ICSUP,ICPSUP,NFBSUP
      JCC = JSUP*100+ICSUP*10+ICPSUP
      IF( JCC .EQ. JSICS ) GOTO 14
      IF( JCC .GT. JSICS ) GOTO 112
      DO I=1,NFBSUP
        READ(50,END=4161)
      ENDDO
      GOTO 12
!
112   PRINT 8110, JS,SYM(ICS),PAR(ICPS),  &
                  JI,SYM(ICI),PAR(ICPI),  &
                  TRIM(FEDS)
      GOTO  9999
!
14    IF( NFBS .NE. NFBSUP ) THEN
        PRINT 8111, NFBS,NFBSUP,                  &
                    JS,SYM(ICS),PAR(ICPS),        &
                    JI,SYM(ICI),PAR(ICPI),        &
                    TRIM(FEMMTD),                 &
                    JSUP,SYM(ICSUP),PAR(ICPSUP),  &
                    TRIM(FEDS)
        GOTO  9999
      ENDIF
!
!    RECHERCHE DU NIVEAU INF
!
      DO IN=1,NTOTI
        IF( JC(IN) .EQ. JIICI ) GOTO 16
        IF( JC(IN) .GT. JIICI ) GOTO 115
      ENDDO
!
115   PRINT 8101, JS,SYM(ICS),PAR(ICPS),  &
                  JI,SYM(ICI),PAR(ICPI),  &
                  TRIM(FEDI)
      GOTO  9999
!
16    IF( NFBI .NE. NFB(IN) ) THEN
        PRINT 8112, NFBI,NFB(IN),                 &
                    JS,SYM(ICS),PAR(ICPS),        &
                    JI,SYM(ICI),PAR(ICPI),        &
                    TRIM(FEMMTD),                 &
                    JINF,SYM(ICINF),PAR(ICPINF),  &
                    TRIM(FEDI)
      ENDIF
      DO IS=1,NFBS
        READ(50,END=4161) ENSUP,(ICENT(ISV),ISV=1,NSVSUP)
E17:    DO II=1,NFBI
          READ(81,END=4150) NSUP,NINF,TM
          IF( TM .EQ. 0.D0 ) CYCLE E17
          IPCSUP = 0
          NUSVS  = 0
E22:      DO IC=1,NSVSUP
            IF( IPCSUP .GT. ICENT(IC) ) CYCLE E22
            IPCSUP = ICENT(IC)
            NUSVS  = IC
          ENDDO E22
!
!    NOMBRE D'ONDE
!
          N = IN+II-1
          F = ENSUP-ENINF(N)
          F = F*CMGHZ
          IF( F .LE. FMIN .OR.              &
              F .GE. FMAX      ) CYCLE E17
          IPCINF = JCENTI(N)/10
          NUSVI  = JCENTI(N)-10*IPCINF
!
!    MOMENT DE TRANSITION
!
          A2 = TM*TM
          WRITE(91) JI,ICI,ICPI,NINF,NUSVI,IPCINF,  &
                    ENINF(N),                       &
                    JS,ICS,ICPS,NSUP,NUSVS,IPCSUP,  &
                    F,A2
        ENDDO E17
      ENDDO
      DO IBS=1,NFBS+1
        BACKSPACE(50)
      ENDDO
      GOTO 11
!
4091  PRINT 8102
      GOTO  9999
4051  PRINT 8103, FEDI
      GOTO  9999
4050  PRINT 8104, FEDS
      GOTO  9999
4081  PRINT 8105, FEMMTD
      GOTO  9999
4160  PRINT 8106
      GOTO  9999
4161  PRINT 8107
      GOTO  9999
4150  PRINT 8108
      GOTO  9999
9996  PRINT 8109
      GOTO  9999
9997  PRINT 8002
      CLOSE(10)
      GOTO  9999
9998  PRINT 8001
9999  PRINT 8000
!
9000  CLOSE(50)
      CLOSE(81)
      CLOSE(91)
      PRINT *
      END PROGRAM TRA
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!     LECTURE ET RECOPIE DES PARAMETRES
!
      SUBROUTINE IOPBB0(LUI,LUO)
      use mod_dppr
      use mod_par_tds
      IMPLICIT NONE
      integer          :: LUI,LUO

      real(kind=dppr)  :: PARA,PREC

      integer          :: I,IP
      integer          :: NBOPH

      character(len = NBCLAB+10)  :: CHAINE
      character(len = NBCTIT)     :: TITRE
!
8000  FORMAT(' !!! TRA    : STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF IN IOPBB0')
!
      DO I=1,4
        READ(LUI,END=2000)          TITRE
        IF( LUO .NE. 0 ) WRITE(LUO) TITRE
      ENDDO
      READ(LUI,END=2000)          NBOPH,TITRE
      IF( LUO .NE. 0 ) WRITE(LUO) NBOPH,TITRE
      DO I=1,2
        READ(LUI,END=2000)          TITRE
        IF( LUO .NE. 0 ) WRITE(LUO) TITRE
      ENDDO
      DO IP=1,NBOPH
        READ(LUI,END=2000)          CHAINE,PARA,PREC
        IF( LUO .NE. 0 ) WRITE(LUO) CHAINE,PARA,PREC
      ENDDO
      RETURN
!
2000  PRINT 8003
      PRINT 8000
      PRINT *
      STOP
!
      END SUBROUTINE IOPBB0
