      PROGRAM ASME
!
!        MAI 2010  CW
!
!  lire un fichier binaire (ex. INFILE) de type ME_
!  et l'ecrire en ASCII dans INFILE'_ASC'
!
! APPEL : asme
!
      use mod_dppr
      use mod_par_tds
      IMPLICIT NONE

      real(kind=dppr)  :: HC

      integer          :: IC,ICC,IFB1,IFB2
      integer          :: ICP,ICPC
      integer          :: J
      integer          :: NELMA,NFB

      character(len = 150)  :: INFILE
      character(len = 160)  :: OUTFILE

      logical               :: LHEAD
!
1000  FORMAT(A)
1020  FORMAT(/,            &
             'ASME   : ')
2000  FORMAT(' <  ',A)
2001  FORMAT(' >> ',A)
7000  FORMAT('ENTER ME_ TYPE FILE NAME :')
8000  FORMAT(' !!! ASME   : STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF FOR ',A)
!
      PRINT 1020
!
!  FICHIER D'ENTREE
!
      PRINT 7000
      READ(*,1000) INFILE
      PRINT 2000,  TRIM(INFILE)
      OPEN(80,FILE=TRIM(INFILE),FORM='UNFORMATTED',STATUS='OLD')
!
!  FICHIER DE SORTIE
!
      OUTFILE = TRIM(INFILE)//'_ASC'
      PRINT 2001,  TRIM(OUTFILE)
      OPEN(10,FILE=TRIM(OUTFILE))
!
! LECTURE DES CARACTERISTIQUES GENERALES
!
      LHEAD = .TRUE.
!
12    DO ICC=1,MXSYM
E13:    DO ICPC=1,2
          READ(80,END=9000) J,IC,ICP,NELMA,NFB
          IF( LHEAD ) THEN
            WRITE(10,*) '>>> J,IC,ICP,NELMA,NFB'
            LHEAD = .FALSE.
          ENDIF
          WRITE(10,*)       J,IC,ICP,NELMA,NFB
          IF( NFB .EQ. 0 ) CYCLE E13
          WRITE(10,*) '>>> H(IFB1,IFB2), IFB1=1,NFB, IFB2=IFB1,NFB'
          DO IFB1=1,NFB
            DO IFB2=IFB1,NFB
              READ (80,END=9003) HC                                                                ! H(IFB1,IFB2)
              WRITE(10,*)        HC
            ENDDO
          ENDDO
          LHEAD = .TRUE.
        ENDDO E13
      ENDDO
      GOTO 12
!
9003  PRINT 8003, TRIM(INFILE)
      PRINT 8000
!
9000  CLOSE(10)
      PRINT *
      END PROGRAM ASME
