      PROGRAM SPECH
!
!  WRITTEN FOR SUN4 OS XXX FORTRAN XXX  11.02.91   J.P.CHAMPION
!  SMIL LABORATOIRE DE SPECTRONOMIE MOLECULAIRE ET INSTRUMENTATION LASER
!       URA CNRS 0777    UNIVERSITE DE DIJON
!    6 BOULEVARD GABRIEL  21000  DIJON  FRANCE
!
!  CALCULATED SPECTRUM : POSITIONS + INTENSITIES
!   XY4  TETRAHEDRAL MOLECULES
!
!  NON-INTERACTIVE VERSION   23 JULY 1993
! REV    JAN 1995 JPC,CW (PARAMETER)
! REV    NOV 1996 JPC,CW (STRxxx)
!
! MODIFIE 03/1998 V. BOUDON ---> XY6 OCTAHEDRAL MOLECULES.
!
! REV    OCT 1999 JPC    (HITRAN OUTPUT)
! REV    JUN 2000 JPC    (STRTOT)
!
! APPEL : spech  FMIN  FMAX  TVIB  TROT  RINMI [fpvib FPVIB] [abund ABUND] [MOL [ISO [SPINX [database]]]]
! MOL, ISO, SPINX and database (if specified) must be defined as the last options and in this order
!
      use mod_dppr
      use mod_par_tds
      use mod_com_branch
      use mod_com_const
      use mod_com_fdate
      use mod_com_fp
      use mod_com_spin
      use mod_com_sy
      use mod_main_spech
      IMPLICIT NONE

      real(kind=dppr)  :: A,A2
      real(kind=dppr)  :: COEF,COEFA
      real(kind=dppr)  :: ENINF,ERINF,ERSUP,EVINF,EXTEMP
      real(kind=dppr)  :: F,F1,F2,FM,FMAX,FMIN,FR,FRJMAX,FV
      real(kind=dppr)  :: GAMAIR,GAMSLF,GI,GS
      real(kind=dppr)  :: HHT,HT
      real(kind=dppr)  :: RIF,RINMI
      real(kind=dppr)  :: SHIF,SPINX,STRSEL,STRTOT
      real(kind=dppr)  :: TR,TR1,TR2,TRJMAX,TRM,TRMI,TROT,TVIB
      real(kind=dppr)  :: W0

      integer          :: I,ICI,ICOV,ICOVN,ICPI,ICPS,ICS,ICVNT,IDYN,IER1,IER2
      integer          :: IER3,IER4,IER5,IER6,III,IISO,IMOL
      integer          :: IPCINF,IPCSUP,IRA,IREF1,IREF2,IREF3,IREF4
      integer          :: IREF5,IREF6,ISV
      integer          :: J,JI,JI1,JI2,JIM,JIMAX,JIMIN,JMAX1,JS
      integer          :: NINF,NREC,NSUP,NSVINF,NSVSUP,NTRANS,NUSVI
      integer          :: NUSVS

      character(len = NBCTIT)  :: BUFF,TITRE
      character(len =   1)  :: FLAG
      character(len =   1)  :: PA1,PA2,PAJMAX,PAM,PAV
      character(len =   2)  :: BR,BR1,BR2,BRJMAX,BRM
      character(len =   2)  :: CCV
      character(len =   2)  :: SYJMAX,SYM1,SYM2,SYMM
      character(len =   5)  :: FSI,FSS
      character(len =  10)  :: OPT
      character(len =  30)  :: PUNIT

      logical               :: LDB
!
1000  FORMAT(I2,I1,F12.6,1P,2E10.3,0P,2F5.4,F10.4,F4.2,F8.6,   &
             2(5X,6I1,I2,A),                                   &
             2(I3,1X,2A,I3,A),                                 &
             6I1,6I2,                                          &
             A,2F7.1                                        )
1001  FORMAT(A)
1002  FORMAT(13X,         &                                                                        ! cf. 1002 de hmodel
             I2 , 9X,     &
             I2 , 9X,     &
             I2 ,13X,     &
             I2 ,13X,     &
             I2 ,13X,     &
             I2 , 9X,2A)
1011  FORMAT(I4,A)
1020  FORMAT('     #       v1         v2         v3             v4             v5             v6          Cv')
1023  FORMAT(F12.6,1PE9.2)
1030  FORMAT(/,              &
             'SPECH  : ',A)
1121  FORMAT(/,                                 &
             I3,' Upper Vibrational States',/)
1122  FORMAT(/,                                 &
             I3,' Lower Vibrational States',/)
1222  FORMAT(/,                                            &
             ' Number of Calculated Transitions     ',I9)
1223  FORMAT(' First Transition      -> ',F15.6,1PE9.2,2X,A,I3,1X,2A)
1224  FORMAT(' Strongest Transition  -> ',F15.6,1PE9.2,2X,A,I3,1X,2A)
1225  FORMAT(' Last Transition       -> ',F15.6,1PE9.2,2X,A,I3,1X,2A)
1227  FORMAT(' Strongest Tr at Jmax  -> ',F15.6,1PE9.2,2X,A,I3,1X,2A)
1228  FORMAT(' Intensity summations:'            ,/,   &
             E9.2,1X,A,'with    threshold =',E9.2,/,   &
             E9.2,1X,A,'without threshold'          )
1230  FORMAT(/,                             &
             'Maximum J value:       ',I3)
1231  FORMAT('Frequency Range:',F12.3,' - ',F12.3)
1232  FORMAT('Vibrational Temperature:',F8.2,' Kelvin')
1233  FORMAT('Rotational  Temperature:',F8.2,' Kelvin')
1234  FORMAT('Intensity Lower Limit:',E10.2,1X,A)
1236  FORMAT(/,                      &
             'POLYAD DEFINITIONS:')
1237  FORMAT(/,                &
             'REFERENCES:',/)
1238  FORMAT('Analysis of line positions:')
1239  FORMAT(/,                                &
             'Analysis of line intensities:')
1240  FORMAT(/,                 &
             'USER SETTINGS:')
1241  FORMAT(/,              &
             'STATISTICS:')
1242  FORMAT(/,                                                &
             '>>>> COMMENTS, WARNINGS AND SUGGESTIONS !!!!:')
1243  FORMAT('No transition above threshold past J = ',I3)
1244  FORMAT('>>>> Save CPU time and Disk Space by setting JMAX to: ',I3)
1245  FORMAT('Possibly unconsistent prediction due to J Truncation')
1246  FORMAT('>>>> Increase SMIN or JMAX or both')
1247  FORMAT('Intensity Dynamic Range:      10**',I2.2)
1248  FORMAT('>>>> Save Disk Space by increasing SMIN')
1249  FORMAT(/,                                                       &
             'EDIT the job file according to the above suggestions')
1250  FORMAT('for all polyads and RUN again ...',/)
1251  FORMAT('Abundance: ',F9.4)
1252  FORMAT('Vibrational partition function: ',1PE12.4)
2000  FORMAT(' <  ',A)
2001  FORMAT(' >> ',A)
8000  FORMAT(' !!! SPECH  : STOP ON ERROR')
8001  FORMAT(' !!! ERROR OPENING CONTROL FILE')
8002  FORMAT(' !!! INCOMPLETE CONTROL FILE')
8013  FORMAT(' !!! 0. < FPVIB    REQUESTED')
8014  FORMAT(' !!! 0. < ABUND <=1.    REQUESTED')
8090  FORMAT(' !!! ERROR OPENING trans.t FILE')
8091  FORMAT(' !!! ERROR OPENING spectr.t FILE')
8092  FORMAT(' !!! ERROR OPENING stdh.log FILE')
8093  FORMAT(' !!! ERROR OPENING warnings FILE')
8094  FORMAT(' !!! ERROR OPENING spectr.xy FILE')
8095  FORMAT(' !!! ERROR OPENING spectr.s FILE')
!
! ARRAY INITIAL ALLOCATION
!
      CALL ALLOC_ALL
!
      OPEN(10,FILE=CTRLF,ERR=9998,STATUS='OLD')
      READ(10,1001,END=9997) FDATE
      PRINT 1030,            FDATE
!
! *** INPUT / OUTPUT FILES
!
      PRINT 2000,           'trans.t'
      OPEN(90,ERR=4090,FILE='trans.t',FORM='UNFORMATTED',STATUS='OLD')
      PRINT 2001,           'spectr.t'
      OPEN(91,ERR=4091,FILE='spectr.t',STATUS='UNKNOWN')
      PRINT 2001,           'stdh.log'
      OPEN(94,ERR=4092,FILE='stdh.log',STATUS='UNKNOWN')
      PRINT 2001,           'warnings'
      OPEN(93,ERR=4093,FILE='warnings',STATUS='UNKNOWN')
      PRINT 2001,           'spectr.xy'
      OPEN(92,ERR=4094,FILE='spectr.xy',STATUS='UNKNOWN')
!
! *** BASIC MOLECULAR CONSTANTS
!
      READ(90)
      READ(90) BUFF
      READ(BUFF,*) SPIN,SPINY
      READ(90)
      READ(90) BUFF
      READ(BUFF,*) VIBNU,B0,D0
      REWIND(90)
!
! *** READING INPUT FILE HEADINGS
!
      CALL IOPBAX(90,0)
!
! INTENSITY UNIT
!
      READ(90)
      READ(90)
      READ(90)
      READ(90) BUFF
      READ(BUFF,1011) III,PUNIT
      PUNIT = 'cm/molecule'
      REWIND(90)
      CALL IOPBAX(90,0)
      CALL IOPBAX(90,0)
      WRITE(94,1236)
      READ (90)      NSVSUP
      WRITE(94,1121) NSVSUP
      PRINT    1121, NSVSUP
      DO WHILE( NSVSUP .GT. MXSNV )
        CALL RESIZE_MXSNV
      ENDDO
      WRITE(94,1020)
      PRINT 1020
      ICOV  = -1
      CCV   = '  '
      PAV   = ' '
      ICVNT =  0
      DO ISV=1,NSVSUP
        READ(90)    TITRE(:96)
        PRINT 1001, TITRE(:96)
        READ(TITRE(:96),1002) (IVIBS(ISV,J),J=1,NBVQN),VIBSC(ISV),PARSC(ISV)
        ICOVN = IVIBS(ISV,1)*100000+IVIBS(ISV,2)*10000+IVIBS(ISV,3)*1000+IVIBS(ISV,4)*100+IVIBS(ISV,5)*10+IVIBS(ISV,6)
        IF( ICOVN      .NE. ICOV .OR.         &
            VIBSC(ISV) .NE. CCV  .OR.         &
            PARSC(ISV) .NE. PAV       ) THEN
          ICVNT = 1
          ICOV  = ICOVN
          CCV   = VIBSC(ISV)
          PAV   = PARSC(ISV)
        ELSE
          ICVNT = ICVNT+1
        ENDIF
        IVIBNS(ISV) = ICVNT
        WRITE(94,1001) TITRE(:96)
      ENDDO
      READ (90)      NSVINF
      WRITE(94,1122) NSVINF
      PRINT    1122, NSVINF
      DO WHILE( NSVINF .GT. MXSNV )
        CALL RESIZE_MXSNV
      ENDDO
      WRITE(94,1020)
      PRINT 1020
      ICOV  = -1
      CCV   = '  '
      PAV   = ' '
      ICVNT =  0
      DO ISV=1,NSVINF
        READ(90)    TITRE(:96)
        PRINT 1001, TITRE(:96)
        READ(TITRE(:96),1002) (IVIBI(ISV,J),J=1,NBVQN),VIBIC(ISV),PARIC(ISV)
        ICOVN = IVIBI(ISV,1)*100000+IVIBI(ISV,2)*10000+IVIBI(ISV,3)*1000+IVIBI(ISV,4)*100+IVIBI(ISV,5)*10+IVIBI(ISV,6)
        IF( ICOVN      .NE. ICOV .OR.         &
            VIBIC(ISV) .NE. CCV  .OR.         &
            PARIC(ISV) .NE. PAV       ) THEN
          ICVNT = 1
          ICOV  = ICOVN
          CCV   = VIBIC(ISV)
          PAV   = PARIC(ISV)
        ELSE
          ICVNT = ICVNT+1
        ENDIF
        IVIBNI(ISV) = ICVNT
        WRITE(94,1001) TITRE(:96)
      ENDDO
      REWIND(90)
      WRITE(93,1237)
      WRITE(93,1238)
      CALL IOPBAX(90,93)
      WRITE(93,1239)
      CALL IOPBAX(90,93)
!
! *** SKIPPING RECORDS
!
      NREC = NSVINF+NSVSUP+2
      DO I=1,NREC
        READ(90)
      ENDDO
!
! *** READING OPTIONS IN INPUT FILE
!
      READ(90) JMAX1
      READ(90) IRA
!
! *** ENTERING NEW OPTIONS FROM ARGUMENTS OF THE SPECT COMMAND
!
      WRITE(94,1240)
      WRITE(94,1230) JMAX1
      PRINT    1230, JMAX1
      READ(10,*,END=9997) FMIN
      READ(10,*,END=9997) FMAX
      WRITE(94,1231) FMIN,FMAX
      PRINT    1231, FMIN,FMAX
      READ (10,*,END=9997) TVIB
      WRITE(94,1232)       TVIB
      PRINT    1232,       TVIB
      READ (10,*,END=9997) TROT
      WRITE(94,1233)       TROT
      PRINT    1233,       TROT
      READ (10,*,END=9997) RINMI
      WRITE(94,1234)       RINMI,TRIM(PUNIT)
      PRINT    1234,       RINMI,TRIM(PUNIT)
      ABUND = 1.D0                                                                                 ! abundance
      FPVIB = 0.D0                                                                                 ! no fpvib option
      IMOL  = 0
      IISO  = 0
      SPINX = 0.D0
      LDB   = .FALSE.                                                                              ! ne pas creer de fichier pour XSAMS
!
19    READ(10,1001,END=21) OPT
      IF( OPT .EQ. 'fpvib' ) THEN
        READ(10,*,END=9997) FPVIB
        IF( FPVIB .LE. 0.D0 ) GOTO 9992
        GOTO 19
      ENDIF
      IF( OPT .EQ. 'abund' ) THEN
        READ(10,*,END=9997) ABUND
        IF( ABUND .LE. 0.D0 .OR.              &
            ABUND .GT. 1.D0      ) GOTO 9991
        GOTO 19
      ENDIF
      BACKSPACE(10)
      READ(10,*,END=21) IMOL
      READ(10,*,END=21) IISO
      READ(10,*,END=21) SPINX
      READ(10,1001,END=21) OPT
      IF( OPT .EQ. 'database' ) LDB = .TRUE.                                                       ! creer un fichier pour XSAMS
!
21    CLOSE(10)
      IF( LDB ) THEN
        PRINT 2001,           'spectr.s'
        OPEN(95,ERR=4095,FILE='spectr.s',FORM='UNFORMATTED')
        WRITE(95) IMOL,IISO
      ENDIF
!
! *** CALCULATING  CONSTANT FACTOR FROM PARTITION FUNCTION
!
      COEF  = (8.D0*PI*PI*PI)/(CLUM*PLANK)*1.D-36
      COEF  = COEF/FPART(TROT,TVIB)
      COEFA = (64.D0*PI*PI*PI*PI)/(3.D0*PLANK)*1.D-36
      WRITE(94,1251) ABUND
      PRINT    1251, ABUND
      WRITE(94,1252) FPVIB
      PRINT    1252, FPVIB
!
! *** INITIAL VALUES FOR Fmin,Fmax,Imax,JImin,JImax,Ntrans
!
      F1     =   1.D+10
      F2     =   0.D0
      TRMI   =   0.D0
      TRM    =   0.D0
      TRJMAX =   0.D0
      JIMIN  = 200
      JIMAX  =   0
      NTRANS =   0
!
! *** HITRAN PARAMETERS DEFAULT
!
      GAMAIR = 0.D0
      GAMSLF = 0.D0
      EXTEMP = 0.D0
      SHIF   = 0.D0
      IER1   = 0
      IER2   = 0
      IER3   = 0
      IER4   = 0
      IER5   = 0
      IER6   = 0
      IREF1  = 0
      IREF2  = 0
      IREF3  = 0
      IREF4  = 0
      IREF5  = 0
      IREF6  = 0
      FLAG   = ' '
      FSS    = ' '
      FSI    = ' '
!
! *** READING INPUT FILE
!
      STRTOT = 0.D0
      STRSEL = 0.D0
!
1003  READ(90,END=9000) JI,ICI,ICPI,NINF,NUSVI,IPCINF,  &
                        ENINF,                          &
                        JS,ICS,ICPS,NSUP,NUSVS,IPCSUP,  &
                        F,A2
      IF( FMIN .GT. F .OR.              &
          FMAX .LT. F      ) GOTO 1003
!
! *** CALCULATING INTENSITY
!
      ERINF = B0*DBLE(JI*(JI+1))
      EVINF = ENINF-ERINF
      HT    = ((EVINF/TVIB)+(ERINF/TROT))*HCOVRK
      W0    = EXP(-HT)*SPIN(ICI)
      ERSUP = B0*DBLE(JS*(JS+1))
      FR    = ERSUP-ERINF
      FV    = F-FR
      HHT   = ((FR/TROT)+(FV/TVIB))*HCOVRK
      IF( EXP(-HHT) .GE. 1.D0 ) GOTO 1003
      RIF = 3.D0*A2/DBLE(2*JI+1)
      TR  = A2*W0*F*COEF*(1.D0-EXP(-HHT))
      IF( IRA .EQ. 0 ) TR = A2*W0
      TR     = TR*ABUND
      STRTOT = STRTOT+TR
      IF( TR .LT. RINMI ) GOTO 1003
      STRSEL = STRSEL+TR
      BR     = BRANCH(JS-JI+MXBRA-2)
      NTRANS = NTRANS+1
!
! *** DETERMINING SPECTRUM LIMITS
!
      IF( F .LT. F1 ) THEN
        F1   = F
        TR1  = TR
        BR1  = BR
        JI1  = JI
        SYM1 = SYM(ICI)
        PA1  = PAR(ICPI)
      ENDIF
      IF( F .GT. F2 ) THEN
        F2   = F
        TR2  = TR
        BR2  = BR
        JI2  = JI
        SYM2 = SYM(ICI)
        PA2  = PAR(ICPI)
      ENDIF
      IF( TR .GT. 0.D0 ) THEN
        IF( TRMI .EQ. 0.D0 .OR.              &
            TR   .LT. TRMI      ) TRMI = TR
      ENDIF
      IF( TR .GT. TRM ) THEN
        FM   = F
        TRM  = TR
        BRM  = BR
        JIM  = JI
        SYMM = SYM(ICI)
        PAM  = PAR(ICPI)
      ENDIF
      IF( JI .LT. JIMIN        ) JIMIN = JI
      IF( JI .GT. JIMAX        ) JIMAX = JI
      IF( JS .EQ. JMAX1  .AND.         &
          TR .GT. TRJMAX       ) THEN
        FRJMAX = F
        TRJMAX = TR
        BRJMAX = BR
        SYJMAX = SYM(ICI)
        PAJMAX = PAR(ICPI)
      ENDIF
!
! *** WRITING IN OUTPUT FILE
!
      GS = (2.D0*SPINX+1.D0)*(2.D0*JS+1.D0)*SPIN(ICS)
      GI = (2.D0*SPINX+1.D0)*(2.D0*JI+1.D0)*SPIN(ICI)
      A  = COEFA*GI/GS*F*F*F*RIF
      WRITE(91,1000) IMOL,IISO,F,TR,A,GAMAIR,GAMSLF,ENINF,EXTEMP,SHIF,       &
                     (IVIBS(NUSVS,J),J=1,NBVQN),IVIBNS(NUSVS),VIBSC(NUSVS),  &
                     (IVIBI(NUSVI,J),J=1,NBVQN),IVIBNI(NUSVI),VIBIC(NUSVI),  &
                     JS,SYM(ICS),PAR(ICPS),NSUP,FSS,                         &
                     JI,SYM(ICI),PAR(ICPI),NINF,FSI,                         &
                     IER1,IER2,IER3,IER4,IER5,IER6,                          &
                     IREF1,IREF2,IREF3,IREF4,IREF5,IREF6,                    &
                     FLAG,GS,GI
      IF( LDB ) THEN
        WRITE(95) F,TR*CL*T0/TROT,TR,REAL(A),ENINF,  &
                  JS,SYM(ICS)//PAR(ICPS),NSUP,       &
                  JI,SYM(ICI)//PAR(ICPI),NINF
      ENDIF
      WRITE(92,1023) F,TR*CL*T0/TROT
      GOTO 1003
!
4090  PRINT 8090
      GOTO  9999
4091  PRINT 8091
      GOTO  9999
4092  PRINT 8092
      GOTO  9999
4093  PRINT 8093
      GOTO  9999
4094  PRINT 8094
      GOTO  9999
4095  PRINT 8095
      GOTO  9999
9991  PRINT 8014
      GOTO  9999
9992  PRINT 8013
      GOTO  9999
9997  PRINT 8002
      GOTO  9999
9998  PRINT 8001
9999  PRINT 8000
      STOP
!
9000  PRINT *
      WRITE(94,1241)
      PRINT    1222, NTRANS
      WRITE(94,1222) NTRANS
      PRINT    1223, F1,TR1,BR1,JI1,SYM1,PA1
      WRITE(94,1223) F1,TR1,BR1,JI1,SYM1,PA1
      PRINT    1224, FM,TRM,BRM,JIM,SYMM,PAM
      WRITE(94,1224) FM,TRM,BRM,JIM,SYMM,PAM
      PRINT    1225, F2,TR2,BR2,JI2,SYM2,PA2
      WRITE(94,1225) F2,TR2,BR2,JI2,SYM2,PA2
      IF( JIMAX .EQ. JMAX1 ) THEN
        PRINT    1227, FRJMAX,TRJMAX,BRJMAX,JMAX1,SYJMAX,PAJMAX
        WRITE(94,1227) FRJMAX,TRJMAX,BRJMAX,JMAX1,SYJMAX,PAJMAX
      ENDIF
      PRINT    1228, STRSEL,PUNIT,RINMI,STRTOT,PUNIT
      WRITE(94,1228) STRSEL,PUNIT,RINMI,STRTOT,PUNIT
      WRITE(93,1242)
      IDYN = NINT(LOG10(TRM/TRMI))
      IF( JIMAX .LT. JMAX1-1 ) THEN
        WRITE(93,1243) JIMAX
        WRITE(93,1244) JIMAX+1
      ENDIF
      IF( JIMAX .GE. JMAX1 ) THEN
        WRITE(93,1245)
        WRITE(93,1246)
      ENDIF
      WRITE(93,1247) IDYN
      IF( STRTOT .LE. STRSEL*1.001D0 ) THEN
        WRITE(93,1248)
      ENDIF
      WRITE(93,1249)
      WRITE(93,1250)
      CLOSE(90)
      CLOSE(91)
      CLOSE(94)
      CLOSE(93)
      CLOSE(92)
      IF( LDB ) CLOSE(95)
      PRINT *
      END PROGRAM SPECH
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!     READING AND COPYING PARAMETERS
!
      SUBROUTINE IOPBAX(LUI,LUO)
      use mod_par_tds
      IMPLICIT NONE
      integer          :: LUI,LUO

      integer          :: I,IP
      integer          :: NBOPH

      character(len = NBCTIT)  :: TITRE
!
1000  FORMAT(A)
8000  FORMAT(' !!! SPECH  : STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF IN IOPBAX')
!
      DO I=1,4
        READ(LUI,END=2000)
      ENDDO
      READ(LUI,END=2000) NBOPH
      READ(LUI,END=2000)               TITRE
      IF( LUO .NE. 0 ) WRITE(LUO,1000) TRIM(TITRE)
      READ(LUI,END=2000)
      DO IP=1,NBOPH
        READ(LUI,END=2000)
      ENDDO
      RETURN
!
2000  PRINT 8003
      PRINT 8000
      PRINT *
      STOP
!
      END SUBROUTINE IOPBAX
