      PROGRAM ASXVP
!
! 2008 AUGUST
! V.BOUDON, CH.WENGER
!
!  lire un fichier binaire (ex. INFILE) de type XVP_
!  et l'ecrire en ASCII dans INFILE'_ASC'
!
! APPEL : asxvp
!
      use mod_dppr
      use mod_par_tds
      use mod_main_xpafit
      IMPLICIT NONE

      integer          :: IC,IP
      integer          :: ICP
      integer          :: J,JB
      integer          :: NFB

      character(len = 150)  :: INFILE
      character(len = 160)  :: OUTFILE
!
1000  FORMAT(A)
1020  FORMAT(/,            &
             'ASXVP  : ')
2000  FORMAT(' <  ',A)
2001  FORMAT(' >> ',A)
7000  FORMAT('ENTER XVP_ TYPE FILE NAME :')
8000  FORMAT(' !!! ASXVP  : STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF FOR ',A)
!
! ARRAY INITIAL ALLOCATION
!
      CALL ALLOC_ALL
!
      PRINT 1020
!
!  FICHIER D'ENTREE
!
      PRINT 7000
      READ(*,1000) INFILE
      PRINT 2000,  TRIM(INFILE)
      OPEN(60,FILE=TRIM(INFILE),FORM='UNFORMATTED',STATUS='OLD')
!
!  FICHIER DE SORTIE
!
      OUTFILE = TRIM(INFILE)//'_ASC'
      PRINT 2001,  TRIM(OUTFILE)
      OPEN(10,FILE=TRIM(OUTFILE))
      WRITE(10,*) '>>> J,IC,ICP,NFB'
      WRITE(10,*) '>>> (HD(K(IP)),IP=1,NFB)'
      WRITE(10,*) '>>> NVCOD(JB),NRCOD(JB),(T(JB,K(IP)),IP=1,NFB)'
!
12    READ (60,END=9000) J,IC,ICP,NFB
      WRITE(10,*)        J,IC,ICP,NFB
      DO WHILE( NFB .GT. MXDIMS )
        CALL RESIZE_MXDIMS
      ENDDO
      IF( NFB .EQ. 0 ) GOTO 12
      READ (60,END=9003) (HD(IP),IP=1,NFB)
      WRITE(10,*)        (HD(IP),IP=1,NFB)
      DO JB=1,NFB
        READ (60,END=9003) NVCOD(1),NRCOD(1),(T(1,IP),IP=1,NFB)
        WRITE(10,*)        NVCOD(1),NRCOD(1),(T(1,IP),IP=1,NFB)
      ENDDO
      GOTO 12
!
9003  PRINT 8003, TRIM(INFILE)
      PRINT 8000
!
9000  CLOSE(10)
      PRINT *
      END PROGRAM ASXVP
