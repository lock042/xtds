      PROGRAM PRECF
!
! JUIN 2010 CW VB d'apres eq_tds.f
!
! inserrer dans un fichier de prediction sortant de eq_int (prediction.t)
!          la precision sur les frequences calculees.
!
! jobs a executer au prealable :
!  un job_fit de frequence (eq_tds+paradj)              -> CL_eqtds, ED_*, para_variance.t
!  un job_fit d'intensite  (eq_int+paradj)
!  puis un 2eme eq_int avec l'option prec et RINMI=0.   -> prediction.t avec la precision sur les intensites
!  sont aussi utilises ED_Pn_ ED_Pn'_
!
! APPEL : precf Pn Pn' JMAX freq_para_variance int_prediction
!
! exemple de job :
!
!  #! /bin/sh
!   set -v
!  ##
!  BASD=/home/wenger/tempo/XTDS/packages/STDS
!  ##
!   SCRD=$BASD/prog/exe
!  ##
!  ## Jmax values.
!  ##
!   JP3=20
!   JP3mP0=$JP3
!  ##
!  ## 4_P3mP0
!  ##
!   cp ../pos/CL_eqtds        CL_eqtds
!   cp ../pos/ED_P0_          ED_P0_
!   cp ../pos/ED_P3_          ED_P3_
!   cp ../pos/para_variance.t freq_para_variance.t
!   cp ../int/Pred_1_P3mP0    int_prediction.t
!
!   $SCRD/passx precf P3 P0 $JP3mP0 freq_para_variance.t int_prediction.t
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
      use mod_dppr
      use mod_par_tds
      use mod_com_der
      use mod_com_fdate
      use mod_com_sy
      use mod_main_precf
      IMPLICIT NONE

      real(kind=dppr)  :: DDT,DIFF
      real(kind=dppr)  :: ENIN,ENSU,ENSUP
      real(kind=dppr)  :: FCAO,FEXP
      real(kind=dppr)  :: PF1,PF2
      real(kind=dppr)  :: SDFCAL,SDFEXP,SDFMOO
      real(kind=dppr)  :: VACOII,VACOIJ

      integer          :: I,ICAL,ICINF,ICNTCL
      integer          :: IPCSUP,ICSUP,IOP,IP
      integer          :: ISV
      integer          :: J,JINF,JMAX,JOP
      integer          :: JSUP
      integer          :: K
      integer          :: NBCAL,NBINF
      integer          :: NBOPHI,NBOPHS,NBSUP
      integer          :: NINF,NSTAR,NSUP,NSVINF
      integer          :: NSVSUP,NUSVSU

      character(len = NBCTIT)     :: TITRE
      character(len =  11) ,dimension(2)      :: CARG
      character(len =   1)  :: FAS
      character(len =   1)  :: PARINF,PARSUP
      character(len =   2)  :: BR
      character(len =   2)  :: SYINF,SYSUP
      character(len =   6)  :: IDENT
      character(len =   8)  :: CTRP  = 'CL_eqtds'
      character(len = 120)  :: FEDI,FEDS,FPVAR,FPRED
      character(len = 127)  :: LINE,FPREDS                                                         ! cf FORMAT 2021

      logical          :: LERR
!
1001  FORMAT(A)
1002  FORMAT(I4)
2000  FORMAT(' <  ',A)
2001  FORMAT(' >> ',A)
2010  FORMAT(/,              &
             'PRECF  : ',A)
2011  FORMAT(' PRECF  -> J = ',I3,'/',I3)
2021  FORMAT(F13.6,2X,2A,2F7.3,F11.3,F13.6,F8.3,I5,1X,2A,I3,F13.6,I3,   &                          ! FORMAT 2020 de eq_int en lecture
             I4,1X ,2X,A,1X,2(I3,1X,2A,I3,1X)                        )
2022  FORMAT(F8.3)                                                                                 ! cf FORMAT 2021
3004  FORMAT(////,   &
             I4,//)
8000  FORMAT(' !!! PRECF  : STOP ON ERROR')
8001  FORMAT(' !!! ERROR OPENING CONTROL FILE')
8002  FORMAT(' !!! INCOMPLETE CONTROL FILE')
8100  FORMAT(' !!! ERROR OPENING INPUT PREDICTION FILE: ',A)
8101  FORMAT(' !!! UNEXPECTED EOF IN INPUT PREDICTION FILE: ',A)
8102  FORMAT(' !!! ERROR READING INPUT PREDICTION FILE: ',A)
8104  FORMAT(' !!! UPPER LEVEL FILE => UNEXPECTED EOF')
8105  FORMAT(' !!! LOWER LEVEL FILE => UNEXPECTED EOF')
8107  FORMAT(' !!! ERROR OPENING UPPER EIGENVAL. AND VECT. FILE: ',A)
8108  FORMAT(' !!! ERROR OPENING LOWER EIGENVAL. AND VECT. FILE: ',A)
8109  FORMAT(' !!! ERROR OPENING para_variance.t FILE: ',A)
8110  FORMAT(' !!! ERROR OPENING OUTPUT PREDICTION FILE: ',A)
8111  FORMAT(' !!! ERROR WRITING OUTPUT PREDICTION FILE: ',A)
8112  FORMAT(' !!! ERROR WRITING SDFACL')
8201  FORMAT(' !!! NOT FOUND: LOWER LEVEL OF TRANSITION  ',2(I3,1X,2A,I3,1X) )
8202  FORMAT(' !!! NOT FOUND: UPPER LEVEL OF TRANSITION  ',2(I3,1X,2A,I3,1X) )
!
! ARRAY INITIAL ALLOCATION
!
      CALL ALLOC_ALL
!
! lecture des arguments d'appel
!
      OPEN(10,FILE=CTRLF,ERR=9998,STATUS='OLD')
      READ(10,1001,END=9997) FDATE
      PRINT 2010,            FDATE
      DO I=1,2                                                                                     ! ARGUMENTS OBLIGATOIRES
        READ(10,1001,END=9997) CARG(I)
      ENDDO
      READ(10,*,END=9997)    JMAX
      READ(10,1001,END=9997) FPVAR                                                                 ! fichier para_variance.t (frequences)
      READ(10,1001,END=9997) FPRED                                                                 ! fichier prediction.t    (intensites)
      CLOSE(10)
      FEDI = 'ED_'//TRIM(CARG(2))//'_'
      FEDS = 'ED_'//TRIM(CARG(1))//'_'
!
!  lecture du fichier de controle reduit
!
      PRINT 2000, CTRP
      OPEN(13,FILE=CTRP,STATUS='OLD')                                                              ! FICHIER DE CONTROLE REDUIT
      READ(13,3004) ICNTCL                                                                         ! nb de paras effectivement fittes
      DO I=1,ICNTCL
        READ(13,1002) IXCL(I)                                                                      ! index des operateurs fittes
        READ(13,1001,END=5) TITRE                                                                  ! lire une eventuelle unique ligne de dependances
        NSTAR = INDEX(TRIM(TITRE),'*')
        IF( NSTAR .LT. 1 ) THEN                                                                    ! n'est pas une ligne de dependances
          BACKSPACE(13)
        ENDIF
      ENDDO
!
5     CLOSE(13)
!
!  lecture du fichier de prediction
!
      NBCAL = 0                                                                                    ! nb de raies lues
      PRINT 2000, TRIM(FPRED)
      OPEN(91,ERR=9996,FILE=TRIM(FPRED),STATUS='OLD')
!
1     READ(91,1001,END=9995) LINE
      IF( LINE .NE. '      SEXP       CODE  %SDEXP %SDTHEO'//            &                         ! FORMAT 3012 de eq_int
                    ' %EXP-CALC      SCAL     %SDCAL  ( J C'//           &
                    '    N      E  )INF'                       ) GOTO 1
      READ(91,1001,END=9995)
!
3     READ(91,2021,ERR=9994,END=4) FEXP,FAS,IDENT,SDFEXP,SDFMOO,DIFF,FCAO,SDFCAL,  &               ! ligne des frequences
                                   FEXP,FAS,IDENT,SDFEXP,SDFMOO,DIFF,FCAO,SDFCAL,  &
                                   JSUP,SYSUP,PARSUP,NSUP,                         &
                                   ENSUP,NUSVSU,IPCSUP,BR,                         &
                                   JINF,SYINF,PARINF,NINF,                         &
                                   JSUP,SYSUP,PARSUP,NSUP
      NBCAL = NBCAL+1
      IF( NBCAL .GT. MXOBS ) CALL RESIZE_MXOBS
      JI(NBCAL)   = JINF
      SYI(NBCAL)  = SYINF
      PARI(NBCAL) = PARINF
      NI(NBCAL)   = NINF
      JS(NBCAL)   = JSUP
      SYS(NBCAL)  = SYSUP
      PARS(NBCAL) = PARSUP
      NS(NBCAL)   = NSUP
      READ(91,1001,END=9995)                                                                       ! ligne des intensites
      GOTO 3
!
4     REWIND(91)
!
! ******************************************
! *** LECTURE DU FICHIER D'ENERGIES INF  ***
! ******************************************
!
      DO I=1,NBCAL
        FOUND(I) = .FALSE.
      ENDDO
      PRINT 2000,           TRIM(FEDI)
      OPEN(51,ERR=9987,FILE=TRIM(FEDI),STATUS='OLD',FORM='UNFORMATTED')
      CALL IOPBA0(51,0)
      READ(51) NSVINF
      DO ISV=1,NSVINF
        READ(51)
      ENDDO
      READ(51) NBOPHI
      DO WHILE( NBOPHI .GT. MXOPH )
        CALL RESIZE_MXOPH
      ENDDO
!
6     READ(51,END=8) JINF,ICINF,NBINF
      DO IP=1,NBINF
        READ(51,END=9992) ENIN,(DERIN(IOP),IOP=1,NBOPHI)
!
! *****************************************************************
! *** EST CE UN NIVEAU CORRESPONDANT A UNE TRANSITION OBSERVEE? ***
! *****************************************************************
!
        DO I=1,NBCAL
          IF( JINF       .EQ. JI(I)   .AND.         &
              SYM(ICINF) .EQ. SYI(I)  .AND.         &
              IP         .EQ. NI(I)         ) THEN
            FOUND(I) = .TRUE.
            DO IOP=1,NBOPHI
              DERINF(I,IOP) = DERIN(IOP)
            ENDDO
          ENDIF
        ENDDO
      ENDDO
      GOTO 6
!
8     CLOSE(51)
!
! tester si le niveau inf de chaque transition a ete trouve
!
      LERR = .FALSE.
      DO I=1,NBCAL
        IF( .NOT. FOUND(I) ) THEN
          LERR = .TRUE.
          PRINT 8201, JI(I),SYI(I),PARI(I),NI(I),  &
                      JS(I),SYS(I),PARS(I),NS(I)
        ENDIF
      ENDDO
      IF( LERR ) GOTO 9999
!
! ******************************************
! *** LECTURE DU FICHIER D'ENERGIES SUP  ***
! ******************************************
!
      DO I=1,NBCAL
        FOUND(I) = .FALSE.
      ENDDO
      PRINT 2000,           TRIM(FEDS)
      OPEN(50,ERR=9988,FILE=TRIM(FEDS),STATUS='OLD',FORM='UNFORMATTED')
      CALL IOPBA0(50,0)
      READ(50) NSVSUP
      DO ISV=1,NSVSUP
        READ(50)
      ENDDO
      READ(50) NBOPHS
      DO WHILE( NBOPHS .GT. MXOPH )
        CALL RESIZE_MXOPH
      ENDDO
!
66    READ(50,END=88) JSUP,ICSUP,NBSUP
!     IF( JSUP .GT. JMAX ) GOTO 88
E77:  DO IP=1,NBSUP
        READ(50,END=9993) ENSU,(DERSU(IOP),IOP=1,NBOPHS)
!
! *****************************************************************
! *** EST CE UN NIVEAU CORRESPONDANT A UNE TRANSITION OBSERVEE? ***
! *****************************************************************
!
        DO I=1,NBCAL
          IF( JSUP+2     .LT. JS(I)         ) CYCLE E77
          IF( JSUP       .EQ. JS(I)   .AND.         &
              SYM(ICSUP) .EQ. SYS(I)  .AND.         &
              IP         .EQ. NS(I)         ) THEN
            FOUND(I) = .TRUE.
            DO IOP=1,NBOPHS
              DERSUP(I,IOP) = DERSU(IOP)
            ENDDO
          ENDIF
        ENDDO
      ENDDO E77
      GOTO 66
!
88    CLOSE(50)
      PRINT 2011, JSUP,JMAX
!
! tester si le niveau sup de chaque transition a ete trouve
!
!
      LERR = .FALSE.
      DO I=1,NBCAL
        IF( .NOT. FOUND(I) ) THEN
          LERR = .TRUE.
          PRINT 8202, JI(I),SYI(I),PARI(I),NI(I),  &
                      JS(I),SYS(I),PARS(I),NI(I)
        ENDIF
      ENDDO
      IF( LERR ) GOTO 9999
!
! ******************************************************
! ***      LECTURE ET STOCKAGE DE LA MATRICE         ***
! ***   DE VARIANCE/COVARIANCE DES PARAMETRES DE H   ***
! ******************************************************
!
      PRINT 2000,           TRIM(FPVAR)
      OPEN(94,ERR=9991,FILE=TRIM(FPVAR),STATUS='OLD',FORM='UNFORMATTED')
      CALL IOPBA0(94,0)
      DO J=1,ICNTCL
        READ(94) (VACO(K,J),K=J,ICNTCL)
      ENDDO
      CLOSE(94)
!
!
! nouveau fichier de predictions avec la precision sur les frequences
!
      FPREDS = TRIM(FPRED)//'_SDFCAL'
      PRINT 2001,           TRIM(FPREDS)
      OPEN(92,ERR=9990,FILE=TRIM(FPREDS),STATUS='UNKNOWN')
!
11    READ(91,1001,END=9995) LINE
      IF( LINE .NE. '      SEXP       CODE  %SDEXP %SDTHEO'//          &
                    ' %EXP-CALC      SCAL     %SDCAL  ( J C'//         &
                    '    N      E  )INF'                       ) THEN
        WRITE(92,1001,ERR=9989) TRIM(LINE)
        GOTO 11
      ENDIF
      WRITE(92,1001,ERR=9989) TRIM(LINE)
      READ(91,1001,END=9995)
      WRITE(92,1001,ERR=9989)
!
! ************************************************
! *** CALCUL DES PRECISIONS SUR LES FREQUENCES ***
! ************************************************
!
      DO ICAL=1,NBCAL
        READ(91,1001,ERR=9994) LINE                                                                ! ligne des frequences
!
! ***************************
! *** CALCUL DES DERIVEES ***
! ***************************
!
        DO IOP=1,NBOPHI
          DERV(IOP) = DERSUP(ICAL,IOP)-DERINF(ICAL,IOP)
        ENDDO
        DO IOP=NBOPHI+1,NBOPHS
          DERV(IOP) = DERSUP(ICAL,IOP)
        ENDDO
!
! ********************************************************
! *** CALCUL DE L'ECART TYPE DE LA TRANSITION THEORIQUE ***
! ********************************************************
!
        PF1 = 0.D0
        PF2 = 0.D0
E773:   DO IOP=1,ICNTCL
          DDT    = DERV(IXCL(IOP))
          VACOII = VACO(IOP,IOP)
          PF1    = PF1+DDT*DDT*VACOII
          IF( IOP .EQ. ICNTCL ) CYCLE E773
          DO JOP=IOP+1,ICNTCL
            VACOIJ = VACO(JOP,IOP)
            PF2    = PF2+DDT*DERV(IXCL(JOP))*VACOIJ
          ENDDO
        ENDDO E773
        SDFCAL = PF1+PF2*2.D0
        SDFCAL = SQRT(SDFCAL)
        SDFCAL = SDFCAL*1.D3
        WRITE(LINE(61:68),2022,ERR=9986) SDFCAL
!
! ecriture de la ligne modifiee
!
        WRITE(92,1001,ERR=9989) TRIM(LINE)                                                          ! ligne des frequences
        READ (91,1001,ERR=9995) LINE                                                                ! ligne des intensites
        WRITE(92,1001,ERR=9989) TRIM(LINE)
      ENDDO
      CLOSE(91)
      CLOSE(92)
      GOTO 9000
!
9986  PRINT 8112
      GOTO  9999
9987  PRINT 8108, TRIM(FEDI)
      GOTO  9999
9988  PRINT 8107, TRIM(FEDS)
      GOTO  9999
9989  PRINT 8111, TRIM(FPREDS)
      GOTO  9999
9990  PRINT 8110, TRIM(FPREDS)
      GOTO  9999
9991  PRINT 8109, FPVAR
      GOTO  9999
9992  PRINT 8105
      GOTO  9999
9993  PRINT 8104
      GOTO  9999
9994  PRINT 8102, TRIM(FPRED)
      GOTO  9999
9995  PRINT 8101, TRIM(FPRED)
      GOTO  9999
9996  PRINT 8100, TRIM(FPRED)
      GOTO  9999
9997  PRINT 8002
      GOTO  9999
9998  PRINT 8001
9999  PRINT 8000
!
9000  PRINT *
      END PROGRAM PRECF
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!  LECTURE ET RECOPIE DES PARAMETRES
!
      SUBROUTINE IOPBA0(LUI,LUO)
      use mod_dppr
      use mod_par_tds
      IMPLICIT NONE
      integer          :: LUI,LUO

      real(kind=dppr)  :: PARA,PREC

      integer          :: I,IP
      integer          :: NBOPH

      character(len = NBCLAB+10)  :: CHAINE
      character(len = NBCTIT)     :: TITRE
!
1000  FORMAT(A)
1001  FORMAT(I4,A)
1002  FORMAT(A,E18.11,E14.7)
8000  FORMAT(' !!! PRECF  : STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF IN IOPBA0')
!
      DO I=1,4
        READ(LUI,END=2000)               TITRE
        IF( LUO .NE. 0 ) WRITE(LUO,1000) TITRE
      ENDDO
      READ(LUI,END=2000)               NBOPH,TITRE
      IF( LUO .NE. 0 ) WRITE(LUO,1001) NBOPH,TITRE
      DO I=1,2
        READ(LUI,END=2000)               TITRE
        IF( LUO .NE. 0 ) WRITE(LUO,1000) TITRE
      ENDDO
      DO IP=1,NBOPH
        READ(LUI,END=2000)               CHAINE,PARA,PREC
        IF( LUO .NE. 0 ) WRITE(LUO,1002) CHAINE,PARA,PREC
      ENDDO
      RETURN
!
2000  PRINT 8003
      PRINT 8000
      PRINT *
      STOP
!
      END SUBROUTINE IOPBA0
