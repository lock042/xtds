!
! *** RELATION TRIANGULAIRE DANS TD : CTR=1 SI OUI,=0 SI NON
!
! SMIL CHAMPION DEC 78
!
      FUNCTION CTR(C1,C2,C3)
      IMPLICIT NONE
      integer          :: CTR
      integer          :: C1,C2,C3

      integer ,dimension(3) :: C
      integer          :: CC,CC1
      integer          :: IP
!
      CTR  = 0
      C(1) = C1
      C(2) = C2
      C(3) = C3
      CALL RENGE(C,3,1,IP)
      CC1 = C(1)
      CC  = C(2)*C(3)
      SELECT CASE( CC1 )
        CASE( 1 )
          IF( C(2) .EQ. C(3)      ) CTR = 1
        CASE( 2 )
          IF( CC   .EQ.  9   .OR.            &
              CC   .EQ. 20        ) CTR = 1
        CASE( 3 )
          IF( CC   .EQ.  9   .OR.            &
              CC   .GE. 16        ) CTR = 1
        CASE( 4,5 )
          CTR = 1
      END SELECT
!
      RETURN
      END FUNCTION CTR
