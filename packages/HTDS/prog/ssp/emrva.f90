!
! SMIL CHAMPION MAI 82
!
! MODIFIE 03/98 V. BOUDON ---> XY6/Oh.
!
!     ELEMENTS MATRICIELS REDUITS DES OPERATEURS VIBRATIONNELS
!     ELEMENTAIRES, LES OPERATEURS SONT CARACTERISES PAR :
!                 IVI     = NUMERO DU MODE
!                 IPM     = +1 POUR UN OPERATEUR CREATION
!                         = -1 POUR UN OPERATEUR ANNIHILATION
!                 OMEGA   = DEGRE VIBRATIONNEL < OU = 3
!                 K       = OMEGA , OMEGA-2 , ... , 1 OU 0
!                 N       = MULTIPLICITE (TRIPLEMENT DEGENERE SEULEMENT)
!                 C       = SYMETRIE DANS O (A1,A2,E,F1,F2)
!
      FUNCTION EMRVA(IVI,VP,LP,NP,CP,IPM,OMEGA,K,GAMA,V,L,N,C)
      use mod_dppr
      use mod_par_tds
      use mod_com_sy
      IMPLICIT NONE
      real(kind=dppr)  :: EMRVA
      integer          :: IVI,VP,LP,NP,CP,IPM,OMEGA,K,GAMA,V,L,N,C

! functions
      real(kind=dppr)  :: A1N,A21,A22,A23,A24,A31,A32,A33,A34
      real(kind=dppr)  :: EUFC
      integer          :: IPSI

      real(kind=dppr)  :: ACOEFF

      integer ,dimension(MDMIGA)  :: ICCA2
      integer          :: C1,CA,CA0,CAP,CP1
      integer          :: GAMA1
      integer          :: IDEG
      integer          :: NCCA2
!
      EMRVA = 0.D0
      IDEG  = IVI
      IF( IVI .GT. 3 ) IDEG = 3
      CP1 = CP
      CAP = 1
      IF( IVI       .GT. 4 .AND.         &
          MOD(VP,2) .EQ. 1       ) THEN
       CALL MULTD(CP,2,NCCA2,ICCA2)
       CP1 = ICCA2(1)
       CAP = 2
      ENDIF
      IF( IPSI(IDEG,VP,LP,NP,CP1) .EQ. 0 ) RETURN
      C1 = C
      CA = 1
      IF( IVI      .GT. 4 .AND.         &
          MOD(V,2) .EQ. 1       ) THEN
       CALL MULTD(C,2,NCCA2,ICCA2)
       C1 = ICCA2(1)
       CA = 2
      ENDIF
      IF( IPSI(IDEG,V,L,N,C1) .EQ. 0 ) RETURN
      GAMA1 = GAMA
      CA0   = 1
      IF( IVI          .GT. 4 .AND.         &
          MOD(OMEGA,2) .EQ. 1       ) THEN
       CALL MULTD(GAMA,2,NCCA2,ICCA2)
       GAMA1 = ICCA2(1)
       CA0   = 2
      ENDIF
      IF( IPSI(IDEG,OMEGA,K,0,GAMA1) .EQ. 0 ) RETURN
      ACOEFF = SQRT(DC(C)*DC(CP)*DC(GAMA))*EUFC(GAMA,C,CP,CA0,CA,CAP,GAMA1,C1,CP1)
      IF( IPM*(VP-V) .NE. OMEGA ) RETURN
      IF( OMEGA      .NE. 0     ) GOTO 10
      IF( L-LP       .NE. 0     ) RETURN
      IF( N-NP       .NE. 0     ) RETURN
      IF( C-CP       .NE. 0     ) RETURN
      EMRVA = SQRT(DC(C))
      RETURN
!
10    IF( OMEGA .LT. 0 ) RETURN
      SELECT CASE( IDEG )
        CASE( 1 )
          EMRVA = A1N(IPM,VP,V,OMEGA)
        CASE( 2 )
          SELECT CASE( OMEGA )
            CASE( 1 )
              EMRVA = A21(IPM,VP,LP,CP,V,L,C)
            CASE( 2 )
              EMRVA = A22(IPM,VP,LP,CP,GAMA,V,L,C)
            CASE( 3 )
              EMRVA = A23(IPM,VP,LP,CP,K,GAMA,V,L,C)
            CASE( 4 )
              EMRVA = A24(IPM,VP,LP,CP,K,GAMA,V,L,C)
          END SELECT
        CASE( 3 )
          SELECT CASE( OMEGA )
            CASE( 1 )
              EMRVA = A31(IPM,VP,LP,NP,CP1,V,L,N,C1,0)*ACOEFF
            CASE( 2 )
              EMRVA = A32(IPM,VP,LP,NP,CP1,K,GAMA1,V,L,N,C1,0)*ACOEFF
            CASE( 3 )
              EMRVA = A33(IPM,VP,LP,NP,CP1,K,GAMA1,V,L,N,C1,0)*ACOEFF
            CASE( 4 )
              EMRVA = A34(IPM,VP,LP,NP,CP,K,GAMA,V,L,N,C,0)
          END SELECT
      END SELECT
!
      RETURN
      END FUNCTION EMRVA
