      SUBROUTINE VLNC(ICO,IV,IL,IN,IC,IP)                                                          ! SMIL G.PIERRE JUIN 82
!
!  VLNC MODIFIE MARS 98 ---> XY6/Oh (V. BOUDON).
!
      IMPLICIT NONE
      integer          :: ICO,IV,IL,IN,IC,IP

!
      IV = ICO/10000
      IL = (ICO-10000*IV)/1000
      IN = (ICO-1000*(ICO/1000))/100
      IC = (ICO-100*(ICO/100))/10
      IP = ICO-10*(ICO/10)
!
      RETURN
      END SUBROUTINE VLNC
