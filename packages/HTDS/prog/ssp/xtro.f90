      SUBROUTINE XTRO(ITR)
!
! 2008 AUGUST
! V.BOUDON, CH.WENGER
!
! ####################################################################
! ##                                                                ##
! ##  MATRIX ELEMENTS OF TRANSITION MOMENT CALCULUS IN PROPER BASE  ##
! ##                                                                ##
! ####################################################################
!
! BASED UPON trmomt.f90
!
!
      use mod_dppr
      use mod_par_tds
      use mod_com_xpoly
      use mod_main_xpafit
      IMPLICIT NONE
      integer          :: ITR

      real(kind=dppr)  :: A
      real(kind=dppr)  :: COEFAN,COEFIS
      real(kind=dppr)  :: EMPRO
      real(kind=dppr)  :: TM,TMA,TMI

      integer          :: I,ICK,ICK1,ICL,ICL1,ICLUK,ICLUL,IDEB,IDJ
      integer          :: ICPK,ICPK1,ICPL,ICPL1,ICPLUK,ICPLUL
      integer          :: II,IK,IK1,IL,IL1,IN,IOP,IOP1,IS,ISV
      integer          :: J,JCL,JK,JK1,JKM,JL,JL1,JLUK,JLUL
      integer          :: KK,KL
      integer          :: LG
      integer          :: NBELM,NBOPTC,NBOTR,NBR,NFBK,NFBL
      integer          :: NICK,NICL,NL1,NK1,NNIVI,NNIVS,NSV

      character(len = 120)  :: FXTRO,FXVPI,FXVPS,FSEM

      logical          :: INTRA
!
8000  FORMAT(' !!! XTRO   : STOP ON ERROR')
8101  FORMAT(' !!! ERROR OPENING TRANSITION MOMENT FILE : ',A)
8102  FORMAT(' !!! ERROR OPENING LOWER EIGENVAL. AND VECT. FILE : ',A)
8103  FORMAT(' !!! ERROR OPENING UPPER EIGENVAL. AND VECT. FILE : ',A)
8105  FORMAT(' !!! ERROR OPENING MATRIX ELEMENT FILE : ',A)
8106  FORMAT(' !!! LOWER LEVEL FILE => UNEXPECTED EOF')
8107  FORMAT(' !!! UPPER LEVEL FILE => UNEXPECTED EOF')
8108  FORMAT(' !!! TRANSITION MOMENT FILE ',a,' => UNEXPECTED EOF')
8109  FORMAT(' !!! J,C,P ERROR')
!
      FXVPI = 'XVP_P'//CPOL(INPI(ITR))//'_'
      FXVPS = 'XVP_P'//CPOL(INPS(ITR))//'_'
      IF( FXVPI .EQ. FXVPS ) THEN
        INTRA = .TRUE.
      ELSE
        INTRA = .FALSE.
      ENDIF
      IF( ISRAM(ITR) ) THEN
! RAMAN INTENSITY COEFFICIENTS
        IF    ( POLST(ITR) .EQ. 'R111' ) THEN
          COEFIS = SQRT(1.D0/3.D0)
          COEFAN = SQRT(7.D0/30.D0)
        ELSEIF( POLST(ITR) .EQ. 'R110' ) THEN
          COEFIS = SQRT(1.D0/3.D0)
          COEFAN = SQRT(4.D0/30.D0)
        ELSEIF( POLST(ITR) .EQ. 'R001' ) THEN
          COEFIS = 0.D0
          COEFAN = SQRT(3.D0/30.D0)
        ENDIF
        FSEM = 'PO_P'//CPOL(INPS(ITR))//'mP'//CPOL(INPI(ITR))//'_D'//TRIM(CDVO(ITR))//'_'          ! polarizability matrix elements
        NBR  = 2
      ELSE
        FSEM = 'DI_P'//CPOL(INPS(ITR))//'mP'//CPOL(INPI(ITR))//'_D'//TRIM(CDVO(ITR))//'_'          ! dipolar moment matrix elements
        NBR  = 1
      ENDIF
      FXTRO = 'XTRO_'//CTR(ITR)                                                                    ! transition moment and derivatives
      NNIVS = NNIV(INPS(ITR))
      NNIVI = NNIV(INPI(ITR))
      IDEB  = INPHT(INTRT(ITR))                                                                    ! index-1 of first T parameter
!
! FILES
!
      OPEN(80,ERR=4081,FILE=FXTRO,STATUS='UNKNOWN',FORM='UNFORMATTED')                             ! transition moment file
      IF( INTRA ) THEN
        OPEN(60,STATUS='SCRATCH',FORM='UNFORMATTED')
      ELSE
        OPEN(60,ERR=4060,FILE=FXVPI,STATUS='OLD',FORM='UNFORMATTED')                               ! lower level file
      ENDIF
      OPEN(61,ERR=4061,FILE=FXVPS,STATUS='OLD',FORM='UNFORMATTED')                                 ! upper level file
      OPEN(40,ERR=4040,FILE=FSEM,STATUS='OLD',FORM='UNFORMATTED')                                  ! matrix element file
!
! *** READ H PARAMETERS
!     FROM UPPER POLYAD EIGENVECTORS/VALUES FILE
!     AND COPY TO OUTPUT FILE
!
      IF( INTRA ) THEN
!
5000    READ (61,END=5002) JLUL,ICLUL,ICPLUL,NFBL
        WRITE(60)          JLUL,ICLUL,ICPLUL,NFBL
        DO WHILE( NFBL .GT. MXDIMI )
          CALL RESIZE_MXDIMI
        ENDDO
        IF( NFBL .EQ. 0 ) GOTO 5000
        READ (61) (HDL(I),I=1,NFBL)
        WRITE(60) (HDL(I),I=1,NFBL)
        DO IL=1,NFBL
          READ (61) NICL,ICLRO,(TL(IL,I),I=1,NFBL)
          WRITE(60) NICL,ICLRO,(TL(IL,I),I=1,NFBL)
        ENDDO
        GOTO 5000
!
5002    ENDFILE(60)
        REWIND(61)
      ENDIF
!
! *** READ HEADING PART OF TRANSITION MOMENT MATRIX ELEMENT FILE
!
      DO I=1,8+NNIVI+NNIVS
        READ(40)
      ENDDO
      DO IN=1,2                                                                                    ! 2 levels
        READ(40) NSV
        DO ISV=1,NSV
          READ(40)
        ENDDO
      ENDDO
      DO I=1,4
        READ(40)
      ENDDO
      READ(40) NBOPTC
      DO WHILE( NBOPTC .GT. MXOPT )
        CALL RESIZE_MXOPT
      ENDDO
      NBOTR = NBOPTC
      WRITE(80) NBOTR
!
! *** UPPER LEVEL LOOP (K=SUP)
! *** JSUP LOOP
!
      JKM = JMAX(INPS(ITR))
      IF( INPS(ITR) .EQ. INPI(ITR) ) JKM = JKM-1
E3:   DO JK=0,JKM
!
! *** RESET POSITION OF LOWER POLYAD EIGENVECTORS FILE
!
        REWIND(60)                                                                                 ! reread heading part of lower file
!
! *** STORE DIMENSIONS AND EIGENVECTORS/VALUES OF LOWER POLYAD
!     FOR THE 3 OR 5 BRANCHES *MXSYM*2 VALUES OF ICINF,ICPINF
!
        DO I=1,MDMJCI
          JDIM(I) = 0
        ENDDO
        DO JL=MAX(JK-NBR,0),JK+NBR
          DO ICL=1,MXSYM
            DO ICPL=1,2
!
6666          READ(60,END=67) JLUL,ICLUL,ICPLUL,NFBL
              IF( JLUL .GT. JK+NBR ) GOTO 67
              DO WHILE( NFBL .GT. MXDIMI )
                CALL RESIZE_MXDIMI
              ENDDO
              IF( NFBL .NE. 0      ) THEN
                READ(60,END=4160) (HDL(I),I=1,NFBL)
                DO IL=1,NFBL
                  READ(60,END=4160) NICL,ICLRO,(TL(IL,I),I=1,NFBL)
                ENDDO
              ENDIF
              IF( JLUL .LT. JL ) GOTO 6666
              JCL       = (JLUL-JK+NBR)*MXSYM*2+(ICLUL-1)*2+ICPLUL
              JDIM(JCL) = NFBL
              DO I=1,NFBL
                HDLJ(I,JCL) = HDL(I)
                DO J=1,NFBL
                  TLJ(J,I,JCL) = TL(J,I)
                ENDDO
              ENDDO
            ENDDO
          ENDDO
        ENDDO
!
! *** ICSUP LOOP
!
67      CONTINUE
E1:     DO ICK=1,MXSYM
E103:     DO ICPK=1,2
!
! *** STORE DIMENSIONS AND EIGENVECTORS/VALUES OF UPPER POLYAD
!     FOR JSUP,ICSUP,ICPSUP BLOCK
!
            READ(61,END=4161) JLUK,ICLUK,ICPLUK,NFBK
            READ(40,END=4140) JK1,ICK1,ICPK1,NK1
            IF( JK   .NE. JK1    .OR.              &
                ICK  .NE. ICK1   .OR.              &
                ICPK .NE. ICPK1       ) GOTO 4151
            IF( NFBK .EQ. 0           ) CYCLE E103
            IF( JK   .NE. JLUK   .OR.         &
                ICK  .NE. ICLUK  .OR.         &
                ICPK .NE. ICPLUK      ) THEN
              BACKSPACE(61)
              CYCLE E103
            ENDIF
            READ(61,END=4161) (HDK(I),I=1,NFBK)
            DO IK=1,NFBK
              READ(61,END=4161) NICK,ICKRO,(TK(IK,I),I=1,NFBK)
            ENDDO
!
! ******************************************************
! *** TRANSITION MATRIX IN BASE FONCTIONS
! ******************************************************
!
! *** STORE MATRIX ELEMENTS IN BASE FONCTIONS
!
E6:         DO JL=MAX(JK-NBR,0),JK+NBR
              READ(40,END=4140) JK1,ICK1,ICPK1,NK1,  &
                                JL1,ICL1,ICPL1,NL1
              ICL  = ICL1
              ICPL = ICPL1
              JCL  = (JL-JK+NBR)*MXSYM*2+(ICL-1)*2+ICPL
              IF( JL  .NE. JL1 ) GOTO 4151
              IF( NL1 .EQ. 0   ) CYCLE E6
              IDJ = JL-JK+NBR+1
!
! *** INITIALIZE DIP(IK1,IL1,IDG,IOP)
!
              DO IOP=1,NBOPTC
                DO IS=1,MXDIMS
                  DO II=1,MXDIMI
                    DIP(IS,II,IDJ,IOP) = 0.D0
                  ENDDO
                ENDDO
              ENDDO
!
! *** OPERATOR LOOP
!
              SELECT CASE( NBR )
!
! *** DIPOLAR MOMENT
!
                CASE( 1 )
E301:             DO IOP=1,NBOPTC
!
146                 READ(40,END=4140) JK1,ICK1,ICPK1,NK1,  &
                                      JL1,ICL1,ICPL1,NL1,  &
                                      IOP1,NBELM
                    DO WHILE( NBELM .GT. MXELMT )
                      CALL RESIZE_MXELMT
                    ENDDO
                    IF( JL    .EQ. JL1   .AND.             &
                        JK    .EQ. JK1   .AND.             &
                        ICK   .EQ. ICK1  .AND.             &
                        ICPK  .EQ. ICPK1 .AND.             &
                        IOP   .EQ. IOP1        ) GOTO 150
                    IF( NBELM .NE. 0           ) READ(40,END=4140)
                    GOTO 146
!
150                 IF( NBELM .EQ. 0 ) CYCLE E301
                    READ(40,END=4140) (LITR(I),KOTR(I),HTR(I),I=1,NBELM)
                    DO I=1,NBELM
                      IK1                  = KOTR(I)
                      IL1                  = LITR(I)
                      DIP(IK1,IL1,IDJ,IOP) = HTR(I)
                    ENDDO
                  ENDDO E301
!
! *** POLARIZABILITY
!
                CASE( 2 )
E351:             DO IOP=1,NBOPTC
!
246                 READ(40,END=4140) JK1,ICK1,ICPK1,NK1,  &
                                      JL1,ICL1,ICPL1,NL1,  &
                                      IOP1,NBELM,LG
                    IF( JL    .EQ. JL1   .AND.             &
                        JK    .EQ. JK1   .AND.             &
                        ICK   .EQ. ICK1  .AND.             &
                        ICPK  .EQ. ICPK1 .AND.             &
                        IOP   .EQ. IOP1        ) GOTO 250
                    IF( NBELM .NE. 0           ) READ(40,END=4140)
                    GOTO 246
!
250                 IF( NBELM .EQ. 0 ) CYCLE E351
                    ICOEFI(IOP) = 1
                    IF( LG .EQ. 2 ) ICOEFI(IOP) = 2
                    READ(40,END=4140) (LITR(I),KOTR(I),HTR(I),I=1,NBELM)
                    DO I=1,NBELM
                      IK1                  = KOTR(I)
                      IL1                  = LITR(I)
                      DIP(IK1,IL1,IDJ,IOP) = HTR(I)
                    ENDDO
                  ENDDO E351
              END SELECT
!
! ******************************************************
! *** TRANSITION MATRIX IN  PROPER FONCTIONS
! ******************************************************
!
! *** (O) P Q R (S) LOOP ON LOWER LEVELS (L=INF)
!
              NFBL = JDIM(JCL)
              IF( NFBL .EQ. 0 ) CYCLE E6
              DO I=1,NFBL
                HDL(I) = HDLJ(I,JCL)
                DO J=1,NFBL
                  TL(J,I) = TLJ(J,I,JCL)
                ENDDO
              ENDDO
              IDJ = JL-JK+NBR+1
              WRITE(80) JK,ICK,ICPK,NFBK,  &
                        JL,ICL,ICPL,NFBL
!
! *** UPPER LEVEL LOOP (H=SUP)
!
E4:           DO IK=1,NFBK
!
! *** LOWER LEVEL LOOP
!
E14:            DO IL=1,NFBL
                  TM  = 0.D0
                  TMI = 0.D0
                  TMA = 0.D0
!
! *** OPERATOR LOOP
!
E114:             DO IOP=1,NBOPTC
                    EMPRO = 0.D0
                    DO KL=1,NFBL
                      A = 0.D0
                      DO KK=1,NFBK
                        A = A+DIP(KK,KL,IDJ,IOP)*TK(KK,IK)
                      ENDDO
                      EMPRO = EMPRO+A*TL(KL,IL)
                    ENDDO
                    DERITR(IOP) = EMPRO                                                            ! --> derivatives % parameters
                    IF( ISRAM(ITR) ) THEN
                      IF( ICOEFI(IOP) .EQ. 1 ) THEN
                        TMI = TMI+EMPRO*PARA(IDEB+IOP)*COEFIS
                      ELSE
                        TMA = TMA+EMPRO*PARA(IDEB+IOP)*COEFAN
                      ENDIF
                    ELSE
                      TM = TM+EMPRO*PARA(IDEB+IOP)                                                 ! --> transition moment matrix elements
                    ENDIF
                  ENDDO E114                                                                       ! operator loop
                  IF( ISRAM(ITR) ) THEN
                    TM = SQRT(TMI*TMI+TMA*TMA)
                    DO IOP = 1,NBOPTC
                      IF( ICOEFI(IOP) .EQ. 1 ) THEN
                        DERITR(IOP) = 2.D0*DERITR(IOP)*TMI*COEFIS
                      ELSE
                        DERITR(IOP) = 2.D0*DERITR(IOP)*TMA*COEFAN
                      ENDIF
                    ENDDO
                  ENDIF
                  WRITE(80) IK,IL,TM,(DERITR(IOP),IOP=1,NBOPTC)
                ENDDO E14                                                                          ! lower level loop
              ENDDO E4                                                                             ! upper level lopp (NFB)
            ENDDO E6                                                                               ! lower level PQR loop
          ENDDO E103                                                                               ! upper level loop (P)
        ENDDO E1                                                                                   ! upper level loop (C)
      ENDDO E3                                                                                     ! upper level loop (J)
      CLOSE(80)
      GOTO 9000
!
4081  PRINT 8101, FXTRO
      GOTO  9999
4060  PRINT 8102, FXVPI
      GOTO  9999
4061  PRINT 8103, FXVPS
      GOTO  9999
4040  PRINT 8105, FSEM
      GOTO  9999
4160  PRINT 8106
      GOTO  9999
4161  PRINT 8107
      GOTO  9999
4140  PRINT 8108, 'P'//CPOL(INPS(ITR))//'mP'//CPOL(INPI(ITR))
      GOTO  9999
4151  PRINT 8109
      GOTO  9999
9999  PRINT 8000
      PRINT *
      STOP
!
9000  CLOSE(40)
      CLOSE(60)
      CLOSE(61)
!
      RETURN
      END SUBROUTINE XTRO
