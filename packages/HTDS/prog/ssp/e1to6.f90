!
! E.M.R.V. COUPLAGE EXTERNE
!
!
! SMIL G.PIERRE JUIN 82
! MOD. T.GABARD DEC 92
! E1234 MODIFIE 03/98 V. BOUDON ---> XY6/Oh.
!
!     E1234=<(IB12345*IB6)//(IA12345*IA6)//(IK12345*IK6)>
!     IPM=+1 POUR UN OPERATEUR CREATION
!     IPM=-1 POUR UN OPERATEUR ANNIHILATION
!
      FUNCTION E1TO6(IPM,IB,IA,IK)
      use mod_dppr
      use mod_par_tds
      use mod_com_sy
      IMPLICIT NONE
      real(kind=dppr)  :: E1TO6
      integer ,dimension(NBVQN+2)  :: IB,IA,IK
      integer          :: IPM

! functions
      real(kind=dppr)  :: E12345,EMRVA,EUFC

      real(kind=dppr)  :: EUF
      real(kind=dppr)  :: R12345,R6,RC

      integer          :: IA2,IA23,IA234,IA2345,IATO6
      integer          :: IAC6,IAL6,IAN6,IAP6,IAV6
      integer          :: IB2,IB23,IB234,IB2345,IBTO6
      integer          :: IBC6,IBL6,IBN6,IBP6,IBV6
      integer          :: IK2,IK23,IK234,IK2345,IKTO6
      integer          :: IKC6,IKL6,IKN6,IKP6,IKV6
!
      E1TO6 = 0.D0
      CALL VLNC(IB(6),IBV6,IBL6,IBN6,IBC6,IBP6)
      CALL VLNC(IA(6),IAV6,IAL6,IAN6,IAC6,IAP6)
      CALL VLNC(IK(6),IKV6,IKL6,IKN6,IKC6,IKP6)
      IF( IAN6 .NE. 0 ) RETURN
      CALL VLNC(IB(7),IB2,IB23,IB234,IB2345,IBTO6)
      CALL VLNC(IA(7),IA2,IA23,IA234,IA2345,IATO6)
      CALL VLNC(IK(7),IK2,IK23,IK234,IK2345,IKTO6)
      RC     = SQRT(DC(IKTO6)*DC(IATO6)*DC(IBTO6))
      EUF    = EUFC(IB2345,IBC6,IBTO6,IA2345,IAC6,IATO6,IK2345,IKC6,IKTO6)
      R6     = EMRVA(6,IBV6,IBL6,IBN6,IBC6,IPM,IAV6,IAL6,IAC6,IKV6,IKL6,IKN6,IKC6)
      R12345 = E12345(IPM,IB,IA,IK)
      E1TO6  = RC*EUF*R12345*R6
!
      RETURN
      END FUNCTION E1TO6
