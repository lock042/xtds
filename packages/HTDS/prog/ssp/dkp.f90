!
!  EXTRAIRE, PAR TRI DICHOTOMIQUE, UN K CALCULE PAR CALKP.
!  S'IL N'EST PAS DANS LE TABLEAU, ON LE RESTITUE COMME 0.
!  POUR UTILISATION DANS LE PROGRAMME POL_MATRIX.F
!
! REV    JAN 1995 JPC,CW (PARAMETER)
!
      FUNCTION DKP(J1,J3,N2,N3,IC1,IC2,IC3)
      use mod_dppr
      use mod_com_ckdipo
      use mod_com_ckpo
      use mod_com_sy
      IMPLICIT NONE
      real(kind=dppr)  :: DKP
      integer          :: J1,J3,N2,N3,IC1,IC2,IC3

      real(kind=dppr)  :: PHAS

      integer          :: I,I1,I2,ICR2,ICR3,IDKXC
      integer          :: NR2,NR3
!
      IF( IC2 .LT. IC3 ) THEN
        NR2  = N3
        NR3  = N2
        ICR2 = IC3
        ICR3 = IC2
        PHAS = PC(ICR2)*PC(ICR3)*( (-1)**J1 )*PC(IC1)
      ELSE
        NR2  = N2
        NR3  = N3
        ICR2 = IC2
        ICR3 = IC3
        PHAS = 1.D0
      ENDIF
      DKP   = 0.D0
      IDKXC = 10000000*J1+1000000*IC1+100000*ICR3+10000*ICR2+100*NR3+NR2
      I1    = INJDK(J3)
      I2    = INJDK(J3+1)-1
      IF( I2    .EQ. 0        ) RETURN
      IF( IDKXC .LT. IDKX(I1) ) RETURN
      IF( IDKXC .GT. IDKX(I2) ) RETURN
      IF( IDKXC .EQ. IDKX(I2) ) GOTO 4
      IF( I1    .EQ. I2       ) RETURN
!
1     I = (I1+I2)/2
      IF    ( IDKXC .EQ. IDKX(I) ) THEN
        GOTO 5
      ELSEIF( IDKXC .GT. IDKX(I) ) THEN
        GOTO 3
      ENDIF
      IF( I2-I1 .EQ. 1 ) RETURN
      I2 = I
      GOTO 1
!
3     IF( I2-I1 .EQ. 1 ) RETURN
      I1 = I
      GOTO 1
!
4     I = I2
!
5     DKP = VDK(I)*PHAS
!
      RETURN
      END FUNCTION DKP
