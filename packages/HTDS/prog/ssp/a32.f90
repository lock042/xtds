!
! SMIL CHAMPION MAI 82,MOD JAN. 83
! MOD. DEC 92 T.GABARD
! MODIFIE 03/98 V. BOUDON ---> XY6/Oh.
!
      FUNCTION A32(IPM,VP,LP,NP,CP,K,GAMA,V,L,N,C,IO3TD)
      use mod_dppr
      use mod_par_tds
      IMPLICIT NONE
      real(kind=dppr)  :: A32
      integer          :: IPM,VP,LP,NP,CP,K,GAMA,V,L,N,C,IO3TD

! functions
      real(kind=dppr)  :: A31,SXJ,XMKUB
      integer          :: IPSI

      real(kind=dppr)  :: AK
      real(kind=dppr)  :: E

      integer          :: CS
      integer          :: IUG,IUGP
      integer          :: LD,LS
      integer          :: VS
!
      A32 = 0.D0
      IF( IO3TD              .EQ. 0 .AND.           &
          IPSI(3,2,K,0,GAMA) .EQ. 0       ) RETURN
      IF( IPM*(VP-V)         .NE. 2       ) RETURN
      VS = V+IPM
E2:   DO LD=-1,1,2
        LS = L+LD
        DO CS=1,MXSYM
          IF( IPSI(3,VS,LS,0,CS) .NE. 0 ) GOTO 20
        ENDDO
        CYCLE E2
!
20      E = A31(IPM,VP,LP,NP,CP,VS,LS,0,CS,1)*  &
            A31(IPM,VS,LS,0,CS,V,L,N,C,1)
        IF( E .EQ. 0.D0 ) CYCLE E2
        A32 = A32+E*( (-1)**(L+LP+2) )*SXJ(1,1,K,L,LP,LS)
      ENDDO E2
      A32 = A32*SQRT(2.D0*K+1.D0)
      IF( IO3TD .NE. 0 ) RETURN
      IUG  = 1
      IUGP = 1
      AK   = XMKUB(K,L,LP,1,IUG,IUGP,0,N,NP,GAMA,C,CP)
      A32  = ( (-1)**LP )*AK*A32
!
      RETURN
      END FUNCTION A32
