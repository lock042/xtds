!
!     RMS for intensities
!
      FUNCTION RMST(M,ISEL)
      use mod_dppr
      use mod_par_tds
      use mod_par_x
      use mod_com_xassi
      use mod_com_xfonc
      IMPLICIT NONE
      real(kind=dppr)        :: RMST
      integer                :: M
      integer ,dimension(M)  :: ISEL

! functions
      real(kind=dppr)  :: ENORM

      real(kind=dppr) ,dimension(MXOBZ)  :: FVECA
      real(kind=dppr)  :: SCAL

      integer          :: I,IOBS
      integer          :: J
      integer          :: MA
!
      MA = 0
      DO I=1,M
        J    = ISEL(I)
        IOBS = INDOBS(J)
        IF( SASOZ(J) .NE. '-' ) THEN
          SCAL = SOBS(IOBS)-FOMC(J)/ZWGT(J)
          IF( SCAL .NE. 0.D0 ) THEN
            MA        = MA+1
            FVECA(MA) = (FOMC(J)/ZWGT(J))/SCAL
          ENDIF
        ENDIF
      ENDDO
      RMST = ENORM(MA,FVECA)
      RMST = RMST/SQRT(DBLE(MA))
!
      RETURN
      END FUNCTION RMST
