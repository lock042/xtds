!
! *** SYMBOLES 6 - C DE TD
!
! D'APRES PROG. ORSAY 80
!
      FUNCTION SXC1(C1,C2,C3,C4,C5,C6)
      use mod_dppr
      use mod_par_tds
      use mod_com_sy
      IMPLICIT NONE
      real(kind=dppr)  :: SXC1
      integer          :: C1,C2,C3,C4,C5,C6

! functions
      real(kind=dppr)  :: TROIC

      real(kind=dppr)  :: F1,F2,F3,F4

      integer          :: I1,I2,I3,I4,I5,I6
!
      SXC1 = 0.D0
      DO I1=1,INT(DC(C1))
        DO I2=1,INT(DC(C2))
          DO I3=1,INT(DC(C3))
            F1 = TROIC(C1,C2,C3,I1,I2,I3)
            IF( F1 .NE. 0.D0 ) GOTO 2
          ENDDO
        ENDDO
      ENDDO
      RETURN
!
2     DO I4=1,INT(DC(C4))
E11:    DO I5=1,INT(DC(C5))
          F2 = TROIC(C4,C5,C3,I4,I5,I3)
          IF( F2 .EQ. 0.D0 ) CYCLE E11
E12:      DO I6=1,INT(DC(C6))
            F3 = TROIC(C1,C5,C6,I1,I5,I6)
            IF( F3 .EQ. 0.D0 ) CYCLE E12
            F4 = TROIC(C4,C2,C6,I4,I2,I6)
            IF( F4 .EQ. 0.D0 ) CYCLE E12
            SXC1 = SXC1+F2*F3*F4
          ENDDO E12
        ENDDO E11
      ENDDO
      SXC1 = SXC1/F1
!
      RETURN
      END FUNCTION SXC1
