!
! *** RELATION TRIANGULAIRE DANS O(3).
!
! SMIL J.HILICO JAN 70
!
      FUNCTION NTR(J1,J2,J3)
      IMPLICIT NONE
      integer          :: NTR
      integer          :: J1,J2,J3

!
      NTR = 1
      IF( J3 .LT. ABS(J1-J2) .OR.            &
          J3 .GT. (J1+J2)         ) NTR = 0
!
      RETURN
      END FUNCTION NTR
