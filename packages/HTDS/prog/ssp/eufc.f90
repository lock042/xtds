!
! *** SYMBOLES 9 - C DE TD CALCULES A PARTIR DES 6 - C
!     (VOIR PAR EXEMPLE : ROTENBERG ET AL. 1959)
!
! SMIL CHAMPION  DEC 78
!
      FUNCTION EUFC(IC1,IC2,IC3,IC4,IC5,IC6,IC7,IC8,IC9)
      use mod_dppr
      use mod_par_tds
      use mod_com_sy
      IMPLICIT NONE
      real(kind=dppr)  :: EUFC
      integer          :: IC1,IC2,IC3,IC4,IC5,IC6,IC7,IC8,IC9

! functions
      real(kind=dppr)  :: SXC

      real(kind=dppr)  :: S1,S2,S3

      integer          :: IC
!
      EUFC = 0.D0
E1:   DO IC=1,MXSYM
        S1 = SXC(IC1,IC2,IC3,IC6,IC9,IC)
        IF( S1 .EQ. 0.D0 ) CYCLE E1
        S2 = SXC(IC4,IC5,IC6,IC2,IC,IC8)
        IF( S2 .EQ. 0.D0 ) CYCLE E1
        S3 = SXC(IC7,IC8,IC9,IC,IC1,IC4)
        IF( S3 .EQ. 0.D0 ) CYCLE E1
        EUFC = EUFC+DC(IC)*S1*S2*S3
      ENDDO E1
!
      RETURN
      END FUNCTION EUFC
