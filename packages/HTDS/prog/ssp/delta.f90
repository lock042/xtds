!
! *** FONCTION DELTA (ROTENBERG ET AL., 1959)
!                    (OU FANO ET RACAH, 1959)
!
! REV. 6/10/94
!
      SUBROUTINE DELTA(IA,IB,IC,Z)
      use mod_dppr
      use mod_com_fa
      IMPLICIT NONE
      real(kind=dppr)  :: Z
      integer          :: IA,IB,IC

! functions
      integer          :: NTR

      integer ,dimension(3)  :: IT
      integer          :: I,IL
      integer          :: JT
      integer          :: K
!
      Z = 0.D0
      IF( NTR(IA,IB,IC) .EQ. 0 ) RETURN
      IT(1) = IA+IB-IC
      IT(2) = IA+IC-IB
      IT(3) = IB+IC-IA
      IL    = IA+IB+IC+1
      Z     = 1.D0
      JT    = 0
E1:   DO I=1,3
        K = IT(I)
        IF    ( K .LT. 0 ) THEN
          GOTO 999
        ELSEIF( K .EQ. 0 ) THEN
          CYCLE E1
        ELSE
          Z  = Z*FACT(K)
          JT = JT+KFAC(K)
        ENDIF
      ENDDO E1
      JT = JT-KFAC(IL)
      Z  = Z/FACT(IL)
      Z  = Z*( (10.D0)**JT )
      Z  = SQRT(Z)
      RETURN
!
999   Z = 0.D0
!
      RETURN
      END SUBROUTINE DELTA
