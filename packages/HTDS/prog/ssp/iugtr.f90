!
! *** RELATION TRIANGULAIRE SUR LES PARITES DANS O(3).
!     IUG = 1, PARITE G
!     IUG = 2, PARITE U
!
      FUNCTION IUGTR(IUG1,IUG2,IUG3)
      IMPLICIT NONE
      integer          :: IUGTR
      integer          :: IUG1,IUG2,IUG3

!
      IUGTR = 0
      IF( IUG1 .EQ. 1 .AND.              &
          IUG2 .EQ. 1 .AND.              &
          IUG3 .EQ. 1       ) IUGTR = 1
      IF( IUG1 .EQ. 2 .AND.              &
          IUG2 .EQ. 2 .AND.              &
          IUG3 .EQ. 1       ) IUGTR = 1
      IF( IUG1 .EQ. 2 .AND.              &
          IUG2 .EQ. 1 .AND.              &
          IUG3 .EQ. 2       ) IUGTR = 1
      IF( IUG1 .EQ. 1 .AND.              &
          IUG2 .EQ. 2 .AND.              &
          IUG3 .EQ. 2       ) IUGTR = 1
!
      RETURN
      END FUNCTION IUGTR
