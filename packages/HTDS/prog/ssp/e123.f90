!
!  E.M.R.V. COUPLAGE EXTERNE
!
!
! SMIL G.PIERRE JUIN 82
! MOD. T.GABARD DEC 92
! E123 MODIFIE 03/98 V. BOUDON ---> XY6/Oh.
!
!     E123=<(IB12*IB3)//(IA12*IA3)//(IK12*IK3)>
!     IPM=+1 POUR UN OPERATEUR CREATION
!     IPM=-1 POUR UN OPERATEUR ANNIHILATION
!
      FUNCTION E123(IPM,IB,IA,IK)
      use mod_dppr
      use mod_par_tds
      use mod_com_sy
      IMPLICIT NONE
      real(kind=dppr)  :: E123
      integer ,dimension(NBVQN+2)  :: IB,IA,IK
      integer          :: IPM

! functions
      real(kind=dppr)  :: E12,EMRVA,EUFC

      real(kind=dppr)  :: EUF
      real(kind=dppr)  :: R12,R3,RC

      integer          :: IA2,IA23,IA234,IA2345,IATO6
      integer          :: IAC3,IAL3,IAN3,IAP3,IAV3
      integer          :: IB2,IB23,IBP3,IB234,IB2345,IBTO6
      integer          :: IBC3,IBL3,IBN3,IBV3
      integer          :: IK2,IK23,IK234,IK2345,IKTO6
      integer          :: IKC3,IKL3,IKN3,IKP3,IKV3
!
      E123 = 0.D0
      CALL VLNC(IB(3),IBV3,IBL3,IBN3,IBC3,IBP3)
      CALL VLNC(IA(3),IAV3,IAL3,IAN3,IAC3,IAP3)
      CALL VLNC(IK(3),IKV3,IKL3,IKN3,IKC3,IKP3)
      IF( IAN3 .NE. 0 ) RETURN
      CALL VLNC(IB(7),IB2,IB23,IB234,IB2345,IBTO6)
      CALL VLNC(IA(7),IA2,IA23,IA234,IA2345,IATO6)
      CALL VLNC(IK(7),IK2,IK23,IK234,IK2345,IKTO6)
      RC   = SQRT(DC(IK23)*DC(IA23)*DC(IB23))
      EUF  = EUFC(IB2,IBC3,IB23,IA2,IAC3,IA23,IK2,IKC3,IK23)
      R3   = EMRVA(3,IBV3,IBL3,IBN3,IBC3,IPM,IAV3,IAL3,IAC3,IKV3,IKL3,IKN3,IKC3)
      R12  = E12(IPM,IB,IA,IK)
      E123 = RC*EUF*R12*R3
!
      RETURN
      END FUNCTION E123
