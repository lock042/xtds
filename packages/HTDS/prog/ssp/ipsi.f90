!
! SMIL CHAMPION MAI 82
! MOD. GABARD   DEC 92
! MODIF V. BOUDON 03/98 ---> XY6/Oh.
!
! LE TENSEUR "FONCTION D'ONDE" D'UN OSCILLATEUR HARMONIQUE (1 FOIS, 2
! FOIS OU 3 FOIS DEGENERE) ORIENTE DANS TD PEUT ETRE CARACTERISE PAR 5
! INDICES :
!            DEG      DEGENERESCENCE (1,2 OU 3)
!            IV       NB QUANTIQUE VIBRATIONNEL PRINCIPAL   0,1,2...
!            L        NB QUANTIQUE VIBRATIONNEL SECONDAIRE  V,V-2,V-4..
!                     ...1 OU 0
!            IC       SYMETRIE DANS O (1,2,3,4,5 POUR A1,A2,E,F1,F2
!                     RESP.)
!            N        NUMEROTATION DES COMPOSANTES CUBIQUES NORMALES
!                     SUIVANT J.M.B. POUR L'OSCILLATEUR 3 FOIS DEGENERE
!                     N = 0,1...
!
! LA FONCTION IPSI  = 1 SI LES NOMBRES QUANTIQUES SONT COHERENTS
!                     0 SINON
!
!
      FUNCTION IPSI(DEG,V,L,N,C)
      IMPLICIT NONE
      integer          :: IPSI
      integer          :: DEG,V,L,N,C

! functions
      integer          :: NSYM1

      integer          :: IUG
      integer          :: NBC
!
      IPSI = 0
      IF( DEG .LT. 1 .OR.           &
          DEG .GT. 3      ) RETURN
      IF( V   .LT. 0 .OR.           &
          L   .LT. 0      ) RETURN
      IF( DEG .GT. 1      ) GOTO 1
      IF( L   .NE. 0 .OR.           &
          N   .NE. 0 .OR.           &
          C   .NE. 1      ) RETURN
      IPSI = 1
      RETURN
!
1     IF( L          .GT. V ) RETURN
      IF( MOD(V-L,2) .NE. 0 ) RETURN
      IF( DEG        .GT. 2 ) GOTO 3
      IF( N          .NE. 0 ) RETURN
      IF( L          .NE. 0 ) GOTO 2
      IF( C          .NE. 1 ) RETURN
      IPSI = 1
      RETURN
!
2     IF( MOD(L,3) .NE. 0 .AND.           &
          C        .NE. 3       ) RETURN
      IF( MOD(L,3) .EQ. 0 .AND.           &
          C        .GT. 2       ) RETURN
      IPSI = 1
      RETURN
!
3     IUG = 1
      NBC = NSYM1(L,IUG,C)
      IF( NBC .EQ. 0     ) RETURN
      IF( N   .GT. NBC-1 ) RETURN
      IPSI = 1
!
      RETURN
      END FUNCTION IPSI
