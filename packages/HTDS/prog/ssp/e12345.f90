!
! E.M.R.V. COUPLAGE EXTERNE
!
!
! SMIL G.PIERRE JUIN 82
! MOD. T.GABARD DEC 92
! E1234 MODIFIE 03/98 V. BOUDON ---> XY6/Oh.
!
!     E123445=<(IB1234*IB5)//(IA1234*IA5)//(IK1234*IK5)>
!     IPM=+1 POUR UN OPERATEUR CREATION
!     IPM=-1 POUR UN OPERATEUR ANNIHILATION
!
      FUNCTION E12345(IPM,IB,IA,IK)
      use mod_dppr
      use mod_par_tds
      use mod_com_sy
      IMPLICIT NONE
      real(kind=dppr)  :: E12345
      integer ,dimension(NBVQN+2)  :: IB,IA,IK
      integer          :: IPM

! functions
      real(kind=dppr)  :: E1234,EMRVA,EUFC

      real(kind=dppr)  :: EUF
      real(kind=dppr)  :: R1234,R5,RC

      integer          :: IA2,IA23,IA234,IA2345,IATO6
      integer          :: IAC5,IAL5,IAN5,IAP5,IAV5
      integer          :: IB2,IB23,IB234,IB2345,IBTO6
      integer          :: IBC5,IBL5,IBN5,IBP5,IBV5
      integer          :: IK2,IK23,IK234,IK2345,IKTO6
      integer          :: IKC5,IKL5,IKN5,IKP5,IKV5
!
      E12345 = 0.D0
      CALL VLNC(IB(5),IBV5,IBL5,IBN5,IBC5,IBP5)
      CALL VLNC(IA(5),IAV5,IAL5,IAN5,IAC5,IAP5)
      CALL VLNC(IK(5),IKV5,IKL5,IKN5,IKC5,IKP5)
      IF( IAN5 .NE. 0 ) RETURN
      CALL VLNC(IB(7),IB2,IB23,IB234,IB2345,IBTO6)
      CALL VLNC(IA(7),IA2,IA23,IA234,IA2345,IATO6)
      CALL VLNC(IK(7),IK2,IK23,IK234,IK2345,IKTO6)
      RC     = SQRT(DC(IK2345)*DC(IA2345)*DC(IB2345))
      EUF    = EUFC(IB234,IBC5,IB2345,IA234,IAC5,IA2345,IK234,IKC5,IK2345)
      R5     = EMRVA(5,IBV5,IBL5,IBN5,IBC5,IPM,IAV5,IAL5,IAC5,IKV5,IKL5,IKN5,IKC5)
      R1234  = E1234(IPM,IB,IA,IK)
      E12345 = RC*EUF*R1234*R5
!
      RETURN
      END FUNCTION E12345
