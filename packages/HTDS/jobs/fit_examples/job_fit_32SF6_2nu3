#! /bin/sh
 set -v
##
## Example of parameter fitting :
## 2nu3 of 32SF6.
##
BASD=will be set by *tds_INSTALL
##
 SCRD=$BASD/prog/exe
 PARD=$BASD/para/32SF6
 EXPD=$BASD/exp/32SF6
 CTRD=$BASD/ctrp/32SF6
##
## Jmax values.
##
 JPlow=96
 JPmed=95
 JPupp=60
 JPupp_low=60
 JPupp_med=60
 JPmed_low=95
##
## Parameter files.
##
 SPARA1=Pa_001000m000000
 SPARA2=Pa_002000m001000
 WPARA1=$SPARA1"_work"
 WPARA2=$SPARA2"_work"
##
## Assignment files.
##
 ASG_DIP=`ls $EXPD/dip/*`
 ASG_2PH=`ls $EXPD/2ph/*`
##
## Parameter constraint files.
##
 CLF1=$CTRD/001000
 CLF2=$CTRD/002000
##
 PARA1=$SPARA1"_nulpar"
 PARA2=$SPARA2"_nulpar"
 $SCRD/passx nulpar H $CLF1 $WPARA1 $PARA1
 $SCRD/passx nulpar H $CLF2 $WPARA2 $PARA2
###############################################################
##
## Hamiltonian diagonalization.
##
## 000000 level.
##
 $SCRD/passx hdiag  P0 N1 D6 $JPlow $PARA1
##
## 001000 level.
##
 $SCRD/passx hdiag  P1 N1 D67 $JPmed $PARA1
##
## 002000 level.
##
 $SCRD/passx hdiag  P2 N1 D676 $JPupp $PARA2
##
## nu3 fit.
##
 $SCRD/exasg '001000m000000' "$ASG_DIP"
 \cp ASG_EXP ASG_001000mP0.t
 \mv ASG_EXP assignments.t
##
 \cp $CLF1 CL_H_001000
 \cp $CLF2 CL_H_002000
##
 $SCRD/passx eq_tds P1 P0 $JPmed_low $PARA1 CL_H_001000 prec GHz
 \mv normal_eq.t         NQ_H_002000_001000mP0
 \mv prediction.t        Pred_001000mP0
 \mv prediction_mix.t    Pred_mix_001000mP0
 \mv statistics.t        Stat_001000mP0
##
## 2-photon spectra fit.
##
 $SCRD/exasg '002000m000000' "$ASG_2PH"
 \cp ASG_EXP ASG_002000mP0.t
 \mv ASG_EXP assignments.t
##
 $SCRD/passx eq_tds P2 P0 $JPupp_low $PARA2 CL_H_002000 prec GHz
 \mv normal_eq.t         NQ_H_002000_002000mP0
 \mv prediction.t        Pred_002000mP0
 \mv prediction_mix.t    Pred_mix_002000mP0
 \mv statistics.t        Stat_002000mP0
##
## 002000-001000 double resonance spectra fit.
##
 $SCRD/exasg '002000m001000' "$ASG_DIP"
 \cp ASG_EXP ASG_002000m001000.t
 \mv ASG_EXP assignments.t
##
 $SCRD/passx eq_tds P2 P1 $JPupp_med $PARA2 CL_H_002000 prec GHz
 \mv normal_eq.t         NQ_H_002000_002000m001000
 \mv prediction.t        Pred_002000m001000
 \mv prediction_mix.t    Pred_mix_002000m001000
 \mv statistics.t        Stat_002000m001000
##
## New Hamiltonian parameter estimates.
##
  $SCRD/passx paradj H NQ_H_002000_001000mP0       $PARA1 \
                       NQ_H_002000_002000mP0       $PARA2 \
                       NQ_H_002000_002000m001000   $PARA2
 \rm NQ_H_002000_001000mP0 NQ_H_002000_002000mP0 NQ_H_002000_002000m001000
##
 \rm assignments.t
 \rm CL* ED*     VP*
###
### The new parameter files are :
###
###                    NQ_H_002000_001000mP0_adj
###                    NQ_H_002000_002000mP0_adj
###                    NQ_H_002000_002000m001000_adj
###

