
      module mod_com_hdic

      use mod_dppr
      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      real(kind=dppr) ,pointer ,dimension(:) ,save  :: HD                                          ! (MXDIMS)
      real(kind=dppr) ,pointer ,dimension(:) ,save  :: PARA                                        ! (MXOPH)
      real(kind=dppr) ,pointer ,dimension(:) ,save  :: PCENT                                       ! (MXSNV)

      integer         ,pointer ,dimension(:) ,save  :: ICENT                                       ! (MXSNV)
      integer         ,pointer ,dimension(:) ,save  :: K,NRCOD,NRVCOD,NVCOD                        ! (MXDIMS)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_HDIC
      IMPLICIT NONE

      integer  :: ierr

      allocate(HD(MXDIMS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_HDIC_HD')
      HD = 0.d0
      allocate(PARA(MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_HDIC_PARA')
      PARA = 0.d0
      allocate(PCENT(MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_HDIC_PCENT')
      PCENT = 0.d0

      allocate(K(MXDIMS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_HDIC_K')
      K = 0
      allocate(NRCOD(MXDIMS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_HDIC_NRCOD')
      NRCOD = 0
      allocate(NRVCOD(MXDIMS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_HDIC_NRVCOD')
      NRVCOD = 0
      allocate(NVCOD(MXDIMS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_HDIC_NVCOD')
      NVCOD = 0
      allocate(ICENT(MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_HDIC_ICENT')
      ICENT = 0
!
      return
      end subroutine ALLOC_HDIC

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXDIMS

      subroutine RESIZE_MXDIMS_HDIC(C_DIMS)
      IMPLICIT NONE
      integer :: C_DIMS

      integer :: ierr

! HD
      allocate(rpd1(C_DIMS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXDIMS_HDIC_HD')
      rpd1 = 0.d0
      rpd1(1:MXDIMS) = HD(:)
      deallocate(HD)
      HD => rpd1
! K
      allocate(ipd1(C_DIMS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXDIMS_HDIC_K')
      ipd1 = 0
      ipd1(1:MXDIMS) = K(:)
      deallocate(K)
      K => ipd1
! NRCOD
      allocate(ipd1(C_DIMS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXDIMS_HDIC_NRCOD')
      ipd1 = 0
      ipd1(1:MXDIMS) = NRCOD(:)
      deallocate(NRCOD)
      NRCOD => ipd1
! NRVCOD
      allocate(ipd1(C_DIMS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXDIMS_HDIC_NRVCOD')
      ipd1 = 0
      ipd1(1:MXDIMS) = NRVCOD(:)
      deallocate(NRVCOD)
      NRVCOD => ipd1
! NVCOD
      allocate(ipd1(C_DIMS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXDIMS_HDIC_NVCOD')
      ipd1 = 0
      ipd1(1:MXDIMS) = NVCOD(:)
      deallocate(NVCOD)
      NVCOD => ipd1
!
      return
      end subroutine RESIZE_MXDIMS_HDIC

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOPH

      subroutine RESIZE_MXOPH_HDIC(C_OPH)
      IMPLICIT NONE
      integer :: C_OPH

      integer :: ierr

! PARA
      allocate(rpd1(C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_HDIC_PARA')
      rpd1 = 0.d0
      rpd1(1:MXOPH) = PARA(:)
      deallocate(PARA)
      PARA => rpd1
!
      return
      end subroutine RESIZE_MXOPH_HDIC

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXSNV

      subroutine RESIZE_MXSNV_HDIC(C_SNV)
      IMPLICIT NONE
      integer :: C_SNV

      integer :: ierr

! PCENT
      allocate(rpd1(C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_HDIC_PCENT')
      rpd1 = 0.d0
      rpd1(1:MXSNV) = PCENT(:)
      deallocate(PCENT)
      PCENT => rpd1
! ICENT
      allocate(ipd1(C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_HDIC_ICENT')
      ipd1 = 0
      ipd1(1:MXSNV) = ICENT(:)
      deallocate(ICENT)
      ICENT => ipd1
!
      return
      end subroutine RESIZE_MXSNV_HDIC

      end module mod_com_hdic
