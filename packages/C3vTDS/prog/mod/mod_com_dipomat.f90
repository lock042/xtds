
      module mod_com_dipomat

      use mod_dppr
      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      integer ,save  :: NBELM = 0
!
      real(kind=dppr) ,pointer ,dimension(:,:) ,save  :: EMRV                                      ! (MXEMR,MXOPVT)
      real(kind=dppr) ,pointer ,dimension(:)   ,save  :: H                                         ! (MXELMT)

      integer         ,pointer ,dimension(:)   ,save  :: ICODR,ICODV                               ! (MXOPT)
      integer         ,pointer ,dimension(:)   ,save  :: KO,LI                                     ! (MXELMT)
      integer         ,pointer ,dimension(:,:) ,save  :: KOR,LIR                                   ! (MXEMR,MXOPVT)
      integer         ,pointer ,dimension(:)   ,save  :: NRCODI,NRVCDI,NVCODI                      ! (MXDIMI)
      integer         ,pointer ,dimension(:)   ,save  :: NRCODS,NRVCDS,NVCODS                      ! (MXDIMS)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_DIPOMAT
      IMPLICIT NONE

      integer  :: ierr

      allocate(EMRV(MXEMR,MXOPVT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DIPOMAT_EMRV')
      EMRV = 0.D0
      allocate(H(MXELMT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DIPOMAT_H')
      H = 0.D0

      allocate(ICODR(MXOPT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DIPOMAT_ICODR')
      ICODR = 0
      allocate(ICODV(MXOPT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DIPOMAT_ICODV')
      ICODV = 0
      allocate(KO(MXELMT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DIPOMAT_KO')
      KO = 0
      allocate(LI(MXELMT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DIPOMAT_LI')
      LI = 0
      allocate(KOR(MXEMR,MXOPVT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DIPOMAT_KOR')
      KOR = 0
      allocate(LIR(MXEMR,MXOPVT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DIPOMAT_LIR')
      LIR = 0
      allocate(NRCODI(MXDIMI),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DIPOMAT_NRCODI')
      NRCODI = 0
      allocate(NRVCDI(MXDIMI),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DIPOMAT_NRVCDI')
      NRVCDI = 0
      allocate(NVCODI(MXDIMI),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DIPOMAT_NVCODI')
      NVCODI = 0
      allocate(NRCODS(MXDIMS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DIPOMAT_NRCODS')
      NRCODS = 0
      allocate(NRVCDS(MXDIMS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DIPOMAT_NRVCDS')
      NRVCDS = 0
      allocate(NVCODS(MXDIMS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DIPOMAT_NVCODS')
      NVCODS = 0
!
      return
      end subroutine ALLOC_DIPOMAT

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXDIMI

      subroutine RESIZE_MXDIMI_DIPOMAT(C_DIMI)
      IMPLICIT NONE
      integer :: C_DIMI

      integer :: ierr

! NRCODI
      allocate(ipd1(C_DIMI),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXDIMI_DIPOMAT_NRCODI')
      ipd1 = 0
      ipd1(1:MXDIMI) = NRCODI(:)
      deallocate(NRCODI)
      NRCODI => ipd1
! NRVCDI
      allocate(ipd1(C_DIMI),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXDIMI_DIPOMAT_NRVCDI')
      ipd1 = 0
      ipd1(1:MXDIMI) = NRVCDI(:)
      deallocate(NRVCDI)
      NRVCDI => ipd1
! NVCODI
      allocate(ipd1(C_DIMI),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXDIMI_DIPOMAT_NVCODI')
      ipd1 = 0
      ipd1(1:MXDIMI) = NVCODI(:)
      deallocate(NVCODI)
      NVCODI => ipd1
!
      return
      end subroutine RESIZE_MXDIMI_DIPOMAT

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXDIMS

      subroutine RESIZE_MXDIMS_DIPOMAT(C_DIMS)
      IMPLICIT NONE
      integer :: C_DIMS

      integer :: ierr

! NRCODS
      allocate(ipd1(C_DIMS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXDIMS_DIPOMAT_NRCODS')
      ipd1 = 0
      ipd1(1:MXDIMS) = NRCODS(:)
      deallocate(NRCODS)
      NRCODS => ipd1
! NRVCDS
      allocate(ipd1(C_DIMS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXDIMS_DIPOMAT_NRVCDS')
      ipd1 = 0
      ipd1(1:MXDIMS) = NRVCDS(:)
      deallocate(NRVCDS)
      NRVCDS => ipd1
! NVCODS
      allocate(ipd1(C_DIMS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXDIMS_DIPOMAT_NVCODS')
      ipd1 = 0
      ipd1(1:MXDIMS) = NVCODS(:)
      deallocate(NVCODS)
      NVCODS => ipd1
!
      return
      end subroutine RESIZE_MXDIMS_DIPOMAT

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXELMT

      subroutine RESIZE_MXELMT_DIPOMAT(C_ELMT)
      IMPLICIT NONE
      integer :: C_ELMT

      integer :: ierr

! H
      allocate(rpd1(C_ELMT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXELMT_DIPOMAT_H')
      rpd1 = 0.d0
      rpd1(1:MXELMT) = H(:)
      deallocate(H)
      H => rpd1
! KO
      allocate(ipd1(C_ELMT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXELMT_DIPOMAT_KO')
      ipd1 = 0
      ipd1(1:MXELMT) = KO(:)
      deallocate(KO)
      KO => ipd1
! LI
      allocate(ipd1(C_ELMT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXELMT_DIPOMAT_LI')
      ipd1 = 0
      ipd1(1:MXELMT) = LI(:)
      deallocate(LI)
      LI => ipd1
!
      return
      end subroutine RESIZE_MXELMT_DIPOMAT

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXEMR

      subroutine RESIZE_MXEMR_DIPOMAT(C_EMR)
      IMPLICIT NONE
      integer :: C_EMR

      integer :: i,ierr

! EMRV
      allocate(rpd2(C_EMR,MXOPVT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXEMR_DIPOMAT_EMRV')
      rpd2 = 0.d0
      do i=1,MXOPVT
        rpd2(1:MXEMR,i) = EMRV(:,i)
      enddo
      deallocate(EMRV)
      EMRV => rpd2
! KOR
      allocate(ipd2(C_EMR,MXOPVT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXEMR_DIPOMAT_KOR')
      ipd2 = 0
      do i=1,MXOPVT
        ipd2(1:MXEMR,i) = KOR(:,i)
      enddo
      deallocate(KOR)
      KOR => ipd2
! LIR
      allocate(ipd2(C_EMR,MXOPVT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXEMR_DIPOMAT_LIR')
      ipd2 = 0
      do i=1,MXOPVT
        ipd2(1:MXEMR,i) = LIR(:,i)
      enddo
      deallocate(LIR)
      LIR => ipd2
!
      return
      end subroutine RESIZE_MXEMR_DIPOMAT

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOPT

      subroutine RESIZE_MXOPT_DIPOMAT(C_OPT)
      IMPLICIT NONE
      integer :: C_OPT

      integer :: ierr

! ICODR
      allocate(ipd1(C_OPT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPT_DIPOMAT_ICODR')
      ipd1 = 0
      ipd1(1:MXOPT) = ICODR(:)
      deallocate(ICODR)
      ICODR => ipd1
! ICODV
      allocate(ipd1(C_OPT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPT_DIPOMAT_ICODV')
      ipd1 = 0
      ipd1(1:MXOPT) = ICODV(:)
      deallocate(ICODV)
      ICODV => ipd1
!
      return
      end subroutine RESIZE_MXOPT_DIPOMAT

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOPVT

      subroutine RESIZE_MXOPVT_DIPOMAT(C_OPVT)
      IMPLICIT NONE
      integer :: C_OPVT

      integer :: i,ierr

! EMRV
      allocate(rpd2(MXEMR,C_OPVT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPVT_DIPOMAT_EMRV')
      rpd2 = 0.d0
      do i=1,MXOPVT
        rpd2(:,i) = EMRV(:,i)
      enddo
      deallocate(EMRV)
      EMRV => rpd2
! KOR
      allocate(ipd2(MXEMR,C_OPVT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPVT_DIPOMAT_KOR')
      ipd2 = 0
      do i=1,MXOPVT
        ipd2(:,i) = KOR(:,i)
      enddo
      deallocate(KOR)
      KOR => ipd2
! LIR
      allocate(ipd2(MXEMR,C_OPVT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPVT_DIPOMAT_LIR')
      ipd2 = 0
      do i=1,MXOPVT
        ipd2(:,i) = LIR(:,i)
      enddo
      deallocate(LIR)
      LIR => ipd2
!
      return
      end subroutine RESIZE_MXOPVT_DIPOMAT

      end module mod_com_dipomat
