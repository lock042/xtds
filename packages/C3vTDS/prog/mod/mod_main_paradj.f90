
      module mod_main_paradj

      use mod_dppr
      use mod_par_tds
      use mod_com_paradj


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine alloc_all
      IMPLICIT NONE

      call alloc_paradj
!
      return
      end subroutine alloc_all

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXFI
      IMPLICIT NONE

      integer  :: M_FI

      M_FI = MXRES(MXFI)
      call RESIZE_MXFI_PARADJ(M_FI)
      MXFI = M_FI
!
      return
      end subroutine RESIZE_MXFI

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXOPH
      IMPLICIT NONE

      integer  :: M_OPH

      M_OPH = MXRES(MXOPH)
      call RESIZE_MXOPH_PARADJ(M_OPH)
      MXOPH = M_OPH
!
      return
      end subroutine RESIZE_MXOPH

      end module mod_main_paradj
