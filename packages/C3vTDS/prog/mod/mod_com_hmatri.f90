
      module mod_com_hmatri

      use mod_dppr
      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      integer ,save  :: NBELM = 0
      integer ,save  :: NBOPH = 0
!
      real(kind=dppr) ,pointer ,dimension(:,:) ,save  :: EMRV                                      ! (MXEMR,MXOPVH)
      real(kind=dppr) ,pointer ,dimension(:)   ,save  :: H                                         ! (MXELMH)

      integer         ,pointer ,dimension(:)   ,save  :: ICODR,ICODV,IGAV,KAV,KTT                  ! (MXOPH)
      integer         ,pointer ,dimension(:)   ,save  :: KO,LI                                     ! (MXELMH)
      integer         ,pointer ,dimension(:,:) ,save  :: KOR,LIR                                   ! (MXEMR,MXOPVH)
      integer         ,pointer ,dimension(:)   ,save  :: NRCOD,NRVCOD,NVCOD                        ! (MXDIMS)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_HMATRI
      IMPLICIT NONE

      integer  :: ierr

      allocate(EMRV(MXEMR,MXOPVH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_HMATRI_EMRV')
      EMRV = 0.d0
      allocate(H(MXELMH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_HMATRI_H')
      H = 0.d0

      allocate(ICODR(MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_HMATRI_ICODR')
      ICODR = 0
      allocate(ICODV(MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_HMATRI_ICODV')
      ICODV = 0
      allocate(IGAV(MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_HMATRI_IGAV')
      IGAV = 0
      allocate(KAV(MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_HMATRI_KAV')
      KAV = 0
      allocate(KTT(MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_HMATRI_KTT')
      KTT = 0
      allocate(KO(MXELMH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_HMATRI_KO')
      KO = 0
      allocate(LI(MXELMH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_HMATRI_LI')
      LI = 0
      allocate(KOR(MXEMR,MXOPVH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_HMATRI_KOR')
      KOR = 0
      allocate(LIR(MXEMR,MXOPVH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_HMATRI_LIR')
      LIR = 0
      allocate(NRCOD(MXDIMS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_HMATRI_NRCOD')
      NRCOD = 0
      allocate(NRVCOD(MXDIMS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_HMATRI_NRVCOD')
      NRVCOD = 0
      allocate(NVCOD(MXDIMS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_HMATRI_NVCOD')
      NVCOD = 0
!
      return
      end subroutine ALLOC_HMATRI

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXDIMS

      subroutine RESIZE_MXDIMS_HMATRI(C_DIMS)
      IMPLICIT NONE
      integer :: C_DIMS

      integer :: ierr

! NRCOD
      allocate(ipd1(C_DIMS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXDIMS_HMATRI_NRCOD')
      ipd1 = 0
      ipd1(1:MXDIMS) = NRCOD(:)
      deallocate(NRCOD)
      NRCOD => ipd1
! NRVCOD
      allocate(ipd1(C_DIMS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXDIMS_HMATRI_NRVCOD')
      ipd1 = 0
      ipd1(1:MXDIMS) = NRVCOD(:)
      deallocate(NRVCOD)
      NRVCOD => ipd1
! NVCOD
      allocate(ipd1(C_DIMS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXDIMS_HMATRI_NVCOD')
      ipd1 = 0
      ipd1(1:MXDIMS) = NVCOD(:)
      deallocate(NVCOD)
      NVCOD => ipd1
!
      return
      end subroutine RESIZE_MXDIMS_HMATRI

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXELMH

      subroutine RESIZE_MXELMH_HMATRI(C_ELMH)
      IMPLICIT NONE
      integer :: C_ELMH

      integer :: ierr

! H
      allocate(rpd1(C_ELMH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXELMH_HMATRI_H')
      rpd1 = 0.d0
      rpd1(1:MXELMH) = H(:)
      deallocate(H)
      H => rpd1
! KO
      allocate(ipd1(C_ELMH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXELMH_HMATRI_KO')
      ipd1 = 0
      ipd1(1:MXELMH) = KO(:)
      deallocate(KO)
      KO => ipd1
! LI
      allocate(ipd1(C_ELMH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXELMH_HMATRI_LI')
      ipd1 = 0
      ipd1(1:MXELMH) = LI(:)
      deallocate(LI)
      LI => ipd1
!
      return
      end subroutine RESIZE_MXELMH_HMATRI

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXEMR

      subroutine RESIZE_MXEMR_HMATRI(C_EMR)
      IMPLICIT NONE
      integer :: C_EMR

      integer :: i,ierr

! EMRV
      allocate(rpd2(C_EMR,MXOPVH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXEMR_HMATRI_EMRV')
      rpd2 = 0.d0
      do i=1,MXOPVH
        rpd2(1:MXEMR,i) = EMRV(:,i)
      enddo
      deallocate(EMRV)
      EMRV => rpd2
! KOR
      allocate(ipd2(C_EMR,MXOPVH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXEMR_HMATRI_KOR')
      ipd2 = 0
      do i=1,MXOPVH
        ipd2(1:MXEMR,i) = KOR(:,i)
      enddo
      deallocate(KOR)
      KOR => ipd2
! LIR
      allocate(ipd2(C_EMR,MXOPVH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXEMR_HMATRI_LIR')
      ipd2 = 0
      do i=1,MXOPVH
        ipd2(1:MXEMR,i) = LIR(:,i)
      enddo
      deallocate(LIR)
      LIR => ipd2
!
      return
      end subroutine RESIZE_MXEMR_HMATRI

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOPH

      subroutine RESIZE_MXOPH_HMATRI(C_OPH)
      IMPLICIT NONE
      integer :: C_OPH

      integer :: ierr

! ICODR
      allocate(ipd1(C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_HMATRI_ICODR')
      ipd1 = 0
      ipd1(1:MXOPH) = ICODR(:)
      deallocate(ICODR)
      ICODR => ipd1
! ICODV
      allocate(ipd1(C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_HMATRI_ICODV')
      ipd1 = 0
      ipd1(1:MXOPH) = ICODV(:)
      deallocate(ICODV)
      ICODV => ipd1
! IGAV
      allocate(ipd1(C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_HMATRI_IGAV')
      ipd1 = 0
      ipd1(1:MXOPH) = IGAV(:)
      deallocate(IGAV)
      IGAV => ipd1
! KAV
      allocate(ipd1(C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_HMATRI_KAV')
      ipd1 = 0
      ipd1(1:MXOPH) = KAV(:)
      deallocate(KAV)
      KAV => ipd1
! KTT
      allocate(ipd1(C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_HMATRI_KTT')
      ipd1 = 0
      ipd1(1:MXOPH) = KTT(:)
      deallocate(KTT)
      KTT => ipd1
!
      return
      end subroutine RESIZE_MXOPH_HMATRI

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOPVH

      subroutine RESIZE_MXOPVH_HMATRI(C_OPVH)
      IMPLICIT NONE
      integer :: C_OPVH

      integer :: i,ierr

! EMRV
      allocate(rpd2(MXEMR,C_OPVH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPVH_HMATRI_EMRV')
      rpd2 = 0.d0
      do i=1,MXOPVH
        rpd2(1:MXEMR,i) = EMRV(:,i)
      enddo
      deallocate(EMRV)
      EMRV => rpd2
! KOR
      allocate(ipd2(MXEMR,C_OPVH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPVH_HMATRI_KOR')
      ipd2 = 0
      do i=1,MXOPVH
        ipd2(1:MXEMR,i) = KOR(:,i)
      enddo
      deallocate(KOR)
      KOR => ipd2
! LIR
      allocate(ipd2(MXEMR,C_OPVH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPVH_HMATRI_LIR')
      ipd2 = 0
      do i=1,MXOPVH
        ipd2(1:MXEMR,i) = LIR(:,i)
      enddo
      deallocate(LIR)
      LIR => ipd2
!
      return
      end subroutine RESIZE_MXOPVH_HMATRI

      end module mod_com_hmatri
