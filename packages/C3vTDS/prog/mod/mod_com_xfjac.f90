
      module mod_com_xfjac

      use mod_dppr
      use mod_par_x
      use mod_alloc
      IMPLICIT NONE

      real(kind=dppr) ,pointer ,dimension(:) ,save  :: WB                                          ! (MXOBZ)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_XFJAC
      IMPLICIT NONE

      integer  :: ierr

      allocate(WB(MXOBZ),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XFJAC_WB')
      WB = 0.d0
!
      return
      end subroutine ALLOC_XFJAC

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOBZ

      subroutine RESIZE_MXOBZ_XFJAC(C_OBZ)
      IMPLICIT NONE
      integer :: C_OBZ

      integer :: ierr

! WB
      allocate(rpd1(C_OBZ),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBZ_XFJAC_WB')
      rpd1 = 0.d0
      rpd1(1:MXOBZ) = WB(:)
      deallocate(WB)
      WB => rpd1
!
      return
      end subroutine RESIZE_MXOBZ_XFJAC

      end module mod_com_xfjac
