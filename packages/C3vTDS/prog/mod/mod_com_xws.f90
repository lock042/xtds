
      module mod_com_xws

      use mod_dppr
      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      real(kind=dppr)      ,pointer ,dimension(:) ,save  :: DERS,DERT             ! (MXOPT)
      real(kind=dppr)      ,pointer ,dimension(:) ,save  :: ENINFC                ! (MXENI)

      integer              ,pointer ,dimension(:) ,save  :: JCENTI                ! (MXENI)
      integer              ,pointer ,dimension(:) ,save  :: IPCSUPS,NUSVSUS       ! (MXSNV)
      integer              ,pointer ,dimension(:) ,save  :: IXCLS                 ! (MXOPT)

      character(len =   3) ,pointer ,dimension(:) ,save  :: DIPPOLS               ! (MXATR)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_XWS
      IMPLICIT NONE

      integer  :: ierr

      allocate(DERS(MXOPT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XWS_DERS')
      DERS = 0.d0
      allocate(DERT(MXOPT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XWS_DERT')
      DERT = 0.d0
      allocate(ENINFC(MXENI),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XWS_ENINFC')
      ENINFC = 0.d0

      allocate(JCENTI(MXENI),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XWS_JCENTI')
      JCENTI = 0
      allocate(IPCSUPS(MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XWS_IPCSUPS')
      IPCSUPS = 0
      allocate(NUSVSUS(MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XWS_NUSVSUS')
      NUSVSUS = 0
      allocate(IXCLS(MXOPT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XWS_IXCLS')
      IXCLS = 0

      allocate(DIPPOLS(MXATR),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XWS_DIPPOLS')
      DIPPOLS = ''
!
      return
      end subroutine ALLOC_XWS

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXATR

      subroutine RESIZE_MXATR_XWS(C_ATR)
      IMPLICIT NONE
      integer :: C_ATR

      integer :: ierr

      character(len =   3) ,pointer ,dimension(:)  :: cpd1

! DIPPOLS
      allocate(cpd1(C_ATR),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXATR_XWS_DIPPOLS')
      cpd1 = ''
      cpd1(1:MXATR) = DIPPOLS(:)
      deallocate(DIPPOLS)
      DIPPOLS => cpd1
!
      return
      end subroutine RESIZE_MXATR_XWS

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXENI

      subroutine RESIZE_MXENI_XWS(C_ENI)
      IMPLICIT NONE
      integer :: C_ENI

      integer :: ierr

! ENINFC
      allocate(rpd1(C_ENI),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXENI_XWS_ENINFC')
      rpd1 = 0.d0
      rpd1(1:MXENI) = ENINFC(:)
      deallocate(ENINFC)
      ENINFC => rpd1

! JCENTI
      allocate(ipd1(C_ENI),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXENI_XWS_JCENTI')
      ipd1 = 0
      ipd1(1:MXENI) = JCENTI(:)
      deallocate(JCENTI)
      JCENTI => ipd1
!
      return
      end subroutine RESIZE_MXENI_XWS

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOPT

      subroutine RESIZE_MXOPT_XWS(C_OPT)
      IMPLICIT NONE
      integer :: C_OPT

      integer :: ierr

! DERS
      allocate(rpd1(C_OPT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPT_XWS_DERS')
      rpd1 = 0.d0
      rpd1(1:MXOPT) = DERS(:)
      deallocate(DERS)
      DERS => rpd1
! DERT
      allocate(rpd1(C_OPT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPT_XWS_DERT')
      rpd1 = 0.d0
      rpd1(1:MXOPT) = DERT(:)
      deallocate(DERT)
      DERT => rpd1

! IXCLS
      allocate(ipd1(C_OPT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPT_XWS_IXCLS')
      ipd1 = 0
      ipd1(1:MXOPT) = IXCLS(:)
      deallocate(IXCLS)
      IXCLS => ipd1
!
      return
      end subroutine RESIZE_MXOPT_XWS

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXSNV

      subroutine RESIZE_MXSNV_XWS(C_SNV)
      IMPLICIT NONE
      integer :: C_SNV

      integer :: ierr

! IPCSUPS
      allocate(ipd1(C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_XWS_IPCSUPS')
      ipd1 = 0
      ipd1(1:MXSNV) = IPCSUPS(:)
      deallocate(IPCSUPS)
      IPCSUPS => ipd1
! NUSVSUS
      allocate(ipd1(C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_XWS_NUSVSUS')
      ipd1 = 0
      ipd1(1:MXSNV) = NUSVSUS(:)
      deallocate(NUSVSUS)
      NUSVSUS => ipd1
!
      return
      end subroutine RESIZE_MXSNV_XWS

      end module mod_com_xws
