
      module mod_com_jener

      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      integer ,pointer ,dimension(:) ,save  :: ICENT                                               ! (MXSNV)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_JENER
      IMPLICIT NONE

      integer  :: ierr

      allocate(ICENT(MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_JENER_ICENT')
      ICENT = 0
!
      return
      end subroutine ALLOC_JENER

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXSNV

      subroutine RESIZE_MXSNV_JENER(C_SNV)
      IMPLICIT NONE
      integer :: C_SNV

      integer :: ierr

! ICENT
      allocate(ipd1(C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_JENER_ICENT')
      ipd1 = 0
      ipd1(1:MXSNV) = ICENT(:)
      deallocate(ICENT)
      ICENT => ipd1
!
      return
      end subroutine RESIZE_MXSNV_JENER

      end module mod_com_jener
