
      module mod_com_derv

      use mod_dppr
      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      real(kind=dppr) ,pointer ,dimension(:) ,save  :: DERV                                        ! (MXOPH)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_DERV
      IMPLICIT NONE

      integer  :: ierr

      allocate(DERV(MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DERV_DERV')
      DERV = 0.d0
!
      return
      end subroutine ALLOC_DERV

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOPH

      subroutine RESIZE_MXOPH_DERV(C_OPH)
      IMPLICIT NONE
      integer :: C_OPH

      integer :: ierr

! DERV
      allocate(rpd1(C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_DERV_DERV')
      rpd1 = 0.d0
      rpd1(1:MXOPH) = DERV(:)
      deallocate(DERV)
      DERV => rpd1
!
      return
      end subroutine RESIZE_MXOPH_DERV

      end module mod_com_derv
