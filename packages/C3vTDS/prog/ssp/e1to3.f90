!
! 05/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3v.
!
!     IPM=+1 POUR UN OPERATEUR CREATION
!     IPM=-1 POUR UN OPERATEUR ANNIHILATION
!
      FUNCTION E1TO3(IPM,IB,IA,IK)
      use mod_dppr
      use mod_par_tds
      IMPLICIT NONE
      real(kind=dppr)  :: E1TO3
      integer ,dimension(NBVQN+1)  :: IB,IA,IK
      integer          :: IPM

! functions
      real(kind=dppr)  :: EMRVA

      integer          :: I,IAL,IAV,IBL,IBV,IKL,IKV
!
      E1TO3 = 1.D0
      DO I=1,3
        CALL VLNC(IB(I),IBV,IBL)
        CALL VLNC(IA(I),IAV,IAL)
        CALL VLNC(IK(I),IKV,IKL)
        E1TO3 = E1TO3*EMRVA(1,IBV,IBL,IPM,IAV,IAL,IKV,IKL)
      ENDDO
!
      RETURN
      END FUNCTION E1TO3
