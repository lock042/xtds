!
! 05/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3v.
!
! CE PROGRAMME CALCULE LES TERMES N INTERVENANT DANS
! LE CALCUL DES 3K-IS
!
      FUNCTION NJK(AJ1,AK1,AJ2,AK2,AJ3,AK3)
      use mod_dppr
      IMPLICIT NONE
      real(kind=dppr)  :: NJK
      real(kind=dppr)  :: AJ1,AK1,AJ2,AK2,AJ3,AK3

! functions
      real(kind=dppr)  :: FJKS

      real(kind=dppr)  :: AN
      real(kind=dppr)  :: FJK

      integer          :: IS1,ISI1,ISP1,ISS1
      integer          :: IS2,ISI2,ISP2,ISS2
      integer          :: IS3,ISI3,ISP3,ISS3
!
      AN = 0.D0
      CALL LIMK(AK1,ISI1,ISS1,ISP1)
      DO IS1=ISI1,ISS1,ISP1
        CALL LIMK(AK2,ISI2,ISS2,ISP2)
        DO IS2=ISI2,ISS2,ISP2
          CALL LIMK(AK3,ISI3,ISS3,ISP3)
          DO IS3=ISI3,ISS3,ISP3
            FJK = FJKS(AJ1,AK1,IS1,AJ2,AK2,IS2,AJ3,AK3,IS3)
            AN  = AN+(ABS(FJK))**2
          ENDDO
        ENDDO
      ENDDO
      NJK = AN
!
      RETURN
      END FUNCTION NJK
