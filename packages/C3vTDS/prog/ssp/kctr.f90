!
! 05/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3v.
!
!       CE PROGRAMME VERIFIE LES REGLES DE REDUCTION DE CIV DANS C3V
!
      FUNCTION KCTR(AJ,AK,IC)
      use mod_dppr
      IMPLICIT NONE
      integer          :: KCTR
      real(kind=dppr)  :: AJ,AK
      integer          :: IC

      real(kind=dppr)  :: AKM

      integer          :: JP
      integer          :: KD,KM,KM3,KP
!
      KCTR = 0
      IF( AJ .EQ.  0.D0 .AND.            &
          AK .EQ.  0.D0 .AND.            &
          IC .EQ.  1         ) KCTR = 1
      IF( AJ .EQ.  1.D0 .AND.            &
          AK .EQ. -1.D0 .AND.            &
          IC .EQ.  2          ) KCTR = 1
      IF( AJ .LT. AK ) THEN
        KCTR = 0
      ELSE
        IF    ( AJ .GT. INT(AJ) ) THEN
          KD  = NINT(2.D0*AK)
          KM  = MOD(KD,6)
          AKM = KM/2.D0
          IF( AKM .EQ. 2.5D0 .AND.             &
              IC  .EQ. 6           ) KCTR = 1
          IF( AKM .EQ. 0.5D0 .AND.             &
              IC  .EQ. 6           ) KCTR = 1
          IF( AKM .EQ. 1.5D0 .AND.             &
              IC  .EQ. 4           ) KCTR = 1
          IF( AKM .EQ. 1.5D0 .AND.             &
              IC  .EQ. 5           ) KCTR = 1
        ELSEIF( AJ .EQ. INT(AJ) ) THEN
          KM3 = MOD(INT(AK),3)
          KP  = KM3-(KM3/2)*2
          JP  = INT(AJ)-INT(AJ)/2*2
          IF( JP .EQ.  0    .AND.             &
              AK .EQ.  0.D0 .AND.             &
              IC .EQ.  1          ) KCTR = 1
          IF( JP .EQ.  1    .AND.             &
              AK .EQ. -1.D0 .AND.             &
              IC .EQ.  2          ) KCTR = 1
          IF( AK .GT.  0.D0 ) THEN
            IF( KM3 .EQ. 1 .AND.             &
                IC  .EQ. 3       ) KCTR = 1
            IF( KM3 .EQ. 2 .AND.             &
                IC  .EQ. 3       ) KCTR = 1
            IF( KM3 .EQ. 0 .AND.             &
                IC  .EQ. 1       ) KCTR = 1
            IF( KM3 .EQ. 0 .AND.             &
                IC  .EQ. 2       ) KCTR = 1
          ENDIF
        ENDIF
      ENDIF
!
      RETURN
      END FUNCTION KCTR
