!
! *** MULTIPLICATION DANS C3VS (REPRESENTATIONS ENTIERES ET DEMI-ENTIERES).
!
! 05/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3vs.
!
!    A1  ------> 1
!    A2  ------> 2
!    E   ------> 3
!    A'1 ------> 4
!    A'2 ------> 5
!    E'  ------> 6
!
      SUBROUTINE MUC3VS(IC1,IC2,N,IC)
      use mod_par_tds
      IMPLICIT NONE
      integer          :: IC1,IC2,N
      integer ,dimension(MDMIGR)  :: IC

      integer          :: I1,I2
!
      IF( IC1 .LE. IC2 ) THEN
        I1 = IC1
        I2 = IC2
        GOTO 3
      ENDIF
2     I1 = IC2
      I2 = IC1
!
3     IF    ( I1 .LT. 2 ) THEN
        N     = 1
        IC(1) = I2
        RETURN
      ELSEIF( I1 .GT. 2 ) THEN
        GOTO 8
      ENDIF
      IF    ( I2 .LT. 2 ) THEN
        N     = 1
        IC(1) = 2
        RETURN
      ELSEIF( I2 .EQ. 2 ) THEN
        N     = 1
        IC(1) = 1
        RETURN
      ENDIF
      IF    ( I2 .LT. 4 ) THEN
        N     = 1
        IC(1) = 3
        RETURN
      ELSEIF( I2 .EQ. 4 ) THEN
        N     = 1
        IC(1) = 5
        RETURN
      ENDIF
      IF    ( I2 .LT. 6 ) THEN
        N     = 1
        IC(1) = 4
        RETURN
      ELSEIF( I2 .EQ. 6 ) THEN
        N     = 1
        IC(1) = 6
        RETURN
      ELSE
        GOTO 2
      ENDIF
!
8     IF    ( I1 .EQ. 4 ) THEN
        GOTO 15
      ELSEIF( I1 .GT. 4 ) THEN
        GOTO 19
      ENDIF
      IF    ( I2 .LT. 4 ) THEN
        N     = 3
        IC(1) = 1
        IC(2) = 2
        IC(3) = 3
        RETURN
      ELSEIF( I2 .EQ. 4 ) THEN
        N     = 1
        IC(1) = 6
        RETURN
      ENDIF
      IF( I2 .LE. 5 ) THEN
        N     = 1
        IC(1) = 6
        RETURN
      ELSE
        N     = 3
        IC(1) = 4
        IC(2) = 5
        IC(3) = 6
        RETURN
      ENDIF
!
15    IF    ( I2 .LT. 5 ) THEN
        N     = 1
        IC(1) = 2
        RETURN
      ELSEIF( I2 .EQ. 5 ) THEN
        N     = 1
        IC(1) = 1
        RETURN
      ELSE
        N     = 1
        IC(1) = 3
        RETURN
      ENDIF
!
19    IF    ( I1 .LT. I2 ) THEN
        N     = 1
        IC(1) = 3
        RETURN
      ELSEIF( I1 .EQ. I2 ) THEN
        IF( I2 .LE. 5 ) THEN
          N     = 1
          IC(1) = 2
          RETURN
        ELSE
          N     = 3
          IC(1) = 1
          IC(2) = 2
          IC(3) = 3
          RETURN
        ENDIF
      ELSE
        GOTO 2
      ENDIF
!
      RETURN
      END SUBROUTINE MUC3VS
