!
! 05/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3v.
!
      SUBROUTINE MULCIV(AK1,AK2,N,AK)
      use mod_dppr
      use mod_par_tds
      IMPLICIT NONE
      real(kind=dppr)  :: AK1,AK2
      integer          :: N
      real(kind=dppr) ,dimension(MDMIGR)  :: AK

      integer          :: A1,A2
!
      IF( AK1 .LE. AK2 ) THEN
        A1 = AK1
        A2 = AK2
      ELSE
        A1 = AK2
        A2 = AK1
      ENDIF
      IF    ( A1 .LT. 0.D0 ) THEN
        IF    ( A2 .LT. 0.D0 ) THEN
          N     = 1
          AK(1) = 0.D0
        ELSEIF( A2 .EQ. 0.D0 ) THEN
          N     =  1
          AK(1) = -1.D0
        ELSE
          N     = 1
          AK(1) = A2
        ENDIF
      ELSEIF( A1 .EQ. 0.D0 ) THEN
        IF( A2 .LT. 0.D0 ) THEN
          N     =  1
          AK(1) = -1.D0
        ELSE
          N     = 1
          AK(1) = A2
        ENDIF
      ELSE
        IF    ( A2 .EQ. A1 ) THEN
          N     =  3
          AK(1) = -1.D0
          AK(2) =  0.D0
          AK(3) =  2.D0*A2
        ELSEIF( A2 .GT. A1 ) THEN
          N     = 2
          AK(1) = A2-A1
          AK(2) = A2+A1
        ENDIF
      ENDIF
!
      RETURN
      END SUBROUTINE MULCIV
