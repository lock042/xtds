!
! 05/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3v.
!
! CE PROGRAMME CALCULE LES COEFFICIENTS 6K DE CIVS
!
      SUBROUTINE SIX6K(AK1,AK2,AK3,AK4,AK5,AK6,SIXK,ISIXK)
      use mod_dppr
      IMPLICIT NONE
      real(kind=dppr)  :: AK1,AK2,AK3,AK4,AK5,AK6,SIXK
      integer          :: ISIXK

! functions
      integer          :: IDIMK,KTR

      real(kind=dppr)  :: AP1,AP2,AP4,AP5
      real(kind=dppr)  :: PHK
      real(kind=dppr)  :: TRK1,TRK2,TRK3,TRK4

      integer          :: IDM1,IDM2,IDM3,IDM4,IDM5,IDM6
      integer          :: IS1,IS2,IS3,IS4,IS5,IS6
      integer          :: ITRK1,ITRK2,ITRK3,ITRK4,ITRKT
!
      SIXK = 0.D0
      IF( KTR(AK1,AK2,AK3) .EQ. 0 .OR.             &
          KTR(AK3,AK4,AK5) .EQ. 0 .OR.             &
          KTR(AK1,AK5,AK6) .EQ. 0 .OR.             &
          KTR(AK2,AK4,AK6) .EQ. 0      ) RETURN
      IDM1 = IDIMK(AK1)
      IDM2 = IDIMK(AK2)
      IDM3 = IDIMK(AK3)
      IDM4 = IDIMK(AK4)
      IDM5 = IDIMK(AK5)
      IDM6 = IDIMK(AK6)
!
      AP1 = MAX(0.D0,AK1)
      AP2 = MAX(0.D0,AK2)
      AP4 = MAX(0.D0,AK4)
      AP5 = MAX(0.D0,AK5)
      DO IS1=1,IDM1
        DO IS2=1,IDM2
          DO IS3=1,IDM3
            DO IS4=1,IDM4
              DO IS5=1,IDM5
                DO IS6=1,IDM6
                  CALL TROIK(AK1,AK2,AK3,IS1,IS2,IS3,TRK1,ITRK1)
                  CALL TROIK(AK3,AK4,AK5,IS3,IS4,IS5,TRK2,ITRK2)
                  CALL TROIK(AK1,AK6,AK5,IS1,IS6,IS5,TRK3,ITRK3)
                  CALL TROIK(AK2,AK4,AK6,IS2,IS4,IS6,TRK4,ITRK4)
                  IF( ITRK3 .EQ. 1 ) TRK3 = -TRK3
                  IF( ITRK4 .EQ. 1 ) TRK4 = -TRK4
                  PHK = ( (-1)**NINT(AP1+AP2+AP4-AP5) )/(DBLE(IDM5)*SQRT(DBLE(IDM3*IDM6)))
                  ISIXK = 0
                  ITRKT = 0
                  ITRKT = ITRK1+ITRK2+ITRK3+ITRK4
                  IF( ITRKT .EQ. 1 .OR.                 &
                      ITRKT .EQ. 3      ) ISIXK =  1
                  IF( ITRKT .EQ. 2 .OR.                 &
                      ITRKT .EQ. 3      ) PHK   = -PHK
                  SIXK = SIXK+TRK1*TRK2*TRK3*TRK4*PHK
                ENDDO
              ENDDO
            ENDDO
          ENDDO
        ENDDO
      ENDDO
!
      RETURN
      END SUBROUTINE SIX6K
