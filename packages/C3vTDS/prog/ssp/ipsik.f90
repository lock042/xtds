!
! 05/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3v.
!
! LE TENSEUR "FONCTION D'ONDE" D'UN OSCILLATEUR HARMONIQUE (1 FOIS, 2
! FOIS OU 3 FOIS DEGENERE) ORIENTE DANS TD PEUT ETRE CARACTERISE PAR 5
! INDICES :
!            DEG      DEGENERESCENCE (1,2)
!            IV       NB QUANTIQUE VIBRATIONNEL PRINCIPAL   0,1,2...
!            L        NB QUANTIQUE VIBRATIONNEL SECONDAIRE  V,V-2,V-4..
!                     ...1 OU 0
!
! LA FONCTION IPSI  = 1 SI LES NOMBRES QUANTIQUES SONT COHERENTS
!                     0 SINON
!
!
      FUNCTION IPSIK(DEG,V,L)
      IMPLICIT NONE
      integer          :: IPSIK
      integer          :: DEG,V,L

!
      IPSIK = 0
      IF( DEG .LT. 1 .OR.           &
          DEG .GT. 2      ) RETURN
      IF( V   .LT. 0 .OR.           &
          L   .LT. 0      ) RETURN
      IF( DEG .GT. 1      ) GOTO 1
      IF( L   .NE. 0      ) RETURN
      IPSIK = 1
      RETURN
!
1     IF( L          .GT. V ) RETURN
      IF( MOD(V-L,2) .NE. 0 ) RETURN
      IPSIK = 1
!
      RETURN
      END FUNCTION IPSIK
