!
! 05/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3v.
!
! CE PROGRAMME DONNE LES COEFFFICIENTS DE LA MATRICE V POUR J ENTIER ET DEMI-ENTIER
!         IS
!      + ---->1
!      - ---->2
!
      SUBROUTINE VKCS(AJ,AK,IS,IC,IL,V,IP)
      use mod_dppr
      IMPLICIT NONE
      real(kind=dppr)  :: AJ,AK,V
      integer          :: IS,IC,IL,IP

! functions
      integer          :: KCTR

      real(kind=dppr)  :: AKM

      integer          :: IMP
      integer          :: JP
      integer          :: KD,KM,KM6,KP
      integer          :: MP
!
      V  = 0.D0
      IP = 0
      IF( AK .LE.  0.D0 ) IS = 1
      IF( IC .EQ.  1    ) IL = 1
      IF( IC .EQ.  2    ) IL = 2
      IF( AJ .EQ.  0.D0 .AND.             &
          AK .EQ.  0.D0 .AND.             &
          IC .EQ.  1          ) V = 1.D0
      IF( AJ .EQ.  1.D0 .AND.             &
          AK .EQ. -1.D0 .AND.             &
          IC .EQ.  2          ) V = 1.D0
      IF( KCTR(AJ,AK,IC) .NE. 0 )THEN
        IF    ( AJ .LT. INT(AJ) ) THEN
          RETURN
        ELSEIF( AJ .EQ. INT(AJ) ) THEN
          GOTO 20
        ENDIF
        MP  = INT(AJ+0.5D0)
        IMP = MP-(MP/2)*2
        KD  = INT(2*AK)
        KM  = MOD(KD,6)
        AKM = KM/2.D0
        IF( IC .EQ. 6 ) THEN
          IF( AKM .EQ. 0.5D0 .OR.                &
              AKM .EQ. 2.5D0      )THEN
            IF( (IMP .EQ. 1  .AND.               &
                 IS  .EQ. IL      ) .OR.         &
                (IMP .EQ. 0  .AND.               &
                 IS  .NE. IL      )      ) THEN
              V = 1.D0
            ENDIF
          ENDIF
        ENDIF
        IF( IC .EQ. 4 ) THEN
          IF( AKM .EQ. 1.5D0 ) THEN
            IF( (IMP .EQ. 1 .AND.              &
                 IS  .EQ. 2      ) .OR.        &
                (IMP .EQ. 0 .AND.              &
                 IS  .EQ. 1      )      )THEN
              V = 1.D0
            ENDIF
          ENDIF
        ENDIF
        IF( IC .EQ. 5 ) THEN
          IF( AKM .EQ. 1.5D0 ) THEN
            IF( (IMP .EQ. 1 .AND.               &
                 IS  .EQ. 1      ) .OR.         &
                (IMP .EQ. 0 .AND.               &
                 IS  .EQ. 2      )      ) THEN
              V = 1.D0
            ENDIF
          ENDIF
        ENDIF
        IF( IMP .EQ. 1 .AND.                   &
            IS  .EQ. 1 .AND.                   &
            IC  .EQ. 5       )     V  = -1.D0
        IF( IMP .EQ. 1     .AND.               &
            AKM .EQ. 2.5D0 .AND.               &
            IS  .EQ. 2           ) V  = -1.D0
        IF( IMP .EQ. 0     .AND.               &
            AKM .EQ. 2.5D0 .AND.               &
            IS  .EQ. 1           ) V  = -1.D0
        IF( IMP .EQ. 0     .AND.            &
            AKM .EQ. 0.5D0       ) IP =  1
        IF( IMP .EQ. 1     .AND.            &
            AKM .EQ. 2.5D0       ) IP =  1
        RETURN
!
20      KM6 = MOD(INT(AK),6)
        KP  = KM6-(KM6/2)*2
        JP  = INT(AJ)-(INT(AJ)/2)*2
!
!CC   ********* POUR J PAIR ET IMPAIR *********************
!
!*********   LES BLOCS E *********
!
        IF( JP .EQ. 0 ) THEN
          IF( AK  .GT. 0.D0 .AND.              &
              IC  .EQ. 3    .AND.              &
              KM6 .EQ. 1    .AND.              &
              IS  .EQ. IL         ) V = -1.D0
          IF( AK  .GT. 0.D0 .AND.              &
              IC  .EQ. 3    .AND.              &
              KM6 .EQ. 2    .AND.              &
              IS  .EQ. IL         ) V =  1.D0
          IF( AK  .GT. 0.D0 .AND.              &
              IC  .EQ. 3    .AND.              &
              KM6 .EQ. 4    .AND.              &
              IS  .EQ. IL         ) V =  1.D0
          IF( AK  .GT. 0.D0 .AND.              &
              IC  .EQ. 3    .AND.              &
              KM6 .EQ. 5    .AND.              &
              IS  .EQ. IL         ) V =  1.D0
!
!*********   LES BLOCS A1+A2 AND MODULO 6 Y COMPRIS LE SIGNE MOINS *********
!
          IF( AK  .GT. 0.D0 .AND.              &
              IC  .EQ. 1    .AND.              &
              KM6 .EQ. 0    .AND.              &
              IS  .EQ. 1    .AND.              &
              IL  .EQ. 1          ) V =  1.D0
          IF( AK  .GT. 0.D0 .AND.              &
              IC  .EQ. 2    .AND.              &
              KM6 .EQ. 0    .AND.              &
              IS  .EQ. 2    .AND.              &
              IL  .EQ. 2          ) V = -1.D0
!
!*********   LES BLOCS A1+A2 AND MODULO 3 Y COMPRIS LE SIGNE MOINS *********
!
          IF( AK  .GT. 0.D0 .AND.              &
              IC  .EQ. 1    .AND.              &
              KM6 .EQ. 3    .AND.              &
              IS  .EQ. 1    .AND.              &
              IL  .EQ. 1          ) V =  1.D0
          IF( AK  .GT. 0.D0 .AND.              &
              IC  .EQ. 2    .AND.              &
              KM6 .EQ. 3    .AND.              &
              IS  .EQ. 2    .AND.              &
              IL  .EQ. 2          ) V = -1.D0
!
!******** TRAITEMENT DES SIGNES MOINS **************
!
          IF( AK  .GT. 0.D0 .AND.              &
              IC  .EQ. 3    .AND.              &
              KM6 .EQ. 4    .AND.              &
              IS  .EQ. IL   .AND.              &
              IL  .EQ. 2          ) V = -1.D0
          IF( AK  .GT. 0.D0 .AND.              &
              IC  .EQ. 3    .AND.              &
              KM6 .EQ. 5    .AND.              &
              IS  .EQ. IL   .AND.              &
              IL  .EQ. 2          ) V = -1.D0
!
!******** TRAITEMENT DU CARACTERE IMAGINAIRE **************
!
          IF( AK  .GT. 0.D0 .AND.           &
              IC  .EQ. 2    .AND.           &
              KM6 .EQ. 0    .AND.           &
              IS  .EQ. 2    .AND.           &
              IL  .EQ. 2          ) IP = 1
          IF( AK  .GT. 0.D0 .AND.           &
              IC  .EQ. 3    .AND.           &
              KM6 .EQ. 5    .AND.           &
              IS  .EQ. 1    .AND.           &
              IL  .EQ. 1          ) IP = 1
          IF( AK  .GT. 0.D0 .AND.           &
              IC  .EQ. 3    .AND.           &
              KM6 .EQ. 4    .AND.           &
              IS  .EQ. 2    .AND.           &
              IL  .EQ. 2          ) IP = 1
          IF( AK  .GT. 0.D0 .AND.           &
              IC  .EQ. 1    .AND.           &
              KM6 .EQ. 3    .AND.           &
              IS  .EQ. 1    .AND.           &
              IL  .EQ. 1          ) IP = 1
          IF( AK  .GT. 0.D0 .AND.           &
              IC  .EQ. 3    .AND.           &
              KM6 .EQ. 2    .AND.           &
              IS  .EQ. 2    .AND.           &
              IL  .EQ. 2          ) IP = 1
          IF( AK  .GT. 0.D0 .AND.           &
              IC  .EQ. 3    .AND.           &
              KM6 .EQ. 1    .AND.           &
              IS  .EQ. 1    .AND.           &
              IL  .EQ. 1          ) IP = 1
        ELSE
!
!CCCCCCCCCCCCCCCCC**********   J IMPAIR *********
!CC*************** 1+6p  *********
!
          IF( AK  .GT. 0.D0 .AND.              &
              IC  .EQ. 3    .AND.              &
              KM6 .EQ. 1    .AND.              &
              IS  .EQ. IL         ) V =  1.D0
          IF( AK  .GT. 0.D0 .AND.              &
              IC  .EQ. 3    .AND.              &
              KM6 .EQ. 1    .AND.              &
              IS  .EQ. IL   .AND.              &
              IL  .EQ. 1          ) V = -1.D0
          IF( AK  .GT. 0.D0 .AND.           &
              IC  .EQ. 3    .AND.           &
              KM6 .EQ. 1    .AND.           &
              IS  .EQ. IL   .AND.           &
              IL  .EQ. 2          ) IP = 1
!
!C**************** 2+6p  *********
!
          IF( AK  .GT. 0.D0 .AND.              &
              IC  .EQ. 3    .AND.              &
              KM6 .EQ. 2    .AND.              &
              IS  .EQ. IL         ) V =  1.D0
          IF( AK  .GT. 0.D0 .AND.              &
              IC  .EQ. 3    .AND.              &
              KM6 .EQ. 2    .AND.              &
              IS  .EQ. IL   .AND.              &
              IL  .EQ. 2          ) V = -1.D0
          IF( AK  .GT. 0.D0 .AND.           &
              IC  .EQ. 3    .AND.           &
              KM6 .EQ. 2    .AND.           &
              IS  .EQ. IL   .AND.           &
              IL  .EQ. 1          ) IP = 1
!
!C**************** 3+6p  ************
!
          IF( AK  .GT. 0.D0 .AND.              &
              IC  .EQ. 1    .AND.              &
              KM6 .EQ. 3    .AND.              &
              IS  .EQ. 1    .AND.              &
              IL  .EQ. 1          ) V =  1.D0
          IF( AK  .GT. 0.D0 .AND.              &
              IC  .EQ. 2    .AND.              &
              KM6 .EQ. 3    .AND.              &
              IS  .EQ. 2    .AND.              &
              IL  .EQ. 2          ) V = -1.D0
          IF( AK  .GT. 0.D0 .AND.           &
              IC  .EQ. 2    .AND.           &
              KM6 .EQ. 3    .AND.           &
              IS  .EQ. 2    .AND.           &
              IL  .EQ. 2          ) IP = 1
!
!C**************** 4+6p  *********
!
          IF( AK  .GT. 0.D0 .AND.              &
              IC  .EQ. 3    .AND.              &
              KM6 .EQ. 4    .AND.              &
              IS  .EQ. IL         ) V = -1.D0
          IF( AK  .GT. 0.D0 .AND.           &
              IC  .EQ. 3    .AND.           &
              KM6 .EQ. 4    .AND.           &
              IS  .EQ. IL   .AND.           &
              IL  .EQ. 1          ) IP = 1
!
!C*************** 5+6p  *********
!
          IF( AK  .GT. 0.D0 .AND.              &
              IC  .EQ. 3    .AND.              &
              KM6 .EQ. 5    .AND.              &
              IS  .EQ. IL         ) V = -1.D0
          IF( AK  .GT. 0.D0 .AND.           &
              IC  .EQ. 3    .AND.           &
              KM6 .EQ. 5    .AND.           &
              IS  .EQ. IL   .AND.           &
              IL  .EQ. 2          ) IP = 1
!
!C**************** 6+6p  ************
!
          IF( AK  .GT. 0.D0 .AND.              &
              IC  .EQ. 1    .AND.              &
              KM6 .EQ. 6    .AND.              &
              IS  .EQ. 1    .AND.              &
              IL  .EQ. 1          ) V =  1.D0
          IF( AK  .GT. 0.D0 .AND.           &
              IC  .EQ. 1    .AND.           &
              KM6 .EQ. 6    .AND.           &
              IS  .EQ. 1    .AND.           &
              IL  .EQ. 1          ) IP = 1
          IF( AK  .GT. 0.D0 .AND.              &
              IC  .EQ. 2    .AND.              &
              KM6 .EQ. 6    .AND.              &
              IS  .EQ. 2    .AND.              &
              IL  .EQ. 2          ) V = -1.D0
        ENDIF
      ENDIF
!
      RETURN
      END SUBROUTINE VKCS
