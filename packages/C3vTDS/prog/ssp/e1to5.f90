!
! 05/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3v.
!
!     E1TO5=<(IB4*IB5)IB45//(IA4*IA5)IA45//(IK4*IK5)IK45>
!     IPM=+1 POUR UN OPERATEUR CREATION
!     IPM=-1 POUR UN OPERATEUR ANNIHILATION
!
      FUNCTION E1TO5(IPM,IB,IA,IK)
      use mod_dppr
      use mod_par_tds
      IMPLICIT NONE
      real(kind=dppr)  :: E1TO5
      integer ,dimension(NBVQN+1)  :: IB,IA,IK
      integer          :: IPM

! functions
      real(kind=dppr)  :: E1TO4,EMRVA
      integer          :: IDIMK

      real(kind=dppr)  :: DA45,DB45,DK45
      real(kind=dppr)  :: EUF
      real(kind=dppr)  :: RK,R1TO4,R5

      integer          :: IAL4,IAV4,IBL4,IBV4,IKL4,IKV4
      integer          :: IAL5,IAV5,IBL5,IBV5,IKL5,IKV5
      integer          :: IA45,IA456,IB45,IB456,IK45,IK456
      integer          :: IFF
!
      E1TO5 = 0.D0
      CALL VLNC(IB(4),IBV4,IBL4)
      CALL VLNC(IA(4),IAV4,IAL4)
      CALL VLNC(IK(4),IKV4,IKL4)
      CALL VLNC(IB(5),IBV5,IBL5)
      CALL VLNC(IA(5),IAV5,IAL5)
      CALL VLNC(IK(5),IKV5,IKL5)
      CALL VLNC(IB(7),IB45,IB456)
      CALL VLNC(IA(7),IA45,IA456)
      CALL VLNC(IK(7),IK45,IK456)
      DB45 = DBLE(IDIMK(DBLE(IB45-2)))
      DA45 = DBLE(IDIMK(DBLE(IA45-2)))
      DK45 = DBLE(IDIMK(DBLE(IK45-2)))
      RK   = SQRT(DB45*DA45*DK45)
      CALL NF9K(DBLE(IBL4),DBLE(IBL5),DBLE(IB45-2),DBLE(IAL4),  &
                DBLE(IAL5),DBLE(IA45-2),DBLE(IKL4),DBLE(IKL5),  &
                DBLE(IK45-2),EUF,IFF)
      R5    = EMRVA(2,IBV5,IBL5,IPM,IAV5,IAL5,IKV5,IKL5)
      R1TO4 = E1TO4(IPM,IB,IA,IK)
      E1TO5 = RK*EUF*R5*R1TO4
!
      RETURN
      END FUNCTION E1TO5
