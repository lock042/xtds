      SUBROUTINE XWPART
!
! 2011 JULY
! V. BOUDON, Ch. WENGER
!
! WRITE OUT  RMS and STD per transition
!
! BASED UPON xwpaes.f90
!
      use mod_dppr
      use mod_par_tds
      use mod_par_x
      use mod_com_cas
      use mod_com_const
      use mod_com_xassi
      use mod_com_xfonc
      use mod_com_xpoly
      use mod_com_xtran
      IMPLICIT NONE

! functions
      real(kind=dppr)  :: ENORM,RMSH,RMST

      real(kind=dppr) ,dimension(MXOBZ)  :: FOMCP
      real(kind=dppr)  :: PSTD
      real(kind=dppr)  :: RMSHP,RMSTP

      integer ,dimension(MXOBZ)  :: ISEL
      integer          :: I,ITR
      integer          :: J,JPF,JPS
      integer          :: NBPFZ,NBPOBZ,NBPSZ

      character(len =   3) ,dimension(MXATR)  :: DIPPOL
      character(len =   3)  :: RMSHU
!
1000  FORMAT(A)
2005  FORMAT(A,' TRANSITION : ',I5,' Frequency Data ; Jmax ',I3,7X,'RMS = ',F11.3,1X,A    ,/,   &
             28X               ,I5,' Intensity Data ; Jmax ',I3,7X,'RMS = ',F11.3,1X,'  %'   )
2006  FORMAT(28X               ,I5,'           Data ; Standard Deviation = ',F11.3)
!
      DO ITR=1,NTR
        IF( ISRAM(ITR) ) THEN
          DIPPOL(ITR) = 'pol'
          POLSTC(ITR) = POLST(ITR)
        ELSE
          DIPPOL(ITR) = ''
          POLSTC(ITR) = ''
        ENDIF
      ENDDO
!
! STANDARD DEVIATION FOR EACH TRANSITION
E33:  DO ITR=1,NTR
        NBPFZ  = 0                                                                                 ! partial number of OBZ - frequency
        NBPSZ  = 0                                                                                 ! partial number of OBZ - intensity
        NBPOBZ = 0                                                                                 ! partial number of OBZ
        JPF    = 0                                                                                 ! partial JMAX          - frequency
        JPS    = 0                                                                                 ! partial JMAX          - intensity
        RMSHP  = 0.D0                                                                              ! partial RMS           - frequency
        RMSTP  = 0.D0                                                                              ! partial RMS           - intensity
        RMSHU  = ' mk'                                                                             ! partial RMS           - frequency unit
        IF( NBFOBZ .NE. 0 ) THEN                                                                   ! frequency
          DO I=1,NBFOBZ
            IF( JTR(INDOBS(I)) .EQ. ITR ) THEN
              NBPFZ         = NBPFZ+1
              ISEL(NBPFZ)   = I
              NBPOBZ        = NBPOBZ+1
              FOMCP(NBPOBZ) = FOMC(I)
              IF( JIOBS(INDOBS(I)) .GT. JPF ) JPF = JIOBS(INDOBS(I))
            ENDIF
          ENDDO
          IF( NBPFZ .NE. 0 ) THEN
            RMSHP = RMSH(NBPFZ,ISEL)
            IF( INPS(ITR) .EQ. INPI(ITR) ) THEN                                                    ! PimPi
              IF    ( ISPT(INDOBS(ISEL(1))) .EQ. 'M' ) THEN                                        ! implies that all the data of this transition are of the same unit
                RMSHU = 'KHz'
                RMSHP = RMSHP*CMHZ                                                                 ! MHz
              ELSEIF( ISPT(INDOBS(ISEL(1))) .EQ. 'G' ) THEN
                RMSHU = 'MHz'
                RMSHP = RMSHP*CGHZ                                                                 ! GHz
              ENDIF
            ENDIF
          ENDIF
        ENDIF
        IF( NBSOBZ .NE. 0 ) THEN                                                                   ! intensity
          DO I=1,NBSOBZ
            J = NBFOBZ+I
            IF( JTR(INDOBS(J)) .EQ. ITR .AND.         &
                SASOZ(I)       .NE. '-'       ) THEN
              NBPSZ         = NBPSZ+1
              ISEL(NBPSZ)   = J
              NBPOBZ        = NBPOBZ+1
              FOMCP(NBPOBZ) = FOMC(J)
              IF( JIOBS(INDOBS(J)) .GT. JPS ) JPS = JIOBS(INDOBS(J))
            ENDIF
          ENDDO
          IF( NBPSZ .NE. 0 ) RMSTP = RMST(NBPSZ,ISEL)
        ENDIF
        PRINT 2005, 'P'//CPOL(INPS(ITR))//'mP'//CPOL(INPI(ITR))//' '//DIPPOL(ITR)//' '//POLSTC(ITR),  &
                    NBPFZ,JPF,RMSHP*1.D+3,                                                            &
                    RMSHU,                                                                            &
                    NBPSZ,JPS,RMSTP*1.D+2
        IF( NBPOBZ .EQ. 0 ) CYCLE E33
        PSTD = ENORM(NBPOBZ,FOMCP)/SQRT(DBLE(NBPOBZ))                                              ! std. dev.
        PRINT 2006, NBPOBZ,PSTD
      ENDDO E33
      PRINT 1000
!
      RETURN
      END SUBROUTINE XWPART
