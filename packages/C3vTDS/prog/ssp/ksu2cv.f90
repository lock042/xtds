!
! 05/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3v.
!
!  CE PROGRAMME CALCULE LES COEFFICIENTS ISOSCALAIRES K
!  DANS LA CHAINE SU(2)_CIV
!
      SUBROUTINE KSU2CV(AJ1,AJ2,AJ3,AK1,AK2,AK3,XK,IFK)
      use mod_dppr
      IMPLICIT NONE
      real(kind=dppr)  :: AJ1,AJ2,AJ3,AK1,AK2,AK3,XK
      integer          :: IFK

! functions
      real(kind=dppr)  :: FJKS
      integer          :: KTR

      real(kind=dppr)  :: FJK
      real(kind=dppr)  :: TRK

      integer          :: IS1,ISI1,ISP1,ISS1
      integer          :: IS2,ISI2,ISP2,ISS2
      integer          :: IS3,ISI3,ISP3,ISS3
      integer          :: ITRK
!
      XK  = 0.D0
      IFK = 0
      IF( KTR(AK1,AK2,AK3) .EQ. 0 ) RETURN
      CALL LIMK(AK1,ISI1,ISS1,ISP1)
      DO IS1=ISI1,ISS1,ISP1
        CALL LIMK(AK2,ISI2,ISS2,ISP2)
        DO IS2=ISI2,ISS2,ISP2
          CALL LIMK(AK3,ISI3,ISS3,ISP3)
E30:      DO IS3=ISI3,ISS3,ISP3
            CALL TROIK(AK1,AK2,AK3,IS1,IS2,IS3,TRK,ITRK)
            FJK = FJKS(AJ1,AK1,IS1,AJ2,AK2,IS2,AJ3,AK3,IS3)
!           IF( TRK*FJK .EQ. 0.D0 ) CYCLE E30
            IF( ABS(TRK*FJK) .LT. 1.D-6 ) CYCLE E30
            XK = FJK/TRK
            IF( ITRK .EQ. 1 ) THEN
              IFK =  1
              XK  = -XK
            ENDIF
            RETURN
          ENDDO E30
        ENDDO
      ENDDO
!
      RETURN
      END SUBROUTINE KSU2CV
