!
! 05/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3v.
!
! CE PROGRAMME CALCULE LES COEFFICIENTS 3K-IS
!
      SUBROUTINE TROIK(AK1,AK2,AK3,IS1,IS2,IS3,TRK,ITRK)
      use mod_dppr
      IMPLICIT NONE
      real(kind=dppr)  :: AK1,AK2,AK3,TRK
      integer          :: IS1,IS2,IS3,ITRK

! functions
      real(kind=dppr)  :: EPSJK2,FJKS,NJK
      integer          :: KTR

      real(kind=dppr)  :: AJ1,AJ2,AJ3,AN
      real(kind=dppr)  :: DMK3
      real(kind=dppr)  :: EPS2
      real(kind=dppr)  :: FJK
!
      TRK  = 0.D0
      ITRK = 0
      IF( KTR(AK1,AK2,AK3) .EQ. 0 ) RETURN
      AJ1  = ABS(AK1)
      AJ2  = ABS(AK2)
      AJ3  = ABS(AK3)
      EPS2 = EPSJK2(AJ1,AK1,AJ2,AK2,AJ3,AK3)
      FJK  = FJKS(AJ1,AK1,IS1,AJ2,AK2,IS2,AJ3,AK3,IS3)
      AN   = NJK(AJ1,AK1,AJ2,AK2,AJ3,AK3)
      IF( AK3 .LE. 0.D0 ) THEN
        DMK3 = 1.D0
      ELSE
        DMK3 = 2.D0
      ENDIF
      TRK = TRK+(SQRT(DMK3)/SQRT(AN))*FJK
      IF( EPS2 .LE. 0.D0 ) ITRK = 1
!
      RETURN
      END SUBROUTINE TROIK
