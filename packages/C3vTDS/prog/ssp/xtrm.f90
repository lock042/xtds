      SUBROUTINE XTRM(ITR)
!
! 2007 SEPT
! V.BOUDON, CH.WENGER
!
! ####################################################################
! ##                                                                ##
! ##  MATRIX ELEMENTS OF TRANSITION MOMENT CALCULUS IN PROPER BASE  ##
! ##                                                                ##
! ####################################################################
!
! BASED UPON trm.f90
!
!
      use mod_dppr
      use mod_par_tds
      use mod_com_xpoly
      use mod_main_xpafit
      IMPLICIT NONE
      integer          :: ITR

      real(kind=dppr)  :: A
      real(kind=dppr)  :: COEFAN,COEFIS
      real(kind=dppr)  :: EMPRO
      real(kind=dppr)  :: TM,TMA,TMI

      integer          :: I,ICK,ICK1,ICL,ICL1,ICLUK,ICLUL,IDEB,IDJ
      integer          :: II,IK,IK1,IL,IL1,IN,IOP,IOP1,IS,ISV
      integer          :: J,JCL,JK,JK1,JKM,JL,JL1,JLUK,JLUL
      integer          :: KK,KL
      integer          :: LG
      integer          :: NBELM,NBOPTC,NBOTR,NBR,NBRANC,NFBK,NFBL
      integer          :: NICK,NICL,NL1,NK1,NNIVI,NNIVS,NSV

      character(len = 120)  :: FXTRM,FXVPI,FXVPS,FSEM

      logical          :: INTRA
!
8000  FORMAT(' !!! XTRM   : STOP ON ERROR')
8101  FORMAT(' !!! ERROR OPENING TRANSITION MOMENT FILE : ',A)
8102  FORMAT(' !!! ERROR OPENING LOWER EIGENVAL. AND VECT. FILE : ',A)
8103  FORMAT(' !!! ERROR OPENING UPPER EIGENVAL. AND VECT. FILE : ',A)
8105  FORMAT(' !!! ERROR OPENING MATRIX ELEMENT FILE : ',A)
8106  FORMAT(' !!! LOWER LEVEL FILE => UNEXPECTED EOF')
8107  FORMAT(' !!! UPPER LEVEL FILE => UNEXPECTED EOF')
8108  FORMAT(' !!! TRANSITION MOMENT FILE ',a,' => UNEXPECTED EOF')
8109  FORMAT(' !!! J,C ERROR')
!
      FXVPI = 'XVP_P'//CPOL(INPI(ITR))//'_'
      FXVPS = 'XVP_P'//CPOL(INPS(ITR))//'_'
      IF( FXVPI .EQ. FXVPS ) THEN
        INTRA = .TRUE.
      ELSE
        INTRA = .FALSE.
      ENDIF
      IF( ISRAM(ITR) ) THEN
! RAMAN INTENSITY COEFFICIENTS
        IF    ( POLST(ITR) .EQ. 'R111' ) THEN
          COEFIS = SQRT(1.D0/3.D0)
          COEFAN = SQRT(7.D0/30.D0)
        ELSEIF( POLST(ITR) .EQ. 'R110' ) THEN
          COEFIS = SQRT(1.D0/3.D0)
          COEFAN = SQRT(4.D0/30.D0)
        ELSEIF( POLST(ITR) .EQ. 'R001' ) THEN
          COEFIS = 0.D0
          COEFAN = SQRT(3.D0/30.D0)
        ENDIF
        FSEM   = 'PO_P'//CPOL(INPS(ITR))//'mP'//CPOL(INPI(ITR))//'_D'//TRIM(CDVO(ITR))//'_'        ! polarizability matrix elements
        NBRANC = 5*MXSYM
        NBR    = 2
      ELSE
        FSEM   = 'DI_P'//CPOL(INPS(ITR))//'mP'//CPOL(INPI(ITR))//'_D'//TRIM(CDVO(ITR))//'_'        ! dipolar moment matrix elements
        NBRANC = 3*MXSYM
        NBR    = 1
      ENDIF
      FXTRM = 'XTRM_'//CTR(ITR)                                                                    ! transition moment
      NNIVS = NNIV(INPS(ITR))
      NNIVI = NNIV(INPI(ITR))
      IDEB  = INPHT(INTRT(ITR))                                                                    ! index-1 of first T parameter
!
! FILES
!
      OPEN(80,ERR=4081,FILE=FXTRM,STATUS='UNKNOWN',FORM='UNFORMATTED')                             ! transition moment file
      IF( INTRA ) THEN
        OPEN(60,STATUS='SCRATCH',FORM='UNFORMATTED')
      ELSE
        OPEN(60,ERR=4060,FILE=FXVPI,STATUS='OLD',FORM='UNFORMATTED')                               ! lower level file
      ENDIF
      OPEN(61,ERR=4061,FILE=FXVPS,STATUS='OLD',FORM='UNFORMATTED')                                 ! upper level file
      OPEN(40,ERR=4040,FILE=FSEM,STATUS='OLD',FORM='UNFORMATTED')                                  ! matrix element file
!
! *** READ H PARAMETERS
!     FROM UPPER POLYAD EIGENVECTORS/VALUES FILE
!     AND COPY TO OUTPUT FILE
!
      IF( INTRA ) THEN
!
5000    READ (61,END=5002) JLUL,ICLUL,NFBL
        WRITE(60)          JLUL,ICLUL,NFBL
        DO WHILE( NFBL .GT. MXDIMI )
          CALL RESIZE_MXDIMI
        ENDDO
        IF( NFBL .EQ. 0 ) GOTO 5000
        READ (61) (HDL(I),I=1,NFBL)
        WRITE(60) (HDL(I),I=1,NFBL)
        DO IL=1,NFBL
          READ (61) NICL,ICLRO,(TL(IL,I),I=1,NFBL)
          WRITE(60) NICL,ICLRO,(TL(IL,I),I=1,NFBL)
        ENDDO
        GOTO 5000
!
5002    ENDFILE(60)
        REWIND(61)
      ENDIF
!
! *** READ HEADING PART OF TRANSITION MOMENT MATRIX ELEMENT FILE
!
      DO I=1,8+NNIVI+NNIVS
        READ(40)
      ENDDO
      DO IN=1,2                                                                                    ! 2 levels
        READ(40) NSV
        DO ISV=1,NSV
          READ(40)
        ENDDO
      ENDDO
      DO I=1,4
        READ(40)
      ENDDO
      READ(40) NBOPTC
      DO WHILE( NBOPTC .GT. MXOPT )
        CALL RESIZE_MXOPT
      ENDDO
      NBOTR = NBOPTC
      WRITE(80) NBOTR
!
! *** UPPER LEVEL LOOP (K=SUP)
! *** JSUP LOOP
!
      JKM = JMAX(INPS(ITR))
      IF( INPS(ITR) .EQ. INPI(ITR) ) JKM = JKM-1
E3:   DO JK=0,JKM
!
! *** RESET POSITION OF LOWER POLYAD EIGENVECTORS FILE
!
        REWIND(60)                                                                                 ! reread heading part of lower file
!
! *** STORE DIMENSIONS AND EIGENVECTORS/VALUES OF LOWER POLYAD
!     FOR THE 3 OR 5 BRANCHES * MXSYM VALUES OF ICINF
!
        DO I=1,NBRANC
          JDIM(I) = 0
        ENDDO
        DO JL=MAX(JK-NBR,0),JK+NBR
          DO ICL=1,MXSYM
!
6666        READ(60,END=67) JLUL,ICLUL,NFBL
            IF( JLUL .GT. JK+NBR ) GOTO 67
            DO WHILE( NFBL .GT. MXDIMI )
              CALL RESIZE_MXDIMI
            ENDDO
            IF( NFBL .NE. 0      ) THEN
              READ(60,END=4160) (HDL(I),I=1,NFBL)
              DO IL=1,NFBL
                READ(60,END=4160) NICL,ICLRO,(TL(IL,I),I=1,NFBL)
              ENDDO
            ENDIF
            IF( JLUL .LT. JL ) GOTO 6666
            JCL       = (JLUL-JK+NBR)*MXSYM+ICLUL
            JDIM(JCL) = NFBL
            DO I=1,NFBL
              HDLJ(I,JCL) = HDL(I)
              DO J=1,NFBL
                TLJ(J,I,JCL) = TL(J,I)
              ENDDO
            ENDDO
          ENDDO
        ENDDO
!
! *** ICSUP LOOP
!
67      CONTINUE
E103:   DO ICK=1,MXSYM
!
! *** STORE DIMENSIONS AND EIGENVECTORS/VALUES OF UPPER POLYAD
!     FOR JSUP,ICSUP BLOCK
!
          READ(61,END=4161) JLUK,ICLUK,NFBK
          READ(40,END=4140) JK1,ICK1,NK1
          IF( JK   .NE. JK1   .OR.              &
              ICK  .NE. ICK1       ) GOTO 4151
          IF( NFBK .EQ. 0          ) CYCLE E103
          IF( JLUK .NE. JK    .OR.                  &
              ICK  .NE. ICLUK      ) BACKSPACE(61)
          IF( JLUK .NE. JK    .OR.               &
              ICK  .NE. ICLUK      ) CYCLE E103
          READ(61,END=4161) (HDK(I),I=1,NFBK)
          DO IK=1,NFBK
            READ(61,END=4161) NICK,ICKRO,(TK(IK,I),I=1,NFBK)
          ENDDO
!
! ******************************************************
! *** TRANSITION MATRIX IN BASE FONCTIONS
! ******************************************************
!
! *** STORE MATRIX ELEMENTS IN BASE FONCTIONS
!
E6:       DO JL=MAX(JK-NBR,0),JK+NBR
            READ(40,END=4140) JK1,ICK1,NK1,  &
                              JL1,ICL1,NL1
            ICL = ICL1
            JCL = (JL-JK+NBR)*MXSYM+ICL
            IF( JL  .NE. JL1 ) GOTO 4151
            IF( NL1 .EQ. 0   ) CYCLE E6
            IDJ = JL-JK+NBR+1
!
! *** INITIALIZE DIP(IK1,IL1,IDG,IOP)
!
            DO IOP=1,NBOPTC
              DO IS=1,MXDIMS
                DO II=1,MXDIMI
                  DIP(IS,II,IDJ,IOP) = 0.D0
                ENDDO
              ENDDO
            ENDDO
!
! *** OPERATOR LOOP
!
            SELECT CASE( NBR )
!
! *** DIPOLAR MOMENT
!
              CASE( 1 )
E301:           DO IOP=1,NBOPTC
!
146               READ(40,END=4140) JK1,ICK1,NK1,  &
                                    JL1,ICL1,NL1,  &
                                    IOP1,NBELM
                  DO WHILE( NBELM .GT. MXELMT )
                    CALL RESIZE_MXELMT
                  ENDDO
                  IF( JL    .EQ. JL1  .AND.             &
                      JK    .EQ. JK1  .AND.             &
                      ICK   .EQ. ICK1 .AND.             &
                      IOP   .EQ. IOP1       ) GOTO 150
                  IF( NBELM .NE. 0          ) READ(40,END=4140)
                  GOTO 146
!
150               IF( NBELM .EQ. 0 ) CYCLE E301
                  READ(40,END=4140) (LITR(I),KOTR(I),HTR(I),I=1,NBELM)
                  DO I=1,NBELM
                    IK1                  = KOTR(I)
                    IL1                  = LITR(I)
                    DIP(IK1,IL1,IDJ,IOP) = HTR(I)
                  ENDDO
                ENDDO E301
!
! *** POLARIZABILITY
!
              CASE( 2 )
E351:           DO IOP=1,NBOPTC
!
246               READ(40,END=4140) JK1,ICK1,NK1,  &
                                    JL1,ICL1,NL1,  &
                                    IOP1,NBELM,LG
                  IF( JL    .EQ. JL1  .AND.             &
                      JK    .EQ. JK1  .AND.             &
                      ICK   .EQ. ICK1 .AND.             &
                      IOP   .EQ. IOP1       ) GOTO 250
                  IF( NBELM .NE. 0          ) READ(40,END=4140)
                  GOTO 246
!
250               IF( NBELM .EQ. 0 ) CYCLE E351
                  ICOEFI(IOP) = 1
                  IF( LG .EQ. 2 ) ICOEFI(IOP) = 2
                  READ(40,END=4140) (LITR(I),KOTR(I),HTR(I),I=1,NBELM)
                  DO I=1,NBELM
                    IK1                  = KOTR(I)
                    IL1                  = LITR(I)
                    DIP(IK1,IL1,IDJ,IOP) = HTR(I)
                  ENDDO
                ENDDO E351
            END SELECT
!
! ******************************************************
! *** TRANSITION MATRIX IN  PROPER FONCTIONS
! ******************************************************
!
! *** (O) P Q R (S) LOOP ON LOWER LEVELS (L=INF)
!
            NFBL = JDIM(JCL)
            IF( NFBL .EQ. 0 ) CYCLE E6
            DO I=1,NFBL
              HDL(I) = HDLJ(I,JCL)
              DO J=1,NFBL
                TL(J,I) = TLJ(J,I,JCL)
              ENDDO
            ENDDO
            IDJ = JL-JK+NBR+1
            WRITE(80) JK,ICK,NFBK,  &
                      JL,ICL,NFBL
!
! *** UPPER LEVEL LOOP (H=SUP)
!
E4:         DO IK=1,NFBK
!
! *** LOWER LEVEL LOOP
!
E14:          DO IL=1,NFBL
                TM  = 0.D0
                TMI = 0.D0
                TMA = 0.D0
!
! *** OPERATOR LOOP
!
E114:           DO IOP=1,NBOPTC
                  EMPRO = 0.D0
                  DO KL=1,NFBL
                    A = 0.D0
                    DO KK=1,NFBK
                      A = A+DIP(KK,KL,IDJ,IOP)*TK(KK,IK)
                    ENDDO
                    EMPRO = EMPRO+A*TL(KL,IL)
                  ENDDO
                  IF( ISRAM(ITR) ) THEN
                    IF( ICOEFI(IOP) .EQ. 1 ) THEN
                      TMI = TMI+EMPRO*PARA(IDEB+IOP)*COEFIS
                    ELSE
                      TMA = TMA+EMPRO*PARA(IDEB+IOP)*COEFAN
                    ENDIF
                  ELSE
                    TM = TM+EMPRO*PARA(IDEB+IOP)                                                   ! --> transition moment matrix elements
                  ENDIF
                ENDDO E114                                                                         ! operator loop
                IF( ISRAM(ITR) ) THEN
                  TM = SQRT(TMI*TMI+TMA*TMA)
                ENDIF
                WRITE(80) IK,IL,TM
              ENDDO E14                                                                            ! lower level loop
            ENDDO E4                                                                               ! upper level lopp (NFB)
          ENDDO E6                                                                                 ! lower level PQR loop
        ENDDO E103                                                                                 ! upper level loop (C)
      ENDDO E3                                                                                     ! upper level loop (J)
      CLOSE(80)
      GOTO 9000
!
4081  PRINT 8101, FXTRM
      GOTO  9999
4060  PRINT 8102, FXVPI
      GOTO  9999
4061  PRINT 8103, FXVPS
      GOTO  9999
4040  PRINT 8105, FSEM
      GOTO  9999
4160  PRINT 8106
      GOTO  9999
4161  PRINT 8107
      GOTO  9999
4140  PRINT 8108, 'P'//CPOL(INPS(ITR))//'mP'//CPOL(INPI(ITR))
      GOTO  9999
4151  PRINT 8109
      GOTO  9999
9999  PRINT 8000
      PRINT *
      STOP
!
9000  CLOSE(40)
      CLOSE(60)
      CLOSE(61)
!
      RETURN
      END SUBROUTINE XTRM
