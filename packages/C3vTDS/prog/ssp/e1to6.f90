!
! 05/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3v.
!
!     E1TO6=<(IB45*IB6)IB456//(IA45*IA6)IA456//(IK45*IK6)IK456>
!     IPM=+1 POUR UN OPERATEUR CREATION
!     IPM=-1 POUR UN OPERATEUR ANNIHILATION
!
      FUNCTION E1TO6(IPM,IB,IA,IK)
      use mod_dppr
      use mod_par_tds
      IMPLICIT NONE
      real(kind=dppr)  :: E1TO6
      integer ,dimension(NBVQN+1)  :: IB,IA,IK
      integer          :: IPM

! functions
      real(kind=dppr)  :: E1TO5,EMRVA
      integer          :: IDIMK

      real(kind=dppr)  :: DA456,DB456,DK456
      real(kind=dppr)  :: EUF
      real(kind=dppr)  :: RK,R1TO5,R6

      integer          :: IAL6,IAV6,IBL6,IBV6,IKL6,IKV6
      integer          :: IA45,IA456,IB45,IB456,IK45,IK456
      integer          :: IFF
!
      E1TO6 = 0.D0
      CALL VLNC(IB(6),IBV6,IBL6)
      CALL VLNC(IA(6),IAV6,IAL6)
      CALL VLNC(IK(6),IKV6,IKL6)
      CALL VLNC(IB(7),IB45,IB456)
      CALL VLNC(IA(7),IA45,IA456)
      CALL VLNC(IK(7),IK45,IK456)
      DB456 = DBLE(IDIMK(DBLE(IB456-2)))
      DA456 = DBLE(IDIMK(DBLE(IA456-2)))
      DK456 = DBLE(IDIMK(DBLE(IK456-2)))
      RK    = SQRT(DB456*DA456*DK456)
      CALL NF9K(DBLE(IB45-2),DBLE(IBL6),DBLE(IB456-2),DBLE(IA45-2),  &
                DBLE(IAL6),DBLE(IA456-2),DBLE(IK45-2),DBLE(IKL6),    &
                DBLE(IK456-2),EUF,IFF)
      R6    = EMRVA(2,IBV6,IBL6,IPM,IAV6,IAL6,IKV6,IKL6)
      R1TO5 = E1TO5(IPM,IB,IA,IK)
      E1TO6 = RK*EUF*R6*R1TO5
!
      RETURN
      END FUNCTION E1TO6
