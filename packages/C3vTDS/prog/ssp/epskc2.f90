!
! 05/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3v.
!
      FUNCTION EPSKC2(AK1,AK2,AK3)
      use mod_dppr
      IMPLICIT NONE
      real(kind=dppr)  :: EPSKC2
      real(kind=dppr)  :: AK1,AK2,AK3

      real(kind=dppr)  :: AP1,AP2,AP3
!
      IF( AK1 .EQ. -1.D0 ) THEN
        AP1 = 0.D0
      ELSE
        AP1 = AK1
      ENDIF
      IF( AK2 .EQ. -1.D0 ) THEN
        AP2 = 0.D0
      ELSE
        AP2 = AK2
      ENDIF
      IF( AK3 .EQ. -1.D0 ) THEN
        AP3 = 0.D0
      ELSE
        AP3 = AK3
      ENDIF
      EPSKC2 = (-1)**NINT(AP1+AP2+AP3)
!
      RETURN
      END FUNCTION EPSKC2
