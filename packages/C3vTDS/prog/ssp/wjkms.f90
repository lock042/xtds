!
! 05/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3v.
!
! CE PROGRAMME DONNE LES COEFFICIENTS DE LA MATRICE DE WANG
! A LA REPRESENTATION (0-) J'ASSOCIE FORMELLEMENT -1, ET A (0+) J'ASSOCIE 0.
!
      FUNCTION WJKMS(AJ,AK,AM,IS)
      use mod_dppr
      IMPLICIT NONE
      real(kind=dppr)  :: WJKMS
      real(kind=dppr)  :: AJ,AK,AM
      integer          :: IS

      real(kind=dppr)  :: PH,PHS
!
      WJKMS = 0.D0
      PH    = (-1.D0)**NINT(AJ)/SQRT(2.D0)
      PHS   = 1.D0/SQRT(2.D0)
      IF( AJ      .LT. AK         ) RETURN
      IF( AK      .LE. 0.D0       ) WJKMS = 1.D0
      IF( ABS(AM) .EQ. AK   .AND.                 &
          AM      .GT. 0.D0 .AND.                 &
          IS      .EQ. 1          ) WJKMS =  PHS
      IF( ABS(AM) .EQ. AK   .AND.                 &
          AM      .LT. 0.D0 .AND.                 &
          IS      .EQ. 1          ) WJKMS =  PH
      IF( ABS(AM) .EQ. AK   .AND.                 &
          AM      .GT. 0.D0 .AND.                 &
          IS      .EQ. 2          ) WJKMS = -PHS
      IF( ABS(AM) .EQ. AK   .AND.                 &
          AM      .LT. 0.D0 .AND.                 &
          IS      .EQ. 2          ) WJKMS =  PH
!
      RETURN
      END FUNCTION WJKMS
