!
! 05/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3v.
!
!     SYMBOLES "3J" DE SU(2).
!
      FUNCTION TROIJD(AJ1,AJ2,AJ3,AM1,AM2,AM3)
      use mod_dppr
      IMPLICIT NONE
      real(kind=dppr)  :: TROIJD
      real(kind=dppr)  :: AJ1,AJ2,AJ3,AM1,AM2,AM3

      real(kind=dppr)  :: A
      real(kind=dppr)  :: C
      real(kind=dppr)  :: T
      real(kind=dppr)  :: X
      real(kind=dppr)  :: Y
      real(kind=dppr)  :: Z

      integer          :: I
      integer          :: K,KT,KX,KY,KZ
      integer          :: L,L1,L2,L3,L4,L5,L6,L7,L8,L9
      integer          :: MM1,MM2,MM3,MP1,MP2,MP3
!
      IF( NINT(AM1+AM2+AM3) .EQ. 0 ) GOTO 12
11    TROIJD = 0.D0
      RETURN
!
12    L1 = NINT(AJ1+AJ2-AJ3)
      L2 = NINT(AJ1-AJ2+AJ3)
      L3 = NINT(AJ2-AJ1+AJ3)
      L4 = NINT(AJ1+AJ2+AJ3+1)
      CALL PROFB(L1,L2,L3,0,0,0,0,L4,Z,KZ)
      IF( Z .EQ. 0.D0 ) GOTO 11
      MM1 = NINT(AJ1-AM1)
      MM2 = NINT(AJ2-AM2)
      MM3 = NINT(AJ3-AM3)
      MP1 = NINT(AJ1+AM1)
      MP2 = NINT(AJ2+AM2)
      MP3 = NINT(AJ3+AM3)
      CALL PROFB(MM1,MM2,MM3,MP1,MP2,MP3,0,1,Y,KY)
      IF( Y .EQ. 0.D0 ) GOTO 11
      T  = SQRT(Z*Y)
      KT = (KZ+KY)/2
      I  = MAX(0,NINT(AJ2-AJ3-AM1),NINT(AJ1-AJ3+AM2))
      L  = MIN(NINT(AJ1+AJ2-AJ3),MM1,MP2)
      C  = 0.D0
      A  = (-1)**(MP3+I+1+NINT(2.D0*(AJ1-AJ2)))
      DO K=I,L
        A  = -A
        L5 = NINT(AJ1+AJ2-AJ3)-K
        L6 = MM1-K
        L7 = NINT(AJ3-AJ2+AM1)+K
        L8 = MP2-K
        L9 = NINT(AJ3-AJ1-AM2)+K
        CALL PROFB(K,L5,L6,L7,L8,L9,0,1,X,KX)
        C = C+((A*T)/X)*( (10.D0)**(KT-KX) )
      ENDDO
      TROIJD = C
!
      RETURN
      END FUNCTION TROIJD
