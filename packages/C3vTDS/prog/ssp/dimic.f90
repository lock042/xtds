!
! 05/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3v.
!
! CE PROGRAMME CALCULE LES DIMENSIONS
! INTERVENANT DANS LE CALCUL DES 6C
!
      FUNCTION DIMIC(IC)
      use mod_dppr
      IMPLICIT NONE
      real(kind=dppr)  :: DIMIC
      integer          :: IC

!
      DIMIC = 1.D0
      IF( IC .EQ. 3 .OR.                 &
          IC .EQ. 6      ) DIMIC = 2.D0
!
      RETURN
      END FUNCTION DIMIC
