!
!  POST-FIT CALCULUS
!
      SUBROUTINE XPFITC(M,N,FJACC,LDFJAC)
      use mod_dppr
      use mod_par_tds
      use mod_par_x
      use mod_com_postf
      use mod_com_xassi
      use mod_com_xfonc
      use mod_com_xpafit
      use mod_com_xpara
      use mod_com_xpoly
      use mod_com_xtran
      use mod_com_xvari
      IMPLICIT NONE
      integer                                 :: M,N,LDFJAC
      real(kind=dppr) ,dimension(MXOBZ,MXOP)  :: FJACC

! functions
      real(kind=dppr)  :: ENORM,RMSH,RMST
      integer          :: NUNSEL

      real(kind=dppr) ,dimension(MXOP,MXOP)  :: CURVA
      real(kind=dppr)  :: CURVAC
      real(kind=dppr)  :: DIFA
      real(kind=dppr)  :: POICF,POICS
      real(kind=dppr)  :: SIG2

      integer          :: I,ITR
      integer          :: IX
      integer          :: J
      integer          :: K
      integer          :: MTRUE
!
8021  FORMAT(' !!! NOT ENOUGH MEANINGFULL DATA AFTER FIT : ',/,   &
             5X,I8,' DATA PRESELECTED'                      ,/,   &
             5X,I8,' DATA UNSELECTED DUE TO RMXOMC'            )
!
      FNORMF = ENORM(M,FOMC)                                                                       ! euclidean norm     after  fit
      MTRUE  = NBROBZ-NUNSEL()
      IF( MTRUE .LE. 0 ) THEN
        PRINT 8021, NBROBZ,NUNSEL()
        STOP
      ENDIF
      STDF  = FNORMF/SQRT(DBLE(MTRUE))                                                             ! standard deviation after  fit
      RMSHF = 0.D0
      RMSTF = 0.D0
      IF( NBFOBZ .NE. 0 ) THEN                                                                     ! frequency
        DO I=1,NBFOBZ                                                                              ! relative indexes
          ISEL(I) = I
        ENDDO
        RMSHF = RMSH(NBFOBZ,ISEL)                                                                  ! RMS                after  fit
      ENDIF
      IF( NBSOBZ .NE. 0 ) THEN                                                                     ! intensity
        DO I=1,NBSOBZ                                                                              ! relative indexes
          ISEL(I) = NBFOBZ+I
        ENDDO
        RMSTF = RMST(NBSOBZ,ISEL)                                                                  ! RMS                after  fit
      ENDIF
      NBITER = NFEV                                                                                ! nb of iterations
!
! ***************************************************
! *** PRECISION, CORRELATION, WEIGHT
! ***************************************************
!
      CALL XFJAC(M,N,XPARA,FOMC,FJACC,LDFJAC)                                                       ! derivatives
      DO J=1,N                                                                                     ! curvature matrix
        DO K=1,N
          CURVAC = 0.D0                                                                            ! initialize
          DO I=1,M
            CURVAC = CURVAC+FJACC(I,J)*FJACC(I,K)
          ENDDO
          CURVA(J,K) = CURVAC
        ENDDO
      ENDDO
! INVERT CURVATURE MATRIX
      CALL MINVRT(N,CURVA,COV)                                                                     ! covariance matrix
!
! SQUARED SIGMA
!
      SIG2 = 0.D0
      DO I=1,NBROBZ                                                                                ! nb of real freq+int to fit
        DIFA = FOMC(I)/ZWGT(I)
        SIG2 = SIG2+DIFA*DIFA
      ENDDO
      SIG2 = SIG2/(NBROBZ-N)
!
! PARAMETER FREEDOM
!
      IX = 0                                                                                       ! current index of fitted parameter
      DO J=1,NBOP                                                                                  ! for each operator
        IF( ICNTRL(J) .EQ. 2 .OR.         &
            ICNTRL(J) .EQ. 4      ) THEN                                                           ! fitted
          IX     = IX+1
          LIB(J) = NINT(1000.D0/SQRT(CURVA(IX,IX)*COV(IX,IX)))
        ELSE
          LIB(J) = 0
        ENDIF
      ENDDO
!
! (ABSOLUTE) WEIGHT OF A PARAMETER FOR A GIVEN TRANSITION
!
      DO ITR=1,NTR                                                                                 ! for each transition
        DO J=1,N                                                                                   ! for each parameter to fit
          POICF = 0.D0                                                                             ! frequency
          POICS = 0.D0                                                                             ! intensity
E209:     DO I=1,NBROBZ                                                                            ! for each real freq+int to fit
            IF( CPOLI(INDOBS(I)) .NE. CPOL(INPI(ITR)) .OR.               &
                CPOLS(INDOBS(I)) .NE. CPOL(INPS(ITR))      ) CYCLE E209                            ! not the right transition
            IF( I                .LE. NBFOBZ               ) THEN
              POICF = POICF+FJACC(I,J)*FJACC(I,J)
            ELSE
              POICS = POICS+FJACC(I,J)*FJACC(I,J)
            ENDIF
          ENDDO E209
          POIDF(J,ITR) = SIG2*POICF
          POIDS(J,ITR) = SIG2*POICS
        ENDDO
      ENDDO
!
      RETURN
      END SUBROUTINE XPFITC
