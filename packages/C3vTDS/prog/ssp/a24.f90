!
! 05/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3v.
!
      FUNCTION A24(IPM,VP,LP,K,V,L)
      use mod_dppr
      IMPLICIT NONE
      real(kind=dppr)  :: A24
      integer          :: IPM,VP,LP,K,V,L

! functions
      real(kind=dppr)  :: A21K,A22,A23
      integer          :: IDIMK,IPSIK

      real(kind=dppr)  :: DK
      real(kind=dppr)  :: E
      real(kind=dppr)  :: SIX

      integer          :: ICP,IDP,IPP,ISIX
      integer          :: KI,KJ
      integer          :: LPP
      integer          :: VPP
!
      A24 = 0.D0
      KI  = 3
      DK  = SQRT(DBLE(IDIMK(DBLE(K))))
      IF( K .NE. 0 .AND.              &
          K .NE. 2 .AND.              &
          K .NE. 4         )  RETURN
      IF( IPM*(VP-V) .NE. 4 ) RETURN
      IF    ( K .EQ. 0 ) THEN
        VPP = V+IPM*2                                                                              ! 2 = OMEGA
        KI  = 0
        KJ  = 0
      ELSEIF( K .EQ. 2 ) THEN
        VPP = V+IPM*2                                                                              ! 2 = OMEGA
        KI  = 0
        KJ  = 2
      ELSE                                                                                         ! K = 4
        VPP = V+IPM*1                                                                              ! 1 = OMEGA
        KI  = 3
        KJ  = 1
      ENDIF
      IF( KI .EQ. 0 ) THEN
        ICP = LP
        IDP = LP
        IPP = 1
      ELSE
        ICP = ABS(LP-KI)
        IDP = LP+KI
        IF( LP .EQ. 0 ) THEN
          IPP = 1
        ELSE
          IPP = 2*MIN(KI,LP)
        ENDIF
      ENDIF
      DO LPP=ICP,IDP,IPP
        IF( IPSIK(2,VPP,LPP) .EQ. 0 ) RETURN
        IF    ( K .EQ. 0 ) THEN
          E = A22(IPM,VP,LP,KI,VPP,LPP)**2
        ELSEIF( K .EQ. 2 ) THEN
          E = A22(IPM,VP,LP,KI,VPP,LPP)*A22(IPM,VPP,LPP,KJ,V,L)
        ELSE                                                                                       ! K = 4
          E = A23(IPM,VP,LP,KI,VPP,LPP)*A21K(IPM,VPP,LPP,V,L)
        ENDIF
        CALL SIX6K(DBLE(KI),DBLE(KJ),DBLE(K),DBLE(L),DBLE(LP),DBLE(LPP),SIX,ISIX)
        A24 = A24+((-1)**(KI+KJ+L+LP))*DK*E*SIX
      ENDDO
!
      RETURN
      END FUNCTION A24
