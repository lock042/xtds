      SUBROUTINE XWFA
!
! 2011 JULY
! V. BOUDON, Ch. WENGER
!
! WRITE OUT FREQUENCY FILES
!   f_prediction_*.t
!   f_statistics.t
!
! BASED UPON eq_tds.f90
!
      use mod_dppr
      use mod_par_tds
      use mod_com_branch
      use mod_com_const
      use mod_com_fdate
      use mod_com_sy
      use mod_com_xpoly
      use mod_com_xvari
      use mod_main_xpafit
      IMPLICIT NONE

      real(kind=dppr)  :: DDT,DIFF
      real(kind=dppr)  :: ENSU
      real(kind=dppr)  :: FCAL,FEXP
      real(kind=dppr)  :: PF1,PF2
      real(kind=dppr)  :: SDFCAL,SDFCALN,SDFEXP,SDFMOC,SDFMOD
      real(kind=dppr)  :: VACOII,VACOIJ

      integer          :: I,IBS,IC,ICI,ICINF,ICNTCL,ICS,ICSUP,IDP,IDPA,IDPB,II,IFOBZC,IN,IOBS,IOP
      integer          :: IOPX,IP,IPOL,IS,ISV,ITR
      integer          :: J,JCI,JI,JINF,JMACS,JOPX,JS,JSUP
      integer          :: LPC,LPCI,LPCMAC
      integer          :: NBENI,NBENIC,NBINF,NBOPHC,NBOPHI,NBOPHR
      integer          :: NBOPHS,NBOTR,NFBI,NFBS,NFBSUP,NINF
      integer          :: NSUP,NSV,NSVINF,NSVSUP

      character(len = NBCTIT)  :: TITRE
      character(len =   3) ,dimension(2)      :: DPTYP = (/ 'dip','pol' /)
      character(len =   1)  :: ISP,FASO
      character(len =   1)  :: PAINF,PASUP
      character(len =   2)  :: BR,SYINF,SYSUP
      character(len =   3)  :: DIPPOL
      character(len =   4)  :: POLSTA
      character(len =   5)  :: NUO
      character(len = 120)  :: FPARAC,FEM,FXTRM,FXEDS,FXEDI

      logical          :: INTRA
      logical          :: LASSNF,LDIP,ISRAMC,LEXDIP,LEXPOL,LWRITE
!D      logical          :: LSDT
!
1000  FORMAT(A)
2001  FORMAT(' >> ',A)
2020  FORMAT(F13.6,2X,2A,2F7.3,F11.3,F13.6,F8.3,I5,1X,2A,I3,F13.6,I3,I4,'%',2X,A,1X,2(I3,1X,2A,I3,1X))
2100  FORMAT(A,'    POLYAD ***********************************',/)
2101  FORMAT(I4,' VIBRATIONAL SUBLEVEL(S)')
2102  FORMAT(/,                                &
             I4,' HAMILTONIAN OPERATOR(S)',/)
2103  FORMAT(I4,' GREATEST VIBRATIONAL PERCENTAGE(S)',/)
2104  FORMAT(/,                                                           &
             '* POSITION :'                                         ,/,   &
             '      FEXP       CODE   SDEXP  SDTHEO  EXP-CALC      ',     &
             'FCAL      SDCAL  ( J C    N      E  )SUP         ....',     &
             ' ASSIGNMENT  ....'                                    ,/ )
2106  FORMAT(/,                                         &
             A,' TRANSITION **********************',/)
2109  FORMAT(I8,' ASSIGNMENTS FOR THIS TRANSITION',/)
!D3000  FORMAT(A,2X,2(I3,1X,2A,I3,1X),F8.3,2X,F8.3,10X,I8)
!D3001  FORMAT(/,                                                &
!D             29X,' SDFCALN    SDFCAL(mk)    delta >',I3,' %')
8000  FORMAT(' !!! XWFA   : STOP ON ERROR')
8003  FORMAT(' !!! INCOMPATIBLE'                         ,/,   &
             ' !!!     TRANSITION MOMENT        JC :',2I4,/,   &
             ' !!! AND LOWER POLYAD HAMILTONIAN JC :',2I4   )
8012  FORMAT(' !!! ERROR OPENING PARAMETER FILE : ',A)
8103  FORMAT(' !!! ERROR OPENING TRANSITION MOMENT FILE : ',A)
8104  FORMAT(' !!! UPPER LEVEL FILE => UNEXPECTED EOF')
8105  FORMAT(' !!! LOWER LEVEL FILE => UNEXPECTED EOF')
8107  FORMAT(' !!! ERROR OPENING MATRIX ELEMENT FILE : ',A)
8113  FORMAT(' !!! ERROR OPENING ',a,' FILE')
8125  FORMAT(' !!! NOR DIP NOR POL TRANSITION DEFINED')
!D8204  FORMAT(' WARNING: PF1+PF2*2.D0 = ',E11.3,' (abs(SDFCALN) used)')
!
      LEXDIP = .FALSE.                                                                             ! there is at least one dip transition
      LEXPOL = .FALSE.                                                                             ! there is at least one pol transition
      DO ITR=1,NTR
        IF( .NOT. ISRAM(ITR) ) THEN                                                                ! not raman
          LEXDIP = .TRUE.
        ELSE
          LEXPOL = .TRUE.
        ENDIF
      ENDDO
!
! indices of E1 loop, 1=dip, 2=pol
!
      IF( LEXDIP .OR.         &                                                                    ! dip transition exists
          LADIP       ) THEN                                                                       ! dip all transitions
        IDPA = 1                                                                                   ! -> dip
        IDPB = 1
        IF( LEXPOL .OR.             &                                                              ! pol transition exists
            LAPOL       ) IDPB = 2                                                                 ! pol  all transitions -> dip,pol
      ELSE
        IF( LEXPOL .OR.         &                                                                  ! pol transition exists
            LAPOL       ) THEN                                                                     ! pol  all transitions
          IDPA = 2                                                                                 ! -> pol
          IDPB = 2
        ELSE                                                                                       ! nor dip, nor pol
          PRINT 8125
          GOTO  9999
        ENDIF
      ENDIF
E1:   DO IDP=IDPA,IDPB                                                                             ! dip and/or pol
        IF( IDP .EQ. 1 ) THEN                                                                      ! dip
          LDIP   = .TRUE.
          DIPPOL = ''
          POLSTA = ''
        ELSE                                                                                       ! pol
          LDIP   = .FALSE.
          DIPPOL = DPTYP(2)
        ENDIF
        PRINT 2001,           'f_prediction_'//DPTYP(IDP)//'.t'
        OPEN(91,ERR=4080,FILE='f_prediction_'//DPTYP(IDP)//'.t',STATUS='UNKNOWN')
        FPARAC = FPARA(ITRHT(0))                                                                   ! the transition referencing H
        FPARAC = TRIM(FPARAC)//'_adj'                                                              ! fitted parameter file
        OPEN(70,ERR=9994,FILE=TRIM(FPARAC),STATUS='OLD')
!
! *******************************
! ***  READ AND COPY HEADERS  ***
! *******************************
        WRITE(91,1000)
        CALL IOAAS(70,91)                                                                          ! hamiltonian parameters
        CLOSE(70)
        WRITE(91,1000)
        WRITE(91,1000)
        DO IPOL=1,NBPOL                                                                            ! for each polyad
          FEM = 'HA_P'//CPOL(IPOL)//'_D'//TRIM(CDEV(IPOL))//'_'
          WRITE(91,2100) 'P'//CPOL(IPOL)
          OPEN(40,ERR=4040,FILE=TRIM(FEM),STATUS='OLD',FORM='UNFORMATTED')                         ! matrix elements
!
! VIBRATIONAL SUBLEVELS
!
          DO I=1,8+NNIV(IPOL)
            READ(40)
          ENDDO
          READ (40)      NSV
          WRITE(91,2101) NSV
          READ(40)
          DO ISV=1,NSV
            READ (40)      TITRE(:58)
            WRITE(91,1000) TITRE(:58)
          ENDDO
!
! NUMBER OF OPERATORS OF THE CURRENT LEVEL
!
          DO I=1,4
            READ(40)
          ENDDO
          READ (40)      NBOPHC
          WRITE(91,2102) NBOPHC
          CLOSE(40)
          IF( NBSOBZ .EQ.  0 ) NBSOBZ = -1                                                         ! fake for FXVP_
          CALL XHDIAG(IPOL)                                                                        ! XED_ files
          IF( NBSOBZ .EQ. -1 ) NBSOBZ =  0                                                         ! restore true value
        ENDDO
        WRITE(91,1000) FDATE
        WRITE(91,2104)
!
! set ICNTCL,IXCL
!
        NBOPHR = NBOHT(0)                                                                          ! nb of H operators
        ICNTCL = 0                                                                                 ! nb of fitted operators
        DO I=1,NBOPHR
          IF( ICNTRL(I) .GE. 2 ) THEN
            ICNTCL       = ICNTCL+1                                                                ! current index of fitted operators
            IXCL(ICNTCL) = I                                                                       ! index of real operator
          ENDIF
        ENDDO
!
! TRANSITION LOOP
!
E2:     DO ITR=1,NTR
!D          PRINT 1000, 'P'//CPOL(INPS(ITR))//'mP'//CPOL(INPI(ITR))
          LWRITE = .FALSE.                                                                         ! do we have to write in 91 ?
          IF( LDIP ) THEN
            IF( .NOT. ISRAM(ITR) .OR.                    &
                LADIP                 ) LWRITE = .TRUE.
          ELSE
            IF(       ISRAM(ITR) .OR.                    &
                LAPOL                 ) LWRITE = .TRUE.
          ENDIF
          IF( .NOT. LDIP ) POLSTA = POLST(ITR)
          IF( LWRITE ) THEN
            WRITE(91,2106) 'P'//CPOL(INPS(ITR))//'mP'//CPOL(INPI(ITR))//' '//DIPPOL//' '//POLSTA
            WRITE(91,2109) NBFTR(ITR)
          ENDIF
          FXEDS = 'XED_P'//CPOL(INPS(ITR))//'_'                                                    ! upper polyad energy file
          FXEDI = 'XED_P'//CPOL(INPI(ITR))//'_'                                                    ! lower polyad energy file
          IF( FXEDS .EQ. FXEDI ) THEN
            INTRA = .TRUE.
          ELSE
            INTRA = .FALSE.
          ENDIF
          OPEN(50,ERR=4050,FILE=FXEDS,STATUS='OLD',FORM='UNFORMATTED')
          IF( INTRA ) THEN                                                                         ! partial copy of FXEDS in 51
            OPEN(51,STATUS='SCRATCH',FORM='UNFORMATTED')
            READ(50) NSVINF                                                                        ! no copy
            DO I=1,NSVINF
              READ(50)                                                                             ! no copy
            ENDDO
            READ (50) NBOPHI                                                                       ! no copy
!
4001        READ (50,END=4003) JINF,ICINF,NBINF
            WRITE(51)          JINF,ICINF,NBINF
            DO IP=1,NBINF
              READ (50,END=4161) ENINFA(1)
              WRITE(51)          ENINFA(1)
              READ (50,END=4161) (DERINFA(IOP,1),IOP=1,NBOPHI)
              WRITE(51)          (DERINFA(IOP,1),IOP=1,NBOPHI)
              READ (50,END=4161) (ICENT(ISV),ISV=1,NSVINF)
              WRITE(51)          (ICENT(ISV),ISV=1,NSVINF)
            ENDDO
            GOTO 4001
!
4003        REWIND(50)
            ENDFILE(51)
            REWIND(51)
          ELSE
            OPEN(51,ERR=4051,FILE=FXEDI,STATUS='OLD',FORM='UNFORMATTED')
          ENDIF
!
! NSVSUP UPPER VIBRATIONAL SUBLEVELS
!
          READ(50) NSVSUP
          LPCMAC = MIN(NSVSUP,LPCMAX)                                                              ! reduce LPCMAX if needed
          IF( LWRITE ) THEN
            WRITE(91,2103) LPCMAC
          ENDIF
          DO ISV=1,NSVSUP
            READ(50)
          ENDDO
          READ(50) NBOPHS
!
! NSVINF  LOWER VIBRATIONAL SUBLEVELS
!
          IF( .NOT. INTRA ) THEN
            READ(51) NSVINF
            DO ISV=1,NSVINF
              READ(51)
            ENDDO
            READ(51) NBOPHI
          ENDIF
!
! TRANSITION MOMENT FILE
!
          ISRAMC = ISRAM(ITR)
          JMACS  = JMAX(INPS(ITR))
          IF( LDIP ) THEN
            ISRAM(ITR) = .FALSE.
          ELSE
            IF( .NOT. ISRAM(ITR) ) JMAX(INPS(ITR)) = JMAX(INPS(ITR))-1
            ISRAM(ITR) = .TRUE.
          ENDIF
          CALL XTRM(ITR)
          ISRAM(ITR)      = ISRAMC
          JMAX(INPS(ITR)) = JMACS
          FXTRM           = 'XTRM_'//CTR(ITR)                                                      ! transition moment file
          OPEN(80,ERR=4081,FILE=FXTRM,STATUS='OLD',FORM='UNFORMATTED')
          READ(80) NBOTR                                                                           ! NUMBER OF TRANSITION OPERATORS
!D          LSDT = .TRUE.
!D          ISDT = 20                                                                                ! SDFCAL* relative deviation in %
!
! *******************************
! *** READ LOWER ENERGY FILE  ***
! *******************************
!
          NBENI = 0
!
6         READ(51,END=8) JINF,ICINF,NBINF
          JCI = JINF*100+ICINF*10
          DO IP=1,NBINF
            NBENI = NBENI+1
            IF( NBENI .GT. MXENI ) CALL RESIZE_MXENI
            JIC(NBENI) = JCI
            READ(51,END=4160) ENINFA(NBENI)
            READ(51,END=4160) (DERINFA(IOP,NBENI),IOP=1,NBOPHI)
            READ(51,END=4160)
          ENDDO
          GOTO 6
!
8         CLOSE(51)
          CALL DEBUG( 'XWFA   => MXENI=',NBENI)
!
! TRANSITION INDEX
!
10        READ(80,END=90) JS,ICS,NFBS,  &
                          JI,ICI,NFBI
          JCI = JI*100+ICI*10
          DO I=1,NFBS
            DO J=1,NFBI
              READ(80)
            ENDDO
          ENDDO
!
! SEARCH FOR UPPER LEVEL
!
11        READ(50,END=4161) JSUP,ICSUP,NFBSUP
          IF( JS   .EQ. JSUP   .AND.            &
              ICS  .EQ. ICSUP        ) GOTO 14
          DO I=1,3*NFBSUP
            READ(50,END=4161)
          ENDDO
          GOTO 11
!
! SEARCH FOR LOWER LEVEL
!
14        DO IN=1,NBENI
            IF( JCI .EQ. JIC(IN) ) GOTO 16
          ENDDO
! NOT FOUND
          JINF   =  JIC(IN)/100
          ICINF  = (JIC(IN)-JINF*100)/10
          PRINT 8003, JI,ICI,     &
                      JINF,ICINF
          GOTO  9999
!
16        CONTINUE
E18:      DO IS=1,NFBS
            READ(50,END=4161) ENSU
            READ(50,END=4161) (DERSUPA(IOP),IOP=1,NBOPHS)
            READ(50,END=4161) (ICENT(ISV),ISV=1,NSVSUP)
            DO LPC=1,LPCMAC
              IPCSNA(LPC)  = 0
              NUSVSUA(LPC) = 0
E702:         DO IC=1,NSVSUP                                                                       ! for each sublevel
                DO LPCI=1,LPC-1
                  IF( NUSVSUA(LPCI) .EQ. IC ) CYCLE E702                                           ! IC already used
                ENDDO
                IF( IPCSNA(LPC) .GT. ICENT(IC) ) CYCLE E702                                        ! not a bigger ICENT
                IPCSNA(LPC)  = ICENT(IC)                                                           ! bigger => save
                NUSVSUA(LPC) = IC
              ENDDO E702
            ENDDO
E17:        DO II=1,NFBI
              NBENIC = IN+II-1
!
! ***********************************************************************
! *** CALCULATION OF THE STANDARD DEVIATION OF THE THEORETICAL TRANSITION
! ***********************************************************************
!
! *** CALCULATION OF THE DERIVATIVE ***
!
              DO IOP=1,NBOPHI
                DERV(IOP) = DERSUPA(IOP)-DERINFA(IOP,NBENIC)
              ENDDO
              DO IOP=NBOPHI+1,NBOPHS
                DERV(IOP) = DERSUPA(IOP)
              ENDDO
              DO IOP=NBOPHS+1,NBOPHR
                DERV(IOP) = 0.D0
              ENDDO
              PF1 = 0.D0
              PF2 = 0.D0
E773:         DO IOPX=1,ICNTCL
                DDT    = DERV(IXCL(IOPX))
                VACOII = COV(IOPX,IOPX)
                PF1    = PF1+DDT*DDT*VACOII
                IF( IOPX .EQ. ICNTCL ) CYCLE E773
                DO JOPX=IOPX+1,ICNTCL
                  VACOIJ = COV(JOPX,IOPX)
                  PF2    = PF2+DDT*DERV(IXCL(JOPX))*VACOIJ
                ENDDO
              ENDDO E773
              SDFCALN = PF1+PF2*2.D0
              IF( SDFCALN < 0.D0 ) THEN
!D                PRINT 8204, SDFCALN
                SDFCALN = ABS(SDFCALN)
              ENDIF
              SDFCALN = SQRT(SDFCALN)
              SDFCALN = SDFCALN*1.D+3
!
! ASSIGNMENT LOOP
!
              JINF   = JI
              SYINF  = SYM(ICI)
              PAINF  = PARGEN
              NINF   = II
              JSUP   = JS
              SYSUP  = SYM(ICS)
              PASUP  = PARGEN
              NSUP   = IS
              BR     = BRANCH(JSUP-JINF+MXBRA-2)
              SDFMOC = 1.D0
              IF( IMEGA(ITR) .NE. 0 ) SDFMOC = DBLE(JSUP)**IMEGA(ITR)
              SDFMOC = PNEG(ITR)*SDFMOC
              LASSNF = .TRUE.                                                                      ! assignment not found
              IF( NBFTR(ITR) .EQ. 0 ) GOTO 110                                                     ! no assignment for this transition
E217:         DO IOBS=1,NBOBS
                IF( JTR(IOBS)    .NE. ITR  .OR.                &
                    FASOT(IOBS)  .NE. '+'        ) CYCLE E217
                IF( JIOBS(IOBS)  .EQ. JI   .AND.         &
                    ICIOBS(IOBS) .EQ. ICI  .AND.         &
                    NIOBS(IOBS)  .EQ. II   .AND.         &
                    JSOBS(IOBS)  .EQ. JS   .AND.         &
                    ICSOBS(IOBS) .EQ. ICS  .AND.         &
                    NSOBS(IOBS)  .EQ. IS         ) THEN
                  LASSNF = .FALSE.                                                                 ! assignment     found
                  ISP    = ISPT(IOBS)
                  NUO    = NUOT(IOBS)
                  FEXP   = FOBS(IOBS)
                  FASO   = FASOT(IOBS)
                  SDFEXP = SDFOBS(IOBS)
                  IFOBZC = IFOBZ(IOBS)
                  DIFF   = FOMC(IFOBZC)/ZWGT(IFOBZC)
                  FCAL   = FEXP-DIFF
                  SDFCAL = SDZCAL(IFOBZC)
                  SDFEXP = SDFEXP*1.D+3
                  SDFMOD = SDFMOD*1.D+3
                  DIFF   = DIFF*1.D+3
                  SDFCAL = SDFCAL*1.D+3
!
!D                  IF( SDFCAL .NE. SDFCALN ) THEN
!D                    IF( SDFCAL .NE. 0.D0 ) THEN
!D                      DSD = ABS((SDFCALN-SDFCAL)/SDFCAL)
!D                    ELSE
!D                      DSD = ABS((SDFCALN-SDFCAL)/SDFCALN)
!D                    ENDIF
!D                    IF( DSD*100.D0 .GE. ISDT ) THEN
!D                      IF( LSDT ) THEN
!D                        WRITE(*,3001) ISDT
!D                        LSDT = .FALSE.
!D                      ENDIF
!D                      WRITE(*,3000) 'P'//CPOL(INPS(ITR))//'mP'//CPOL(INPI(ITR)),  &
!D                                    JINF,SYINF,PAINF,NINF,                        &
!D                                    JSUP,SYSUP,PASUP,NSUP,                        &
!D                                    SDFCALN,SDFCAL,INT(DSD*100.D0)
!D                    ENDIF
!D                  ENDIF
!
                  SDFMOD = SDFMOC
                  IF    ( ISP .EQ. 'M' ) THEN
                    FEXP   = FEXP*CMHZ
                    SDFEXP = SDFEXP*CMHZ
                    SDFMOD = SDFMOD*CMHZ
                    DIFF   = DIFF*CMHZ
                    FCAL   = FCAL*CMHZ
                    SDFCAL = SDFCAL*CMHZ
                  ELSEIF( ISP .EQ. 'G' ) THEN
                    FEXP   = FEXP*CGHZ
                    SDFEXP = SDFEXP*CGHZ
                    SDFMOD = SDFMOD*CGHZ
                    DIFF   = DIFF*CGHZ
                    FCAL   = FCAL*CGHZ
                    SDFCAL = SDFCAL*CGHZ
                  ENDIF
                  DIFF   = MIN(DIFF,  9999.999D0)                                                  ! cut off
                  SDFCAL = MIN(SDFCAL,9999.999D0)                                                  ! cut off
!
! WRITE OUT OUTPUT FILES
!
                  IF( LWRITE ) THEN
                    WRITE(91,2020) FEXP,FASO,ISP//NUO,SDFEXP,SDFMOD,DIFF,FCAL,SDFCAL,  &
                                   JSUP,SYSUP,PASUP,NSUP,                              &
                                   ENSU,NUSVSUA(1),IPCSNA(1),BR,                       &
                                   JINF,SYINF,PAINF,NINF,                              &
                                   JSUP,SYSUP,PASUP,NSUP
                  ENDIF
                ENDIF
              ENDDO E217
!
110           IF( LASSNF ) THEN                                                                    ! assignment not found
                ISP  = 'C'
                NUO  = ''
                FEXP = 0.D0
                FASO = ''
                FCAL = ENSU-ENINFA(NBENIC)
                IF( FCAL .LE. 0.D0 ) CYCLE E17
                SDFMOD = SDFMOC
                SDFEXP = 0.D0
                SDFMOD = SDFMOD*1.D+3
                DIFF   = 9999.999D0
                SDFCAL = SDFCALN
                SDFCAL = MIN(SDFCAL,9999.999D0)
!
! WRITE OUT OUTPUT FILES
!
                IF( LWRITE ) THEN
                  WRITE(91,2020) FEXP,FASO,ISP//NUO,SDFEXP,SDFMOD,DIFF,FCAL,SDFCAL,  &
                                 JSUP,SYSUP,PASUP,NSUP,                              &
                                 ENSU,NUSVSUA(1),IPCSNA(1),BR,                       &
                                 JINF,SYINF,PAINF,NINF,                              &
                                 JSUP,SYSUP,PASUP,NSUP
                ENDIF
              ENDIF
            ENDDO E17
          ENDDO E18
          DO IBS=1,3*NFBSUP+1
            BACKSPACE(50)
          ENDDO
          GOTO 10
!
90        CLOSE(50)
          CLOSE(80)
        ENDDO E2
        CLOSE(91)
      ENDDO E1
      RETURN
!
4040  PRINT 8107, FEM
      GOTO  9999
4050  PRINT 8107, FXEDS
      GOTO  9999
4051  PRINT 8107, FXEDI
      GOTO  9999
4080  PRINT 8113, 'f_prediction_'//DPTYP(IDP)//'.t'
      GOTO  9999
4081  PRINT 8103, FXTRM
      GOTO  9999
4160  PRINT 8105
      GOTO  9999
4161  PRINT 8104
      GOTO  9999
9994  PRINT 8012, FPARAC
      GOTO  9999
9999  PRINT 8000
      PRINT *
      STOP
!
      END SUBROUTINE XWFA
