      PROGRAM JENER
!
!
!  JAN 95
!  MODIFIE 03/98 V. BOUDON ---> XY6/Oh.
!  04/2001 B0,D0,CORR (J=199)
!  MODIFIE 05/09 V. BOUDON, A. EL HILALI ---> XY3Z/C3v.
!
! ############################################
! ##                                        ##
! ##  LECTURE/ECRITURE DES ENERGIES ET DE J ##
! ##                                        ##
! ############################################
!
! APPEL : jener Pn
!
!  ******    LIMITATIONS DU PROGRAMME
!
!     MXSNV      ICENT
!
!
      use mod_dppr
      use mod_par_tds
      use mod_com_fdate
      use mod_com_fp
      use mod_main_jener
      IMPLICIT NONE

      real(kind=dppr)  :: CORR
      real(kind=dppr)  :: ENERG

      integer          :: IC,ICC,IP,ISV
      integer          :: J,JCENT
      integer          :: NFB,NSV,NUSV

      character(len =  10)  :: FOUT  = 'jener.t'
      character(len =  10)  :: FOUXY = 'jener.xy'
      character(len =  11)  :: CARG
      character(len = 120)  :: FED
!
1000  FORMAT(A)
1200  FORMAT(/,                                       &
             '         Energy   J  C     n  #vib',/)
1201  FORMAT(F15.6,1X,I3,I3,3X,2I3,I5,'%')
1210  FORMAT(I3,F15.6)
1020  FORMAT(/,              &
             'JENER  : ',A)
2000  FORMAT(' <  ',A)
2001  FORMAT(' >> ',A)
8000  FORMAT(' !!! JENER  : STOP ON ERROR')
8001  FORMAT(' !!! ERROR OPENING CONTROL FILE')
8002  FORMAT(' !!! INCOMPLETE CONTROL FILE')
!
! ARRAY INITIAL ALLOCATION
!
      CALL ALLOC_ALL
!
      OPEN(10,FILE=CTRLF,ERR=9998,STATUS='OLD')                                                    ! COMMANDES
      READ(10,1000,END=9997) FDATE
      PRINT 1020,            FDATE
      READ(10,1000,END=9997) CARG
      CLOSE(10)
      FED = 'EN_'//TRIM(CARG)//'_'
      PRINT 2000, TRIM(FED)
      PRINT 2001, TRIM(FOUT)
      PRINT 2001, TRIM(FOUXY)
!
!  ***  APPLICATION DES DIRECTIVES
!
      OPEN(50,FILE=FED,FORM='UNFORMATTED',STATUS='OLD')                                            ! ENERGIES
      OPEN(20,FILE=FOUT,STATUS='UNKNOWN')
      OPEN(21,FILE=FOUXY,STATUS='UNKNOWN')
!
!
! *** LECTURE DES PARAMETRES DE H
!
      CALL IOPBA(50,20)
      READ(50) NSV
      DO WHILE( NSV .GT. MXSNV )
        CALL RESIZE_MXSNV
      ENDDO
      DO ISV=1,NSV
        READ(50)
      ENDDO
      READ(50)
!
!
! ***  LECTURE/ECRITURE
!
      WRITE(20,1200)
!
1     READ(50,END=2) J,IC,NFB
      IF( NFB .EQ. 0 ) GOTO 1
      CORR = BB0*J*(J+1)-DD0*J*J*(J+1)*(J+1)
      ICC  = IC
      DO IP=1,NFB
        READ(50) ENERG,(ICENT(ISV),ISV=1,NSV)
        JCENT = 0
        NUSV  = 0
E11:    DO IC=1,NSV
          IF( JCENT .GT. ICENT(IC) ) CYCLE E11
          JCENT = ICENT(IC)
          NUSV  = IC
        ENDDO E11
        WRITE(20,1201) ENERG,J,ICC,IP,NUSV,JCENT
        WRITE(21,1210) J,ENERG-CORR
      ENDDO
      GOTO 1
!
2     CLOSE(20)
      CLOSE(21)
      CLOSE(50)
      GOTO 9000
!
9997  PRINT 8002
      CLOSE(10)
      GOTO  9999
9998  PRINT 8001
9999  PRINT 8000
!
9000  PRINT *
      END PROGRAM JENER
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!     LECTURE DES PARAMETRES
!
      SUBROUTINE IOPBA(LUI,LUO)
      use mod_dppr
      use mod_par_tds
      use mod_com_fp
      IMPLICIT NONE
      integer          :: LUI,LUO

      real(kind=dppr)  :: PARA,PREC

      integer          :: I,IP
      integer          :: NBOPH

      character(len = NBCLAB+10) :: CHAINE
      character(len = NBCTIT)    :: TITRE
!
1000  FORMAT(A)
1001  FORMAT(I4,A)
1002  FORMAT(A,E18.11,E14.7)
8000  FORMAT(' !!! JENER  : STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF IN IOPBA')
!
      DO I=1,3
        READ(LUI,END=2000) TITRE
        IF( LEN_TRIM(TITRE) .EQ. 0 ) THEN
          WRITE(LUO,1000)
        ELSE
          WRITE(LUO,1000)  TRIM(TITRE)
        ENDIF
      ENDDO
      READ (LUI,END=2000) TITRE
      WRITE(LUO,1000)     TRIM(TITRE)
      READ(TITRE,*) VIBNU,B0,A0,BB0,DD0
      READ (LUI,END=2000) NBOPH,TITRE
      WRITE(LUO,1001)     NBOPH,TRIM(TITRE)
      DO I=1,2
        READ(LUI,END=2000) TITRE
        IF( LEN_TRIM(TITRE) .EQ. 0 ) THEN
          WRITE(LUO,1000)
        ELSE
          WRITE(LUO,1000)  TRIM(TITRE)
        ENDIF
      ENDDO
      DO IP=1,NBOPH
        READ (LUI,END=2000) CHAINE,PARA,PREC
        WRITE(LUO,1002)     CHAINE,PARA,PREC
      ENDDO
      RETURN
!
2000  PRINT 8003
      PRINT 8000
      PRINT *
      STOP
!
      END SUBROUTINE IOPBA
