      PROGRAM XPAFIT
!
! 2011 JULY
! V. BOUDON, Ch. WENGER
!
! HAMILTONIAN AND TRANSITION PARAMETER FIT
! USING LEVENBERG-MARQUARDT METHOD
! THANKS TO MINPACK PROJECT IMPLEMENTATION
!
! CALL  : xpafit polyad Pi Nk Dmm'...m'' JPi
!                ...... ....................
!                polyad Pj Nl Dnn'...n'' JPj
!                trans  P*mP* [para_file [Dp [pol [Rxxx]]]]
!                .....  .....
!                trans  P*mP* [para_file [Dp [pol [Rxxx]]]]
!               [adj    P*mP*  para_file     [pol]]
!                .....  .....
!               [adj    P*mP*  para_file     [pol]]
!                end    para_control [mix N] [rmxomc X] [mxiter M] [allcal] [alldip] [allpol]
!
! pol_state :  R111   |   R110   |   R001
!  means    : without | parallel | perpendicular (polarizer)
!
! mix N       = THE N GREATER VIBRATIONAL PERCENTAGES
!               default : 1
! rmxomc X    = MAXIMUM RELATIVE INTENSITY OBS-CAL (REAL)
!               default : 50%
! mxiter M    = MAXIMUM NUMBER OF ITERATIONS
!               default : not user limited (0)
!
!               ONLY ASSIGNED TRANSITIONS ARE WRITTEN IN f_prediction.t
! allcal      = to be used in H only fit case
!               FORCE ALL CALCULATED TRANSITIONS TO BE WRITTEN IN f_prediction_dip.t
! alldip      = FORCE ALL P*mP*      TRANSITIONS TO BE WRITTEN IN f_prediction_dip.t
! allpol      = FORCE ALL P*mP*      TRANSITIONS TO BE WRITTEN IN f_prediction_pol.t
!
! WARNING : 'polyad', 'trans', 'adj' AND 'end' ARE NOT ALLOWED FOR para_file NAME.
!
      use mod_dppr
      use mod_par_tds
      use mod_par_x
      use mod_com_branch
      use mod_com_const
      use mod_com_fdate
      use mod_com_fp
      use mod_com_postf
      use mod_com_sy
      use mod_com_xpoly
      use mod_com_xvari
      use mod_main_xpafit
      IMPLICIT NONE

! functions
      real(kind=dppr)  :: DPMPAR,ENORM,RMSH,RMST
      integer          :: NUNSEL

      real(kind=dppr)  :: COEF0
      real(kind=dppr)  :: FEXP
      real(kind=dppr)  :: PARAC,PREC
      real(kind=dppr)  :: SDFEXP,SDFMOC,SDSEXP,SEXP
      real(kind=dppr)  :: TEMPC,TOL
      real(kind=dppr)  :: VARC

      integer          :: I,IB,ICI,ICS,IDEB,IDELC,IDELTA,IDUM,IFLAG
      integer          :: IFOBZC,IOBS,INFO,INPIC,INPSC,IPOL,IPI
      integer          :: IPS,IRA,IRAC,IRACM1,ISOBZC,ITR,ITRAD,ITRT
      integer          :: IXDEB
      integer          :: J,JINF,JMAXI,JMAXS,JSUP
      integer          :: KA,KTRTC
      integer          :: LDFJAC,LTR
      integer          :: M,MTRUE
      integer          :: NBARH,NBART,JMXH,JMXT
      integer          :: NBFTRC,NBOPTC,NBSTRC,NBXOPH,NBXOPT,NINF
      integer          :: MXITER
      integer          :: N,NBC,NBHPF,NBOPHC,NBXOTT,NCL1,NP,NPC
      integer          :: NSUP,NTRAD

      character(len = NBCLAB+10)  :: COPAJC
      character(len = NBCTIT)     :: TITRE
      character(len =   1)  :: CPOLC,ISP,FASO,SASO,CPSUP,CPINF
      character(len =   1)  :: PAINF,PASUP
      character(len =   2)  :: SYINF,SYSUP
      character(len =   4)  :: POLSTD
      character(len =   5)  :: NUO
      character(len = 120)  :: CARG,FCLX,FEM,FPARAC

      logical          :: ISRAMC,LHPF,LTPF,LHFIT,LTFIT,LACAL
!
      EXTERNAL FCN                                                                                 ! fobs-fcal calculus function
!
1000  FORMAT(A)
1002  FORMAT(A,E18.11,E14.7)
1020  FORMAT(/,              &
             'XPAFIT : ',A)
2000  FORMAT(' <  ',A)
2003  FORMAT(/,                                                               &
             'NB OF OBSERVED DATA    : ',I8                             ,/,   &
             'NB OF DATA SELECTED BEFORE FIT'                           ,/,   &
             'FREQUENCY              : ',I8                             ,/,   &
             'INTENSITY              : ',I8,' (',I8,' PRESELECTED) (',I8,     &
             ' UNSELECTED DUE TO RMXOMC)'                               ,/,   &
             'VIRTUAL    (TYPE 2)    : ',I8                             ,/,   &
             'TOTAL                  : ',I8                             ,/ )
2004  FORMAT(/,                                                     &
             'NB OF FITTABLE HAMILTONIAN OPERATORS : ',I5     ,/,   &
             'NB OF FITTABLE TRANSITION  OPERATORS : ',(100I5)   )
2005  FORMAT('TOTAL NB OF FITTABLE       OPERATORS : ',I5)
2006  FORMAT(/,                                                     &
             'NB OF FITTED   HAMILTONIAN OPERATORS : ',I5     ,/,   &
             'NB OF FITTED   TRANSITION  OPERATORS : ',(100I5)   )
2007  FORMAT('TOTAL NB OF         FITTED OPERATORS : ',I5)
3000  FORMAT(2A,F12.6,1X,A,E11.4,1X,A,F10.6,F6.1,2X,2(I3,1X,2A,I3,1X),   &
             1X,A,2X,A,1X,F7.2,1X,I1,1X,A,E12.4,F9.5                  )
3001  FORMAT(I1)
3002  FORMAT(I2)
3003  FORMAT(I3.3)
3004  FORMAT(////,   &
             I4,//)
3005  FORMAT(////,   &
             I4,A )
3010  FORMAT(I10)
4000  FORMAT(2A,F12.6,1X,A,E11.4,1X,A,F10.6,F6.1,2X,2(I3,1X,2A,I3,1X),   &                         ! read 3000, print 4000
             'P',A,'mP',A,1X,F7.2,1X,I1,1X,A                          )
4001  FORMAT(/,          &
             'INITIAL')
4002  FORMAT('    L2 NORM OF THE RESIDUALS ='   ,D18.11     ,/,   &
             '    Standard Deviation       =',7X,F11.3      ,/,   &
             '    Frequency RMS            =',7X,D11.4,' mk',/,   &
             '    Intensity RMS            ='   ,F18.3,'  %'   )
4003  FORMAT('FINAL')
4004  FORMAT('NUMBER OF FUNCTION EVALUATIONS  ',I10,/,   &
             'NUMBER OF JACOBIAN EVALUATIONS  ',I10,/,   &
             'EXIT PARAMETER                  ',I10   )
8000  FORMAT(' !!! XPAFIT : STOP ON ERROR')
8001  FORMAT(' !!! ERROR OPENING CONTROL FILE')
8002  FORMAT(' !!! UNEXPECTED EOF OF CONTROL FILE')
8003  FORMAT(' !!! TOO MUCH POLYADS'              ,/,   &
             ' !!! MXPOL  EXCEEDED : ',I8,' > ',I8   )
8005  FORMAT(' !!! P',A,' POLYAD NOT FOUND',/,   &
             ' !!! FOR ',A,' TRANSITION'      )
8006  FORMAT(' !!! POLYADS HAVE TO BE DECLARED IN',/,   &
             ' !!! INCREASING NUMBER ORDER'          )
8009  FORMAT(' !!! BAD TAG IN CONTROL FILE'                           ,/,   &
             '     polyad, trans, adj OR end EXPECTED, BUT FOUND : ',A   )
8010  FORMAT(' !!! ERROR OPENING EXPERIMENTAL FILE : ',A)
8011  FORMAT(' !!! ERROR OPENING PARAMETER CONTROL FILE : ',A)
8012  FORMAT(' !!! ERROR OPENING PARAMETER FILE : ',A)
8013  FORMAT(' !!! ERROR READING PARAMETER FILE : ',A)
8014  FORMAT(' !!! UNEXPECTED EOF IN PARAMETER FILE : ',A)
8015  FORMAT(' !!! TAGS MUST BE USED IN FOLLOWING ORDER:'             ,/,   &
             '    polyad Pi Nk Dmm''...m'''' JPi'                     ,/,   &
             '    ...... ....................'                        ,/,   &
             '    polyad Pj Nl Dnn''...n'''' JPj'                     ,/,   &
             '    trans  P*mP* [para_file [Dp [pol [Rxxx]]]]'         ,/,   &
             '    .....  .....'                                       ,/,   &
             '    trans  P*mP* [para_file [Dp [pol [Rxxx]]]]'         ,/,   &
             '   [adj    P*mP*  para_file     [pol]]'                 ,/,   &
             '    .....  .....'                                       ,/,   &
             '   [adj    P*mP*  para_file     [pol]]'                 ,/,   &
             '    end    para_control [mix N] [rmxomc X] [mxiter M]'//      &
             ' [allcal] [alldip] [allpol]'                               )
8016  FORMAT(' !!! NO PARAMETER FILE DEFINED')
8017  FORMAT(' !!! NO TRANSITION DEFINED')
8018  FORMAT(' !!! UNEXPECTED ARGUMENT ON COMMAND LINE : ',A)
8019  FORMAT(' !!! NOT ENOUGH MEANINGFULL DATA BEFORE FIT : ',/,   &
             5X,I8,' DATA'                                   ,/,   &
             5X,I8,' OPERATORS TO FIT'                          )
8020  FORMAT(' !!! NOT ENOUGH MEANINGFULL DATA BEFORE FIT : ',/,   &
             5X,I8,' INTENSITY DATA'                         ,/,   &
             5X,I8,' TRANSITION OPERATORS TO FIT'               )
8022  FORMAT(' WARNING: adj ',A,' : TRANSITION TYPE NOT DEFINED THROUGH ''trans'' TAG',/,   &
             '          ONLY H PARAMETERS WILL BE UPDATED IN ',A                         )
8023  FORMAT(' !!! adj ',A,' : ALREADY DEFINED THROUGH ''trans'' TAG')
8024  FORMAT(' !!! ERROR READING EXPERIMENTAL FILE : ',A)
8025  FORMAT(' !!! ERROR READING #',I2,' TRANSITION DEVELOPMENT ORDER : ',A)
8125  FORMAT(' !!! JMAX TOO LARGE'                ,/,   &
             ' !!! MXJ+2  EXCEEDED : ',I8,' > ',I8   )
8126  FORMAT(' !!! NB OF HAMILTONIAN  OPERATORS  = ',I6)
8127  FORMAT(' !!! NB OF HAMILTONIAN  OP. TO FIT = ',I6,'     NB OF FREQUENCY DATA   TO FIT = ',I6)
8129  FORMAT(' !!! NB OF TRANS. MOMT. OP. TO FIT = ',I6,'     NB OF INTENSITY DATA   TO FIT = ',I6)
8200  FORMAT(' !!! TRANSITIONS HAVE TO BE DECLARED IN'      ,/,   &
             ' !!! LOWER THEN UPPER POLYAD INCREASING ORDER'   )
8201  FORMAT(' !!! DUPLICATED TRANSITIONS')
8202  FORMAT(' WARNING: ASSIGNMENT IGNORED DUE TO MISSING ',A,' TRANSITION')
8203  FORMAT(' WARNING: NO ASSIGNMENT SELECTED FOR ',A,' TRANSITION')
8204  FORMAT(' !!! SAME P*mP* TRANSITIONS MUST HAVE SAME DEVELOPPMENT ORDER')
8205  FORMAT(' !!! INTENSITY DATA ASSIGNED TO ',A,' TRANSITION',/,   &
             '     NEEDS DEVELOPMENT ORDER TO BE SET'             )
8206  FORMAT(' !!! INCONSISTENT NUMBER OF DATA TO FIT',/,   &
             ' !!! FREQUENCY : ',I8,' v/s ',I8        ,/,   &
             ' !!! INTENSITY : ',I8,' v/s ',I8           )
8207  FORMAT(' !!! ',A                              ,/,   &
             ' !!! POLYAD NOT NEEDED BY TRANSITIONS'   )
8208  FORMAT(' !!! INCONSISTENT HAMILTONIAN PARAMETERS :',/,   &
             '     ',A,E18.11,' : ',A                    ,/,   &
             '     ',A,E18.11,' : ',A                       )
8209  FORMAT(' !!! INCONSISTENT TRANSITION  PARAMETERS :',/,   &
             '     ',A,E18.11,' : ',A                    ,/,   &
             '     ',A,E18.11,' : ',A                       )
8211  FORMAT(' !!! FREQUENCY VALUE AND PRECISION MUST BE >0.')
8212  FORMAT(' !!! INTENSITY VALUE AND PRECISION MUST BE >0.')
8213  FORMAT(' !!! TEMPERATURE VALUE MUST BE >0.')
8214  FORMAT(/,                                                                                &
             ' WARNING: FREQUENCY VALUE OR PRECISION (cm-1)'                             ,/,   &
             '  OR/AND  INTENSITY VALUE OR PRECISION        DIFFER FOR SAME ASSIGNMENT :'   )
8215  FORMAT(' !!! ALL PARAMETER FILES MUST BE OF THE SAME TYPE:',/,   &
             '     H or T'                                          )
8216  FORMAT(' !!! A SINGLE H PARAMETER FILE IS REQUIRED')
8218  FORMAT(' !!! ONLY H TYPE TRANSITION(S) AND T OPERATORS TO FIT',/,   &
             '     IN CONTROL FILE : ',A                               )
8219  FORMAT(' !!! T TYPE TRANSITION(S) AND NO T OPERATORS TO FIT',/,   &
             '     IN CONTROL FILE : ',A                             )
8220  FORMAT(' !!! INCONSISTENT POLYAD DEVELOPMENT ORDERS: ',A,2X,A)
8221  FORMAT(' !!! INVALID POLYAD JMAX FOR TRANSITION ',A,': ',2I8)
8222  FORMAT(' !!! NO PARAMETER FILE DEFINED FOR TRANSITION(S)')
8223  FORMAT(' !!! ',A,' MUST BE USED ONLY ONE TIME')
8224  FORMAT(' !!! ERROR OPENING  xpafit_BREAKNOW file')
8225  FORMAT(' !!! ERROR DELETING xpafit_BREAKNOW file')
8226  FORMAT(' !!! ERROR OPENING  xpafit_PARESTIM file')
8227  FORMAT(' !!! ERROR DELETING xpafit_PARESTIM file')
8228  FORMAT(/,                                                        &
             ' WARNING: ',I8,1X,a,' ASSIGNMENTS (UP TO J=',I3,')',/,   &
             '  IGNORED DUE TO JMAX LIMITATION'                     )
!
! ARRAY INITIAL ALLOCATION
!
      CALL ALLOC_ALL
!
      COEF0 = (8.D0*PI*PI*PI*CL)/(CLUM*PLANK)*1.D-36*T0
!
! ***************************************************
! *** GET PROGRAM ARGUMENTS
! ***************************************************
!
      OPEN(10,FILE=CTRLF,ERR=9998,STATUS='OLD')                                                    ! command line arguments
      READ(10,1000,END=9997) FDATE
      PRINT 1020,            FDATE
      NBPOL =  0                                                                                   ! number of polyads
      NP    = -1                                                                                   ! last read polyad #
      NTR   =  0                                                                                   ! number of transitions
      NAD   =  0                                                                                   ! number of extra fpara_adj
!
1     READ(10,1000,END=9997) CARG                                                                  ! read an argument
      IF    ( CARG .EQ. 'polyad' ) THEN
        IF( NTR .NE. 0 ) THEN                                                                      ! no 'polyad' after 'trans'
          PRINT 8015
          GOTO  9999
        ENDIF
        NBPOL = NBPOL+1                                                                            ! one more polyad
        IF( NBPOL .GT. MXPOL ) THEN                                                                ! too much polyads
          PRINT 8003, NBPOL,MXPOL
          GOTO  9999
        ENDIF
        READ(10,1000,END=9997) CARG
        CPOL(NBPOL) = CARG(2:2)                                                                    ! polyad # (char)
        READ(10,1000,END=9997) CARG
        READ(CARG(2:3),3002) NNIV(NBPOL)                                                           ! number of levels of this polyad
        READ(10,1000,END=9997) CARG
        CDEV(NBPOL) = CARG(2:)                                                                     ! development orders (char)
        FEM         = 'HA_P'//CPOL(NBPOL)//'_D'//TRIM(CDEV(NBPOL))//'_'                            ! matrix element file
        PRINT 2000, TRIM(FEM)
        READ(CPOL(NBPOL),3001) NPC                                                                 ! polyad number (int)
        IF( NPC .LE. NP ) THEN                                                                     ! polyads must be defined in increasing order
          PRINT 8006
          GOTO  9999
        ELSE
          NP = NPC                                                                                 ! last read polyad #
        ENDIF
        READ(10,*,END=9997) JMAX(NBPOL)                                                            ! polyad Jmax
        IF( JMAX(NBPOL) .GT. MXJ+2 ) THEN                                                          ! Jmax too big (2 = raman)
          PRINT 8125, JMAX(NBPOL),MXJ+2
          GOTO  9999
        ENDIF
        GOTO 1                                                                                     ! next argument
      ELSEIF( CARG .EQ. 'trans'  ) THEN
        IF( NBPOL .EQ. 0 .OR.         &                                                            ! no 'polyad' defined
            NAD   .NE. 0      ) THEN                                                               ! no 'trans'  after 'adj'
          PRINT 8015
          GOTO  9999
        ENDIF
        NTR = NTR+1                                                                                ! one more transition
        IF( NTR .GT. MXATR ) CALL RESIZE_MXATR
        READ(10,1000,END=9997) CARG
        CPOLC = CARG(2:2)                                                                          ! upper polyad #
        DO I=1,NBPOL                                                                               ! defined polyad ?
          IF( CPOLC .EQ. CPOL(I) ) GOTO 4                                                          ! yes, I -> upper polyad index
        ENDDO
        PRINT 8005, CPOLC,TRIM(CARG)                                                               ! no
        GOTO  9999
!
4       CPOLC = CARG(5:5)                                                                          ! lower polyad #
        DO J=1,NBPOL                                                                               ! defined polyad ?
          IF( CPOLC .EQ. CPOL(J) ) GOTO 21                                                         ! yes, J -> lower polyad index
        ENDDO
        PRINT 8005, CPOLC,TRIM(CARG)                                                               ! no
        GOTO  9999
!
!      ---> IRA = 1 IF INFRARED ; IRA = 0 IF ISOTROPIC RAMAN
21      CDVO(NTR)  = '-'                                                                           ! default = H only, development order only for Ts
        FPARA(NTR) = ''                                                                            ! default = no para_file
        IRAC       = 1                                                                             ! default = infrared
        POLSTD     = 'R111'                                                                        ! default = without polarizer
        READ(10,1000,END=9997) CARG
        IF( CARG .EQ. 'polyad' ) THEN                                                              ! no 'polyad' after 'trans'
          PRINT 8015
          GOTO  9999
        ENDIF
        IF( CARG .EQ. 'trans' .OR.         &
            CARG .EQ. 'adj'   .OR.         &
            CARG .EQ. 'end'        ) THEN
          BACKSPACE(10)                                                                            ! later processed
        ELSE                                                                                       ! more arguments for this 'trans'
          FPARA(NTR) = CARG                                                                        ! para_file
          READ(10,1000,END=9997) CARG
          IF( CARG(1:1) .EQ. 'D' ) THEN                                                            ! development order
            CDVO(NTR) = CARG(2:)
            READ(CDVO(NTR),3010,ERR=9990) N                                                        ! must be integer
            READ(10,1000,END=9997) CARG
            IF( CARG .EQ. 'pol' )  THEN                                                            ! isotropic raman
              IRAC = 0
              READ(10,1000,END=9997) POLSTD                                                        ! polarization state
              IF( POLSTD .NE. 'R111' .AND.         &
                  POLSTD .NE. 'R110' .AND.         &
                  POLSTD .NE. 'R001'       ) THEN                                                  ! CARG was not 'Rxxx'
                POLSTD = 'R111'                                                                    ! default
                BACKSPACE(10)                                                                      ! later processed
              ENDIF
            ELSE                                                                                   ! CARG was not 'pol'
              BACKSPACE(10)                                                                        ! later processed
            ENDIF
          ELSE                                                                                     ! CARG was not 'D...'
            BACKSPACE(10)                                                                          ! later processed
          ENDIF
        ENDIF
        INPS(NTR) = I                                                                              ! upper polyad index
        INPI(NTR) = J                                                                              ! lower polyad index
        IF( IRAC .EQ. 0 ) THEN
          ISRAM(NTR) = .TRUE.                                                                      ! is     raman
        ELSE
          ISRAM(NTR) = .FALSE.                                                                     ! is not raman
        ENDIF
        POLST(NTR) = POLSTD                                                                        ! polarization state
        WRITE(CTR(NTR),3003) NTR                                                                   ! transition number
        GOTO 1                                                                                     ! next argument
      ELSEIF( CARG .EQ. 'adj'    ) THEN                                                            ! extra fpara_adj
        IF( NBPOL .EQ. 0 .OR.         &                                                            ! no 'polyad' defined
            NTR   .EQ. 0      ) THEN                                                               ! no 'trans'  defined
          PRINT 8015
          GOTO  9999
        ENDIF
        NAD = NAD+1                                                                                ! one more fpara_adj
        IF( NAD .GT. MXATR ) CALL RESIZE_MXATR
        READ(10,1000,END=9997) CARG
        CPOLC = CARG(2:2)                                                                          ! upper polyad #
        DO I=1,NBPOL                                                                               ! defined polyad ?
          IF( CPOLC .EQ. CPOL(I) ) GOTO 507                                                        ! yes, I -> upper polyad index
        ENDDO
        PRINT 8005, CPOLC,TRIM(CARG)                                                               ! no
        GOTO  9999
!
507     CPOLC = CARG(5:5)                                                                          ! lower polyad #
        DO J=1,NBPOL                                                                               ! defined polyad ?
          IF( CPOLC .EQ. CPOL(J) ) GOTO 509                                                        ! yes, J -> lower polyad index
        ENDDO
        PRINT 8005, CPOLC,TRIM(CARG)                                                               ! no
        GOTO  9999
!
509     ITRAD = NTR+NAD
        IRAC  = 1                                                                                  ! default = infrared
        READ(10,1000,END=9997) FPARA(ITRAD)
        READ(10,1000,END=9997) CARG
        IF( CARG .EQ. 'pol' )  THEN                                                                ! isotropic raman
          IRAC = 0
        ELSE                                                                                       ! CARG was not 'pol'
          BACKSPACE(10)                                                                            ! later processed
        ENDIF
        INPS(ITRAD) = I                                                                            ! upper polyad index
        INPI(ITRAD) = J                                                                            ! lower polyad index
        IF( IRAC .EQ. 0 ) THEN
          ISRAM(ITRAD) = .TRUE.                                                                    ! is     raman
        ELSE
          ISRAM(ITRAD) = .FALSE.                                                                   ! is not raman
        ENDIF
        GOTO 1
      ELSEIF( CARG .EQ. 'end'    ) THEN                                                            ! last tag
        GOTO 25
      ELSE                                                                                         ! bad tag
        PRINT 8009, CARG
        GOTO  9999
      ENDIF
!
25    IF( NTR .EQ. 0 ) THEN                                                                        ! no 'trans' defined
        PRINT 8017
        GOTO  9999
      ENDIF
      CALL DEBUG( 'XPAFIT => MXATR=',NTR)
! DEFINE EACH POLYAD AS UPPER AND/OR LOWER
      DO IPOL=1,NBPOL
        ISSUP(IPOL) = .FALSE.
        ISINF(IPOL) = .FALSE.
      ENDDO
      DO ITR=1,NTR
        ISSUP(INPS(ITR)) = .TRUE.
        ISINF(INPI(ITR)) = .TRUE.
      ENDDO
! CHECK IF A POLYAD IS NOR UPPER NOR LOWER
E60:  DO IPOL=1,NBPOL
        IF( ISSUP(IPOL) .OR. ISINF(IPOL) ) CYCLE E60
        PRINT 8207, 'P'//CPOL(IPOL)                                                                ! nor lower nor upper polyad
        GOTO  9999
      ENDDO E60
! CHECK POLYAD DEV. ORDER CONSISTENCY
      IF( NBPOL .GT. 1 ) THEN
        DO I=1,NBPOL-1
          DO J=I+1,NBPOL
            NBC = LEN_TRIM(CDEV(I))
            IF( CDEV(I)(:NBC) .NE. CDEV(J)(:NBC) ) THEN
              PRINT 8220, TRIM(CDEV(I)),TRIM(CDEV(J))
              GOTO  9999
            ENDIF
          ENDDO
        ENDDO
      ENDIF
! CHECK POLYAD JMAX CONSISTENCY
E505: DO ITR=1,NTR
        INPSC = INPS(ITR)
        INPIC = INPI(ITR)
        IF( INPSC .EQ. INPIC ) CYCLE E505
        IF( ISRAM(ITR) ) THEN
          IDELTA = 2
        ELSE
          IDELTA = 1
        ENDIF
        JMAXS = JMAX(INPSC)
        JMAXI = JMAX(INPIC)
        IF( JMAXI-JMAXS .LT. IDELTA ) THEN
          PRINT 8221, 'P'//CPOL(INPSC)//'mP'//CPOL(INPIC),JMAXS,JMAXI
          GOTO  9999
        ENDIF
      ENDDO E505
! CHECK TRANSITIONS CONSISTENCY
!
! must be one of H or T type
      NBHPF = 0                                                                                    ! number of   H parameter file
      LHPF  = .FALSE.                                                                              ! there is NO H parameter file
      LTPF  = .FALSE.                                                                              ! there is NO T parameter file
E500: DO ITR=1,NTR
        IF( FPARA(ITR) .EQ. ''  ) CYCLE E500                                                       ! no related parameter file
        IF( CDVO(ITR)  .EQ. '-' ) THEN
          LHPF  = .TRUE.                                                                           ! some H
          NBHPF = NBHPF+1
        ELSE
          LTPF = .TRUE.                                                                            ! some T
        ENDIF
        IF( LHPF .AND. LTPF ) THEN                                                                 ! both, not allowed
          PRINT 8215
          GOTO  9999
        ENDIF
      ENDDO E500
! NOT H AND NOT T
      IF( ( .NOT. LHPF ) .AND.         &
          ( .NOT. LTPF )       ) THEN                                                              ! NO H and NO T
        PRINT 8222
        GOTO  9999
      ENDIF
! H AND MORE THAN 1 FILE
      IF( LHPF         .AND.         &
          NBHPF .NE. 1       ) THEN
        PRINT 8216
        GOTO  9999
      ENDIF
! trans kind restrictions
      IF( NTR .GT. 1 ) THEN                                                                        ! transitions to compare with
! transition increasing order
        DO ITR=2,NTR
          LTR = ITR-1
          IF(  INPI(ITR) .LT. INPI(LTR)        .OR.         &                                      ! increasing order Pinf then Psup required
              (INPI(ITR) .EQ. INPI(LTR) .AND.               &
               INPS(ITR) .LT. INPS(LTR)      )      ) THEN
            PRINT 8200
            GOTO  9999
          ENDIF
        ENDDO
        DO ITR=1,NTR-1                                                                             ! each transition except last one
          DO LTR=ITR+1,NTR                                                                         ! all remaining transitions
            IF( INPS(ITR) .EQ. INPS(LTR) .AND.         &
                INPI(ITR) .EQ. INPI(LTR)       ) THEN                                              ! same P*mP*
              IF( LHPF ) THEN                                                                      ! H and duplicated PimPj
                PRINT 8223, 'P'//CPOL(INPS(ITR))//'mP'//CPOL(INPI(ITR))
                GOTO  9999
              ENDIF
              IF( CDVO(ITR) .NE. CDVO(LTR) ) THEN                                                  ! different CDVO
                PRINT 8204                                                                         ! not allowed
                GOTO  9999
              ENDIF
              IF( ISRAM(ITR) .EQV. ISRAM(LTR) .AND.         &
                  POLST(ITR) .EQ.  POLST(LTR)       ) THEN                                         ! same IRA and same POLST
                PRINT 8201                                                                         ! not allowed
                GOTO  9999
              ENDIF
            ENDIF
          ENDDO
        ENDDO
      ENDIF
! adj kind restrictions
      NTRAD = NTR+NAD
! check if adj transition is not already defined through 'trans' or 'adj'
      DO ITRAD=NTR+1,NTRAD
        DO ITR=1,ITRAD-1
          IF( INPS(ITR)  .EQ.  INPS(ITRAD)  .AND.         &
              INPI(ITR)  .EQ.  INPI(ITRAD)  .AND.         &
              ISRAM(ITR) .EQV. ISRAM(ITRAD) .AND.         &
              FPARA(ITR) .NE.  ''                 ) THEN
            PRINT 8023, 'P'//CPOL(INPS(ITRAD))//'mP'//CPOL(INPI(ITRAD))
            GOTO  9999
          ENDIF
        ENDDO
      ENDDO
! LAST ARGUMENTS
      READ(10,1000,END=9997) FCLX                                                                  ! parameter control file
      LPCMAX =  1                                                                                  ! default
      RMXOMC = 50.D0                                                                               ! default = 50%
      MXITER =  0                                                                                  ! default : not user limited
      LACAL  = .FALSE.                                                                             ! default : write only assigned transitions in f_prediction.t
      LADIP  = .FALSE.                                                                             ! default : write only .NOT. ISRAM(ITR)
      LAPOL  = .FALSE.                                                                             ! default : write only       ISRAM(ITR)
!
19    READ(10,1000,END=22) CARG
      IF    ( CARG .EQ. 'mix'    ) THEN
        READ(10,*,END=9997) LPCMAX
      ELSEIF( CARG .EQ. 'rmxomc' ) THEN
        READ(10,*,END=9997) RMXOMC
      ELSEIF( CARG .EQ. 'mxiter' ) THEN
        READ(10,*,END=9997) MXITER
      ELSEIF( CARG .EQ. 'allcal' ) THEN
        LACAL = .TRUE.
      ELSEIF( CARG .EQ. 'alldip' ) THEN
        LADIP = .TRUE.
      ELSEIF( CARG .EQ. 'allpol' ) THEN
        LAPOL = .TRUE.
      ELSE                                                                                         ! unexpected argument
        PRINT 8018, CARG
        GOTO  9999
      ENDIF
      GOTO 19                                                                                      ! next argument
!
22    CLOSE(10)
!
! READ PARAMETER FILES
!
! SET NBOHT(0) (H) AND THE RELATED PARAMETER FILE
! 1ST FILE WITH THE GREATER NBOHC
!
      NBOHT(0) = 0                                                                                 ! nb of hamiltonian operators
      ITRHT(0) = 0                                                                                 ! index of related transition
E350: DO ITR=1,NTR                                                                                 ! for each transition
        FPARAC = FPARA(ITR)
        IF( FPARAC .EQ. '' ) CYCLE E350                                                            ! no related parameter file
        OPEN(70,ERR=9994,FILE=TRIM(FPARAC),STATUS='OLD')
        READ(70,3004,ERR=9993) NBOPHC
        IF( NBOPHC .GT. NBOHT(0) ) THEN                                                            ! greater NBOPHC file
          NBOHT(0) = NBOPHC
          ITRHT(0) = ITR                                                                           ! the transition referencing H
        ENDIF
        CLOSE(70)
      ENDDO E350
      IF( NBOHT(0) .EQ. 0 ) THEN                                                                   ! parameter file not found
        PRINT 8016
        GOTO  9999
      ENDIF
! READ H PARAMETERS
      FPARAC = FPARA(ITRHT(0))                                                                     ! related parameter file
      PRINT 2000,           TRIM(FPARAC)
      OPEN(70,ERR=9994,FILE=TRIM(FPARAC),STATUS='OLD')
      CALL IOPH(70)                                                                                ! read H parameters
      CLOSE(70)
! CHECK H PARAMETERS CONSISTENCY FOR ALL PARAMETER FILES
      NCL1 = NBCLAB+1+NBAM+1                                                                       ! Hmn  included
E351: DO ITR=1,NTRAD
        IF( ITR .EQ. ITRHT(0) ) CYCLE E351                                                         ! no self test
        FPARAC = FPARA(ITR)                                                                        ! current parameter file
        IF( FPARAC .EQ. '' ) CYCLE E351                                                            ! no parameter file
        OPEN(70,ERR=9994,FILE=TRIM(FPARAC),STATUS='OLD')
        READ(70,3004,ERR=9993) NBOPHC                                                              ! current nb of H operators
        DO I=1,NBOPHC                                                                              ! for each operator
          READ(70,1002,END=9992) COPAJC,PARAC,PREC
! COPAJ (OPERATOR DEFINITION) AND PARAO (ORIGINAL PARAMETER VALUE) SET IN IOPH
          IF( COPAJC(:NCL1) .NE. COPAJ(I)(:NCL1) .OR.         &                                    ! test definition
              PARAC         .NE. PARAO(I)             ) THEN                                       ! and  value
            PRINT 8208, COPAJC,PARAC,          &                                                   ! not consistent
                        TRIM(FPARAC),          &
                        COPAJ(I),PARAO(I),     &
                        TRIM(FPARA(ITRHT(0)))
            GOTO  9999
          ENDIF
        ENDDO
        CLOSE(70)
      ENDDO E351
! READ PNEG,IMEGA FOR EACH TRANSITION
      DO ITR=1,NTR
        FPARAC = FPARA(ITR)
        IF( FPARAC .EQ. '' ) FPARAC = FPARA(ITRHT(0))                                              ! default = H parameter file
        OPEN(70,ERR=9994,FILE=TRIM(FPARAC),STATUS='OLD')
        READ(70,3005,ERR=9993) IDUM,TITRE
        READ(TITRE,*,ERR=9993) PNEG(ITR),IMEGA(ITR)
        CLOSE(70)
      ENDDO
! SET TYPE OF EACH TRANSITION
! A TYPE IS UNIQUE AND DEPENDS ON DELTA AND ISRAM
! DELTA = i-j FOR PimPj TRANSITION
! ISRAM = RAMAN (0) OR NOT (1)
!
! TYPE IS 0 FOR H, NOT NULL (1,NTRT) FOR Ts
      DO ITR=1,NTR                                                                                 ! for each transition
        IF( CDVO(ITR) .EQ. '-' ) THEN                                                              ! means H
          KTRT(ITR)  = -1                                                                          ! dummy type
          INTRT(ITR) =  0                                                                          ! dummy index of transition type
        ELSE
          READ(CPOL(INPS(ITR)),3001) IPS                                                           ! T case
          READ(CPOL(INPI(ITR)),3001) IPI
          IDELC = IPS-IPI                                                                          ! current DELTA (0,MXPOL-1)
          IF( ISRAM(ITR) ) THEN                                                                    ! set current IRA (0,1)
            IRAC = 0
          ELSE
            IRAC = 1
          ENDIF
          KTRT(ITR) = IDELC*10+IRAC                                                                ! type code (0->(MXPOL-1)*10+1)
        ENDIF
      ENDDO
! SET THE NUMBER OF DIFFERENT TYPES
! AND SET THE UNIQUE TYPE # FOR EACH TRANSITION
      NTRT = 0                                                                                     ! nb of transition types
      DO IDELTA=0,MXPOL-1                                                                          ! for all DELTA
E355:   DO IRA=0,1                                                                                 ! for all ISRAM (0 if ISRAM=TRUE)
          KTRTC = IDELTA*10+IRA                                                                    ! type code
          DO ITR=1,NTR                                                                             ! for each transition
            IF( KTRTC .EQ. KTRT(ITR) ) THEN                                                        ! new type
              NTRT = NTRT+1                                                                        ! nb of unique types
              IF( NTRT .GT. MXATRT ) CALL RESIZE_MXATRT
              DO LTR=1,NTR
                IF( KTRTC .EQ. KTRT(LTR) ) THEN
                  INTRT(LTR) = NTRT                                                                ! index of transition type of this transition
                ENDIF
              ENDDO
              CYCLE E355                                                                           ! next possible type
            ENDIF
          ENDDO
        ENDDO E355
      ENDDO
      CALL DEBUG( 'XPAFIT => MXATRT=',NTRT)
!
! SET INTRT FOR fpara_adj
E510: DO ITRAD=NTR+1,NTRAD
        INTRT(ITRAD) = 0
        IF( LTPF ) THEN
          READ(CPOL(INPS(ITRAD)),3001) IPS                                                         ! T case
          READ(CPOL(INPI(ITRAD)),3001) IPI
          IDELC = IPS-IPI                                                                          ! current DELTA (0,MXPOL-1)
          IF( ISRAM(ITRAD) ) THEN                                                                  ! set current IRA (0,1)
            IRAC = 0
          ELSE
            IRAC = 1
          ENDIF
          KTRTC = IDELC*10+IRAC                                                                    ! type code (0->(MXPOL-1)*10+1)
          DO ITR=1,NTR
            IF( KTRTC .EQ. KTRT(ITR) ) GOTO 512
          ENDDO
          PRINT 8022, 'P'//CPOL(INPS(ITRAD))//'mP'//CPOL(INPI(ITRAD)),  &
                      TRIM(FPARA(ITRAD))//'_adj'
          CYCLE E510
!
512       INTRT(ITRAD) = INTRT(ITR)
        ENDIF
      ENDDO E510
!
! SET NBOHT() FOR EACH (T) TYPE
      DO ITRT=1,NTRT                                                                               ! Ts
        NBOHT(ITRT) = 0
        ITRHT(ITRT) = 0
E359:   DO ITR=1,NTR                                                                               ! for each transition
          IF( ITRT .NE. INTRT(ITR) ) CYCLE E359                                                    ! not of this type
          FPARAC = FPARA(ITR)                                                                      ! para_file necesseraly present (T)
          OPEN(70,ERR=9994,FILE=TRIM(FPARAC),STATUS='OLD')
          READ(70,3004,ERR=9993) NBOPHC                                                            ! current nb of H operators
          DO I=1,NBOPHC                                                                            ! skip  H operators
            READ(70,1002,END=9992)
          ENDDO
          READ(70,3004,ERR=9993) NBOPTC                                                            ! current nb of T operators
          CLOSE(70)
          IF( NBOPTC .GT. NBOHT(ITRT) ) THEN
            NBOHT(ITRT) = NBOPTC                                                                   ! update nb of T operators for this type
            ITRHT(ITRT) = ITR                                                                      ! the transition related to this type
          ENDIF
        ENDDO E359
      ENDDO
! READ Ts PARAMETERS
      NCL1 = NBCLAB+1+NBAM+1                                                                       ! Hmn  included
      DO ITRT=1,NTRT                                                                               ! Ts
        FPARAC = FPARA(ITRHT(ITRT))
        PRINT 2000, TRIM(FPARAC)
        OPEN(70,ERR=9994,FILE=TRIM(FPARAC),STATUS='OLD')
        CALL IOPT(70,ITRT)                                                                         ! read T parameters
        CLOSE(70)
! CHECK T CONSISTENCY FOR ALL TRANSITION PARAMETER FILES OF THIS TYPE
        IDEB = INPHT(ITRT)                                                                         ! INPHT set in IOPT
E362:   DO ITR=1,NTRAD                                                                             ! for each transition
          IF( ITR  .EQ. ITRHT(ITRT) .OR.               &                                           ! no self test
              ITRT .NE. INTRT(ITR)       ) CYCLE E362                                              ! different type
          FPARAC = FPARA(ITR)
          OPEN(70,ERR=9994,FILE=TRIM(FPARAC),STATUS='OLD')
          READ(70,3004,ERR=9993) NBOPHC                                                            ! current nb of H operators
          DO I=1,NBOPHC                                                                            ! skip  H operators
            READ(70,1000,END=9992)
          ENDDO
          READ(70,3004,ERR=9993) NBOPTC                                                            ! current nb of T operators
          DO I=IDEB+1,IDEB+NBOPTC                                                                  ! for each operator
            READ(70,1002,END=9992) COPAJC,PARAC,PREC
! COPAJ (OPERATOR DEFINITION) AND PARAO (ORIGINAL PARAMETER VALUE) SET IN IOPT
            IF( COPAJC(:NCL1) .NE. COPAJ(I)(:NCL1) .OR.         &                                  ! test definition
                PARAC         .NE. PARAO(I)             ) THEN                                     ! and  value
              PRINT 8209, COPAJC,PARAC,             &                                              ! not consistent
                          TRIM(FPARAC),             &
                          COPAJ(I),PARAO(I),        &
                          TRIM(FPARA(ITRHT(ITRT)))
              GOTO  9999
            ENDIF
          ENDDO
          CLOSE(70)
        ENDDO E362
      ENDDO
! TOTAL NUMBER OF OPERATORS (H+Ts)
      NBOP = 0
      DO ITRT=0,NTRT                                                                               ! H+Ts
        NBOP = NBOP+NBOHT(ITRT)
      ENDDO
!
! READ PARAMETER CONTROL FILE
!
      PRINT 2000, TRIM(FCLX)
      OPEN(12,ERR=9995,FILE=TRIM(FCLX),STATUS='OLD')
      CALL READCH                                                                                  ! hamiltonian       parameters
      DO ITRT=1,NTRT                                                                               ! Ts
        CALL READCT(ITRT)                                                                          ! transition moment parameters
      ENDDO
      CLOSE(12)
!
! TAKE CONTRIBUTIONS INTO ACCOUNT
! AND INITIALIZE PARAR (REAL ACTUAL PARAMETER / FUNCTION             IN FCN)
!            AND PARA  (CALCULUS    PARAMETER / FUNCTION OR JACOBIAN IN FCN)
!
      DO I=1,NBOHT(0)                                                                              ! H
        IF( ICNTRL(I) .EQ. 5 ) THEN                                                                ! 5 -> contribution
          PARAC = 0.D0
          DO J=1,NBOHT(0)
! CTRIBH (CONTRIBUTION FACTORS) SET IN READCH
            IF( CTRIBH(I,J) .NE. 0.D0 ) PARAC = PARAC+CTRIBH(I,J)*PARAR(J)                         ! add contribution
          ENDDO
          PARAR(I) = PARAC                                                                         ! set real parameter value
        ENDIF
      ENDDO
      DO ITRT=1,NTRT                                                                               ! Ts
        IDEB = INPHT(ITRT)
        DO I=IDEB+1,IDEB+NBOHT(ITRT)
          IF( ICNTRL(I) .EQ. 5 ) THEN                                                              ! 5 -> contribution
            PARAC = 0.D0
            IB    = I-IDEB                                                                         ! for CTRIBT
            DO J=1,NBOHT(ITRT)
! CTRIBT (CONTRIBUTION FACTORS) SET IN READCT
              IF( CTRIBT(ITRT,IB,J) .NE. 0.D0 ) THEN
                PARAC = PARAC+CTRIBT(ITRT,IB,J)*PARAR(IDEB+J)                                      ! add contribution
              ENDIF
            ENDDO
            PARAR(I) = PARAC                                                                       ! set real parameter value
          ENDIF
        ENDDO
      ENDDO
      DO I=1,NBOP                                                                                  ! for each operator
        PARA(I) = PARAR(I)                                                                         ! initialization of calculus used parameter
      ENDDO
!
! NUMBER OF PARAMETERS TO FIT FOR EACH TYPE (H+Ts)
!
      NBXOP = 0                                                                                    ! total number of fitted parameters
      IXDEB = 0                                                                                    ! starting current index
      DO ITRT=0,NTRT                                                                               ! H+Ts
        INXPHT(ITRT) = IXDEB                                                                       ! starting index (-1) (fitted parameters)
        NBXOHT(ITRT) = 0                                                                           ! number of fitted parameters of this type
        IDEB         = INPHT(ITRT)                                                                 ! starting index (-1) (all    parameters)
        DO I=IDEB+1,IDEB+NBOHT(ITRT)                                                               ! for each parameter of this type
          IF( ICNTRL(I) .EQ. 2 .OR.         &
              ICNTRL(I) .EQ. 4      ) THEN                                                         ! fitted
            NBXOP                            = NBXOP+1                                             ! total number of fitted parameters
            IXDEB                            = IXDEB+1                                             ! current index
            NBXOHT(ITRT)                     = NBXOHT(ITRT)+1                                      ! one more parameter to fit
            XPARA(INXPHT(ITRT)+NBXOHT(ITRT)) = PARAR(I)                                            ! solution vector (fitted parameters)
          ENDIF
        ENDDO
      ENDDO
      NBXOPH = NBXOHT(0)                                                                           ! nb of H parameter(s) to fit
      NBXOPT = NBXOP-NBXOPH                                                                        ! nb of T parameter(s) to fit
      IF( LHPF          .AND.         &                                                            ! H transition(s) only but
          NBXOPT .NE. 0       ) THEN                                                               ! T parameter(s) to fit
        PRINT 8218, TRIM(FCLX)
        GOTO  9999
      ENDIF
      IF( LTPF          .AND.         &                                                            ! T transition(s) but
          NBXOPT .EQ. 0       ) THEN                                                               ! NO T parameter(s) to fit
        PRINT 8219, TRIM(FCLX)
        GOTO  9999
      ENDIF
      IF( NBXOPH .EQ. 0 ) THEN
        LHFIT = .FALSE.                                                                            ! NO H parameter(s) to fit
      ELSE
        LHFIT = .TRUE.                                                                             !    H parameter(s) to fit
      ENDIF
      IF( NBXOPT .EQ. 0 ) THEN
        LTFIT = .FALSE.                                                                            ! NO T parameter(s) to fit
      ELSE
        LTFIT = .TRUE.                                                                             !    T parameter(s) to fit
      ENDIF
!
! READ ASSIGNMENT FILE
!
      PRINT 2000, FASSI
      OPEN(90,ERR=9996,FILE=FASSI,STATUS='OLD')
!
! SET THE NUMBER OF FREQUENCY DATA TO FIT
!
      NBFOBZ = 0                                                                                   ! nb of frequency data to fit
      IF( LHFIT ) THEN                                                                             ! some H parameter to fit
!
325     READ(90,3000,ERR=9991,END=326) ISP,NUO,FEXP,FASO,SEXP,SASO,SDFEXP,SDSEXP,  &               ! read observed data
                                       JINF,SYINF,PAINF,NINF,                      &
                                       JSUP,SYSUP,PASUP,NSUP,                      &
                                       CPSUP,CPINF,TEMPC,IRAC,POLSTD,FPVIB,ABUND
! JINF OR JSUP TOO BIG
        DO I=1,NBPOL                                                                               ! for each polyad; cf. E31
          IF( CPINF .EQ. CPOL(I) .AND.             &
              JINF  .GT. JMAX(I)       ) GOTO 325                                                  ! JINF too big
          IF( CPSUP .EQ. CPOL(I) .AND.             &
              JSUP  .GT. JMAX(I)       ) GOTO 325                                                  ! JSUP too big
        ENDDO
        IF( FASO .EQ. '+' ) NBFOBZ = NBFOBZ+1                                                      ! freq fit implied
        GOTO 325                                                                                   ! read next observed data line
!
326     REWIND(90)                                                                                 ! eof
      ENDIF
!
! READ OBSERVED DATA
!
      IOBS   = 0                                                                                   ! nb of observed  data
      IFOBZC = 0                                                                                   ! current frequency index
      ISOBZC = NBFOBZ                                                                              ! current intensity (strength) index
      DO WHILE( ISOBZC .GT. MXOBZ )                                                                ! ISOBZC and NBFOBZ
        CALL RESIZE_MXOBS                                                                          ! MXOBS -> MXOBZ
      ENDDO
      NBARH  = 0                                                                                   ! nb of frequency assignments ignored due to JMAX limitation
      JMXH   = 0                                                                                   ! corresponding J max value
      NBART  = 0                                                                                   ! nb of intensity assignments ignored due to JMAX limitation
      JMXT   = 0                                                                                   ! corresponding J max value
!
5     READ(90,3000,ERR=9991,END=324) ISP,NUO,FEXP,FASO,SEXP,SASO,SDFEXP,SDSEXP,  &                 ! read observed data
                                     JINF,SYINF,PAINF,NINF,                      &
                                     JSUP,SYSUP,PASUP,NSUP,                      &
                                     CPSUP,CPINF,TEMPC,IRAC,POLSTD,FPVIB,ABUND
! JINF OR JSUP TOO BIG
E31:  DO I=1,NBPOL
        IF( (CPINF .EQ. CPOL(I) .AND.               &
             JINF  .GT. JMAX(I)      ) .OR.         &                                              ! JINF too big
            (CPSUP .EQ. CPOL(I) .AND.               &
             JSUP  .GT. JMAX(I)      )      ) THEN                                                 ! JSUP too big
          IF( LHFIT         .AND.         &
              FASO .EQ. '+'       ) THEN                                                           ! frequency assignment ignored due to JMAX limitation
            NBARH = NBARH+1
!     write(*,3000)                  ISP,NUO,FEXP,FASO,SEXP,SASO,SDFEXP,SDSEXP,  &
!                                    JINF,SYINF,PAINF,NINF,                      &
!                                    JSUP,SYSUP,PASUP,NSUP,                      &
!                                    CPSUP,CPINF,TEMPC,IRAC,POLSTD,FPVIB,ABUND
            JMXH = MAX(JMXH,MAX(JINF,JSUP))
          ENDIF
          IF( LTFIT         .AND.         &
              SASO .EQ. '+'       ) THEN                                                           ! intensity assignment ignored due to JMAX limitation
            NBART = NBART+1
!     write(*,3000)                  ISP,NUO,FEXP,FASO,SEXP,SASO,SDFEXP,SDSEXP,  &
!                                    JINF,SYINF,PAINF,NINF,                      &
!                                    JSUP,SYSUP,PASUP,NSUP,                      &
!                                    CPSUP,CPINF,TEMPC,IRAC,POLSTD,FPVIB,ABUND
            JMXT = MAX(JMXT,MAX(JINF,JSUP))
          ENDIF
          GOTO 5
        ENDIF
      ENDDO E31
!
! VARIOUS CHECKS
!
      IF( IRAC .EQ. 0 ) THEN                                                                       ! isotropic raman
        ISRAMC = .TRUE.
      ELSE
        ISRAMC = .FALSE.
      ENDIF
! SET THE TRANSITION OF THIS ASSIGNMENT
      DO ITR=1,NTR
        IF( (CPSUP  .EQ.  CPOL(INPS(ITR))) .AND.            &
            (CPINF  .EQ.  CPOL(INPI(ITR))) .AND.            &
            (ISRAMC .EQV. ISRAM(ITR)     ) .AND.            &
            (POLSTD .EQ.  POLST(ITR)     )       ) GOTO 54
      ENDDO
! TRANSITION NOT FOUND
      IF( (LHFIT         .AND.               &
           FASO .EQ. '+'      ) .OR.         &
          (LTFIT         .AND.               &
           SASO .EQ. '+'      )      ) THEN
        IF( ISRAMC ) THEN
          PRINT 8202, 'P'//CPSUP//'mP'//CPINF//' pol '//POLSTD
        ELSE
          PRINT 8202, 'P'//CPSUP//'mP'//CPINF
        ENDIF
      ENDIF
      GOTO 5
!
54    IF( (FASO               .NE. '+' .OR.                  &
           .NOT. LHFIT                     ) .AND.           &
          (SASO               .NE. '+' .OR.                  &
           .NOT. LTFIT                 .OR.                  &
           NBXOHT(INTRT(ITR)) .EQ.  0      )       ) GOTO 5                                        ! nor frequency nor intensity data to fit
! FIT FREQUENCY BUT NEGATIVE FREQUENCY OR STANDARD DEVIATION
      IF(  FASO   .EQ. '+'         .AND.         &
           LHFIT                   .AND.         &
          (FEXP   .LE.  0.D0 .OR.                &
           SDFEXP .LE.  0.D0     )       ) THEN
        PRINT 8211
        PRINT 4000, ISP,NUO,FEXP,FASO,SEXP,SASO,SDFEXP,SDSEXP,  &
                    JINF,SYINF,PAINF,NINF,                      &
                    JSUP,SYSUP,PASUP,NSUP,                      &
                    CPSUP,CPINF,TEMPC,IRAC,POLSTD
        GOTO  9999
      ENDIF
! FIT INTENSITY BUT NEGATIVE INTENSITY OR STANDARD DEVIATION
      IF(  SASO               .EQ. '+' .AND.         &
           LTFIT                       .AND.         &
           NBXOHT(INTRT(ITR)) .NE.  0  .AND.         &
          (SEXP   .LE.  0.D0 .OR.                    &
           SDSEXP .LE.  0.D0     )           ) THEN
        PRINT 8212
        PRINT 4000, ISP,NUO,FEXP,FASO,SEXP,SASO,SDFEXP,SDSEXP,  &
                    JINF,SYINF,PAINF,NINF,                      &
                    JSUP,SYSUP,PASUP,NSUP,                      &
                    CPSUP,CPINF,TEMPC,IRAC,POLSTD
        GOTO  9999
      ENDIF
! FIT INTENSITY BUT NEGATIVE TEMPERATURE
      IF( SASO               .EQ. '+'   .AND.         &
          LTFIT                         .AND.         &
          NBXOHT(INTRT(ITR)) .NE.  0    .AND.         &
          TEMPC              .LE.  0.D0       ) THEN
        PRINT 8213
        PRINT 4000, ISP,NUO,FEXP,FASO,SEXP,SASO,SDFEXP,SDSEXP,  &
                    JINF,SYINF,PAINF,NINF,                      &
                    JSUP,SYSUP,PASUP,NSUP,                      &
                    CPSUP,CPINF,TEMPC,IRAC,POLSTD
        GOTO  9999
      ENDIF
      IOBS = IOBS+1
      IF( IOBS .GT. MXOBS ) CALL RESIZE_MXOBS
! APPLY UNITS
! ISP (FREQUENCY UNIT) SET IN XASG0
      IF    ( ISP .EQ. 'M' ) THEN                                                                  ! MHz
        FEXP   = FEXP/CMHZ
        SDFEXP = SDFEXP/CMHZ
      ELSEIF( ISP .EQ. 'G' ) THEN                                                                  ! GHz
        FEXP   = FEXP/CGHZ
        SDFEXP = SDFEXP/CGHZ
      ENDIF
      ISPT(IOBS)   = ISP                                                                           ! frequency unit
      NUOT(IOBS)   = NUO                                                                           ! #
      FOBS(IOBS)   = FEXP                                                                          ! observed frequency
      FASOT(IOBS)  = FASO                                                                          ! frequency status (+/-/space)
      SOBS(IOBS)   = SEXP/ABUND                                                                    ! observed intensity
      SASOT(IOBS)  = SASO                                                                          ! intensity status (+/-/space)
      SDFOBS(IOBS) = SDFEXP                                                                        ! frequency standard deviation
      SDSOBS(IOBS) = SDSEXP                                                                        ! intensity standard deviation
      JIOBS(IOBS)  = JINF                                                                          ! lower J
      DO ICI=1,MXSYM                                                                               ! set ICI
        IF( SYINF .EQ. SYM(ICI) ) GOTO 7
      ENDDO
!
7     ICIOBS(IOBS) = ICI                                                                           ! lower C
      NIOBS(IOBS)  = NINF                                                                          ! lower N
      JSOBS(IOBS)  = JSUP                                                                          ! upper J
      DO ICS=1,MXSYM                                                                               ! set ICS
        IF( SYSUP .EQ. SYM(ICS) ) GOTO 9
      ENDDO
!
9     ICSOBS(IOBS) = ICS                                                                           ! upper C
      NSOBS(IOBS)  = NSUP                                                                          ! upper N
      CPOLS(IOBS)  = CPSUP                                                                         ! upper polyad number
      CPOLI(IOBS)  = CPINF                                                                         ! lower polyad number
      JTR(IOBS)    = ITR                                                                           ! related transition
! CHECKS
      IF( IOBS .GT. 1 ) THEN
        IF( JINF .EQ. JIOBS(IOBS-1)  .AND.         &
            ICI  .EQ. ICIOBS(IOBS-1) .AND.         &
            NINF .EQ. NIOBS(IOBS-1)  .AND.         &
            JSUP .EQ. JSOBS(IOBS-1)  .AND.         &
            ICS  .EQ. ICSOBS(IOBS-1) .AND.         &
            NSUP .EQ. NSOBS(IOBS-1)        ) THEN                                                  ! same assignment
          IF( (LHFIT                             .AND.               &
               FASO          .EQ. '+'            .AND.               &
               FASOT(IOBS-1) .EQ. '+'            .AND.               &
               (FEXP   .NE. FOBS(IOBS-1)   .OR.                      &
                SDFEXP .NE. SDFOBS(IOBS-1)     )      ) .OR.         &
              (LTFIT                             .AND.               &
               SASO          .EQ. '+'            .AND.               &
               SASOT(IOBS-1) .EQ. '+'            .AND.               &
               (SEXP   .NE. SOBS(IOBS-1)   .OR.                      &
                SDSEXP .NE. SDSOBS(IOBS-1)     )      )      ) THEN
! WARNING : SAME ASSIGNMENT BUT H FIT AND FREQUENCY OR PRECISION DIFFER
!                           OR  T FIT AND INTENSIY  OR PRECISION DIFFER
            PRINT 8214
            IF( ISRAM(JTR(IOBS-1)) ) THEN
              IRACM1 = 0
            ELSE
              IRACM1 = 1
            ENDIF
            PRINT 4000, ISPT(IOBS-1),NUOT(IOBS-1),              &
                        FOBS(IOBS-1),FASOT(IOBS-1),             &
                        SOBS(IOBS-1),SASOT(IOBS-1),             &
                        SDFOBS(IOBS-1),SDSOBS(IOBS-1),          &
                        JIOBS(IOBS-1),SYM(ICIOBS(IOBS-1)),      &
                        PARGEN,NIOBS(IOBS-1),                   &
                        JSOBS(IOBS-1),SYM(ICSOBS(IOBS-1)),      &
                        PARGEN,NSOBS(IOBS-1),                   &
                        CPOLS(IOBS-1),CPOLI(IOBS-1),            &
                        TEMP(IOBS-1),IRACM1,POLST(JTR(IOBS-1))
            PRINT 4000, ISP,NUO,FEXP,FASO,SEXP,SASO,SDFEXP,SDSEXP,  &
                        JINF,SYINF,PAINF,NINF,                      &
                        JSUP,SYSUP,PASUP,NSUP,                      &
                        CPSUP,CPINF,TEMPC,IRAC,POLSTD
          ENDIF
        ENDIF
      ENDIF
!
! STORE THE DATA VECTOR : FREQUENCIES THEN INTENSITIES
!
      IF( FASO  .EQ. '+' .AND.         &
          LHFIT                ) THEN                                                              ! frequency to fit
        IFOBZC = IFOBZC+1                                                                          ! frequency index
        IF( IFOBZC .GT. MXOBZ ) CALL RESIZE_MXOBS                                                  ! MXOBS -> MXOBZ
        SDZEXP(IFOBZC) = SDFEXP                                                                    ! absolute standard deviation
        SDFMOC         = 1.D0                                                                      ! standard deviation due to the model
        IF( IMEGA(ITR) .NE. 0 ) SDFMOC = DBLE(JSUP)**IMEGA(ITR)
        SDFMOC         = PNEG(ITR)*SDFMOC
        ZWGT(IFOBZC)   = 1.D0/SQRT((SDFEXP*SDFEXP+SDFMOC*SDFMOC))                                  ! absolute weight
        INDOBS(IFOBZC) = IOBS                                                                      ! index of the observed data
        IFOBZ(IOBS)    = IFOBZC                                                                    ! index of the fitted frequency of this observed data
      ELSE
        IFOBZ(IOBS) = 0                                                                            ! no fitted frequency for this observed data
      ENDIF
      IF( SASO               .EQ. '+' .AND.         &
          LTFIT                       .AND.         &
          NBXOHT(INTRT(ITR)) .NE.  0        ) THEN                                                 ! intensity to fit
        IF( CDVO(ITR) .EQ. '-' ) THEN                                                              ! development order not defined
          PRINT 8205, 'P'//CPOL(INPS(ITR))//'mP'//CPOL(INPI(ITR))
          GOTO  9999
        ENDIF
        ISOBZC = ISOBZC+1                                                                          ! intensity index
        IF( ISOBZC .GT. MXOBZ ) CALL RESIZE_MXOBS                                                  ! MXOBS -> MXOBZ
        SDZEXP(ISOBZC) = SEXP*SDSEXP/100.D0                                                        ! absolute standard deviation (SDSEXP is relative)
        ZWGT(ISOBZC)   = 1.D0/SDZEXP(ISOBZC)                                                       ! absolute weight
        INDOBS(ISOBZC) = IOBS                                                                      ! index of the observed data
        ISOBZ(IOBS)    = ISOBZC                                                                    ! index of the fitted intensity of this observed data
        TEMP(IOBS)     = TEMPC                                                                     ! temperature of this observed data
        COEF(IOBS)     = COEF0/TEMPC/FPART(TEMPC,TEMPC)                                            ! COEF is used for theoretical intensity calculus
      ELSE
        ISOBZ(IOBS) = 0                                                                            ! no fitted intensity for this observed data
      ENDIF
      GOTO 5                                                                                       ! read next observed data
!
324   CLOSE(90)                                                                                    ! eof
      CALL DEBUG( 'XPAFIT => MXOBS=',IOBS)
      IF( NBARH .NE. 0 ) PRINT 8228, NBARH,'FREQUENCY',JMXH
      IF( NBART .NE. 0 ) PRINT 8228, NBART,'INTENSITY',JMXT
      NBOBS  = IOBS                                                                                ! number of selected observed data
      NBSOBZ = ISOBZC-NBFOBZ                                                                       ! number of intensities to fit
      NBROBZ = NBFOBZ+NBSOBZ                                                                       ! number of frequencies+intensities to fit
!
! ADD VIRTUAL OBZ FOR ICNTRL=2 PARAMETERS
!
      NBOBZ = NBROBZ
      DO J=1,NBOP                                                                                  ! for each operator
        IF( ICNTRL(J) .EQ. 2 ) THEN                                                                ! bound to a fixed value
          NBOBZ         = NBOBZ+1                                                                  ! a new OBZ
          SDZEXP(NBOBZ) = PRAT(J)                                                                  ! absolute standard deviation
          ZWGT(NBOBZ)   = 1.D0/PRAT(J)                                                             ! absolute weight
          INDOBS(NBOBZ) = 0                                                                        ! unused index
        ENDIF
      ENDDO
!
! CONSISTENCY CHECK
!
      IF    ( NBOHT(0) .EQ. 0       ) THEN
        PRINT 8126, NBOHT(0)                                                                       ! NBOPH = 0
        GOTO  9999
      ELSEIF( LHFIT           .AND.         &                                                      ! nb of H parameters to fit not = 0
              NBFOBZ   .EQ. 0       ) THEN                                                         ! and number of assigned frequencies = 0
        PRINT 8127, NBXOHT(0),NBFOBZ
        GOTO  9999
      ELSEIF( LTFIT           .AND.         &                                                      ! nb of T parameters to fit not = 0
              NBSOBZ   .EQ. 0       ) THEN                                                         ! and number of assigned intensities = 0
        PRINT 8129, NBXOP-NBXOHT(0),NBSOBZ
        GOTO  9999
      ENDIF
! NUMBER OF FREQUENCY AND INTENSITY DATA TO FIT PER TRANSITION
      DO ITR=1,NTR                                                                                 ! for each transition
        NBFTR(ITR) = 0                                                                             ! initalize
        NBSTR(ITR) = 0
      ENDDO
      DO IOBS=1,NBOBS                                                                              ! for each observed data
        ITR = JTR(IOBS)
        IF( FASOT(IOBS) .EQ. '+' .AND.         &
            LHFIT                      ) THEN
          NBFTR(ITR) = NBFTR(ITR)+1                                                                ! increment nb of frequency data to fit for the transition related to this observed data
        ENDIF
        IF( SASOT(IOBS)        .EQ. '+' .AND.         &
            LTFIT                       .AND.         &
            NBXOHT(INTRT(ITR)) .NE.  0        ) THEN
          NBSTR(ITR) = NBSTR(ITR)+1                                                                ! increment nb of intensity data to fit for the transition related to this observed data
        ENDIF
      ENDDO
! TOTAL NUMBER OF FREQUENCY AND INTENSITY DATA TO FIT
      NBFTRC = 0                                                                                   ! total number of frequencies to fit
      NBSTRC = 0                                                                                   ! total number of intensities to fit
      DO ITR=1,NTR                                                                                 ! for each transition
        IF( NBFTR(ITR) .EQ. 0 .AND.         &
            NBSTR(ITR) .EQ. 0       ) THEN
! WARNING : NO ASSIGNMENT FOR THIS TRANSITION
          IF( ISRAM(ITR) ) THEN
            PRINT 8203, 'P'//CPOL(INPS(ITR))//'mP'//CPOL(INPI(ITR))//' pol '//POLST(ITR)
          ELSE
            PRINT 8203, 'P'//CPOL(INPS(ITR))//'mP'//CPOL(INPI(ITR))
          ENDIF
        ENDIF
        NBFTRC = NBFTRC+NBFTR(ITR)                                                                 ! add
        NBSTRC = NBSTRC+NBSTR(ITR)                                                                 ! add
      ENDDO
      IF( NBFTRC .NE. NBFOBZ .OR.         &                                                        ! internal check
          NBSTRC .NE. NBSOBZ      ) THEN
        PRINT 8206, NBFTRC,NBFOBZ,NBSTRC,NBSOBZ
        GOTO  9999
      ENDIF
!
! ***************************************************
! *** FIT
! ***************************************************
!
      OPEN (11,FILE="xpafit_BREAKNOW",STATUS="UNKNOWN",ERR=9989)                                   ! delete breaknow flag file
      CLOSE(11,STATUS="DELETE",ERR=9988)
      OPEN (11,FILE="xpafit_PARESTIM",STATUS="UNKNOWN",ERR=9987)                                   ! delete para_estimates.t flag file
      CLOSE(11,STATUS="DELETE",ERR=9986)
      M = NBOBZ                                                                                    ! nb of functions (frequencies+intensities to fit)
      N = NBXOP                                                                                    ! nb of variables (parameters to fit)
      IF( NTRT .EQ. 0 ) THEN
        PRINT 2004, NBOHT(0),0
      ELSE
        PRINT 2004, (NBOHT(ITRT),ITRT=0,NTRT)
      ENDIF
      PRINT 2005, NBOP
      IF( NTRT .EQ. 0 ) THEN
        PRINT 2006, NBXOHT(0),0
      ELSE
        PRINT 2006, (NBXOHT(ITRT),ITRT=0,NTRT)
      ENDIF
      PRINT 2007, NBXOP
      LDFJAC = MXOBZ                                                                               ! leading dimension of FJAC
      IFLAG  = 1                                                                                   ! fonction call
      IONLY  = .FALSE.                                                                             ! NOT ONLY intensity
      CALL FCN(M,N,XPARA,FOMC,FJAC,LDFJAC,IFLAG)                                                   ! obs-cal            before fit
      MTRUE = NBROBZ-NUNSEL()
      PRINT 2003, NBOBS,            &
                  NBFOBZ,           &
                  NBSOBZ-NUNSEL(),  &
                  NBSOBZ,           &
                  NUNSEL(),         &
                  NBOBZ-NBROBZ,     &
                  MTRUE
      IF( MTRUE .LE. N ) THEN
        PRINT 8019, MTRUE,N
        GOTO  9999
      ENDIF
      CALL XWPART                                                                                  ! RMS and STD per transition
      NBXOTT = 0
      DO I=1,NTRT
        NBXOTT = NBXOTT+NBXOHT(I)
      ENDDO
      IF( NBXOTT          .NE. 0      .AND.         &
          NBSOBZ-NUNSEL() .LE. NBXOTT       ) THEN
        PRINT 8020, NBSOBZ-NUNSEL(),NBXOTT
        GOTO  9999
      ENDIF
      FNORMI = ENORM(M,FOMC)                                                                       ! euclidean norm     before fit
      STDI   = FNORMI/SQRT(DBLE(MTRUE))                                                            ! standard deviation before fit
      RMSHI  = 0.D0
      RMSTI  = 0.D0
      IF( NBFOBZ .NE. 0 ) THEN                                                                     ! frequency
        DO I=1,NBFOBZ                                                                              ! relative indexes
          ISEL(I) = I
        ENDDO
        RMSHI = RMSH(NBFOBZ,ISEL)                                                                  ! RMS                before fit
      ENDIF
      IF( NBSOBZ .NE. 0 ) THEN                                                                     ! intensity
        DO I=1,NBSOBZ                                                                              ! relative indexes
          ISEL(I) = NBFOBZ+I
        ENDDO
        RMSTI = RMST(NBSOBZ,ISEL)                                                                  ! RMS                before fit
      ENDIF
      NFEV = 0                                                                                     ! nb of call of FCN as function
      NJEV = 0                                                                                     ! nb of call of FCN as jacobian
      TOL  = SQRT(DPMPAR(1))                                                                       ! machine precision
      CALL LMDER1(FCN,M,N,XPARA,FOMC,FJAC,LDFJAC,TOL,INFO,IPVT,WA,LWA,MXITER)                      ! fit
      CALL XPFITC(M,N,FJAC,LDFJAC)                                                                 ! post-fit calculus
      PRINT 4001
      PRINT 4002, FNORMI,STDI,RMSHI*1.D+3,RMSTI*1.D+2
      PRINT 4003
      PRINT 4002, FNORMF,STDF,RMSHF*1.D+3,RMSTF*1.D+2
      PRINT 4004, NFEV,NJEV,INFO
!
! WRITE OUT OUTPUT FILES
!
!     IFLAG = 1
!     IONLY = .FALSE.
!     CALL FCN(M,N,XPARA,FOMC,FJAC,LDFJAC,IFLAG)                                                   ! obs-cal after fit
      CALL XWPAES
      CALL XWPAFI
!
! STANDARD DEVIATION OF CALCULATED DATA
!
      DO I=1,M
        VARC = 0.D0
        DO J=1,N
          DO KA=1,N
            VARC = VARC+SDZEXP(I)*SDZEXP(I)*FJAC(I,J)*FJAC(I,KA)*COV(J,KA)
          ENDDO
        ENDDO
        SDZCAL(I) = SQRT(VARC)
      ENDDO
!
! WRITE FREQUENCY PREDICTION AND STATISTICS FILES
!
      IF( NBFOBZ .NE. 0 ) THEN
        CALL XWF
        IF( LACAL .OR.              &
            LADIP .OR.              &
            LAPOL      ) CALL XWFA
      ENDIF
!
! WRITE INTENSITY PREDICTION AND STATISTICS FILES
!
      IF( NBSOBZ .NE. 0 ) CALL XWS
      GOTO 9000                                                                                    ! end
!
9986  PRINT 8227
      GOTO  9999
9987  PRINT 8226
      GOTO  9999
9988  PRINT 8225
      GOTO  9999
9989  PRINT 8224
      GOTO  9999
9990  PRINT 8025, NTR,CARG
      GOTO  9999
9991  PRINT 8024, FASSI
      GOTO  9999
9992  PRINT 8014, FPARAC
      GOTO  9999
9993  PRINT 8013, FPARAC
      GOTO  9999
9994  PRINT 8012, FPARAC
      GOTO  9999
9995  PRINT 8011, FCLX
      GOTO  9999
9996  PRINT 8010, FASSI
      GOTO  9999
9997  PRINT 8002
      CLOSE(10)
      GOTO  9999
9998  PRINT 8001
9999  PRINT 8000
!
9000  PRINT *
      END PROGRAM XPAFIT
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
! READ HAMILTONIAN PARAMETERS
!
      SUBROUTINE IOPH(LUI)
      use mod_dppr
      use mod_par_tds
      use mod_com_fp
      use mod_com_spin
      use mod_main_xpafit
      IMPLICIT NONE
      integer          :: LUI

      real(kind=dppr)  :: PREC

      integer          :: I,IP
      integer          :: NBOPHC

      character(len = NBCTIT)  :: TITRE
!
1000  FORMAT(A)
1001  FORMAT(I4)
1002  FORMAT(A,E18.11,E14.7)
8000  FORMAT(' !!! XPAFIT : STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF IN IOPH')
!
      READ(LUI,1000,END=2000)
      READ(LUI,1000,END=2000) TITRE
      READ(TITRE,*) SPIN,SPINY
      READ(LUI,1000,END=2000)
      READ(LUI,1000,END=2000) TITRE
      READ(TITRE,*) VIBNU,B0,A0,BB0,DD0
      READ(LUI,1001,END=2000) NBOPHC
      DO WHILE( NBOPHC .GT. MXOPH )
        CALL RESIZE_MXOPH
      ENDDO
      INPHT(0) = 0                                                                                 ! starting index (-1) of H parameters
      NBOHT(0) = NBOPHC                                                                            ! current number of H operators
      DO I=1,2
        READ(LUI,1000,END=2000) TITRE
      ENDDO
      DO IP=INPHT(0)+1,INPHT(0)+NBOHT(0)
        READ(LUI,1002,END=2000) COPAJ(IP),PARAO(IP),PREC                                           ! operator definition, original parameter value
      ENDDO
      RETURN
!
2000  PRINT 8003
      PRINT 8000
      PRINT *
      STOP
!
      END SUBROUTINE IOPH
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
! READ TRANSITION PARAMETERS
!
      SUBROUTINE IOPT(LUI,ITRT)
      use mod_dppr
      use mod_par_tds
      use mod_main_xpafit
      IMPLICIT NONE
      integer          :: LUI,ITRT

      real(kind=dppr)  :: PREC

      integer          :: IDEB,IP
      integer          :: NBOPHC,NBOPTC
!
1000  FORMAT(A)
1002  FORMAT(A,E18.11,E14.7)
3004  FORMAT(////,   &
             I4,//)
8000  FORMAT(' !!! XPAFIT : STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF IN IOPT')
!
      READ(LUI,3004,END=2000) NBOPHC
      DO IP=1,NBOPHC
        READ(LUI,1000,END=2000)
      ENDDO
      READ(LUI,3004,END=2000) NBOPTC
      DO WHILE( NBOPTC .GT. MXOPT )
        CALL RESIZE_MXOPT
      ENDDO
      INPHT(ITRT) = INPHT(ITRT-1)+NBOHT(ITRT-1)                                                    ! starting index (-1) of this type of T parameters
      NBOHT(ITRT) = NBOPTC                                                                         ! current number of T operators
      IDEB        = INPHT(ITRT)
      DO IP=IDEB+1,IDEB+NBOHT(ITRT)
        READ(LUI,1002,END=2000) COPAJ(IP),PARAO(IP),PREC                                           ! operator definition, original parameter value
      ENDDO
      RETURN
!
2000  PRINT 8003
      PRINT 8000
      PRINT *
      STOP
!
      END SUBROUTINE IOPT
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!  OBS-CAL CALCULUS
!  TO BE CALLED BY MINPACK SUBROUTINES
!
!       FCN IS THE NAME OF THE USER-SUPPLIED SUBROUTINE WHICH
!         CALCULATES THE FUNCTIONS AND THE JACOBIAN. FCN MUST
!         BE DECLARED IN AN EXTERNAL STATEMENT IN THE USER
!         CALLING PROGRAM, AND SHOULD BE WRITTEN AS FOLLOWS.
!
!         SUBROUTINE FCN(M,N,X,FVEC,FJAC,LDFJAC,IFLAG)
!         INTEGER M,N,LDFJAC,IFLAG
!         DOUBLE PRECISION X(N),FVEC(M),FJAC(LDFJAC,N)
!         ----------
!         IF IFLAG = 1 CALCULATE THE FUNCTIONS AT X AND
!         RETURN THIS VECTOR IN FVEC. DO NOT ALTER FJAC.
!         IF IFLAG = 2 CALCULATE THE JACOBIAN AT X AND
!         RETURN THIS MATRIX IN FJAC. DO NOT ALTER FVEC.
!         ----------
!         RETURN
!         END
!
!         THE VALUE OF IFLAG SHOULD NOT BE CHANGED BY FCN UNLESS
!         THE USER WANTS TO TERMINATE EXECUTION OF LMDER1.
!         IN THIS CASE SET IFLAG TO A NEGATIVE INTEGER.
!
!       M IS A POSITIVE INTEGER INPUT VARIABLE SET TO THE NUMBER
!         OF FUNCTIONS.
!
!       N IS A POSITIVE INTEGER INPUT VARIABLE SET TO THE NUMBER
!         OF VARIABLES. N MUST NOT EXCEED M.
!
!       X IS AN ARRAY OF LENGTH N. X CONTAINS THE SOLUTION VECTOR.
!
!       FVEC IS AN OUTPUT ARRAY OF LENGTH M WHICH CONTAINS
!         THE FUNCTIONS EVALUATED AT THE OUTPUT X.
!
!       FJAC IS AN OUTPUT M BY N ARRAY. THE UPPER N BY N SUBMATRIX
!         OF FJAC CONTAINS AN UPPER TRIANGULAR MATRIX R WITH
!         DIAGONAL ELEMENTS OF NONINCREASING MAGNITUDE SUCH THAT
!
!                T     T           T
!               P *(JAC *JAC)*P = R *R,
!
!         WHERE P IS A PERMUTATION MATRIX AND JAC IS THE FINAL
!         CALCULATED JACOBIAN. COLUMN J OF P IS COLUMN IPVT(J)
!         (SEE BELOW) OF THE IDENTITY MATRIX. THE LOWER TRAPEZOIDAL
!         PART OF FJAC CONTAINS INFORMATION GENERATED DURING
!         THE COMPUTATION OF R.
!
!       LDFJAC IS A POSITIVE INTEGER INPUT VARIABLE NOT LESS THAN M
!         WHICH SPECIFIES THE LEADING DIMENSION OF THE ARRAY FJAC.
!
      SUBROUTINE FCN(M,N,X,FVEC,FJACC,LDFJAC,IFLAG)
      use mod_dppr
      use mod_par_tds
      use mod_com_xfonc
      use mod_com_xpafit
      use mod_com_xpara
      use mod_com_xpoly
      use mod_com_xtran
      use mod_com_xvari
      IMPLICIT NONE
      real(kind=dppr) ,dimension(N)         :: X
      real(kind=dppr) ,dimension(M)         :: FVEC
      real(kind=dppr) ,dimension(LDFJAC,N)  :: FJACC
      integer          :: M,N,LDFJAC,IFLAG

! functions
      real(kind=dppr)  :: ENORM
      integer          :: NUNSEL

      real(kind=dppr)  :: FNORM
      real(kind=dppr)  :: PARAC
      real(kind=dppr)  :: STD

      integer          :: I,IB,IDEB,ITRT,IX
      integer          :: J

      logical          :: LBREAK
!
1000  FORMAT('Standard deviation for current FCN function call : ',D18.11)
1001  FORMAT('                               FCN jacobian call')
1002  FORMAT('Standard deviation for current FCN function call : ',D18.11,5X,   &
             '(INTENSITY :',I8,' UNSELECTED DUE TO RMXOMC)'                  )
8100  FORMAT(' !!! ERROR TESTING xpafit_BREAKNOW file')
!
      OPEN(unit = 90 , file = 'xpafit.out', position = 'append')
      IF( IFLAG .EQ. 1 ) NFEV = NFEV+1                                                             ! function call
      IF( IFLAG .EQ. 2 ) NJEV = NJEV+1                                                             ! jacobian call
      IX = 0                                                                                       ! index of fitted parameter
      DO I=1,NBOP                                                                                  ! for each parameter
        IF( ICNTRL(I) .EQ. 2 .OR.         &
            ICNTRL(I) .EQ. 4      ) THEN                                                           ! fitted
          IX      = IX+1
          PARA(I) = X(IX)                                                                          ! calculus used parameter
          IF( IFLAG .EQ. 1 ) PARAR(I) = PARA(I)                                                    ! set real parameter
        ENDIF
      ENDDO
!
! TAKE CONTRIBUTIONS INTO ACCOUNT
!
      DO I=1,NBOHT(0)                                                                              ! H
        IF( ICNTRL(I) .EQ. 5 ) THEN                                                                ! 5 -> contribution
          PARAC = 0.D0
          DO J=1,NBOHT(0)                                                                          ! for each H operator
            IF( CTRIBH(I,J) .NE. 0.D0 ) PARAC = PARAC+CTRIBH(I,J)*PARA(J)                          ! add contribution
          ENDDO
          PARA(I) = PARAC                                                                          ! calculus used parameter
          IF( IFLAG .EQ. 1 ) PARAR(I) = PARA(I)                                                    ! set real parameter
        ENDIF
      ENDDO
      DO ITRT=1,NTRT                                                                               ! Ts
        IDEB = INPHT(ITRT)                                                                         ! starting index (-1)
        DO I=IDEB+1,IDEB+NBOHT(ITRT)
          IF( ICNTRL(I) .EQ. 5 ) THEN                                                              ! 5 -> contribution
            PARAC = 0.D0
            IB    = I-IDEB                                                                         ! for CTRIBT
            DO J=1,NBOHT(ITRT)
              IF( CTRIBT(ITRT,IB,J) .NE. 0.D0 ) THEN
                PARAC = PARAC+CTRIBT(ITRT,IB,J)*PARA(IDEB+J)                                       ! add contribution
              ENDIF
            ENDDO
            PARA(I) = PARAC                                                                        ! calculus used parameter
            IF( IFLAG .EQ. 1 ) PARAR(I) = PARA(I)                                                  ! set real parameter
          ENDIF
        ENDDO
      ENDDO
      IF( IFLAG .EQ. 2 ) THEN
        CALL XFJAC(M,N,X,FVEC,FJACC,LDFJAC)                                                        ! derivatives calculus
        PRINT 1001
        WRITE(90,1001)
      ELSE
        CALL XOBMCA(M,FVEC,IFLAG)                                                                  ! 1,3 : obs-cal calculus
        IF( IFLAG .EQ. 1 ) THEN                                                                    ! function call
          FNORM = ENORM(M,FVEC)                                                                    ! euclidean norm
          STD   = FNORM/SQRT(DBLE(M))                                                              ! standard deviation
          IF( NTRT .NE. 0 ) THEN                                                                   ! Ts implied
            PRINT 1002, STD,NUNSEL()
            WRITE(90,1002) STD,NUNSEL()
          ELSE
            PRINT 1000, STD                                                                        ! H only
            WRITE(90,1000) STD  
          ENDIF
          DO I=1,M
            FOMC(I) = FVEC(I)
          ENDDO
          INQUIRE(FILE="xpafit_BREAKNOW",EXIST=LBREAK,ERR=4)                                       ! test break flag file
          GOTO 5
!
4         PRINT 8100
!
5         IF( LBREAK ) IFLAG = -1                                                                  ! end lmder
        ENDIF
      ENDIF
      CLOSE(90)
!
      RETURN
      END SUBROUTINE FCN
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
! BASED UPON GAUJD (Y=0, X IGNORED)
! RESOLUTION OF A N RANK LINEAR SYSTEM (Y=A*X)
! THANKS TO GAUSS JORDAN ALGORITHM WITH A INVERSE CALCULUS
!
      SUBROUTINE MINVRT(N,A,AM1)
      use mod_dppr
      use mod_par_tds
      use mod_par_x
      use mod_com_xpafit
      IMPLICIT NONE
      real(kind=dppr) ,dimension(MXOP,MXOP)      :: A,AM1
      integer          :: N

      real(kind=dppr)  :: FACTC
      real(kind=dppr)  :: H
      real(kind=dppr)  :: PIVOT

      integer          :: J
      integer          :: KA
      integer          :: L,LI,LP
      integer          :: NBCOL
!
8000  FORMAT(' !!! XPAFIT : STOP ON ERROR')
8100  FORMAT(' !!! DIMENSION = ',I4,' EXCEEDING MXOP = ',I8)
8101  FORMAT(' !!! THE MATRIX IS SINGULAR TO WORKING PRECISION')
!
      IF( N .GT. MXOP ) THEN
        PRINT 8100, N,MXOP
        PRINT 8000
        PRINT *
        STOP
!
      ENDIF
!
! INITIALIZATION OF AIY
!
      NBCOL = 2*N+1
      DO L=1,N
        DO KA=1,N
          AIY(L,KA)   = A(L,KA)
          AIY(L,KA+N) = 0.D0
        ENDDO
        AIY(L,NBCOL) = 0.D0
        AIY(L,L+N)   = 1.D0
      ENDDO
!
! PIVOTS LOOP
!
      DO LP=1,N
        LI = LP
!
110     PIVOT = AIY(LI,LP)
        IF( PIVOT .NE. 0.D0 ) GOTO 200
        LI = LI+1
        IF( LI .LE. N ) GOTO 110
        PRINT 8101
        PRINT 8000
        PRINT *
        STOP
!
! PERMUTATION OF LI AND LP LINES
!
200     DO J=LP,NBCOL
          H         = AIY(LP,J)
          AIY(LP,J) = AIY(LI,J)
          AIY(LI,J) = H
        ENDDO
!
! LP LINE DIVISION BY PIVOT
!
        DO KA=LP,NBCOL
          AIY(LP,KA) = AIY(LP,KA)/PIVOT
        ENDDO
!
! ANNULMENT OF LP COLUMN ELEMENTS
!
E5:     DO L=1,N
          IF( L .EQ. LP ) CYCLE E5
          FACTC = AIY(L,LP)
          DO KA=LP,NBCOL
            AIY(L,KA) = AIY(L,KA)-FACTC*AIY(LP,KA)
          ENDDO
        ENDDO E5
      ENDDO
!
! SOLUTION
!
      DO L=1,N
        DO KA=1,N
          AM1(L,KA) = AIY(L,KA+N)
        ENDDO
      ENDDO
!
      RETURN
      END SUBROUTINE MINVRT
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!  READ CONTROL OF H PARAMETERS IN PARAMETER CONTROL FILE
!
! 0 -> FIXED TO 0
! 1 -> FIXED TO ITS VALUE
! 2 -> BOUND TO THE VALUE IN COLUMN (B) +/- VALUE IN COLUMN (C)
! 3 -> BOUND TO ITS CURRENT VALUE WITH MAXIMUM RELATIVE DEVIATION GIVEN IN COLUMN (C)
!      NOT APPLICABLE : ALREADY TAKEN INTO ACCOUNT BY FIT MODEL
! 4 -> FREE (IN FACT PROCESSED AS 3 BY FIT MODEL)
! 5 -> CONTRIBUTION
!
      SUBROUTINE READCH
      use mod_par_tds
      use mod_com_xpara
      use mod_com_xtran
      IMPLICIT NONE

      integer          :: I
      integer          :: J
      integer          :: NBOPHC,NBOPHR,NCL1,NCL2,NDEB,NFIN,NSTAR

      character(len = NBCLAB+10)  :: COPAJC
      character(len = NBCTIT)     :: TITRE

      logical          :: CTRIBC
!
1000  FORMAT(A)
3001  FORMAT(I1)
3004  FORMAT(////,   &
             I4,//)
3006  FORMAT(A,E18.11,E14.7)
8120  FORMAT(' !!! INCOMPATIBLE NUMBER OF H PARAMETERS : ',I8,' < ',I8,/,   &
             '     FOR PARAMETER FILE : ',A                           ,/,   &
             '     AND CONTROL   FILE'                                   )
8121  FORMAT(' WARNING: LAST ',I4,' UNCONTROLED H PARAMETERS'          ,/,   &
             '          WILL BE FIXED (type 1) FOR PARAMETER FILE : ',A   )
8201  FORMAT(' !!! INCOMPATIBLE H PARAMETERS'          ,/,   &
             ' !!! IN  PARA. CONTROL FILE : ',A        ,/,   &
             ' !!! AND PARAMETER     FILE : ',A,' : ',A   )
8202  FORMAT(' !!! BAD CONTRIBUTION LINE FOR PARAMETER #',I4)
8203  FORMAT(' !!! BAD CONTROL STATE FOR PARAMETER #',I4)
!
      NBOPHR = NBOHT(0)                                                                            ! real    nb of H operators
      READ(12,3004) NBOPHC                                                                         ! current nb of H operators (in control file)
      IF    ( NBOPHC .GT. NBOPHR ) THEN                                                            ! too much parameters
        PRINT 8120, NBOPHR,NBOPHC,FPARA(ITRHT(0))
        GOTO  9999
      ELSEIF( NBOPHC .LT. NBOPHR ) THEN                                                            ! not all parameters
! WARNING
        PRINT 8121, NBOPHR-NBOPHC,FPARA(ITRHT(0))
      ENDIF
      NCL1 = NBCLAB+1+NBAM+1                                                                       ! Hmn  included
      NCL2 = NBCLAB+1+NBAM+1+6                                                                     ! Frdm included
E10:  DO I=1,NBOPHC                                                                                ! for each parameter
        READ(12,1000) COPAJC
        IF( COPAJC(:NCL1) .NE. COPAJ(I)(:NCL1) ) THEN                                              ! check consistency
          PRINT 8201, COPAJC(:NCL1),COPAJ(I)(:NCL1),FPARA(ITRHT(0))
          GOTO  9999
        ENDIF
        READ(COPAJC(NCL2:NCL2),3001) ICNTRL(I)                                                     ! parameter control state
        IF    ( ICNTRL(I) .EQ. 0 ) THEN                                                            ! 0 -> fixed to 0
          PARAR(I) = 0.D0
        ELSEIF( ICNTRL(I) .EQ. 1 ) THEN                                                            ! 1 -> fixed to its current value
          PARAR(I) = PARAO(I)
        ELSEIF( ICNTRL(I) .EQ. 2 ) THEN                                                            ! 2 -> bound to VALAT        +/- PRAT
          BACKSPACE(12)
          READ(12,3006) COPAJC,VALAT(I),PRAT(I)
          PARAR(I) = VALAT(I)
        ELSEIF( ICNTRL(I) .EQ. 3 ) THEN                                                            ! 3 -> bound to current val. +/- PRAT
          GOTO 9992                                                                                ! N/A
        ELSEIF( ICNTRL(I) .EQ. 4 ) THEN                                                            ! 4 -> free
          PARAR(I) = PARAO(I)
        ELSEIF( ICNTRL(I) .EQ. 5 ) THEN                                                            ! 5 -> contribution
          DO J=1,NBOHT(0)                                                                          ! initialize contribution
            CTRIBH(I,J) = 0.D0
          ENDDO
!
18        READ(12,1000,END=15) TITRE                                                               ! next line
          NSTAR = INDEX(TITRE,'*')                                                                 ! index of a possible *
          IF( NSTAR .LT. 1 ) THEN                                                                  ! not a contribution line
            BACKSPACE(12)
            GOTO 15                                                                                ! end contribution processing
          ENDIF
          NDEB = 1                                                                                 ! starting index
          NFIN = LEN_TRIM(TITRE)                                                                   ! ending   index
!
17        NSTAR = NDEB-1+INDEX(TITRE(NDEB:NFIN),'*')                                               ! star     index
          IF( NSTAR .LT. NDEB ) GOTO 18                                                            ! no more * in the line
          READ(TITRE(NSTAR+1:NFIN),*) J                                                            ! current parameter #
          IF( CTRIBH(I,J) .NE. 0.D0 ) GOTO 9993                                                    ! already set : error
          READ(TITRE(NDEB:NSTAR-1),*) CTRIBH(I,J)                                                  ! contribution coefficient
          NDEB = NSTAR+INDEX(TITRE(NSTAR+1:),' ')                                                  ! next contribution
          GOTO 17                                                                                  ! find next contribution in this line
!
15        CTRIBC = .FALSE.                                                                         ! no contribution for this parameter
          DO J=1,NBOP                                                                              ! for each operator
            IF( J .EQ. I ) THEN
              IF( CTRIBH(I,J) .EQ. 0.D0 ) GOTO 9993                                                ! current parameter missing
            ELSE
              IF( CTRIBH(I,J) .NE. 0.D0 ) CTRIBC = .TRUE.                                          ! there is contribution for this parameter
            ENDIF
          ENDDO
          IF( .NOT. CTRIBC ) GOTO 9993                                                             ! no contribution for this parameter
          DO J=1,NBOP
            CTRIBH(I,J) = -CTRIBH(I,J)/CTRIBH(I,I)                                                 ! optimize
          ENDDO
          CTRIBH(I,I) = 0.D0                                                                       ! optimize
        ELSE
          GOTO 9992                                                                                ! bad control state
        ENDIF
      ENDDO E10
! PARTIAL CONTROL FILE CASE
      IF( NBOPHC .LT. NBOPHR ) THEN
        DO I=NBOPHC+1,NBOPHR
          ICNTRL(I) = 1                                                                            ! 1 -> fixed to its current value
          PARAR(I)  = PARAO(I)
        ENDDO
      ENDIF
      RETURN
!
9992  PRINT 8203, I
      GOTO  9999
9993  PRINT 8202, I
      GOTO  9999
9999  CALL CLORD                                                                                   ! print file definition order in CL_
      END SUBROUTINE READCH
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!  READ CONTROL OF T PARAMETERS IN PARAMETER CONTROL FILE
!
! 0 -> FIXED TO 0
! 1 -> FIXED TO ITS VALUE
! 2 -> BOUND TO THE VALUE IN COLUMN (B) +/- VALUE IN COLUMN (C)
! 3 -> BOUND TO ITS CURRENT VALUE WITH MAXIMUM RELATIVE DEVIATION GIVEN IN COLUMN (C)
!      NOT APPLICABLE : ALREADY TAKEN INTO ACCOUNT BY FIT MODEL
! 4 -> FREE (IN FACT PROCESSED AS 3 BY FIT MODEL)
! 5 -> CONTRIBUTION
!
      SUBROUTINE READCT(ITRT)
      use mod_par_tds
      use mod_com_xpara
      use mod_com_xtran
      IMPLICIT NONE
      integer          :: ITRT

      integer          :: I,IB,IDEB
      integer          :: J
      integer          :: NBOPTC,NBOPTR,NCL1,NCL2,NDEB,NFIN,NSTAR

      character(len = NBCLAB+10)  :: COPAJC
      character(len = NBCTIT)     :: TITRE

      logical          :: CTRIBC
!
1000  FORMAT(A)
3001  FORMAT(I1)
3004  FORMAT(////,   &
             I4,//)
3006  FORMAT(A,E18.11,E14.7)
8120  FORMAT(' !!! INCOMPATIBLE NUMBER OF T PARAMETERS : ',I8,' < ',I8,/,   &
             '     FOR PARAMETER FILE : ',A                           ,/,   &
             '     AND CONTROL   FILE'                                   )
8121  FORMAT(' WARNING: LAST ',I4,' UNCONTROLED T PARAMETERS'          ,/,   &
             '          WILL BE FIXED (type 1) FOR PARAMETER FILE : ',A   )
8201  FORMAT(' !!! INCOMPATIBLE T PARAMETERS'          ,/,   &
             ' !!! IN  PARA. CONTROL FILE : ',A        ,/,   &
             ' !!! AND PARAMETER     FILE : ',A,' : ',A   )
8202  FORMAT(' !!! BAD CONTRIBUTION LINE FOR PARAMETER #',I4)
8203  FORMAT(' !!! BAD CONTROL STATE FOR PARAMETER #',I4)
!
      NBOPTR = NBOHT(ITRT)                                                                         ! real    nb of T operators for this type
      IDEB   = INPHT(ITRT)                                                                         ! starting index (-1) (all parameters) for this type
      READ(12,3004) NBOPTC                                                                         ! current nb of T operators (in control file)
      IF    ( NBOPTC .GT. NBOPTR ) THEN                                                            ! too much parameters
        PRINT 8120, NBOPTR,NBOPTC,FPARA(ITRHT(ITRT))
        GOTO  9999
      ELSEIF( NBOPTC .LT. NBOPTR ) THEN                                                            ! not all parameters
        PRINT 8121, NBOPTR-NBOPTC,FPARA(ITRHT(ITRT))
      ENDIF
      NCL1 = NBCLAB+1+NBAM+1                                                                       ! Hmn  included
      NCL2 = NBCLAB+1+NBAM+1+6                                                                     ! Frdm included
E10:  DO I=IDEB+1,IDEB+NBOPTC                                                                      ! for each parameter
        READ(12,1000) COPAJC
        IF( COPAJC(:NCL1) .NE. COPAJ(I)(:NCL1) ) THEN                                              ! check consistency
          PRINT 8201, COPAJC(:NCL1),COPAJ(I)(:NCL1),FPARA(ITRHT(ITRT))
          GOTO  9999
        ENDIF
        READ(COPAJC(NCL2:NCL2),3001) ICNTRL(I)                                                     ! parameter control state
        IF    ( ICNTRL(I) .EQ. 0 ) THEN                                                            ! 0 -> fixed to 0
          PARAR(I) = 0.D0
        ELSEIF( ICNTRL(I) .EQ. 1 ) THEN                                                            ! 1 -> fixed to its current value
          PARAR(I) = PARAO(I)
        ELSEIF( ICNTRL(I) .EQ. 2 ) THEN                                                            ! 2 -> bound to VALAT        +/- PRAT
          BACKSPACE(12)
          READ(12,3006) COPAJC,VALAT(I),PRAT(I)
          PARAR(I) = VALAT(I)
        ELSEIF( ICNTRL(I) .EQ. 3 ) THEN                                                            ! 3 -> bound to current val. +/- PRAT
          GOTO 9992                                                                                ! N/A
        ELSEIF( ICNTRL(I) .EQ. 4 ) THEN                                                            ! 4 -> free
          PARAR(I) = PARAO(I)
        ELSEIF( ICNTRL(I) .EQ. 5 ) THEN                                                            ! 5 -> contribution
          IB = I-IDEB                                                                              ! for CTRIBT
          DO J=1,NBOHT(ITRT)                                                                       ! initialize contribution
            CTRIBT(ITRT,IB,J) = 0.D0
          ENDDO
!
18        READ(12,1000,END=15) TITRE                                                               ! next line
          NSTAR = INDEX(TITRE,'*')                                                                 ! index of a possible *
          IF( NSTAR .LT. 1 ) THEN                                                                  ! not a contribution line
            BACKSPACE(12)
            GOTO 15                                                                                ! end contribution processing
          ENDIF
          NDEB = 1                                                                                 ! starting index
          NFIN = LEN_TRIM(TITRE)                                                                   ! ending   index
!
17        NSTAR = NDEB-1+INDEX(TITRE(NDEB:NFIN),'*')                                               ! star     index
          IF( NSTAR .LT. NDEB ) GOTO 18                                                            ! no more * in the line
          READ(TITRE(NSTAR+1:NFIN),*) J                                                            ! current parameter #
          IF( CTRIBT(ITRT,IB,J) .NE. 0.D0 ) GOTO 9993                                              ! already set : error
          READ(TITRE(NDEB:NSTAR-1),*) CTRIBT(ITRT,IB,J)                                            ! contribution coefficient
          NDEB = NSTAR+INDEX(TITRE(NSTAR+1:),' ')                                                  ! next contribution
          GOTO 17                                                                                  ! find next contribution in this line
!
15        CTRIBC = .FALSE.                                                                         ! no contribution for this parameter
          DO J=1,NBOP                                                                              ! for each operator
            IF( J .EQ. IB ) THEN
              IF( CTRIBT(ITRT,IB,J) .EQ. 0.D0 ) GOTO 9993                                          ! current parameter missing
            ELSE
              IF( CTRIBT(ITRT,IB,J) .NE. 0.D0 ) CTRIBC = .TRUE.                                    ! there is contribution for this parameter
            ENDIF
          ENDDO
          IF( .NOT. CTRIBC ) GOTO 9993                                                             ! no contribution for this parameter
          DO J=1,NBOP
            CTRIBT(ITRT,IB,J) = -CTRIBT(ITRT,IB,J)/CTRIBT(ITRT,IB,IB)                              ! optimize
          ENDDO
          CTRIBT(ITRT,IB,IB) = 0.D0                                                                ! optimize
        ELSE
          GOTO 9992                                                                                ! bad control state
        ENDIF
      ENDDO E10
! PARTIAL CONTROL FILE CASE
      IF( NBOPTC .LT. NBOPTR ) THEN
        DO I=IDEB+NBOPTC+1,IDEB+NBOPTR
          ICNTRL(I) = 1                                                                            ! 1 -> fixed to its current value
          PARAR(I)  = PARAO(I)
        ENDDO
      ENDIF
      RETURN
!
9992  PRINT 8203, I
      GOTO  9999
9993  PRINT 8202, I
      GOTO  9999
9999  CALL CLORD                                                                                   ! print file definition order in CL_
      END SUBROUTINE READCT
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
! PRINT FILE DEFINITION ORDER IN CL_
!
      SUBROUTINE CLORD
      use mod_par_tds
      use mod_com_xtran
      IMPLICIT NONE

      integer          :: ITRT
!
8000  FORMAT(' !!! XPAFIT : STOP ON ERROR')
8001  FORMAT(' !!! PARAMETER CONTROL FILE MUST SUCCESSIVELY SET CONTROL STATUS OF :',/,   &
             '     HAMILTONIAN PARAMETERS OF :', /,5X,A                                )
8002  FORMAT('     TRANSITION  PARAMETERS OF :',(/,5X,A))
!
      PRINT 8001, FPARA(ITRHT(0))                                                                  ! H first
      IF( NTRT .GT. 0 ) THEN
        PRINT 8002, (FPARA(ITRHT(ITRT)),ITRT=1,NTRT)                                               ! then Ts in type order
      ENDIF
      PRINT 8000
      PRINT *
      STOP
!
      END SUBROUTINE CLORD
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!  NUMBER OF UNSELECTED (NOT FITTED) INTENSITY DATA
!  DUE TO A RELATIVE DELTA (OBS-CAL) GREATER THEN RMXOMC
!
      FUNCTION NUNSEL()
      use mod_com_xfonc
      IMPLICIT NONE
      integer          :: NUNSEL

      integer          :: IZC
      integer          :: NBC
!
      NBC = 0
      DO IZC=1,NBSOBZ
        IF( SASOZ(IZC) .EQ. '-' ) NBC = NBC+1
      ENDDO
      NUNSEL = NBC
!
      RETURN
      END FUNCTION NUNSEL
