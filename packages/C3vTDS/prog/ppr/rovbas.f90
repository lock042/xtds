      PROGRAM ROVBAS
!
!  27.7.88 FORTRAN 77 POUR SUN4  J.M.JOUVARD
!  REV 25 JAN 1990
!  REV 15 FEV 1990
! REV    JAN 1995 JPC,CW (PARAMETER)
! MODIF 01/99 V. BOUDON ---> SCHEMA DE POLYADE QUELCONQUE.
! MODIF 05/09 V. BOUDON, A. EL HILAI ---> XY3Z/C3v.
!
!  CODAGE DES FONCTIONS DE BASE
!
! APPEL : rovbas Pn Nm Dk Jmax
!
!  ******    LIMITATIONS DU PROGRAMME
!
!
! DIMENSION MAXIMALE D'UN BLOC J,C
!     MXDIMS     !NRCOD,NVCOD
!
! NB MAXIMUM DE SOUS-NIVEAUX VIBRATIONNELS
!     MXSNV      !ICV:NFSB:IMIN
!
! NB MAXIMUM DE FONCTIONS DANS UN SOUS-BLOC
!     MXNCR      !NCR
!
      use mod_par_tds
      use mod_com_fdate
      use mod_main_rovbas
      IMPLICIT NONE

! functions
      character(len =   3)  :: KC

      integer          :: I,IC,IP,ISV
      integer          :: J,JMAX
      integer          :: NELMA,NFB,NNIV,NSV

      character(len = NBCTIT)  :: IDENT,TITRE
      character(len =  11) ,dimension(3)  :: CARG
      character(len =  40)  :: IDEMR
      character(len = 120)  :: FCFB,FEMRV
!
1000  FORMAT(A)
1001  FORMAT(I4,A)
1002  FORMAT(I2)
1009  FORMAT(/,              &
             'ROVBAS : ',A)
1010  FORMAT(' ROVBAS -> J = ',I3,'/',I3)
2000  FORMAT(' <  ',A)
2001  FORMAT(' >> ',A)
8000  FORMAT(' !!! ROVBAS : STOP ON ERROR')
8001  FORMAT(' !!! ERROR OPENING CONTROL FILE')
8002  FORMAT(' !!! INCOMPLETE CONTROL FILE')
8111  FORMAT(' !!! INCOMPATIBLE JMAX : ',I3)
!
! ARRAY INITIAL ALLOCATION
!
      CALL ALLOC_ALL
!
      OPEN(10,FILE=CTRLF,ERR=9998,STATUS='OLD')
      READ(10,1000,END=9997) FDATE
      PRINT 1009,            FDATE
      DO I=1,3
        READ(10,1000,END=9997) CARG(I)
      ENDDO
      READ(CARG(2)(2:3),1002) NNIV
      READ(10,*,END=9997) JMAX
      FEMRV = 'MH_'//TRIM(CARG(1))//'_'//CARG(3)
      FCFB  = 'FN_'//TRIM(CARG(1))//'_'
      CLOSE(10)
      PRINT 2000, TRIM(FEMRV)
      PRINT 2001, TRIM(FCFB)
      IF( JMAX .LT. 0 ) THEN
        PRINT 8111, JMAX
        GOTO  9999
      ENDIF
!
!  APPLICATION DES DIRECTIVES
!  NSV EST LE NOMBRE DE SOUS-NIVEAUX VIBRATIONNELS
      OPEN(20,STATUS='OLD',FILE=FEMRV)                                                             ! FICHIER DES E.M.R.V.
      OPEN(30,FILE=FCFB,FORM='UNFORMATTED',STATUS='UNKNOWN')                                       ! FICHIER DES CODES DES FCTS DE BASE
      DO I=1,3
        READ (20,1000) TITRE
        WRITE(30)      TITRE
      ENDDO
      READ (20,1000) IDEMR
      WRITE(30)      IDEMR
      DO I=1,4+NNIV
        READ (20,1000) TITRE
        WRITE(30)      TITRE
      ENDDO
      READ (20,1001) NSV,TITRE
      WRITE(30)      NSV,TITRE
      DO WHILE( NSV .GT. MXSNV )
        CALL RESIZE_MXSNV
      ENDDO
      READ (20,1000) TITRE
      WRITE(30)      TITRE
      DO ISV=1,NSV
        READ (20,1000) IDENT(:80)
        WRITE(30)      IDENT(:80)
        DO IC=1,MXSYR
          IF( KC(IC) .EQ. IDENT(76:78) ) ICV(ISV) = IC-2                                           ! cf. FORMAT 1002 de hmodel
        ENDDO
      ENDDO
!
! ***  BOUCLE J
!
      NB_SUP = -1
      DO J=0,JMAX
!
! ***  BOUCLE IC
!
        DO IC=1,MXSYM
          CALL INB(J,IC,NSV,NFB)
          NELMA = NFB*(NFB+1)/2
          WRITE(30) J,IC,NELMA,NFB
    IF( NFB .NE. 0 ) WRITE(*,*) J,IC,MXDIMS,NFB,':'
    IF( NFB .NE. 0 ) WRITE(*,*) (NVCOD(IP),NRCOD(IP),NRVCOD(IP),IP=1,NFB)
          IF( NFB .NE. 0 ) WRITE(30) (NVCOD(IP),NRCOD(IP),NRVCOD(IP),IP=1,NFB)
        ENDDO
      ENDDO
      CALL DEBUG( 'ROVBAS => MXNCR=',NB_SUP)
      CLOSE(20)
      CLOSE(30)
      PRINT 1010, J-1,JMAX
      GOTO  9000
!
9997  PRINT 8002
      GOTO  9999
9998  PRINT 8001
9999  PRINT 8000
!
9000  PRINT *
      END PROGRAM ROVBAS
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
! ***  DETERMINE LES INDICES DES FONCTIONS D'ONDE DANS LE BLOC J,C
! ***  REPRESENTANT H.
!
!  SMIL G.P.  J.P.C. JUIL 85
!
      SUBROUTINE INB(J,IC,NSV,NFB)
      use mod_par_tds
      use mod_com_rovbas
      IMPLICIT NONE
      integer          :: J,IC,NSV,NFB

      integer          :: I,IB,ICVI,ISB
!
      CALL PLADI(J,IC,NSV,NFB)
      IF( NFB .EQ. 0 ) RETURN
E15:  DO I=1,NSV
        ICVI = ICV(I)
        CALL INLIG(J,IC,ICVI,NFSB(I))
        IF( NFSB(I) .EQ. 0 ) CYCLE E15
        DO ISB=1,NFSB(I)
          IB         = IMIN(I)+ISB-1
          NRCOD(IB)  = NCR(ISB)
          NVCOD(IB)  = 10*I+ICV(I)+2
          NRVCOD(IB) = NCRV(ISB)
        ENDDO
      ENDDO E15
!
      RETURN
      END SUBROUTINE INB
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
! ***  DETERMINE LES INDICES DES FONCTIONS D'ONDE DU S-ESPACE J,C
!
!   SMIL G.P.  J.P.C. JUIL 85
!
      SUBROUTINE INLIG(J,IC,ICVI,NB)
      use mod_dppr
      use mod_par_tds
      use mod_main_rovbas
      IMPLICIT NONE
      integer          :: J,IC,ICVI,NB

! functions
      integer          :: KICTR

      real(kind=dppr) ,dimension(MDMIGA)  :: AKK
      real(kind=dppr)  :: AK

      integer          :: I,IK,IKK
      integer          :: L
      integer          :: NN
!
      I = 0
      DO IK=0,J
        IKK = IK
        IF( J-2*(J/2) .EQ. 1 .AND.             &
            IK        .EQ. 0       ) IKK = -1
        CALL MULCIV(DBLE(IKK),DBLE(ICVI),NN,AKK)
E4:     DO L=1,NN
         IF( KICTR(AKK(L),IC) .EQ. 1 )THEN
           AK = AKK(L)
           GOTO 3
         ENDIF
         CYCLE E4
!
3        I = I+1
         IF( I .GT. MXNCR ) CALL RESIZE_MXNCR
         NCR(I)  = IKK+2
         NCRV(I) = INT(AK)+2
        ENDDO E4
      ENDDO
      NB = I
      IF( NB .GT. NB_SUP ) NB_SUP = NB
!
      RETURN
      END SUBROUTINE INLIG
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
      SUBROUTINE PLADI(J,IC,NSV,NFB)
      use mod_par_tds
      use mod_main_rovbas
      IMPLICIT NONE
      integer          :: J,IC,NSV,NFB

      integer          :: I,ICVI
!
      NFB = 0
      DO I=1,NSV
        ICVI = ICV(I)
        CALL INLIG(J,IC,ICVI,NFSB(I))
        NFB = NFB+NFSB(I)
        DO WHILE( NFB .GT. MXDIMS )
          CALL RESIZE_MXDIMS
        ENDDO
        IMIN(I) = NFB-NFSB(I)+1
      ENDDO
!
      RETURN
      END SUBROUTINE PLADI
