      PROGRAM ASED
!
! REV    SEPT 2006  CW
!
!  lire un fichier binaire (ex. INFILE) de type ED_
!  et l'ecrire en ASCII dans INFILE'_ASC'
!
! APPEL : ased
!
      use mod_dppr
      use mod_par_tds
      use mod_main_hdiag
      IMPLICIT NONE

      integer          :: IC,IOP,IP,ISV
      integer          :: J
      integer          :: NBOPH,NFB,NSV

      character(len = NBCTIT)  :: TITRE
      character(len = 150)  :: INFILE
      character(len = 160)  :: OUTFILE
!
1000  FORMAT(A)
1020  FORMAT(/,            &
             'ASED   : ')
2000  FORMAT(' <  ',A)
2001  FORMAT(' >> ',A)
7000  FORMAT('ENTER ED_ TYPE FILE NAME :')
8000  FORMAT(' !!! ASED   : STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF FOR ',A)
!
! ARRAY INITIAL ALLOCATION
!
      CALL ALLOC_ALL
!
      PRINT 1020
!
!  FICHIER D'ENTREE
!
      PRINT 7000
      READ(*,1000) INFILE
      PRINT 2000,  TRIM(INFILE)
      OPEN(50,FILE=TRIM(INFILE),FORM='UNFORMATTED',STATUS='OLD')
!
!  FICHIER DE SORTIE
!
      OUTFILE = TRIM(INFILE)//'_ASC'
      PRINT 2001,  TRIM(OUTFILE)
      OPEN(10,FILE=TRIM(OUTFILE))
!
!     LECTURE DES PARAMETRES
!
      CALL IOPBAS(50,10)
      WRITE(10,*) '>>> NSV'
      READ (50,END=9003) NSV,TITRE
      WRITE(10,*)        NSV,TITRE
      DO WHILE( NSV .GT. MXSNV )
        CALL RESIZE_MXSNV
      ENDDO
      DO ISV=1,NSV
        READ (50,END=9003) TITRE(:80)
        WRITE(10,*)        TITRE(:80)
      ENDDO
      WRITE(10,*) '>>> NBOPH'
      READ (50,END=9003) NBOPH
      WRITE(10,*)        NBOPH
      DO WHILE( NBOPH .GT. MXOPH )
        CALL RESIZE_MXOPH
      ENDDO
      WRITE(10,*) '>>> J,IC,NFB'
      WRITE(10,*) '>>> HD(K(IP)),(DERI(IOP),IOP=1,NBOPH),(ICENT(ISV),ISV=1,NSV)'
!
12    READ (50,END=9000) J,IC,NFB
      WRITE(10,*)        J,IC,NFB
      DO IP=1,NFB
        READ (50,END=9003) HD(1),(DERI(IOP),IOP=1,NBOPH),(ICENT(ISV),ISV=1,NSV)
        WRITE(10,*)        HD(1),(DERI(IOP),IOP=1,NBOPH),(ICENT(ISV),ISV=1,NSV)
      ENDDO
      GOTO 12
!
9003  PRINT 8003, TRIM(INFILE)
      PRINT 8000
!
9000  CLOSE(10)
      PRINT *
      END PROGRAM ASED
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!  LECTURE ET RECOPIE DES PARAMETRES
!
      SUBROUTINE IOPBAS(LUI,LUO)
      use mod_dppr
      use mod_par_tds
      IMPLICIT NONE
      integer          :: LUI,LUO

      real(kind=dppr)  :: PARA,PREC

      integer          :: I,IP
      integer          :: NBOPH

      character(len = NBCLAB+10)  :: CHAINE
      character(len = NBCTIT)     :: TITRE
!
1000  FORMAT(A)
1001  FORMAT(I4,A)
1002  FORMAT(A,E18.11,E14.7)
8000  FORMAT(' !!! ASED   : STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF IN IOPBAS')
!
      DO I=1,4
        READ (LUI,END=2000) TITRE
        WRITE(LUO,1000)     TRIM(TITRE)
      ENDDO
      READ (LUI,END=2000) NBOPH,TITRE
      WRITE(LUO,1001)     NBOPH,TRIM(TITRE)
      DO I=1,2
        READ (LUI,END=2000) TITRE
        WRITE(LUO,1000)     TRIM(TITRE)
      ENDDO
      DO IP=1,NBOPH
        READ (LUI,END=2000) CHAINE,PARA,PREC
        WRITE(LUO,1002)     CHAINE,PARA,PREC
      ENDDO
      RETURN
!
2000  PRINT 8003
      PRINT 8000
      PRINT *
      STOP
!
      END SUBROUTINE IOPBAS
