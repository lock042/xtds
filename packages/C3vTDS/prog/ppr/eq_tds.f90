      PROGRAM EQTDS
!
!  PROGRAMME REALISE A PARTIR DE POSITION_RDN_3.F : (J.M.JOUVARD ET
!  J.P.CHAMPION DU 9 FEVRIER 90) ET DE LDTRO.F : (G.PIERRE DU 16 SEPTEMBRE 88)
!  REALISE LE 16 JUILLET 90 G.PIERRE
!  modifie le 18 novembre 93 et le 11 decembre 95  C. Pierre
!  afin d'obtenir des statistiques par bandes
! REV    JAN 1995 JPC,CW (PARAMETER)
! REV    DEC 2002 VB : EQ. NORM. QUE POUR PARA. FITES !!!
! MODIFIE 05/09 V. BOUDON, A. EL HILALI ---> XY3Z/C3v
!
! ####################################################################
! ##                                                                ##
! ## - COMPARAISON AVEC LES RAIES EXPERIMENTALES                    ##
! ## - CALCUL DES EQUATIONS NORMALES                                ##
! ## - STATISTIQUES                                                 ##
! ##                                                                ##
! ####################################################################
!
! APPEL : eq_tds Pn Pn' JMAX FPARA CLx [prec] [MHz/GHz] [mix N]
! LES OPTIONS PEUVENT ETRE INDIQUEES DANS UN ORDRE QUELCONQUE .
!
! *** LIMITATIONS DU PROGRAMME
!
! NOMBRE MAXIMUM D'OPERATEURS DE L'HAMILTONIEN
!     MXOPH                ! DERSUP, DERINF
!
! NOMBRE MAXIMUM DE RAIES OBSERVEES
!     MXOBS                ! ENINF,DERINF,ENSUP,DERSUP
!
! NOMBRE MAXIMUM DE SYMETRIES ROVIBRATIONNELLES
!     MXSYM                ! SYM,DIMREP
!
! NOMBRE MAXIMUM DE SOUS-NIVEAUX
!     MXSNV            ! NCODNS,NCODNI
!
! NOMBRE MAXIMUM DE bandes vibrationnelles
!     MXNIV            ! CODNS,CODNI
!
      use mod_dppr
      use mod_par_tds
      use mod_com_branch
      use mod_com_const
      use mod_com_fdate
      use mod_com_sy
      use mod_main_eqtds
      IMPLICIT NONE

      real(kind=dppr)  :: CMGHZ
      real(kind=dppr)  :: DDT,DIFF
      real(kind=dppr)  :: ECTPJ,ECUM,ENIN,ENSU
      real(kind=dppr)  :: FCAL,FCAO,FEXP,FOBS
      real(kind=dppr)  :: PARAC,PDIFF,PF1,PF2,PNEG,POIDS,PRECC
      real(kind=dppr)  :: SD2,SDFCAL,SDFEXP,SDFOBS,SDFMOD,SDFMOO
      real(kind=dppr)  :: SDSOBS,SEXP,SOBS,SS0,SS1
      real(kind=dppr)  :: VACOII,VACOIJ,VV

      integer          :: I,IC,ICINF,ICNTCL,ICNTRL,ICOEFU
      integer          :: ICSUP,III,IMEGA,IOBS,IOP,IOPH,IP,IPCBN
      integer          :: ISB,ISV,ISVV
      integer          :: J,JCENT,JI,JINF,JJ,JMAX,JOBS,JOBS0,JOP,JOPH
      integer          :: JS,JSUP
      integer          :: K
      integer          :: LPC,LPCI,LPCMAX
      integer          :: MVAR
      integer          :: NBAND,NBINF,NBNIVI,NBNIVS,NBOM,NBOPC,NBOPHC
      integer          :: NBOBS,NBOPHI,NBOPHS,NBSUP,NCL2,NFIT
      integer          :: NINF,NOBS,NOBS0,NSTAR,NSUP,NSVINF
      integer          :: NSVSUP,NUI,NUNI,NUNS,NUS,NUSVI,NUSVS

      character(len = NBCLAB+10)  :: COPAJ
      character(len = NBCTIT)     :: TITRE
      character(len =  11) ,dimension(2)  :: CARG
      character(len =   1)  :: FAS,FASO
      character(len =   1)  :: ISP
      character(len =   1)  :: PAOBS,PAOBS0
      character(len =   1)  :: SASO
      character(len =   2)  :: BR
      character(len =   2)  :: SYINF,SYOBS,SYOBS0,SYSUP
      character(len =   5)  :: NUO
      character(len =   6)  :: IDENT
      character(len =   8)  :: CTRP  = 'CL_eqtds'
      character(len =  10)  :: OPT
      character(len =  20)  :: CODI,CODS                                                           ! 20 CAR NBVQN NON PRIS EN COMPTE
      character(len = 120)  :: FEDI,FEDS,FPARA,NOM

      logical          :: INTRA = .FALSE.
      logical          :: LERR
      logical          :: REP
!
1001  FORMAT(A)
1002  FORMAT(I4,A)
1003  FORMAT(A,E18.11,E14.7)
2000  FORMAT(' <  ',A)
2001  FORMAT(' >> ',A)
2005  FORMAT(2A,3F11.4,E13.4,2X,A,2(I3,1X,2A,I3,1X))
2006  FORMAT((10X,20(2I4,'%')))
2007  FORMAT(I4,' UPPER VIBRATIONAL SUBLEVEL(S)')
2008  FORMAT(I4,' LOWER VIBRATIONAL SUBLEVEL(S)')
2009  FORMAT(I4,' GREATER VIBRATIONAL PERCENTAGE(S)',/)
2010  FORMAT(/,              &
             'EQ_TDS : ',A)
2011  FORMAT(' EQ_TDS -> J = ',I3,'/',I3)
2012  FORMAT(' EQ_TDS -> NB ASG+ =',I6)
2020  FORMAT(F13.6,2X,2A,2F7.3,F11.3,F13.6,F8.3,I5,1X,2A,I3,F13.6,I3,I4,'%',2X,A,1X,   &
             2(I3,1X,2A,I3,1X)                                                      )
3000  FORMAT(2A,F12.6,1X,A,E11.4,1X,A,F10.6,F6.1,2X,2(I3,1X,2A,I3,1X))
3003  FORMAT(I3,I8,F15.6,6X,I6,4X,3F13.6)
3004  FORMAT(////,   &
             I4,//)
3007  FORMAT('     UPPER LEVEL : ',I3,' OPERATORS',6X,'LOWER LEVEL : ',I3,' OPERATORS')
3009  FORMAT('* POSITION :')
3010  FORMAT('      FEXP       CODE   SDEXP  SDTHEO  EXP-CALC      ',   &
             'FCAL      SDCAL  ( J C    N      E  )SUP         ....',   &
             ' ASSIGNMENT  ....'                                     )
3013  FORMAT(//,                      &
             ' PARTIAL STATISTICS ')
3014  FORMAT(//,                                  &
             I6,' DATA FROM BAND ',A,' <=== ',A)
3015  FORMAT(//,                                                                                        &
             '      NUMBER OF   THEORETICAL   CUMULATIVE      PARTIAL      MEAN       CUMULATIVE', /,   &
             'Jsup    DATA       PRECISION    NB OF DATA     STD. DEV.     DEV.       STD. DEV.' ,// )
3017  FORMAT(////,   &
             I4   )
3018  FORMAT(I1)
8000  FORMAT(' !!! EQ_TDS : STOP ON ERROR')
8001  FORMAT(' !!! ERROR OPENING CONTROL FILE')
8002  FORMAT(' !!! INCOMPLETE CONTROL FILE')
8004  FORMAT(' !!! BAD OPTION IN CONTROL FILE')
8100  FORMAT(' !!! WRONG NUMBER OF LOWER BANDS')
8101  FORMAT(' !!! WRONG NUMBER OF UPPER BANDS')
8104  FORMAT(' !!! UPPER LEVEL FILE => UNEXPECTED EOF')
8105  FORMAT(' !!! LOWER LEVEL FILE => UNEXPECTED EOF')
8106  FORMAT(' !!! ERROR OPENING EXPERIMENTAL FILE: assignments.t')
8107  FORMAT(' !!! ERROR OPENING UPPER EIGENVAL. AND VECT. FILE: ',A)
8108  FORMAT(' !!! ERROR OPENING LOWER EIGENVAL. AND VECT. FILE: ',A)
8109  FORMAT(' !!! ERROR OPENING para_variance.t FILE')
8110  FORMAT(' !!! ERROR OPENING statistics.t FILE')
8111  FORMAT(' !!! ERROR OPENING normal_eq.t FILE')
8112  FORMAT(' !!! ERROR OPENING prediction_mix.t FILE')
8113  FORMAT(' !!! ERROR OPENING prediction.t FILE')
8124  FORMAT(' !!! NB OF OPERATORS MUST BE EQUAL IN'   ,/,   &
             '         PARAMETER CONTROL FILE:',I8,2X,A,/,   &
             '     AND PARAMETER         FILE:',I8,2X,A   )
8125  FORMAT(' !!! BAD CONTROL VALUE FOR A PARAMETER WITH DEPENDANCY LINE',/,   &
             '     MUST BE 0 OR 1: ',A                                       )
8200  FORMAT(' WARNING: DUPLICATED TRANSITION',3X,2(I3,1X,2A,I3,1X))
8201  FORMAT(' !!! NOT FOUND: LOWER LEVEL OF TRANSITION  ',2(I3,1X,2A,I3,1X))
8202  FORMAT(' !!! NOT FOUND: UPPER LEVEL OF TRANSITION  ',2(I3,1X,2A,I3,1X))
!
! ARRAY INITIAL ALLOCATION
!
      CALL ALLOC_ALL
!
      MVAR   = 0                                                                                   ! INITIALISATION PAR DEFAUT
      NBNIVS = 0                                                                                   ! INITIALISATION PAR DEFAUT
      NBNIVI = 0                                                                                   ! INITIALISATION PAR DEFAUT
      NUSVS  = 0                                                                                   ! INITIALISATION PAR DEFAUT
      NUSVI  = 0                                                                                   ! INITIALISATION PAR DEFAUT
      OPEN(10,FILE=CTRLF,ERR=9998,STATUS='OLD')
      READ(10,1001,END=9997) FDATE
      PRINT 2010,            FDATE
      DO I=1,2                                                                                     ! ARGUMENTS OBLIGATOIRES
        READ(10,1001,END=9997) CARG(I)
      ENDDO
      READ(10,*,END=9997) JMAX
      READ(10,1001,END=9997) FPARA
      READ(10,1001,END=9997) NOM                                                                   ! FICHIER DE CONTROLE DES PARA
      PRINT 2000,  TRIM(NOM)
      OPEN(12,FILE=TRIM(NOM),STATUS='OLD')
      READ(12,3004) NBOPC
      DO WHILE( NBOPC .GT. MXOPH )
        CALL RESIZE_MXOPH
      ENDDO
      OPEN(13,FILE=FPARA,STATUS='OLD')
      READ(13,3017) NBOM
      IF( NBOM .NE. NBOPC ) THEN
        PRINT 8124, NBOPC,TRIM(NOM),NBOM,TRIM(FPARA)
        GOTO  9999
      ENDIF
      CLOSE(13)
      ICNTCL = 0                                                                                   ! nb reduit de paras fittes
      NCL2   = NBCLAB+9                                                                            ! Frmd included
      DO IPCBN=1,NBOPC                                                                             ! POUR CHAQUE PARA
!  lecture des caracteristiques du parametre
        READ(12,1001) COPAJ
        READ(COPAJ(NCL2:NCL2),3018) ICNTRL                                                         ! ETAT DU PARA
        ISFIT(IPCBN) = ICNTRL
!  lecture d'une eventuelle unique ligne de dependances
        NSTAR = 0
        READ(12,1001,END=300) TITRE
        NSTAR = INDEX(TRIM(TITRE),'*')
        IF( NSTAR .LT. 1 ) THEN                                                                    ! pas une ligne de dependances
          BACKSPACE(12)                                                                            ! se repositionner pour le parametre suivant
        ENDIF
!
300     CONTINUE                                                                                   ! EOF
! traitement des differents cas
        IF( NSTAR .LT. 1 ) THEN                                                                    ! pas une ligne de dependances
          IF( ICNTRL .GT. 1 ) THEN                                                                 ! SI PARA FITTE ...
            ICNTCL = ICNTCL+1
            IF( ICNTCL .GT. MXOPH ) CALL RESIZE_MXOPH
            IXCL(ICNTCL) = IPCBN                                                                   ! ... STOCKER SON NUMERO ...
          ENDIF
        ELSE                                                                                       ! ligne de dependances
          IF( ICNTRL .LE. 1 ) THEN                                                                 ! SI PARA FITTE ...
            ISFIT(IPCBN) = 5                                                                       ! marque les dependances
            ICNTCL = ICNTCL+1
            IF( ICNTCL .GT. MXOPH ) CALL RESIZE_MXOPH
            IXCL(ICNTCL) = IPCBN
          ELSE
            PRINT 8125, TRIM(COPAJ)                                                                ! autre que 0,1 : interdit
            GOTO  9999
          ENDIF
        ENDIF
      ENDDO
! ecriture du fichier CTRP
! fichier de controle des paramateres non fixes a 0.
      PRINT 2001,  TRIM(CTRP)
      OPEN(15,FILE=TRIM(CTRP),STATUS='UNKNOWN')                                                    ! FICHIER DE CONTROLE REDUIT
! CTRP : codes de controle > 1
      REWIND(12)
      READ(12,3004) III
      WRITE(15,3004) ICNTCL
      DO IPCBN=1,NBOPC
        READ(12,1001)    TITRE
        IF( ISFIT(IPCBN) .GT. 1 ) THEN                                                             ! SI PARA FITTE ...
          WRITE(15,1001) TITRE                                                                     ! ... LE RECOPIER
          IF( ISFIT(IPCBN) .EQ. 5 ) THEN                                                           ! ligne de dependances
            READ (12,1001) TITRE
            WRITE(15,1001) TITRE
          ENDIF
        ENDIF
      ENDDO
! CTRP : codes de controle = 0
      REWIND(12)
      READ(12,3004) III
      WRITE(15,1001)                                                                               ! ligne vide, de separation
      DO IPCBN=1,NBOPC
        READ(12,1001)    COPAJ
        IF    ( ISFIT(IPCBN) .EQ. 0 ) THEN                                                         ! SI PARA FIXE ...
          WRITE(15,1001) COPAJ                                                                     ! ... LE RECOPIER
        ELSEIF( ISFIT(IPCBN) .EQ. 5 ) THEN                                                         ! ligne de dependances
          READ(12,1001)                                                                            ! la lire
        ENDIF
      ENDDO
! CTRP : codes de controle = 1
      REWIND(12)
      READ(12,3004) III
      WRITE(15,1001)                                                                               ! ligne vide, de separation
      DO IPCBN=1,NBOPC
        READ(12,1001)    COPAJ
        IF    ( ISFIT(IPCBN) .EQ. 1 ) THEN                                                         ! SI PARA FIXE ...
          WRITE(15,1001) COPAJ                                                                     ! ... LE RECOPIER
        ELSEIF( ISFIT(IPCBN) .EQ. 5 ) THEN                                                         ! ligne de dependances
          READ(12,1001)                                                                            ! la lire
        ENDIF
      ENDDO
      CLOSE(15)
      CLOSE(12)
!
!      ---> MVAR  =1 SI ON VEUT CALCULER LA PRECISION SUR LES FREQUENCES
!                      ATTENTION : AJOUTER FICHIER VARIANCE/COVARIANCE
!      ---> ICOEFU=1 CONVERTIT LES DONNEES DE MHZ EN CM-1
!      ---> ICOEFU=2 CONVERTIT LES DONNEES DE GHZ EN CM-1
!      ---> LPCMAX = LES LPC PLUS GRANDS POURCENTAGES VIBRATIONNELS
!
      ICOEFU = 0
      LPCMAX = 1
!
19    READ(10,1001,END=21) OPT
      IF    ( OPT .EQ. 'prec' ) THEN
        INQUIRE(FILE='para_variance.t',EXIST=REP)
        IF( REP ) MVAR = 1
      ELSEIF( OPT .EQ. 'MHz'  ) THEN
        IF( ICOEFU .NE. 0 ) GOTO 9997
        ICOEFU = 1
      ELSEIF( OPT .EQ. 'GHz'  ) THEN
        IF( ICOEFU .NE. 0 ) GOTO 9997
        ICOEFU = 2
      ELSEIF( OPT .EQ. 'mix'  ) THEN
        READ(10,*,END=9997) LPCMAX
      ELSE
        GOTO 9996                                                                                  ! OPTION NON RECONNUE
      ENDIF
      GOTO 19
!
21    CLOSE(10)
      FEDS = 'ED_'//TRIM(CARG(1))//'_'
      FEDI = 'ED_'//TRIM(CARG(2))//'_'
      IF( FEDS .EQ. FEDI ) INTRA = .TRUE.
!
!   FICHIER DE PARAMETRES
!
      PRINT 2000,  TRIM(FPARA)
      OPEN(70,FILE=TRIM(FPARA),STATUS='OLD')
!
!   FICHIER D'ENERGIES ET DERIVEES SUP
!
      PRINT 2000,           TRIM(FEDS)
      OPEN(50,ERR=4050,FILE=TRIM(FEDS),STATUS='OLD',FORM='UNFORMATTED')
      OPEN(10,STATUS='SCRATCH')
      READ(50)
      READ(50)
      READ(50)
      READ(50)
      READ(50)     I,TITRE
      WRITE(10,1001) TITRE
      REWIND(50)
      REWIND(10)
      READ(10,*) PNEG,IMEGA
      CLOSE(10)
!
!   FICHIER D'ENERGIES ET DERIVEES INF
!
      PRINT 2000, TRIM(FEDI)
      IF( INTRA ) THEN
        OPEN(51,STATUS='SCRATCH',FORM='UNFORMATTED')
        CALL IOPBA0(50,0)
        READ(50) NSVINF
        DO I=1,NSVINF
          READ(50)
        ENDDO
        READ (50) NBOPHI
        WRITE(51) NBOPHI
        DO WHILE( NBOPHI .GT. MXOPH )
          CALL RESIZE_MXOPH
        ENDDO
!
4001    READ(50,END=4003) JINF,ICINF,NBINF
        IF( JINF .GT. JMAX+2 ) GOTO 4003
        WRITE(51)         JINF,ICINF,NBINF
        DO IP=1,NBINF
          READ (50,END=4160) ENIN,(DERIN(IOP),IOP=1,NBOPHI),(ICENT(ISV),ISV=1,NSVINF)
          WRITE(51)          ENIN,(DERIN(IOP),IOP=1,NBOPHI),(ICENT(ISV),ISV=1,NSVINF)
        ENDDO
        GOTO 4001
!
4003    REWIND(50)
        REWIND(51)
      ELSE
        OPEN(51,ERR=4051,FILE=FEDI,STATUS='OLD',FORM='UNFORMATTED')
      ENDIF
!
!   FICHIER D'OBSERVEES
!
      PRINT 2000,           'assignments.t'
      OPEN(90,ERR=4090,FILE='assignments.t',STATUS='OLD')
!
!   FICHIER DE VARIANCE/COVARIANCE
!
      IF( MVAR .EQ. 1 ) THEN
        PRINT 2000,           'para_variance.t'
        OPEN(94,ERR=4094,FILE='para_variance.t',STATUS='OLD',FORM='UNFORMATTED')
      ENDIF
!
!   FICHIERS DE SORTIE :
!   TRANSITIONS OBSERVEES - CALCULEES
!
      PRINT 2001,           'prediction.t'
      OPEN(91,ERR=4080,FILE='prediction.t',STATUS='UNKNOWN')
      PRINT 2001,           'prediction_mix.t'
      OPEN(95,ERR=4091,FILE='prediction_mix.t',STATUS='UNKNOWN')
!
!   EQUATION NORMALE : Y=AX
!
      PRINT 2001,           'normal_eq.t'
      OPEN(92,ERR=4092,FILE='normal_eq.t',FORM='UNFORMATTED',STATUS='UNKNOWN')
!
!   STATISTIQUES
!
      PRINT 2001,           'statistics.t'
      OPEN(93,ERR=4093,FILE='statistics.t',STATUS='UNKNOWN')
!
!   INITIALISATION
!
      NFIT = 0
      SS0  = 0.D0
      SS1  = 0.D0
      DO J=0,JMAX
        TNEGL(J) = 0.D0
        DO ISV=0,MXNIV
          DO ISVV=0,MXNIV
            NFJ(J,ISV,ISVV)   = 0
            SMJ(J,ISV,ISVV)   = 0.D0
            SPOI(J,ISV,ISVV)  = 0.D0
            SPROR(J,ISV,ISVV) = 0.D0
          ENDDO
        ENDDO
      ENDDO
      DO IOPH=1,ICNTCL
        YS(IOPH) = 0.D0
        DO JOPH=1,ICNTCL
          AS(JOPH,IOPH) = 0.D0
        ENDDO
      ENDDO
      CMGHZ = 1.D0
      IF( ICOEFU .EQ. 1 ) CMGHZ = CMHZ
      IF( ICOEFU .EQ. 2 ) CMGHZ = CGHZ
!
! ******************************************************
! ***   LECTURE , RECOPIE ET POSITIONNEMENT           ***
! ******************************************************
!
!   LECTURE DES PARAMETRES DE H
!
      WRITE(91,1001)
      CALL IOBA0B(50,91,92,ICNTCL)
      IF( MVAR .EQ. 1 ) CALL IOPBA0(94,0)
      WRITE(91,1001)
      READ(70,3004) NBOPHC
      DO I=1,NBOPHC
        READ(70,1001)
      ENDDO
      DO I=1,4
        READ (70,1001) TITRE
        WRITE(92)      TITRE
        WRITE(91,1001) TRIM(TITRE)
      ENDDO
      READ (70,1002) NBOPHC,TITRE
      WRITE(92)      NBOPHC,TITRE
      WRITE(91,1002) NBOPHC,TRIM(TITRE)
      DO I=1,2
        READ (70,1001) TITRE
        WRITE(92)      TITRE
        WRITE(91,1001) TRIM(TITRE)
      ENDDO
      DO I=1,NBOPHC
        READ (70,1003) COPAJ,PARAC,PRECC
        WRITE(92)      COPAJ,PARAC,PRECC
        WRITE(91,1003) COPAJ,PARAC,PRECC
      ENDDO
      CLOSE(70)
      WRITE(91,1001)
!
!   NSVSUP SOUS-NIVEAUX VIBRATIONNELS SUP
!
      READ (50)      NSVSUP,TITRE
      WRITE(91,2007) NSVSUP
      WRITE(93,2007) NSVSUP
      WRITE(95,2007) NSVSUP
      DO WHILE( NSVSUP .GT. MXSNV )
        CALL RESIZE_MXSNV
      ENDDO
      LPCMAX = MIN(NSVSUP,LPCMAX)                                                                  ! reduce LPCMAX if needed
E111: DO ISV=1,NSVSUP
        READ (50)      TITRE(:80)
        WRITE(91,1001) TRIM(TITRE)
        WRITE(93,1001) TRIM(TITRE)
        WRITE(95,1001) TRIM(TITRE)
        CODS(1:1) = TITRE(13:13)
        CODS(2:2) = TITRE(23:23)
        CODS(3:3) = TITRE(33:33)
        CODS(4:4) = TITRE(43:43)
        CODS(5:5) = TITRE(53:53)
        CODS(6:6) = TITRE(67:67)
        IF( ISV .EQ. 1 ) THEN
          NBNIVS           = 1
          CODNS(1)(:NBVQN) = CODS(:NBVQN)
          NCODNS(1)        = 1
          CYCLE E111
        ENDIF
        IF( CODS(:NBVQN) .EQ. CODNS(NBNIVS)(:NBVQN) ) THEN
          NCODNS(ISV) = NBNIVS
        ELSE
          NBNIVS = NBNIVS+1
          IF( NBNIVS .GT. MXNIV ) CALL RESIZE_MXNIV
          CODNS(NBNIVS)(:NBVQN) = CODS(:NBVQN)
          NCODNS(ISV)           = NBNIVS
        ENDIF
      ENDDO E111
      WRITE(91,2009) LPCMAX
      WRITE(93,2009) LPCMAX
      WRITE(95,2009) LPCMAX
!
!   NSVINF SOUS-NIVEAUX VIBRATIONNELS INF
!
      IF( .NOT. INTRA ) THEN
        CALL IOPBA0(51,0)
        READ (51)      NSVINF,TITRE
        WRITE(91,2008) NSVINF
        DO WHILE( NSVINF .GT. MXSNV )
          CALL RESIZE_MXSNV
        ENDDO
E121:   DO ISV=1,NSVINF
          READ (51)      TITRE(:80)
          WRITE(91,1001) TRIM(TITRE)
          CODI(1:1) = TITRE(13:13)
          CODI(2:2) = TITRE(23:23)
          CODI(3:3) = TITRE(33:33)
          CODI(4:4) = TITRE(43:43)
          CODI(5:5) = TITRE(53:53)
          CODI(6:6) = TITRE(67:67)
          IF( ISV .EQ. 1 ) THEN
            NBNIVI           = 1
            CODNI(1)(:NBVQN) = CODI(:NBVQN)
            NCODNI(1)        = 1
            CYCLE E121
          ENDIF
          IF( CODI(:NBVQN) .EQ. CODNI(NBNIVI)(:NBVQN) ) THEN
            NCODNI(ISV) = NBNIVI
          ELSE
            NBNIVI = NBNIVI+1
            IF( NBNIVI .GT. MXNIV ) CALL RESIZE_MXNIV
            CODNI(NBNIVI)(:NBVQN) = CODI(:NBVQN)
            NCODNI(ISV)           = NBNIVI
          ENDIF
        ENDDO E121
        WRITE(93,1001)
      ELSE
!
!     cas particulier de la bande inf = sup
!
        NBNIVI = NBNIVS
        DO WHILE( NBNIVI .GT. MXNIV )
          CALL RESIZE_MXNIV
        ENDDO
        DO ISV=1,NSVSUP
          NCODNI(ISV) = NCODNS(ISV)
        ENDDO
        DO ISB=1,NBNIVI
          CODNI(ISB)(:NBVQN) = CODNS(ISB)(:NBVQN)
        ENDDO
      ENDIF
      CALL DEBUG( 'EQTDS  => MXNIV=',NBNIVI)
!
!   NBOPHS : NOMBRE D'OPERATEURS DU NIVEAU SUP
!   NBOPHI : NOMBRE D'OPERATEURS DU NIVEAU INF
!
      WRITE(91,1001)
      READ(50) NBOPHS
      READ(51) NBOPHI
      WRITE(91,3007) NBOPHS,NBOPHI
      DO WHILE( NBOPHS .GT. MXOPH .OR.    &
                NBOPHI .GT. MXOPH      )
        CALL RESIZE_MXOPH
      ENDDO
      WRITE(91,1001) FDATE
      WRITE(91,1001)
      WRITE(93,1001) FDATE
      WRITE(93,1001)
      WRITE(91,3009)
      WRITE(91,3010)
      WRITE(91,1001)
!
! ***************************************************
! *** LECTURE DU FICHIER DE RAIES EXPERIMENTALES  ***
! ***************************************************
!
      IOBS = 0
!
5     CONTINUE
      READ(90,3000,ERR=5,END=324) ISP,NUO,FOBS,FASO,SOBS,SASO,SDFOBS,SDSOBS,  &
                                  JOBS0,SYOBS0,PAOBS0,NOBS0,                  &
                                  JOBS,SYOBS,PAOBS,NOBS
      IF( JOBS .GT. JMAX ) GOTO 5
      IOBS = IOBS+1
      IF( IOBS .GT. MXOBS ) CALL RESIZE_MXOBS
      ISPT(IOBS)   = ISP
      NUOT(IOBS)   = NUO
      FOBST(IOBS)  = FOBS
      FASOT(IOBS)  = FASO
      SOBST(IOBS)  = SOBS
      SASOT(IOBS)  = SASO
      SDFOBT(IOBS) = SDFOBS
      SDSOBT(IOBS) = SDSOBS
      JOBS0T(IOBS) = JOBS0
      SYOB0T(IOBS) = SYOBS0
      NOBS0T(IOBS) = NOBS0
      JOBST(IOBS)  = JOBS
      SYOBST(IOBS) = SYOBS
      NOBST(IOBS)  = NOBS
      GOTO 5
!
324   CALL DEBUG( 'EQTDS  => MXOBS=',IOBS)
      NBOBS = IOBS
!
!  tester les doublons dans les transitions
!
      DO I=2,NBOBS
        IF( JOBST(I)  .EQ. JOBST(I-1)  .AND.         &
            SYOBST(I) .EQ. SYOBST(I-1) .AND.         &
            NOBST(I)  .EQ. NOBST(I-1)  .AND.         &
            JOBS0T(I) .EQ. JOBS0T(I-1) .AND.         &
            SYOB0T(I) .EQ. SYOB0T(I-1) .AND.         &
            NOBS0T(I) .EQ. NOBS0T(I-1)       ) THEN
          PRINT 8200, JOBS0T(I),SYOB0T(I),PARGEN,NOBS0T(I),  &
                      JOBST(I) ,SYOBST(I),PARGEN,NOBST(I)
        ENDIF
      ENDDO
!
! ******************************************
! *** LECTURE DU FICHIER D'ENERGIES INF  ***
! ******************************************
!
      DO I=1,NBOBS
        FOUND(I) = .FALSE.
      ENDDO
!
6     READ(51,END=8) JINF,ICINF,NBINF
      DO IP=1,NBINF
        READ(51,END=4160) ENIN,(DERIN(IOP),IOP=1,NBOPHI),(ICENT(ISV),ISV=1,NSVINF)
        JCENT         = ICENT(1)
        NBAND         = NCODNI(1)
        JCENTT(NBAND) = JCENT
        DO IC=2,NSVINF
          IF( NCODNI(IC) .EQ. NCODNI(IC-1) ) THEN
            JCENT = JCENT+ICENT(IC)
          ELSE
            JCENTT(NBAND) = JCENT
            NBAND         = NCODNI(IC)
            JCENT         = ICENT(IC)
          ENDIF
        ENDDO
        JCENTT(NBAND) = JCENT
!
! ********************
! *** ENERGIE MAXIMUM
! ********************
!
        IF( NBAND .NE. NBNIVI ) GOTO 4165
        JCENT = 0
E214:   DO IC=1,NBAND
          IF( JCENT .GE. JCENTT(IC) ) CYCLE E214
          JCENT = JCENTT(IC)
          NUSVI = IC
        ENDDO E214
!
! *****************************************************************
! *** EST CE UN NIVEAU CORRESPONDANT A UNE TRANSITION OBSERVEE? ***
! *****************************************************************
!
        DO I=1,NBOBS
          IF( JINF       .EQ. JOBS0T(I) .AND.         &
              SYM(ICINF) .EQ. SYOB0T(I) .AND.         &
              IP         .EQ. NOBS0T(I)       ) THEN
            FOUND(I)  = .TRUE.
            ENINF(I)  = ENIN
            IPCINF(I) = NUSVI
            DO IOP=1,NBOPHI
              DERINF(I,IOP) = DERIN(IOP)
            ENDDO
          ENDIF
        ENDDO
      ENDDO
      GOTO 6
!
8     CONTINUE
!
! tester si le niveau inf de chaque transition a ete trouve
!
      LERR = .FALSE.
      DO I=1,NBOBS
        IF( .NOT. FOUND(I) ) THEN
          LERR = .TRUE.
          PRINT 8201, JOBS0T(I),SYOB0T(I),PARGEN,NOBS0T(I),  &
                      JOBST(I),SYOBST(I),PARGEN,NOBST(I)
        ENDIF
      ENDDO
      IF( LERR ) GOTO 9999
!
! ******************************************
! *** LECTURE DU FICHIER D'ENERGIES SUP  ***
! ******************************************
!
      DO I=1,NBOBS
        FOUND(I) = .FALSE.
      ENDDO
!
66    READ(50,END=88) JSUP,ICSUP,NBSUP
      IF( JSUP .GT. JMAX ) GOTO 88
E77:  DO IP=1,NBSUP
        READ(50,END=4161) ENSU,(DERSU(IOP),IOP=1,NBOPHS),(ICENT(ISV),ISV=1,NSVSUP)
!
! *********************************************
! *** CUMUL DES ENERGIES POUR UNE BANDE DONNEE
! *********************************************
!
        JCENT         = ICENT(1)
        NBAND         = NCODNS(1)
        JCENTT(NBAND) = JCENT
        DO IC=2,NSVSUP
          IF( NCODNS(IC) .EQ. NCODNS(IC-1) ) THEN
            JCENT = JCENT+ICENT(IC)
          ELSE
            JCENTT(NBAND) = JCENT
            NBAND         = NCODNS(IC)
            JCENT         = ICENT(IC)
          ENDIF
        ENDDO
        JCENTT(NBAND) = JCENT
!
! ********************
! *** ENERGIE MAXIMUM
! ********************
!
        IF( NBAND .NE. NBNIVS ) GOTO 4164
        JCENT = 0
E23:    DO IC=1,NBAND
          IF( JCENT .GE. JCENTT(IC) ) CYCLE E23
          JCENT = JCENTT(IC)
          NUSVS = IC
        ENDDO E23
!
! *****************************************************************
! *** EST CE UN NIVEAU CORRESPONDANT A UNE TRANSITION OBSERVEE? ***
! *****************************************************************
!
        DO I=1,NBOBS
          IF( JSUP+2     .LT. JOBST(I)        ) CYCLE E77
          IF( JSUP       .EQ. JOBST(I)  .AND.         &
              SYM(ICSUP) .EQ. SYOBST(I) .AND.         &
              IP         .EQ. NOBST(I)        ) THEN
            FOUND(I)  = .TRUE.
            ENSUP(I)  = ENSU
            IPCSUP(I) = NUSVS
            DO LPC=1,LPCMAX
              IPCSN(I,LPC)  = 0
              NUSVSU(I,LPC) = 0
E702:         DO IC=1,NSVSUP
                DO LPCI=1,LPC-1
                  IF( IC .EQ. NUSVSU(I,LPCI) ) CYCLE E702
                ENDDO
                IF( IPCSN(I,LPC) .GT. ICENT(IC) ) CYCLE E702
                IPCSN(I,LPC)  = ICENT(IC)
                NUSVSU(I,LPC) = IC
              ENDDO E702
            ENDDO
            DO IOP=1,NBOPHS
              DERSUP(I,IOP) = DERSU(IOP)
            ENDDO
          ENDIF
        ENDDO
      ENDDO E77
      GOTO 66
!
88    PRINT 2011, JSUP,JMAX
!
! tester si le niveau sup de chaque transition a ete trouve
!
      LERR = .FALSE.
      DO I=1,NBOBS
        IF( .NOT. FOUND(I) ) THEN
          LERR = .TRUE.
          PRINT 8202, JOBS0T(I),SYOB0T(I),PARGEN,NOBS0T(I),  &
                      JOBST(I) ,SYOBST(I),PARGEN,NOBST(I)
        ENDIF
      ENDDO
      IF( LERR ) GOTO 9999
!
! ******************************************************
! ***      LECTURE ET STOCKAGE DE LA MATRICE         ***
! ***   DE VARIANCE/COVARIANCE DES PARAMETRES DE H   ***
! ******************************************************
!
      IF( MVAR .EQ. 1 ) THEN
        DO J=1,ICNTCL
          READ(94) (VACO(K,J),K=J,ICNTCL)
        ENDDO
      ENDIF
!
!
! ****************************************
! *** CALCUL DES TRANSITIONS OBSERVEES ***
! ****************************************
!
E17:  DO IOBS=1,NBOBS
        JI     = JOBS0T(IOBS)
        NINF   = NOBS0T(IOBS)
        SYINF  = SYOB0T(IOBS)
        JS     = JOBST(IOBS)
        NSUP   = NOBST(IOBS)
        SYSUP  = SYOBST(IOBS)
        FOBS   = FOBST(IOBS)
        SDFOBS = SDFOBT(IOBS)
        FASO   = FASOT(IOBS)
        SASO   = SASOT(IOBS)
        ISP    = ISPT(IOBS)
        NUO    = NUOT(IOBS)
        BR     = BRANCH(JS-JI+MXBRA-2)
        FCAL   = ENSUP(IOBS)-ENINF(IOBS)
!
! ***************************
! *** CALCUL DES DERIVEES ***
! ***************************
!
        DO IOP=1,NBOPHI
          DERV(IOP) = DERSUP(IOBS,IOP)-DERINF(IOBS,IOP)
        ENDDO
        DO IOP=NBOPHI+1,NBOPHS
          DERV(IOP) = DERSUP(IOBS,IOP)
        ENDDO
!
! ********************************************************
! *** CALCUL DE L'ECART TYPE DE LA TRANSITION THEORIQUE ***
! ********************************************************
!
        IF( MVAR .EQ. 1 ) THEN
          PF1 = 0.D0
          PF2 = 0.D0
E773:     DO IOP=1,ICNTCL
            DDT    = DERV(IXCL(IOP))
            VACOII = VACO(IOP,IOP)
            PF1    = PF1+DDT*DDT*VACOII
            IF( IOP .EQ. ICNTCL ) CYCLE E773
            DO JOP=IOP+1,ICNTCL
              VACOIJ = VACO(JOP,IOP)
              PF2    = PF2+DDT*DERV(IXCL(JOP))*VACOIJ
            ENDDO
          ENDDO E773
          SDFCAL = PF1+PF2*2.D0
          SDFCAL = SQRT(SDFCAL)
        ELSE
          SDFCAL = 0.D0
        ENDIF
        SDFMOD = 1.D0
        IF( IMEGA .NE. 0 ) SDFMOD = DBLE(JS)**IMEGA
        SDFMOD = PNEG*SDFMOD
        SDFMOO = SDFMOD*1.D+3*CMGHZ
        SDFCAL = SDFCAL*1.D+3*CMGHZ
        FCAO   = FCAL*CMGHZ
!
! *******************************************
! *** RECHERCHE DES TRANSITIONS OBSERVEES ***
! *******************************************
!
!   INITIALISATION
!
        FEXP   = 0.D0
        SEXP   = 0.D0
        SDFEXP = 0.D0
        IDENT  = '      '
        FAS    = ' '
        FOBS   = FOBS/CMGHZ                                                                        ! -> CM-1
        SDFOBS = SDFOBS/CMGHZ                                                                      ! -> CM-1
!
!   TRANSITION OBSERVEE
!
        IDENT  = ISP(1:1)//NUO(1:5)
        FEXP   = FOBS
        SDFEXP = SDFOBS
        IF( FOBS .EQ. 0.D0 ) SDFEXP = 0.D0
        IF( FASO .NE. '+'  ) SDFEXP = 0.D0
        FAS  = FASO
        DIFF = FEXP-FCAL
!
! ********************************************
! ***    CALCUL DES EQUATIONS NORMALES     ***
! ********************************************
!
        SD2 = SDFEXP*SDFEXP+SDFMOD*SDFMOD
        IF( SD2    .LE. 1.D-16 .OR.             &
            SDFEXP .EQ. 0.D0        ) GOTO 326
        POIDS = 1.D0/SD2
        NUS   = IPCSUP(IOBS)
        NUI   = IPCINF(IOBS)
        DO WHILE( NUS .GT. MXNIV .OR.    &
                  NUI .GT. MXNIV      )
          CALL RESIZE_MXNIV
        ENDDO
!
!   STATISTIQUES
!
        NFIT              = NFIT+1
        PDIFF             = POIDS*DIFF*DIFF
        SS0               = SS0+POIDS
        SS1               = SS1+PDIFF
        NFJ(JS,NUS,NUI)   = NFJ(JS,NUS,NUI)+1
        NFJ(JS,0,0)       = NFJ(JS,0,0)+1
        SMJ(JS,NUS,NUI)   = SMJ(JS,NUS,NUI)+POIDS*DIFF
        SMJ(JS,0,0)       = SMJ(JS,0,0)+POIDS*DIFF
        SPOI(JS,NUS,NUI)  = SPOI(JS,NUS,NUI)+POIDS
        SPOI(JS,0,0)      = SPOI(JS,0,0)+POIDS
        SPROR(JS,NUS,NUI) = SPROR(JS,NUS,NUI)+PDIFF
        SPROR(JS,0,0)     = SPROR(JS,0,0)+PDIFF
!
!   EQUATIONS NORMALES
!
        DO I=1,ICNTCL
          VV    = POIDS*DERV(IXCL(I))
          YS(I) = YS(I)+DIFF*VV
          DO K=I,ICNTCL
            AS(K,I) = AS(K,I)+VV*DERV(IXCL(K))
          ENDDO
        ENDDO
!
! ********************************************
! ***  ECRITURE DANS LE FICHIER DE SORTIE  ***
! ********************************************
!
326     IF( ICOEFU .NE. 0 ) THEN
          FEXP   = FEXP*CMGHZ
          SDFEXP = SDFEXP*CMGHZ
          DIFF   = DIFF*CMGHZ
        ENDIF
        DIFF   = DIFF*1.D+3
        SDFEXP = SDFEXP*1.D+3
        WRITE(91,2020) FEXP,FAS,IDENT,SDFEXP,SDFMOO,DIFF,FCAO,SDFCAL,  &
                       JS,SYSUP,PARGEN,NSUP,                           &
                       ENSUP(IOBS),NUSVSU(IOBS,1),IPCSN(IOBS,1),BR,    &
                       JI,SYINF,PARGEN,NINF,                           &
                       JS,SYSUP,PARGEN,NSUP
        WRITE(95,2005) ISP,FAS,FEXP,FCAO,DIFF,SEXP,BR,  &
                       JI,SYINF,PARGEN,NINF,            &
                       JS,SYSUP,PARGEN,NSUP
        WRITE(95,2006) (NUSVSU(IOBS,JJ),IPCSN(IOBS,JJ),JJ=1,LPCMAX)
      ENDDO E17
!
!
! ********************************************
! ***      STOCKAGE DE AS ET YS            ***
! ********************************************
!
      WRITE(92) NFIT,JMAX,SS0,SS1
      PRINT 2012, NFIT
      WRITE(92) (YS(I),I=1,ICNTCL)
      DO I=1,ICNTCL
        WRITE(92) (AS(K,I),K=I,ICNTCL)
      ENDDO
!
!
! ********************************************
! ***   STATISTIQUES                       ***
! ********************************************
!
! par bande
!
      WRITE(93,3013)
      DO NUNS=1,NBNIVS
E115:   DO NUNI=1,NBNIVI
          DO J=0,JMAX
            NCU(J)  = NFJ(J,NUNS,NUNI)
            SPCU(J) = SPOI(J,NUNS,NUNI)
            ECU(J)  = SPROR(J,NUNS,NUNI)
          ENDDO
          DO J=1,JMAX
            NCU(J)  = NCU(J-1)+NCU(J)
            SPCU(J) = SPCU(J-1)+SPCU(J)
            ECU(J)  = ECU(J-1)+ECU(J)
          ENDDO
          IF( NCU(JMAX) .EQ. 0 ) CYCLE E115
          WRITE(93,3014) NCU(JMAX),CODNS(NUNS)(:NBVQN),CODNI(NUNI)(:NBVQN)
          WRITE(93,3015)
E104:     DO J=0,JMAX
            IF( NFJ(J,NUNS,NUNI) .EQ. 0 ) CYCLE E104
            SMJ(J,NUNS,NUNI) = SMJ(J,NUNS,NUNI)/SPOI(J,NUNS,NUNI)*CMGHZ
            ECTPJ            = SQRT(SPROR(J,NUNS,NUNI)/SPOI(J,NUNS,NUNI))*CMGHZ
            ECUM             = SQRT(ECU(J)/SPCU(J))*CMGHZ
            TNEGL(J)         = 1.D0
            IF( IMEGA .NE. 0 ) TNEGL(J) = DBLE(J)**IMEGA
            TNEGL(J) = PNEG*TNEGL(J)*CMGHZ
            WRITE(93,3003) J,NFJ(J,NUNS,NUNI),TNEGL(J),NCU(J),ECTPJ,SMJ(J,NUNS,NUNI),ECUM
          ENDDO E104
        ENDDO E115
      ENDDO
      GOTO 9000
!
4080  PRINT 8113
      GOTO  9999
4091  PRINT 8112
      GOTO  9999
4092  PRINT 8111
      GOTO  9999
4093  PRINT 8110
      GOTO  9999
4094  PRINT 8109
      GOTO  9999
4051  PRINT 8108, FEDI
      GOTO  9999
4050  PRINT 8107, FEDS
      GOTO  9999
4090  PRINT 8106
      GOTO  9999
4160  PRINT 8105
      GOTO  9999
4161  PRINT 8104
      GOTO  9999
4164  PRINT 8101
      GOTO  9999
4165  PRINT 8100
      GOTO  9999
9996  PRINT 8004
      GOTO  9999
9997  PRINT 8002
      GOTO  9999
9998  PRINT 8001
9999  PRINT 8000
!
9000  CLOSE(50)
      CLOSE(51)
      CLOSE(90)
      CLOSE(91)
      CLOSE(92)
      CLOSE(93)
      IF( MVAR .EQ. 1 ) CLOSE(94)
      CLOSE(95)
      PRINT *
      END PROGRAM EQTDS
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!  LECTURE ET RECOPIE DES PARAMETRES
!
      SUBROUTINE IOPBA0(LUI,LUO)
      use mod_dppr
      use mod_par_tds
      IMPLICIT NONE
      integer          :: LUI,LUO

      real(kind=dppr)  :: PARA,PREC

      integer          :: I,IP
      integer          :: NBOPH

      character(len = NBCLAB+10)  :: CHAINE
      character(len = NBCTIT)     :: TITRE
!
1000  FORMAT(A)
1001  FORMAT(I4,A)
1002  FORMAT(A,E18.11,E14.7)
8000  FORMAT(' !!! EQ_TDS : STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF IN IOPBA0')
!
      DO I=1,4
        READ(LUI,END=2000)               TITRE
        IF( LUO .NE. 0 ) WRITE(LUO,1000) TITRE
      ENDDO
      READ(LUI,END=2000)               NBOPH,TITRE
      IF( LUO .NE. 0 ) WRITE(LUO,1001) NBOPH,TITRE
      DO I=1,2
        READ(LUI,END=2000)               TITRE
        IF( LUO .NE. 0 ) WRITE(LUO,1000) TITRE
      ENDDO
      DO IP=1,NBOPH
        READ(LUI,END=2000)               CHAINE,PARA,PREC
        IF( LUO .NE. 0 ) WRITE(LUO,1002) CHAINE,PARA,PREC
      ENDDO
      RETURN
!
2000  PRINT 8003
      PRINT 8000
      PRINT *
      STOP
!
      END SUBROUTINE IOPBA0
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!  LECTURE ET RECOPIE DES PARAMETRES
!
      SUBROUTINE IOBA0B(LUI,LUO,LUU,ICNTCL)
      use mod_dppr
      use mod_par_tds
      use mod_main_eqtds
      IMPLICIT NONE
      integer          :: LUI,LUO,LUU,ICNTCL

      real(kind=dppr)  :: PARA,PREC

      integer          :: I,IP
      integer          :: NBOPH

      character(len = NBCLAB+10) :: CHAINE
      character(len = NBCTIT)    :: TITRE
!
1000  FORMAT(A)
1001  FORMAT(I4,A)
1002  FORMAT(A,E18.11,E14.7)
8000  FORMAT(' !!! EQ_TDS : STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF IN IOBA0B')
!
      DO I=1,4
        READ (LUI,END=2000)              TITRE
        WRITE(LUU)                       TITRE
        IF( LUO .NE. 0 ) WRITE(LUO,1000) TRIM(TITRE)
      ENDDO
      READ(LUI,END=2000)                NBOPH,TITRE
      DO WHILE( NBOPH .GT. MXOPH )
        CALL RESIZE_MXOPH
      ENDDO
      WRITE(LUU)                       ICNTCL,TITRE
      IF( LUO .NE. 0 ) WRITE(LUO,1001) ICNTCL,TRIM(TITRE)
      DO I=1,2
        READ (LUI,END=2000)              TITRE
        WRITE(LUU)                       TITRE
        IF( LUO .NE. 0 ) WRITE(LUO,1000) TRIM(TITRE)
      ENDDO
      DO IP=1,NBOPH
        READ(LUI,END=2000)                 CHAINE,PARA,PREC
        IF( ISFIT(IP) .GT. 1 ) THEN
          WRITE(LUU)                       CHAINE,PARA,PREC
          IF( LUO .NE. 0 ) WRITE(LUO,1002) CHAINE,PARA,PREC
        ENDIF
      ENDDO
      RETURN
!
2000  PRINT 8003
      PRINT 8000
      PRINT *
      STOP
!
      END SUBROUTINE IOBA0B
