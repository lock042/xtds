
*************************
INSTALLATION INSTRUCTIONS
*************************

The present file has been extracted from the archive file by:

      gunzip <C3vTDS.tar.gz | tar xvf -
      which creates the ./C3vTDS directory.

The procedure

      c3vtds_INSTALL (or ./c3vtds_INSTALL)

needs to be executed to customize all executable files and compile all programs.
It updates pathes in the different scripts and runs the compile script.
Once uncompressed and extracted C3vTDS.tar.gz can be purged.

The C3vTDS package will then be ready for use (see next Section).

NOTES:

> Compilation and execution have been tested on the following UNIX platforms:

      HP, IBM, SUN, Compaq Alpha, Mac G3/G4/G5/Intel, Linux.

      The compiling options depend on the compiler type.

      SGI workstations can also run the package
      with a few modifications. Please contact us.

> For complex band systems a free disk space of several hundreds of
      Megabytes is required.

> To use jobs it is highly recommanded to create first a calculation directory
  outside install directory.

***********
DESCRIPTION
***********

The installation procedure described above creates the following directories:

  C3vTDS/bin/
  C3vTDS/ctrp/
  C3vTDS/exp/
  C3vTDS/jobs/
  C3vTDS/para/
  C3vTDS/prog/

- C3vTDS/bin/  contains the executables (compiled FORTRAN programs only).

- C3vTDS/ctrp/ contains parameter control files for fits arranged by molecule
               (one sub-directory per molecule).
               Example:

                  C3vTDS/ctrp/12CH3D/nu2

- C3vTDS/exp/  contains assignment files arranged by molecule and by transition
               type.
               Example:

                  C3vTDS/exp/12CH3D/dip/ASG_Brown_nu2

- C3vTDS/jobs/ contains UNIX jobs for

               1) calculation of spectra (one directory per molecule),
               2) experimental data fit (examples only in fit_examples).
               3) simulation of synthetic spectra (examples only in simul_examples).

- C3vTDS/para/ contains Hamiltonian, dipole moment and polarisability parameters
               arranged by molecule.
               Example:

                  C3vTDS/para/12CH3D/Pa_010000m000000

- C3vTDS/prog/ contains three sub-directories:

               1) exe contains:
                 - compile that compiles all the package programs. After installation,
                   it can be re-runned if different compiling options are needed.
                 - xasg, exasg and passx files that are used by the jobs.

               2) ppr contains the F95 sources of the different main programs.

               3) ssp contains the F95 sources of the different subroutines and functions.

               4) mod contains the F95 sources of the different modules.

************************************
MOLECULES AND BAND SYSTEMS INCLUDED
************************************

Please check the Web site

  http://icb.u-bourgogne.fr/OMR/SMA/SHTDS/C3VTDS.html

for news and updates.

NOTE: The jobs that can be found in the subdirectories of C3vTDS/jobs can be used
      just as they are.

C3vTDS uses the C3v formalism described in the following article :

  - in preparation

For those who want to create their own calculation and fit jobs, they can modify the given
examples. The C3vTDS syntax is explained in the syntax_README file.

********
CONTACTS
********

         V.Boudon and Ch.Wenger
         E-mail Vincent.Boudon@u-bourgogne.fr
                Christian.Wenger@u-bourgogne.fr

