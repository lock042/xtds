!
! ***** Table de multiplication de C4v
!
! ***** M. Rotger 12/98
!
      SUBROUTINE MULC4V(IC1,IC2,N,IC)
      IMPLICIT NONE
      integer ,dimension(4)  :: IC
      integer          :: IC1,IC2,N

      integer          :: I1,I2
!
      IF( IC1 .GT. IC2 ) GOTO 2
      I1 = IC1
      I2 = IC2
      GOTO 3
!
2     I1 = IC2
      I2 = IC1
!
3     IF    ( I1 .EQ. 2 ) THEN
        GOTO 5
      ELSEIF( I1 .GT. 2 ) THEN
        GOTO 12
      ENDIF
      N     = 1
      IC(1) = I2
      RETURN
!
5     N = 1
      IF    ( I2 .EQ. 3 ) THEN
        GOTO 7
      ELSEIF( I2 .GT. 3 ) THEN
        GOTO 8
      ENDIF
      IC(1) = 1
      RETURN
!
7     IC(1) = 4
      RETURN
!
8     IF    ( I2 .EQ. 4 ) THEN
        GOTO 10
      ELSEIF( I2 .GT. 4 ) THEN
        GOTO 11
      ENDIF
      IC(1) = 5
      RETURN
!
10    IC(1) = 3
      RETURN
!
11    IC(1) = 5
      RETURN
!
12    IF( I1 .GE. 4 ) GOTO 16
      IF( I2 .GT. 3 ) GOTO 15
      N     = 1
      IC(1) = 1
      RETURN
!
15    N = 1
      IF( I2 .GT. 4 ) GOTO 21
      IC(1) = 2
      RETURN
!
21    IC(1) = 5
      RETURN
!
16    IF( I2 .GT. 4 ) GOTO 19
      N     = 1
      IC(1) = 1
      RETURN
!
19    N     = 1
      IC(1) = 5
      IF( I1 .EQ. I2 ) THEN
         N     = 4
         IC(1) = 1
         IC(2) = 2
         IC(3) = 3
         IC(4) = 4
      ENDIF
!
      RETURN
      END SUBROUTINE MULC4V
