      SUBROUTINE XWF
!
! 2011 JULY
! V. BOUDON, Ch. WENGER
!
! WRITE OUT FREQUENCY FILES
!   f_prediction.t
!   f_prediction_mix.t
!   f_statistics.t
!
! BASED UPON eq_tds.f90
!
      use mod_dppr
      use mod_par_tds
      use mod_com_branch
      use mod_com_const
      use mod_com_fdate
      use mod_com_sy
      use mod_com_xpoly
      use mod_com_xvari
      use mod_main_xpafit
      IMPLICIT NONE

      real(kind=dppr)  :: CXHZ
      real(kind=dppr)  :: DIFF
      real(kind=dppr)  :: ECTPJ,ECUM,ENIN,ENSU
      real(kind=dppr)  :: FCAL,FEXP
      real(kind=dppr)  :: PDIFF,POIC
      real(kind=dppr)  :: SDFCAL,SDFEXP,SDFMOC,SDFMOD,SEXP

      integer          :: I,IC,ICINF,ICSUP,IFOBZC,IINF,IOBS,IP,IPOL,ISB,ISUP,ISV,ISVV,ITOT,ITR
      integer          :: J,JCENT,JINF,JJ,JMACS,JSUP
      integer          :: LPC,LPCI,LPCMAC
      integer          :: NBAND,NBFTRC,NBINF,NBNIVI,NBNIVS,NBOPHC,NBSUP,NINF
      integer          :: NSUP,NSV,NSVINF,NSVSUP,NUI,NUNI,NUNS,NUS,NUSVI,NUSVS

      character(len = NBCTIT)  :: TITRE
      character(len =   1)  :: PAINF,PASUP
      character(len =   1)  :: ISP,ISPX,FASO
      character(len =   2)  :: BR,SYINF,SYSUP
      character(len =   3)  :: DIPPOL
      character(len =   4)  :: POLSTA
      character(len =   5)  :: NUO
      character(len =  20)  :: CODI,CODS                                                           ! 20 : NBVQN not taken into account
      character(len = 120)  :: FPARAC,FEM,FXEDS,FXEDI

      logical          :: INTRA
!
1000  FORMAT(A)
2001  FORMAT(' >> ',A)
2005  FORMAT(2A,3F11.4,E13.4,2X,A,2(I3,1X,2A,I3,1X))
2006  FORMAT((10X,20(2I4,'%')))
2020  FORMAT(F13.6,2X,2A,2F7.3,F11.3,F13.6,F8.3,I5,1X,2A,I3,F13.6,I3,I4,'%',2X,A,1X,2(I3,1X,2A,I3,1X))
2100  FORMAT(A,'    POLYAD ***********************************',/)
2101  FORMAT(I4,' VIBRATIONAL SUBLEVEL(S)')
2102  FORMAT(/,                                &
             I4,' HAMILTONIAN OPERATOR(S)',/)
2103  FORMAT(I4,' GREATEST VIBRATIONAL PERCENTAGE(S)',/)
2104  FORMAT(/,                                                           &
             '* POSITION :'                                         ,/,   &
             '      FEXP       CODE   SDEXP  SDTHEO  EXP-CALC      ',     &
             'FCAL      SDCAL  ( J C    N      E  )SUP         ....',     &
             ' ASSIGNMENT  ....'                                    ,/ )
2106  FORMAT(/,                                         &
             A,' TRANSITION **********************',/)
2109  FORMAT(I8,' ASSIGNMENTS FOR THIS TRANSITION',/)
3003  FORMAT(I3,I8,F15.6,6X,I6,4X,3F13.6)
3013  FORMAT(' PARTIAL STATISTICS ')
3014  FORMAT(//,                                  &
             I6,' DATA FROM BAND ',A,' <=== ',A)
3015  FORMAT(//,                                                                                        &
             '      NUMBER OF   THEORETICAL   CUMULATIVE      PARTIAL      MEAN       CUMULATIVE', /,   &
             'Jsup    DATA       PRECISION    NB OF DATA     STD. DEV.     DEV.       STD. DEV.' ,// )
3016  FORMAT(/,                                                         &
             'DUE TO ASSIGNMENT MIXED UNITS, STATISTICS UNIT IS CM-1')
8000  FORMAT(' !!! XWF    : STOP ON ERROR')
8012  FORMAT(' !!! ERROR OPENING PARAMETER FILE : ',A)
8100  FORMAT(' !!! WRONG NUMBER OF LOWER BANDS')
8101  FORMAT(' !!! WRONG NUMBER OF UPPER BANDS')
8104  FORMAT(' !!! UPPER LEVEL FILE => UNEXPECTED EOF')
8105  FORMAT(' !!! LOWER LEVEL FILE => UNEXPECTED EOF')
8107  FORMAT(' !!! ERROR OPENING MATRIX ELEMENT FILE : ',A)
8112  FORMAT(' !!! ERROR OPENING f_prediction_mix.t FILE')
8113  FORMAT(' !!! ERROR OPENING f_prediction.t FILE')
8114  FORMAT(' !!! ERROR OPENING f_statistics.t FILE')
8125  FORMAT(' !!! ',A,' TRANSITION'              ,/,   &
             ' !!! INCOMPATIBLE NUMBERS OF'       ,/,   &
             ' !!!   LOWER POLYAD LEVELS     :',I6,/,   &
             ' !!!   UPPER POLYAD LEVELS     :',I6,/,   &
             ' !!!            AND TRANSITIONS:',I6   )
!
! PREDICTIONS
!
      PRINT 2001,           'f_prediction_mix.t'
      OPEN(95,ERR=4091,FILE='f_prediction_mix.t',STATUS='UNKNOWN')
!
! STATISTICS
!
      PRINT 2001,           'f_statistics.t'
      OPEN(93,ERR=4093,FILE='f_statistics.t',STATUS='UNKNOWN')
      PRINT 2001,           'f_prediction.t'
      OPEN(91,ERR=4080,FILE='f_prediction.t',STATUS='UNKNOWN')
      FPARAC = FPARA(ITRHT(0))                                                                     ! the transition referencing H
      FPARAC = TRIM(FPARAC)//'_adj'                                                                ! fitted parameter file
      OPEN(70,ERR=9994,FILE=TRIM(FPARAC),STATUS='OLD')
!
! *******************************
! ***  READ AND COPY HEADERS  ***
! *******************************
      WRITE(91,1000)
      CALL IOAAS(70,91)                                                                            ! hamiltonian parameters
      CLOSE(70)
      WRITE(91,1000)
      WRITE(91,1000)
      DO IPOL=1,NBPOL                                                                              ! for each polyad
        FEM = 'HA_P'//CPOL(IPOL)//'_D'//TRIM(CDEV(IPOL))//'_'
        WRITE(91,2100) 'P'//CPOL(IPOL)
        WRITE(93,2100) 'P'//CPOL(IPOL)
        WRITE(95,2100) 'P'//CPOL(IPOL)
        OPEN(40,ERR=4040,FILE=TRIM(FEM),STATUS='OLD',FORM='UNFORMATTED')                           ! matrix elements
!
! VIBRATIONAL SUBLEVELS
!
        DO I=1,8+NNIV(IPOL)
          READ(40)
        ENDDO
        READ (40)      NSV
        WRITE(91,2101) NSV
        WRITE(93,2101) NSV
        WRITE(95,2101) NSV
        READ(40)
        DO ISV=1,NSV
          READ (40)      TITRE(:100)
          WRITE(91,1000) TITRE(:100)
          WRITE(93,1000) TITRE(:100)
          WRITE(95,1000) TITRE(:100)
        ENDDO
!
! NUMBER OF OPERATORS OF THE CURRENT LEVEL
!
        DO I=1,4
          READ(40)
        ENDDO
        READ (40)      NBOPHC
        WRITE(91,2102) NBOPHC
        WRITE(93,2102) NBOPHC
        WRITE(95,2102) NBOPHC
        CLOSE(40)
        CALL XHDIAG(IPOL)                                                                          ! XED_ files
      ENDDO
      WRITE(91,1000) FDATE
      WRITE(93,1000) FDATE
      WRITE(91,2104)
!
! TRANSITION LOOP
!
E1:   DO ITR=1,NTR
        IF( ISRAM(ITR) ) THEN
          DIPPOL = 'pol'
          POLSTA = POLST(ITR)
        ELSE
          DIPPOL = ''
          POLSTA = ''
        ENDIF
        WRITE(91,2106) 'P'//CPOL(INPS(ITR))//'mP'//CPOL(INPI(ITR))//' '//DIPPOL//' '//POLSTA
        WRITE(91,2109) NBFTR(ITR)
        WRITE(93,2106) 'P'//CPOL(INPS(ITR))//'mP'//CPOL(INPI(ITR))//' '//DIPPOL//' '//POLSTA
        WRITE(95,2106) 'P'//CPOL(INPS(ITR))//'mP'//CPOL(INPI(ITR))//' '//DIPPOL//' '//POLSTA
        WRITE(93,2109) NBFTR(ITR)
        WRITE(95,2109) NBFTR(ITR)
        IF( NBFTR(ITR) .EQ. 0 ) CYCLE E1                                                           ! no fitted frequency
        NBNIVS = 0
        NBNIVI = 0
        NUSVS  = 0
        NUSVI  = 0
        JMACS  = JMAX(INPS(ITR))
        FXEDS  = 'XED_P'//CPOL(INPS(ITR))//'_'                                                     ! upper polyad energy file
        FXEDI  = 'XED_P'//CPOL(INPI(ITR))//'_'                                                     ! lower polyad energy file
        IF( FXEDS .EQ. FXEDI ) THEN
          INTRA = .TRUE.
        ELSE
          INTRA = .FALSE.
        ENDIF
        OPEN(50,ERR=4050,FILE=FXEDS,STATUS='OLD',FORM='UNFORMATTED')
        IF( INTRA ) THEN                                                                           ! partial copy of FXEDS in 51
          OPEN(51,STATUS='SCRATCH',FORM='UNFORMATTED')
          READ(50) NSVINF                                                                          ! no copy
          DO I=1,NSVINF
            READ(50)                                                                               ! no copy
          ENDDO
          READ(50)                                                                                 ! no copy
!
4001      READ (50,END=4003) JINF,ICINF,NBINF
          WRITE(51)          JINF,ICINF,NBINF
          DO IP=1,NBINF
            READ (50,END=4161) ENIN
            WRITE(51)          ENIN
            READ(50,END=4161)
            WRITE(51)
            READ (50,END=4161) (ICENT(ISV),ISV=1,NSVINF)
            WRITE(51)          (ICENT(ISV),ISV=1,NSVINF)
          ENDDO
          GOTO 4001
!
4003      REWIND(50)
          ENDFILE(51)
          REWIND(51)
        ELSE
          OPEN(51,ERR=4051,FILE=FXEDI,STATUS='OLD',FORM='UNFORMATTED')
        ENDIF
!
! NSVSUP UPPER VIBRATIONAL SUBLEVELS
!
        READ(50) NSVSUP
        LPCMAC = MIN(NSVSUP,LPCMAX)                                                                ! reduce LPCMAX if needed
        WRITE(91,2103) LPCMAC
        WRITE(93,2103) LPCMAC
        WRITE(95,2103) LPCMAC
E111:   DO ISV=1,NSVSUP
          READ(50) TITRE(:100)
          CODS(1:1) = TITRE(15:15)
          CODS(2:2) = TITRE(26:26)
          CODS(3:3) = TITRE(37:37)
          CODS(4:4) = TITRE(52:52)
          CODS(5:5) = TITRE(67:67)
          CODS(6:6) = TITRE(82:82)
          IF( ISV .EQ. 1 ) THEN
            NBNIVS           = 1
            CODNS(1)(:NBVQN) = CODS(:NBVQN)
            NCODNS(1)        = 1
            CYCLE E111
          ENDIF
          IF( CODS(:NBVQN) .EQ. CODNS(NBNIVS)(:NBVQN) ) THEN
            NCODNS(ISV) = NBNIVS
          ELSE
            NBNIVS = NBNIVS+1
            IF( NBNIVS .GT. MXNIV ) CALL RESIZE_MXNIV
            CODNS(NBNIVS)(:NBVQN) = CODS(:NBVQN)
            NCODNS(ISV)           = NBNIVS
          ENDIF
        ENDDO E111
        READ(50)
!
! NSVINF  LOWER VIBRATIONAL SUBLEVELS
!
        IF( .NOT. INTRA ) THEN
          READ(51) NSVINF
E121:     DO ISV=1,NSVINF
            READ(51) TITRE(:100)
            CODI(1:1) = TITRE(15:15)
            CODI(2:2) = TITRE(26:26)
            CODI(3:3) = TITRE(37:37)
            CODI(4:4) = TITRE(52:52)
            CODI(5:5) = TITRE(67:67)
            CODI(6:6) = TITRE(82:82)
            IF( ISV .EQ. 1 ) THEN
              NBNIVI           = 1
              CODNI(1)(:NBVQN) = CODI(:NBVQN)
              NCODNI(1)        = 1
              CYCLE E121
            ENDIF
            IF( CODI(:NBVQN) .EQ. CODNI(NBNIVI)(:NBVQN) ) THEN
              NCODNI(ISV) = NBNIVI
            ELSE
              NBNIVI = NBNIVI+1
              IF( NBNIVI .GT. MXNIV ) CALL RESIZE_MXNIV
              CODNI(NBNIVI)(:NBVQN) = CODI(:NBVQN)
              NCODNI(ISV)           = NBNIVI
            ENDIF
          ENDDO E121
          READ(51)
        ELSE
!
! SPECIFIC CASE : LOWER BAND = UPPER BAND
!
          NBNIVI = NBNIVS
          DO WHILE( NBNIVI .GT. MXNIV )
            CALL RESIZE_MXNIV
          ENDDO
          DO ISV=1,NSVSUP
            NCODNI(ISV) = NCODNS(ISV)
          ENDDO
          DO ISB=1,NBNIVI
            CODNI(ISB)(:NBVQN) = CODNS(ISB)(:NBVQN)
          ENDDO
        ENDIF
        CALL DEBUG( 'XWF    => MXNIV=',NBNIVI)
!
! INITIALIZATION
!
        DO J=0,JMACS
          DO ISV=0,MXNIV
            DO ISVV=0,MXNIV
              NFJ(J,ISV,ISVV)   = 0
              SMJ(J,ISV,ISVV)   = 0.D0
              SPOI(J,ISV,ISVV)  = 0.D0
              SPROR(J,ISV,ISVV) = 0.D0
            ENDDO
          ENDDO
        ENDDO
!
! *******************************
! *** READ LOWER ENERGY FILE  ***
! *******************************
!
        IINF   = 0
        NBFTRC = 0
!
6       READ(51,END=8) JINF,ICINF,NBINF
E7:     DO IP=1,NBINF
          READ(51,END=4160) ENIN
          READ(51,END=4160)
          READ(51,END=4160) (ICENT(ISV),ISV=1,NSVINF)
          JCENT         = ICENT(1)
          NBAND         = NCODNI(1)
          JCENTT(NBAND) = JCENT
          DO IC=2,NSVINF
            IF( NCODNI(IC) .EQ. NCODNI(IC-1) ) THEN
              JCENT = JCENT+ICENT(IC)
            ELSE
              JCENTT(NBAND) = JCENT
              NBAND         = NCODNI(IC)
              JCENT         = ICENT(IC)
            ENDIF
          ENDDO
          JCENTT(NBAND) = JCENT
!
! ********************
! *** MAXIMUM ENERGY
! ********************
!
          IF( NBAND .NE. NBNIVI ) GOTO 4165
          JCENT = 0
E214:     DO IC=1,NBAND
            IF( JCENT .GE. JCENTT(IC) ) CYCLE E214
            JCENT = JCENTT(IC)
            NUSVI = IC
          ENDDO E214
!
! ***************************************************************
! *** IS IT A LEVEL CORRESPONDING TO AN OBSERVED TRANSITION ? ***
! ***************************************************************
!
E200:     DO IOBS=1,NBOBS
            IF( JTR(IOBS)   .NE. ITR          .OR.                &
                FASOT(IOBS) .NE. '+'                ) CYCLE E200
            IF( JINF        .EQ. JIOBS (IOBS) .AND.         &
                ICINF       .EQ. ICIOBS(IOBS) .AND.         &
                IP          .EQ. NIOBS (IOBS)       ) THEN
              IINF         = IINF+1
              IPCINF(IOBS) = NUSVI
              NBFTRC       = NBFTRC+1
              IF( NBFTRC .EQ. NBFTR(ITR) ) GOTO 8
            ENDIF
          ENDDO E200
        ENDDO E7
        GOTO 6
!
8       CLOSE(51)
!
! *******************************
! *** READ UPPER ENERGY FILE  ***
! *******************************
!
        ISUP   = 0
        NBFTRC = 0
!
66      READ(50,END=88) JSUP,ICSUP,NBSUP
E77:    DO IP=1,NBSUP
          READ(50,END=4161) ENSU
          READ(50,END=4161)
          READ(50,END=4161) (ICENT(ISV),ISV=1,NSVSUP)
!
! *********************************************
! *** ACCUMULATION OF ENERGIES FOR A GIVEN BAND
! *********************************************
!
          JCENT         = ICENT(1)
          NBAND         = NCODNS(1)
          JCENTT(NBAND) = JCENT
          DO IC=2,NSVSUP
            IF( NCODNS(IC) .EQ. NCODNS(IC-1) ) THEN
              JCENT = JCENT+ICENT(IC)
            ELSE
              JCENTT(NBAND) = JCENT
              NBAND         = NCODNS(IC)
              JCENT         = ICENT(IC)
            ENDIF
          ENDDO
          JCENTT(NBAND) = JCENT
!
! ********************
! *** MAXIMUM ENERGY
! ********************
!
          IF( NBAND .NE. NBNIVS ) GOTO 4164
          JCENT = 0
E23:      DO IC=1,NBAND
            IF( JCENT .GE. JCENTT(IC) ) CYCLE E23
            JCENT = JCENTT(IC)
            NUSVS = IC
          ENDDO E23
!
! ***************************************************************
! *** IS IT A LEVEL CORRESPONDING TO AN OBSERVED TRANSITION ? ***
! ***************************************************************
!
E202:     DO IOBS=1,NBOBS
            IF( JTR(IOBS)   .NE. ITR          .OR.                &
                FASOT(IOBS) .NE. '+'                ) CYCLE E202
            IF( JSUP        .EQ. JSOBS (IOBS) .AND.         &                                      !     already checked
                ICSUP       .EQ. ICSOBS(IOBS) .AND.         &                                      ! not already checked
                IP          .EQ. NSOBS (IOBS)       ) THEN
              ISUP         = ISUP+1
              ENSUP(IOBS)  = ENSU
              IPCSUP(IOBS) = NUSVS
              DO LPC=1,LPCMAC
                IPCSN(IOBS,LPC)  = 0
                NUSVSU(IOBS,LPC) = 0
E702:           DO IC=1,NSVSUP                                                                     ! for each sublevel
                  DO LPCI=1,LPC-1
                    IF( NUSVSU(IOBS,LPCI) .EQ. IC ) CYCLE E702                                     ! IC already used
                  ENDDO
                  IF( IPCSN(IOBS,LPC) .GT. ICENT(IC) ) CYCLE E702                                  ! not a bigger ICENT
                  IPCSN(IOBS,LPC)  = ICENT(IC)                                                     ! bigger => save
                  NUSVSU(IOBS,LPC) = IC
                ENDDO E702
              ENDDO
              NBFTRC = NBFTRC+1
              IF( NBFTRC .EQ. NBFTR(ITR) ) GOTO 88
            ENDIF
          ENDDO E202
        ENDDO E77
        GOTO 66
!
88      CONTINUE
!
! ASSIGNMENT LOOP
!
        ITOT   = 0
        NBFTRC = 0
E217:   DO IOBS=1,NBOBS
          IF( JTR(IOBS)   .NE. ITR .OR.               &
              FASOT(IOBS) .NE. '+'      ) CYCLE E217
          ITOT = ITOT+1
          IF( ITOT .EQ. 1 ) THEN
            ISPX = ISPT(IOBS)
          ELSE
            IF( ISPT(IOBS) .NE. ISPX ) ISPX = '?'
          ENDIF
          ISP    = ISPT(IOBS)
          NUO    = NUOT(IOBS)
          FEXP   = FOBS(IOBS)
          FASO   = FASOT(IOBS)
          SEXP   = SOBS(IOBS)
          SDFEXP = SDFOBS(IOBS)
          JINF   = JIOBS(IOBS)
          SYINF  = SYM(ICIOBS(IOBS))
          PAINF  = PARGEN
          NINF   = NIOBS(IOBS)
          JSUP   = JSOBS(IOBS)
          SYSUP  = SYM(ICSOBS(IOBS))
          PASUP  = PARGEN
          NSUP   = NSOBS(IOBS)
          BR     = BRANCH(JSUP-JINF+MXBRA-2)
          SDFMOD = 1.D0
          IF( IMEGA(ITR) .NE. 0 ) SDFMOD = DBLE(JSUP)**IMEGA(ITR)
          SDFMOD = PNEG(ITR)*SDFMOD
          IFOBZC = IFOBZ(IOBS)
          DIFF   = FOMC(IFOBZC)/ZWGT(IFOBZC)
          FCAL   = FEXP-DIFF
          SDFCAL = SDZCAL(IFOBZC)
!
! STATISTICS
!
          POIC                = ZWGT(IFOBZC)*ZWGT(IFOBZC)
          NUS                 = IPCSUP(IOBS)
          NUI                 = IPCINF(IOBS)
          PDIFF               = POIC*DIFF*DIFF
          NFJ(JSUP,NUS,NUI)   = NFJ(JSUP,NUS,NUI)+1
          NFJ(JSUP,0,0)       = NFJ(JSUP,0,0)+1
          SMJ(JSUP,NUS,NUI)   = SMJ(JSUP,NUS,NUI)+POIC*DIFF
          SMJ(JSUP,0,0)       = SMJ(JSUP,0,0)+POIC*DIFF
          SPOI(JSUP,NUS,NUI)  = SPOI(JSUP,NUS,NUI)+POIC
          SPOI(JSUP,0,0)      = SPOI(JSUP,0,0)+POIC
          SPROR(JSUP,NUS,NUI) = SPROR(JSUP,NUS,NUI)+PDIFF
          SPROR(JSUP,0,0)     = SPROR(JSUP,0,0)+PDIFF
          IF    ( ISP .EQ. 'M' ) THEN
            FEXP   = FEXP*CMHZ
            SDFEXP = SDFEXP*CMHZ
            SDFMOD = SDFMOD*CMHZ
            DIFF   = DIFF*CMHZ
            FCAL   = FCAL*CMHZ
            SDFCAL = SDFCAL*CMHZ
          ELSEIF( ISP .EQ. 'G' ) THEN
            FEXP   = FEXP*CGHZ
            SDFEXP = SDFEXP*CGHZ
            SDFMOD = SDFMOD*CGHZ
            DIFF   = DIFF*CGHZ
            FCAL   = FCAL*CGHZ
            SDFCAL = SDFCAL*CGHZ
          ENDIF
          SDFEXP = SDFEXP*1.D+3
          SDFMOD = SDFMOD*1.D+3
          DIFF   = DIFF*1.D+3
          SDFCAL = SDFCAL*1.D+3
          DIFF   = MIN(DIFF  ,9999.999D0)                                                          ! cut off
          SDFCAL = MIN(SDFCAL,9999.999D0)                                                          ! cut off
!
! WRITE OUT OUTPUT FILES
!
          WRITE(91,2020) FEXP,FASO,ISP//NUO,SDFEXP,SDFMOD,DIFF,FCAL,SDFCAL,  &
                         JSUP,SYSUP,PASUP,NSUP,                              &
                         ENSUP(IOBS),NUSVSU(IOBS,1),IPCSN(IOBS,1),BR,        &
                         JINF,SYINF,PAINF,NINF,                              &
                         JSUP,SYSUP,PASUP,NSUP
          WRITE(95,2005) ISP,FASO,FEXP,FCAL,DIFF,SEXP,BR,  &
                         JINF,SYINF,PAINF,NINF,            &
                         JSUP,SYSUP,PASUP,NSUP
          WRITE(95,2006) (NUSVSU(IOBS,JJ),IPCSN(IOBS,JJ),JJ=1,LPCMAC)
          NBFTRC = NBFTRC+1
          IF( NBFTRC .EQ. NBFTR(ITR) ) GOTO 18
        ENDDO E217
!
18      IF( IINF .NE. ISUP .OR.         &
            ISUP .NE. ITOT      ) THEN
          PRINT 8125, 'P'//CPOL(INPS(ITR))//'mP'//CPOL(INPI(ITR))//' '//DIPPOL//' '//POLSTA,  &
                      IINF,ISUP,ITOT
          GOTO  9999
        ENDIF
!
! ********************
! ***  STATISTICS  ***
! ********************
!
! PER BAND
!
        IF(     ISPX .EQ. '?' ) WRITE(93,3016)
        IF    ( ISPX .EQ. 'M' ) THEN
          CXHZ = CMHZ
        ELSEIF( ISPX .EQ. 'G' ) THEN
          CXHZ = CGHZ
        ELSE
          CXHZ = 1.D0
        ENDIF
        WRITE(93 ,3013)
        DO NUNS=1,NBNIVS
E115:     DO NUNI=1,NBNIVI
            DO J=0,JMACS
              NCU(J)  = NFJ(J,NUNS,NUNI)
              SPCU(J) = SPOI(J,NUNS,NUNI)
              ECU(J)  = SPROR(J,NUNS,NUNI)
            ENDDO
            DO J=1,JMACS
              NCU(J)  = NCU(J-1)+NCU(J)
              SPCU(J) = SPCU(J-1)+SPCU(J)
              ECU(J)  = ECU(J-1)+ECU(J)
            ENDDO
            IF( NCU(JMACS) .EQ. 0 ) CYCLE E115
            WRITE(93,3014) NCU(JMACS),           &
                           CODNS(NUNS)(:NBVQN),  &
                           CODNI(NUNI)(:NBVQN)
            WRITE(93,3015)
E104:       DO J=0,JMACS
              IF( NFJ(J,NUNS,NUNI) .EQ. 0 ) CYCLE E104
              SMJ(J,NUNS,NUNI) = SMJ(J,NUNS,NUNI)/SPOI(J,NUNS,NUNI)*CXHZ
              ECTPJ            = SQRT(SPROR(J,NUNS,NUNI)/SPOI(J,NUNS,NUNI))*CXHZ
              ECUM             = SQRT(ECU(J)/SPCU(J))*CXHZ
              SDFMOC           = 1.D0
              IF( IMEGA(ITR) .NE. 0 ) SDFMOC = DBLE(J)**IMEGA(ITR)
              SDFMOC = PNEG(ITR)*SDFMOC*CXHZ
              WRITE(93,3003) J,NFJ(J,NUNS,NUNI),SDFMOC,NCU(J),ECTPJ,SMJ(J,NUNS,NUNI),ECUM
            ENDDO E104
          ENDDO E115
        ENDDO
        CLOSE(50)
      ENDDO E1
      CLOSE(91)
      CLOSE(93)
      CLOSE(95)
      RETURN
!
4040  PRINT 8107, FEM
      GOTO  9999
4050  PRINT 8107, FXEDS
      GOTO  9999
4051  PRINT 8107, FXEDI
      GOTO  9999
4080  PRINT 8113
      GOTO  9999
4091  PRINT 8112
      GOTO  9999
4093  PRINT 8114
      GOTO  9999
4160  PRINT 8105
      GOTO  9999
4161  PRINT 8104
      GOTO  9999
4164  PRINT 8101
      GOTO  9999
4165  PRINT 8100
      GOTO  9999
9994  PRINT 8012, FPARAC
      GOTO  9999
9999  PRINT 8000
      PRINT *
      STOP
!
      END SUBROUTINE XWF
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
! READ AND COPY PARAMETERS
!
      SUBROUTINE IOAAS(LUI,LUO)
      use mod_dppr
      use mod_par_tds
      use mod_com_xpara
      IMPLICIT NONE
      integer          :: LUI,LUO

      real(kind=dppr)  :: PARAC,PREC

      integer          :: I,IP
      integer          :: NBOPHC

      character(len = NBCLAB+10)  :: CHAINE
      character(len = NBCTIT)     :: TITRE
!
1000  FORMAT(A)
1001  FORMAT(I4,A)
1002  FORMAT(A,E18.11,E14.7)
8000  FORMAT(' !!! XWF    : STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF IN IOAAS')
!
      DO I=1,4
        READ (LUI,1000,END=2000) TITRE
        WRITE(LUO,1000)          TRIM(TITRE)
      ENDDO
      READ (LUI,1001,END=2000)    NBOPHC,TITRE
      WRITE(LUO,1001)          NBXOHT(0),TRIM(TITRE)
      DO I=1,2
        READ (LUI,1000,END=2000) TITRE
        WRITE(LUO,1000)          TRIM(TITRE)
      ENDDO
      DO IP=1,NBOPHC
        READ(LUI,1002,END=2000) CHAINE,PARAC,PREC
        IF( ICNTRL(IP) .EQ. 2 .OR.         &
            ICNTRL(IP) .EQ. 4      ) THEN                                                          ! fitted
          WRITE(LUO,1002)       CHAINE,PARAC,PREC
        ENDIF
      ENDDO
      RETURN
!
2000  PRINT 8003
      PRINT 8000
      PRINT *
      STOP
!
      END SUBROUTINE IOAAS
