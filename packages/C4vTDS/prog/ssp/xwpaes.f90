      SUBROUTINE XWPAES
!
! 2011 JULY
! V. BOUDON, Ch. WENGER
!
! WRITE OUT  para_estimates.t
!
! BASED UPON eq_tds.f, paradj.f90
!
      use mod_dppr
      use mod_par_tds
      use mod_par_x
      use mod_com_arond
      use mod_com_cas
      use mod_com_const
      use mod_com_postf
      use mod_com_xassi
      use mod_com_xfonc
      use mod_com_xpara
      use mod_com_xpoly
      use mod_com_xtran
      use mod_com_xvari
      IMPLICIT NONE

      integer ,parameter  :: MXCOR = 9

! functions
      real(kind=dppr)  :: ENORM,RMSH,RMST

      real(kind=dppr) ,dimension(MXOP)   :: SJTR
      real(kind=dppr) ,dimension(MXOBZ)  :: FOMCP
      real(kind=dppr) ,dimension(MXCOR)  :: COR                                                    ! cf. FORMAT 1011
      real(kind=dppr)  :: CORC
      real(kind=dppr)  :: DPCBN
      real(kind=dppr)  :: POIJ
      real(kind=dppr)  :: PSTD
      real(kind=dppr)  :: RMSHP,RMSTP
      real(kind=dppr)  :: SJTRC,SJTRT

      integer         ,dimension(MXOBZ)  :: ISEL
      integer          :: I,IB,IDEB,IEXP,IOP,ITR,ITRT
      integer          :: IX
      integer          :: J,JPCBN,JPF,JPS,JX
      integer          :: K
      integer          :: LTR
      integer          :: NBCNST,NBPFZ,NBPOBZ,NBPSZ
      integer          :: NC,NCL1,NCO,NDEB,NFIN

      character(len = NBCTIT)  :: TITRE
      character(len =   3) ,dimension(MXATR)  :: DIPPOL
      character(len =   6) ,dimension(MXCOR)  :: NUCOR                                             ! cf. FORMAT 1011
      character(len =   1)  :: HT1,HT2
      character(len =   3)  :: RMSHU
!
1000  FORMAT(A)
1005  FORMAT(A,' TRANSITION')
1006  FORMAT(I4,A,': ',A,2X,F7.1,' %   bound within ',E18.11,' +/-',E14.7)
1008  FORMAT(I4,A,': ',A,3X,A,I5,110(E12.4))                                                       ! 110 : cf. MXNTR=55
1009  FORMAT(I4,A,': ',A,'    100.0 %   fixed to     ',E18.11)
1010  FORMAT(I4,A,': ',A,'   ( ',F16.10,22X,') x10',I3)
1011  FORMAT(I4,A,': ',A,6X,9(1X,A,F6.1,'%'))                                                      ! cf. MXCOR
1012  FORMAT(I4,A,': ',A,'    100.0 %   due   to',23X,A)
1013  FORMAT(/,                                                     &
             'PARAMETER ESTIMATES (NON ZERO PARAMETERS)'      ,/,   &
              33X,'Parameter',29X,'Value(St.Dev)    Variation',     &
              10X,'Frdm   Relative weights of Data'           ,/,   &
             111X,55(10X,A,9X)                                   )                                 ! 55 : cf. MXNTR
1014  FORMAT(111X,55(9X,A,1X,A,10X))                                                               ! DIPPOL, POLSTC
1016  FORMAT(111X,55(6X,A,6X))                                                                     ! freq     int
1017  FORMAT('No type 2 constraint (bound to fixed value).',/)
1100  FORMAT('DATA DETAILS',/)
1101  FORMAT(/,                        &
             'PARAMETER CONSTRAINTS')
1102  FORMAT(64X,'Constraint')
1103  FORMAT(33X,'Parameter',18X,'Weight   Description')
1104  FORMAT(/,                                                           &
             I8,' Data fitted ; Initial Standard Deviation : ',F10.3,/,   &
             I8,' Iterations  ; Final   Standard Deviation : ',F10.3   )
1105  FORMAT(E9.2)
1106  FORMAT(I3)
1108  FORMAT(/,                          &
             'Correlations ( >85% )',/)
1109  FORMAT('H PARA')
1110  FORMAT('T PARA')
2001  FORMAT(' >> ',A)
2005  FORMAT(A,' TRANSITION : ',I5,' Frequency Data ; Jmax ',I3,7X,'RMS = ',F11.3,1X,A    ,/,   &
             28X               ,I5,' Intensity Data ; Jmax ',I3,7X,'RMS = ',F11.3,1X,'  %'   )
2006  FORMAT(28X               ,I5,'           Data ; Standard Deviation = ',F11.3)
3010  FORMAT('P',I4,'=')
3011  FORMAT(A,'(',E10.3,'*P',I4,')')
3012  FORMAT(I4,A,':')
!
      NCL1 = NBCLAB+1+NBAM+1                                                                       ! Hmn  included
      DO ITR=1,NTR
        IF( ISRAM(ITR) ) THEN
          DIPPOL(ITR) = 'pol'
          POLSTC(ITR) = POLST(ITR)
        ELSE
          DIPPOL(ITR) = ''
          POLSTC(ITR) = ''
        ENDIF
      ENDDO
!
! WRITE OUT PARAMETER ESTIMATES FILE
!
      PRINT 2001,  'para_estimates.t'
      OPEN(92,FILE='para_estimates.t',STATUS='UNKNOWN')
      WRITE(92,1100)
! STANDARD DEVIATION FOR EACH TRANSITION
E33:  DO ITR=1,NTR
        NBPFZ  = 0                                                                                 ! partial number of OBZ - frequency
        NBPSZ  = 0                                                                                 ! partial number of OBZ - intensity
        NBPOBZ = 0                                                                                 ! partial number of OBZ
        JPF    = 0                                                                                 ! partial JMAX          - frequency
        JPS    = 0                                                                                 ! partial JMAX          - intensity
        RMSHP  = 0.D0                                                                              ! partial RMS           - frequency
        RMSTP  = 0.D0                                                                              ! partial RMS           - intensity
        RMSHU  = ' mk'                                                                             ! partial RMS           - frequency unit
        IF( NBFOBZ .NE. 0 ) THEN                                                                   ! frequency
          DO I=1,NBFOBZ
            IF( JTR(INDOBS(I)) .EQ. ITR ) THEN
              NBPFZ         = NBPFZ+1
              ISEL(NBPFZ)   = I
              NBPOBZ        = NBPOBZ+1
              FOMCP(NBPOBZ) = FOMC(I)
              IF( JIOBS(INDOBS(I)) .GT. JPF ) JPF = JIOBS(INDOBS(I))
            ENDIF
          ENDDO
          IF( NBPFZ .NE. 0 ) THEN
            RMSHP = RMSH(NBPFZ,ISEL)
            IF( INPS(ITR) .EQ. INPI(ITR) ) THEN                                                    ! PimPi
              IF    ( ISPT(INDOBS(ISEL(1))) .EQ. 'M' ) THEN                                        ! implies that all the data of this transition are of the same unit
                RMSHU = 'KHz'
                RMSHP = RMSHP*CMHZ                                                                 ! MHz
              ELSEIF( ISPT(INDOBS(ISEL(1))) .EQ. 'G' ) THEN
                RMSHU = 'MHz'
                RMSHP = RMSHP*CGHZ                                                                 ! GHz
              ENDIF
            ENDIF
          ENDIF
        ENDIF
        IF( NBSOBZ .NE. 0 ) THEN                                                                   ! intensity
          DO I=1,NBSOBZ
            J = NBFOBZ+I
            IF( JTR(INDOBS(J)) .EQ. ITR .AND.         &
                SASOZ(I)       .NE. '-'       ) THEN
              NBPSZ         = NBPSZ+1
              ISEL(NBPSZ)   = J
              NBPOBZ        = NBPOBZ+1
              FOMCP(NBPOBZ) = FOMC(J)
              IF( JIOBS(INDOBS(J)) .GT. JPS ) JPS = JIOBS(INDOBS(J))
            ENDIF
          ENDDO
          IF( NBPSZ .NE. 0 ) RMSTP = RMST(NBPSZ,ISEL)
        ENDIF
        WRITE(92,2005) 'P'//CPOL(INPS(ITR))//'mP'//CPOL(INPI(ITR))//' '//DIPPOL(ITR)//' '//POLSTC(ITR),  &
                       NBPFZ,JPF,RMSHP*1.D+3,                                                            &
                       RMSHU,                                                                            &
                       NBPSZ,JPS,RMSTP*1.D+2
        PRINT    2005, 'P'//CPOL(INPS(ITR))//'mP'//CPOL(INPI(ITR))//' '//DIPPOL(ITR)//' '//POLSTC(ITR),  &
                       NBPFZ,JPF,RMSHP*1.D+3,                                                            &
                       RMSHU,                                                                            &
                       NBPSZ,JPS,RMSTP*1.D+2
        IF( NBPOBZ .EQ. 0 ) CYCLE E33
        PSTD = ENORM(NBPOBZ,FOMCP)/SQRT(DBLE(NBPOBZ))                                              ! std. dev.
        WRITE(92,2006)  NBPOBZ,PSTD
        PRINT    2006,  NBPOBZ,PSTD
      ENDDO E33
      SJTRT = 0.D0
      DO J=1,NBXOP
        SJTRC = 0.D0
        DO ITR=1,NTR
          SJTRC = SJTRC+POIDF(J,ITR)+POIDS(J,ITR)                                                  ! sum of weights for this operator
        ENDDO
        SJTR(J) = SJTRC
        SJTRT   = SJTRT+SJTRC                                                                      ! sum for all operators
      ENDDO
      WRITE(92,1101)
      WRITE(92,1102)
      WRITE(92,1103)
      NBCNST = 0
      IX     = 0
E409: DO ITRT=0,NTRT
        IF( ITRT .EQ. 0 ) THEN
          WRITE(92,1109)
          HT1 = 'H'                                                                                ! hamiltonian operator
        ELSE
          IF( ITRT .EQ. 1 ) THEN
            WRITE(92,1110)
          ENDIF
          HT1 = 'T'                                                                                ! transition operator
          ITR = ITRHT(ITRT)
          WRITE(92,1005) 'P'//CPOL(INPS(ITR))//'mP'//CPOL(INPI(ITR))//' '//DIPPOL(ITR)//' '//POLSTC(ITR)
          DO LTR=1,NTR
            IF( INTRT(LTR) .EQ. ITRT .AND.         &
                LTR        .NE. ITR        ) THEN                                                  ! associated transition
              WRITE(92,1000) 'P'//CPOL(INPS(LTR))//'mP'//CPOL(INPI(LTR))//' '//DIPPOL(LTR)//' '//POLSTC(LTR)
            ENDIF
          ENDDO
        ENDIF
        IDEB = INPHT(ITRT)
        DO IOP=IDEB+1,IDEB+NBOHT(ITRT)
          IF    ( ICNTRL(IOP) .EQ. 0 .OR.         &
                  ICNTRL(IOP) .EQ. 1      ) THEN
            WRITE(92,1009) IOP,HT1,COPAJ(IOP)(:NCL1),PARAR(IOP)
          ELSEIF( ICNTRL(IOP) .EQ. 2      ) THEN
            IX   = IX+1
            POIJ = 0.D0
            DO ITR=1,NTR
              POIJ = POIJ+POIDF(IX,ITR)+POIDS(IX,ITR)
            ENDDO
            WRITE(92,1006) IOP,HT1,COPAJ(IOP)(:NCL1),POIJ/SJTRT,VALAT(IOP),PRAT(IOP)
            NBCNST = NBCNST+1
          ELSEIF( ICNTRL(IOP) .EQ. 5      ) THEN                                                   ! contribution
            WRITE(TITRE,3010) IOP
            NC = 0
            IB = IOP-IDEB
            DO J=1,NBOHT(ITRT)
              IF( ITRT .EQ. 0 ) THEN                                                               ! H
                IF( CTRIBH(IB,J) .NE. 0.D0 ) THEN
                  NC = NC+1
                  IF( NC .NE. 1 ) TITRE = TRIM(TITRE)//'+'
                  WRITE(TITRE,3011) TRIM(TITRE),CTRIBH(IB,J),IDEB+J
                ENDIF
              ELSE                                                                                 ! T
                IF( CTRIBT(ITRT,IB,J) .NE. 0.D0 ) THEN
                  NC = NC+1
                  IF( NC .NE. 1 ) TITRE = TRIM(TITRE)//'+'
                  WRITE(TITRE,3011) TRIM(TITRE),CTRIBT(ITRT,IB,J),IDEB+J
                ENDIF
              ENDIF
            ENDDO
            NDEB = 1
            NFIN = LEN_TRIM(TITRE)
!
30          IF( TITRE(NDEB:NDEB) .EQ. ' ' ) THEN
              TITRE(NDEB:) = TITRE(NDEB+1:)
              NFIN         = NFIN-1
            ELSE
              NDEB = NDEB+1
              IF( NDEB .GE. NFIN ) GOTO 31
            ENDIF
            GOTO 30
!
31          WRITE(92,1012) IOP,HT1,COPAJ(IOP)(:NCL1),TITRE(:NFIN)
          ELSE
            IX = IX+1
          ENDIF
        ENDDO
      ENDDO E409
      IF( NBCNST .EQ. 0 ) WRITE(92,1017)
      WRITE(92,1104) NBROBZ,STDI,NBITER,STDF
      WRITE(92,1013) ('P'//CPOL(INPS(ITR))//'mP'//CPOL(INPI(ITR)),ITR=1,NTR)
      WRITE(92,1014) (DIPPOL(ITR),POLSTC(ITR),ITR=1,NTR)
      WRITE(92,1016) ("freq     int",ITR=1,NTR)
      IX = 0                                                                                       ! index of fitted parameter
      DO ITRT=0,NTRT
        IF( ITRT .EQ. 0 ) THEN
          WRITE(92,1109)
          HT1 = 'H'
        ELSE
          IF( ITRT .EQ. 1 ) WRITE(92,1110)
          HT1 = 'T'
          ITR = ITRHT(ITRT)
          WRITE(92,1005) 'P'//CPOL(INPS(ITR))//'mP'//CPOL(INPI(ITR))//' '//DIPPOL(ITR)//' '//POLSTC(ITR)
          DO LTR=1,NTR
            IF( INTRT(LTR) .EQ. ITRT .AND.         &
                LTR        .NE. ITR        ) THEN                                                  ! associated transition
              WRITE(92,1000) 'P'//CPOL(INPS(LTR))//'mP'//CPOL(INPI(LTR))//' '//DIPPOL(LTR)//' '//POLSTC(LTR)
            ENDIF
          ENDDO
        ENDIF
        IDEB = INPHT(ITRT)
        DO IOP=IDEB+1,IDEB+NBOHT(ITRT)
          IF    ( ICNTRL(IOP) .EQ. 1 .OR.         &
                  ICNTRL(IOP) .EQ. 5      ) THEN                                                   ! 1,5
            WRITE(TITRE,1105) PARAR(IOP)
            READ(TITRE(7:9),1106) IEXP
            WRITE(92,1010) IOP,HT1,COPAJ(IOP)(:NCL1),PARAR(IOP)/( (10.D0)**(IEXP-1) ),IEXP-1
          ELSEIF( ICNTRL(IOP) .NE. 0      ) THEN                                                   ! 2,4
            IX    = IX+1
            DPCBN = PARAR(IOP)-PARAO(IOP)
            CALL AROND(PARAR(IOP),SQRT(COV(IX,IX)),DPCBN,CARON)
            WRITE(92,1008) IOP,HT1,COPAJ(IOP)(:NCL1),CARON,LIB(IOP),                            &
                           (100.*POIDF(IX,ITR)/SJTR(IX),100.*POIDS(IX,ITR)/SJTR(IX),ITR=1,NTR)
          ENDIF
        ENDDO
      ENDDO
! CORRELATIONS
      WRITE(92,1108)
      IX = 0                                                                                       ! index of fitted parameter
      DO ITRT=0,NTRT
        IF( ITRT .EQ. 0 ) THEN
          WRITE(92,1109)
          HT1 = 'H'
        ELSE
          IF( ITRT .EQ. 1 ) WRITE(92,1110)
          HT1 = 'T'
          ITR = ITRHT(ITRT)
          WRITE(92,1005) 'P'//CPOL(INPS(ITR))//'mP'//CPOL(INPI(ITR))//' '//DIPPOL(ITR)//' '//POLSTC(ITR)
          DO LTR=1,NTR
            IF( INTRT(LTR) .EQ. ITRT .AND.         &
                LTR        .NE. ITR        ) THEN                                                  ! associated transition
              WRITE(92,1000) 'P'//CPOL(INPS(LTR))//'mP'//CPOL(INPI(LTR))//' '//DIPPOL(LTR)//' '//POLSTC(LTR)
            ENDIF
          ENDDO
        ENDIF
        IDEB = INPHT(ITRT)
        DO IOP=IDEB+1,IDEB+NBOHT(ITRT)
          IF( ICNTRL(IOP) .EQ. 2 .OR.              &
              ICNTRL(IOP) .EQ. 4      ) IX = IX+1                                                  ! fitted
          NCO = 0
          JX  = 0
E20:      DO JPCBN=1,NBOP
            IF( ICNTRL(JPCBN) .EQ. 2 .OR.              &
                ICNTRL(JPCBN) .EQ. 4      ) JX = JX+1                                              ! fitted
            IF( IOP           .NE. JPCBN .AND.         &                                           ! different
                ICNTRL(IOP)   .EQ. 4     .AND.         &                                           ! free
                ICNTRL(JPCBN) .EQ. 4           ) THEN                                              ! free
              CORC = 100.D0*COV(IX,JX)/SQRT(COV(IX,IX)*COV(JX,JX))
              IF( ABS(CORC) .LT. 85.D0 ) CYCLE E20
              NCO = NCO+1
              IF( JPCBN .LE. NBOHT(0) ) THEN
                HT2 = 'H'
              ELSE
                HT2 = 'T'
              ENDIF
              WRITE(NUCOR(NCO),3012) JPCBN,HT2
              COR(NCO) = CORC
              IF( NCO .EQ. MXCOR ) GOTO 29
            ENDIF
          ENDDO E20
!
29        IF( NCO .NE. 0 ) WRITE(92,1011) IOP,HT1,COPAJ(IOP)(:NCL1),(NUCOR(K),COR(K),K=1,NCO)
        ENDDO
      ENDDO
      CLOSE(92)
!
      RETURN
      END SUBROUTINE XWPAES
