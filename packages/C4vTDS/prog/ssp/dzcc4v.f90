!
!                                 ( ICT1  , ICT2  , ICT12 , ICT512)
! *** CALCUL DES SYMBOLES 12CC4V  ( ICT3  , ICT4  , ICT34 , ICT513)
!                                 ( ICT13 , ICT24 , ICT5  , ICT   )
!
! *** METHODE ' ELLIOTT '
!     (M. ROTGER 3/99. D'APRES SPG DZCA2.F GABARD - 1 / 93 )
!
      FUNCTION DZCC4V(ICT1,ICT2,ICT12,ICT512,ICT3,ICT4,ICT34,ICT513,ICT13,ICT24,ICT5,ICT)
!
      use mod_dppr
      use mod_par_tds
      use mod_com_sy
      IMPLICIT NONE
      real(kind=dppr)  :: DZCC4V
      integer          :: ICT1,ICT2,ICT12,ICT512,ICT3,ICT4,ICT34,ICT513,ICT13,ICT24,ICT5,ICT

! function
      real(kind=dppr)  :: SXC1C4
      integer          :: CTRC4V

      real(kind=dppr)  :: S,SIGN

      integer          :: ICTX
!
! RELATIONS TRIANGULAIRES DU 12C
      IF( CTRC4V(ICT1,ICT2,ICT12)   .EQ. 0 ) GOTO 100
      IF( CTRC4V(ICT5,ICT12,ICT512) .EQ. 0 ) GOTO 100
      IF( CTRC4V(ICT1,ICT3,ICT13)   .EQ. 0 ) GOTO 100
      IF( CTRC4V(ICT5,ICT13,ICT513) .EQ. 0 ) GOTO 100
      IF( CTRC4V(ICT2,ICT4,ICT24)   .EQ. 0 ) GOTO 100
      IF( CTRC4V(ICT3,ICT4,ICT34)   .EQ. 0 ) GOTO 100
      IF( CTRC4V(ICT512,ICT34,ICT)  .EQ. 0 ) GOTO 100
      IF( CTRC4V(ICT513,ICT24,ICT)  .EQ. 0 ) GOTO 100
! SOMMATION
      S = 0.D0
E1:   DO ICTX=1,MXSYM
! RELATIONS TRIANGULAIRES LIEES A LA METHODE ELLIOTT
        IF( CTRC4V(ICT24,ICT34,ICTX)   .EQ. 0 ) CYCLE E1
        IF( CTRC4V(ICT3,ICTX,ICT2)     .EQ. 0 ) CYCLE E1
        IF( CTRC4V(ICT513,ICT512,ICTX) .EQ. 0 ) CYCLE E1
        S = S+PC4V(ICTX)*DC4(ICTX)*SXC1C4(ICT24,ICT34,ICTX,ICT512,ICT513,ICT)*  &
                                   SXC1C4(ICT2,ICT3,ICTX,ICT34,ICT24,ICT4)*     &
                                   SXC1C4(ICT3,ICTX,ICT2,ICT12,ICT1,ICT13)*     &
                                   SXC1C4(ICT513,ICT512,ICTX,ICT12,ICT13,ICT5)
      ENDDO E1
      SIGN   = PC4V(ICT1)*PC4V(ICT2)*PC4V(ICT12)*PC4V(ICT512)*            &
               PC4V(ICT3)*PC4V(ICT4)*PC4V(ICT34)*                         &
               PC4V(ICT513)*PC4V(ICT13)*PC4V(ICT24)*PC4V(ICT5)*PC4V(ICT)
      DZCC4V = S*SIGN
      RETURN
!
100   DZCC4V = 0.D0
!
      RETURN
      END FUNCTION DZCC4V
