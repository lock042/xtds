!
!                                 ( IC1  , IC2  , IC12 , IC512)
! *** CALCUL DES SYMBOLES 12C     ( IC3  , IC4  , IC34 , IC513)
!                                 ( IC13 , IC24 , IC5  , IC   )
!
! *** METHODE ' ELLIOTT '
!     (D'APRES SPG DZCA2.F GABARD - 1 / 93 )
!
      FUNCTION DZC(IC1,IC2,IC12,IC512,IC3,IC4,IC34,IC513,IC13,IC24,IC5,IC)
      use mod_dppr
      use mod_par_tds
      use mod_com_sy
      IMPLICIT NONE
      real(kind=dppr)  :: DZC
      integer          :: IC1,IC2,IC12,IC512,IC3,IC4,IC34,IC513,IC13,IC24,IC5,IC

! functions
      real(kind=dppr)  :: SXC
      integer          :: CTR

      real(kind=dppr)  :: S,SIGN

      integer          :: ICX
!
! RELATIONS TRIANGULAIRES DU 12C
      IF( CTR(IC1,IC2,IC12)   .EQ. 0 ) GOTO 100
      IF( CTR(IC5,IC12,IC512) .EQ. 0 ) GOTO 100
      IF( CTR(IC1,IC3,IC13)   .EQ. 0 ) GOTO 100
      IF( CTR(IC5,IC13,IC513) .EQ. 0 ) GOTO 100
      IF( CTR(IC2,IC4,IC24)   .EQ. 0 ) GOTO 100
      IF( CTR(IC3,IC4,IC34)   .EQ. 0 ) GOTO 100
      IF( CTR(IC512,IC34,IC)  .EQ. 0 ) GOTO 100
      IF( CTR(IC513,IC24,IC)  .EQ. 0 ) GOTO 100
! SOMMATION
      S = 0.D0
E1:   DO ICX=1,MXSYR
! RELATIONS TRIANGULAIRES LIEES A LA METHODE ELLIOTT
        IF( CTR(IC24,IC34,ICX)   .EQ. 0 ) CYCLE E1
        IF( CTR(IC3,ICX,IC2)     .EQ. 0 ) CYCLE E1
        IF( CTR(IC513,IC512,ICX) .EQ. 0 ) CYCLE E1
        S = S+PC(ICX)*DC(ICX)*SXC(IC24,IC34,ICX,IC512,IC513,IC)*  &
                              SXC(IC2,IC3,ICX,IC34,IC24,IC4)*     &
                              SXC(IC3,ICX,IC2,IC12,IC1,IC13)*     &
                              SXC(IC513,IC512,ICX,IC12,IC13,IC5)
      ENDDO E1
      SIGN = PC(IC1)*PC(IC2)*PC(IC12)*PC(IC512)*PC(IC3)*PC(IC4)*PC(IC34)*  &
             PC(IC513)*PC(IC13)*PC(IC24)*PC(IC5)*PC(IC)
      DZC  = S*SIGN
      RETURN
!
100   DZC = 0.D0
!
      RETURN
      END FUNCTION DZC
