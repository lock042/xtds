!
! ***** Symboles 3 - C de C4v
!
! ***** M. Rotger 12/98
!
      FUNCTION TRCC4V(ICT1,ICT2,ICT3,IST1,IST2,IST3)
      use mod_dppr
      use mod_par_tds
      IMPLICIT NONE
      real(kind=dppr)  :: TRCC4V
      integer          :: ICT1,ICT2,ICT3,IST1,IST2,IST3

! function
      real(kind=dppr)  :: TRCO4
      integer          :: CTR

      real(kind=dppr)  :: COEFF
      real(kind=dppr)  :: RES,RESB
      real(kind=dppr)  :: SUM

      integer ,dimension(MXSYM)    :: IDIM  = (/ 1, 1, 1, 1, 2 /)
      integer ,dimension(MXSYM)    :: IDIMB = (/ 3, 3, 3, 3, 4 /)
      integer ,dimension(MXSYM,4)  :: ISYM  = reshape( source = (/ 1, 1, 2, 2, 4,        &
                                                                   3, 3, 3, 3, 5,        &
                                                                   4, 4, 5, 5, 4,        &
                                                                   0, 0, 0, 0, 5  /),    &
                                                       shape  = shape(ISYM)        )
      integer ,dimension(MXSYM,4)  :: IPAR  = reshape( source = (/ 1, 2, 1, 2, 1,        &
                                                                   1, 2, 1, 2, 1,        &
                                                                   2, 1, 2, 1, 2,        &
                                                                   0, 0, 0, 0, 2  /),    &
                                                       shape  = shape(IPAR)        )
      integer ,dimension(2,3)      :: ITAB
      integer          :: IC1,IC2,IC3,IP1,IP2,IP3,ITRI
      integer          :: J,J1,J2,J3,JBIS,JTER
!
      TRCC4V    = 0.D0
      COEFF     = 1.D0
      ITAB(1,1) = ICT1
      ITAB(1,2) = ICT2
      ITAB(1,3) = ICT3
      ITAB(2,1) = IST1
      ITAB(2,2) = IST2
      ITAB(2,3) = IST3
      CALL ORDC4V(ITAB,COEFF)
E50:  DO J=1,IDIMB(ITAB(1,1))
        IC1 = ISYM(ITAB(1,1),J)
        IP1 = IPAR(ITAB(1,1),J)
        IF( IC1*IP1 .EQ. 0 ) CYCLE E50
E60:    DO JBIS=1,IDIMB(ITAB(1,2))
          IC2 = ISYM(ITAB(1,2),JBIS)
          IP2 = IPAR(ITAB(1,2),JBIS)
          IF( IC2*IP2 .EQ. 0 ) CYCLE E60
E70:      DO JTER=1,IDIMB(ITAB(1,3))
            IC3 = ISYM(ITAB(1,3),JTER)
            IP3 = IPAR(ITAB(1,3),JTER)
            IF( IC3*IP3 .EQ. 0 ) CYCLE E70
            ITRI = CTR(IC1,IC2,IC3)
            IF( ITRI .LE. 0 ) CYCLE E70
            RES = TRCO4(IC1,IC2,IC3,IP1,IP2,IP3,ITAB(1,1),ITAB(1,2),ITAB(1,3),ITAB(2,1),ITAB(2,2),ITAB(2,3))
            IF( RES .NE. 0 ) GOTO 100
          ENDDO E70
        ENDDO E60
      ENDDO E50
      RETURN
!
100   SUM = 0.D0
      DO J1=1,IDIM(ITAB(1,1))
        DO J2=1,IDIM(ITAB(1,2))
          DO J3=1,IDIM(ITAB(1,3))
            RESB = TRCO4(IC1,IC2,IC3,IP1,IP2,IP3,ITAB(1,1),ITAB(1,2),ITAB(1,3),J1,J2,J3)
            SUM  = SUM+RESB*RESB
          ENDDO
        ENDDO
      ENDDO
      TRCC4V = 1.D0/SQRT(SUM)*RES*COEFF
!
      RETURN
      END FUNCTION TRCC4V
