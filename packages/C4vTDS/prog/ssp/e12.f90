!
! SMIL G.PIERRE JUIN 82
! MOD. T.GABARD DEC 92
! E123 MODIFIE 03/98 V. BOUDON ---> XY6/Oh.
!
!     E12=<(IB1*IB2)//(IA1*IA2)//(IK1*IK2)>
!     IPM=+1 POUR UN OPERATEUR CREATION
!     IPM=-1 POUR UN OPERATEUR ANNIHILATION
!
      FUNCTION E12(IPM,IB,IA,IK)
      use mod_dppr
      use mod_par_tds
      use mod_com_sy
      IMPLICIT NONE
      real(kind=dppr)  :: E12
      integer ,dimension(NBVQN+2)  :: IB,IA,IK
      integer          :: IPM

! functions
      real(kind=dppr)  :: EMRVA,EUFC

      real(kind=dppr)  :: EUF
      real(kind=dppr)  :: R1,R2,RC

      integer          :: IAC1,IAC2,IAL1,IAL2,IAN1,IAN2,IAP1,IAP2,IAV1,IAV2
      integer          :: IBC1,IBC2,IBL1,IBL2,IBN1,IBN2,IBP1,IBP2,IBV1,IBV2
      integer          :: IKC1,IKC2,IKL1,IKL2,IKN1,IKN2,IKP1,IKP2,IKV1,IKV2
!
      E12 = 0.D0
      CALL VLNC(IB(1),IBV1,IBL1,IBN1,IBC1,IBP1)
      CALL VLNC(IA(1),IAV1,IAL1,IAN1,IAC1,IAP1)
      CALL VLNC(IK(1),IKV1,IKL1,IKN1,IKC1,IKP1)
      CALL VLNC(IB(2),IBV2,IBL2,IBN2,IBC2,IBP2)
      CALL VLNC(IA(2),IAV2,IAL2,IAN2,IAC2,IAP2)
      CALL VLNC(IK(2),IKV2,IKL2,IKN2,IKC2,IKP2)
      IF( IAN1 .NE. 0 .OR.           &
          IAN2 .NE. 0      ) RETURN
      RC  = SQRT(DC(IBC2)*DC(IAC2)*DC(IKC2))
      EUF = EUFC(IBC1,IBC2,IBC2,IAC1,IAC2,IAC2,IKC1,IKC2,IKC2)
      R1  = EMRVA(1,IBV1,IBL1,IBN1,IBC1,IPM,IAV1,IAL1,IAC1,IKV1,IKL1,IKN1,IKC1)
      R2  = EMRVA(2,IBV2,IBL2,IBN2,IBC2,IPM,IAV2,IAL2,IAC2,IKV2,IKL2,IKN2,IKC2)
      E12 = RC*EUF*R1*R2
!
      RETURN
      END FUNCTION E12
