!
! E.M.R.V. COUPLAGE EXTERNE
!
!
! SMIL G.PIERRE JUIN 82
! MOD. T.GABARD DEC 92
! E1234 MODIFIE 03/98 V. BOUDON ---> XY6/Oh.
!
!     E1234=<(IB123*IB4)//(IA123*IA4)//(IK123*IK4)>
!     IPM=+1 POUR UN OPERATEUR CREATION
!     IPM=-1 POUR UN OPERATEUR ANNIHILATION
!
      FUNCTION E1234(IPM,IB,IA,IK)
      use mod_dppr
      use mod_par_tds
      use mod_com_sy
      IMPLICIT NONE
      real(kind=dppr)  :: E1234
      integer ,dimension(NBVQN+2)  :: IB,IA,IK
      integer          :: IPM

! functions
      real(kind=dppr)  :: E123,EMRVA,EUFC

      real(kind=dppr)  :: EUF
      real(kind=dppr)  :: R123,R4,RC

      integer          :: IA2,IA23,IA234,IA2345,IATO6
      integer          :: IAC4,IAL4,IAN4,IAP4,IAV4
      integer          :: IB2,IB23,IB234,IB2345,IBTO6
      integer          :: IBC4,IBL4,IBN4,IBP4,IBV4
      integer          :: IK2,IK23,IK234,IK2345,IKTO6
      integer          :: IKC4,IKL4,IKN4,IKV4
!
      E1234 = 0.D0
      CALL VLNC(IB(4),IBV4,IBL4,IBN4,IBC4,IBP4)
      CALL VLNC(IA(4),IAV4,IAL4,IAN4,IAC4,IAP4)
      CALL VLNC(IK(4),IKV4,IKL4,IKN4,IKC4,IAP4)
      IF( IAN4 .NE. 0 ) RETURN
      CALL VLNC(IB(7),IB2,IB23,IB234,IB2345,IBTO6)
      CALL VLNC(IA(7),IA2,IA23,IA234,IA2345,IATO6)
      CALL VLNC(IK(7),IK2,IK23,IK234,IK2345,IKTO6)
      RC    = SQRT(DC(IK234)*DC(IA234)*DC(IB234))
      EUF   = EUFC(IB23,IBC4,IB234,IA23,IAC4,IA234,IK23,IKC4,IK234)
      R4    = EMRVA(4,IBV4,IBL4,IBN4,IBC4,IPM,IAV4,IAL4,IAC4,IKV4,IKL4,IKN4,IKC4)
      R123  = E123(IPM,IB,IA,IK)
      E1234 = RC*EUF*R123*R4
!
      RETURN
      END FUNCTION E1234
