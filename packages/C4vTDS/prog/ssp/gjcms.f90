!
! *** SYMBOLES G DE SO(3)
!
! SMIL CHAMPION DEC 78
! REV    JAN 1995 JPC,CW (PARAMETER)
!
      SUBROUTINE GJCMS(J,M,N,IC,IS,G,IP)
      use mod_dppr
      use mod_com_lg
      use mod_com_sy
      IMPLICIT NONE
      real(kind=dppr)  :: G
      integer          :: J,M,N,IC,IS,IP

      integer          :: I,I1,I2,IC1,ICO,IM
      integer          :: JP1,JP2
      integer          :: NN
!
      G   =  0.D0
      IP  = -1
      IC1 = IC
      IM  = ABS(M)
      NN  = N
      ICO = IC1+10*NN+1000*IM
      IF    ( J .LT. 0 ) THEN
        RETURN
      ELSEIF( J .GT. 0 ) THEN
        GOTO 6
      ENDIF
      IF( ICO .NE. 1 ) RETURN
      G  = 1.D0
      IP = 0
      RETURN
!
6     JP1 = J+1
      I1  = IN(JP1)
      JP2 = J+2
      I2  = IN(JP2)-1
! TRI DICHOTOMIQUE
      IF( ICO .LT. ICOD(I1) ) RETURN
      IF( ICO .GT. ICOD(I2) ) RETURN
      IF( ICO .EQ. ICOD(I2) ) GOTO 12
!
1     I = (I1+I2)/2
      IF    ( ICO .EQ. ICOD(I) ) THEN
        GOTO 7
      ELSEIF( ICO .GT. ICOD(I) ) THEN
        GOTO 3
      ENDIF
      IF( I2-I1 .EQ. 1 ) RETURN
      I2 = I
      GOTO 1
!
3     IF( I2-I1 .EQ. 1 ) RETURN
      I1 = I
      GOTO 1
!
12    I = I2
!
7     IF    ( IC .LT. 3 ) THEN
        GOTO 11
      ELSEIF( IC .GT. 3 ) THEN
        GOTO 8
      ENDIF
      IF    ( IS .EQ. 2 ) THEN
        GOTO 113
      ELSEIF( IS .GT. 2 ) THEN
        RETURN
      ENDIF
      IF( IM .EQ. (IM/4)*4 ) THEN
        GOTO 11
      ELSE
        RETURN
      ENDIF
!
113   IF( IM+2 .EQ. ((IM+2)/4)*4 ) THEN
        GOTO 11
      ELSE
        RETURN
      ENDIF
!
8     IF    ( IS .LT. 3 ) THEN
        GOTO 100
      ELSEIF( IS .GT. 3 ) THEN
        RETURN
      ENDIF
      IF    ( IC .LT. 4 ) THEN
        RETURN
      ELSEIF( IC .GT. 4 ) THEN
        GOTO 10
      ENDIF
      IF( IM/4*4 .EQ. IM ) THEN
        GOTO 11
      ELSE
        RETURN
      ENDIF
!
100   IF( (IM-1)/2*2 .EQ. IM-1 ) THEN
        GOTO 11
      ELSE
        RETURN
      ENDIF
!
10    IF( (IM-2)/4*4 .NE. IM-2 ) RETURN
!
11    IP = 0
      G  = GJ(I)
      IF( J  .NE. (J/2)*2 ) GOTO 31
      IF( IC .LE. 3       ) THEN
        GOTO 16
      ELSE
        GOTO 13
      ENDIF
!
31    IF( IC .LE. 3 ) THEN
        GOTO 13
      ELSE
        GOTO 16
      ENDIF
!
13    IP = 1
      IF( M .LT. 0 ) GOTO 18
      G = G*( (-1)**(M+1) )
      GOTO 18
!
16    IF( M .LT. 0 ) GOTO 18
      G = G*( (-1)**M )
!
18    IF( IC .LE. 3 ) RETURN
      IF( IS .NE. 2 ) RETURN
      G  = PC(IC)*G
      IP = IP-M
      IP = IP-IP/4*4
      IF( IP .GE. 0 ) GOTO 22
      IP = IP+4
!
22    IF( IP/2 .LE. 0 ) RETURN
      G  = -G
      IP = IP-2
!
      RETURN
      END SUBROUTINE GJCMS
