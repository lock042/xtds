!
! ***** Test : icp contenu dans ict ?
!
! ***** M. Rotger 12/98
!
      FUNCTION ISCTC(IC,IP,ICT)
      use mod_par_tds
      use mod_com_sy
      IMPLICIT NONE
      integer          :: ISCTC
      integer          :: IC,IP,ICT

      integer ,dimension(2*MXSYR,2)  :: IX = reshape( source = (/ 1, 3, 1, 2, 4, 2, 4, 2, 1, 3,        &
                                                                  0, 0, 3, 5, 5, 0, 0, 4, 5, 5  /),    &
                                                      shape  = shape(IX)                            )
      integer          :: J,JMAX
!
      ISCTC = 0
      IF( IC .LT. 3 ) THEN
        JMAX = 1
        GOTO 11
      ENDIF
      JMAX = 2
11    DO J=1,JMAX
        IF( IX(IC+MXSYR*(IP-1),J) .EQ. ICT ) ISCTC = 1
      ENDDO
!
      RETURN
      END FUNCTION ISCTC
