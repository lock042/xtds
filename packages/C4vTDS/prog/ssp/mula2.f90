!
! ***** Multiplication par a2 dans C4v
!
! ***** M. Rotger 3/99
!
      SUBROUTINE MULA2(ICS,IST,ICI,ISIG)
      use mod_par_tds
      IMPLICIT NONE
      integer          :: ICS,IST,ICI,ISIG

      integer ,dimension(MXSYM)  :: IPAR = (/ 1, 1, 1, 1, 2 /)
      integer ,dimension(MXSYM)  :: ISYM = (/ 2, 1, 4, 3, 5 /)
!
      IF( ICS .EQ. 1 .AND.         &
          IST .EQ. 1       ) THEN
        ICI  = ISYM(1)
        ISIG = IPAR(1)
      ENDIF
      IF( ICS .EQ. 2 .AND.         &
          IST .EQ. 1       ) THEN
        ICI  = ISYM(2)
        ISIG = IPAR(2)
      ENDIF
      IF( ICS .EQ. 3 .AND.         &
          IST .EQ. 1       ) THEN
        ICI  = ISYM(3)
        ISIG = IPAR(3)
      ENDIF
      IF( ICS .EQ. 4 .AND.         &
          IST .EQ. 1       ) THEN
        ICI  = ISYM(4)
        ISIG = IPAR(4)
      ENDIF
      IF( ICS .EQ. 5 .AND.         &
          IST .EQ. 1       ) THEN
        ICI  = ISYM(5)
        ISIG = IPAR(5)
      ENDIF
!
      RETURN
      END SUBROUTINE MULA2
