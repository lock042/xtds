      PROGRAM DIPMOD
!
!  14.12.88 FORTRAN 77 DU SUN4 REV 13 JAN 1989
!  REV 25 JAN 1990
!  MOD. DEC. 92 T.GABARD
! REV    JAN 1995 JPC,CW (PARAMETER)
! MODIFIE 03/98 V. BOUDON ---> XY6/Oh.
! MODIFIE 02/99 M. ROTGER ---> XY5Z/C4v.
!
!   CODAGE DES OPERATEURS ROVIBRATIONNELS DU MOMENT DIPOLAIRE EFFECTIF
!   ENTRE DES RESTRICTIONS DONNEES DE POLYADES VIBRATIONNELLES TYPE CH4
!
!     LECTURE DES G  MXDROT
!
!     CALCUL DES K  JMKCUB = FIXE DANS LE PROGRAMME
!
! APPEL : dipmod Pn  Nm  v1  v2  v3  v4  v5  v6   ...   w1  w2  w3  w4  w5  w6            |
!                    Nm' v1' v2' v3' v4' v5' v6'  ...   w1' w2' w3' w4' w5' w6'           | (n+1 lines)
!                    ...........................
!                Pn' Nm" v1" v2" v3" v4" v5" v6"  ...   w"1 w"2 w"3 w"4 w"5 w"6           |
!                    ...........................                                          | (n'+1 lines)
!                Dp
!
!  *****  LIMITATIONS DU PROGRAMME GEREES PAR PARAMETER.
!
!     MXOPT                                                             !ICODVI:ICODRO:ICODVC:ICODVA:ICODSY
! NB MAXIMUM D'OPERATEURS ROVIBRATIONNELS
!     MXOCV                                                             !IRDRE:IDVI:IDRO:IDVC:IDVA:IDSY
! NB MAXIMUM D'OPERATEURS ROVIBRATIONNELS PAR CASE VIBRATIONNELLE
!     MXEMR                                                             !EMOP:LI:IC
! NB MAXIMUM D'E.M.R.V. NON NULS
!     MXSNV                                                             !IFFI:IFFS
! NB MAXIMUM TOTAL DE SOUS-NIVEAUX VIBRATIONNELS SUP ET INF
!     MXSNB                                                             !IKVC:IKVA
! NB MAXIMUM DE SOUS-NIVEAUX VIBRATIONNELS POUR UN ENSEMBLE DONNE DES VI
!     MXOPR                                                             !IROCO
! NB MAXIMUM D'OPERATEURS ROVIBRATIONNELS PAR OPERATEUR VIBRATIONNEL
!     MXPOL                                                             ! NVIBI:NVIBS:NIVII:NIVIS
! NB MAXIMUM DE POLYADES
!
      use mod_dppr
      use mod_par_tds
      use mod_com_fdate
      use mod_com_lg
      use mod_com_phase
      use mod_com_sy
      use mod_main_dipmod
      IMPLICIT NONE

! functions
      real(kind=dppr)  :: AKPRIM,ELMR
      integer          :: NSYM1,MULGU

      real(kind=dppr)  :: AKP
      real(kind=dppr)  :: EM,EM1,EM2,EN
      real(kind=dppr)  :: SP

      integer ,dimension(NBVQN+2)  :: IBELMR,ICELMR,IAELMR,IKELMR
      integer ,dimension(NBVQN)    :: ICA,ILA,INA,IPA,IVA
      integer ,dimension(NBVQN)    :: ICC,ILC,INC,IPC,IVC
      integer ,dimension(MDMIGA)   :: IGAMA
      integer ,dimension(NBVQN)    :: ICF,ICP,IL,INUM,IV
      integer ,dimension(0:MXPOL)  :: IORTR
      integer ,dimension(MXSYR)    :: IROT
      integer ,dimension(MXPOL)    :: NIVII,NIVIS
      integer          :: NFS_SUP,NFFS_SUP,IOP_SUP,NROT_SUP,NBOPT_SUP
      integer          :: IROTA_SUP,NIV_SUP
      integer          :: I,IB,IC2,IC23,IC234,IC2345,ICTO6,ICA2,ICA23,ICA234,ICATO5,ICATO6
      integer          :: ICC2,ICC23,ICC234,ICCTO5,ICCTO6,ICROT,ID,IDEGV,IDK
      integer          :: IDROT,IF,IF1,IF2,IG,IGA,IJK,IK,IM,IMIA,IOA
      integer          :: IOA1,IOC,IOC1,IOP,IOPA,IOPC,IP2,IP23,IP234,IP2345,IPTO6
      integer          :: IPA2,IPA23,IPA234,IPATO5,IPATO6
      integer          :: IPBC4,IPKC4
      integer          :: IPC2,IPC23,IPC234,IPCTO5,IPCTO6,IPGAMA
      integer          :: IRO,IRO1,IRO2,IRO3,IRO4,IRO5,IROTA
      integer          :: IU,IVI,IVS
      integer          :: ICBC4,ICC4V,ICC4VB,ICC4VK,ICC4VI,ICC4VS,ICKC4,IFC4I,IFC4S,ITEST
      integer          :: IX1,IXB1,IXK1,IX2,IXB2,IXK2,IX3,IXB3,IXK3,IX4,IXB4,IXB5,IXB6,IXB7,IXB8,IXK4
      integer          :: J,JD,JJJ,JMINF,JMKCUB,JMSUP
      integer          :: JC4V,JC4VB,JC4VK
      integer          :: KNVIB,KNIVP,KOP,KPOL,KPOLI,KPOLS,KVI
      integer          :: L,LOP
      integer          :: NBOPT,NBVA,NBVC,NFS,NFFI,NFFS,NFI,NGAMA,NIC
      integer          :: NFFC4I,NFFC4S,NSC4VI,NSC4VS
      integer          :: NPOLI,NPOLS,NROT,NS
      integer          :: N

      character(len =   1) ,dimension(0:MXPOL)  :: AORTR
      character(len =   1)  :: AVI,AVS
      character(len =  11)  :: CHAINE
      character(len = 120)  :: IFICH
!
1000  FORMAT('  EFFECTIVE ROVIBRATIONAL DIPOLE MOMENT :' , /,   &
             ' VIBRATIONAL REDUCED MATRIX ELEMENTS (RME)',//,   &
             A                                               )
1001  FORMAT(//)
1002  FORMAT(I6,'  |[[[[',                           &
             I2,'(',I1,',',I1,A,A,')*',              &
             I2,'(',I1,',',I1,A,A,')*',              &
             I2,'(',I1,',',I1,A,A,')]',A,A,'*',      &
             I2,'(',I1,',',I1,A,A,')]',A,A,'*',      &
             I2,'(',I1,',',I1,A,A,')]',A,A,'*',      &
             I2,'(',I1,',',I1,A,A,')]',A,A,', ',A,   &
             ' >'                                 )
1004  FORMAT(I4,' TH VIBRATIONAL OPERATOR' ,//,   &
             A,1X,' {[[[[',                       &
             I2,'(',I1,',',A,A,')*',              &
             I2,'(',I1,',',A,A,')*',              &
             I2,'(',I1,',',A,A,')]',A,A,'*',      &
             I2,'(',I1,',',A,A,')]',A,A,'*',      &
             I2,'(',I1,',',A,A,')]',A,A,'*',      &
             I2,'(',I1,',',A,A,')]',A,A,          &
             '} '                          , /,   &
             2X,' {[[[[',                         &
             I2,'(',I1,',',A,A,')*',              &
             I2,'(',I1,',',A,A,')*',              &
             I2,'(',I1,',',A,A,')]',A,A,'*',      &
             I2,'(',I1,',',A,A,')]',A,A,'*',      &
             I2,'(',I1,',',A,A,')]',A,A,'*',      &
             I2,'(',I1,',',A,A,')]',A,A,          &
             '} ',A,A,', ',A                   )
1006  FORMAT(I1)
1007  FORMAT(I2)
1010  FORMAT(A)
1029  FORMAT(/,                     &
             I5,' NON ZERO RME',/)
1030  FORMAT(2X,A1,'<',I5,'|',3X,'|',I5,'>',3X,F19.13)
1100  FORMAT(/,                                                  &
             I4,' VIBRATIONAL SUBLEVEL(S) OF UPPER POLYAD :',/)
1133  FORMAT(/,                                                  &
             I4,' VIBRATIONAL SUBLEVEL(S) OF LOWER POLYAD :',/)
1199  FORMAT(/,              &
             'DIPMOD : ',A)
1205  FORMAT(' UPPER LEVEL ',I2,' : (v1;v2;v3;v4;v5;v6) = (',5(I2,';'),I2,')')
1206  FORMAT(/,                                     &
             ' POLYAD ',I2,'  MINUS POLYAD ',I2,/)
1207  FORMAT(' LOWER LEVEL ',I2,' : (v1;v2;v3;v4;v5;v6) = (',5(I2,';'),I2,')')
1215  FORMAT(/,                                             &
             ' DIPOLE MOMENT DEVELOPMENT ORDER :  ',9(I2))                                         ! 9 = MXPOL-1
1218  FORMAT(/,                               &
             I4,' ROTATIONAL OPERATOR(S)',/)
1219  FORMAT(/,           &
             1X,46('-'))
1220  FORMAT(I4,' ROVIBRATIONAL OPERATORS',/)
1313  FORMAT(I4,I3,'(',I1,',',I1,A,A,1X,A,')',1X,6I1,A,A,1X,6I1,A,A,1X,A,A,1X,A,1X,A,I8,I7)        ! 6 = NBVQN
2001  FORMAT(' >> ',A)
8000  FORMAT(' !!! DIPMOD : STOP ON ERROR')
8001  FORMAT(' !!! ERROR OPENING CONTROL FILE')
8002  FORMAT(' !!! INCOMPLETE CONTROL FILE')
8003  FORMAT(' !!! INCOMPATIBLE ARGUMENTS')
!
! ARRAY INITIAL ALLOCATION
!
      CALL ALLOC_ALL
!
      NFS_SUP   = -1
      NFFS_SUP  = -1
      IOP_SUP   = -1
      NROT_SUP  = -1
      NBOPT_SUP = -1
      IROTA_SUP = -1
      NIV_SUP   = -1
      OPEN(10,FILE=CTRLF,ERR=9998,STATUS='OLD')
      READ(10,1010,END=9997) FDATE
      PRINT 1199,            FDATE
!
! NUMERO DE LA POLYADE SUPERIEURE
!
      READ(10,1010,END=9997) CHAINE
      AVS = CHAINE(2:2)
      READ(AVS,1006) IVS
      NPOLS = IVS+1
!
! LECTURE DES NIVEAUX VIBRATIONNELS
!
      JMSUP = 0
      DO KPOL=1,NPOLS
        READ(10,1010,END=9997) CHAINE
        READ(CHAINE(2:3),1007) NIVIS(KPOL)
        IF( NIVIS(KPOL) .GT. NIV_SUP ) NIV_SUP = NIVIS(KPOL)
        DO WHILE( NIVIS(KPOL) .GT. MXNIV )
          CALL RESIZE_MXNIV
        ENDDO
        DO KNIVP=1,NIVIS(KPOL)
          DO KNVIB=1,NBVQN
            READ(10,1006,END=9997) NVIBS(KPOL,KNIVP,KNVIB)
            IF( KNVIB .GT. 2 ) JMSUP = MAX(JMSUP,NVIBS(KPOL,KNIVP,KNVIB))
          ENDDO
        ENDDO
      ENDDO
!
! NUMERO DE LA POLYADE INFERIEURE
!
      READ(10,1010,END=9997) CHAINE
      AVI = CHAINE(2:2)
      READ(AVI,1006) IVI
      NPOLI = IVI+1
!
! LECTURE DES NIVEAUX VIBRATIONNELS
!
      JMINF = 0
      DO KPOL=1,NPOLI
        READ(10,1010,END=9997) CHAINE
        READ(CHAINE(2:3),1007) NIVII(KPOL)
        IF( NIVII(KPOL) .GT. NIV_SUP ) NIV_SUP = NIVII(KPOL)
        DO WHILE( NIVII(KPOL) .GT. MXNIV )
          CALL RESIZE_MXNIV
        ENDDO
        DO KNIVP=1,NIVII(KPOL)
          DO KNVIB=1,NBVQN
            READ(10,1006,END=9997) NVIBI(KPOL,KNIVP,KNVIB)
            IF( KNVIB .GT. 2 ) JMINF = MAX(JMINF,NVIBI(KPOL,KNIVP,KNVIB))
          ENDDO
        ENDDO
      ENDDO
      JMKCUB = MAX(JMINF,JMSUP)
!
! ORDRE DU DEVELOPPEMENT
!
      READ(10,1010,END=9997) CHAINE
      IF( LEN_TRIM(CHAINE) .NE. IVI+2 ) GOTO 9996
      DO KVI=0,IVI
        I          = KVI+2
        AORTR(KVI) = CHAINE(I:I)
        READ(AORTR(KVI),1006) IORTR(KVI)
      ENDDO
!
!  NOM DU FICHIER DE SORTIE
!
      IFICH = 'MD_P'//AVS//'mP'//AVI//'_D'
      DO KVI=0,IVI
        IFICH = TRIM(IFICH)//AORTR(KVI)
      ENDDO
      CLOSE(10)
      PRINT 2001, TRIM(IFICH)
!
! INITIALISATIONS
!
      CALL LECTG(MXDROT)
      CALL FACTO
      CALL CAL6C
      CALL CALKM(JMKCUB)
      OPEN(20,FILE=IFICH,STATUS='UNKNOWN')
      WRITE(20,1000) FDATE
      WRITE(20,1206) IVS,IVI
      DO I=1,NIVIS(NPOLS)
        WRITE(20,1205) I,(NVIBS(NPOLS,I,J),J=1,NBVQN)
      ENDDO
      DO I=1,NIVII(NPOLI)
        WRITE(20,1207) I,(NVIBI(NPOLI,I,J),J=1,NBVQN)
      ENDDO
!
!     CODAGE DES SOUS-NIVEAUX VIBRATIONNELS
!
!     SOUS-NIVEAUX SUPERIEURS.
!
      NFFS   = 0
      NFFC4S = 0
      DO IF1=1,NIVIS(NPOLS)
        DO IF2=1,NBVQN
          IV(IF2) = NVIBS(NPOLS,IF1,IF2)
        ENDDO
        CALL SIGVI(IV,'IFFS',NFFS,NFS)
        CALL VLNC(IFFS(NBVQN+1,NFFS+1),IX1,IX2,IX3,IX4,ICTO6)
        CALL VLNC(IFFS(NBVQN+2,NFFS+1),IX1,IX2,IX3,IX4,IPTO6)
        NSC4VS = 0
E100:   DO JC4V=1,2
          IF( IC4VV(ICTO6,IPTO6,JC4V) .EQ. 0 ) CYCLE E100
          NSC4VS = NSC4VS+1
        ENDDO E100
        NFFS   = NFFS+NFS
        NFFC4S = NFFC4S+NFS*NSC4VS
        IF( NFS  .GT. NFS_SUP  ) NFS_SUP  = NFS
        IF( NFFS .GT. NFFS_SUP ) NFFS_SUP = NFFS
      ENDDO
      WRITE(20,1100) NFFC4S
      IFC4S = 0
      DO IF=1,NFFS
        DO I=1,NBVQN
          CALL VLNC(IFFS(I,IF),IV(I),IL(I),INUM(I),ICF(I),ICP(I))
        ENDDO
        CALL VLNC(IFFS(NBVQN+1,IF),IC2,IC23,IC234,IC2345,ICTO6)
        CALL VLNC(IFFS(NBVQN+2,IF),IP2,IP23,IP234,IP2345,IPTO6)
        DO J=1,2
          ICC4VS = IC4VV(ICTO6,IPTO6,J)
          IF( ICC4VS .NE. 0 ) THEN
          IFC4S = IFC4S+1
          WRITE(20,1002) IFC4S,                                                &
                         (IV(I),IL(I),INUM(I),KC(ICF(I)),ACGU(ICP(I)),I=1,3),  &
                         KC(IC23),ACGU(IP23),                                  &
                          IV(4),IL(4),INUM(4),KC(ICF(4)),ACGU(ICP(4)),         &
                         KC(IC234),ACGU(IP234),                                &
                          IV(5),IL(5),INUM(5),KC(ICF(5)),ACGU(ICP(5)),         &
                         KC(IC2345),ACGU(IP2345),                              &
                          IV(6),IL(6),INUM(6),KC(ICF(6)),ACGU(ICP(6)),         &
                         KC(ICTO6),ACGU(IPTO6),SYM(ICC4VS)
          ENDIF
        ENDDO
      ENDDO
!
!     SOUS-NIVEAUX INFERIEURS.
!
      NFFI   = 0
      NFFC4I = 0
      DO IF1=1,NIVII(NPOLI)
        DO IF2=1,NBVQN
          IV(IF2) = NVIBI(NPOLI,IF1,IF2)
        ENDDO
        CALL SIGVI(IV,'IFFI',NFFI,NFI)
        CALL VLNC(IFFI(NBVQN+1,NFFI+1),IX1,IX2,IX3,IX4,ICTO6)
        CALL VLNC(IFFI(NBVQN+2,NFFI+1),IX1,IX2,IX3,IX4,IPTO6)
        NSC4VI = 0
E120:   DO JC4V=1,2
          IF( IC4VV(ICTO6,IPTO6,JC4V) .EQ. 0 ) CYCLE E120
          NSC4VI = NSC4VI+1
        ENDDO E120
        NFFI   = NFFI+NFI
        NFFC4I = NFFC4I+NFI*NSC4VI
      ENDDO
      WRITE(20,1133) NFFC4I
      IFC4I = 0
      DO IF=1,NFFI
        DO I=1,NBVQN
          CALL VLNC(IFFI(I,IF),IV(I),IL(I),INUM(I),ICF(I),ICP(I))
        ENDDO
        CALL VLNC(IFFI(NBVQN+1,IF),IC2,IC23,IC234,IC2345,ICTO6)
        CALL VLNC(IFFI(NBVQN+2,IF),IP2,IP23,IP234,IP2345,IPTO6)
        DO J=1,2
          ICC4VI = IC4VV(ICTO6,IPTO6,J)
          IF( ICC4VI .NE. 0 ) THEN
            IFC4I = IFC4I+1
            WRITE(20,1002) IFC4I,                                                &
                           (IV(I),IL(I),INUM(I),KC(ICF(I)),ACGU(ICP(I)),I=1,3),  &
                           KC(IC23),ACGU(IP23),                                  &
                            IV(4),IL(4),INUM(4),KC(ICF(4)),ACGU(ICP(4)),         &
                           KC(IC234),ACGU(IP234),                                &
                            IV(5),IL(5),INUM(5),KC(ICF(5)),ACGU(ICP(5)),         &
                           KC(IC2345),ACGU(IP2345),                              &
                            IV(6),IL(6),INUM(6),KC(ICF(6)),ACGU(ICP(6)),         &
                           KC(ICTO6),ACGU(IPTO6),SYM(ICC4VI)
          ENDIF
        ENDDO
      ENDDO
      WRITE(20,1215) (IORTR(I),I=0,IVI)
      LOP   = 0
      NBOPT = 0
!
!     BOUCLE SUR LES POLYADES INFERIEURES
!
E36:  DO KPOLI=1,NPOLI
!
!     DETERMINATION DES CREATEURS
!
E35:    DO IOC=1,NIVII(KPOLI)
          DO IOC1=1,NBVQN
            IVC(IOC1) = NVIBI(KPOLI,IOC,IOC1)
          ENDDO
          CALL SIGVI(IVC,'IKVC',0,NBVC)
!
!     POLYADES SUPERIEURES
!
          KPOLS = KPOLI+IVS-IVI
!
!     DETERMINATION DES ANNIHILATEURS
!
E33:      DO IOA=1,NIVIS(KPOLS)
            DO IOA1=1,NBVQN
              IVA(IOA1) = NVIBS(KPOLS,IOA,IOA1)
            ENDDO
            IF( IVI .EQ. IVS .AND.              &
                IOA .LT. IOC       ) CYCLE E33
            CALL SIGVI(IVA,'IKVA',0,NBVA)
            IDEGV = 0
            DO IOP=1,NBVQN
              IDEGV = IVA(IOP)+IVC(IOP)+IDEGV
            ENDDO
            IDROT = IORTR(KPOLI-1)+1-IDEGV
!
!     COUPLAGE CREATEURS-ANNIHILATEURS
!
E1:         DO IOPC=1,NBVC
              IMIA = 1
              IF( IOA .EQ. IOC ) IMIA = IOPC
              DO I=1,NBVQN
                CALL VLNC(IKVC(I,IOPC),IVC(I),ILC(I),INC(I),ICC(I),IPC(I))
              ENDDO
              CALL VLNC(IKVC(NBVQN+1,IOPC),ICC2,ICC23,ICC234,ICCTO5,ICCTO6)
              CALL VLNC(IKVC(NBVQN+2,IOPC),IPC2,IPC23,IPC234,IPCTO5,IPCTO6)
E2:           DO IOPA=IMIA,NBVA
                DO I=1,NBVQN
                  CALL VLNC(IKVA(I,IOPA),IVA(I),ILA(I),INA(I),ICA(I),IPA(I))
                ENDDO
                CALL VLNC(IKVA(NBVQN+1,IOPA),ICA2,ICA23,ICA234,ICATO5,ICATO6)
                CALL VLNC(IKVA(NBVQN+2,IOPA),IPA2,IPA23,IPA234,IPATO5,IPATO6)
                CALL MULTD(ICCTO6,ICATO6,NGAMA,IGAMA)
                IPGAMA = MULGU(IPCTO6,IPATO6)
                NROT   = 0
E111:           DO I=1,NGAMA
E734:             DO JC4V=1,2
                    IF( IC4VV(IGAMA(I),IPGAMA,JC4V) .EQ. 0 ) CYCLE E734
                    ICC4V = IC4VV(IGAMA(I),IPGAMA,JC4V)
E733:               DO IU=1,2
                      DO IJK=1,MXSYR
                        IROT(IJK) = 0
                      ENDDO
!
!     DETERMINATION DES OPERATEURS ROTATIONNELS POSSIBLES
!
                      IROTA = 0
                      IF( IDROT .LT. IU-1 ) CYCLE E733
                      DO ID=IU-1,IDROT,2
                        CALL NBJC(ID,1,IRO1,IRO2,IRO3,IRO4,IRO5)
                        IROT(1) = IROT(1)+IRO1
                        IROT(2) = IROT(2)+IRO2
                        IROT(3) = IROT(3)+IRO3
                        IROT(4) = IROT(4)+IRO4
                        IROT(5) = IROT(5)+IRO5
                        DO IDK=ID,0,-2
E361:                     DO ICROT=1,MXSYM
                            NIC = NSYM1(IDK,1,ICROT)
                            IF( NIC .EQ. 0 ) CYCLE E361
E370:                       DO JC4VB=1,2
                              IF( IC4VV(ICROT,1,JC4VB) .EQ. 0 ) CYCLE E370
                              ICC4VB = IC4VV(ICROT,1,JC4VB)
                              CALL MULC4V(ICC4VB,ICC4V,N,IC)
E380:                         DO ITEST=1,N
                                IF( IC(ITEST) .GT. 1 .AND.               &
                                    IC(ITEST) .LT. 5       ) CYCLE E380
                                DO NS=1,NIC
                                  IROTA = IROTA+1
                                  IF( IROTA .GT. IROTA_SUP ) IROTA_SUP = IROTA
                                  IF( IROTA .GT. MXOPR     ) CALL RESIZE_MXOPR
                                  IROCO(IROTA)  = 100000*ID+10000*IDK+              &
                                                  1000*ICROT+100*(NS-1)+10*ICC4VB+  &
                                                  ICC4V
                                  ICOTOT(IROTA) = IC(ITEST)
                                ENDDO
                              ENDDO E380
                            ENDDO E370
                          ENDDO E361
                        ENDDO
                      ENDDO
!
!    CALCUL DU FACTEUR DE NORMALISATION
!
                      IF( IROTA .EQ. 0 ) CYCLE E733
                      SP        = (-1)**(IU+ICATO6+ICCTO6+IGAMA(I))
                      IBELMR(:) = IKVC(:,IOPC)
                      ICELMR(:) = IKVC(:,IOPC)
                      IAELMR(:) = IKVA(:,IOPA)
                      IKELMR(:) = IKVA(:,IOPA)
                      EN        = ELMR(IBELMR,ICELMR,IAELMR,IKELMR,IGAMA(I))
                      IBELMR(:) = IKVC(:,IOPC)
                      ICELMR(:) = IKVA(:,IOPA)
                      IKELMR(:) = IKVC(:,IOPC)
                      IKELMR(:) = IKVA(:,IOPA)
                      EN        = ELMR(IBELMR,ICELMR,IAELMR,IKELMR,IGAMA(I))*SP+EN
                      IF( ABS(EN) .LT. 1.D-05 ) CYCLE E733
                      LOP = LOP+1
                      WRITE(20,1219)
                      WRITE(20,1004) LOP,AUG(IU),                                    &
                                     (IVC(J),ILC(J),KC(ICC(J)),ACGU(IPC(J)),J=1,3),  &
                                     KC(ICC23),ACGU(IPC23),                          &
                                      IVC(4),ILC(4),KC(ICC(4)),ACGU(IPC(4)),         &
                                     KC(ICC234),ACGU(IPC234),                        &
                                      IVC(5),ILC(5),KC(ICC(5)),ACGU(IPC(5)),         &
                                     KC(ICCTO5),ACGU(IPCTO5),                        &
                                      IVC(6),ILC(6),KC(ICC(6)),ACGU(IPC(6)),         &
                                     KC(ICCTO6),ACGU(IPCTO6),                        &
                                     (IVA(J),ILA(J),KC(ICA(J)),ACGU(IPA(J)),J=1,3),  &
                                     KC(ICA23),ACGU(IPA23),                          &
                                      IVA(4),ILA(4),KC(ICA(4)),ACGU(IPA(4)),         &
                                     KC(ICA234),ACGU(IPA234),                        &
                                      IVA(5),ILA(5),KC(ICA(5)),ACGU(IPA(5)),         &
                                     KC(ICATO5),ACGU(IPATO5),                        &
                                      IVA(6),ILA(6),KC(ICA(6)),ACGU(IPA(6)),         &
                                     KC(ICATO6),ACGU(IPATO6),                        &
                                     KC(IGAMA(I)),ACGU(IPGAMA),SYM(ICC4V)
!
!     CALCUL ET CODAGE DES E.M.R.V.
!
                      IOP = 0
E4:                   DO IB=1,NFFI
E5:                     DO IK=1,NFFS
                          CALL VLNC(IFFI(NBVQN+1,IB),IXB1,IXB2,IXB3,ICBC4,IXB4)
                          CALL VLNC(IFFS(NBVQN+1,IK),IXK1,IXK2,IXK3,ICKC4,IXK4)
                          CALL VLNC(IFFI(NBVQN+2,IB),IXB5,IXB6,IXB7,IXB8,IPBC4)
                          CALL VLNC(IFFS(NBVQN+2,IK),IXB5,IXB6,IXB7,IXB8,IPKC4)
E6:                       DO JC4VB=1,2
                            ICC4VB = IC4VV(ICBC4,IPBC4,JC4VB)
                            IF( ICC4VB .EQ. 0 ) CYCLE E6
E7:                         DO JC4VK=1,2
                              ICC4VK = IC4VV(ICKC4,IPKC4,JC4VK)
                              IF( ICC4VK .EQ. 0 ) CYCLE E7
                              AKP = AKPRIM(IGAMA(I),ICKC4,ICBC4,IPGAMA,IPKC4,IPBC4,ICC4V,ICC4VK,ICC4VB)
                              IF( AKP .EQ. 0.D0 ) CYCLE E7
                              IBELMR(:) = IFFI(:,IB)
                              ICELMR(:) = IKVC(:,IOPC)
                              IAELMR(:) = IKVA(:,IOPA)
                              IKELMR(:) = IFFS(:,IK)
                              EM1       = ELMR(IBELMR,ICELMR,IAELMR,IKELMR,IGAMA(I))
                              IBELMR(:) = IFFI(:,IB)
                              ICELMR(:) = IKVA(:,IOPA)
                              IAELMR(:) = IKVC(:,IOPC)
                              IKELMR(:) = IFFS(:,IK)
                              EM2       = ELMR(IBELMR,ICELMR,IAELMR,IKELMR,IGAMA(I))*SP
                              EM        = EM1+EM2
                              EM        = EM*AKP
                              IF( EM*EM .LT. 1.D-10 ) CYCLE E7
                              EM = EM/EN
                              IF( IU .EQ. 2 ) EM = -EM
! changement de convention pour les operateurs V dont les elements
! matriciels sont imaginaires (pour retablir, dans la majorite des cas,
! les anciens signes des parametres)     4 / 5 / 93
                              IOP = IOP+1
                              IF( IOP .GT. IOP_SUP ) IOP_SUP = IOP
                              IF( IOP .GT. MXEMR   ) CALL RESIZE_MXEMR
                              LI(IOP)   = JC4VK
                              IC(IOP)   = JC4VB
                              EMOP(IOP) = EM
                            ENDDO E7
                          ENDDO E6
                        ENDDO E5
                      ENDDO E4
                      WRITE(20,1029) IOP
                      DO KOP=1,IOP
                        WRITE(20,1030) AIM(IU),IC(KOP),LI(KOP),EMOP(KOP)
                      ENDDO
                      WRITE(20,1218) IROTA
!
!    CODAGE DES OPERATEURS ROVIBRATIONNELS
!
                      DO IRO=1,IROTA
                        NROT = NROT+1
                        IF( NROT .GT. NROT_SUP ) NROT_SUP = NROT
                        IF( NROT .GT. MXOCV    ) CALL RESIZE_MXOCV
                        ICTO(NROT) = ICOTOT(IRO)
                        IDRO(NROT) = IROCO(IRO)
                        IDVC(NROT) = IVC(1)*10000+IVC(2)*1000+IVC(3)*100+IVC(4)*10+IVC(5)
                        IDVA(NROT) = IVA(1)*10000+IVA(2)*1000+IVA(3)*100+IVA(4)*10+IVA(5)
                        IDSY(NROT) = ICCTO6*10000+ICATO6*1000+IGAMA(I)*100+IVC(6)*10+IVA(6)
                        IDVI(NROT) = LOP*10000+ICATO6*1000+IPCTO6*100+IPATO6*10+IPGAMA
                      ENDDO
                    ENDDO E733
                  ENDDO E734
                ENDDO E111
!
!    MISE EN ORDRE A L'INTERIEUR D'UNE CASE VIBRATIONNELLE DONNEE
!
                IF( NROT .EQ. 0 ) CYCLE E2
                CALL IRDER(NROT,IDRO,IRDRE,MXOCV)
                DO JJJ=1,NROT
                  NBOPT = NBOPT+1
                  IF( NBOPT .GT. NBOPT_SUP ) NBOPT_SUP = NBOPT
                  IF( NBOPT .GT. MXOPT     ) CALL RESIZE_MXOPT
                  IRO           = IRDRE(JJJ)
                  ICODTO(NBOPT) = ICTO(IRO)
                  ICODRO(NBOPT) = IDRO(IRO)
                  ICODVC(NBOPT) = IDVC(IRO)
                  ICODVA(NBOPT) = IDVA(IRO)
                  ICODSY(NBOPT) = IDSY(IRO)
                  ICODVI(NBOPT) = IDVI(IRO)
                ENDDO
              ENDDO E2
            ENDDO E1
          ENDDO E33
        ENDDO E35
      ENDDO E36
      CALL DEBUG( 'DIPMOD => MXOPVT=',LOP)
!
!     STOCKAGE DES CODES DES OPERATEURS ROVIBRATIONNELS
!
      WRITE(20,1001)
      WRITE(20,1220) NBOPT
      DO IRO=1,NBOPT
        IM          = ICODRO(IRO)/100000
        IK          = (ICODRO(IRO)-100000*IM)/10000
        IG          = (ICODRO(IRO)-100000*IM-10000*IK)/1000
        JD          = (ICODRO(IRO)-100000*IM-10000*IK-1000*IG)/100
        ICC4VB      = (ICODRO(IRO)-100000*IM-10000*IK-1000*IG-100*JD)/10
        ICC4V       = ICODRO(IRO)-100000*IM-10000*IK-1000*IG-100*JD-10*ICC4VB
        ICODRO(IRO) = ICODRO(IRO)/10
        CALL VLNC(ICODVC(IRO),IVC(1),IVC(2),IVC(3),IVC(4),IVC(5))
        CALL VLNC(ICODVA(IRO),IVA(1),IVA(2),IVA(3),IVA(4),IVA(5))
        CALL VLNC(ICODSY(IRO),ICCTO6,ICATO6,IGA,IVC(6),IVA(6))
        CALL VLNC(ICODVI(IRO),LOP,ICATO6,IPCTO6,IPATO6,IPGAMA)
        ICODVI(IRO) = (ICODVI(IRO)/1000)*10000+1000*IGA+100*IPGAMA+10*ICC4V+ICODTO(IRO)
        WRITE(20,1313) IRO,IM,IK,JD,KC(IG),ACGU(1),SYM(ICC4VB),           &
                       (IVC(L),L=1,NBVQN),KC(ICCTO6),ACGU(IPCTO6),        &
                       (IVA(L),L=1,NBVQN),KC(ICATO6),ACGU(IPATO6),        &
                       KC(IGA),ACGU(IPGAMA),SYM(ICC4V),SYM(ICODTO(IRO)),  &
                       ICODRO(IRO),ICODVI(IRO)
      ENDDO
      CALL DEBUG( 'DIPMOD => MXSNB=',NFS_SUP)
      CALL DEBUG( 'DIPMOD => MXSNV=',NFFS_SUP)
      CALL DEBUG( 'DIPMOD => MXEMR=',IOP_SUP)
      CALL DEBUG( 'DIPMOD => MXOCV=',NROT_SUP)
      CALL DEBUG( 'DIPMOD => MXOPT=',NBOPT_SUP)
      CALL DEBUG( 'DIPMOD => MXOPR=',IROTA_SUP)
      CALL DEBUG( 'DIPMOD => MXNIV=',NIV_SUP)
      GOTO 9000
!
9996  PRINT 8003
      GOTO  9999
9997  PRINT 8002
      GOTO  9999
9998  PRINT 8001
9999  PRINT 8000
!
9000  CLOSE(20)
      PRINT *
      END PROGRAM DIPMOD
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! CALCUL DES K, EXTRAITS PAR LA FONCTION XMKUB.
!     POUR UTILISATION DANS HAM_MODEL
!     JMAXK LIMITE A 9
!
!  REV 29 SEP 94
! REV    JAN 1995 JPC,CW (PARAMETER)
!
      SUBROUTINE CALKM(JMAXK)
      use mod_dppr
      use mod_par_tds
      use mod_main_dipmod
      IMPLICIT NONE
      integer          :: JMAXK

! functions
      integer          :: CTR,IUGTR,NSYM1,NTR

      real(kind=dppr)  :: VKC

      integer          :: I,IC1,IC2,IC3,ICO,IKNCOD,IKNT,ILI
      integer          :: ITEST1,ITEST2,ITEST3,IUG1,IUG2,IUG3
      integer          :: J1,J2,J3
      integer          :: N1,N2,N3,NCO,NCO0,NCO1,NLI
!
8000  FORMAT(' !!! CALKM  : STOP ON ERROR')
8100  FORMAT(' !!! CALKM  : CODING ERROR')
8103  FORMAT(' !!! CALKM  : JMAXK TOO LARGE ',I5,' > ',I5)
!
      IF( JMAXK .GT. 9 ) THEN
        PRINT 8103, JMAXK,9
        GOTO  9999
      ENDIF
      NCO1      = 0                                                                                ! INITIALISATION PAR DEFAUT
      NCO0      = 1
      IKINIC(1) = 0
      NLI       = 0
E1:   DO J1=0,JMAXK
E2:     DO J2=J1,JMAXK
E3:       DO J3=J2,JMAXK
            IF( NTR(J1,J2,J3) .EQ. 0 ) CYCLE E3
E4:         DO IUG1=1,2
E5:           DO IUG2=1,2
E6:             DO IUG3=1,2
                  IF( IUGTR(IUG1,IUG2,IUG3) .EQ. 0 ) CYCLE E6
                  NLI = NLI+1
                  IF( NLI .GT. MXKLI ) CALL RESIZE_MXKLI
                  IKJUG(NLI) = 100000*J1+10000*J2+1000*J3+100*IUG1+10*IUG2+IUG3
E7:               DO IC1=1,MXSYR
                    ITEST1 = NSYM1(J1,IUG1,IC1)
                    IF( ITEST1 .EQ. 0 ) CYCLE E7
E8:                 DO IC2=1,MXSYR
                      ITEST2 = NSYM1(J2,IUG2,IC2)
                      IF( ITEST2 .EQ. 0 ) CYCLE E8
E9:                   DO IC3=1,MXSYR
                        ITEST3 = NSYM1(J3,IUG3,IC3)
                        IF( ITEST3           .EQ. 0 ) CYCLE E9
                        IF( CTR(IC1,IC2,IC3) .EQ. 0 ) CYCLE E9
E10:                    DO N1=0,ITEST1-1
E11:                      DO N2=0,ITEST2-1
E12:                        DO N3=0,ITEST3-1
                              CALL KCUBU(J1,J2,J3,IUG1,IUG2,IUG3,N1,N2,N3,IC1,IC2,IC3,VKC,I)
                              IF( ABS(VKC) .LT. 1.D-10 ) CYCLE E12
                              IKNCOD = 100000*IC1+10000*IC2+1000*IC3+100*N1+10*N2+N3
                              IF( NCO0 .EQ. 1 ) THEN
!
! POUR INITIALISATION
!
                                IF( NLI .EQ. 1 ) THEN
                                  IKINIC(2)    = IKNCOD
                                  NCO0         = 2
                                  VK(NLI,NCO0) = VKC
                                  CYCLE E12
                                ENDIF
                              ELSE
!
! POSITIONNEMENT DANS LA LISTE DE CODES, D APRES LA VALEUR COURANTE
!
E13:                            DO NCO=1,NCO0
                                  IKNT = IKNCOD-IKINIC(NCO)
                                  IF    ( IKNT .EQ. 0 ) THEN
                                    GOTO 15
                                  ELSEIF( IKNT .GT. 0 ) THEN
                                    GOTO 16
                                  ENDIF
                                  IF( IKNCOD .LE. IKINIC(NCO-1) ) THEN
                                    PRINT 8100
                                    GOTO  9999
                                  ENDIF
                                  DO ICO=NCO0,NCO,-1
                                    IKINIC(ICO+1) = IKINIC(ICO)
                                    DO ILI=1,NLI
                                      VK(ILI,ICO+1) = VK(ILI,ICO)
                                    ENDDO
                                  ENDDO
                                  IKINIC(NCO) = IKNCOD
                                  VK(NLI,NCO) = VKC
                                  DO ILI=1,NLI-1
                                    VK(ILI,NCO) = 0.D0
                                  ENDDO
                                  NCO1 = NCO0+1
                                  GOTO 20
!
15                                VK(NLI,NCO) = VKC
                                  NCO1        = NCO0
                                  GOTO 20
!
16                                IF( NCO .LT. NCO0 ) THEN
                                    CYCLE E13
                                  ELSE
                                    VK(NLI,NCO0+1) = VKC
                                    IKINIC(NCO0+1) = IKNCOD
                                    NCO1           = NCO0+1
                                    GOTO 20
                                  ENDIF
                                ENDDO E13
                              ENDIF
!
20                            NCO0 = NCO1
                              IF( NCO0 .GE. MXKCO-1 ) CALL RESIZE_MXKCO
                            ENDDO E12
                          ENDDO E11
                        ENDDO E10
                      ENDDO E9
                    ENDDO E8
                  ENDDO E7
                ENDDO E6
              ENDDO E5
            ENDDO E4
          ENDDO E3
        ENDDO E2
      ENDDO E1
      NBKLI = NLI
      NBKCO = NCO0
      CALL DEBUG( 'CALKM  => JMAXK=',JMAXK)
      CALL DEBUG( 'CALKM  => MXKLI=',NBKLI)
      CALL DEBUG( 'CALKM  => MXKCO=',NBKCO)
      RETURN
!
9999  PRINT 8000
      PRINT *
      STOP
!
      END SUBROUTINE CALKM
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! SMIL G.PIERRE MAI 82
! MOD. T.GABARD DEC 92
! REV. 6/10/94
! REV    JAN 1995 JPC,CW (PARAMETER)
! MODIFIE 03/98 V. BOUDON ---> XY6/Oh.
!
! *** COUPLAGE DES OPERATEURS CREATIONS ET ANNHILATIONS
!
! ***   < IB !! IC * IA !! IK > = (-1)**(ICB+ICK+IC+IA) * SQRT(IGAMA)
!
!                        ( ICC , ICA ,IGAMA)
! ***   SOMME SUR IBK DE (                 ) <IB!!IC!!IBK><IBK!!IA!!IK>
!                        ( ICB , ICK , ICBK)
!
! ***   AVEC IX(I)       = 10000*VI+1000*LI+100*NI+10*CI+PI            POUR I=1,NBVQN
! ***   ET   IX(NBVQN+1) = 10000*C2+1000*C23+100*C234+10*C2345+C23456
! ***   ET   IX(NBVQN+2) = 10000*P2+1000*P23+100*P234+10*P2345+P23456
! ***   AVEC C23=C2*C3, C234=C23*C4, C2345=C234*C5 ET C23456=C2345*C6
! ***   IX PEUT ETRE  IB,IA,IC OU IK
!
      FUNCTION ELMR(IB,IC,IA,IK,IGAMA)
      use mod_dppr
      use mod_par_tds
      use mod_com_sigvi
      use mod_com_sy
      IMPLICIT NONE
      real(kind=dppr)  :: ELMR
      integer ,dimension(NBVQN+2)  :: IB,IC,IA,IK
      integer          :: IGAMA

! functions
      real(kind=dppr)  :: E1TO6,SXC
      integer          :: IPSI

      real(kind=dppr)  :: EM1,EM2
      real(kind=dppr)  :: P
      real(kind=dppr)  :: SIX

      integer ,dimension(MDMIGA)         :: ICCA2
      integer ,dimension(NBVQN)          :: IPB
      integer ,dimension(NBVQN,4)        :: IBCAK
      integer          :: I,ICA,ICB,ICC,ICC1,ICK,ICPK,II,IL,IPC,IV
      integer          :: J
      integer          :: N,NB,NCCA2
!
      ELMR = 0.D0
!
! *** TEST SUR LES FONCTIONS ET LES OPERATEURS.
!
      DO I=1,NBVQN
        IBCAK(I,1) = IB(I)
        IBCAK(I,2) = IC(I)
        IBCAK(I,3) = IA(I)
        IBCAK(I,4) = IK(I)
      ENDDO
!
! TEST DE LA COHERENCE DE L'ENSEMBLE DES NOMBRES QUANTIQUES
!
      DO J=1,4
        DO I=1,NBVQN
          CALL VLNC(IBCAK(I,J),IV,IL,N,ICC,IPC)
          II = I
          IF( I .GE. 4 ) II = 3
          ICC1 = ICC
          IF( I         .GT. 4 .AND.         &
              MOD(IV,2) .EQ. 1       ) THEN
            CALL MULTD(ICC,2,NCCA2,ICCA2)
            ICC1 = ICCA2(1)
          ENDIF
          IF( IPSI(II,IV,IL,N,ICC1) .EQ. 0 ) RETURN
        ENDDO
      ENDDO
!
! *** TEST SUR LES VI ET LES OMEGAI
!
      DO I=1,NBVQN
        IPK(I,1) = IB(I)/10000-IC(I)/10000
        IPB(I)   = IK(I)/10000-IA(I)/10000
        IF( IPB(I)   .LT. 0      ) RETURN
        IF( IPK(I,1) .NE. IPB(I) ) RETURN
      ENDDO
      ICB = IB(NBVQN+1)-10*(IB(NBVQN+1)/10)
      ICK = IK(NBVQN+1)-10*(IK(NBVQN+1)/10)
      ICC = IC(NBVQN+1)-10*(IC(NBVQN+1)/10)
      ICA = IA(NBVQN+1)-10*(IA(NBVQN+1)/10)
      P   = PC(ICC)*PC(ICA)*PC(ICB)*PC(ICK)*SQRT(DC(IGAMA))
!
! *** SOMMATION SUR LES ETATS INTERMEDIAIRES.
!
      CALL SIGVI(IPB,'IPK',0,NB)
      DO I=1,NB
        EM1  = E1TO6(1,IB,IC,IPK(1,I))
        EM2  = E1TO6(-1,IPK(1,I),IA,IK)
        ICPK = IPK(NBVQN+1,I)-10*(IPK(NBVQN+1,I)/10)
        SIX  = SXC(ICC,ICA,IGAMA,ICK,ICB,ICPK)
        ELMR = ELMR+P*SIX*EM1*EM2
      ENDDO
!
      RETURN
      END FUNCTION ELMR
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! DIPMOD version
! *** CHERCHE POUR UN ENSEMBLE DONNE DE VI : (IV(I),I=1,6)
! *** LES NB INDICES DE LA SIGNATURE VIBRATIONNELLE
! *** IKNC(I,K), I=1,8 ET K=ISN+1,ISN+NB
!
!  REV 29 SEP 94
!  MODIFIE MARS 98 ---> XY6/Oh (V. BOUDON).
!
! ATTENTION : SIGVI peut redimensionner les tableaux dependants de MXC
!
      SUBROUTINE SIGVI(IV,MXC,ISN,NB)                                                              ! SMIL G.PIERRE JUIN 82
      use mod_par_tds
      use mod_main_dipmod
      IMPLICIT NONE
      integer            ,dimension(NBVQN)  :: IV
      character(len = *)                    :: MXC
      integer                               :: ISN,NB

! functions
      integer :: MULGU

      integer ,parameter  :: MDMIB2 = 7
      integer ,parameter  :: MDMIB3 = 22
      integer ,parameter  :: MDMIB4 = 22
      integer ,parameter  :: MDMIB5 = 22
      integer ,parameter  :: MDMIB6 = 22

      integer ,pointer ,dimension(:,:)     :: IKNC
      integer          ,dimension(MDMIB2)  :: IB2
      integer          ,dimension(MDMIB3)  :: IB3
      integer          ,dimension(MDMIB4)  :: IB4
      integer          ,dimension(MDMIB5)  :: IB5
      integer          ,dimension(MDMIB6)  :: IB6
      integer          ,dimension(MDMIGA)  :: IC23
      integer          ,dimension(MDMIGA)  :: IC234
      integer          ,dimension(MDMIGA)  :: IC2345
      integer          ,dimension(MDMIGA)  :: ICTO6
      integer                              :: I2,I23,I234,I2345,I23456,I3,I4,I5,I6
      integer                              :: IC2,IC3,IC4,IC5,IC6,IP2,IP234,IP3,IP4,IP5,IP6,IPTO6
      integer                              :: K
      integer                              :: MDMKNC
      integer                              :: N23,N234,N2345,N23456
      integer                              :: NB2,NB3,NB4,NB5,NB6
!
8000  FORMAT(' !!! SIGVI  : STOP ON ERROR'   ,/,   &
             '              ',A,' UNEXPECTED'   )
!
      SELECT CASE( MXC )
        CASE( 'IFFI' )
          MDMKNC = MXSNV
          IKNC => IFFI
        CASE( 'IFFS' )
          MDMKNC = MXSNV
          IKNC => IFFS
        CASE( 'IKVA' )
          MDMKNC = MXSNB
          IKNC => IKVA
        CASE( 'IKVC' )
          MDMKNC = MXSNB
          IKNC => IKVC
        CASE( 'IPK' )
          MDMKNC = MXSNB
          IKNC => IPK
        CASE DEFAULT
          PRINT 8000, MXC
          STOP
!
      END SELECT
!
      K = ISN
      CALL TCUBE(2,IV(2),NB2,IB2,IP2,MDMIB2)
      CALL TCUBE(3,IV(3),NB3,IB3,IP3,MDMIB3)
      CALL TCUBE(4,IV(4),NB4,IB4,IP4,MDMIB4)
      CALL TCUBE(5,IV(5),NB5,IB5,IP5,MDMIB5)
      CALL TCUBE(6,IV(6),NB6,IB6,IP6,MDMIB6)
      DO I2=1,NB2
        IC2 = IB2(I2)-10*(IB2(I2)/10)
        DO I3=1,NB3
          IC3 = IB3(I3)-10*(IB3(I3)/10)
          CALL MULTD(IC2,IC3,N23,IC23)
          DO I23=1,N23
            DO I4=1,NB4
              IC4 = IB4(I4)-10*(IB4(I4)/10)
              CALL MULTD(IC23(I23),IC4,N234,IC234)
              IP234 = MULGU(IP3,IP4)
              DO I234=1,N234
                DO I5=1,NB5
                  IC5 = IB5(I5)-10*(IB5(I5)/10)
                  CALL MULTD(IC234(I234),IC5,N2345,IC2345)
                  DO I2345=1,N2345
                    DO I6=1,NB6
                      IC6 = IB6(I6)-10*(IB6(I6)/10)
                      CALL MULTD(IC2345(I2345),IC6,N23456,ICTO6)
                      IPTO6 = MULGU(IP234,IP6)
E2:                   DO I23456=1,N23456
                        K = K+1
                        IF( K .GT. MDMKNC ) THEN
                          SELECT CASE( MXC )
                            CASE( 'IFFI' )
                              CALL RESIZE_MXSNV
                              MDMKNC = MXSNV
                              IKNC => IFFI
                            CASE( 'IFFS' )
                              CALL RESIZE_MXSNV
                              MDMKNC = MXSNV
                              IKNC => IFFS
                            CASE( 'IKVA' )
                              CALL RESIZE_MXSNB
                              MDMKNC = MXSNB
                              IKNC => IKVA
                            CASE( 'IKVC' )
                              CALL RESIZE_MXSNB
                              MDMKNC = MXSNB
                              IKNC => IKVC
                            CASE( 'IPK' )
                              CALL RESIZE_MXSNB
                              MDMKNC = MXSNB
                              IKNC => IPK
                          END SELECT
                        ENDIF
                        IKNC(1,K) = 10000*IV(1)+11
                        IKNC(2,K) = IB2(I2)*10+IP2
                        IKNC(3,K) = IB3(I3)*10+IP3
                        IKNC(4,K) = IB4(I4)*10+IP4
                        IKNC(5,K) = IB5(I5)*10+IP5
                        IKNC(6,K) = IB6(I6)*10+IP6
                        IKNC(7,K) = 10000*IC2+1000*IC23(I23)+100*IC234(I234)+10*IC2345(I2345)+ICTO6(I23456)
                        IKNC(8,K) = 10000+1000*IP3+100*IP234+10*IP234+IPTO6
                      ENDDO E2
                    ENDDO
                  ENDDO
                ENDDO
              ENDDO
            ENDDO
          ENDDO
        ENDDO
      ENDDO
      NB = K-ISN
!
      RETURN
      END SUBROUTINE SIGVI
