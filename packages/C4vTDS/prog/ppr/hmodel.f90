      PROGRAM HMODEL
!
!  6.7.88 FORTRAN 77 DU SUN4
!  REV 25 JAN 1990
!  REV    DEC 1992 T.GABARD
!  REV 29 SEP 1994
!  REV    JAN 1995 JPC,CW (PARAMETER)
!
!  MODIFICATION MARS 1998 V. BOUDON ---> XY6 / Oh.
!
!  MODIFICATION JANV. 1999 M. ROTGER ---> XY5Z / C4v.
!
!      CODAGE DES OPERATEURS ROVIBRATIONNELS DE L'HAMILTONIEN EFFECTIF
!     POUR UNE RESTRICTION DONNEE D'UNE POLYADE VIBRATIONNELLE TYPE CH4
!
!     LECTURE DES G  MXDROT
!
!     CALCUL DES 6-C
!
!     CALCUL DES K JMKCUB = FIXE DANS LE PROGRAMME
!
! APPEL : hmodel Pn Nm  Dk  v1  v2  v3  v4  v5  v6   ...   w1  w2  w3  w4  w5  w6            |
!                   Nm' Dk' v1' v2' v3' v4' v5' v6'  ...   w1' w2' w3' w4' w5' w6'           | (n+1 lines)
!                   ...........................                                              |
!
!  *****  LIMITATIONS DU PROGRAMME GEREES PAR PARAMETER.
!
!     MXOPH  = NB MAXIMUM D'OPERATEURS ROVIBRATIONNELS
!               (ICODVI,ICODRO,ICODVC,ICODVA,ICODSY)
!     MXOCV  = NB MAXIMUM D'OPERATEURS ROVIBRATIONNELS PAR CASE
!              VIBRATIONNELLE
!               (IRDRE,IDVI,IDRO,IDVC,IDVA,IDSY)
!     MXEMR  = NB MAXIMUM D'E.M.R.V. NON NULS
!               (EMOP,LI,IC)
!     MXSNV  = NB MAXIMUM TOTAL DE SOUS-NIVEAUX VIBRATIONNELS
!               (IFF)
!     MXSNB  = NB MAXIMUM DE SOUS-NIVEAUX VIBRATIONNELS POUR UN
!              ENSEMBLE DONNE DES VI
!               (IKVC,IKVA)
!     MXOPR  = NB MAXIMUM D'OPERATEURS ROVIBRATIONNELS PAR OPERATEUR
!              VIBRATIONNEL
!               (IROCO)
!     MXNIV  = NB MAXIMUM DE NIVEAUX VIBRATIONNELS DANS UNE POLYADE DONNEE
!               (NVIB)
!     MXPOL  = NB MAXIMUM DE POLYADES
!               (NIVI,NVIB)
!
      use mod_dppr
      use mod_par_tds
      use mod_com_fdate
      use mod_com_lg
      use mod_com_phase
      use mod_com_sy
      use mod_main_hmodel
      IMPLICIT NONE

! functions
      real(kind=dppr)  :: AKPRIM,ELMR
      integer          :: NSYM2,MULGU

      real(kind=dppr)  :: AKP
      real(kind=dppr)  :: EM,EM1,EM2,EN
      real(kind=dppr)  :: SP

      integer ,dimension(NBVQN+2)  :: IBELMR,ICELMR,IAELMR,IKELMR
      integer ,dimension(NBVQN)    :: ICA,ICC,ICF,ICP,ILA,IL,ILC,IPA,IPC
      integer ,dimension(NBVQN)    :: INA,INC,INUM,IV,IVA,IVC
      integer ,dimension(MDMIGA)   :: IGAMA
      integer ,dimension(MXSYM)    :: IROTC4
      integer ,dimension(MXSYR)    :: IROT,N
      integer ,dimension(MXPOL)    :: IORH,NIVI
      integer          :: NF_SUP,NFF_SUP,IOP_SUP,NROT_SUP,NBOPH_SUP
      integer          :: IROTA_SUP,NIV_SUP
      integer          :: I,IB,IC2,IC23,IC234,ICA2,ICA23
      integer          :: ICA234,ICATO5,ICATO6,ICC2,ICC23,ID,IDEGV,IDK
      integer          :: IC2345,ICC234,ICCTO5,ICCTO6,ICTO6,IPC2,IPC23,IPC234,IPCTO5,IPCTO6
      integer          :: IPA2,IPA23,IPA234,IPATO5,IPATO6,IPGAMA,IP2,IP23,IP234,IP2345,IPTO6
      integer          :: IDROT,IF,IF1,IF2,IG,IGA,IJK,IK,IM,IMIA
      integer          :: IOA,IOA1,IOC,IOC1,IOP,IOPA,IOPC,IPOL,IRO
      integer          :: IRO1,IRO2,IRO3,IRO4,IRO5,IROTA,IU,IVI
      integer          :: IBIS,ICBC4,ICC4V,ICC4VB,ICC4VK,ICKC4,IFC4,IJKB,IPBC4,IPKC4
      integer          :: IX1,IX2,IXB1,IXK1,IX4,IXB2,IXK2,IX3,IXB3,IXB4,IXB5,IXB6,IXB7,IXB8,IXK3,IXK4,JC4V,JC4VB,JC4VK
      integer          :: J,JD,JJJ,JMKCUB
      integer          :: KNVIB,KNIVP,KPOL,KOP
      integer          :: L,LOP
      integer          :: NBOPH,NBVA,NBVC,NF,NFF,NGAMA
      integer          :: NA1,NA2,NE,NF1,NF2,NFFC4,NSC4V
      integer          :: NPOL,NROT,NS

      character(len =   1)  :: AVI,AORH
      character(len =  11)  :: CHAINE
      character(len = 120)  :: IFICH
!
1000  FORMAT('   EFFECTIVE ROVIBRATIONAL HAMILTONIAN :'  , /,   &
             ' VIBRATIONAL REDUCED MATRIX ELEMENTS (RME)',//,   &
             A                                               )
1001  FORMAT(//)
1002  FORMAT(I6,'  |[[[[',                           &
             I2,'(',I1,',',I1,A,A,')*',              &
             I2,'(',I1,',',I1,A,A,')*',              &
             I2,'(',I1,',',I1,A,A,')]',A,A,'*',      &
             I2,'(',I1,',',I1,A,A,')]',A,A,'*',      &
             I2,'(',I1,',',I1,A,A,')]',A,A,'*',      &
             I2,'(',I1,',',I1,A,A,')]',A,A,', ',A,   &
             ' >'                                 )
1004  FORMAT(I4,' TH VIBRATIONAL OPERATOR'  ,//,  &
             A,1X,' {[[[[',                       &
             I2,'(',I1,',',A,A,')*',              &
             I2,'(',I1,',',A,A,')*',              &
             I2,'(',I1,',',A,A,')]',A,A,'*',      &
             I2,'(',I1,',',A,A,')]',A,A,'*',      &
             I2,'(',I1,',',A,A,')]',A,A,'*',      &
             I2,'(',I1,',',A,A,')]',A,A,          &
             '} '                          , /,   &
             2X,' {[[[[',                         &
             I2,'(',I1,',',A,A,')*',              &
             I2,'(',I1,',',A,A,')*',              &
             I2,'(',I1,',',A,A,')]',A,A,'*',      &
             I2,'(',I1,',',A,A,')]',A,A,'*',      &
             I2,'(',I1,',',A,A,')]',A,A,'*',      &
             I2,'(',I1,',',A,A,')]',A,A,          &
             '} ',A,A,', ',A                   )
1005  FORMAT(/,              &
             'HMODEL : ',A)
1025  FORMAT(I4,' ROVIBRATIONAL OPERATORS',/)
1026  FORMAT(/,            &
             1X,100('-'))
1027  FORMAT(/,                                          &
             ' HAMILTONIAN DEVELOPMENT ORDER :  ',10I2)
1028  FORMAT(A)
1029  FORMAT(/,                     &
             I5,' NON ZERO RME',/)
1030  FORMAT(2X,A1,'<',I5,'|',3X,'|',I5,'>',3X,F19.13)
1031  FORMAT(/,                               &
             I4,' ROTATIONAL OPERATOR(S)',/)
1032  FORMAT(I1)
1033  FORMAT(/,                         &
             ' POLYAD NUMBER : ',I2,/)
1034  FORMAT(5X,'(v1;v2;v3;v4;v5;v6) = (',5(I2,';'),I2,')')                                        ! ATTENTION respecter cette regle dans tous les packages pour VAMDC
1035  FORMAT(I2)
1100  FORMAT(/,                                  &
             I4,' VIBRATIONAL SUBLEVEL(S) :',/)
1313  FORMAT(I4,I3,'(',I1,',',I1,A,A,1X,A,')',         &
             1X,6I1,A,A,1X,6I1,A,A,1X,A,A,1X,A,I8,I7)                                              ! 6 = NBVQN
2001  FORMAT(' >> ',A)
8000  FORMAT(' !!! HMODEL : STOP ON ERROR')
8001  FORMAT(' !!! ERROR OPENING CONTROL FILE')
8002  FORMAT(' !!! INCOMPLETE CONTROL FILE')
!
! ARRAY INITIAL ALLOCATION
!
      CALL ALLOC_ALL
!
      NF_SUP    = -1
      NFF_SUP   = -1
      IOP_SUP   = -1
      NROT_SUP  = -1
      NBOPH_SUP = -1
      IROTA_SUP = -1
      NIV_SUP   = -1
      OPEN(10,FILE=CTRLF,ERR=9998,STATUS='OLD')
      READ(10,1028,END=9997) FDATE
      PRINT 1005,            FDATE
!
! NUMERO DE LA POLYADE COURANTE
!
      READ(10,1028,END=9997) CHAINE
      READ (CHAINE(2:2),1032) IVI
      WRITE(AVI,1032)         IVI
!
! NOMBRE TOTAL DE POLYADES
!
      NPOL = IVI+1
!
! NOM DU FICHIER DE SORTIE
!
      IFICH = 'MH_P'//AVI//'_D'
!
! LECTURE DES NIVEAUX VIBRATIONNELS
!
      JMKCUB = 0
      DO KPOL=1,NPOL
        READ(10,1028,END=9997) CHAINE
        READ(CHAINE(2:3),1035) NIVI(KPOL)
        IF( NIVI(KPOL) .GT. NIV_SUP ) NIV_SUP = NIVI(KPOL)
        DO WHILE( NIVI(KPOL) .GT. MXNIV )
          CALL RESIZE_MXNIV
        ENDDO
        READ(10,1028,END=9997) CHAINE
        READ (CHAINE(2:2),1032) IORH(KPOL)
        WRITE(AORH,1032)        IORH(KPOL)
        IFICH = TRIM(IFICH)//AORH
        DO KNIVP=1,NIVI(KPOL)
          DO KNVIB=1,NBVQN
            READ(10,1032,END=9997) NVIB(KPOL,KNIVP,KNVIB)
            IF( KNVIB .GT. 2 ) JMKCUB = MAX(JMKCUB,NVIB(KPOL,KNIVP,KNVIB))
          ENDDO
        ENDDO
      ENDDO
      CLOSE(10)
      PRINT 2001, TRIM(IFICH)
!
! INITIALISATIONS
!
      CALL LECTG(MXDROT)
      CALL FACTO
      CALL CAL6C
      CALL CALKM(JMKCUB)
      OPEN(20,FILE=IFICH,STATUS='UNKNOWN')
      WRITE(20,1000) FDATE
      WRITE(20,1033) IVI
      DO I=1,NIVI(NPOL)
        WRITE(20,1034) (NVIB(NPOL,I,J),J=1,NBVQN)
      ENDDO
!
! CODAGE DES SOUS-NIVEAUX VIBRATIONNELS
!
      NFF   = 0
      NFFC4 = 0
      DO IF1=1,NIVI(NPOL)
        DO IF2=1,NBVQN
          IV(IF2) = NVIB(NPOL,IF1,IF2)
        ENDDO
        CALL SIGVI(IV,'IFF',NFF,NF)
        CALL VLNC(IFF(NBVQN+1,NFF+1),IX1,IX2,IX3,IX4,ICTO6)
        CALL VLNC(IFF(NBVQN+2,NFF+1),IX1,IX2,IX3,IX4,IPTO6)
        NSC4V = 0
E70:    DO JC4V=1,2
          IF( IC4VV(ICTO6,IPTO6,JC4V) .EQ. 0 ) CYCLE E70
          NSC4V = NSC4V+1
        ENDDO E70
        NFF   = NFF+NF
        NFFC4 = NFFC4+NF*NSC4V
        IF( NF  .GT. NF_SUP  ) NF_SUP  = NF
        IF( NFF .GT. NFF_SUP ) NFF_SUP = NFF
      ENDDO
      WRITE(20,1100) NFFC4
      IFC4 = 0
      DO IF=1,NFF
        DO I=1,NBVQN
          CALL VLNC(IFF(I,IF),IV(I),IL(I),INUM(I),ICF(I),ICP(I))
        ENDDO
        CALL VLNC(IFF(NBVQN+1,IF),IC2,IC23,IC234,IC2345,ICTO6)
        CALL VLNC(IFF(NBVQN+2,IF),IP2,IP23,IP234,IP2345,IPTO6)
!      CALL VLNC(IFF(9,IF),IX1,IX2,IX3,IX4,ICC4V)
        DO J=1,2
          ICC4V = IC4VV(ICTO6,IPTO6,J)
          IF( ICC4V .NE. 0 ) THEN
            IFC4 = IFC4+1
            WRITE(20,1002) IFC4,                                                 &
                           (IV(I),IL(I),INUM(I),KC(ICF(I)),ACGU(ICP(I)),I=1,3),  &
                           KC(IC23),ACGU(IP23),                                  &
                            IV(4),IL(4),INUM(4),KC(ICF(4)),ACGU(ICP(4)),         &
                           KC(IC234),ACGU(IP234),                                &
                            IV(5),IL(5),INUM(5),KC(ICF(5)),ACGU(ICP(5)),         &
                           KC(IC2345),ACGU(IP2345),                              &
                            IV(6),IL(6),INUM(6),KC(ICF(6)),ACGU(ICP(6)),         &
                           KC(ICTO6),ACGU(IPTO6),SYM(ICC4V)
          ENDIF
        ENDDO
      ENDDO
      WRITE(20,1027) (IORH(IPOL),IPOL=1,NPOL)
      LOP   = 0
      NBOPH = 0
!
! BOUCLE SUR LES POLYADES
!
E36:  DO KPOL=1,NPOL
!
! DETERMINATION DES CREATEURS
!
E35:    DO IOC=1,NIVI(KPOL)
          DO IOC1=1,NBVQN
            IVC(IOC1) = NVIB(KPOL,IOC,IOC1)
          ENDDO
          CALL SIGVI(IVC,'IKVC',0,NBVC)
!
! DETERMINATION DES ANNIHILATEURS
!
E33:      DO IOA=1,NIVI(KPOL)
            DO IOA1=1,NBVQN
              IVA(IOA1) = NVIB(KPOL,IOA,IOA1)
            ENDDO
            IF( IOA .LT. IOC ) CYCLE E33
            CALL SIGVI(IVA,'IKVA',0,NBVA)
            IDEGV = 0
            DO IOP=1,NBVQN
              IDEGV = IVA(IOP)+IVC(IOP)+IDEGV
            ENDDO
            IDROT = IORH(KPOL)+2-IDEGV
            IF( IDROT .LT. 0 ) CYCLE E33
!
! COUPLAGE CREATEURS-ANNIHILATEURS
!
E1:         DO IOPC=1,NBVC
              IMIA = 1
              IF( IOA .EQ. IOC ) IMIA = IOPC
              DO I=1,NBVQN
                CALL VLNC(IKVC(I,IOPC),IVC(I),ILC(I),INC(I),ICC(I),IPC(I))
              ENDDO
              CALL VLNC(IKVC(NBVQN+1,IOPC),ICC2,ICC23,ICC234,ICCTO5,ICCTO6)
              CALL VLNC(IKVC(NBVQN+2,IOPC),IPC2,IPC23,IPC234,IPCTO5,IPCTO6)
E2:           DO IOPA=IMIA,NBVA
                DO I=1,NBVQN
                  CALL VLNC(IKVA(I,IOPA),IVA(I),ILA(I),INA(I),ICA(I),IPA(I))
                ENDDO
                CALL VLNC(IKVA(NBVQN+1,IOPA),ICA2,ICA23,ICA234,ICATO5,ICATO6)
                CALL VLNC(IKVA(NBVQN+2,IOPA),IPA2,IPA23,IPA234,IPATO5,IPATO6)
                CALL MULTD(ICCTO6,ICATO6,NGAMA,IGAMA)
                IPGAMA = MULGU(IPCTO6,IPATO6)
                NROT = 0
E111:           DO I=1,NGAMA
E734:             DO JC4V=1,2
                    IF( IC4VV(IGAMA(I),IPGAMA,JC4V) .EQ. 0 ) CYCLE E734
                    ICC4V = IC4VV(IGAMA(I),IPGAMA,JC4V)
E733:               DO IU=1,2
                      DO IJK=1,MXSYR
                        IROT(IJK) = 0
                      ENDDO
                      DO IJKB=1,MXSYM
                        IROTC4(IJKB) = 0
                      ENDDO
!
! DETERMINATION DES OPERATEURS ROTATIONNELS POSSIBLES
!
                      IROTA = 0
                      IF( IDROT .LT. IU-1 ) CYCLE E733
                      DO ID=IU-1,IDROT,2
                        CALL NBJC(ID,1,IRO1,IRO2,IRO3,IRO4,IRO5)
                        IROT(1)   = IROT(1)+IRO1
                        IROT(2)   = IROT(2)+IRO2
                        IROT(3)   = IROT(3)+IRO3
                        IROT(4)   = IROT(4)+IRO4
                        IROT(5)   = IROT(5)+IRO5
                        IROTC4(1) = IROTC4(1)+IROT(1)+IROT(3)
                        IROTC4(2) = IROTC4(2)+IROT(4)
                        IROTC4(3) = IROTC4(3)+IROT(2)+IROT(3)
                        IROTC4(4) = IROTC4(4)+IROT(5)
                        IROTC4(5) = IROTC4(5)+IROT(4)+IROT(5)
E365:                   DO IDK=ID,0,-2
                          IF( IDEGV .EQ. 0 .AND.             &
                              ID    .EQ. 0       ) CYCLE E365
                          CALL NBJC(IDK,1,NA1,NA2,NE,NF1,NF2)
                          N(1) = NA1
                          N(2) = NA2
                          N(3) = NE
                          N(4) = NF1
                          N(5) = NF2
E366:                     DO IBIS=1,MXSYR
                            IF( N(IBIS)             .EQ. 0 ) CYCLE E366
                            IF( NSYM2(IBIS,1,ICC4V) .EQ. 0 ) CYCLE E366
                            DO NS=1,N(IBIS)
                              IROTA = IROTA+1
                              IF( IROTA .GT. IROTA_SUP ) IROTA_SUP = IROTA
                              IF( IROTA .GT. MXOPR     ) CALL RESIZE_MXOPR
                              IROCO(IROTA) = 10000*ID+1000*IDK+100*IBIS+10*(NS-1)+ICC4V
                            ENDDO
                          ENDDO E366
                        ENDDO E365
                      ENDDO
!
! CALCUL DU FACTEUR DE NORMALISATION
!      IU = 1, EPSILON = +
!      IU = 2, EPSILON = -
!
                      IF( IROTC4(ICC4V) .EQ. 0 ) CYCLE E733
                      SP        = ( (-1)**(IU+1) )*PC(ICATO6)*PC(ICCTO6)*PC(IGAMA(I))
                      IBELMR(:) = IKVC(:,IOPC)
                      ICELMR(:) = IKVC(:,IOPC)
                      IAELMR(:) = IKVA(:,IOPA)
                      IKELMR(:) = IKVA(:,IOPA)
                      EN        = ELMR(IBELMR,ICELMR,IAELMR,IKELMR,IGAMA(I))
                      IBELMR(:) = IKVC(:,IOPC)
                      ICELMR(:) = IKVA(:,IOPA)
                      IKELMR(:) = IKVC(:,IOPC)
                      IKELMR(:) = IKVA(:,IOPA)
                      EN        = ELMR(IBELMR,ICELMR,IAELMR,IKELMR,IGAMA(I))*SP+EN
                      IF( ABS(EN) .LT. 1.D-05 ) CYCLE E733
                      LOP = LOP+1
                      WRITE(20,1026)
                      WRITE(20,1004) LOP,AUG(IU),                                    &
                                     (IVC(J),ILC(J),KC(ICC(J)),ACGU(IPC(J)),J=1,3),  &
                                     KC(ICC23),ACGU(IPC23),                          &
                                      IVC(4),ILC(4),KC(ICC(4)),ACGU(IPC(4)),         &
                                     KC(ICC234),ACGU(IPC234),                        &
                                      IVC(5),ILC(5),KC(ICC(5)),ACGU(IPC(5)),         &
                                     KC(ICCTO5),ACGU(IPCTO5),                        &
                                      IVC(6),ILC(6),KC(ICC(6)),ACGU(IPC(6)),         &
                                     KC(ICCTO6),ACGU(IPCTO6),                        &
                                     (IVA(J),ILA(J),KC(ICA(J)),ACGU(IPA(J)),J=1,3),  &
                                     KC(ICA23),ACGU(IPA23),                          &
                                      IVA(4),ILA(4),KC(ICA(4)),ACGU(IPA(4)),         &
                                     KC(ICA234),ACGU(IPA234),                        &
                                      IVA(5),ILA(5),KC(ICA(5)),ACGU(IPA(5)),         &
                                     KC(ICATO5),ACGU(IPATO5),                        &
                                      IVA(6),ILA(6),KC(ICA(6)),ACGU(IPA(6)),         &
                                     KC(ICATO6),ACGU(IPATO6),                        &
                                     KC(IGAMA(I)),ACGU(IPGAMA),SYM(ICC4V)
!
! CALCUL ET CODAGE DES E.M.R.V.
!
                      IOP = 0
                      DO IB=1,NFF
                        DO IK=IB,NFF
                          CALL VLNC(IFF(NBVQN+1,IB),IXB1,IXB2,IXB3,ICBC4,IXB4)
                          CALL VLNC(IFF(NBVQN+1,IK),IXK1,IXK2,IXK3,ICKC4,IXK4)
                          CALL VLNC(IFF(NBVQN+2,IB),IXB5,IXB6,IXB7,IXB8,IPBC4)
                          CALL VLNC(IFF(NBVQN+2,IK),IXB5,IXB6,IXB7,IXB8,IPKC4)
E6:                       DO JC4VB=1,2
                            ICC4VB = IC4VV(ICBC4,IPBC4,JC4VB)
                            IF( ICC4VB .EQ. 0 ) CYCLE E6
E7:                         DO JC4VK=1,2
                              ICC4VK = IC4VV(ICKC4,IPKC4,JC4VK)
                              IF( ICC4VK .EQ. 0 ) CYCLE E7
                              AKP = AKPRIM(IGAMA(I),ICKC4,ICBC4,IPGAMA,IPKC4,IPBC4,ICC4V,ICC4VK,ICC4VB)
                              IF( AKP .EQ. 0.D0 ) CYCLE E7
                              IBELMR(:) = IFF(:,IB)
                              ICELMR(:) = IKVC(:,IOPC)
                              IAELMR(:) = IKVA(:,IOPA)
                              IKELMR(:) = IFF(:,IK)
                              EM1       = ELMR(IBELMR,ICELMR,IAELMR,IKELMR,IGAMA(I))
                              IBELMR(:) = IFF(:,IB)
                              ICELMR(:) = IKVA(:,IOPA)
                              IAELMR(:) = IKVC(:,IOPC)
                              IKELMR(:) = IFF(:,IK)
                              EM2       = ELMR(IBELMR,ICELMR,IAELMR,IKELMR,IGAMA(I))*SP
                              EM        = EM1+EM2
                              EM        = EM*AKP
                              IF( ABS(EM) .LT. 1.D-10 ) CYCLE E7
                              EM = EM/EN
                              IF( IU .EQ. 2 ) EM = -EM
! changement de convention pour les operateurs V dont les elements
! matriciels sont imaginaires (pour retablir, dans la majorite des cas,
! les anciens signes des parametres)     4 / 5 / 93
                              IOP = IOP+1
                              IF( IOP .GT. IOP_SUP ) IOP_SUP = IOP
                              IF( IOP .GT. MXEMR   ) CALL RESIZE_MXEMR
                              LI(IOP)   = JC4VK
                              IC(IOP)   = JC4VB
                              EMOP(IOP) = EM
                            ENDDO E7
                          ENDDO E6
                        ENDDO
                      ENDDO
                      WRITE(20,1029) IOP
                      DO KOP=1,IOP
                        WRITE(20,1030) AIM(IU),IC(KOP),LI(KOP),EMOP(KOP)
                      ENDDO
                      WRITE(20,1031) IROTA
!
! CODAGE DES OPERATEURS ROVIBRATIONNELS
!
                      DO IRO=1,IROTA
                        NROT = NROT+1
                        IF( NROT .GT. NROT_SUP ) NROT_SUP = NROT
                        IF( NROT .GT. MXOCV    ) CALL RESIZE_MXOCV
                        IDRO(NROT) = IROCO(IRO)
                        IDVC(NROT) = IVC(1)*10000+IVC(2)*1000+IVC(3)*100+IVC(4)*10+IVC(5)
                        IDVA(NROT) = IVA(1)*10000+IVA(2)*1000+IVA(3)*100+IVA(4)*10+IVA(5)
                        IDSY(NROT) = ICCTO6*10000+ICATO6*1000+IGAMA(I)*100+IVC(6)*10+IVA(6)
                        IDVI(NROT) = LOP*10000+ICCTO6*1000+IPCTO6*100+IPATO6*10+IPGAMA
                      ENDDO
                    ENDDO E733
                  ENDDO E734
                ENDDO E111
!
! MISE EN ORDRE A L'INTERIEUR D'UNE CASE VIBRATIONNELLE DONNEE
!
                IF( NROT .EQ. 0 ) CYCLE E2
                CALL IRDER(NROT,IDRO,IRDRE,MXOCV)
                DO JJJ=1,NROT
                  NBOPH = NBOPH+1
                  IF( NBOPH .GT. NBOPH_SUP ) NBOPH_SUP = NBOPH
                  IF( NBOPH .GT. MXOPH     ) CALL RESIZE_MXOPH
                  IRO           = IRDRE(JJJ)
                  ICODRO(NBOPH) = IDRO(IRO)
                  ICODVC(NBOPH) = IDVC(IRO)
                  ICODVA(NBOPH) = IDVA(IRO)
                  ICODSY(NBOPH) = IDSY(IRO)
                  ICODVI(NBOPH) = IDVI(IRO)
                ENDDO
              ENDDO E2
            ENDDO E1
          ENDDO E33
        ENDDO E35
      ENDDO E36
      CALL DEBUG( 'HMODEL => MXOPVH=',LOP)
!
! STOCKAGE DES CODES DES OPERATEURS ROVIBRATIONNELS
!
      WRITE(20,1001)
      WRITE(20,1025) NBOPH
      DO IRO=1,NBOPH
        IM    = ICODRO(IRO)/10000
        IK    = (ICODRO(IRO)-10000*IM)/1000
        IG    = (ICODRO(IRO)-10000*IM-1000*IK)/100
        JD    = (ICODRO(IRO)-10000*IM-1000*IK-100*IG)/10
        ICC4V = ICODRO(IRO)-10000*IM-1000*IK-100*IG-10*JD
        CALL VLNC(ICODVC(IRO),IVC(1),IVC(2),IVC(3),IVC(4),IVC(5))
        CALL VLNC(ICODVA(IRO),IVA(1),IVA(2),IVA(3),IVA(4),IVA(5))
        CALL VLNC(ICODSY(IRO),ICCTO6,ICATO6,IGA,IVC(6),IVA(6))
        CALL VLNC(ICODVI(IRO),LOP,ICCTO6,IPCTO6,IPATO6,IPGAMA)
        ICODVI(IRO) = (ICODVI(IRO)/1000)*100+10*IGA+IPGAMA
        WRITE(20,1313) IRO,IM,IK,JD,KC(IG),ACGU(IPGAMA),SYM(ICC4V),             &
                       (IVC(L),L=1,6),KC(ICCTO6),ACGU(IPCTO6),                  &
                       (IVA(L),L=1,6),KC(ICATO6),ACGU(IPATO6),                  &
                       KC(IGA),ACGU(IPGAMA),SYM(ICC4V),ICODRO(IRO),ICODVI(IRO)
      ENDDO
      CLOSE(20)
      CALL DEBUG( 'HMODEL => MXSNB=',NF_SUP)
      CALL DEBUG( 'HMODEL => MXSNV=',NFF_SUP)
      CALL DEBUG( 'HMODEL => MXEMR=',IOP_SUP)
      CALL DEBUG( 'HMODEL => MXOCV=',NROT_SUP)
      CALL DEBUG( 'HMODEL => MXOPH=',NBOPH_SUP)
      CALL DEBUG( 'HMODEL => MXOPR=',IROTA_SUP)
      CALL DEBUG( 'HMODEL => MXNIV=',NIV_SUP)
      GOTO 9000
!
9997  PRINT 8002
      CLOSE(10)
      GOTO  9999
9998  PRINT 8001
9999  PRINT 8000
!
9000  PRINT *
      END PROGRAM HMODEL
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! CALCUL DES K, EXTRAITS PAR LA FONCTION XMKUB.
!     POUR UTILISATION DANS HAM_MODEL
!     JMAXK LIMITE A 9
!
!  REV 29 SEP 94
! REV    JAN 1995 JPC,CW (PARAMETER)
!
      SUBROUTINE CALKM(JMAXK)
      use mod_dppr
      use mod_par_tds
      use mod_main_hmodel
      IMPLICIT NONE
      integer          :: JMAXK

! functions
      integer          :: CTR,IUGTR,NSYM1,NTR

      real(kind=dppr)  :: VKC

      integer          :: I,IC1,IC2,IC3,ICO,IKNCOD,IKNT,ILI
      integer          :: ITEST1,ITEST2,ITEST3,IUG1,IUG2,IUG3
      integer          :: J1,J2,J3
      integer          :: N1,N2,N3,NCO,NCO0,NCO1,NLI
!
8000  FORMAT(' !!! CALKM  : STOP ON ERROR')
8100  FORMAT(' !!! CALKM  : CODING ERROR')
8103  FORMAT(' !!! CALKM  : JMAXK TOO LARGE ',I5,' > ',I5)
!
      IF( JMAXK .GT. 9 ) THEN
        PRINT 8103, JMAXK,9
        GOTO  9999
      ENDIF
      NCO1      = 0                                                                                ! INITIALISATION PAR DEFAUT
      NCO0      = 1
      IKINIC(1) = 0
      NLI       = 0
E1:   DO J1=0,JMAXK
E2:     DO J2=J1,JMAXK
E3:       DO J3=J2,JMAXK
            IF( NTR(J1,J2,J3) .EQ. 0 ) CYCLE E3
E4:         DO IUG1=1,2
E5:           DO IUG2=1,2
E6:             DO IUG3=1,2
                  IF( IUGTR(IUG1,IUG2,IUG3) .EQ. 0 ) CYCLE E6
                  NLI = NLI+1
                  IF( NLI .GT. MXKLI ) CALL RESIZE_MXKLI
                  IKJUG(NLI) = 100000*J1+10000*J2+1000*J3+100*IUG1+10*IUG2+IUG3
E7:               DO IC1=1,MXSYR
                    ITEST1 = NSYM1(J1,IUG1,IC1)
                    IF( ITEST1 .EQ. 0 ) CYCLE E7
E8:                 DO IC2=1,MXSYR
                      ITEST2 = NSYM1(J2,IUG2,IC2)
                      IF( ITEST2 .EQ. 0 ) CYCLE E8
E9:                   DO IC3=1,MXSYR
                        ITEST3 = NSYM1(J3,IUG3,IC3)
                        IF( ITEST3           .EQ. 0 ) CYCLE E9
                        IF( CTR(IC1,IC2,IC3) .EQ. 0 ) CYCLE E9
E10:                    DO N1=0,ITEST1-1
E11:                      DO N2=0,ITEST2-1
E12:                        DO N3=0,ITEST3-1
                              CALL KCUBU(J1,J2,J3,IUG1,IUG2,IUG3,N1,N2,N3,IC1,IC2,IC3,VKC,I)
                              IF( ABS(VKC) .LT. 1.D-10 ) CYCLE E12
                              IKNCOD = 100000*IC1+10000*IC2+1000*IC3+100*N1+10*N2+N3
                              IF( NCO0 .EQ. 1 ) THEN
!
! POUR INITIALISATION
!
                                IF( NLI .EQ. 1 ) THEN
                                  IKINIC(2)    = IKNCOD
                                  NCO0         = 2
                                  VK(NLI,NCO0) = VKC
                                  CYCLE E12
                                ENDIF
                              ELSE
!
! POSITIONNEMENT DANS LA LISTE DE CODES, D APRES LA VALEUR COURANTE
!
E13:                            DO NCO=1,NCO0
                                  IKNT = IKNCOD-IKINIC(NCO)
                                  IF    ( IKNT .EQ. 0 ) THEN
                                    GOTO 15
                                  ELSEIF( IKNT .GT. 0 ) THEN
                                    GOTO 16
                                  ENDIF
                                  IF( IKNCOD .LE. IKINIC(NCO-1) ) THEN
                                    PRINT 8100
                                    GOTO  9999
                                  ENDIF
                                  DO ICO=NCO0,NCO,-1
                                    IKINIC(ICO+1) = IKINIC(ICO)
                                    DO ILI=1,NLI
                                      VK(ILI,ICO+1) = VK(ILI,ICO)
                                    ENDDO
                                  ENDDO
                                  IKINIC(NCO) = IKNCOD
                                  VK(NLI,NCO) = VKC
                                  DO ILI=1,NLI-1
                                    VK(ILI,NCO) = 0.D0
                                  ENDDO
                                  NCO1 = NCO0+1
                                  GOTO 20
!
15                                VK(NLI,NCO) = VKC
                                  NCO1        = NCO0
                                  GOTO 20
!
16                                IF( NCO .LT. NCO0 ) THEN
                                    CYCLE E13
                                  ELSE
                                    VK(NLI,NCO0+1) = VKC
                                    IKINIC(NCO0+1) = IKNCOD
                                    NCO1           = NCO0+1
                                    GOTO 20
                                  ENDIF
                                ENDDO E13
                              ENDIF
!
20                            NCO0 = NCO1
                              IF( NCO0 .GE. MXKCO-1 ) CALL RESIZE_MXKCO
                            ENDDO E12
                          ENDDO E11
                        ENDDO E10
                      ENDDO E9
                    ENDDO E8
                  ENDDO E7
                ENDDO E6
              ENDDO E5
            ENDDO E4
          ENDDO E3
        ENDDO E2
      ENDDO E1
      NBKLI = NLI
      NBKCO = NCO0
      CALL DEBUG( 'CALKM  => JMAXK=',JMAXK)
      CALL DEBUG( 'CALKM  => MXKLI=',NBKLI)
      CALL DEBUG( 'CALKM  => MXKCO=',NBKCO)
      RETURN
!
9999  PRINT 8000
      PRINT *
      STOP
!
      END SUBROUTINE CALKM
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! SMIL G.PIERRE MAI 82
! MOD. T.GABARD DEC 92
! REV. 6/10/94
! REV    JAN 1995 JPC,CW (PARAMETER)
! MODIFIE 03/98 V. BOUDON ---> XY6/Oh.
!
! *** COUPLAGE DES OPERATEURS CREATIONS ET ANNHILATIONS
!
! ***   < IB !! IC * IA !! IK > = (-1)**(ICB+ICK+IC+IA) * SQRT(IGAMA)
!
!                        ( ICC , ICA ,IGAMA)
! ***   SOMME SUR IBK DE (                 ) <IB!!IC!!IBK><IBK!!IA!!IK>
!                        ( ICB , ICK , ICBK)
!
! ***   AVEC IX(I)       = 10000*VI+1000*LI+100*NI+10*CI+PI            POUR I=1,NBVQN
! ***   ET   IX(NBVQN+1) = 10000*C2+1000*C23+100*C234+10*C2345+C23456
! ***   ET   IX(NBVQN+2) = 10000*P2+1000*P23+100*P234+10*P2345+P23456
! ***   AVEC C23=C2*C3, C234=C23*C4, C2345=C234*C5 ET C23456=C2345*C6
! ***   IX PEUT ETRE  IB,IA,IC OU IK
!
      FUNCTION ELMR(IB,IC,IA,IK,IGAMA)
      use mod_dppr
      use mod_par_tds
      use mod_com_sigvi
      use mod_com_sy
      IMPLICIT NONE
      real(kind=dppr)  :: ELMR
      integer ,dimension(NBVQN+2)  :: IB,IC,IA,IK
      integer          :: IGAMA

! functions
      real(kind=dppr)  :: E1TO6,SXC
      integer          :: IPSI

      real(kind=dppr)  :: EM1,EM2
      real(kind=dppr)  :: P
      real(kind=dppr)  :: SIX

      integer ,dimension(MDMIGA)         :: ICCA2
      integer ,dimension(NBVQN)          :: IPB
      integer ,dimension(NBVQN,4)        :: IBCAK
      integer          :: I,ICA,ICB,ICC,ICC1,ICK,ICPK,II,IL,IPC,IV
      integer          :: J
      integer          :: N,NB,NCCA2
!
      ELMR = 0.D0
!
! *** TEST SUR LES FONCTIONS ET LES OPERATEURS.
!
      DO I=1,NBVQN
        IBCAK(I,1) = IB(I)
        IBCAK(I,2) = IC(I)
        IBCAK(I,3) = IA(I)
        IBCAK(I,4) = IK(I)
      ENDDO
!
! TEST DE LA COHERENCE DE L'ENSEMBLE DES NOMBRES QUANTIQUES
!
      DO J=1,4
        DO I=1,NBVQN
          CALL VLNC(IBCAK(I,J),IV,IL,N,ICC,IPC)
          II = I
          IF( I .GE. 4 ) II = 3
          ICC1 = ICC
          IF( I         .GT. 4 .AND.         &
              MOD(IV,2) .EQ. 1       ) THEN
            CALL MULTD(ICC,2,NCCA2,ICCA2)
            ICC1 = ICCA2(1)
          ENDIF
          IF( IPSI(II,IV,IL,N,ICC1) .EQ. 0 ) RETURN
        ENDDO
      ENDDO
!
! *** TEST SUR LES VI ET LES OMEGAI
!
      DO I=1,NBVQN
        IPK(I,1) = IB(I)/10000-IC(I)/10000
        IPB(I)   = IK(I)/10000-IA(I)/10000
        IF( IPB(I)   .LT. 0      ) RETURN
        IF( IPK(I,1) .NE. IPB(I) ) RETURN
      ENDDO
      ICB = IB(NBVQN+1)-10*(IB(NBVQN+1)/10)
      ICK = IK(NBVQN+1)-10*(IK(NBVQN+1)/10)
      ICC = IC(NBVQN+1)-10*(IC(NBVQN+1)/10)
      ICA = IA(NBVQN+1)-10*(IA(NBVQN+1)/10)
      P   = PC(ICC)*PC(ICA)*PC(ICB)*PC(ICK)*SQRT(DC(IGAMA))
!
! *** SOMMATION SUR LES ETATS INTERMEDIAIRES.
!
      CALL SIGVI(IPB,'IPK',0,NB)
      DO I=1,NB
        EM1  = E1TO6(1,IB,IC,IPK(1,I))
        EM2  = E1TO6(-1,IPK(1,I),IA,IK)
        ICPK = IPK(NBVQN+1,I)-10*(IPK(NBVQN+1,I)/10)
        SIX  = SXC(ICC,ICA,IGAMA,ICK,ICB,ICPK)
        ELMR = ELMR+P*SIX*EM1*EM2
      ENDDO
!
      RETURN
      END FUNCTION ELMR
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! HMODEL version
! *** CHERCHE POUR UN ENSEMBLE DONNE DE VI : (IV(I),I=1,6)
! *** LES NB INDICES DE LA SIGNATURE VIBRATIONNELLE
! *** IKNC(I,K), I=1,8 ET K=ISN+1,ISN+NB
!
!  REV 29 SEP 94
!  MODIFIE MARS 98 ---> XY6/Oh (V. BOUDON).
!
! ATTENTION : SIGVI peut redimensionner les tableaux dependants de MXC
!
      SUBROUTINE SIGVI(IV,MXC,ISN,NB)                                                              ! SMIL G.PIERRE JUIN 82
      use mod_par_tds
      use mod_main_hmodel
      IMPLICIT NONE
      integer            ,dimension(NBVQN)  :: IV
      character(len = *)                    :: MXC
      integer                               :: ISN,NB

! functions
      integer :: MULGU

      integer ,parameter  :: MDMIB2 = 7
      integer ,parameter  :: MDMIB3 = 22
      integer ,parameter  :: MDMIB4 = 22
      integer ,parameter  :: MDMIB5 = 22
      integer ,parameter  :: MDMIB6 = 22

      integer ,pointer ,dimension(:,:)     :: IKNC
      integer          ,dimension(MDMIB2)  :: IB2
      integer          ,dimension(MDMIB3)  :: IB3
      integer          ,dimension(MDMIB4)  :: IB4
      integer          ,dimension(MDMIB5)  :: IB5
      integer          ,dimension(MDMIB6)  :: IB6
      integer          ,dimension(MDMIGA)  :: IC23
      integer          ,dimension(MDMIGA)  :: IC234
      integer          ,dimension(MDMIGA)  :: IC2345
      integer          ,dimension(MDMIGA)  :: ICTO6
      integer                              :: I2,I23,I234,I2345,I23456,I3,I4,I5,I6
      integer                              :: IC2,IC3,IC4,IC5,IC6,IP2,IP234,IP3,IP4,IP5,IP6,IPTO6
      integer                              :: K
      integer                              :: MDMKNC
      integer                              :: N23,N234,N2345,N23456
      integer                              :: NB2,NB3,NB4,NB5,NB6
!
8000  FORMAT(' !!! SIGVI  : STOP ON ERROR'   ,/,   &
             '              ',A,' UNEXPECTED'   )
!
      SELECT CASE( MXC )
        CASE( 'IFF' )
          MDMKNC = MXSNV
          IKNC => IFF
        CASE( 'IKVA' )
          MDMKNC = MXSNB
          IKNC => IKVA
        CASE( 'IKVC' )
          MDMKNC = MXSNB
          IKNC => IKVC
        CASE( 'IPK' )
          MDMKNC = MXSNB
          IKNC => IPK
        CASE DEFAULT
          PRINT 8000, MXC
          STOP
!
      END SELECT
!
      K = ISN
      CALL TCUBE(2,IV(2),NB2,IB2,IP2,MDMIB2)
      CALL TCUBE(3,IV(3),NB3,IB3,IP3,MDMIB3)
      CALL TCUBE(4,IV(4),NB4,IB4,IP4,MDMIB4)
      CALL TCUBE(5,IV(5),NB5,IB5,IP5,MDMIB5)
      CALL TCUBE(6,IV(6),NB6,IB6,IP6,MDMIB6)
      DO I2=1,NB2
        IC2 = IB2(I2)-10*(IB2(I2)/10)
        DO I3=1,NB3
          IC3 = IB3(I3)-10*(IB3(I3)/10)
          CALL MULTD(IC2,IC3,N23,IC23)
          DO I23=1,N23
            DO I4=1,NB4
              IC4 = IB4(I4)-10*(IB4(I4)/10)
              CALL MULTD(IC23(I23),IC4,N234,IC234)
              IP234 = MULGU(IP3,IP4)
              DO I234=1,N234
                DO I5=1,NB5
                  IC5 = IB5(I5)-10*(IB5(I5)/10)
                  CALL MULTD(IC234(I234),IC5,N2345,IC2345)
                  DO I2345=1,N2345
                    DO I6=1,NB6
                      IC6 = IB6(I6)-10*(IB6(I6)/10)
                      CALL MULTD(IC2345(I2345),IC6,N23456,ICTO6)
                      IPTO6 = MULGU(IP234,IP6)
E2:                   DO I23456=1,N23456
                        K = K+1
                        IF( K .GT. MDMKNC ) THEN
                          SELECT CASE( MXC )
                            CASE( 'IFF' )
                              CALL RESIZE_MXSNV
                              MDMKNC = MXSNV
                              IKNC => IFF
                            CASE( 'IKVA' )
                              CALL RESIZE_MXSNB
                              MDMKNC = MXSNB
                              IKNC => IKVA
                            CASE( 'IKVC' )
                              CALL RESIZE_MXSNB
                              MDMKNC = MXSNB
                              IKNC => IKVC
                            CASE( 'IPK' )
                              CALL RESIZE_MXSNB
                              MDMKNC = MXSNB
                              IKNC => IPK
                          END SELECT
                        ENDIF
                        IKNC(1,K) = 10000*IV(1)+11
                        IKNC(2,K) = IB2(I2)*10+IP2
                        IKNC(3,K) = IB3(I3)*10+IP3
                        IKNC(4,K) = IB4(I4)*10+IP4
                        IKNC(5,K) = IB5(I5)*10+IP5
                        IKNC(6,K) = IB6(I6)*10+IP6
                        IKNC(7,K) = 10000*IC2+1000*IC23(I23)+100*IC234(I234)+10*IC2345(I2345)+ICTO6(I23456)
                        IKNC(8,K) = 10000+1000*IP3+100*IP234+10*IP234+IPTO6
                      ENDDO E2
                    ENDDO
                  ENDDO
                ENDDO
              ENDDO
            ENDDO
          ENDDO
        ENDDO
      ENDDO
      NB = K-ISN
!
      RETURN
      END SUBROUTINE SIGVI
