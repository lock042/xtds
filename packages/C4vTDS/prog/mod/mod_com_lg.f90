
      module mod_com_lg

      use mod_dppr
      use mod_par_tds
      IMPLICIT NONE

      real(kind=dppr) ,dimension(MXG)    ,save  :: GJ

      integer         ,dimension(MXG)    ,save  :: ICOD
      integer         ,dimension(MXJG+2) ,save  :: IN


      contains

!
! *** LECTURE DES G SUR LE DISQUE
!
! SMIL G.PIERRE JAN 82,MOD:MAR.86
! MOD:DEC 92, T.GABARD
! REV    JAN 1995 JPC,CW (PARAMETER)
!
!  ATTENTION : LE FICHIER LECTG_INCLUDE APPELE PAR INCLUDE, DEFINIT
!  PAR DATA LA CHAINE BASD.
!  LES TAILLES DEFINIES PAR 'character' DOIVENT ETRE SUFFISANTES.
!
      subroutine LECTG(JGLI)
      use mod_par_tds
      IMPLICIT NONE
      integer               :: JGLI

      real(kind=8)          :: GJC                                                                 ! REAL*8    dans le fichier !

      integer(kind=4)       :: ICD                                                                 ! INTEGER*4 dans le fichier !
      integer               :: I
      integer               :: MXGLI

      character(len = 150)  :: BASD
      INCLUDE 'LECTG_INCLUDE'
      character(len = 200)  :: FICG,FICNBG
!
200   FORMAT(I10)
8000  FORMAT(' !!! STOP ON ERROR')
8100  FORMAT(' !!! LECTG : INCOMPATIBLE MXJG, MXG or IN :',3I10)
8101  FORMAT(' !!! LECTG : JGLI TOO LARGE'          ,/,   &
             ' !!! MXJG   EXCEEDED : ',I10,' > ',I10   )
!
      IF( JGLI .GT. MXJG ) THEN
        PRINT 8101, JGLI,MXJG
        GOTO  9999
      ENDIF
      FICG   = TRIM(BASD)//'/gtd/gjc199.bd'
      FICNBG = TRIM(BASD)//'/gtd/nbg199'
      OPEN(14,FILE=TRIM(FICNBG),STATUS='OLD')
      READ(14,200) (IN(I),I=1,MXJG+2)
      CLOSE(14)
      IF( IN(MXJG+2)-1 .NE. MXG ) THEN
        PRINT 8100, MXJG,MXG,IN(MXJG+2)-1
        GOTO  9999
      ENDIF
      OPEN(14,FILE=TRIM(FICG),STATUS='OLD',ACCESS='DIRECT',RECL=12)
      MXGLI = IN(JGLI+2)-1
      DO I=1,MXGLI
        READ(14,REC=I) ICD,GJC
        GJ(I)   = GJC
        ICOD(I) = ICD
      ENDDO
      CLOSE(14)
      RETURN
!
9999  PRINT 8000
      PRINT *
      STOP
!
      end subroutine LECTG

      end module mod_com_lg
