
      module mod_com_eqtds

      use mod_dppr
      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      real(kind=dppr)      ,pointer ,dimension(:,:) ,save  :: AS                                   ! (MXOPH,MXOPH)
      real(kind=dppr)      ,pointer ,dimension(:)   ,save  :: YS                                   ! (MXOPH)
      real(kind=dppr)      ,pointer ,dimension(:)   ,save  :: FOBST,SDFOBT                         ! (MXOBS)
      real(kind=dppr)      ,pointer ,dimension(:)   ,save  :: SDSOBT,SOBST                         ! (MXOBS)

      integer              ,pointer ,dimension(:)   ,save  :: NCODNI,NCODNS                        ! (MXSNV)
      integer              ,pointer ,dimension(:,:) ,save  :: IPCSN,NUSVSU                         ! (MXOBS,MXSNV)
      integer              ,pointer ,dimension(:)   ,save  :: IPCINF,IPCSUP,JOBS0T,JOBST           ! (MXOBS)
      integer              ,pointer ,dimension(:)   ,save  :: NOBS0T,NOBST                         ! (MXOBS)
      integer              ,pointer ,dimension(:)   ,save  :: ICENT                                ! (MXSNV)
      integer              ,pointer ,dimension(:)   ,save  :: ISFIT,IXCL                           ! (MXOPH)
      integer              ,pointer ,dimension(:)   ,save  :: JCENTT                               ! (MXNIV)

      character(len =   2) ,pointer ,dimension(:)   ,save  :: SYOB0T,SYOBST                        ! (MXOBS)
      character(len =  20) ,pointer ,dimension(:)   ,save  :: CODNI,CODNS                          ! (MXNIV)

      logical              ,pointer ,dimension(:)   ,save  :: FOUND                                ! (MXOBS)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_EQTDS
      IMPLICIT NONE

      integer  :: ierr

      allocate(AS(MXOPH,MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_EQTDS_AS')
      AS = 0.d0
      allocate(YS(MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_EQTDS_YS')
      YS = 0.d0
      allocate(FOBST(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_EQTDS_FOBST')
      FOBST = 0.d0
      allocate(SDFOBT(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_EQTDS_SDFOBT')
      SDFOBT = 0.d0
      allocate(SDSOBT(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_EQTDS_SDSOBT')
      SDSOBT = 0.d0
      allocate(SOBST(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_EQTDS_SOBST')
      SOBST = 0.d0

      allocate(NCODNI(MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_EQTDS_NCODNI')
      NCODNI = 0
      allocate(NCODNS(MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_EQTDS_NCODNS')
      NCODNS = 0
      allocate(IPCSN(MXOBS,MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_EQTDS_IPCSN')
      IPCSN = 0
      allocate(NUSVSU(MXOBS,MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_EQTDS_NUSVSU')
      NUSVSU = 0
      allocate(IPCINF(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_EQTDS_IPCINF')
      IPCINF = 0
      allocate(IPCSUP(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_EQTDS_IPCSUP')
      IPCSUP = 0
      allocate(JOBS0T(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_EQTDS_JOBS0T')
      JOBS0T = 0
      allocate(JOBST(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_EQTDS_JOBST')
      JOBST = 0
      allocate(NOBS0T(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_EQTDS_NOBS0T')
      NOBS0T = 0
      allocate(NOBST(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_EQTDS_NOBST')
      NOBST = 0
      allocate(ICENT(MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_EQTDS_ICENT')
      ICENT = 0
      allocate(ISFIT(MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_EQTDS_ISFIT')
      ISFIT = 0
      allocate(IXCL(MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_EQTDS_IXCL')
      IXCL = 0
      allocate(JCENTT(MXNIV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_EQTDS_JCENTT')
      JCENTT = 0

      allocate(SYOB0T(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_EQTDS_SYOB0T')
      SYOB0T = ''
      allocate(SYOBST(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_EQTDS_SYOBST')
      SYOBST = ''
      allocate(CODNI(MXNIV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_EQTDS_CODNI')
      CODNI = ''
      allocate(CODNS(MXNIV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_EQTDS_CODNS')
      CODNS = ''

      allocate(FOUND(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_EQTDS_FOUND')
      FOUND = .false.
!
      return
      end subroutine ALLOC_EQTDS

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXNIV

      subroutine RESIZE_MXNIV_EQTDS(C_NIV)
      IMPLICIT NONE
      integer :: C_NIV

      integer :: ierr
      character(len =  20) ,pointer ,dimension(:)  :: cpd1

! JCENTT
      allocate(ipd1(C_NIV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXNIV_EQTDS_JCENTT')
      ipd1 = 0
      ipd1(1:MXNIV) = JCENTT(:)
      deallocate(JCENTT)
      JCENTT => ipd1

! CODNI
      allocate(cpd1(C_NIV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXNIV_EQTDS_CODNI')
      cpd1 = ''
      cpd1(1:MXNIV) = CODNI(:)
      deallocate(CODNI)
      CODNI => cpd1
! CODNS
      allocate(cpd1(C_NIV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXNIV_EQTDS_CODNS')
      cpd1 = ''
      cpd1(1:MXNIV) = CODNS(:)
      deallocate(CODNS)
      CODNS => cpd1
!
      return
      end subroutine RESIZE_MXNIV_EQTDS

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOBS

      subroutine RESIZE_MXOBS_EQTDS(C_OBS)
      IMPLICIT NONE
      integer :: C_OBS

      integer :: i,ierr

      character(len =   2) ,pointer ,dimension(:)  :: cpd1

! FOBST
      allocate(rpd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_EQTDS_FOBST')
      rpd1 = 0.d0
      rpd1(1:MXOBS) = FOBST(:)
      deallocate(FOBST)
      FOBST => rpd1
! SDFOBT
      allocate(rpd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_EQTDS_SDFOBT')
      rpd1 = 0.d0
      rpd1(1:MXOBS) = SDFOBT(:)
      deallocate(SDFOBT)
      SDFOBT => rpd1
! SDSOBT
      allocate(rpd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_EQTDS_SDSOBT')
      rpd1 = 0.d0
      rpd1(1:MXOBS) = SDSOBT(:)
      deallocate(SDSOBT)
      SDSOBT => rpd1
! SOBST
      allocate(rpd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_EQTDS_SOBST')
      rpd1 = 0.d0
      rpd1(1:MXOBS) = SOBST(:)
      deallocate(SOBST)
      SOBST => rpd1
! IPCSN
      allocate(ipd2(C_OBS,MXSNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_EQTDS_IPCSN')
      ipd2 = 0
      do i=1,MXSNV
        ipd2(1:MXOBS,i) = IPCSN(:,i)
      enddo
      deallocate(IPCSN)
      IPCSN => ipd2
! NUSVSU
      allocate(ipd2(C_OBS,MXSNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_EQTDS_NUSVSU')
      ipd2 = 0
      do i=1,MXSNV
        ipd2(1:MXOBS,i) = NUSVSU(:,i)
      enddo
      deallocate(NUSVSU)
      NUSVSU => ipd2
! IPCINF
      allocate(ipd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_EQTDS_IPCINF')
      ipd1 = 0
      ipd1(1:MXOBS) = IPCINF(:)
      deallocate(IPCINF)
      IPCINF => ipd1
! IPCSUP
      allocate(ipd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_EQTDS_IPCSUP')
      ipd1 = 0
      ipd1(1:MXOBS) = IPCSUP(:)
      deallocate(IPCSUP)
      IPCSUP => ipd1
! JOBS0T
      allocate(ipd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_EQTDS_JOBS0T')
      ipd1 = 0
      ipd1(1:MXOBS) = JOBS0T(:)
      deallocate(JOBS0T)
      JOBS0T => ipd1
! JOBST
      allocate(ipd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_EQTDS_JOBST')
      ipd1 = 0
      ipd1(1:MXOBS) = JOBST(:)
      deallocate(JOBST)
      JOBST => ipd1
! NOBS0T
      allocate(ipd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_EQTDS_NOBS0T')
      ipd1 = 0
      ipd1(1:MXOBS) = NOBS0T(:)
      deallocate(NOBS0T)
      NOBS0T => ipd1
! NOBST
      allocate(ipd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_EQTDS_NOBST')
      ipd1 = 0
      ipd1(1:MXOBS) = NOBST(:)
      deallocate(NOBST)
      NOBST => ipd1

! SYOB0T
      allocate(cpd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_EQTDS_SYOB0T')
      cpd1 = ''
      cpd1(1:MXOBS) = SYOB0T(:)
      deallocate(SYOB0T)
      SYOB0T => cpd1
! SYOBST
      allocate(cpd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_EQTDS_SYOBST')
      cpd1 = ''
      cpd1(1:MXOBS) = SYOBST(:)
      deallocate(SYOBST)
      SYOBST => cpd1

! FOUND
      allocate(lpd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_EQTDS_FOUND')
      lpd1 = .false.
      lpd1(1:MXOBS) = FOUND(:)
      deallocate(FOUND)
      FOUND => lpd1
!
      return
      end subroutine RESIZE_MXOBS_EQTDS

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOPH

      subroutine RESIZE_MXOPH_EQTDS(C_OPH)
      IMPLICIT NONE
      integer :: C_OPH

      integer :: i,ierr

! AS
      allocate(rpd2(C_OPH,C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_EQTDS_AS')
      rpd2 = 0.d0
      do i=1,MXOPH
        rpd2(1:MXOPH,i) = AS(:,i)
      enddo
      deallocate(AS)
      AS => rpd2
! YS
      allocate(rpd1(C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_EQTDS_YS')
      rpd1 = 0.d0
      rpd1(1:MXOPH) = YS(:)
      deallocate(YS)
      YS => rpd1
! ISFIT
      allocate(ipd1(C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_EQTDS_ISFIT')
      ipd1 = 0
      ipd1(1:MXOPH) = ISFIT(:)
      deallocate(ISFIT)
      ISFIT => ipd1
! IXCL
      allocate(ipd1(C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_EQTDS_IXCL')
      ipd1 = 0
      ipd1(1:MXOPH) = IXCL(:)
      deallocate(IXCL)
      IXCL => ipd1
!
      return
      end subroutine RESIZE_MXOPH_EQTDS

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXSNV

      subroutine RESIZE_MXSNV_EQTDS(C_SNV)
      IMPLICIT NONE
      integer :: C_SNV

      integer :: i,ierr

! NCODNI
      allocate(ipd1(C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_EQTDS_NCODNI')
      ipd1 = 0
      ipd1(1:MXSNV) = NCODNI(:)
      deallocate(NCODNI)
      NCODNI => ipd1
! NCODNS
      allocate(ipd1(C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_EQTDS_NCODNS')
      ipd1 = 0
      ipd1(1:MXSNV) = NCODNS(:)
      deallocate(NCODNS)
      NCODNS => ipd1
! IPCSN
      allocate(ipd2(MXOBS,C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_EQTDS_IPCSN')
      ipd2 = 0
      do i=1,MXOBS
        ipd2(i,1:MXSNV) = IPCSN(i,:)
      enddo
      deallocate(IPCSN)
      IPCSN => ipd2
! NUSVSU
      allocate(ipd2(MXOBS,C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_EQTDS_NUSVSU')
      ipd2 = 0
      do i=1,MXOBS
        ipd2(i,1:MXSNV) = NUSVSU(i,:)
      enddo
      deallocate(NUSVSU)
      NUSVSU => ipd2
! ICENT
      allocate(ipd1(C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_EQTDS_ICENT')
      ipd1 = 0
      ipd1(1:MXSNV) = ICENT(:)
      deallocate(ICENT)
      ICENT => ipd1
!
      return
      end subroutine RESIZE_MXSNV_EQTDS

      end module mod_com_eqtds
