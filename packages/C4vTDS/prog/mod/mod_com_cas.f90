
      module mod_com_cas

      use mod_dppr
      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      character(len =   1) ,pointer ,dimension(:) ,save  :: FASOT                                  ! (MXOBS)
      character(len =   1) ,pointer ,dimension(:) ,save  :: ISPT                                   ! (MXOBS)
      character(len =   1) ,pointer ,dimension(:) ,save  :: SASOT                                  ! (MXOBS)
      character(len =   5) ,pointer ,dimension(:) ,save  :: NUOT                                   ! (MXOBS)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_CAS
      IMPLICIT NONE

      integer  :: ierr

      allocate(FASOT(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_CAS_FASOT')
      FASOT = ''
      allocate(ISPT(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_CAS_ISPT')
      ISPT = ''
      allocate(SASOT(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_CAS_SASOT')
      SASOT = ''
      allocate(NUOT(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_CAS_NUOT')
      NUOT = ''
!
      return
      end subroutine ALLOC_CAS

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOBS

      subroutine RESIZE_MXOBS_CAS(C_OBS)
      IMPLICIT NONE
      integer :: C_OBS

      integer :: ierr

      character(len =   1) ,pointer ,dimension(:)  :: cpd1_1
      character(len =   5) ,pointer ,dimension(:)  :: cpd1_5

! FASOT
      allocate(cpd1_1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_CAS_FASOT')
      cpd1_1 = ''
      cpd1_1(1:MXOBS) = FASOT(:)
      deallocate(FASOT)
      FASOT => cpd1_1
! ISPT
      allocate(cpd1_1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_CAS_ISPT')
      cpd1_1 = ''
      cpd1_1(1:MXOBS) = ISPT(:)
      deallocate(ISPT)
      ISPT => cpd1_1
! SASOT
      allocate(cpd1_1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_CAS_SASOT')
      cpd1_1 = ''
      cpd1_1(1:MXOBS) = SASOT(:)
      deallocate(SASOT)
      SASOT => cpd1_1
! NUOT
      allocate(cpd1_5(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_CAS_NUOT')
      cpd1_5 = ''
      cpd1_5(1:MXOBS) = NUOT(:)
      deallocate(NUOT)
      NUOT => cpd1_5
!
      return
      end subroutine RESIZE_MXOBS_CAS

      end module mod_com_cas
