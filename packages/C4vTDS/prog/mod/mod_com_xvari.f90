
      module mod_com_xvari                                                                         ! variables related COMMON

      IMPLICIT NONE

      integer ,save  :: LPCMAX = 0
      integer ,save  :: NBITER = 0

      logical ,save  :: IONLY  = .false.
      logical ,save  :: LADIP  = .false.
      logical ,save  :: LAPOL  = .false.

      end module mod_com_xvari
