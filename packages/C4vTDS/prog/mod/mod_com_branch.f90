
      module mod_com_branch

      use mod_par_tds
      IMPLICIT NONE

      character(len =   2) ,dimension(MXBRA) ,parameter  :: BRANCH = (/ 'O ', 'P ', 'Q ', 'R ', 'S ' /)

      end module mod_com_branch
