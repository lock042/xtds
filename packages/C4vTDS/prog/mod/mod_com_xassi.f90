
      module mod_com_xassi                                                                         ! assignment related COMMON

      use mod_dppr
      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      real(kind=dppr) ,save  :: RMXOMC = 0.d0

      integer         ,save  :: NBOBS  = 0
!
      real(kind=dppr)      ,pointer ,dimension(:) ,save  :: COEF                                   ! (MXOBS)
      real(kind=dppr)      ,pointer ,dimension(:) ,save  :: FOBS                                   ! (MXOBS)
      real(kind=dppr)      ,pointer ,dimension(:) ,save  :: SDFOBS                                 ! (MXOBS)
      real(kind=dppr)      ,pointer ,dimension(:) ,save  :: SDSOBS                                 ! (MXOBS)
      real(kind=dppr)      ,pointer ,dimension(:) ,save  :: SOBS                                   ! (MXOBS)
      real(kind=dppr)      ,pointer ,dimension(:) ,save  :: TEMP                                   ! (MXOBS)

      integer              ,pointer ,dimension(:) ,save  :: ICIOBS                                 ! (MXOBS)
      integer              ,pointer ,dimension(:) ,save  :: ICSOBS                                 ! (MXOBS)
      integer              ,pointer ,dimension(:) ,save  :: IFOBZ                                  ! (MXOBS)
      integer              ,pointer ,dimension(:) ,save  :: ISOBZ                                  ! (MXOBS)
      integer              ,pointer ,dimension(:) ,save  :: JIOBS                                  ! (MXOBS)
      integer              ,pointer ,dimension(:) ,save  :: JSOBS                                  ! (MXOBS)
      integer              ,pointer ,dimension(:) ,save  :: JTR                                    ! (MXOBS)
      integer              ,pointer ,dimension(:) ,save  :: NIOBS                                  ! (MXOBS)
      integer              ,pointer ,dimension(:) ,save  :: NSOBS                                  ! (MXOBS)

      character(len =   1) ,pointer ,dimension(:) ,save  :: CPOLI                                  ! (MXOBS)
      character(len =   1) ,pointer ,dimension(:) ,save  :: CPOLS                                  ! (MXOBS)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_XASSI
      IMPLICIT NONE

      integer  :: ierr

      allocate(COEF(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XASSI_COEF')
      COEF = 0.d0
      allocate(FOBS(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XASSI_FOBS')
      FOBS = 0.d0
      allocate(SDFOBS(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XASSI_SDFOBS')
      SDFOBS = 0.d0
      allocate(SDSOBS(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XASSI_SDSOBS')
      SDSOBS = 0.d0
      allocate(SOBS(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XASSI_SOBS')
      SOBS = 0.d0
      allocate(TEMP(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XASSI_TEMP')
      TEMP = 0.d0

      allocate(ICIOBS(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XASSI_ICIOBS')
      ICIOBS = 0
      allocate(ICSOBS(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XASSI_ICSOBS')
      ICSOBS = 0
      allocate(IFOBZ(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XASSI_IFOBZ')
      IFOBZ = 0
      allocate(ISOBZ(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XASSI_ISOBZ')
      ISOBZ = 0
      allocate(JIOBS(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XASSI_JIOBS')
      JIOBS = 0
      allocate(JSOBS(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XASSI_JSOBS')
      JSOBS = 0
      allocate(JTR(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XASSI_JTR')
      JTR = 0
      allocate(NIOBS(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XASSI_NIOBS')
      NIOBS = 0
      allocate(NSOBS(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XASSI_NSOBS')
      NSOBS = 0

      allocate(CPOLI(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XASSI_CPOLI')
      CPOLI = ''
      allocate(CPOLS(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XASSI_CPOLS')
      CPOLS = ''
!
      return
      end subroutine ALLOC_XASSI

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOBS

      subroutine RESIZE_MXOBS_XASSI(C_OBS)
      IMPLICIT NONE
      integer :: C_OBS

      integer :: ierr

      character(len =   1) ,pointer ,dimension(:)  :: cpd1

! COEF
      allocate(rpd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_XASSI_COEF')
      rpd1 = 0.d0
      rpd1(1:MXOBS) = COEF(:)
      deallocate(COEF)
      COEF => rpd1
! FOBS
      allocate(rpd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_XASSI_FOBS')
      rpd1 = 0.d0
      rpd1(1:MXOBS) = FOBS(:)
      deallocate(FOBS)
      FOBS => rpd1
! SDFOBS
      allocate(rpd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_XASSI_SDFOBS')
      rpd1 = 0.d0
      rpd1(1:MXOBS) = SDFOBS(:)
      deallocate(SDFOBS)
      SDFOBS => rpd1
! SDSOBS
      allocate(rpd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_XASSI_SDSOBS')
      rpd1 = 0.d0
      rpd1(1:MXOBS) = SDSOBS(:)
      deallocate(SDSOBS)
      SDSOBS => rpd1
! SOBS
      allocate(rpd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_XASSI_SOBS')
      rpd1 = 0.d0
      rpd1(1:MXOBS) = SOBS(:)
      deallocate(SOBS)
      SOBS => rpd1
! TEMP
      allocate(rpd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_XASSI_TEMP')
      rpd1 = 0.d0
      rpd1(1:MXOBS) = TEMP(:)
      deallocate(TEMP)
      TEMP => rpd1

! ICIOBS
      allocate(ipd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_XASSI_ICIOBS')
      ipd1 = 0
      ipd1(1:MXOBS) = ICIOBS(:)
      deallocate(ICIOBS)
      ICIOBS => ipd1
! ICSOBS
      allocate(ipd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_XASSI_ICSOBS')
      ipd1 = 0
      ipd1(1:MXOBS) = ICSOBS(:)
      deallocate(ICSOBS)
      ICSOBS => ipd1
! IFOBZ
      allocate(ipd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_XASSI_IFOBZ')
      ipd1 = 0
      ipd1(1:MXOBS) = IFOBZ(:)
      deallocate(IFOBZ)
      IFOBZ => ipd1
! ISOBZ
      allocate(ipd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_XASSI_ISOBZ')
      ipd1 = 0
      ipd1(1:MXOBS) = ISOBZ(:)
      deallocate(ISOBZ)
      ISOBZ => ipd1
! JIOBS
      allocate(ipd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_XASSI_JIOBS')
      ipd1 = 0
      ipd1(1:MXOBS) = JIOBS(:)
      deallocate(JIOBS)
      JIOBS => ipd1
! JSOBS
      allocate(ipd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_XASSI_JSOBS')
      ipd1 = 0
      ipd1(1:MXOBS) = JSOBS(:)
      deallocate(JSOBS)
      JSOBS => ipd1
! JTR
      allocate(ipd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_XASSI_JTR')
      ipd1 = 0
      ipd1(1:MXOBS) = JTR(:)
      deallocate(JTR)
      JTR => ipd1
! NIOBS
      allocate(ipd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_XASSI_NIOBS')
      ipd1 = 0
      ipd1(1:MXOBS) = NIOBS(:)
      deallocate(NIOBS)
      NIOBS => ipd1
! NSOBS
      allocate(ipd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_XASSI_NSOBS')
      ipd1 = 0
      ipd1(1:MXOBS) = NSOBS(:)
      deallocate(NSOBS)
      NSOBS => ipd1

! CPOLI
      allocate(cpd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_XASSI_CPOLI')
      cpd1 = ''
      cpd1(1:MXOBS) = CPOLI(:)
      deallocate(CPOLI)
      CPOLI => cpd1
! CPOLS
      allocate(cpd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_XASSI_CPOLS')
      cpd1 = ''
      cpd1(1:MXOBS) = CPOLS(:)
      deallocate(CPOLS)
      CPOLS => cpd1
!
      return
      end subroutine RESIZE_MXOBS_XASSI

      end module mod_com_xassi
