
      module mod_com_simul

      use mod_dppr
      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      real(kind=dppr) ,save  :: XU    = 0.d0
      real(kind=dppr) ,save  :: XUMIS = 0.d0
      real(kind=dppr) ,save  :: XUMAS = 0.d0
      real(kind=dppr) ,save  :: PASU  = 0.d0

      integer         ,save  :: NF    = 0
      integer         ,save  :: L0    = 0
      integer         ,save  :: NTRA  = 0

      logical         ,save  :: TRANS = .false.
      logical         ,save  :: RAMAN = .false.
      logical         ,save  :: ABSOR = .false.
      logical         ,save  :: COABS = .false.
!
      real(kind=dppr)      ,pointer ,dimension(:)   ,save  :: CDAE,CMGHZ                           ! (MXNLF)
      real(kind=dppr)      ,pointer ,dimension(:)   ,save  :: FREQ,TINT                            ! (MXNTRA+1)
      real(kind=dppr)      ,pointer ,dimension(:,:) ,save  :: PV                                   ! (MXNPV,MXNLF)
      real(kind=dppr)      ,pointer ,dimension(:)   ,save  :: FA                                   ! (2*MXNMI+1)
      real(kind=dppr)      ,pointer ,dimension(:)   ,save  :: SP                                   ! (2*MXNMI+2)
      real(kind=dppr)      ,pointer ,dimension(:)   ,save  :: PPA,CLEN,UK,XUMI,XUMA                ! (MXNLF)
      real(kind=dppr)      ,pointer ,dimension(:)   ,save  :: PPTC,PTA,PTTC                        ! (MXNLF)
      real(kind=dppr)      ,pointer ,dimension(:)   ,save  :: RM                                   ! (MXNLF)
      real(kind=dppr)      ,pointer ,dimension(:)   ,save  :: YCAL                                 ! (MXNBES)

      integer              ,pointer ,dimension(:)   ,save  :: K,INC                                ! (MXNTRA+1)
      integer              ,pointer ,dimension(:)   ,save  :: NPV                                  ! (MXNLF)

      character(len = 100) ,pointer ,dimension(:)   ,save  :: NOM                                  ! (MXNLF)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_SIMUL
      IMPLICIT NONE

      integer  :: ierr

      allocate(CDAE(MXNLF),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_SIMUL_CDAE')
      CDAE = 0.d0
      allocate(CMGHZ(MXNLF),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_SIMUL_CMGHZ')
      CMGHZ = 0.d0
      allocate(FREQ(MXNTRA+1),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_SIMUL_FREQ')
      FREQ = 0.d0
      allocate(TINT(MXNTRA+1),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_SIMUL_TINT')
      TINT = 0.d0
      allocate(PV(MXNPV,MXNLF),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_SIMUL_PV')
      PV = 0.d0
      allocate(FA(2*MXNMI+1),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_SIMUL_FA')
      FA = 0.d0
      allocate(SP(2*MXNMI+2),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_SIMUL_SP')
      SP = 0.d0
      allocate(PPA(MXNLF),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_SIMUL_PPA')
      PPA = 0.d0
      allocate(CLEN(MXNLF),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_SIMUL_CLEN')
      CLEN = 0.d0
      allocate(UK(MXNLF),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_SIMUL_UK')
      UK = 0.d0
      allocate(XUMI(MXNLF),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_SIMUL_XUMI')
      XUMI = 0.d0
      allocate(XUMA(MXNLF),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_SIMUL_XUMA')
      XUMA = 0.d0
      allocate(PPTC(MXNLF),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_SIMUL_PPTC')
      PPTC = 0.d0
      allocate(PTA(MXNLF),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_SIMUL_PTA')
      PTA = 0.d0
      allocate(PTTC(MXNLF),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_SIMUL_PTTC')
      PTTC = 0.d0
      allocate(RM(MXNLF),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_SIMUL_RM')
      RM = 0.d0
      allocate(YCAL(MXNBES),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_SIMUL_YCAL')
      YCAL = 0.d0

      allocate(K(MXNTRA+1),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_SIMUL_K')
      K = 0
      allocate(INC(MXNTRA+1),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_SIMUL_INC')
      INC = 0
      allocate(NPV(MXNLF),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_SIMUL_NPV')
      NPV = 0

      allocate(NOM(MXNLF),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_SIMUL_NOM')
      NOM = ''
!
      return
      end subroutine ALLOC_SIMUL

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXNBES

      subroutine RESIZE_MXNBES_SIMUL(C_NBES)
      IMPLICIT NONE
      integer :: C_NBES

      integer :: ierr

! YCAL
      allocate(rpd1(C_NBES),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXNBES_SIMUL_YCAL')
      rpd1 = 0.d0
      rpd1(1:MXNBES) = YCAL(:)
      deallocate(YCAL)
      YCAL => rpd1
!
      return
      end subroutine RESIZE_MXNBES_SIMUL

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXNLF

      subroutine RESIZE_MXNLF_SIMUL(C_NLF)
      IMPLICIT NONE
      integer :: C_NLF

      integer :: i,ierr

      character(len = 100) ,pointer ,dimension(:)  :: cpd1_100

! CDAE
      allocate(rpd1(C_NLF),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXNLF_SIMUL_CDAE')
      rpd1 = 0.d0
      rpd1(1:MXNLF) = CDAE(:)
      deallocate(CDAE)
      CDAE => rpd1
! CMGHZ
      allocate(rpd1(C_NLF),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXNLF_SIMUL_CMGHZ')
      rpd1 = 0.d0
      rpd1(1:MXNLF) = CMGHZ(:)
      deallocate(CMGHZ)
      CMGHZ => rpd1
! PV
      allocate(rpd2(MXNPV,C_NLF),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXNLF_SIMUL_PV')
      rpd2 = 0.d0
      do i=1,MXNPV
        rpd2(i,1:MXNLF) = PV(i,:)
      enddo
      deallocate(PV)
      PV => rpd2
! PPA
      allocate(rpd1(C_NLF),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXNLF_SIMUL_PPA')
      rpd1 = 0.d0
      rpd1(1:MXNLF) = PPA(:)
      deallocate(PPA)
      PPA => rpd1
! CLEN
      allocate(rpd1(C_NLF),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXNLF_SIMUL_CLEN')
      rpd1 = 0.d0
      rpd1(1:MXNLF) = CLEN(:)
      deallocate(CLEN)
      CLEN => rpd1
! UK
      allocate(rpd1(C_NLF),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXNLF_SIMUL_UK')
      rpd1 = 0.d0
      rpd1(1:MXNLF) = UK(:)
      deallocate(UK)
      UK => rpd1
! XUMI
      allocate(rpd1(C_NLF),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXNLF_SIMUL_XUMI')
      rpd1 = 0.d0
      rpd1(1:MXNLF) = XUMI(:)
      deallocate(XUMI)
      XUMI => rpd1
! XUMA
      allocate(rpd1(C_NLF),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXNLF_SIMUL_XUMA')
      rpd1 = 0.d0
      rpd1(1:MXNLF) = XUMA(:)
      deallocate(XUMA)
      XUMA => rpd1
! PPTC
      allocate(rpd1(C_NLF),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXNLF_SIMUL_PPTC')
      rpd1 = 0.d0
      rpd1(1:MXNLF) = PPTC(:)
      deallocate(PPTC)
      PPTC => rpd1
! PTA
      allocate(rpd1(C_NLF),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXNLF_SIMUL_PTA')
      rpd1 = 0.d0
      rpd1(1:MXNLF) = PTA(:)
      deallocate(PTA)
      PTA => rpd1
! PTTC
      allocate(rpd1(C_NLF),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXNLF_SIMUL_PTTC')
      rpd1 = 0.d0
      rpd1(1:MXNLF) = PTTC(:)
      deallocate(PTTC)
      PTTC => rpd1
! RM
      allocate(rpd1(C_NLF),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXNLF_SIMUL_RM')
      rpd1 = 0.d0
      rpd1(1:MXNLF) = RM(:)
      deallocate(RM)
      RM => rpd1
! NPV
      allocate(ipd1(C_NLF),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXNLF_SIMUL_NPV')
      ipd1 = 0
      ipd1(1:MXNLF) = NPV(:)
      deallocate(NPV)
      NPV => ipd1
! NOM
      allocate(cpd1_100(C_NLF),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXNLF_SIMUL_NOM')
      cpd1_100 = ''
      cpd1_100(1:MXNLF) = NOM(:)
      deallocate(NOM)
      NOM => cpd1_100
!
      return
      end subroutine RESIZE_MXNLF_SIMUL

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXNMI

      subroutine RESIZE_MXNMI_SIMUL(C_NMI)
      IMPLICIT NONE
      integer :: C_NMI

      integer :: ierr

! FA
      allocate(rpd1(2*C_NMI+1),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXNMI_SIMUL_FA')
      rpd1 = 0.d0
      rpd1(1:2*MXNMI+1) = FA(:)
      deallocate(FA)
      FA => rpd1
! SP
      allocate(rpd1(2*C_NMI+2),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXNMI_SIMUL_SP')
      rpd1 = 0.d0
      rpd1(1:2*MXNMI+2) = SP(:)
      deallocate(SP)
      SP => rpd1
!
      return
      end subroutine RESIZE_MXNMI_SIMUL

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXNPV

      subroutine RESIZE_MXNPV_SIMUL(C_NPV)
      IMPLICIT NONE
      integer :: C_NPV

      integer :: i,ierr

! PV
      allocate(rpd2(C_NPV,MXNLF),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXNPV_SIMUL_PV')
      rpd2 = 0.d0
      do i=1,MXNLF
        rpd2(1:MXNPV,i) = PV(:,i)
      enddo
      deallocate(PV)
      PV => rpd2
!
      return
      end subroutine RESIZE_MXNPV_SIMUL

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXNTRA

      subroutine RESIZE_MXNTRA_SIMUL(C_TRA)
      IMPLICIT NONE
      integer :: C_TRA

      integer :: ierr

! FREQ
      allocate(rpd1(C_TRA+1),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXNTRA_SIMUL_FREQ')
      rpd1 = 0.d0
      rpd1(1:MXNTRA+1) = FREQ(:)
      deallocate(FREQ)
      FREQ => rpd1
! TINT
      allocate(rpd1(C_TRA+1),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXNTRA_SIMUL_TINT')
      rpd1 = 0.d0
      rpd1(1:MXNTRA+1) = TINT(:)
      deallocate(TINT)
      TINT => rpd1
! K
      allocate(ipd1(C_TRA+1),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXNTRA_SIMUL_K')
      ipd1 = 0
      ipd1(1:MXNTRA+1) = K(:)
      deallocate(K)
      K => ipd1
! INC
      allocate(ipd1(C_TRA+1),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXNTRA_SIMUL_INC')
      ipd1 = 0
      ipd1(1:MXNTRA+1) = INC(:)
      deallocate(INC)
      INC => ipd1
!
      return
      end subroutine RESIZE_MXNTRA_SIMUL

      end module mod_com_simul
