
      module mod_com_dipomod

      use mod_dppr
      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      integer         ,pointer ,dimension(:,:)   ,save  :: IFFI,IFFS                               ! (NBVQN+1,MXSNV)
      integer         ,pointer ,dimension(:)     ,save  :: ICODRO,ICODSY,ICODVA,ICODVC,ICODVI      ! (MXOPT)
      integer         ,pointer ,dimension(:,:,:) ,save  :: NVIBI,NVIBS                             ! (MXPOL,MXNIV,NBVQN)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_DIPOMOD
      IMPLICIT NONE

      integer  :: ierr

      allocate(IFFI(NBVQN+1,MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'IFFI')
      IFFI = 0
      allocate(IFFS(NBVQN+1,MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'IFFS')
      IFFS = 0
      allocate(ICODRO(MXOPT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DIPOMOD_ICODRO')
      ICODRO = 0
      allocate(ICODSY(MXOPT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DIPOMOD_ICODSY')
      ICODSY = 0
      allocate(ICODVA(MXOPT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DIPOMOD_ICODVA')
      ICODVA = 0
      allocate(ICODVC(MXOPT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DIPOMOD_ICODVC')
      ICODVC = 0
      allocate(ICODVI(MXOPT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DIPOMOD_ICODVI')
      ICODVI = 0
      allocate(NVIBI(MXPOL,MXNIV,NBVQN),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DIPOMOD_NVIBI')
      NVIBI = 0
      allocate(NVIBS(MXPOL,MXNIV,NBVQN),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DIPOMOD_NVIBS')
      NVIBS = 0
!
      return
      end subroutine ALLOC_DIPOMOD

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXNIV

      subroutine RESIZE_MXNIV_DIPOMOD(C_NIV)
      IMPLICIT NONE
      integer :: C_NIV

      integer :: i,ierr
      integer :: j

! NVIBI
      allocate(ipd3(MXPOL,C_NIV,NBVQN),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXNIV_DIPOMOD_NVIBI')
      ipd3 = 0
      do j=1,NBVQN
        do i=1,MXNIV
          ipd3(:,i,j) = NVIBI(:,i,j)
        enddo
      enddo
      deallocate(NVIBI)
      NVIBI => ipd3
! NVIBS
      allocate(ipd3(MXPOL,C_NIV,NBVQN),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXNIV_DIPOMOD_NVIBS')
      ipd3 = 0
      do j=1,NBVQN
        do i=1,MXNIV
          ipd3(:,i,j) = NVIBS(:,i,j)
        enddo
      enddo
      deallocate(NVIBS)
      NVIBS => ipd3
!
      return
      end subroutine RESIZE_MXNIV_DIPOMOD

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOPT

      subroutine RESIZE_MXOPT_DIPOMOD(C_OPT)
      IMPLICIT NONE
      integer :: C_OPT

      integer :: ierr

! ICODRO
      allocate(ipd1(C_OPT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPT_DIPOMOD_ICODRO')
      ipd1 = 0
      ipd1(1:MXOPT) = ICODRO(:)
      deallocate(ICODRO)
      ICODRO => ipd1
! ICODSY
      allocate(ipd1(C_OPT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPT_DIPOMOD_ICODSY')
      ipd1 = 0
      ipd1(1:MXOPT) = ICODSY(:)
      deallocate(ICODSY)
      ICODSY => ipd1
! ICODVA
      allocate(ipd1(C_OPT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPT_DIPOMOD_ICODVA')
      ipd1 = 0
      ipd1(1:MXOPT) = ICODVA(:)
      deallocate(ICODVA)
      ICODVA => ipd1
! ICODVC
      allocate(ipd1(C_OPT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPT_DIPOMOD_ICODVC')
      ipd1 = 0
      ipd1(1:MXOPT) = ICODVC(:)
      deallocate(ICODVC)
      ICODVC => ipd1
! ICODVI
      allocate(ipd1(C_OPT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPT_DIPOMOD_ICODVI')
      ipd1 = 0
      ipd1(1:MXOPT) = ICODVI(:)
      deallocate(ICODVI)
      ICODVI => ipd1
!
      return
      end subroutine RESIZE_MXOPT_DIPOMOD

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXSNV

      subroutine RESIZE_MXSNV_DIPOMOD(C_SNV)
      IMPLICIT NONE
      integer :: C_SNV

      integer :: i,ierr

! IFFI
      allocate(ipd2(NBVQN+1,C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_DIPOMOD_IFFI')
      ipd2 = 0
      do i=1,MXSNV
        ipd2(:,i) = IFFI(:,i)
      enddo
      deallocate(IFFI)
      IFFI => ipd2
! IFFS
      allocate(ipd2(NBVQN+1,C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_DIPOMOD_IFFS')
      ipd2 = 0
      do i=1,MXSNV
        ipd2(:,i) = IFFS(:,i)
      enddo
      deallocate(IFFS)
      IFFS => ipd2
!
      return
      end subroutine RESIZE_MXSNV_DIPOMOD

      end module mod_com_dipomod
