
      module mod_com_hmodel

      use mod_dppr
      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      integer         ,pointer ,dimension(:,:)   ,save  :: IFF                                     ! (NBVQN+1,MXSNV)
      integer         ,pointer ,dimension(:)     ,save  :: ICODRO,ICODSY,ICODVA,ICODVC,ICODVI      ! (MXOPH)
      integer         ,pointer ,dimension(:,:,:) ,save  :: NVIB                                    ! (MXPOL,MXNIV,NBVQN)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_HMODEL
      IMPLICIT NONE

      integer  :: ierr

      allocate(IFF(NBVQN+1,MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_HMODEL_IFF')
      IFF = 0
      allocate(ICODRO(MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_HMODEL_ICODRO')
      ICODRO = 0
      allocate(ICODSY(MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_HMODEL_ICODSY')
      ICODSY = 0
      allocate(ICODVA(MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_HMODEL_ICODVA')
      ICODVA = 0
      allocate(ICODVC(MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_HMODEL_ICODVC')
      ICODVC = 0
      allocate(ICODVI(MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_HMODEL_ICODVI')
      ICODVI = 0
      allocate(NVIB(MXPOL,MXNIV,NBVQN),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_HMODEL_NVIB')
      NVIB = 0
!
      return
      end subroutine ALLOC_HMODEL

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXNIV

      subroutine RESIZE_MXNIV_HMODEL(C_NIV)
      IMPLICIT NONE
      integer :: C_NIV

      integer :: i,ierr
      integer :: j

! NVIB
      allocate(ipd3(MXPOL,C_NIV,NBVQN),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXNIV_HMODEL_NVIB')
      ipd3 = 0
      do j=1,NBVQN
        do i=1,MXNIV
          ipd3(:,i,j) = NVIB(:,i,j)
        enddo
      enddo
      deallocate(NVIB)
      NVIB => ipd3
!
      return
      end subroutine RESIZE_MXNIV_HMODEL

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOPH

      subroutine RESIZE_MXOPH_HMODEL(C_OPH)
      IMPLICIT NONE
      integer :: C_OPH

      integer :: ierr

! ICODRO
      allocate(ipd1(C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_HMODEL_ICODRO')
      ipd1 = 0
      ipd1(1:MXOPH) = ICODRO(:)
      deallocate(ICODRO)
      ICODRO => ipd1
! ICODSY
      allocate(ipd1(C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_HMODEL_ICODSY')
      ipd1 = 0
      ipd1(1:MXOPH) = ICODSY(:)
      deallocate(ICODSY)
      ICODSY => ipd1
! ICODVA
      allocate(ipd1(C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_HMODEL_ICODVA')
      ipd1 = 0
      ipd1(1:MXOPH) = ICODVA(:)
      deallocate(ICODVA)
      ICODVA => ipd1
! ICODVC
      allocate(ipd1(C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_HMODEL_ICODVC')
      ipd1 = 0
      ipd1(1:MXOPH) = ICODVC(:)
      deallocate(ICODVC)
      ICODVC => ipd1
! ICODVI
      allocate(ipd1(C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_HMODEL_ICODVI')
      ipd1 = 0
      ipd1(1:MXOPH) = ICODVI(:)
      deallocate(ICODVI)
      ICODVI => ipd1
!
      return
      end subroutine RESIZE_MXOPH_HMODEL

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXSNV

      subroutine RESIZE_MXSNV_HMODEL(C_SNV)
      IMPLICIT NONE
      integer :: C_SNV

      integer :: i,ierr

! IFF
      allocate(ipd2(NBVQN+1,C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_HMODEL_IFF')
      ipd2 = 0
      do i=1,MXSNV
        ipd2(:,i) = IFF(:,i)
      enddo
      deallocate(IFF)
      IFF => ipd2
!
      return
      end subroutine RESIZE_MXSNV_HMODEL

      end module mod_com_hmodel
