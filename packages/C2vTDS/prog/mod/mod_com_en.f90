
      module mod_com_en

      use mod_dppr
      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      real(kind=dppr) ,pointer ,dimension(:) ,save  :: ENINF,ENSUP                                 ! (MXOBS)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_EN
      IMPLICIT NONE

      integer  :: ierr

      allocate(ENINF(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_EN_ENINF')
      ENINF = 0.d0
      allocate(ENSUP(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_EN_ENSUP')
      ENSUP = 0.d0
!
      return
      end subroutine ALLOC_EN

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOBS

      subroutine RESIZE_MXOBS_EN(C_OBS)
      IMPLICIT NONE
      integer :: C_OBS

      integer :: ierr

! ENINF
      allocate(rpd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_EN_ENINF')
      rpd1 = 0.d0
      rpd1(1:MXOBS) = ENINF(:)
      deallocate(ENINF)
      ENINF => rpd1
! ENSUP
      allocate(rpd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_EN_ENSUP')
      rpd1 = 0.d0
      rpd1(1:MXOBS) = ENSUP(:)
      deallocate(ENSUP)
      ENSUP => rpd1
!
      return
      end subroutine RESIZE_MXOBS_EN

      end module mod_com_en
