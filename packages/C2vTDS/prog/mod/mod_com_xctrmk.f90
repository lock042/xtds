
      module mod_com_xctrmk                                                                        ! variables related COMMON

      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      integer              ,save  :: NCL1   = 0

      character(len = 120) ,save  :: FPARAC = ''
!
      integer ,pointer ,dimension(:) ,save  :: KTRT                                                ! (MXATR)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_XCTRMK
      IMPLICIT NONE

      integer  :: ierr

      allocate(KTRT(MXATR),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_XCTRMK_KTRT')
      KTRT = 0
!
      return
      end subroutine ALLOC_XCTRMK

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXATR

      subroutine RESIZE_MXATR_XCTRMK(C_ATR)
      IMPLICIT NONE
      integer :: C_ATR

      integer :: ierr

! KTRT
      allocate(ipd1(C_ATR),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXATR_XCTRMK_KTRT')
      ipd1 = 0
      ipd1(1:MXATR) = KTRT(:)
      deallocate(KTRT)
      KTRT => ipd1
!
      return
      end subroutine RESIZE_MXATR_XCTRMK

      end module mod_com_xctrmk
