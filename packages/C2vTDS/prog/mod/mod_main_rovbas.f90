
      module mod_main_rovbas

      use mod_dppr
      use mod_par_tds
      use mod_com_rovbas


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine alloc_all
      IMPLICIT NONE

      call alloc_rovbas
!
      return
      end subroutine alloc_all

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXDIMS
      IMPLICIT NONE

      integer  :: M_DIMS

      M_DIMS = MXRES(MXDIMS)
      call RESIZE_MXDIMS_ROVBAS(M_DIMS)
      MXDIMS = M_DIMS
!
      return
      end subroutine RESIZE_MXDIMS

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXNCR
      IMPLICIT NONE

      integer  :: M_NCR

      M_NCR = MXRES(MXNCR)
      call RESIZE_MXNCR_ROVBAS(M_NCR)
      MXNCR = M_NCR
!
      return
      end subroutine RESIZE_MXNCR

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXSNV
      IMPLICIT NONE

      integer  :: M_SNV

      M_SNV = MXRES(MXSNV)
      call RESIZE_MXSNV_ROVBAS(M_SNV)
      MXSNV = M_SNV
!
      return
      end subroutine RESIZE_MXSNV

      end module mod_main_rovbas
