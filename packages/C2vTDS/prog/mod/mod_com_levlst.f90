
      module mod_com_levlst

      use mod_dppr
      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      real(kind=dppr)      ,pointer ,dimension(:) ,save  :: PCENT                                  ! (MXSNV)

      integer              ,pointer ,dimension(:) ,save  :: ICENT,KCENT                            ! (MXSNV)
      integer              ,pointer ,dimension(:) ,save  :: JS,NSUP                                ! (MXOBS)

      character(len =   1) ,pointer ,dimension(:) ,save  :: FAS                                    ! (MXOBS)
      character(len =   2) ,pointer ,dimension(:) ,save  :: SYSUP                                  ! (MXOBS)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_LEVLST
      IMPLICIT NONE

      integer  :: ierr

      allocate(PCENT(MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_LEVLST_PCENT')
      PCENT = 0.d0
      allocate(ICENT(MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_LEVLST_ICENT')
      ICENT = 0
      allocate(KCENT(MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_LEVLST_KCENT')
      KCENT = 0
      allocate(JS(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_LEVLST_JS')
      JS = 0
      allocate(NSUP(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_LEVLST_NSUP')
      NSUP = 0
      allocate(FAS(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_LEVLST_FAS')
      FAS = ''
      allocate(SYSUP(MXOBS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_LEVLST_SYSUP')
      SYSUP = ''
!
      return
      end subroutine ALLOC_LEVLST

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOBS

      subroutine RESIZE_MXOBS_LEVLST(C_OBS)
      IMPLICIT NONE
      integer :: C_OBS

      integer :: ierr

      character(len =   1) ,pointer ,dimension(:)  :: cpd1_1
      character(len =   2) ,pointer ,dimension(:)  :: cpd1_2

! JS
      allocate(ipd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_LEVLST_JS')
      ipd1 = 0
      ipd1(1:MXOBS) = JS(:)
      deallocate(JS)
      JS => ipd1
! NSUP
      allocate(ipd1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_LEVLST_NSUP')
      ipd1 = 0
      ipd1(1:MXOBS) = NSUP(:)
      deallocate(NSUP)
      NSUP => ipd1
! FAS
      allocate(cpd1_1(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_LEVLST_FAS')
      cpd1_1 = ''
      cpd1_1(1:MXOBS) = FAS(:)
      deallocate(FAS)
      FAS => cpd1_1
! SYSUP
      allocate(cpd1_2(C_OBS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOBS_LEVLST_SYSUP')
      cpd1_2 = ''
      cpd1_2(1:MXOBS) = SYSUP(:)
      deallocate(SYSUP)
      SYSUP => cpd1_2
!
      return
      end subroutine RESIZE_MXOBS_LEVLST

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXSNV

      subroutine RESIZE_MXSNV_LEVLST(C_SNV)
      IMPLICIT NONE
      integer :: C_SNV

      integer :: ierr

! PCENT
      allocate(rpd1(C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_LEVLST_PCENT')
      rpd1 = 0.d0
      rpd1(1:MXSNV) = PCENT(:)
      deallocate(PCENT)
      PCENT => rpd1
! ICENT
      allocate(ipd1(C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_LEVLST_ICENT')
      ipd1 = 0
      ipd1(1:MXSNV) = ICENT(:)
      deallocate(ICENT)
      ICENT => ipd1
! KCENT
      allocate(ipd1(C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_LEVLST_KCENT')
      ipd1 = 0
      ipd1(1:MXSNV) = KCENT(:)
      deallocate(KCENT)
      KCENT => ipd1
!
      return
      end subroutine RESIZE_MXSNV_LEVLST

      end module mod_com_levlst
