
      module mod_com_dipmod

      use mod_dppr
      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      integer ,pointer ,dimension(:) ,save  :: ICODTO                                              ! (MXOPT)
      integer ,pointer ,dimension(:) ,save  :: ICOTOT                                              ! (MXOPR)
      integer ,pointer ,dimension(:) ,save  :: ICTO                                                ! (MXOCV)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_DIPMOD
      IMPLICIT NONE

      integer  :: ierr

      allocate(ICODTO(MXOPT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DIPMOD_ICODTO')
      ICODTO = 0
      allocate(ICOTOT(MXOPR),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DIPMOD_ICOTOT')
      ICOTOT = 0
      allocate(ICTO(MXOCV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DIPMOD_ICTO')
      ICTO = 0
!
      return
      end subroutine ALLOC_DIPMOD

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOCV

      subroutine RESIZE_MXOCV_DIPMOD(C_OCV)
      IMPLICIT NONE
      integer :: C_OCV

      integer :: ierr

! ICTO
      allocate(ipd1(C_OCV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOCV_DIPMOD_ICTO')
      ipd1 = 0
      ipd1(1:MXOCV) = ICTO(:)
      deallocate(ICTO)
      ICTO => ipd1
!
      return
      end subroutine RESIZE_MXOCV_DIPMOD

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOPR

      subroutine RESIZE_MXOPR_DIPMOD(C_OPR)
      IMPLICIT NONE
      integer :: C_OPR

      integer :: ierr

! ICOTOT
      allocate(ipd1(C_OPR),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPR_DIPMOD_ICOTOT')
      ipd1 = 0
      ipd1(1:MXOPR) = ICOTOT(:)
      deallocate(ICOTOT)
      ICOTOT => ipd1
!
      return
      end subroutine RESIZE_MXOPR_DIPMOD

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOPT

      subroutine RESIZE_MXOPT_DIPMOD(C_OPT)
      IMPLICIT NONE
      integer :: C_OPT

      integer :: ierr

! ICODTO
      allocate(ipd1(C_OPT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPT_DIPMOD_ICODTO')
      ipd1 = 0
      ipd1(1:MXOPT) = ICODTO(:)
      deallocate(ICODTO)
      ICODTO => ipd1
!
      return
      end subroutine RESIZE_MXOPT_DIPMOD

      end module mod_com_dipmod
