
      module mod_com_eqint

      use mod_dppr
      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      real(kind=dppr) ,pointer ,dimension(:,:) ,save  :: AS,VACO                                   ! (MXOPT,MXOPT)
      real(kind=dppr) ,pointer ,dimension(:)   ,save  :: DERS,DERT,YS                              ! (MXOPT)
      real(kind=dppr) ,pointer ,dimension(:)   ,save  :: DERBID                                    ! (MXOPH)
      real(kind=dppr) ,pointer ,dimension(:)   ,save  :: ENINF                                     ! (MXENI)

      integer         ,pointer ,dimension(:)   ,save  :: ICENT,IPCSUP,NUSVSU                       ! (MXSNV)
      integer         ,pointer ,dimension(:)   ,save  :: ISFIT,IXCL                                ! (MXOPT)
      integer         ,pointer ,dimension(:)   ,save  :: JC,JCENTI                                 ! (MXENI)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_EQINT
      IMPLICIT NONE

      integer  :: ierr

      allocate(AS(MXOPT,MXOPT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_EQINT_AS')
      AS = 0.d0
      allocate(VACO(MXOPT,MXOPT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_EQINT_VACO')
      VACO = 0.d0
      allocate(DERS(MXOPT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_EQINT_DERS')
      DERS = 0.d0
      allocate(DERT(MXOPT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_EQINT_DERT')
      DERT = 0.d0
      allocate(YS(MXOPT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_EQINT_YS')
      YS = 0.d0
      allocate(DERBID(MXOPH),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_EQINT_DERBID')
      DERBID = 0.d0
      allocate(ENINF(MXENI),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_EQINT_ENINF')
      ENINF = 0.d0

      allocate(ICENT(MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_EQINT_ICENT')
      ICENT = 0
      allocate(IPCSUP(MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_EQINT_IPCSUP')
      IPCSUP = 0
      allocate(NUSVSU(MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_EQINT_NUSVSU')
      NUSVSU = 0
      allocate(ISFIT(MXOPT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_EQINT_ISFIT')
      ISFIT = 0
      allocate(IXCL(MXOPT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_EQINT_IXCL')
      IXCL = 0
      allocate(JC(MXENI),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_EQINT_JC')
      JC = 0
      allocate(JCENTI(MXENI),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_EQINT_JCENTI')
      JCENTI = 0
!
      return
      end subroutine ALLOC_EQINT

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXENI

      subroutine RESIZE_MXENI_EQINT(C_ENI)
      IMPLICIT NONE
      integer :: C_ENI

      integer :: ierr

! ENINF
      allocate(rpd1(C_ENI),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXENI_EQINT_ENINF')
      rpd1 = 0.d0
      rpd1(1:MXENI) = ENINF(:)
      deallocate(ENINF)
      ENINF => rpd1
! JC
      allocate(ipd1(C_ENI),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXENI_EQINT_JC')
      ipd1 = 0
      ipd1(1:MXENI) = JC(:)
      deallocate(JC)
      JC => ipd1
! JCENTI
      allocate(ipd1(C_ENI),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXENI_EQINT_JCENTI')
      ipd1 = 0
      ipd1(1:MXENI) = JCENTI(:)
      deallocate(JCENTI)
      JCENTI => ipd1
!
      return
      end subroutine RESIZE_MXENI_EQINT

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOPH

      subroutine RESIZE_MXOPH_EQINT(C_OPH)
      IMPLICIT NONE
      integer :: C_OPH

      integer :: ierr

! DERBID
      allocate(rpd1(C_OPH),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPH_EQINT_DERBID')
      rpd1 = 0.d0
      rpd1(1:MXOPH) = DERBID(:)
      deallocate(DERBID)
      DERBID => rpd1
!
      return
      end subroutine RESIZE_MXOPH_EQINT

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOPT

      subroutine RESIZE_MXOPT_EQINT(C_OPT)
      IMPLICIT NONE
      integer :: C_OPT

      integer :: i,ierr

! AS
      allocate(rpd2(C_OPT,C_OPT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPT_EQINT_AS')
      rpd2 = 0.d0
      do i=1,MXOPT
        rpd2(1:MXOPT,i) = AS(:,i)
      enddo
      deallocate(AS)
      AS => rpd2
! VACO
      allocate(rpd2(C_OPT,C_OPT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPT_EQINT_VACO')
      rpd2 = 0.d0
      do i=1,MXOPT
        rpd2(1:MXOPT,i) = VACO(:,i)
      enddo
      deallocate(VACO)
      VACO => rpd2
! DERS
      allocate(rpd1(C_OPT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPT_EQINT_DERS')
      rpd1 = 0.d0
      rpd1(1:MXOPT) = DERS(:)
      deallocate(DERS)
      DERS => rpd1
! DERT
      allocate(rpd1(C_OPT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPT_EQINT_DERT')
      rpd1 = 0.d0
      rpd1(1:MXOPT) = DERT(:)
      deallocate(DERT)
      DERT => rpd1
! YS
      allocate(rpd1(C_OPT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPT_EQINT_YS')
      rpd1 = 0.d0
      rpd1(1:MXOPT) = YS(:)
      deallocate(YS)
      YS => rpd1
! ISFIT
      allocate(ipd1(C_OPT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPT_EQINT_ISFIT')
      ipd1 = 0
      ipd1(1:MXOPT) = ISFIT(:)
      deallocate(ISFIT)
      ISFIT => ipd1
! IXCL
      allocate(ipd1(C_OPT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPT_EQINT_IXCL')
      ipd1 = 0
      ipd1(1:MXOPT) = IXCL(:)
      deallocate(IXCL)
      IXCL => ipd1
!
      return
      end subroutine RESIZE_MXOPT_EQINT

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXSNV

      subroutine RESIZE_MXSNV_EQINT(C_SNV)
      IMPLICIT NONE
      integer :: C_SNV

      integer :: ierr

! ICENT
      allocate(ipd1(C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_EQINT_ICENT')
      ipd1 = 0
      ipd1(1:MXSNV) = ICENT(:)
      deallocate(ICENT)
      ICENT => ipd1
! IPCSUP
      allocate(ipd1(C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_EQINT_IPCSUP')
      ipd1 = 0
      ipd1(1:MXSNV) = IPCSUP(:)
      deallocate(IPCSUP)
      IPCSUP => ipd1
! NUSVSU
      allocate(ipd1(C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_EQINT_NUSVSU')
      ipd1 = 0
      ipd1(1:MXSNV) = NUSVSU(:)
      deallocate(NUSVSU)
      NUSVSU => ipd1
!
      return
      end subroutine RESIZE_MXSNV_EQINT

      end module mod_com_eqint
