
      module mod_com_tri

      use mod_dppr
      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      integer ,parameter  :: NBCMAX = 5
      integer ,parameter  :: NRE    = NBCMAX+1

      integer              ,dimension(NRE)    ,save  :: PCOL   = 0
      integer              ,dimension(NRE)    ,save  :: DCOL   = 0
      integer              ,dimension(NRE)    ,save  :: NBCOL  = 0
      integer                                 ,save  :: NBCHPS = 0
      integer                                 ,save  :: NBLGN  = 0

      character(len =   1) ,dimension(NBCMAX) ,save  :: TYPE   = ''
      character(len =   1) ,dimension(NBCMAX) ,save  :: VAR    = ''
      character(len =  20) ,dimension(NBCMAX) ,save  :: FOR    = ''
      character(len = 152) ,dimension(NBCMAX) ,save  :: TEST   = ''
      character(len = 152)                    ,save  :: LIGNE  = ''
!
      real(kind=dppr)      ,pointer ,dimension(:,:) ,save  :: DVAR                                 ! (NBCMAX,NBLMAX)

      integer              ,pointer ,dimension(:)   ,save  :: IND                                  ! (NBLMAX)

      character(len = 152) ,pointer ,dimension(:)   ,save  :: CHAINE                               ! (NBLMAX+1)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_TRI
      IMPLICIT NONE

      integer  :: ierr

      allocate(DVAR(NBCMAX,NBLMAX),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_TRI_DVAR')
      DVAR = 0.d0
      allocate(IND(NBLMAX),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_TRI_IND')
      IND = 0
      allocate(CHAINE(NBLMAX+1),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_TRI_CHAINE')
      CHAINE = ''
!
      return
      end subroutine ALLOC_TRI

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! NBLMAX

      subroutine RESIZE_NBLMAX_TRI(C_LMAX)
      IMPLICIT NONE
      integer :: C_LMAX

      integer :: i,ierr

      character(len = 152) ,pointer ,dimension(:)  :: cpd1_152

! DVAR
      allocate(rpd2(NBCMAX,C_LMAX),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_NBLMAX_TRI_DVAR')
      rpd2 = 0.d0
      do i=1,NBCMAX
        rpd2(i,1:NBLMAX) = DVAR(i,:)
      enddo
      deallocate(DVAR)
      DVAR => rpd2
! IND
      allocate(ipd1(C_LMAX),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_NBLMAX_TRI_IND')
      ipd1 = 0
      ipd1(1:NBLMAX) = IND(:)
      deallocate(IND)
      IND => ipd1
! CHAINE
      allocate(cpd1_152(C_LMAX+1),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_NBLMAX_TRI_CHAINE')
      cpd1_152 = ''
      cpd1_152(1:NBLMAX+1) = CHAINE(:)
      deallocate(CHAINE)
      CHAINE => cpd1_152
!
      return
      end subroutine RESIZE_NBLMAX_TRI

      end module mod_com_tri
