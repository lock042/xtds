
      module mod_par_tds

      use mod_dppr
      IMPLICIT NONE

!
!
!  This file provides various limitating parameters concerning the different programs in the package.
!  A short description as well as involved programs are given for each parameter.
!  Note: some program specific parameters are still defined in the related program source.
!
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
! Program stamped with (*) provides maximum value of the concerned parameter
! written in the 'debug.t' file.
!
!
! MXBRA  = Maximum number of branches.
!          In           -> mod_com_branch
!                          mod_com_trm
!                          eq_int
!                          eq_tds
!                          spech
!                          spect
!                          xwf
!                          xwfa
!                          xws
!
! MXDIMI = JC block maximum dimension for lower polyad.
!          Dependancies -> polyad and Jmax.
!          In           -> mod_com_dipmat
!                          mod_com_dipomat
!                          mod_com_trm
!                          mod_main_dipmat
!                          mod_main_trm
!                          mod_main_trmomt
!                          mod_main_xpafit
!                          dipmat (*)
!                          trm
!                          trmomt
!                          xtrm
!                          xtro
!
! MXDIMS = JC block maximum dimension for upper polyad.
!          Dependancies -> polyad and Jmax.
!          In           -> mod_com_dipomat
!                          mod_com_hdic
!                          mod_com_hmatri
!                          mod_com_pgdh
!                          mod_com_rovbas
!                          mod_com_trm
!                          mod_com_xhdic
!                          mod_main_dipmat
!                          mod_main_hdi
!                          mod_main_hdiag
!                          mod_main_hmatri
!                          mod_main_rovbas
!                          mod_main_trm
!                          mod_main_trmomt
!                          mod_main_xpafit
!                          asfn
!                          asha
!                          asvp
!                          asxvp
!                          diago
!                          dipmat (*)
!                          dmspr
!                          hdi
!                          hdiag
!                          hmatri (*)
!                          rovbas
!                          trm
!                          trmomt
!                          xhdi
!                          xhdiag
!                          xtrm
!                          xtro
!
! MXDK   = Maximum number of J-diagonal K (for a given J).
!          Dependancies -> Jmax and development order.
!          In           -> mod_com_ckdipo
!                          mod_main_dipmat
!                          calkd  (*)
!
! MXELMD = Maximum number of matrix elements for all H operators per JC block.
!          Dependancies -> polyad, development order and Jmax.
!          In           -> mod_com_pgdh
!                          mod_main_hdi
!                          mod_main_hdiag
!                          mod_main_xpafit
!                          hdi    (*)
!                          hdiag  (*)
!                          xhdi   (*)
!                          xhdiag (*)
!
! MXELMH = Maximum number of matrix elements for one H operator per JC block.
!          Dependancies -> polyad, development order and Jmax.
!          In           -> mod_com_hmatri
!                          mod_main_hmatri
!                          asha
!                          hmatri (*)
!
! MXELMT = Maximum number of matrix elements for one transition moment operator
!                          for a given upper JC block.
!          Dependancies -> polyad, development order and Jmax.
!          In           -> mod_com_dipomat
!                          mod_com_trm
!                          mod_main_dipmat
!                          mod_main_trm
!                          mod_main_trmomt
!                          mod_main_xpafit
!                          asdi
!                          caldi
!                          dipmat (*)
!                          trm
!                          trmomt
!                          xtrm
!                          xtro
!
! MXEMR  = Maximum number of vibrational reduced matrix elements.
!          Dependancies -> polyad, development order.
!          In           -> mod_com_dipmod
!                          mod_com_dipomat
!                          mod_com_hmatri
!                          mod_com_hmodel
!                          mod_com_model
!                          mod_main_dipmat
!                          mod_main_dipmod
!                          mod_main_hmatri
!                          mod_main_hmodel
!                          dipmat
!                          dipmod (*)
!                          hmatri
!                          hmodel (*)
!
! MXENI  = Maximum number of lower polyad energies.
!          Dependancies -> lower polyad and Jmax.
!          In           -> mod_com_eqint
!                          mod_com_tra
!                          mod_com_xwfa
!                          mod_com_xws
!                          mod_main_eqint
!                          mod_main_tra
!                          mod_main_xpafit
!                          eq_int
!                          tra    (*)
!                          xwfa   (*)
!                          xws    (*)
!
! MXG    = Maximum number of G coefficients.
!          Dependancies -> Jmax. Linked to MXJG by: MXG=IN(MXJG+2)-1 read in nbg199
!          In           -> mod_com_lg
!
! MXJ    = Jmax.
!          Dependancies -> MXJ should be < or = to MXJG-2 for Raman,
!                                               to MXJG-1 for infrared.
!          In           -> mod_com_ckdi
!                          mod_com_statf
!                          mod_com_stats
!                          dipmat
!                          eq_int
!                          hdi
!                          hdiag
!                          hmatri
!                          xpafit
!                          xws
!
! MXJG   = Maximum J for available G coefficients.
!          In           -> mod_com_lg
!
! MXK    = Maximum number of diagonal K (Hamiltonian) for a given J.
!          Dependancies -> Jmax and development order.
!          In           -> mod_com_hmatri
!                          mod_main_hmatri
!                          hmatri (*)
!
! MXKCO  = Maximum number of columns in the K table.
!          In           -> mod_com_ckmo
!                          mod_main_dipmod
!                          mod_main_hmodel
!                          dipmod (*)
!                          hmodel (*)
!
! MXKLI  = Maximum number of rows in the K table.
!          In           -> mod_com_ckmo
!                          mod_main_dipmod
!                          mod_main_hmodel
!                          dipmod (*)
!                          hmodel (*)
!
! MXNCR  = Maximum number of rotational codes for a JC block.
!          Dependancies -> polyad and Jmax.
!          In           -> mod_com_rovbas
!                          mod_main_rovbas
!                          rovbas (*)
!
! MXNIV  = Maximum number of vibrational levels for a given polyad.
!          In           -> mod_com_dipomod
!                          mod_com_eqtds
!                          mod_com_hmodel
!                          mod_com_statf
!                          mod_com_xwf
!                          mod_main_dipmod
!                          mod_main_eqtds
!                          mod_main_hmodel
!                          mod_main_xpafit
!                          dipmod (*)
!                          eq_tds (*)
!                          hmodel (*)
!                          xwf    (*)
!
! MXOBS  = Maximum number of observed lines.
!          In           -> mod_com_cas
!                          mod_com_der
!                          mod_com_en
!                          mod_com_eqtds
!                          mod_com_levlst
!                          mod_com_precf
!                          mod_com_predlx
!                          mod_com_xassi
!                          mod_com_xwf
!                          mod_main_eqtds
!                          mod_main_levlst
!                          mod_main_precf
!                          mod_main_predlx
!                          mod_main_xctrmk
!                          mod_main_xpafit
!                          mod_par_x             -> MXOBZ,LWA
!                          eq_tds (*)
!                          levlst
!                          precf
!                          predlx
!                          xpafit (*)
!
! MXOCV  = Maximum number of rovibrational operators for a given vibrational block.
!          Dependancies -> polyad and development order.
!          In           -> mod_com_dipmod
!                          mod_com_model
!                          mod_main_dipmod
!                          mod_main_hmodel
!                          dipmod (*)
!                          hmodel (*)
!
! MXODK  = Maxmum number of off-diagonal K.
!          Dependancies -> Jmax and development order.
!          In           -> mod_com_ckdi
!                          mod_main_dipmat
!                          calkd  (*)
!
! MXOPH  = Maximum number of operators in H.
!          Dependancies -> polyad and development order.
!          In           -> mod_com_der
!                          mod_com_derv
!                          mod_com_eqfc
!                          mod_com_eqint
!                          mod_com_eqtds
!                          mod_com_hdic
!                          mod_com_hmatri
!                          mod_com_hmodel
!                          mod_com_paradj
!                          mod_com_pgde
!                          mod_com_pgdh
!                          mod_com_precf
!                          mod_com_xpara
!                          mod_com_xwfa
!                          mod_main_eqint
!                          mod_main_eqtds
!                          mod_main_hdi
!                          mod_main_hdiag
!                          mod_main_hmatri
!                          mod_main_hmodel
!                          mod_main_paradj
!                          mod_main_precf
!                          mod_main_xctrmk
!                          mod_main_xpafit
!                          mod_par_x             -> MXOP,MXOBZ,LWA
!                          ased
!                          asnorm
!                          asparv
!                          asxed
!                          eq_int
!                          eq_tds
!                          hdi
!                          hdiag
!                          hmatri
!                          hmodel (*)
!                          paradj
!                          precf
!                          xctrmk
!                          xhdi
!                          xhdiag
!                          xpafit
!
! MXOPR  = Maximum number of rovibrational operators for each vibrational operator.
!          Dependancies -> polyad and development order.
!          In           -> mod_com_dipmod
!                          mod_com_model
!                          mod_main_dipmod
!                          mod_main_hmodel
!                          dipmod (*)
!                          hmodel (*)
!
! MXOPT  = Maximum number of transition operators.
!          Dependancies -> polyad and development order.
!          In           -> mod_com_dipmod
!                          mod_com_dipomat
!                          mod_com_dipomod
!                          mod_com_eqint
!                          mod_com_pa
!                          mod_com_trm
!                          mod_com_trmomt
!                          mod_com_xpara
!                          mod_com_xws
!                          mod_main_dipmat
!                          mod_main_dipmod
!                          mod_main_eqint
!                          mod_main_trm
!                          mod_main_trmomt
!                          mod_main_xctrmk
!                          mod_main_xpafit
!                          mod_par_x             -> MXOP,MXOBZ,LWA
!                          astatd
!                          asxtro
!                          dipmat
!                          dipmod (*)
!                          eq_int
!                          trm
!                          trmomt
!                          xctrmk
!                          xpafit
!                          xtrm
!                          xtro
!
! MXOPVH = Maximum number of vibrational operators (Hamiltonian).
!          Dependancies -> polyad and development order.
!          In           -> mod_com_hmatri
!                          mod_main_hmatri
!                          hmatri (*)
!                          hmodel (*)
!
! MXOPVT = Maximum number of vibrational operators (transition moment).
!          Dependancies -> polyad and development order.
!          In           -> mod_com_dipomat
!                          mod_main_dipmat
!                          dipmat (*)
!                          dipmod (*)
!
! MXSNB  = Maximum number of vibrational sublevels per band.
!          Dependancies -> polyad.
!          In           -> mod_com_sigvi
!                          mod_main_dipmod
!                          mod_main_hmodel
!                          dipmod (*)
!                          hmodel (*)
!
! MXSNV  = Maximum number of vibrational sublevels.
!          Dependancies -> polyad.
!          In           -> mod_com_dipomod
!                          mod_com_eqint
!                          mod_com_eqtds
!                          mod_com_hdic
!                          mod_com_hmodel
!                          mod_com_jener
!                          mod_com_levlst
!                          mod_com_matri
!                          mod_com_rovbas
!                          mod_com_spech
!                          mod_com_stats
!                          mod_com_tra
!                          mod_com_xhdic
!                          mod_com_xwf
!                          mod_com_xwfa
!                          mod_com_xws
!                          mod_main_dipmat
!                          mod_main_dipmod
!                          mod_main_eqint
!                          mod_main_eqtds
!                          mod_main_hdi
!                          mod_main_hdiag
!                          mod_main_hmatri
!                          mod_main_hmodel
!                          mod_main_jener
!                          mod_main_levlst
!                          mod_main_rovbas
!                          mod_main_spech
!                          mod_main_tra
!                          mod_main_xpafit
!                          ased
!                          asen
!                          asxed
!                          dipmat (*)
!                          dipmod (*)
!                          eq_int
!                          eq_tds
!                          hdi
!                          hdiag
!                          hmatri (*)
!                          hmodel (*)
!                          jener
!                          levlst
!                          rovbas
!                          spech
!                          tra
!                          xhdi
!                          xhdiag
!                          xws
!
! MXSYM  = Maximum number of symmetries.
!          In           -> mod_com_dipmat        -> MDMJCI
!                          mod_com_spin
!                          mod_com_sy
!                          a34
!                          akprim
!                          asdi
!                          asfn
!                          asha
!                          asme
!                          caldi
!                          dipmat
!                          dipmod
!                          dzcc2v
!                          eq_int
!                          eq_tds
!                          hdi
!                          hdiag
!                          hmatri
!                          hmodel
!                          mula2
!                          parchk
!                          rovbas
!                          tra
!                          trcc2v
!                          trm
!                          trmomt
!                          xpafit
!                          xhdi
!                          xhdiag
!                          xtrm
!                          xtro
!
! MXSYR  = Maximum number of symmetries in intermediate group.
!          In           -> mod_com_sy
!                          mod_com_trm           -> MDMJCI
!                          a32
!                          a33
!                          cal6c
!                          caldi
!                          calkd
!                          dipmod
!                          dzc
!                          eufc
!                          hmatri
!                          hmodel
!                          nsym1
!                          rovbas
!                          tcube
!                          trm
!                          trmomt
!                          xtrm
!                          xtro
!
! MXPOL  = Maximum number of polyads.
!          In           -> mod_com_dipomod
!                          mod_com_hmodel
!                          mod_com_xpoly
!                          dipmod
!                          hmodel
!                          xasg0
!                          xctrmk
!                          xpafit
!
! MXNTR  = Maximum number of P*mP* transitions
!          MXNTR(MXPOL) = MXPOL+MXNTR(MXPOL-1)
!          55 for MXPOL=10
!          In           -> xwpaes
!
! MXATR  = Allowed number of P*mP* transitions ( MXATR <= MXNTR )
!          In           -> mod_com_xctrmk
!                          mod_com_xpafit
!                          mod_com_xpara
!                          mod_com_xtran
!                          mod_com_xws
!                          mod_main_xctrmk
!                          mod_main_xpafit
!                          mod_par_x             -> MXTRAD
!                          xctrmk
!                          xpafit (*)
!                          xwpaes
!                          xwpart
!
! MXNTRT = Maximum number of P*mP* transition types (DELTA+ISRAM)
!
! MXATRT = Allowed number of P*mP* transition types ( MXATRT <= MXNTRT )
!                                                   ( MXATRT <= MXATR  )
!          In           -> mod_com_xpara
!                          mod_com_xtran
!                          mod_main_xctrmk
!                          mod_main_xpafit
!                          mod_par_x             -> MXOP,MXOBZ,LWA
!                          xctrmk
!                          xpafit (*)
!                          xwpafi
!
! MXGAM  = Maximum rotational degree.
!
! MXDROT = MXGAM+1
!          Dependancies -> development order.
!          In           -> dipmod
!                          hmodel
!
! MXFAC  = size of the factorial table.
!          2*(MXJG+1)+(MXGAM+1)+1     (for instance: 2*200     +10       +1 = 411)
!          In           -> mod_com_fa
!                          facto
!
! NBAM   = number of angular momenta
!          In           -> ctrpmk
!                          nulpar
!                          paradj
!                          parchk
!                          parmk
!                          xpafit
!                          xwpaes
!                          xwpafi
!                          xwpart
!
! NBCLAB = number of characters in the label defining a parameter.
!          In           -> mod_com_paradj
!                          mod_com_xpara
!                          ased
!                          asen
!                          asnorm
!                          astatd
!                          astptr
!                          astran
!                          asvp
!                          ctrpmk
!                          dipmat
!                          eq_int
!                          eq_tds
!                          hdi
!                          hdiag
!                          hmatri
!                          jener
!                          levlst
!                          nulpar
!                          paradj
!                          parchk
!                          parmk
!                          precf
!                          spect
!                          tra
!                          trm
!                          trmomt
!                          xctrmk
!                          xpafit
!                          xwf
!                          xwpaes
!                          xwpafi
!                          xwpart
!                          xws
!                          XTDS
!
! NBCTIT = number of characters of the TITRE string (parameter file header)
!          In           -> mod_com_paradj
!                          asdi
!                          ased
!                          asen
!                          asfn
!                          asha
!                          asnorm
!                          asparv
!                          astatd
!                          astptr
!                          astran
!                          asvp
!                          asxed
!                          ctrpmk
!                          dipmat
!                          eq_int
!                          eq_tds
!                          hdi
!                          hdiag
!                          hmatri
!                          jener
!                          levlst
!                          nulpar
!                          paradj
!                          parchk
!                          parmk
!                          precf
!                          rovbas
!                          spech
!                          spect
!                          tra
!                          trm
!                          trmomt
!                          xctrmk
!                          xpafit
!                          xhdiag
!                          xwf
!                          xwfa
!                          xwpaes
!                          xwpafi
!                          xws
!
! NBVQN  = number of vibrationnal quantum numbers.
!          In           -> mod_com_dipomod
!                          mod_com_fp
!                          mod_com_hmodel
!                          mod_com_sigvi
!                          mod_com_spech
!                          mod_com_xwf
!                          dipmod
!                          e12
!                          e123
!                          e1234
!                          eq_tds
!                          hmodel
!                          levlst
!                          parchk
!                          parmk
!                          spech
!                          xwf
!                          XTDS
!
! MDMIGA = Maximum number of irreps in a product.
!          In           -> dipmod
!                          hmodel
!                          multd
!                          rovbas
!
! NB6C   = number of 6C.
!          In           -> mod_com_com6c
!                          cal6c  (*)
!                          sxc
!
! MXNBES = Maximum number of spectral elements.
! MXNLF  = Maximum number of line files.
! MXNPV  = Maximum number of points in half Voigt profile (including center, should respect MXNPV = 101+10*I).
! MXNMI  = Maximum number of points in half apparatus function profile (2*MXNMI+1).
! MXNTRA = Maximum number of implied transitions for one spectrum point.
!          In           -> mod_com_simul
!                          mod_main_simul
!                          simul
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! defined in mod_par_x
!
! MXOP   = MXOPH+MXOPT*MXATRT
!          In           -> mod_com_xpafit
!                          mod_com_xpara
!                          mod_main_xctrmk
!                          mod_main_xpafit
!                          mod_par_x             -> MXOBZ,LWA
!                          xpafit
!                          xpfitc
!                          xwpaes
!
! MXOBZ  = 2*MXOBS+MXOP
!          In           -> mod_com_xfjac
!                          mod_com_xfonc
!                          mod_com_xpafit
!                          mod_main_xctrmk
!                          mod_main_xpafit
!                          mod_par_x             -> LWA
!                          rmsh
!                          rmst
!                          xpafit
!                          xpfitc
!                          xwpaes
!                          xwpart
!
! LWA    = 5*MXOP+MXOBZ
!          In           -> mod_com_xpafit
!                          mod_main_xctrmk
!                          mod_main_xpafit
!                          mod_par_x
!                          lmder1
!                          xpafit
!
! MXTRAD = 2*MXATR
!          In           -> mod_com_xtran
!                          mod_main_xctrmk
!                          mod_main_xpafit
!                          mod_par_x
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! defined in mod_com_(x)tri
!
! NBLMAX
!          In           -> mod_com_tri
!                          mod_com_xtri
!                          mod_main_tri
!                          mod_main_xtri
!                          tri
!                          triasg
!                          xtrias
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! MDMJCI
!          In           -> mod_com_dipmat        ->  3*MXSYM
!                          mod_com_trm           ->  5*MXSYR
!                          dipmat
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
!
      integer ,parameter  :: MXBRA  = 5                                                            !  do NOT change
      integer ,save       :: MXDIMI = 1                                                            !  136            150
      integer ,save       :: MXDIMS = 1                                                            !  136            150
      integer ,save       :: MXDK   = 1                                                            !  114886         127000
      integer ,save       :: MXELMD = 1                                                            !  249329         275000
      integer ,save       :: MXELMH = 1                                                            !  2599           2860
      integer ,save       :: MXELMT = 1                                                            !  6120           6740
      integer ,save       :: MXEMR  = 1                                                            !  3              4
      integer ,save       :: MXENI  = 1                                                            !  24843          27400
      integer ,parameter  :: MXG    = 1333500                                                      !  do NOT change
      integer ,parameter  :: MXJ    = 198                                                          !  do NOT change
      integer ,parameter  :: MXJG   = MXJ+1                                                        !  do NOT change
      integer ,save       :: MXK    = 1                                                            !  43765          48200
      integer ,save       :: MXKCO  = 3                     !!!! pas < 3 !!!!                      !  13             14
      integer ,save       :: MXKLI  = 1                                                            !  12             13
      integer ,save       :: MXNCR  = 1                                                            !  51             56
      integer ,save       :: MXNIV  = 1                                                            !  1              2
      integer ,save       :: MXOBS  = 1                                                            !  1070           1180
      integer ,save       :: MXOCV  = 1                                                            !  120            132
      integer ,save       :: MXODK  = 1                                                            !  174301         192000
      integer ,save       :: MXOPH  = 1                                                            !  139            153
      integer ,save       :: MXOPR  = 1                                                            !  20             22
      integer ,save       :: MXOPT  = 1                                                            !  3              4
      integer ,save       :: MXOPVH = 1                                                            !  10             11
      integer ,save       :: MXOPVT = 1                                                            !  3              4
      integer ,save       :: MXSNB  = 1                                                            !  1              2
      integer ,save       :: MXSNV  = 1                                                            !  3              4
      integer ,parameter  :: MXSYM  = 4                                                            !  do NOT change
      integer ,parameter  :: MXSYR  = 5                                                            !  do NOT change
      integer ,parameter  :: MXPOL  = 10                                                           !  do NOT change
      integer ,save       :: MXATR  = 1                                                            !                     5
      integer ,parameter  :: MXNTRT = 2*MXPOL                                                      !  do NOT change
      integer ,save       :: MXATRT = 1                                                            !                     5
      integer ,parameter  :: MXGAM  = 9                                                            !  do NOT change
      integer ,parameter  :: MXDROT = MXGAM+1                                                      !  do NOT change
      integer ,parameter  :: MXFAC  = 2*(MXJG+1)+(MXGAM+1)+1                                       !  do NOT change
      integer ,parameter  :: NBAM   = 1                                                            !  do NOT change
      integer ,parameter  :: NBCLAB = 37                                                           !  do NOT change
      integer ,parameter  :: NBCTIT = 112                                                          !  do NOT change
      integer ,parameter  :: NBVQN  = 4                                                            !  do NOT change
      integer ,parameter  :: MDMIGA = 4                                                            !  do NOT change
      integer ,parameter  :: NB6C   = 681                                                          !  do NOT change
      integer ,save       :: MXNBES = 1                                                            !                     2000000
      integer ,save       :: MXNLF  = 1                                                            !                     10
      integer ,save       :: MXNPV  = 111                                                          !  MXNPV = 101+10*I   1001
      integer ,save       :: MXNMI  = 1                                                            !                     500
      integer ,save       :: MXNTRA = 1                                                            !                     1000000
      integer ,save       :: NBLMAX = 1                                                            !                     MXOBS


      end module mod_par_tds
