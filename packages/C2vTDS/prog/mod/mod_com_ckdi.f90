
      module mod_com_ckdi

      use mod_dppr
      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      integer ,dimension(0:MXJ+2) ,save  :: INJDK  = 0
      integer ,dimension(MXJ+2)   ,save  :: INJODK = 0
!
      real(kind=dppr) ,pointer ,dimension(:)       ,save  :: VODK                                  ! (MXODK)

      integer         ,pointer ,dimension(:)       ,save  :: IODKX                                 ! (MXODK)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_CKDI
      use mod_alloc
      IMPLICIT NONE

      integer  :: ierr

      allocate(VODK(MXODK),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_CKDI_VODK')
      VODK = 0.d0

      allocate(IODKX(MXODK),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_CKDI_IODKX')
      IODKX = 0
!
      return
      end subroutine ALLOC_CKDI

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXODK

      subroutine RESIZE_MXODK_CKDI(C_ODK)
      IMPLICIT NONE
      integer :: C_ODK

      integer :: ierr

! VODK
      allocate(rpd1(C_ODK),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXODK_CKDI_VODK')
      rpd1 = 0.d0
      rpd1(1:MXODK) = VODK(:)
      deallocate(VODK)
      VODK => rpd1
! IODKX
      allocate(ipd1(C_ODK),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXODK_CKDI_IODKX')
      ipd1 = 0
      ipd1(1:MXODK) = IODKX(:)
      deallocate(IODKX)
      IODKX => ipd1
!
      return
      end subroutine RESIZE_MXODK_CKDI

      end module mod_com_ckdi
