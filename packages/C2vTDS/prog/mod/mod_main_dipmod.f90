
      module mod_main_dipmod

      use mod_par_tds
      use mod_com_ckmo
      use mod_com_dipmod
      use mod_com_dipomod
      use mod_com_model
      use mod_com_sigvi

      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine alloc_all
      IMPLICIT NONE

      call alloc_ckmo
      call alloc_dipmod
      call alloc_dipomod
      call alloc_model
      call alloc_sigvi
!
      return
      end subroutine alloc_all

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXKLI
      IMPLICIT NONE

      integer  :: M_KLI

      M_KLI = MXRES(MXKLI)
      call RESIZE_MXKLI_CKMO(M_KLI)
      MXKLI = M_KLI
!
      return
      end subroutine RESIZE_MXKLI

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXKCO
      IMPLICIT NONE

      integer  :: M_KCO

      M_KCO = MXRES(MXKCO)
      call RESIZE_MXKCO_CKMO(M_KCO)
      MXKCO = M_KCO
!
      return
      end subroutine RESIZE_MXKCO

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXOPR
      IMPLICIT NONE

      integer  :: M_OPR

      M_OPR = MXRES(MXOPR)
      call RESIZE_MXOPR_DIPMOD(M_OPR)
      call RESIZE_MXOPR_MODEL(M_OPR)
      MXOPR = M_OPR
!
      return
      end subroutine RESIZE_MXOPR

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXOCV
      IMPLICIT NONE

      integer  :: M_OCV

      M_OCV = MXRES(MXOCV)
      call RESIZE_MXOCV_DIPMOD(M_OCV)
      call RESIZE_MXOCV_MODEL(M_OCV)
      MXOCV = M_OCV
!
      return
      end subroutine RESIZE_MXOCV

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXOPT
      IMPLICIT NONE

      integer  :: M_OPT

      M_OPT = MXRES(MXOPT)
      call RESIZE_MXOPT_DIPMOD(M_OPT)
      call RESIZE_MXOPT_DIPOMOD(M_OPT)
      MXOPT = M_OPT
!
      return
      end subroutine RESIZE_MXOPT

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXNIV
      IMPLICIT NONE

      integer  :: M_NIV

      M_NIV = MXRES(MXNIV)
      call RESIZE_MXNIV_DIPOMOD(M_NIV)
      MXNIV = M_NIV
!
      return
      end subroutine RESIZE_MXNIV

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXSNV
      IMPLICIT NONE

      integer  :: M_SNV

      M_SNV = MXRES(MXSNV)
      call RESIZE_MXSNV_DIPOMOD(M_SNV)
      MXSNV = M_SNV
!
      return
      end subroutine RESIZE_MXSNV

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXSNB
      IMPLICIT NONE

      integer  :: M_SNB

      M_SNB = MXRES(MXSNB)
      call RESIZE_MXSNB_SIGVI(M_SNB)
      MXSNB = M_SNB
!
      return
      end subroutine RESIZE_MXSNB

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine RESIZE_MXEMR
      IMPLICIT NONE

      integer  :: M_EMR

      M_EMR = MXRES(MXEMR)
      call RESIZE_MXEMR_MODEL(M_EMR)
      MXEMR = M_EMR
!
      return
      end subroutine RESIZE_MXEMR

      end module mod_main_dipmod
