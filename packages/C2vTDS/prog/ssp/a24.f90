!
! VB CW NOV 2009
!
      FUNCTION A24(IPM,VP,LP,CP,K,GAMA,V,L,C)
      use mod_dppr
      use mod_com_sy
      IMPLICIT NONE
      real(kind=dppr)  :: A24
      integer          :: IPM,VP,LP,CP,K,GAMA,V,L,C

! functions
      real(kind=dppr)  :: A21,A22,A23,SXC
      integer          :: IPSI

      real(kind=dppr)  :: E

      integer          :: CS
      integer          :: GAMAI,GAMAJ
      integer          :: KI,KJ
      integer          :: LD,LS
      integer          :: VS
!
      A24 = 0.D0
      IF( IPSI(2,4,K,0,GAMA) .EQ. 0 ) RETURN
      IF( IPM*(VP-V)         .NE. 4 ) RETURN
      IF    ( K .EQ. 0 ) THEN
        KI    = 0
        KJ    = 0
        VS    = V+IPM*2                                                                            ! 2 = OMEGA
        GAMAI = 1
        GAMAJ = 1
      ELSEIF( K .EQ. 2 ) THEN
        KI    = 0
        KJ    = 2
        VS    = V+IPM*2                                                                            ! 2 = OMEGA
        GAMAI = 1
        GAMAJ = 3
      ELSE                                                                                         ! K = 4
        KI    = 3
        KJ    = 1
        VS    = V+IPM*1                                                                            ! 1 = OMEGA
        GAMAI = 1                                                                                  ! arbitrary choice
        GAMAJ = 3
      ENDIF
      DO LD=-KJ,KJ,2
        LS = L+LD
E1:     DO CS=1,3
          IF( IPSI(2,VS,LS,0,CS) .EQ. 0 ) CYCLE E1
          IF    ( K .EQ. 0 ) THEN
            E = A22(IPM,VP,LP,CP,GAMAI,VS,LS,CS)**2
          ELSEIF( K .EQ. 2 ) THEN
            E = A22(IPM,VP,LP,CP,GAMAI,VS,LS,CS)*  &
                A22(IPM,VS,LS,CS,GAMAJ,V,L,C)
          ELSE                                                                                     ! K = 4
            E = A23(IPM,VP,LP,CP,KI,GAMAI,VS,LS,CS)*  &
                A21(IPM,VS,LS,CS,V,L,C)
          ENDIF
          IF( E .EQ. 0.D0 ) CYCLE E1
          A24 = A24+E*PC(C)*PC(CP)*PC(GAMAI)*PC(GAMAJ)*SXC(GAMAI,GAMAJ,GAMA,C,CP,CS)
        ENDDO E1
      ENDDO
      A24 = A24*SQRT(DC(GAMA))
!
      RETURN
      END FUNCTION A24
