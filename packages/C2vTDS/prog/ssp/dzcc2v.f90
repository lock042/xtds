!
!                                 ( ICT1  , ICT2  , ICT12 , ICT512)
! *** CALCUL DES SYMBOLES 12CC2V  ( ICT3  , ICT4  , ICT34 , ICT513)
!                                 ( ICT13 , ICT24 , ICT5  , ICT   )
!
! *** METHODE ' ELLIOTT '
!     (M. ROTGER 3/99. D'APRES SPG DZCA2.F GABARD - 1 / 93 )
!
      FUNCTION DZCC2V(ICT1,ICT2,ICT12,ICT512,ICT3,ICT4,ICT34,ICT513,ICT13,ICT24,ICT5,ICT)
!
      use mod_dppr
      use mod_par_tds
      use mod_com_sy
      IMPLICIT NONE
      real(kind=dppr)  :: DZCC2V
      integer          :: ICT1,ICT2,ICT12,ICT512,ICT3,ICT4,ICT34,ICT513,ICT13,ICT24,ICT5,ICT

! function
      real(kind=dppr)  :: SXC1C2
      integer          :: CTRC2V

      real(kind=dppr)  :: S,SIGN

      integer          :: ICTX
!
! RELATIONS TRIANGULAIRES DU 12C
      IF( CTRC2V(ICT1,ICT2,ICT12)   .EQ. 0 ) GOTO 100
      IF( CTRC2V(ICT5,ICT12,ICT512) .EQ. 0 ) GOTO 100
      IF( CTRC2V(ICT1,ICT3,ICT13)   .EQ. 0 ) GOTO 100
      IF( CTRC2V(ICT5,ICT13,ICT513) .EQ. 0 ) GOTO 100
      IF( CTRC2V(ICT2,ICT4,ICT24)   .EQ. 0 ) GOTO 100
      IF( CTRC2V(ICT3,ICT4,ICT34)   .EQ. 0 ) GOTO 100
      IF( CTRC2V(ICT512,ICT34,ICT)  .EQ. 0 ) GOTO 100
      IF( CTRC2V(ICT513,ICT24,ICT)  .EQ. 0 ) GOTO 100
! SOMMATION
      S = 0.D0
E1:   DO ICTX=1,MXSYM
! RELATIONS TRIANGULAIRES LIEES A LA METHODE ELLIOTT
        IF( CTRC2V(ICT24,ICT34,ICTX)   .EQ. 0 ) CYCLE E1
        IF( CTRC2V(ICT3,ICTX,ICT2)     .EQ. 0 ) CYCLE E1
        IF( CTRC2V(ICT513,ICT512,ICTX) .EQ. 0 ) CYCLE E1
        S = S+PC2V(ICTX)*DC2(ICTX)*SXC1C2(ICT24,ICT34,ICTX,ICT512,ICT513,ICT)*  &
                                   SXC1C2(ICT2,ICT3,ICTX,ICT34,ICT24,ICT4)*     &
                                   SXC1C2(ICT3,ICTX,ICT2,ICT12,ICT1,ICT13)*     &
                                   SXC1C2(ICT513,ICT512,ICTX,ICT12,ICT13,ICT5)
      ENDDO E1
      SIGN   = PC2V(ICT1)*PC2V(ICT2)*PC2V(ICT12)*PC2V(ICT512)*            &
               PC2V(ICT3)*PC2V(ICT4)*PC2V(ICT34)*                         &
               PC2V(ICT513)*PC2V(ICT13)*PC2V(ICT24)*PC2V(ICT5)*PC2V(ICT)
      DZCC2V = S*SIGN
      RETURN
!
100   DZCC2V = 0.D0
!
      RETURN
      END FUNCTION DZCC2V
