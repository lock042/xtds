!
! ***** Symboles 3 - C de C2v
!
! ***** M. Rotger 12/98
!
      FUNCTION TRCC2V(ICT1,ICT2,ICT3,IST1,IST2,IST3)
      use mod_dppr
      use mod_par_tds
      IMPLICIT NONE
      real(kind=dppr)  :: TRCC2V
      integer          :: ICT1,ICT2,ICT3,IST1,IST2,IST3

! function
      real(kind=dppr)  :: TRCO2
      integer          :: CTR

      real(kind=dppr)  :: COEFF
      real(kind=dppr)  :: RES,RESB
      real(kind=dppr)  :: SUM

      integer ,dimension(MXSYM)    :: IDIM  = (/ 1, 1, 1, 1 /)
      integer ,dimension(MXSYM)    :: IDIMB = (/ 3, 3, 2, 2 /)
      integer ,dimension(MXSYM,3)  :: ISYM  = reshape( source = (/ 1, 2, 4, 4,        &
                                                                   3, 3, 5, 5,        &
                                                                   5, 4, 0, 0  /),    &
                                                       shape  = shape(ISYM)        )
      integer ,dimension(2,3)      :: ITAB
      integer          :: IC1,IC2,IC3,ITRI
      integer          :: J,J1,J2,J3,JBIS,JTER
!
      TRCC2V    = 0.D0
      COEFF     = 1.D0
      ITAB(1,1) = ICT1
      ITAB(1,2) = ICT2
      ITAB(1,3) = ICT3
      ITAB(2,1) = IST1
      ITAB(2,2) = IST2
      ITAB(2,3) = IST3
      CALL ORDC2V(ITAB,COEFF)
E50:  DO J=1,IDIMB(ITAB(1,1))
        IC1 = ISYM(ITAB(1,1),J)
E60:    DO JBIS=1,IDIMB(ITAB(1,2))
          IC2 = ISYM(ITAB(1,2),JBIS)
E70:      DO JTER=1,IDIMB(ITAB(1,3))
            IC3  = ISYM(ITAB(1,3),JTER)
            ITRI = CTR(IC1,IC2,IC3)
            IF( ITRI .LE. 0 ) CYCLE E70
            RES = TRCO2(IC1,IC2,IC3,ITAB(1,1),ITAB(1,2),ITAB(1,3),ITAB(2,1),ITAB(2,2),ITAB(2,3))
            IF( RES .NE. 0 ) GOTO 100
          ENDDO E70
        ENDDO E60
      ENDDO E50
      RETURN
!
100   SUM = 0.D0
      DO J1=1,IDIM(ITAB(1,1))
        DO J2=1,IDIM(ITAB(1,2))
          DO J3=1,IDIM(ITAB(1,3))
            RESB = TRCO2(IC1,IC2,IC3,ITAB(1,1),ITAB(1,2),ITAB(1,3),J1,J2,J3)
            SUM  = SUM+RESB*RESB
          ENDDO
        ENDDO
      ENDDO
      TRCC2V = 1.D0/SQRT(SUM)*RES*COEFF
!
      RETURN
      END FUNCTION TRCC2V
