!
! SMIL CHAMPION MAI 82
! MOD. T.GABARD DEC 92
!
! ELEMENTS MATRICIELS REDUITS DES OPERATEURS ELEMENTAIRES A1(1,E) ET
! A+1(1,E) RELATIFS A L'OSCILLATEUR DOUBLEMENT DEGENERE.
!     IPM=+1 POUR UN OPERATEUR CREATION
!     IPM=-1 POUR UN OPERATEUR ANNIHILATION
!
      FUNCTION A21(IPM,VVP,LLP,CCP,VV,LL,CC)
      use mod_dppr
      use mod_com_sy
      IMPLICIT NONE
      real(kind=dppr)  :: A21
      integer          :: IPM,VVP,LLP,CCP,VV,LL,CC

! functions
      integer          :: IPSI

      real(kind=dppr)  :: ALPHA,ALPHA2
      real(kind=dppr)  :: BETA,BETA2
      real(kind=dppr)  :: S2

      integer          :: C,CP
      integer          :: L,LP
      integer          :: V,VP
!
      A21 = 0.D0
      IF( IPM .NE. -1 ) GOTO 10
      VP = VVP
      LP = LLP
      CP = CCP
      V  = VV
      L  = LL
      C  = CC
      GOTO 20
!
10    IF( IPM .NE. 1 ) RETURN
      VP = VV
      LP = LL
      CP = CC
      V  = VVP
      L  = LLP
      C  = CCP
!
20    IF( V-VP                               .NE. 1 ) RETURN
      IF( ABS(L-LP)                          .NE. 1 ) RETURN
      IF( IPSI(2,VP,LP,0,CP)*IPSI(2,V,L,0,C) .EQ. 0 ) RETURN
      S2     = SQRT(2.D0)
      ALPHA2 = V+L
      ALPHA  = SQRT(ALPHA2)/S2
      BETA2  = V-L
      BETA   = SQRT(BETA2)/S2
!
! ***  L=0
!
      IF( L .NE. 0 ) GOTO 100
      A21 = -S2*ALPHA
      GOTO 1000
!
! *** LP=0
!
100   IF( LP .NE. 0 ) GOTO 200
      A21 = S2*ALPHA
      GOTO 1000
!
200   SELECT CASE( MOD(L,6) )
!
! ***  L=6P  (DIFFERENT DE ZERO)
!
        CASE( 0 )
          IF( LP-L .EQ.  1 ) A21 = -PC(C)*BETA
          IF( LP-L .EQ. -1 ) A21 = -ALPHA
!
! ***  L=1+6P
!
        CASE( 1 )
          IF( LP-L .EQ.  1 ) A21 = S2*BETA
          IF( LP-L .EQ. -1 ) A21 = ALPHA
!
! ***  L=2+6P
!
        CASE( 2 )
          IF( LP-L .EQ.  1 ) A21 = BETA
          IF( LP-L .EQ. -1 ) A21 = -S2*ALPHA
!
! ***  L=3+6P
!
        CASE( 3 )
          IF( LP-L .EQ.  1 ) A21 = BETA
          IF( LP-L .EQ. -1 ) A21 = -PC(C)*ALPHA
!
! ***  L=4+6P
!
        CASE( 4 )
          IF( LP-L .EQ.  1 ) A21 = -S2*BETA
          IF( LP-L .EQ. -1 ) A21 = -PC(CP)*ALPHA
!
! ***  L=5+6P
!
        CASE( 5 )
          IF( LP-L .EQ.  1 ) A21 = BETA*PC(CP)
          IF( LP-L .EQ. -1 ) A21 = S2*ALPHA
      END SELECT
!
1000  IF( IPM .NE. 1 ) RETURN
      A21 = A21*PC(C)*PC(CP)
!
      RETURN
      END FUNCTION A21
