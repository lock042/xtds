!
! ***** Test : icp contenu dans ict ?
!
! ***** M. Rotger 12/98
!
      FUNCTION ISCTC(IC,ICT)
      use mod_par_tds
      use mod_com_sy
      IMPLICIT NONE
      integer          :: ISCTC
      integer          :: IC,ICT

      integer          :: J,JMAX
!
      ISCTC = 0
      IF( IC .LT. 3 ) THEN
        JMAX = 1
        GOTO 11
      ENDIF
      JMAX = 3
11    DO J=1,JMAX
        IF( IC2VV(IC,J) .EQ. ICT ) ISCTC = 1
      ENDDO
!
      RETURN
      END FUNCTION ISCTC
