!
! ***** Ordonnancement des 3-C de C2v
!
! ***** M. Rotger 02/99
!
      SUBROUTINE ORDC2V(ITAB,COEFF)
      use mod_dppr
      use mod_par_tds
      use mod_com_sy
      IMPLICIT NONE
      real(kind=dppr)  :: COEFF
      integer         ,dimension(2,3)    :: ITAB

      integer         ,dimension(3)      :: IRES
      integer          :: IBUF,ICODE1,ICODE2,ICODE3,INUM
      integer          :: J
!
      ICODE1  = ITAB(1,1)*10+ITAB(2,1)
      ICODE2  = ITAB(1,2)*10+ITAB(2,2)
      ICODE3  = ITAB(1,3)*10+ITAB(2,3)
      IRES(1) = ICODE1
      IRES(2) = ICODE2
      IRES(3) = ICODE3
      INUM    = 0
      IF( IRES(1) .GT. IRES(2) ) THEN
        IBUF    = IRES(2)
        IRES(2) = IRES(1)
        IRES(1) = IBUF
        INUM    = INUM+1
      ENDIF
      IF( IRES(1) .GT. IRES(3) ) THEN
        IBUF    = IRES(3)
        IRES(3) = IRES(1)
        IRES(1) = IBUF
        INUM    = INUM+1
      ENDIF
      IF( IRES(2) .GT. IRES(3) ) THEN
        IBUF    = IRES(3)
        IRES(3) = IRES(2)
        IRES(2) = IBUF
        INUM    = INUM+1
      ENDIF
      DO J=1,3
        ITAB(1,J) = IRES(J)/10
        ITAB(2,J) = IRES(J)-10*ITAB(1,J)
      ENDDO
      IF( INUM-2*(INUM/2) .EQ. 1 ) THEN
        COEFF = PC2V(ITAB(1,1))*PC2V(ITAB(1,2))*PC2V(ITAB(1,3))
      ENDIF
!
      RETURN
      END SUBROUTINE ORDC2V
