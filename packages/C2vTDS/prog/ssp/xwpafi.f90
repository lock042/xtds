      SUBROUTINE XWPAFI
!
! 2011 JULY
! V. BOUDON, Ch. WENGER
!
! WRITE OUT  PARAMETER-FILE_adj
!
! BASED UPON eq_tds.f90, paradj.f90
!
      use mod_dppr
      use mod_par_tds
      use mod_par_x
      use mod_com_fdate
      use mod_com_postf
      use mod_com_xassi
      use mod_com_xfonc
      use mod_com_xpara
      use mod_com_xtran
      use mod_com_xvari
      IMPLICIT NONE

      real(kind=dppr)  :: PARAC
      real(kind=dppr)  :: STDC,STDTOT

      integer ,dimension(0:MXATRT)  :: JTRHT,NTRHT
      integer          :: I,IDEB,IOBS,IOBZ,ITRAD,ITRT
      integer          :: IX,IZC
      integer          :: J,JPF
      integer          :: LIBC
      integer          :: NBOPHC,NBOPTC
      integer          :: NCL1,NTRAD

      character(len = NBCLAB+10)  :: COPAJC
      character(len = NBCTIT)     :: TITRE
      character(len = 120)  :: FPARAC
!
1000  FORMAT(A)
1002  FORMAT(I5,' Data ; Jmax ',I3,' ; St Dev ',F8.3)
1003  FORMAT(I4,A)
1004  FORMAT(A, 5X,'Hmn  Frdm         Value/cm-1  St.Dev./cm-1')
1200  FORMAT(A,2X,I4,1X,E18.11,E14.7)
2001  FORMAT(' >> ',A)
8000  FORMAT(' !!! XWPAFI : STOP ON ERROR')
8012  FORMAT(' !!! ERROR OPENING PARAMETER FILE : ',A)
8013  FORMAT(' !!! ERROR OPENING FITTED PARAMETER FILE : ',A)
8014  FORMAT(' !!! UNEXPECTED EOF FOR PARAMETER FILE : ',A)
!
      STDTOT = STDF
      NCL1   = NBCLAB+1+NBAM+1                                                                     ! Hmn  included
!
! WRITE OUT FITTED PARAMETER FILE
!
! INITIALIZE ASSIGNMENT J MAX VALUES
! AND        NUMBER OF RELATED ASSIGNMENTS
! FOR EACH TRANSITION TYPE.
      DO ITRT=0,NTRT
        JTRHT(ITRT) = 0
        NTRHT(ITRT) = 0
      ENDDO
! FREQUENCY
      JPF = 0
      DO IOBZ=1,NBFOBZ
        IOBS = INDOBS(IOBZ)
        IF( JIOBS(IOBS) .GT. JPF ) JPF = JIOBS(IOBS)
      ENDDO
      JTRHT(0) = JPF
      NTRHT(0) = NBFOBZ
! INTENSITY
      DO IZC=1,NBSOBZ
        IF( SASOZ(IZC) .NE. '-' ) THEN
          IOBS = INDOBS(IZC+NBFOBZ)
          ITRT = INTRT(JTR(IOBS))
          IF( JIOBS(IOBS) .GT. JTRHT(ITRT) ) JTRHT(ITRT) = JIOBS(IOBS)
          NTRHT(ITRT) = NTRHT(ITRT)+1
        ENDIF
      ENDDO
! SPECIFIC JTRHT(0)
      DO ITRT=1,NTRT
        IF( JTRHT(ITRT) .GT. JTRHT(0) ) JTRHT(0) = JTRHT(ITRT)
      ENDDO
      NTRAD = NTR+NAD
E408: DO ITRAD=1,NTRAD
        FPARAC = FPARA(ITRAD)                                                                      ! original parameter file
        IF( FPARAC .EQ. '' ) CYCLE E408                                                            ! no related para_file
        OPEN(70,ERR=9994,FILE=TRIM(FPARAC),STATUS='OLD')
        FPARAC = TRIM(FPARAC)//'_adj'
        PRINT 2001,           TRIM(FPARAC)
        OPEN(71,ERR=9995,FILE=TRIM(FPARAC),STATUS='UNKNOWN')                                       ! fitted parameter file
        FPARAC = FPARA(ITRAD)
! HAMILTONIAN PARAMETERS
        READ(70,1000,END=9996)
        IF( NBFOBZ .EQ. 0 ) THEN                                                                   ! T type
          WRITE(71,1002) 0,0,STDTOT
        ELSE
          WRITE(71,1002) NBROBZ,JTRHT(0),STDTOT
        ENDIF
        DO I=2,4
          READ (70,1000,END=9996) TITRE
          WRITE(71,1000)          TRIM(TITRE)
        ENDDO
        READ (70,1003,END=9996) NBOPHC,TITRE
        WRITE(71,1003)          NBOPHC,TRIM(TITRE)
        READ (70,1000,END=9996) TITRE
        WRITE(71,1000)          TRIM(TITRE)
        READ(70,1000,END=9996) TITRE
        IF( NBFOBZ .EQ. 0 ) THEN                                                                   ! T type
          WRITE(71,1000)       TRIM(TITRE)
        ELSE
          WRITE(71,1004) FDATE
        ENDIF
        IX = 0
E402:   DO J=1,NBOPHC
          READ(70,1000,END=9996)
          IF( ICNTRL(J) .EQ. 2 .OR.         &
              ICNTRL(J) .EQ. 4      ) THEN                                                         ! fitted
            IX   = IX+1
            STDC = SQRT(COV(IX,IX))
          ELSE
            STDC = 0.D0
          ENDIF
          WRITE(71,1200) COPAJ(J)(:NCL1),LIB(J),PARAR(J),STDC
          IF( ICNTRL(J) .EQ. 5 ) THEN
!
403         READ(70,1000,END=402) TITRE
            IF( INDEX(TITRE,'*') .GE. 1 ) THEN
              WRITE(71,1000) TRIM(TITRE)
              GOTO 403
            ELSE
              BACKSPACE(70)
              CYCLE E402
            ENDIF
          ENDIF
!
402       CONTINUE
        ENDDO E402
! TRANSITION PARAMETERS
        READ(70,1000,END=9996)
        ITRT = INTRT(ITRAD)
        IF( ITRT .EQ. 0 ) THEN                                                                     ! H type
          IDEB = 0
          WRITE(71,1002) 0,0,STDTOT
        ELSE                                                                                       ! T type
          IX   = INXPHT(ITRT)
          IDEB = INPHT(ITRT)
          WRITE(71,1002) NTRHT(ITRT),JTRHT(ITRT),STDTOT
        ENDIF
        DO I=2,4
          READ (70,1000,END=9996) TITRE
          WRITE(71,1000)          TRIM(TITRE)
        ENDDO
        READ (70,1003,END=9996) NBOPTC,TITRE
        WRITE(71,1003)          NBOPTC,TRIM(TITRE)
        READ (70,1000,END=9996) TITRE
        WRITE(71,1000)          TRIM(TITRE)
        READ(70,1000,END=9996) TITRE
        IF( ITRT .EQ. 0 ) THEN                                                                     ! H type
          WRITE(71,1000)       TRIM(TITRE)
        ELSE                                                                                       ! T type
          WRITE(71,1004) FDATE
        ENDIF
E406:   DO J=IDEB+1,IDEB+NBOPTC
          READ(70,1200,END=9996) COPAJC(:NCL1),LIBC,PARAC
          IF( ITRT .EQ. 0 ) THEN                                                                   ! H type
            WRITE(71,1200) COPAJC(:NCL1),0,PARAC,0.D0
          ELSE                                                                                     ! T type
            IF( ICNTRL(J) .EQ. 2 .OR.         &
                ICNTRL(J) .EQ. 4      ) THEN                                                       ! fitted
              IX   = IX+1
              STDC = SQRT(COV(IX,IX))
            ELSE
              STDC = 0.D0
            ENDIF
            WRITE(71,1200) COPAJ(J)(:NCL1),LIB(J),PARAR(J),STDC
          ENDIF
          IF( ICNTRL(J) .EQ. 5 ) THEN
!
407         READ(70,1000,END=406) TITRE
            IF( INDEX(TITRE,'*') .GE. 1 ) THEN
              WRITE(71,1000) TRIM(TITRE)
              GOTO 407
            ELSE
              BACKSPACE(70)
              CYCLE E406
            ENDIF
          ENDIF
!
406       CONTINUE
        ENDDO E406
        CLOSE(71)
        CLOSE(70)
      ENDDO E408
      RETURN
!
9994  PRINT 8012, FPARAC
      GOTO  9999
9995  PRINT 8013, FPARAC
      GOTO  9999
9996  PRINT 8014, FPARAC
      GOTO  9999
9999  PRINT 8000
      PRINT *
      STOP
!
      END SUBROUTINE XWPAFI
