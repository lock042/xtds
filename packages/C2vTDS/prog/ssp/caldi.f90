!
! ***  CALCULE LES ELEM. MAT. DES OP. DU M. DIP.
!
! SMIL J.P.C., J.M.J. , G.P.    DEC.88
! MOD. T.GABARD MAR. 93
! REV    JAN 1995 JPC,CW (PARAMETER)
! MODIFIE 03/98 V. BOUDON ---> XY6/Oh.
! MODIFIE 03/99 M. ROTGER ---> XY5Z/C4v.
! MODIFIE 02/01 M. ROTGER ---> XY2Z2/C2v.
!
      SUBROUTINE CALDI(JS,ICS,NFBS,JI,ICI,ISIG,NFBI,ICOR,IGVT,IGVRT)
      use mod_dppr
      use mod_par_tds
      use mod_com_sy
      use mod_main_dipmat
      IMPLICIT NONE
      integer          :: JS,ICS,NFBS,JI,ICI,ISIG,NFBI,ICOR,IGVT,IGVRT

! functions
      real(kind=dppr)  :: AKPRIM,DKUB,DZCC2V,EMRRO,ODKUB,TRCC2V
      integer          :: NSYM1

      real(kind=dppr)  :: AK1,AK2
      real(kind=dppr)  :: CACOS,CALAB,CAMOL,COEF
      real(kind=dppr)  :: ELM
      real(kind=dppr)  :: PHASE1,PHASE2
      real(kind=dppr)  :: SUMC,SUMN
      real(kind=dppr)  :: TERM1,TERM2,TRC
      real(kind=dppr)  :: XC

      integer          :: IB,IBRA,IC2VB,IC2VK,IC2VRB,IC2VRK,ICRB,ICRK,ICSUM,ICTSUM
      integer          :: IG,IGT,IGVRTB,IK,IKET,IO,ISVRTB,IVB,IVK
      integer          :: KR
      integer          :: NG,NMB,NRB,NRCB,NRCK,NRK,NSSUM,NVB,NVK
!
! OMEGA
!
      IO = ICOR/10000
!
! KR
!
      KR = (ICOR-IO*10000)/1000
!
! GAMMA R
!
      IG = (ICOR-IO*10000-KR*1000)/100
!
! N R
!
      NG    = (ICOR-IO*10000-KR*1000-IG*100)/10
      IGT   = ICOR-IO*10000-KR*1000-IG*100-10*NG
      NBELM = 0
!
! POLYADE INFERIEURE
!
E3:   DO IBRA=1,NFBI
        NVB = NVCODI(IBRA)
!
! NUMERO DU SOUS-NIVEAU VIBRATIONNEL INF.
!
        IB = NVB/100
!
! C'V
!
        IC2VB = NVB-100*IB
        IVB   = IC2VB/10
        IC2VB = IC2VB-10*IVB
        NRCB  = NRCODI(IBRA)
!
! PARITE DE C'V
!
!
! N'
!
        NRB    = NRCB/100
        IC2VRB = NRCB-100*NRB
!
! C'R
!
        ICRB   = IC2VRB/10
        IC2VRB = IC2VRB-10*ICRB
!
!   POLYADE SUPERIEURE
!
E4:     DO IKET=1,NFBS
          NVK = NVCODS(IKET)
!
! NUMERO DU SOUS-NIVEAU VIBRATIONNEL SUP.
!
          IK = NVK/100
          IF( EMRD(IK,IB) .EQ. 0.D0 ) CYCLE E4
!
! CV
!
          IC2VK = NVK-100*IK
          IVK   = IC2VK/10
          IC2VK = IC2VK-10*IVK
          NRCK  = NRCODS(IKET)
!
! PARITE DE CV
!
!
! N
!
          NRK    = NRCK/100
          IC2VRK = NRCK-100*NRK
!
! CR
!
          ICRK   = IC2VRK/10
          IC2VRK = IC2VRK-10*ICRK
!
          CALL MULA2(IGVRT,1,IGVRTB,ISVRTB)
!
!    DEUXIEME SOMMATION
!
          TERM1 = 0.D0
E5:       DO ICTSUM=1,MXSYM
            XC = DZCC2V(IGT,IGVT,IGVRT,2,IC2VRB,IC2VB,       &
                        ICI,IC2VRK,ICTSUM,IC2VK,IGVRTB,ICS)
            IF( XC .EQ. 0.D0 ) CYCLE E5
            SUMN = 0.D0
E120:       DO ICSUM=1,MXSYR
              NSSUM = NSYM1(JI,1,ICSUM)
              IF( NSSUM .EQ. 0 ) CYCLE E120
              DO NMB=0,NSSUM-1
                IF( JS .EQ. JI ) THEN
                  CACOS = DKUB(1,JI,NRK,NMB,4,ICRK,ICSUM)
                ELSE
                  CACOS = ODKUB(JS,JI,NRK,NMB,ICRK,ICSUM)
                ENDIF
                IF(  KR    .EQ. 0          .AND.         &
                    (ICSUM .NE. ICRB .OR.                &
                     NMB   .NE. NRB      )       ) THEN
                  CAMOL = 0.D0
                ELSE
                  CAMOL = DKUB(KR,JI,NMB,NRB,IG,ICSUM,ICRB)
                ENDIF
                AK1   = AKPRIM(ICSUM,4,ICRK,ICTSUM,IGVRTB,IC2VRK)
                AK2   = AKPRIM(ICRB,IG,ICSUM,IC2VRB,IGT,ICTSUM)
                CALAB = CACOS*CAMOL*AK1*AK2
                SUMN  = SUMN+CALAB
              ENDDO
            ENDDO E120
            SUMC  = SUMN*XC
            TERM1 = TERM1+SUMC
          ENDDO E5
          PHASE1 = PC2V(2)*PC2V(ICS)*PC2V(ICI)*PC2V(IGT)*PC2V(IGVT)*    &
                   PC2V(IGVRTB)*PC2V(IC2VRB)*PC2V(IC2VB)*PC2V(IC2VRK)*  &
                   PC2V(IC2VK)
          TERM1  = TERM1*PHASE1*EMRRO(IO,KR,JI)
!
!    PREMIERE SOMMATION
!
          TERM2 = 0.D0
E6:       DO ICTSUM=1,MXSYM
            XC = DZCC2V(IGT,IGVT,IGVRT,2,IC2VRK,IC2VK,       &
                        ICS,IC2VRB,ICTSUM,IC2VB,IGVRTB,ICI)
            IF( XC .EQ. 0.D0 ) CYCLE E6
            SUMN = 0.D0
E220:       DO ICSUM=1,MXSYR
              NSSUM = NSYM1(JS,1,ICSUM)
              IF( NSSUM .EQ. 0 ) CYCLE E220
              DO NMB=0,NSSUM-1
                IF( JS .EQ. JI ) THEN
                  CACOS = DKUB(1,JI,NMB,NRB,4,ICSUM,ICRB)
                ELSE
                  CACOS = ODKUB(JS,JI,NMB,NRB,ICSUM,ICRB)
                ENDIF
                IF(  KR    .EQ. 0          .AND.         &
                    (ICSUM .NE. ICRK .OR.                &
                     NMB   .NE. NRK      )       ) THEN
                  CAMOL = 0.D0
                ELSE
                  CAMOL = DKUB(KR,JS,NRK,NMB,IG,ICRK,ICSUM)
                ENDIF
                AK1   = AKPRIM(ICRB,4,ICSUM,IC2VRB,IGVRTB,ICTSUM)
                AK2   = AKPRIM(ICSUM,IG,ICRK,ICTSUM,IGT,IC2VRK)
                CALAB = CACOS*CAMOL*AK1*AK2
                SUMN  = SUMN+CALAB
              ENDDO
            ENDDO E220
            SUMC  = SUMN*XC
            TERM2 = TERM2+SUMC
          ENDDO E6
          PHASE2 = (-1)**(JI+JS)
          TERM2  = TERM2*PHASE2*EMRRO(IO,KR,JS)
!
!    SOMME DES 2 TERMES
!
          TRC   = TRCC2V(2,ICS,ICI,1,1,ISIG)
          COEF  = 1.D0/2.0D0*TRC*SQRT(DC2(IGVRT))
          COEF  = COEF*SQRT(DC2(ICI))*SQRT(DC2(ICS))*EMRD(IK,IB)*SQRT(DBLE((2*JI+1)*(2*JS+1)))
          ELM   = (TERM1+TERM2)*COEF
          NBELM = NBELM+1
          IF( NBELM .GT. MXELMT ) CALL RESIZE_MXELMT
          LI(NBELM) = IBRA
          KO(NBELM) = IKET
          H(NBELM)  = ELM
        ENDDO E4
      ENDDO E3
!
      RETURN
      END SUBROUTINE CALDI
