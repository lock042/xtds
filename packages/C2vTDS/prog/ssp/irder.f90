!
! *** CREATION D'UN TABLEAU D'ORDONNANCEMENT.
!     LE TABLEAU ARGUMENT EST ENTIER
!
! SMIL C. MILAN MAI 75
!
      SUBROUTINE IRDER(M,K1,IND,MDM)
      IMPLICIT NONE
      integer ,dimension(MDM)  :: K1,IND
      integer          :: M,MDM

      integer          :: I,I1,I2,IK,IR,IT1,IT2
      integer          :: J
      integer          :: N
!
      N      = 1
      IND(1) = 1
      IF( M .LE. 1 ) RETURN
!
1     IT1 = N+1
      IK  = K1(IT1)
      J   = IND(1)
      IF( IK .LE. K1(J) ) GOTO 2
      I1 = 1
      I2 = IT1
!
3     I = (I1+I2)/2
      J = IND(I)
      IF( IK .GE. K1(J) ) GOTO 4
      I2 = I
      GOTO 5
!
4     I1 = I
!
5     IF( I2-I1 .EQ. 1 ) THEN
        GOTO 6
      ELSE
        GOTO 3
      ENDIF
!
2     I2 = 1
!
6     IF( I2 .EQ. IT1 ) GOTO 7
      IR = N
!
8     IT2      = IR+1
      IND(IT2) = IND(IR)
      IF( I2 .EQ. IR ) GOTO 7
      IR = IR-1
      GOTO 8
!
7     IND(I2) = IT1
      IF( M .EQ. IT1 ) RETURN
      N = N+1
      GOTO 1
!
      END SUBROUTINE IRDER
