      PROGRAM ASTRAN
!
! REV    SEPT 2006  CW
!
!  lire un fichier binaire (ex. INFILE) de type trans.t
!  et l'ecrire en ASCII dans INFILE'_ASC'
!
! APPEL : astran
!
      use mod_dppr
      use mod_par_tds
      IMPLICIT NONE

      real(kind=dppr)  :: A2
      real(kind=dppr)  :: ENINF
      real(kind=dppr)  :: F

      integer          :: ICI,ICS,IPCINF,IPCSUP,IRA,ISV
      integer          :: JI,JMAX,JS
      integer          :: NINF,NSUP,NSVINF,NSVSUP,NUSVI,NUSVS

      character(len = NBCTIT)  :: TITRE
      character(len = 150)  :: INFILE
      character(len = 160)  :: OUTFILE
!
1000  FORMAT(A)
1020  FORMAT(/,            &
             'ASTRAN : ')
2000  FORMAT(' <  ',A)
2001  FORMAT(' >> ',A)
7000  FORMAT('ENTER trans.t TYPE FILE NAME [trans.t] :')
8000  FORMAT(' !!! ASTRAN : STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF FOR ',A)
!
      PRINT 1020
!
!  FICHIER D'ENTREE
!
      PRINT 7000
      READ(*,1000) INFILE
      IF( LEN_TRIM(INFILE) .EQ. 0 ) THEN
        INFILE = 'trans.t'
      ENDIF
      PRINT 2000,  TRIM(INFILE)
      OPEN(91,FILE=TRIM(INFILE),FORM='UNFORMATTED',STATUS='OLD')
!
!  FICHIER DE SORTIE
!
      OUTFILE = TRIM(INFILE)//'_ASC'
      PRINT 2001,  TRIM(OUTFILE)
      OPEN(10,FILE=TRIM(OUTFILE))
!
!    LECTURE DES PARAMETRES DE H
!
      CALL IOPBAS(91,10)
!
!    LECTURE DES PARAMETRES DU M.D.
!
      CALL IOPBAS(91,10)
!
!   NSVSUP SOUS-NIVEAUX VIBRATIONNELS SUP
!
      WRITE(10,*) '>>> NSVSUP'
      READ (91,END=9003) NSVSUP
      WRITE(10,*)        NSVSUP
      DO ISV=1,NSVSUP
        READ (91,END=9003) TITRE(:62)
        WRITE(10,*)        TITRE(:62)
      ENDDO
!
!    NSVINF SOUS-NIVEAUX VIBRATIONNELS INF
!
      WRITE(10,*) '>>> NSVINF'
      READ (91,END=9003) NSVINF
      WRITE(10,*)        NSVINF
      DO ISV=1,NSVINF
        READ (91,END=9003) TITRE(:62)
        WRITE(10,*)        TITRE(:62)
      ENDDO
      WRITE(10,*) '>>> JMAX'
      READ (91,END=9003) JMAX
      WRITE(10,*)        JMAX
      WRITE(10,*) '>>> IRA   (0 if pol option set at TRA call)'
      READ (91,END=9003) IRA
      WRITE(10,*)        IRA
      WRITE(10,*) '>>> JI,ICI,NINF,NUSVI,IPCINF,ENINF,'//  &
                      'JS,ICS,NSUP,NUSVS,IPCSUP,F,A2'
!
3     READ (91,END=9000) JI,ICI,NINF,NUSVI,IPCINF,  &
                         ENINF,                     &
                         JS,ICS,NSUP,NUSVS,IPCSUP,  &
                         F,A2
      WRITE(10,*)        JI,ICI,NINF,NUSVI,IPCINF,  &
                         ENINF,                     &
                         JS,ICS,NSUP,NUSVS,IPCSUP,  &
                         F,A2
      GOTO 3
!
9003  PRINT 8003, TRIM(INFILE)
      PRINT 8000
!
9000  CLOSE(10)
      PRINT *
      END PROGRAM ASTRAN
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!     LECTURE ET RECOPIE DES PARAMETRES
!
      SUBROUTINE IOPBAS(LUI,LUO)
      use mod_dppr
      use mod_par_tds
      IMPLICIT NONE
      integer          :: LUI,LUO

      real(kind=dppr)  :: PARA,PREC

      integer          :: I,IP
      integer          :: NBOPH

      character(len = NBCLAB+10)  :: CHAINE
      character(len = NBCTIT)     :: TITRE
!
1000  FORMAT(A)
1001  FORMAT(I4,A)
1002  FORMAT(A,E18.11,E14.7)
8000  FORMAT(' !!! ASTRAN : STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF IN IOPBAS')
!
      DO I=1,4
        READ (LUI,END=2000) TITRE
        WRITE(LUO,1000)     TRIM(TITRE)
      ENDDO
      READ (LUI,END=2000) NBOPH,TITRE
      WRITE(LUO,1001)     NBOPH,TRIM(TITRE)
      DO I=1,2
        READ (LUI,END=2000) TITRE
        WRITE(LUO,1000)     TRIM(TITRE)
      ENDDO
      DO IP=1,NBOPH
        READ (LUI,END=2000) CHAINE,PARA,PREC
        WRITE(LUO,1002)     CHAINE,PARA,PREC
      ENDDO
      RETURN
!
2000  PRINT 8003
      PRINT 8000
      PRINT *
      STOP
!
      END SUBROUTINE IOPBAS
