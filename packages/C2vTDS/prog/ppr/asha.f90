      PROGRAM ASHA
!
! REV    SEPT 2006  CW
!
!  lire un fichier binaire (ex. INFILE) de type HA_
!  et l'ecrire en ASCII dans INFILE'_ASC'
!
! APPEL : asha
!
      use mod_dppr
      use mod_par_tds
      use mod_main_hmatri
      IMPLICIT NONE

      integer          :: I,IC,ICC,IOPH,IOPHC,IP,ISV
      integer          :: J
      integer          :: NELMA,NFB,NSV

      character(len = NBCTIT)  :: IDENT,TITRE
      character(len =  40)  :: IDEMR
      character(len = 150)  :: INFILE
      character(len = 160)  :: OUTFILE
!
1000  FORMAT(A)
1020  FORMAT(/,            &
             'ASHA   : ')
2000  FORMAT(' <  ',A)
2001  FORMAT(' >> ',A)
3000  FORMAT(2I8,E20.11)
7000  FORMAT('ENTER HA_ TYPE FILE NAME :')
8000  FORMAT(' !!! ASHA   : STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF FOR ',A)
!
! ARRAY INITIAL ALLOCATION
!
      CALL ALLOC_ALL
!
      PRINT 1020
!
!  FICHIER D'ENTREE
!
      PRINT 7000
      READ(*,1000) INFILE
      PRINT 2000,  TRIM(INFILE)
      OPEN(40,FILE=TRIM(INFILE),FORM='UNFORMATTED',STATUS='OLD')
!
!  FICHIER DE SORTIE
!
      OUTFILE = TRIM(INFILE)//'_ASC'
      PRINT 2001,  TRIM(OUTFILE)
      OPEN(10,FILE=TRIM(OUTFILE))
!
! LECTURE DES CARACTERISTIQUES GENERALES
!
      DO I=1,3
        READ (40,END=9003) TITRE
        WRITE(10,*)        TITRE
      ENDDO
      READ (40,END=9003) IDEMR
      WRITE(10,*)        IDEMR
      DO I=1,3
        READ (40,END=9003) TITRE
        WRITE(10,*)        TITRE
      ENDDO
!
5     READ(40,END=9003) TITRE
      IF( TITRE(:22) .NE. '     (v1;v2;v3;v4) = (' ) THEN
        BACKSPACE(40)
        GOTO 4
      ENDIF
      WRITE(10,*)       TITRE
      GOTO 5
!
4     READ (40,END=9003) TITRE
      WRITE(10,*)        TITRE
      WRITE(10,*) '>>> NSV'
      READ (40,END=9003) NSV,TITRE
      WRITE(10,*)        NSV,TITRE
      READ (40,END=9003) TITRE
      WRITE(10,*)        TITRE
      DO ISV=1,NSV
        READ (40,END=9003) IDENT(:62)
        WRITE(10,*)        IDENT(:62)
      ENDDO
      DO I=1,3
        READ (40,END=9003) TITRE
        WRITE(10,*)        TITRE
      ENDDO
      READ (40,END=9003) TITRE(:67)
      WRITE(10,*)        TITRE(:67)
      READ (40,END=9003) NBOPH,TITRE(:24)
      WRITE(10,*)        NBOPH,TITRE(:24)
      WRITE(10,*) '>>> J,IC,NELMA,NFB'
      WRITE(10,*) '>>> (NVCOD(IP),NRCOD(IP),IP=1,NFB)'
      WRITE(10,*) '>>> J,IC,NELMA,NFB,IOPH,NBELM'
      WRITE(10,*) '>>> (LI(I),KO(I),H(I),I=1,NBELM)'
!
12    CONTINUE
E13:  DO ICC=1,MXSYM
        READ (40,END=9000) J,IC,NELMA,NFB
        WRITE(10,*)        J,IC,NELMA,NFB
        DO WHILE( NFB .GT. MXDIMS )
          CALL RESIZE_MXDIMS
        ENDDO
        IF( NFB .EQ. 0 ) CYCLE E13
        READ (40,END=9003) (NVCOD(IP),NRCOD(IP),IP=1,NFB)
        WRITE(10,*)        (NVCOD(IP),NRCOD(IP),IP=1,NFB)
E14:    DO IOPHC=1,NBOPH
          READ (40,END=9003) J,IC,NELMA,NFB,IOPH,NBELM
          WRITE(10,*)        J,IC,NELMA,NFB,IOPH,NBELM
          DO WHILE( NBELM .GT. MXELMH )
            CALL RESIZE_MXELMH
          ENDDO
          IF( NBELM .EQ. 0 ) CYCLE E14
          READ (40,END=9003) (LI(I),KO(I),H(I),I=1,NBELM)
          WRITE(10,3000)     (LI(I),KO(I),H(I),I=1,NBELM)
        ENDDO E14
      ENDDO E13
      GOTO 12
!
9003  PRINT 8003, TRIM(INFILE)
      PRINT 8000
!
9000  CLOSE(10)
      PRINT *
      END PROGRAM ASHA
