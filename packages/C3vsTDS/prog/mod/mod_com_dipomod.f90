
      module mod_com_dipomod

      use mod_dppr
      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      integer ,pointer ,dimension(:,:)   ,save  :: IFFI,IFFS                               ! (NBVQN+1,MXSNV)
      integer ,pointer ,dimension(:)     ,save  :: ICODEI,ICODES,INDI,INDS                 ! (MXSNEV)
      integer ,pointer ,dimension(:)     ,save  :: ICODLL,ICODLS                           ! (MXOPT)
      integer ,pointer ,dimension(:)     ,save  :: ICODRO,ICODSY,ICODVA,ICODVC,ICODVI      ! (MXOPT)
      integer ,pointer ,dimension(:,:,:) ,save  :: NVIBI,NVIBS                             ! (MXPOL,MXNIV,NBVQN)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_DIPOMOD
      IMPLICIT NONE

      integer  :: ierr

      allocate(IFFI(NBVQN+1,MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'IFFI')
      IFFI = 0
      allocate(IFFS(NBVQN+1,MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'IFFS')
      IFFS = 0
      allocate(ICODLL(MXOPT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DIPOMOD_ICODLL')
      ICODLL = 0
      allocate(ICODEI(MXSNEV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DIPOMOD_ICODEI')
      ICODEI = 0
      allocate(ICODES(MXSNEV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DIPOMOD_ICODES')
      ICODES = 0
      allocate(INDI(MXSNEV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DIPOMOD_INDI')
      INDI = 0
      allocate(INDS(MXSNEV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DIPOMOD_INDS')
      INDS = 0
      allocate(ICODLS(MXOPT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DIPOMOD_ICODLS')
      ICODLS = 0
      allocate(ICODRO(MXOPT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DIPOMOD_ICODRO')
      ICODRO = 0
      allocate(ICODSY(MXOPT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DIPOMOD_ICODSY')
      ICODSY = 0
      allocate(ICODVA(MXOPT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DIPOMOD_ICODVA')
      ICODVA = 0
      allocate(ICODVC(MXOPT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DIPOMOD_ICODVC')
      ICODVC = 0
      allocate(ICODVI(MXOPT),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DIPOMOD_ICODVI')
      ICODVI = 0
      allocate(NVIBI(MXPOL,MXNIV,NBVQN),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DIPOMOD_NVIBI')
      NVIBI = 0
      allocate(NVIBS(MXPOL,MXNIV,NBVQN),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_DIPOMOD_NVIBS')
      NVIBS = 0
!
      return
      end subroutine ALLOC_DIPOMOD

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXNIV

      subroutine RESIZE_MXNIV_DIPOMOD(C_NIV)
      IMPLICIT NONE
      integer :: C_NIV

      integer :: i,ierr
      integer :: j

! NVIBI
      allocate(ipd3(MXPOL,C_NIV,NBVQN),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXNIV_DIPOMOD_NVIBI')
      ipd3 = 0
      do j=1,NBVQN
        do i=1,MXNIV
          ipd3(:,i,j) = NVIBI(:,i,j)
        enddo
      enddo
      deallocate(NVIBI)
      NVIBI => ipd3
! NVIBS
      allocate(ipd3(MXPOL,C_NIV,NBVQN),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXNIV_DIPOMOD_NVIBS')
      ipd3 = 0
      do j=1,NBVQN
        do i=1,MXNIV
          ipd3(:,i,j) = NVIBS(:,i,j)
        enddo
      enddo
      deallocate(NVIBS)
      NVIBS => ipd3
!
      return
      end subroutine RESIZE_MXNIV_DIPOMOD

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOPT

      subroutine RESIZE_MXOPT_DIPOMOD(C_OPT)
      IMPLICIT NONE
      integer :: C_OPT

      integer :: ierr

! ICODLL
      allocate(ipd1(C_OPT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPT_DIPOMOD_ICODLL')
      ipd1 = 0
      ipd1(1:MXOPT) = ICODLL(:)
      deallocate(ICODLL)
      ICODLL => ipd1
! ICODLS
      allocate(ipd1(C_OPT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPT_DIPOMOD_ICODLS')
      ipd1 = 0
      ipd1(1:MXOPT) = ICODLS(:)
      deallocate(ICODLS)
      ICODLS => ipd1
! ICODRO
      allocate(ipd1(C_OPT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPT_DIPOMOD_ICODRO')
      ipd1 = 0
      ipd1(1:MXOPT) = ICODRO(:)
      deallocate(ICODRO)
      ICODRO => ipd1
! ICODSY
      allocate(ipd1(C_OPT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPT_DIPOMOD_ICODSY')
      ipd1 = 0
      ipd1(1:MXOPT) = ICODSY(:)
      deallocate(ICODSY)
      ICODSY => ipd1
! ICODVA
      allocate(ipd1(C_OPT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPT_DIPOMOD_ICODVA')
      ipd1 = 0
      ipd1(1:MXOPT) = ICODVA(:)
      deallocate(ICODVA)
      ICODVA => ipd1
! ICODVC
      allocate(ipd1(C_OPT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPT_DIPOMOD_ICODVC')
      ipd1 = 0
      ipd1(1:MXOPT) = ICODVC(:)
      deallocate(ICODVC)
      ICODVC => ipd1
! ICODVI
      allocate(ipd1(C_OPT),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPT_DIPOMOD_ICODVI')
      ipd1 = 0
      ipd1(1:MXOPT) = ICODVI(:)
      deallocate(ICODVI)
      ICODVI => ipd1
!
      return
      end subroutine RESIZE_MXOPT_DIPOMOD

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXSNEV

      subroutine RESIZE_MXSNEV_DIPOMOD(C_SNEV)
      IMPLICIT NONE
      integer :: C_SNEV

      integer :: ierr

! ICODEI
      allocate(ipd1(C_SNEV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNEV_DIPOMOD_ICODEI')
      ipd1 = 0
      ipd1(1:MXSNEV) = ICODEI(:)
      deallocate(ICODEI)
      ICODEI => ipd1
! ICODES
      allocate(ipd1(C_SNEV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNEV_DIPOMOD_ICODES')
      ipd1 = 0
      ipd1(1:MXSNEV) = ICODES(:)
      deallocate(ICODES)
      ICODES => ipd1
! INDI
      allocate(ipd1(C_SNEV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNEV_DIPOMOD_INDI')
      ipd1 = 0
      ipd1(1:MXSNEV) = INDI(:)
      deallocate(INDI)
      INDI => ipd1
! INDS
      allocate(ipd1(C_SNEV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNEV_DIPOMOD_INDS')
      ipd1 = 0
      ipd1(1:MXSNEV) = INDS(:)
      deallocate(INDS)
      INDS => ipd1
!
      return
      end subroutine RESIZE_MXSNEV_DIPOMOD

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXSNV

      subroutine RESIZE_MXSNV_DIPOMOD(C_SNV)
      IMPLICIT NONE
      integer :: C_SNV

      integer :: i,ierr

! IFFI
      allocate(ipd2(NBVQN+1,C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_DIPOMOD_IFFI')
      ipd2 = 0
      do i=1,MXSNV
        ipd2(:,i) = IFFI(:,i)
      enddo
      deallocate(IFFI)
      IFFI => ipd2
! IFFS
      allocate(ipd2(NBVQN+1,C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_DIPOMOD_IFFS')
      ipd2 = 0
      do i=1,MXSNV
        ipd2(:,i) = IFFS(:,i)
      enddo
      deallocate(IFFS)
      IFFS => ipd2
!
      return
      end subroutine RESIZE_MXSNV_DIPOMOD

      end module mod_com_dipomod
