
      module mod_par_tds

      use mod_dppr
      IMPLICIT NONE

!
!
!  This file provides various limitating parameters concerning the different programs in the package.
!  A short description as well as involved programs are given for each parameter.
!  Note: some program specific parameters are still defined in the related program source.
!
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
! Program stamped with (*) provides maximum value of the concerned parameter
! written in the 'debug.t' file.
!
!
! MXBRA  = Maximum number of branches.
!          In           -> mod_com_branch
!                          mod_com_trm
!                          spech
!                          spect
!
! MXDIMI = JC block maximum dimension for lower polyad.
!          Dependancies -> polyad and Jmax.
!          In           -> mod_com_dipmat
!                          mod_com_dipomat
!                          mod_com_trm
!                          mod_main_dipmat
!                          mod_main_trm
!                          dipmat (*)
!                          trm
!
! MXDIMS = JC block maximum dimension for upper polyad.
!          Dependancies -> polyad and Jmax.
!          In           -> mod_com_dipomat
!                          mod_com_hdic
!                          mod_com_hmatri
!                          mod_com_pgdh
!                          mod_com_rovbas
!                          mod_com_trm
!                          mod_main_dipmat
!                          mod_main_hdi
!                          mod_main_hmatri
!                          mod_main_rovbas
!                          mod_main_trm
!                          asfn
!                          asha
!                          asvp
!                          diago
!                          dipmat (*)
!                          dmspr
!                          hdi
!                          hmatri (*)
!                          rovbas
!                          trm
!
! MXELMD = Maximum number of matrix elements for all H operators per JC block.
!          Dependancies -> polyad, development order and Jmax.
!          In           -> mod_com_pgdh
!                          mod_main_hdi
!                          hdi    (*)
!
! MXELMH = Maximum number of matrix elements for one H operator per JC block.
!          Dependancies -> polyad, development order and Jmax.
!          In           -> mod_com_hmatri
!                          mod_main_hmatri
!                          asha
!                          hmatri (*)
!
! MXELMT = Maximum number of matrix elements for one transition moment operator
!                          for a given upper JC block.
!          Dependancies -> polyad, development order and Jmax.
!          In           -> mod_com_dipomat
!                          mod_com_trm
!                          mod_main_dipmat
!                          mod_main_trm
!                          asdi
!                          caldi
!                          dipmat (*)
!                          trm
!
! MXEMR  = Maximum number of vibrational reduced matrix elements.
!          Dependancies -> polyad, development order.
!          In           -> mod_com_dipomat
!                          mod_com_hmatri
!                          mod_com_model
!                          mod_main_dipmat
!                          mod_main_dipmod
!                          mod_main_hmatri
!                          mod_main_hmodel
!                          dipmat
!                          dipmod (*)
!                          hmatri
!                          hmodel (*)
!
! MXENI  = Maximum number of lower polyad energies.
!          Dependancies -> lower polyad and Jmax.
!          In           -> mod_com_tra
!                          mod_main_tra
!                          tra    (*)
!
! MXJ    = Jmax.
!          Dependancies -> MXJ should be < or = to MXJG-2 for Raman,
!                                               to MXJG-1 for infrared.
!          In           -> dipmat
!                          hdi
!                          hmatri
!
! MXJG   = MXJ+1
!
! MXNCR  = Maximum number of rotational codes for a JC block.
!          Dependancies -> polyad and Jmax.
!          In           -> mod_com_rovbas
!                          mod_main_rovbas
!                          rovbas (*)
!
! MXNIV  = Maximum number of vibrational levels for a given polyad.
!          In           -> mod_com_dipomod
!                          mod_com_hmodel
!                          mod_main_dipmod
!                          mod_main_hmodel
!                          dipmod (*)
!                          hmodel (*)
!
! MXOBS  = Maximum number of observed lines.
!
! MXOCV  = Maximum number of rovibrational operators for a given vibrational block.
!          Dependancies -> polyad and development order.
!          In           -> mod_com_model
!                          mod_main_dipmod
!                          mod_main_hmodel
!                          dipmod (*)
!                          hmodel (*)
!
! MXOPH  = Maximum number of operators in H.
!          Dependancies -> polyad and development order.
!          In           -> mod_com_hdic
!                          mod_com_hmatri
!                          mod_com_hmodel
!                          mod_com_pgdh
!                          mod_main_hdi
!                          mod_main_hmatri
!                          mod_main_hmodel
!                          hdi
!                          hmatri
!                          hmodel (*)
!
! MXOPR  = Maximum number of rovibrational operators for each vibrational operator.
!          Dependancies -> polyad and development order.
!          In           -> mod_com_model
!                          mod_main_dipmod
!                          mod_main_hmodel
!                          dipmod (*)
!                          hmodel (*)
!
! MXOPT  = Maximum number of transition operators.
!          Dependancies -> polyad and development order.
!          In           -> mod_com_dipomat
!                          mod_com_dipomod
!                          mod_com_pa
!                          mod_com_trm
!                          mod_main_dipmat
!                          mod_main_dipmod
!                          mod_main_trm
!                          dipmat
!                          dipmod (*)
!                          trm
!
! MXOPVH = Maximum number of vibrational operators (Hamiltonian).
!          Dependancies -> polyad and development order.
!          In           -> mod_com_hmatri
!                          mod_main_hmatri
!                          hmatri (*)
!                          hmodel (*)
!
! MXOPVT = Maximum number of vibrational operators (transition moment).
!          Dependancies -> polyad and development order.
!          In           -> mod_com_dipomat
!                          mod_main_dipmat
!                          dipmat (*)
!                          dipmod (*)
!
! MXSNB  = Maximum number of vibrational sublevels per band.
!          Dependancies -> polyad.
!          In           -> mod_com_sigvi
!                          mod_main_dipmod
!                          mod_main_hmodel
!                          dipmod (*)
!                          hmodel (*)
!
! MXSNV  = Maximum number of vibrational sublevels.
!          Dependancies -> polyad.
!          In           -> mod_com_dipomod
!                          mod_com_hdic
!                          mod_com_hmodel
!                          mod_com_jener
!                          mod_com_matri
!                          mod_com_rovbas
!                          mod_com_spech
!                          mod_com_tra
!                          mod_main_dipmat
!                          mod_main_dipmod
!                          mod_main_hdi
!                          mod_main_hmatri
!                          mod_main_hmodel
!                          mod_main_jener
!                          mod_main_rovbas
!                          mod_main_spech
!                          mod_main_tra
!                          asen
!                          dipmat (*)
!                          dipmod (*)
!                          hdi
!                          hmatri (*)
!                          hmodel (*)
!                          jener
!                          rovbas
!                          spech
!                          tra
!
! MXSNEV = Maximun number of vibronic sublevels.
!          Dependancies -> polyad.
!          In           -> mod_com_dipomod
!                          mod_com_hmodel
!                          mod_main_dipmod
!                          mod_main_hmodel
!                          dipmod (*)
!                          hmodel (*)
!
! MXSYM  = Maximum number of symmetries.
!          In           -> mod_com_dipmat        -> MDMJCI
!                          mod_com_spin
!                          mod_com_sy
!                          a21k
!                          asdi
!                          asfn
!                          asha
!                          asme
!                          dipmat
!                          hdi
!                          hmatri
!                          parchk
!                          rovbas
!                          tra
!                          trm
!
! MXSYR  = Maximum number of symmetries in intermediate group.
!          In           -> mod_com_trm           -> MDMJCI
!                          caldi
!                          dipmod
!                          hmatri
!                          hmodel
!                          kc
!                          rovbas
!                          trm
!
! MXPOL  = Maximum number of polyads.
!          In           -> mod_com_dipomod
!                          mod_com_hmodel
!                          dipmod
!                          hmodel
!
! MXGAM  = Maximum rotational degree.
!
! MXFAC  = size of the factorial table.
!          2*(MXJG+1)+(MXGAM+1)+1     (for instance: 2*200     +10       +1 = 411)
!          In           -> mod_com_fa
!                          facto
!
! NBAM   = number of angular momenta
!          In           -> ctrpmk
!                          parchk
!                          parmk
!
! NBCLAB = number of characters in the label defining a parameter.
!          In           -> asen
!                          astptr
!                          astran
!                          asvp
!                          ctrpmk
!                          dipmat
!                          hdi
!                          hmatri
!                          jener
!                          parchk
!                          parmk
!                          spect
!                          tra
!                          trm
!                          XTDS
!
! NBCTIT = number of characters of the TITRE string (parameter file header)
!          In           -> asdi
!                          asen
!                          asfn
!                          asha
!                          astptr
!                          astran
!                          asvp
!                          ctrpmk
!                          dipmat
!                          hdi
!                          hmatri
!                          jener
!                          parchk
!                          parmk
!                          rovbas
!                          spech
!                          spect
!                          tra
!                          trm
!
! NBVQN  = number of vibrationnal quantum numbers.
!          In           -> mod_com_dipomod
!                          mod_com_fp
!                          mod_com_hmodel
!                          mod_com_sigvi
!                          mod_com_spech
!                          dipmod
!                          e1to3
!                          e1to4
!                          e1to5
!                          e1to6
!                          hmodel
!                          parchk
!                          parmk
!                          spech
!                          XTDS
!
! MDMIGA = Maximum number of irreps in a product.
!          In           -> dipmod
!                          hmodel
!                          multciv
!                          rovbas
!
! MXNBES = Maximum number of spectral elements.
! MXNLF  = Maximum number of line files.
! MXNPV  = Maximum number of points in half Voigt profile (including center, should respect MXNPV = 101+10*I).
! MXNMI  = Maximum number of points in half apparatus function profile (2*MXNMI+1).
! MXNTRA = Maximum number of implied transitions for one spectrum point.
!          In           -> mod_com_simul
!                          mod_main_simul
!                          simul
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! MDMJCI
!          In           -> mod_com_dipmat        ->  3*MXSYM
!                          mod_com_trm           ->  5*MXSYR
!                          dipmat
!
! MDMIGR
!          In           -> caldi
!                          muc3vs
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! defined in mod_com_tri
!
! NBLMAX
!          In           -> mod_com_tri
!                          mod_main_tri
!                          tri
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
      integer ,parameter  :: MXBRA  = 5                                                            !  do NOT change
      integer ,save       :: MXDIMI = 1                                                            !  5              6
      integer ,save       :: MXDIMS = 1                                                            !  5              6
      integer ,save       :: MXELMD = 1                                                            !  76             84
      integer ,save       :: MXELMH = 1                                                            !  5              6
      integer ,save       :: MXELMT = 1                                                            !  13             14
      integer ,save       :: MXEMR  = 1                                                            !  4              5
      integer ,save       :: MXENI  = 1                                                            !  16             18
      integer ,parameter  :: MXJ    = 198                                                          !  do NOT change
      integer ,parameter  :: MXJG   = MXJ+1                                                        !  do NOT change
      integer ,save       :: MXNCR  = 1                                                            !  3              4
      integer ,save       :: MXNIV  = 1                                                            !  1              2
      integer ,save       :: MXOBS  = 1                                                            !
      integer ,save       :: MXOCV  = 1                                                            !  17             19
      integer ,save       :: MXOPH  = 1                                                            !  24             26
      integer ,save       :: MXOPR  = 1                                                            !  6              7
      integer ,save       :: MXOPT  = 1                                                            !  3              4
      integer ,save       :: MXOPVH = 1                                                            !  29             32
      integer ,save       :: MXOPVT = 1                                                            !  2              3
      integer ,save       :: MXSNB  = 1                                                            !  1              2
      integer ,save       :: MXSNV  = 1                                                            !  4              5
      integer ,save       :: MXSNEV = 1                                                            !  4              5
      integer ,parameter  :: MXSYM  = 3                                                            !  do NOT change
      integer ,parameter  :: MXSYR  = 50                                                           !  do NOT change
      integer ,parameter  :: MXPOL  = 10                                                           !  do NOT change
      integer ,parameter  :: MXGAM  = 9                                                            !  do NOT change
      integer ,parameter  :: MXFAC  = 2*(MXJG+1)+(MXGAM+1)+1                                       !  do NOT change
      integer ,parameter  :: NBAM   = 3                                                            !  do NOT change
      integer ,parameter  :: NBCLAB = 72                                                           !  do NOT change
      integer ,parameter  :: NBCTIT = 140                                                          !  do NOT change
      integer ,parameter  :: NBVQN  = 6                                                            !  do NOT change
      integer ,parameter  :: MDMIGA = 4                                                            !  do NOT change
      integer ,save       :: MXNBES = 1                                                            !                     2000000
      integer ,save       :: MXNLF  = 1                                                            !                     10
      integer ,save       :: MXNPV  = 111                                                          !  MXNPV = 101+10*I   1001
      integer ,save       :: MXNMI  = 1                                                            !                     500
      integer ,save       :: MXNTRA = 1                                                            !                     1000000
      integer ,parameter  :: MDMIGR = 3                                                            !  do NOT change
      integer ,save       :: NBLMAX = 1                                                            !                     MXOBS


      end module mod_par_tds
