
      module mod_alloc

      use mod_dppr
      IMPLICIT NONE

      real(kind=dppr) ,pointer ,dimension(:)        :: rpd1
      real(kind=dppr) ,pointer ,dimension(:,:)      :: rpd2
      real(kind=dppr) ,pointer ,dimension(:,:,:)    :: rpd3
      real(kind=dppr) ,pointer ,dimension(:,:,:,:)  :: rpd4

      integer         ,pointer ,dimension(:)        :: ipd1
      integer         ,pointer ,dimension(:,:)      :: ipd2
      integer         ,pointer ,dimension(:,:,:)    :: ipd3

      logical         ,pointer ,dimension(:)        :: lpd1


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ALLOCATION ERROR
!

      subroutine ERR_ALLOC(ierr,nomtab)
      IMPLICIT NONE

      integer             :: ierr

      character(len = *)  :: nomtab
!
8000  format(  ' !!! ERR_ALLOC : STOP ON ERROR')
8021  format(  ' !!! ERROR : ',i0,/,                &
               '     WHILE ALLOCATING ARRAY : ',a)
!
      print 8021, ierr,trim(nomtab)
      print 8000
      stop
!
      end subroutine ERR_ALLOC

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! RESIZE MX*
!

      function MXRES(MXC)
      IMPLICIT NONE
      integer  :: MXRES
      integer  :: MXC

! RESFAC = dynamic array resizing factor
      real(kind=dppr) ,parameter  :: RESFAC = 1.5

!
      MXRES = MXC*RESFAC+1
!
      RETURN
      end function MXRES

      end module mod_alloc
