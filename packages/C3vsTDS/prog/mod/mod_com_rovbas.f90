
      module mod_com_rovbas

      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      integer ,save  :: NB_SUP = 0
!
      integer ,pointer ,dimension(:) ,save  :: IAV,ICV,IKE,IMIN,NFSB                               ! (MXSNV)
      integer ,pointer ,dimension(:) ,save  :: NRCOD,NRVCOD,NVCOD                                  ! (MXDIMS)
      integer ,pointer ,dimension(:) ,save  :: NCR,NCRV                                            ! (MXNCR)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_ROVBAS
      IMPLICIT NONE

      integer  :: ierr

      allocate(IAV(MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_ROVBAS_IAV')
      IAV = 0
      allocate(ICV(MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_ROVBAS_ICV')
      ICV = 0
      allocate(IKE(MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_ROVBAS_IKE')
      IKE = 0
      allocate(IMIN(MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_ROVBAS_IMIN')
      IMIN = 0
      allocate(NFSB(MXSNV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_ROVBAS_NFSB')
      NFSB = 0
      allocate(NRCOD(MXDIMS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_ROVBAS_NRCOD')
      NRCOD = 0
      allocate(NRVCOD(MXDIMS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_ROVBAS_NRVCOD')
      NRVCOD = 0
      allocate(NVCOD(MXDIMS),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_ROVBAS_NVCOD')
      NVCOD = 0
      allocate(NCR(MXNCR),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_ROVBAS_NCR')
      NCR = 0
      allocate(NCRV(MXNCR),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_ROVBAS_NCRV')
      NCRV = 0
!
      return
      end subroutine ALLOC_ROVBAS

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXDIMS

      subroutine RESIZE_MXDIMS_ROVBAS(C_DIMS)
      IMPLICIT NONE
      integer :: C_DIMS

      integer :: ierr

! NRCOD
      allocate(ipd1(C_DIMS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXDIMS_ROVBAS_NRCOD')
      ipd1 = 0
      ipd1(1:MXDIMS) = NRCOD(:)
      deallocate(NRCOD)
      NRCOD => ipd1
! NRVCOD
      allocate(ipd1(C_DIMS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXDIMS_ROVBAS_NRVCOD')
      ipd1 = 0
      ipd1(1:MXDIMS) = NRVCOD(:)
      deallocate(NRVCOD)
      NRVCOD => ipd1
! NVCOD
      allocate(ipd1(C_DIMS),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXDIMS_ROVBAS_NVCOD')
      ipd1 = 0
      ipd1(1:MXDIMS) = NVCOD(:)
      deallocate(NVCOD)
      NVCOD => ipd1
!
      return
      end subroutine RESIZE_MXDIMS_ROVBAS

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXNCR

      subroutine RESIZE_MXNCR_ROVBAS(C_NCR)
      IMPLICIT NONE
      integer :: C_NCR

      integer :: ierr

! NCR
      allocate(ipd1(C_NCR),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXNCR_ROVBAS_NCR')
      ipd1 = 0
      ipd1(1:MXNCR) = NCR(:)
      deallocate(NCR)
      NCR => ipd1
! NCRV
      allocate(ipd1(C_NCR),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXNCR_ROVBAS_NCRV')
      ipd1 = 0
      ipd1(1:MXNCR) = NCRV(:)
      deallocate(NCRV)
      NCRV => ipd1
!
      return
      end subroutine RESIZE_MXNCR_ROVBAS

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXSNV

      subroutine RESIZE_MXSNV_ROVBAS(C_SNV)
      IMPLICIT NONE
      integer :: C_SNV

      integer :: ierr

! IAV
      allocate(ipd1(C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_ROVBAS_IAV')
      ipd1 = 0
      ipd1(1:MXSNV) = IAV(:)
      deallocate(IAV)
      IAV => ipd1
! ICV
      allocate(ipd1(C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_ROVBAS_ICV')
      ipd1 = 0
      ipd1(1:MXSNV) = ICV(:)
      deallocate(ICV)
      ICV => ipd1
! IKE
      allocate(ipd1(C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_ROVBAS_IKE')
      ipd1 = 0
      ipd1(1:MXSNV) = IKE(:)
      deallocate(IKE)
      IKE => ipd1
! IMIN
      allocate(ipd1(C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_ROVBAS_IMIN')
      ipd1 = 0
      ipd1(1:MXSNV) = IMIN(:)
      deallocate(IMIN)
      IMIN => ipd1
! NFSB
      allocate(ipd1(C_SNV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXSNV_ROVBAS_NFSB')
      ipd1 = 0
      ipd1(1:MXSNV) = NFSB(:)
      deallocate(NFSB)
      NFSB => ipd1
!
      return
      end subroutine RESIZE_MXSNV_ROVBAS

      end module mod_com_rovbas
