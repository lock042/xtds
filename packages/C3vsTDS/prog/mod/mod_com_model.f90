
      module mod_com_model

      use mod_dppr
      use mod_par_tds
      use mod_alloc
      IMPLICIT NONE

      real(kind=dppr) ,pointer ,dimension(:)     ,save  :: EMOP                                    ! (MXEMR)

      integer         ,pointer ,dimension(:)     ,save  :: IC,LI                                   ! (MXEMR)
      integer         ,pointer ,dimension(:)     ,save  :: IDELL,IDELS                             ! (MXOCV)
      integer         ,pointer ,dimension(:)     ,save  :: IDRO,IDSY,IDVA,IDVC,IDVI,IRDRE          ! (MXOCV)
      integer         ,pointer ,dimension(:)     ,save  :: IELCOL,IELCOS                           ! (MXOPR)
      integer         ,pointer ,dimension(:)     ,save  :: IROCO                                   ! (MXOPR)


      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ARRAY ALLOCATION
!
      subroutine ALLOC_MODEL
      IMPLICIT NONE

      integer  :: ierr

      allocate(EMOP(MXEMR),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_MODEL_EMOP')
      EMOP = 0.d0

      allocate(IC(MXEMR),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_MODEL_IC')
      IC = 0
      allocate(LI(MXEMR),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_MODEL_LI')
      LI = 0
      allocate(IDELL(MXOCV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_MODEL_IDELL')
      IDELL = 0
      allocate(IDELS(MXOCV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_MODEL_IDELS')
      IDELS = 0
      allocate(IDRO(MXOCV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_MODEL_IDRO')
      IDRO = 0
      allocate(IDSY(MXOCV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_MODEL_IDSY')
      IDSY = 0
      allocate(IDVA(MXOCV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_MODEL_IDVA')
      IDVA = 0
      allocate(IDVC(MXOCV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_MODEL_IDVC')
      IDVC = 0
      allocate(IDVI(MXOCV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_MODEL_IDVI')
      IDVI = 0
      allocate(IRDRE(MXOCV),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_MODEL_IRDRE')
      IRDRE = 0
      allocate(IELCOL(MXOPR),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_MODEL_IELCOL')
      IELCOL = 0
      allocate(IELCOS(MXOPR),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_MODEL_IELCOS')
      IELCOS = 0
      allocate(IROCO(MXOPR),stat=ierr)
      if( ierr /= 0 ) call err_alloc(ierr,'ALLOC_MODEL_IROCO')
      IROCO = 0
!
      return
      end subroutine ALLOC_MODEL

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXEMR

      subroutine RESIZE_MXEMR_MODEL(C_EMR)
      IMPLICIT NONE
      integer :: C_EMR

      integer :: ierr

! EMOP
      allocate(rpd1(C_EMR),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXEMR_MODEL_EMOP')
      rpd1 = 0.d0
      rpd1(1:MXEMR) = EMOP(:)
      deallocate(EMOP)
      EMOP => rpd1

! IC
      allocate(ipd1(C_EMR),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXEMR_MODEL_IC')
      ipd1 = 0
      ipd1(1:MXEMR) = IC(:)
      deallocate(IC)
      IC => ipd1
! LI
      allocate(ipd1(C_EMR),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXEMR_MODEL_LI')
      ipd1 = 0
      ipd1(1:MXEMR) = LI(:)
      deallocate(LI)
      LI => ipd1
!
      return
      end subroutine RESIZE_MXEMR_MODEL

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOCV

      subroutine RESIZE_MXOCV_MODEL(C_OCV)
      IMPLICIT NONE
      integer :: C_OCV

      integer :: ierr

! IDELL
      allocate(ipd1(C_OCV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOCV_MODEL_IDELL')
      ipd1 = 0
      ipd1(1:MXOCV) = IDELL(:)
      deallocate(IDELL)
      IDELL => ipd1
! IDELS
      allocate(ipd1(C_OCV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOCV_MODEL_IDELS')
      ipd1 = 0
      ipd1(1:MXOCV) = IDELS(:)
      deallocate(IDELS)
      IDELS => ipd1
! IDRO
      allocate(ipd1(C_OCV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOCV_MODEL_IDRO')
      ipd1 = 0
      ipd1(1:MXOCV) = IDRO(:)
      deallocate(IDRO)
      IDRO => ipd1
! IDSY
      allocate(ipd1(C_OCV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOCV_MODEL_IDSY')
      ipd1 = 0
      ipd1(1:MXOCV) = IDSY(:)
      deallocate(IDSY)
      IDSY => ipd1
! IDVA
      allocate(ipd1(C_OCV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOCV_MODEL_IDVA')
      ipd1 = 0
      ipd1(1:MXOCV) = IDVA(:)
      deallocate(IDVA)
      IDVA => ipd1
! IDVC
      allocate(ipd1(C_OCV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOCV_MODEL_IDVC')
      ipd1 = 0
      ipd1(1:MXOCV) = IDVC(:)
      deallocate(IDVC)
      IDVC => ipd1
! IDVI
      allocate(ipd1(C_OCV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOCV_MODEL_IDVI')
      ipd1 = 0
      ipd1(1:MXOCV) = IDVI(:)
      deallocate(IDVI)
      IDVI => ipd1
! IRDRE
      allocate(ipd1(C_OCV),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOCV_MODEL_IRDRE')
      ipd1 = 0
      ipd1(1:MXOCV) = IRDRE(:)
      deallocate(IRDRE)
      IRDRE => ipd1
!
      return
      end subroutine RESIZE_MXOCV_MODEL

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! MXOPR

      subroutine RESIZE_MXOPR_MODEL(C_OPR)
      IMPLICIT NONE
      integer :: C_OPR

      integer :: ierr

! IELCOL
      allocate(ipd1(C_OPR),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPR_MODEL_IELCOL')
      ipd1 = 0
      ipd1(1:MXOPR) = IELCOL(:)
      deallocate(IELCOL)
      IELCOL => ipd1
! IELCOS
      allocate(ipd1(C_OPR),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPR_MODEL_IELCOS')
      ipd1 = 0
      ipd1(1:MXOPR) = IELCOS(:)
      deallocate(IELCOS)
      IELCOS => ipd1
! IROCO
      allocate(ipd1(C_OPR),stat=ierr)
      if( ierr /= 0 ) call ERR_ALLOC(ierr,'RESIZE_MXOPR_MODEL_IROCO')
      ipd1 = 0
      ipd1(1:MXOPR) = IROCO(:)
      deallocate(IROCO)
      IROCO => ipd1
!
      return
      end subroutine RESIZE_MXOPR_MODEL

      end module mod_com_model
