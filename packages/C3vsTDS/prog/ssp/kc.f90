!
! *** DIMENSIONS, PARITES ET DESIGNATIONS DES REPRESENTATIONS DE TD
! MXSYR supposed to be <= 200
!
! returns KC
!         KCE
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
      FUNCTION KC(I)
      use mod_par_tds
      use mod_com_sy
      IMPLICIT NONE
      character(len=   3)  :: KC
      integer          :: I

!
1000  FORMAT(I3.3)
8000  FORMAT(' !!! WRONG CALLING INTEGER ARGUMENT IN KC: ',I6,/,   &
             ' !!! MUST BE IN [0,',I3,'] RANGE.'                )
8001  FORMAT(' !!! ERROR WHILE FORMATING KC: ',I6)
!
      IF( I .LT. 1     .OR.         &
          I .GT. MXSYR      ) THEN
        PRINT 8000, I,MXSYR
        STOP
      ENDIF
      IF( I .LE. LKCS ) THEN
        KC = LKC(I)
      ELSE
        WRITE(KC,1000,ERR=1) I
      ENDIF
      RETURN
!
1     PRINT 8001, I
      STOP
      END FUNCTION KC
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!
      FUNCTION KCE(I)
      use mod_par_tds
      use mod_com_sy
      IMPLICIT NONE
      character(len=   5)  :: KCE
      integer          :: I

! functions
      character(len=   3)  :: KC

      integer          :: J
!
1000  FORMAT(I3.3,'/2')
8000  FORMAT(' !!! WRONG CALLING INTEGER ARGUMENT IN KCE: ',I6,/,   &
             ' !!! MUST BE IN [0,',I3,'] RANGE.'                 )
8001  FORMAT(' !!! ERROR WHILE FORMATING KCE: ',I6)
!
      IF( I .LT. 1       .OR.         &
          I .GT. 2*MXSYR      ) THEN
        PRINT 8000, I,2*MXSYR
        STOP
      ENDIF
      IF( I .LE. MXSYR ) THEN
        KCE = KC(I)//'  '
      ELSE
        J = I-MXSYR
        IF( J .LE. LKCES ) THEN
          KCE = LKCE(J)
        ELSE
          WRITE(KCE,1000,ERR=1) J
        ENDIF
      ENDIF
      RETURN
!
1     PRINT 8001, J
      STOP
      END FUNCTION KCE
