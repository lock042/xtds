!
! 05/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3v.
!
!     E1TO4=<(IB3*IB4)//(IA3*IA4)//(IK3*IK4)>
!     IB3=0^{+}, IK3=0^{+}
!     IPM=+1 POUR UN OPERATEUR CREATION
!     IPM=-1 POUR UN OPERATEUR ANNIHILATION
!
      FUNCTION E1TO4(IPM,IB,IA,IK)
      use mod_dppr
      use mod_par_tds
      IMPLICIT NONE
      real(kind=dppr)  :: E1TO4
      integer ,dimension(NBVQN+1)  :: IB,IA,IK
      integer          :: IPM

! functions
      real(kind=dppr)  :: E1TO3,EMRVA
      integer          :: IDIMK

      real(kind=dppr)  :: DAL4,DBL4,DKL4
      real(kind=dppr)  :: EUF
      real(kind=dppr)  :: RK,R1TO3,R4

      integer          :: IAL3,IAV3,IBL3,IBV3,IKL3,IKV3
      integer          :: IAL4,IAV4,IBL4,IBV4,IKL4,IKV4
      integer          :: IFF
!
      E1TO4 = 0.D0
      CALL VLNC(IB(3),IBV3,IBL3)
      CALL VLNC(IA(3),IAV3,IAL3)
      CALL VLNC(IK(3),IKV3,IKL3)
      CALL VLNC(IB(4),IBV4,IBL4)
      CALL VLNC(IA(4),IAV4,IAL4)
      CALL VLNC(IK(4),IKV4,IKL4)
      DBL4 = DBLE(IDIMK(DBLE(IBL4)))
      DAL4 = DBLE(IDIMK(DBLE(IAL4)))
      DKL4 = DBLE(IDIMK(DBLE(IKL4)))
      RK   = SQRT(DBL4*DAL4*DKL4)
      CALL NF9K(DBLE(IBL3),DBLE(IBL4),DBLE(IBL4),DBLE(IAL3),DBLE(IAL4),  &
                DBLE(IAL4),DBLE(IKL3),DBLE(IKL4),DBLE(IKL4),EUF,IFF)
      R4    = EMRVA(2,IBV4,IBL4,IPM,IAV4,IAL4,IKV4,IKL4)
      R1TO3 = E1TO3(IPM,IB,IA,IK)
      E1TO4 = RK*EUF*R4*R1TO3
!
      RETURN
      END FUNCTION E1TO4
