!
! 05/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3v.
!
! ***** CE PROGRAMME DONNE LES COEFFICIENTS DE CLEBSCH-GORDAN ORIENTE DE CIV
!         DANS C3V
!
      SUBROUTINE FKCL(AK1,AK2,AK3,IC1,IC2,IC3,IL1,IL2,IL3,XK,IFF)
      use mod_dppr
      IMPLICIT NONE
      real(kind=dppr)  :: AK1,AK2,AK3,XK
      integer          :: IC1,IC2,IC3,IL1,IL2,IL3,IFF

! functions
      integer          :: CTR,KICTR

      real(kind=dppr)  :: H
      real(kind=dppr)  :: TRK
      real(kind=dppr)  :: V1,V2,V3
      real(kind=dppr)  :: Y

      integer          :: IP1,IP123K,IP2,IP3,IS1,IS2,IS3,ISI1,ISI2,ISI3,ISP1,ISP2,ISP3,ISS1,ISS2,ISS3,ITRK
!
      XK = 0.D0
      Y  = 0.D0
      IF( KICTR(AK1,IC1)   .NE. 1 .OR.           &
          KICTR(AK2,IC2)   .NE. 1 .OR.           &
          KICTR(AK3,IC3)   .NE. 1 .OR.           &
          CTR(IC1,IC2,IC3) .NE. 1      ) RETURN
      CALL LIMK(AK1,ISI1,ISS1,ISP1)
      DO IS1=ISI1,ISS1,ISP1
        CALL VKCS(ABS(AK1),AK1,IS1,IC1,IL1,V1,IP1)
        CALL LIMK(AK2,ISI2,ISS2,ISP2)
        DO IS2=ISI2,ISS2,ISP2
          CALL VKCS(ABS(AK2),AK2,IS2,IC2,IL2,V2,IP2)
          CALL LIMK(AK3,ISI3,ISS3,ISP3)
          DO IS3=ISI3,ISS3,ISP3
            CALL VKCS(ABS(AK3),AK3,IS3,IC3,IL3,V3,IP3)
            IF( IP3 .EQ. 1 ) V3 = -V3
            CALL TROIK(AK1,AK2,AK3,IS1,IS2,IS3,TRK,ITRK)
            H      = V1*V2*V3*TRK
            IFF    = 0
            IP123K = IP1+IP2+IP3+ITRK
            IF( IP123K .EQ. 1 .OR.             &
                IP123K .EQ. 3      ) IFF =  1
            IF( IP123K .EQ. 2 .OR.             &
                IP123K .EQ. 3      ) H   = -H
            Y = Y+H
          ENDDO
        ENDDO
      ENDDO
      XK = Y
!
      RETURN
      END SUBROUTINE FKCL
