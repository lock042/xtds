!
! 05/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3v.
!
      FUNCTION A23(IPM,VP,LP,K,V,L)
      use mod_dppr
      IMPLICIT NONE
      real(kind=dppr)  :: A23
      integer          :: IPM,VP,LP,K,V,L

! functions
      real(kind=dppr)  :: A21K,A22
      integer          :: IDIMK,IPSIK

      real(kind=dppr)  :: DK
      real(kind=dppr)  :: E
      real(kind=dppr)  :: SIX

      integer          :: ICP,IDP,IPP,ISIX
      integer          :: KS
      integer          :: LS
      integer          :: VS
!
      DK  = IDIMK(K)
      A23 = 0.D0
      IF( IPSIK(2,3,K) .EQ. 0 ) RETURN
      IF( IPM*(VP-V)   .NE. 3 ) RETURN
      VS = V+IPM
      IF( L .LE. 0 ) THEN
        ICP = L
        IDP = L
        IPP = 1
      ELSE
        ICP = ABS(L-1)
        IDP = L+1
        IPP = 2*L
      ENDIF
E2:   DO LS=ICP,IDP,IPP
        IF( IPSIK(2,VS,LS) .EQ. 0 ) CYCLE E2
        KS = K-1
        E  = A22(IPM,VP,LP,KS,VS,LS)*A21K(IPM,VS,LS,V,L)
        IF( E .EQ. 0.D0 ) CYCLE E2
        CALL SIX6K(DBLE(KS),3.D0,DBLE(K),DBLE(L),DBLE(LP),DBLE(LS),SIX,ISIX)
        A23 = A23+( (-1)**(L+LP+KS) )*SIX*E
      ENDDO E2
      A23 = A23*SQRT(DK)
!
      RETURN
      END FUNCTION A23
