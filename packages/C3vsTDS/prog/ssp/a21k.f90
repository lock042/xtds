!
! 05/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3v.
!
! ELEMENTS MATRICIELS REDUITS DES OPERATEURS ELEMENTAIRES A1(1,E) ET
! A+1(1,E) RELATIFS A L'OSCILLATEUR DOUBLEMENT DEGENERE.
!     IPM=+1 POUR UN OPERATEUR CREATION
!     IPM=-1 POUR UN OPERATEUR ANNIHILATION
!
      FUNCTION A21K(IPM,VVP,LLP,VV,LL)
      use mod_dppr
      use mod_par_tds
      IMPLICIT NONE
      real(kind=dppr)  :: A21K
      integer          :: IPM,VVP,LLP,VV,LL

! functions
      real(kind=dppr)  :: A21,DIMIC
      integer          :: IDIMK

      real(kind=dppr)  :: EMA21
      real(kind=dppr)  :: DCP,DLP
      real(kind=dppr)  :: XK

      integer          :: CC,CCP
      integer          :: IFK
!
      A21K = 0.D0
      DO CC=1,MXSYM
        DO CCP=1,MXSYM
          CALL KIV3C(1.D0,DBLE(LL),DBLE(LLP),3,CC,CCP,XK,IFK)
          IF( XK .NE. 0.D0 ) GOTO 20
        ENDDO
      ENDDO
      RETURN
!
20    EMA21 = A21(IPM,VVP,LLP,CCP,VV,LL,CC)
      DCP   = DIMIC(CCP)
      DLP   = DBLE(IDIMK(DBLE(LLP)))
      A21K  = SQRT(DLP/DCP)*(EMA21/XK)
!
      RETURN
      END FUNCTION A21K
