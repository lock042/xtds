!
! 07/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3vs.
!
!      CE PROGRAMME DONNE LES COEFFICIENTS DE CLEBSCH-GORDAN ORIENTE  CIV
!      J'ASSOCIE FORMELLEMENT LA VALEUR (-1) A L IRREP 0-, PUIS 0 A 0+.
!
      SUBROUTINE FJKS(AJ1,AK1,IS1,AJ2,AK2,IS2,AJ3,AK3,IS3,FJK,IFJ)
      use mod_dppr
      IMPLICIT NONE
      real(kind=dppr)  :: AJ1,AK1,AJ2,AK2,AJ3,AK3,FJK
      integer          :: IS1,IS2,IS3,IFJ

! functions
      real(kind=dppr)  :: TROIJD
      integer          :: KTR,NTRD

      real(kind=dppr)  :: AM1,AM2,AM3
      real(kind=dppr)  :: C
      real(kind=dppr)  :: F
      real(kind=dppr)  :: H,H1,H2,H3
      real(kind=dppr)  :: PH

      integer          :: IP1,IP2,IP3
      integer          :: K1D,K1P,K2D,K2P,K3D,K3P
      integer          :: M1D,M2D,M3D
!
      FJK = 0.D0
      IF( AK1 .LE. 0.D0 ) THEN
        K1D = 0
        K1P = 1
      ELSE
        K1D = NINT(2.D0*AK1)
        K1P = 2*K1D
      ENDIF
      IF( AK2 .LE. 0.D0 ) THEN
        K2D = 0
        K2P = 1
      ELSE
        K2D = NINT(2.D0*AK2)
        K2P = 2*K2D
      ENDIF
      IF( AK3 .LE. 0.D0 ) THEN
        K3D = 0
        K3P = 1
      ELSE
        K3D = NINT(2.D0*AK3)
        K3P = 2*K3D
      ENDIF
      IF( AK1 .LE. AJ1 .AND.         &
          AK2 .LE. AJ2 .AND.         &
          AK3 .LE. AJ3       ) THEN
        IF( NTRD(AJ1,AJ2,AJ3) .NE. 0 .AND.         &
            KTR(AK1,AK2,AK3)  .NE. 0       ) THEN
          F = 0.D0
          DO M1D=-K1D,K1D,K1P
            AM1 = DBLE(M1D)/2.D0
            CALL WJKMS(AJ1,AK1,AM1,IS1,H1,IP1)
            DO M2D=-K2D,K2D,K2P
              AM2 = DBLE(M2D)/2.D0
              CALL WJKMS(AJ2,AK2,AM2,IS2,H2,IP2)
              DO M3D=-K3D,K3D,K3P
                AM3 = DBLE(M3D)/2.D0
                CALL WJKMS(AJ3,AK3,AM3,IS3,H3,IP3)
                IF( IP3 .EQ. 1 ) H3 = -H3
                PH  = SQRT(2.D0*AJ3+1.D0)*( (-1)**NINT(AJ2-AJ1-AM3) )
                C   = PH*TROIJD(AJ1,AJ2,AJ3,AM1,AM2,-AM3)
                H   = H1*H2*H3*C
                IFJ = 0
                IF( IP1+IP2+IP3 .EQ. 2 .OR.             &
                    IP1+IP2+IP3 .EQ. 3      ) H   = -H
                F = F+H
                IF( IP1+IP2+IP3 .EQ. 1 .OR.             &
                    IP1+IP2+IP3 .EQ. 3      ) IFJ =  1
              ENDDO
            ENDDO
          ENDDO
          FJK = F
        ENDIF
      ENDIF
!
      RETURN
      END SUBROUTINE FJKS
