!
! 05/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3v.
!
      FUNCTION EPSJK2(AJ1,AK1,AJ2,AK2,AJ3,AK3)
      use mod_dppr
      IMPLICIT NONE
      real(kind=dppr)  :: EPSJK2
      real(kind=dppr)  :: AJ1,AK1,AJ2,AK2,AJ3,AK3

      real(kind=dppr)  :: AP1,AP2,AP3
!
      IF( AK1 .EQ. -1.D0 ) THEN
        AP1 = AJ1
      ELSE
        AP1 = AK1
      ENDIF
      IF( AK2 .EQ. -1.D0 ) THEN
        AP2 = AJ2
      ELSE
        AP2 = AK2
      ENDIF
      IF( AK3 .EQ. -1.D0 ) THEN
        AP3 = AJ3
      ELSE
        AP3 = AK3
      ENDIF
      EPSJK2 = (-1)**NINT(AP1+AP2+AP3)
!
      RETURN
      END FUNCTION EPSJK2
