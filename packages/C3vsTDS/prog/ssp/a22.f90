!
! 05/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3v.
!
      FUNCTION A22(IPM,VP,LP,K,V,L)
      use mod_dppr
      IMPLICIT NONE
      real(kind=dppr)  :: A22
      integer          :: IPM,VP,LP,K,V,L

! functions
      real(kind=dppr)  :: A21K
      integer          :: IDIMK,IPSIK

      real(kind=dppr)  :: DK
      real(kind=dppr)  :: E
      real(kind=dppr)  :: SIX

      integer          :: ICP,IDP,IPP,ISIX
      integer          :: LPP
      integer          :: VPP
!
      A22 = 0.D0
      DK  = SQRT(DBLE(IDIMK(DBLE(K))))
      IF( K          .NE. 0 .AND.           &
          K          .NE. 2       ) RETURN
      IF( IPM*(VP-V) .NE. 2       ) RETURN
      VPP = V+IPM
      IF( L .LE. 0 ) THEN
        ICP = LPP
        IDP = LPP
        IPP = 1
      ELSE
        ICP = LPP-1
        IDP = LPP+1
        IPP = 2
      ENDIF
      DO LPP=ICP,IDP,IPP
        IF( IPSIK(2,VPP,LPP) .EQ. 0 ) RETURN
        CALL SIX6K(1.D0,1.D0,DBLE(K),DBLE(L),DBLE(LP),DBLE(LPP),SIX,ISIX)
        E   = A21K(IPM,VP,LP,VPP,LPP)*A21K(IPM,VPP,LPP,V,L)
        A22 = A22+((-1)**(LP+L))*DK*E*SIX
      ENDDO
!
      RETURN
      END FUNCTION A22
