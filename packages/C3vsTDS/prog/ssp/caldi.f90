!
! ***  CALCULE LES ELEM. MAT. DES OP. DU M. DIP.
!
!   < INF||� || sup>
!
! 07/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3vs.
!       MOLECULES SYMETRIQUES DANS UN ETAT ELECTR. DEGENERE
!
      SUBROUTINE CALDI(AJS,ICS,NFBS,AJI,ICI,NFBI,ICOR,ICOL,ICOS,   &
                       IGEV,IGREV,KCC,IGREVC                         )
      use mod_dppr
      use mod_par_tds
      use mod_main_dipmat
      IMPLICIT NONE
      real(kind=dppr)  :: AJS,AJI
      integer          :: ICS,NFBS,ICI,NFBI,ICOR,ICOL,ICOS,IGEV,IGREV,KCC,IGREVC

! functions
      integer          :: IDIMK

      real(kind=dppr) , dimension(MDMIGR)  ::  ACREV,BCREV
      real(kind=dppr)  :: A1,A2,A3,A4,A5,A6,A7,A8,AKRII,AKRIS,AKRVII,AKRVIS
      real(kind=dppr)  :: B1,B2,B3,B4,B5,B6,B8,B9
      real(kind=dppr)  :: COEF
      real(kind=dppr)  :: ELM
      real(kind=dppr)  :: PH,PHASE,PHASE1,PHASE2,PHI,PHS,PH1,PH12,PH2,P1,P2
      real(kind=dppr)  :: SIXK,SXK
      real(kind=dppr)  :: TERM1,TERM2
      real(kind=dppr)  :: XK,XK1,XK2,XK3,XK4
      real(kind=dppr)  :: ZK1,ZK2,ZK3,ZK4

      integer          :: IAVI,IAVS,IBRA,IF1,IF2,IFK,IK,IK1,IK2,IK3,IK4,IKEVI,IKL,IG,IJ,IKET
      integer          :: IGE,IS,IKEI,IKRI,IKREVI,IKES,IKEVS,IKRS,IKREVS,IX,IKR
      integer          :: IL,ILL,ILS,IO,IP1,IP2,ISXK,ISIXK,IZ,I1,I2
      integer          :: KR
      integer          :: NK,NN
!
! IO = DEGRE OP. ROTATIONNEL
!
      IO = ICOR/10000
!
! KR = RANG OP. ROTATIONNEL
!
      KR = ICOR/1000-(ICOR/10000)*10
!
! IG = SYMETRIE OP. ROTATIONNEL
!
      IG = ICOR/100-(ICOR/1000)*10
!
! IGE SYMETRIE ELECTRONIQUE TOTALE ( ORBIT+SPIN )
!
      IGE = ICOR-10*(ICOR/10)
!
! IGE SYMETRIE VIBRONIQUE
!
      IGEV = ICOR/10-10*(ICOR/100)
! PARTIE ELECTRONIQUE DE SPIN
      IS  = ICOS/100
      ILS = ICOS-(ICOS/10)*10
! PARTIE ELECTRONIQUE ORBITALE
      IL    = ICOL/100
      ILL   = ICOL-(ICOL/10)*10
      NBELM = 0
!
! POLYADE INFERIEURE
!
E3:   DO IBRA=1,NFBI
        I1     = NVCODI(IBRA)/1000000
        IAVI   = NVCODI(IBRA)-100*(NVCODI(IBRA)/100)
        IKEI   = NVCODI(IBRA)/10000-100*(NVCODI(IBRA)/1000000)
        IKEVI  = NVCODI(IBRA)/100-100*(NVCODI(IBRA)/10000)
        IKRI   = NRCODI(IBRA)
        IKREVI = NRVCDI(IBRA)

!
! POLYADE SUPERIEURE
!
E4:     DO IKET=1,NFBS
          I2 = NVCODS(IKET)/1000000
          IF( EMRD(I2,I1) .EQ. 0 ) CYCLE E4
          IAVS   = NVCODS(IKET)-100*(NVCODS(IKET)/100)
          IKES   = NVCODS(IKET)/10000-100*(NVCODS(IKET)/1000000)
          IKEVS  = NVCODS(IKET)/100-100*(NVCODS(IKET)/10000)
          IKRS   = NRCODS(IKET)
          IKREVS = NRVCDS(IKET)
          CALL KIV3C(DBLE(IGREVC-2),DBLE(IKREVS-2),DBLE(IKREVI-2),2,ICS,ICI,XK,IFK)
          IX    = KCC+IGREV+IKREVI+IKREVS
          PH    = (-1)**IX
          P1    = DBLE(IDIMK(DBLE(IGREVC-2)))
          P2    = DBLE(IDIMK(DBLE(IKREVI-2)))
          PH12  = SQRT(P1/P2)
          PHASE = PH*PH12*XK
!
! PREMIERE SOMMATION
!
          TERM1 = 0.D0
          IP1   = 0
          PHI   = 1.D0
E5:       DO IKR=1,INT(2.D0*AJS),2
            AKRII = DBLE(IKR)/2.D0
            CALL MULCIV(DBLE(IGREV-2),DBLE(IKREVS-2),NN,ACREV)
E6:         DO IJ=1,NN
              AKRVII = ACREV(IJ)
              CALL SIX6K(DBLE(KCC-2),DBLE(IGREV-2),DBLE(IGREVC-2),   &
                         DBLE(IKREVS-2),DBLE(IKREVI-2),AKRVII,       &
                         SIXK,ISIXK                               )
              CALL KSU2CV(1.D0,AJS,AJI,DBLE(KCC-2),AKRII,DBLE(IKRI)-0.5,XK1,IF1)
              CALL KSU2CV(DBLE(KR),AJS,AJS,DBLE(IG-2),DBLE(IKRS)-0.5,AKRII,XK2,IF2)
              A1 = DBLE(IDIMK(DBLE(IKREVI-2)))
              A2 = DBLE(IDIMK(AKRVII))
              A3 = DBLE(IDIMK(DBLE(KCC-2)))
              A4 = DBLE(IDIMK(DBLE(IKEVI)-(MXSYR+0.5D0)))
              A5 = DBLE(IDIMK(DBLE(IGREV-2)))
              A6 = DBLE(IDIMK(DBLE(IKREVS-2)))
              A7 = DBLE(IDIMK(DBLE(IKRI)-0.5))
              A8 = DBLE(IDIMK(AKRII))
              CALL NF9K(DBLE(IKRI)-0.5D0,DBLE(IKEVI)-(MXSYR+0.5D0),DBLE(IKREVI-2),   &
                        DBLE(KCC-2)     ,0.D0                     ,DBLE(KCC-2)   ,   &
                        AKRII           ,DBLE(IKEVI)-(MXSYR+0.5D0),AKRVII        ,   &
                        XK3             ,IK1                                      )
              CALL NF9K(AKRII           ,DBLE(IKEVI)-(MXSYR+0.5D0),AKRVII        ,   &
                        DBLE(IG-2)      ,DBLE(IGEV-2)             ,DBLE(IGREV-2) ,   &
                        DBLE(IKRS)-0.5D0,DBLE(IKEVS)-(MXSYR+0.5D0),DBLE(IKREVS-2),   &
                        XK4             ,IK2                                      )
              PH1    = SQRT((2.D0*AJI+1.D0)*(2.D0*AJS+1.D0))
              PHASE1 = A2*SQRT(A1*A3*A4*A5*A6*A7*A8)/PH1
              IP1    = ISIXK+IF1+IF2+IK1+IK2
              IF( IP1 .EQ. 2 .OR.               &
                  IP1 .EQ. 3      ) PHI = -PHI
              TERM1 = TERM1+PHI*SIXK*XK1*XK2*XK3*XK4*PHASE1
            ENDDO E6
          ENDDO E5
!
! DEUXIEME SOMMATION
!
          TERM2 = 0.D0
          PHI   = 1.D0
          IP2   = 1
E7:       DO IKL=1,INT(2.D0*AJI),2
            AKRIS = DBLE(IKL)/2.D0
            CALL MULCIV(DBLE(KCC-2),DBLE(IKREVS-2),NK,BCREV)
E8:         DO IK=1,NK
              AKRVIS = BCREV(IK)
              IZ     = KCC+IGREV+IGREVC
              PHS    = (-1)**IZ
              CALL SIX6K(DBLE(IGREV-2),DBLE(KCC-2),DBLE(IGREVC-2),   &
                         DBLE(IKREVS-2),DBLE(IKREVI-2),AKRVIS,       &
                         SXK,ISXK                                 )
              CALL KSU2CV(1.D0,AJS,AJI,DBLE(KCC-2),DBLE(IKRS)-0.5,AKRIS,ZK1,IK1)
              CALL KSU2CV(DBLE(KR),AJI,AJI,DBLE(IG-2),AKRIS,DBLE(IKRI)-0.5,ZK2,IK2)
! LES ELEMENTS DE OPERATEUR DIPOLAIRE
              B1 = DBLE(IDIMK(AKRVIS))
              B2 = DBLE(IDIMK(DBLE(IKREVS-2)))
              B3 = DBLE(IDIMK(DBLE(KCC-2)))
              B4 = DBLE(IDIMK(DBLE(IKEVI)-(MXSYR+0.5D0)))
              B5 = DBLE(IDIMK(DBLE(IKREVI-2)))
              B6 = DBLE(IDIMK(DBLE(IGREV-2)))
              B8 = DBLE(IDIMK(DBLE(IKRI)-0.5))
              B9 = DBLE(IDIMK(AKRIS))
              CALL NF9K(AKRIS         ,DBLE(IKEVS)-(MXSYR+0.5D0),AKRVIS     ,      &
                        DBLE(KCC-2)   ,0.D0                     ,DBLE(KCC-2),      &
                        DBLE(IKRS)-0.5,DBLE(IKEVS)-(MXSYR+0.5D0),DBLE(IKREVS-2),   &
                        ZK3           ,IK3                                      )
              CALL NF9K(DBLE(IKRI)-0.5,DBLE(IKEVI)-(MXSYR+0.5D0),DBLE(IKREVI-2),   &
                        DBLE(IG-2)    ,DBLE(IGEV-2)             ,DBLE(IGREV-2) ,   &
                        AKRIS         ,DBLE(IKEVS)-(MXSYR+0.5D0),AKRVIS        ,   &
                        ZK4,IK4                                                 )
              PH2    = 2.D0*AJI+1.D0
              PHASE2 = B1*SQRT(B2*B3*B4*B5*B6*B8*B9)/PH2
              IP2    = ISXK+IK1+IK2+IK3+IK4
              IF( IP2 .EQ. 2 .OR.               &
                  IP2 .EQ. 3      ) PHI = -PHI
              TERM2 = TERM2+PHI*PHS*SXK*ZK1*ZK2*ZK3*ZK4*PHASE2
            ENDDO E8
          ENDDO E7
!
! SOMME DES 2 TERMES
!
          COEF = PHASE*EMRD(I2,I1)/6.D0
          ELM  = (TERM1+TERM2)*COEF*SQRT((2.D0*AJI+1.D0)*(2.D0*AJS+1.D0))
          NBELM = NBELM+1
          IF( NBELM .GT. MXELMT ) CALL RESIZE_MXELMT
          LI(NBELM) = IBRA
          KO(NBELM) = IKET
          H(NBELM)  = ELM
        ENDDO E4
      ENDDO E3
!
      RETURN
      END SUBROUTINE CALDI
