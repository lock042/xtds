!
! 05/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3v.
!
!  CE PROGRAMME CALCULE LES COEFFICIENTS ISOSCALAIRES K
!  DANS LA CHAINE CIV_C3V
!
      SUBROUTINE KIV3C(AK1,AK2,AK3,IC1,IC2,IC3,XK,IFK)
      use mod_dppr
      IMPLICIT NONE
      real(kind=dppr)  :: AK1,AK2,AK3,XK
      integer          :: IC1,IC2,IC3,IFK

! functions
      integer          :: KICTR

      real(kind=dppr)  :: TRC
      real(kind=dppr)  :: Y

      integer          :: IL1,ILI1,ILP1,ILS1
      integer          :: IL2,ILI2,ILP2,ILS2
      integer          :: IL3,ILI3,ILP3,ILS3
      integer          :: IFF,ITRC
!
      XK  = 0.D0
      IFK = 0
      IF( KICTR(AK1,IC1)*KICTR(AK2,IC2)*KICTR(AK3,IC3) .EQ. 0 ) RETURN
      CALL LIMC(IC1,ILI1,ILS1,ILP1)
      DO IL1=ILI1,ILS1,ILP1
        CALL LIMC(IC2,ILI2,ILS2,ILP2)
        DO IL2=ILI2,ILS2,ILP2
          CALL LIMC(IC3,ILI3,ILS3,ILP3)
E30:      DO IL3=ILI3,ILS3,ILP3
            CALL FKCL(AK1,AK2,AK3,IC1,IC2,IC3,IL1,IL2,IL3,Y,IFF)
            CALL TROIC(IC1,IC2,IC3,IL1,IL2,IL3,TRC,ITRC)
            IF( TRC*Y .EQ. 0.D0 ) CYCLE E30
            XK  = Y/TRC
            IFK = 0
            IF( IFF+ITRC .EQ. 1       ) IFK =   1
            IF( ITRC     .EQ. 1 .AND.              &
                IFF      .EQ. 0       ) XK  = -XK
            RETURN
          ENDDO E30
        ENDDO
      ENDDO
!
      RETURN
      END SUBROUTINE KIV3C
