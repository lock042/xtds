!
! 05/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3v.
!
!CCCCC   CE PROGRAMME VERIFIE LES REGLES DE REDUCTION DE CIV DANS C3V
!
      FUNCTION KICTR(AK,IC)
      use mod_dppr
      IMPLICIT NONE
      integer          :: KICTR
      real(kind=dppr)  :: AK
      integer          :: IC

      real(kind=dppr)  :: AKM

      integer          :: KD,KM,KM3
!
      KICTR = 0
      IF( AK .EQ.  0.D0 .AND.              &
          IC .EQ.  1          ) KICTR = 1
      IF( AK .EQ. -1.D0 .AND.              &
          IC .EQ.  2          ) KICTR = 1
      IF( AK .GT.  0.D0 ) THEN
        IF    ( AK .GT. INT(AK) ) THEN
          KD  = NINT(2.D0*AK)
          KM  = MOD(KD,6)
          AKM = KM/2.D0
          IF( AKM .EQ. 2.5D0 .AND.              &
              IC  .EQ. 6           ) KICTR = 1
          IF( AKM .EQ. 0.5D0 .AND.              &
              IC  .EQ. 6           ) KICTR = 1
          IF( AKM .EQ. 1.5D0 .AND.              &
              IC  .EQ. 4           ) KICTR = 1
          IF( AKM .EQ. 1.5D0 .AND.              &
              IC  .EQ. 5           ) KICTR = 1
        ELSEIF( AK .EQ. INT(AK) ) THEN
          KM3 = MOD(INT(AK),3)
          IF( KM3 .EQ. 1 .AND.              &
              IC  .EQ. 3       ) KICTR = 1
          IF( KM3 .EQ. 2 .AND.              &
              IC  .EQ. 3       ) KICTR = 1
          IF( KM3 .EQ. 0 .AND.              &
              IC  .EQ. 1       ) KICTR = 1
          IF( KM3 .EQ. 0 .AND.              &
              IC  .EQ. 2       ) KICTR = 1
        ENDIF
      ENDIF
!
      RETURN
      END FUNCTION KICTR
