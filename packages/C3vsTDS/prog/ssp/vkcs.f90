!
! 05/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3v.
!
! CE PROGRAMME DONNE LES COEFFFICIENTS DE LA MATRICE V POUR J ENTIER ET DEMI-ENTIER
!         IS
!      + ---->1
!      - ---->2
!
      SUBROUTINE VKCS(AJ,AK,IS,IC,IL,V,IP)
      use mod_dppr
      IMPLICIT NONE
      real(kind=dppr)  :: AJ,AK,V
      integer          :: IS,IC,IL,IP

      real(kind=dppr)  :: AKM
      real(kind=dppr)  :: PHS

      integer          :: IMP,IPJ,IPK
      integer          :: JP
      integer          :: KD,KM,KM6,KP
      integer          :: MP

!
      V  = 0.D0
      IP = 0
      IF( AK .LE.  0.D0 ) IS = 1
      IF( IC .EQ.  1    ) IL = 1
      IF( IC .EQ.  2    ) IL = 2
      IF( IC .EQ.  4    ) IL = 1
      IF( IC .EQ.  5    ) IL = 2
      IF( AJ .EQ.  0.D0 .AND.             &
          AK .EQ.  0.D0 .AND.             &
          IC .EQ.  1          ) V = 1.D0
      IF( AJ .EQ.  1.D0 .AND.             &
          AK .EQ. -1.D0 .AND.             &
          IC .EQ.  2          ) V = 1.D0
      IPJ = INT(2.*AJ)-2*(INT(2.*AJ)/2)
      IPK = INT(2.*AK)-2*(INT(2.*AK)/2)
      IF( AJ  .LT. AK  ) RETURN
      IF( IPJ .NE. IPK ) RETURN
      IF( IPJ .EQ. IPK .AND.            &
          IPK .EQ. 1         ) GOTO 10
      IF( IPJ .EQ. IPK .AND.            &
          IPK .EQ. 0         ) GOTO 20
10    MP  = INT(AJ+0.5D0)
      IMP = MP-(MP/2)*2
      KD  = INT(2*AK)
      KM  = MOD(KD,6)
      AKM = KM/2.D0
      PHS = 1.D0/SQRT(2.D0)
      IF( IMP .EQ. 1 ) THEN
!
!CC LE BLOC K=5/2 MODULO 3
!
        IF( AKM .EQ. 2.5D0 .AND.              &
            IS  .EQ. 1     .AND.              &
            IC  .EQ. 6     .AND.              &
            IL  .EQ. 1           ) V  =  PHS
        IF( AKM .EQ. 2.5D0 .AND.              &
            IS  .EQ. 1     .AND.              &
            IC  .EQ. 6     .AND.              &
            IL  .EQ. 2           ) V  =  PHS
        IF( AKM .EQ. 2.5D0 .AND.              &
            IS  .EQ. 2     .AND.              &
            IC  .EQ. 6     .AND.              &
            IL  .EQ. 1           ) V  =  PHS
        IF( AKM .EQ. 2.5D0 .AND.              &
            IS  .EQ. 2     .AND.              &
            IC  .EQ. 6     .AND.              &
            IL  .EQ. 2           ) V  =  PHS
        IF( AKM .EQ. 2.5D0 .AND.              &
            IS  .EQ. 1     .AND.              &
            IC  .EQ. 6     .AND.              &
            IL  .EQ. 2           ) IP =  1
        IF( AKM .EQ. 2.5D0 .AND.              &
            IS  .EQ. 2     .AND.              &
            IC  .EQ. 6     .AND.              &
            IL  .EQ. 1           ) IP =  1
!
!CC LE BLOC K=3/2 MODULO 3
!
        IF( AKM .EQ. 1.5D0 .AND.              &
            IS  .EQ. 1     .AND.              &
            IC  .EQ. 4     .AND.              &
            IL  .EQ. 1           ) V  =  PHS
        IF( AKM .EQ. 1.5D0 .AND.              &
            IS  .EQ. 2     .AND.              &
            IC  .EQ. 4     .AND.              &
            IL  .EQ. 1           ) V  = -PHS
        IF( AKM .EQ. 1.5D0 .AND.              &
            IS  .EQ. 1     .AND.              &
            IC  .EQ. 5     .AND.              &
            IL  .EQ. 2           ) V  = -PHS
        IF( AKM .EQ. 1.5D0 .AND.              &
            IS  .EQ. 2     .AND.              &
            IC  .EQ. 5     .AND.              &
            IL  .EQ. 2           ) V  =  PHS
        IF( AKM .EQ. 1.5D0 .AND.              &
            IS  .EQ. 1     .AND.              &
            IC  .EQ. 5     .AND.              &
            IL  .EQ. 2           ) IP =  1
        IF( AKM .EQ. 1.5D0 .AND.              &
            IS  .EQ. 2     .AND.              &
            IC  .EQ. 4     .AND.              &
            IL  .EQ. 1           ) IP =  1
!
!CC LE BLOC K=1/2 MODULO 3
!
        IF( AKM .EQ. 0.5D0 .AND.              &
            IS  .EQ. 1     .AND.              &
            IC  .EQ. 6     .AND.              &
            IL  .EQ. 1           ) V  =  PHS
        IF( AKM .EQ. 0.5D0 .AND.              &
            IS  .EQ. 1     .AND.              &
            IC  .EQ. 6     .AND.              &
            IL  .EQ. 2           ) V  =  PHS
        IF( AKM .EQ. 0.5D0 .AND.              &
            IS  .EQ. 2     .AND.              &
            IC  .EQ. 6     .AND.              &
            IL  .EQ. 1           ) V  = -PHS
        IF( AKM .EQ. 0.5D0 .AND.              &
            IS  .EQ. 2     .AND.              &
            IC  .EQ. 6     .AND.              &
            IL  .EQ. 2           ) V  = -PHS
        IF( AKM .EQ. 0.5D0 .AND.              &
            IS  .EQ. 1     .AND.              &
            IC  .EQ. 6     .AND.              &
            IL  .EQ. 1           ) IP =  1
        IF( AKM .EQ. 0.5D0 .AND.              &
            IS  .EQ. 2     .AND.              &
            IC  .EQ. 6     .AND.              &
            IL  .EQ. 2           ) IP =  1
      ELSE
!
!CC LE BLOC K=5/2 MODULO 3
!
        IF( AKM .EQ. 2.5D0 .AND.              &
            IS  .EQ. 1     .AND.              &
            IC  .EQ. 6     .AND.              &
            IL  .EQ. 1           ) V  =  PHS
        IF( AKM .EQ. 2.5D0 .AND.              &
            IS  .EQ. 1     .AND.              &
            IC  .EQ. 6     .AND.              &
            IL  .EQ. 2           ) V  = -PHS
        IF( AKM .EQ. 2.5D0 .AND.              &
            IS  .EQ. 2     .AND.              &
            IC  .EQ. 6     .AND.              &
            IL  .EQ. 1           ) V  = -PHS
        IF( AKM .EQ. 2.5D0 .AND.              &
            IS  .EQ. 2     .AND.              &
            IC  .EQ. 6     .AND.              &
            IL  .EQ. 2           ) V  =  PHS
        IF( AKM .EQ. 2.5D0 .AND.              &
            IS  .EQ. 1     .AND.              &
            IC  .EQ. 6     .AND.              &
            IL  .EQ. 2           ) IP =  1
        IF( AKM .EQ. 2.5D0 .AND.              &
            IS  .EQ. 2     .AND.              &
            IC  .EQ. 6     .AND.              &
            IL  .EQ. 1           ) IP =  1
!
!CC LE BLOC K=3/2 MODULO 3
!
        IF( AKM .EQ. 1.5D0 .AND.              &
            IS  .EQ. 1     .AND.              &
            IC  .EQ. 4     .AND.              &
            IL  .EQ. 1           ) V  =  PHS
        IF( AKM .EQ. 1.5D0 .AND.              &
            IS  .EQ. 2     .AND.              &
            IC  .EQ. 4     .AND.              &
            IL  .EQ. 1           ) V  =  PHS
        IF( AKM .EQ. 1.5D0 .AND.              &
            IS  .EQ. 1     .AND.              &
            IC  .EQ. 5     .AND.              &
            IL  .EQ. 2           ) V  =  PHS
        IF( AKM .EQ. 1.5D0 .AND.              &
            IS  .EQ. 2     .AND.              &
            IC  .EQ. 5     .AND.              &
            IL  .EQ. 2           ) V  =  PHS
        IF( AKM .EQ. 1.5D0 .AND.              &
            IS  .EQ. 1     .AND.              &
            IC  .EQ. 5     .AND.              &
            IL  .EQ. 2           ) IP =  1
        IF( AKM .EQ. 1.5D0 .AND.              &
            IS  .EQ. 2     .AND.              &
            IC  .EQ. 4     .AND.              &
            IL  .EQ. 1           ) IP =  1
!
!CC LE BLOC K=1/2 MODULO 3
!
        IF( AKM .EQ. 0.5D0 .AND.              &
            IS  .EQ. 1     .AND.              &
            IC  .EQ. 6     .AND.              &
            IL  .EQ. 1           ) V  =  PHS
        IF( AKM .EQ. 0.5D0 .AND.              &
            IS  .EQ. 1     .AND.              &
            IC  .EQ. 6     .AND.              &
            IL  .EQ. 2           ) V  = -PHS
        IF( AKM .EQ. 0.5D0 .AND.              &
            IS  .EQ. 2     .AND.              &
            IC  .EQ. 6     .AND.              &
            IL  .EQ. 1           ) V  =  PHS
        IF( AKM .EQ. 0.5D0 .AND.              &
            IS  .EQ. 2     .AND.              &
            IC  .EQ. 6     .AND.              &
            IL  .EQ. 2           ) V  = -PHS
        IF( AKM .EQ. 0.5D0 .AND.              &
            IS  .EQ. 1     .AND.              &
            IC  .EQ. 6     .AND.              &
            IL  .EQ. 1           ) IP =  1
        IF( AKM .EQ. 0.5D0 .AND.              &
            IS  .EQ. 2     .AND.              &
            IC  .EQ. 6     .AND.              &
            IL  .EQ. 2           ) IP =  1
      ENDIF
      RETURN
!
20    KM6 = MOD(INT(AK),6)
      KP  = KM6-(KM6/2)*2
      JP  = INT(AJ)-(INT(AJ)/2)*2
!
!CC POUR J PAIR ET IMPAIR (DANS CE CAS ON METTRA LES DEUX CAS ENSEMBLE POUR AVOIR UNE FORME COMPACTE POUR LE TENSEUR METRIQUE
!
!CC LES BLOCS E
      IF( JP .EQ. 0 ) THEN
        IF( AK  .GT. 0.D0 .AND.               &
            IC  .EQ. 3    .AND.               &
            KM6 .EQ. 1    .AND.               &
            IS  .EQ. IL         ) V  = -1.D0
        IF( AK  .GT. 0.D0 .AND.               &
            IC  .EQ. 3    .AND.               &
            KM6 .EQ. 2    .AND.               &
            IS  .EQ. IL         ) V  =  1.D0
        IF( AK  .GT. 0.D0 .AND.               &
            IC  .EQ. 3    .AND.               &
            KM6 .EQ. 4    .AND.               &
            IS  .EQ. IL         ) V  =  1.D0
        IF( AK  .GT. 0.D0 .AND.               &
            IC  .EQ. 3    .AND.               &
            KM6 .EQ. 5    .AND.               &
            IS  .EQ. IL         ) V  =  1.D0
!CC LES BLOCS A1+A2 AND MODULO 6 Y COMPRIS LE SIGNE MOINS
        IF( AK  .GT. 0.D0 .AND.               &
            IC  .EQ. 1    .AND.               &
            KM6 .EQ. 0    .AND.               &
            IS  .EQ. 1    .AND.               &
            IL  .EQ. 1          ) V  =  1.D0
        IF( AK  .GT. 0.D0 .AND.               &
            IC  .EQ. 2    .AND.               &
            KM6 .EQ. 0    .AND.               &
            IS  .EQ. 2    .AND.               &
            IL  .EQ. 2          ) V  = -1.D0
!CC LES BLOCS A1+A2 AND MODULO 3 Y COMPRIS LE SIGNE MOINS
        IF( AK  .GT. 0.D0 .AND.               &
            IC  .EQ. 1    .AND.               &
            KM6 .EQ. 3    .AND.               &
            IS  .EQ. 1    .AND.               &
            IL  .EQ. 1          ) V  =  1.D0
        IF( AK  .GT. 0.D0 .AND.               &
            IC  .EQ. 2    .AND.               &
            KM6 .EQ. 3    .AND.               &
            IS  .EQ. 2    .AND.               &
            IL  .EQ. 2          ) V  = -1.D0
!CC TRAITEMENT DES SIGNES MOINS
        IF( AK  .GT. 0.D0 .AND.               &
            IC  .EQ. 3    .AND.               &
            KM6 .EQ. 4    .AND.               &
            IS  .EQ. IL   .AND.               &
            IL  .EQ. 2          ) V  = -1.D0
        IF( AK  .GT. 0.D0 .AND.               &
            IC  .EQ. 3    .AND.               &
            KM6 .EQ. 5    .AND.               &
            IS  .EQ. IL   .AND.               &
            IL  .EQ. 2          ) V  = -1.D0
!CC TRAITEMENT DU CARACTERE IMAGINAIRE
        IF( AK  .GT. 0.D0 .AND.               &
            IC  .EQ. 2    .AND.               &
            KM6 .EQ. 0    .AND.               &
            IS  .EQ. 2    .AND.               &
            IL  .EQ. 2          ) IP =  1
        IF( AK  .GT. 0.D0 .AND.               &
            IC  .EQ. 3    .AND.               &
            KM6 .EQ. 5    .AND.               &
            IS  .EQ. 1    .AND.               &
            IL  .EQ. 1          ) IP =  1
        IF( AK  .GT. 0.D0 .AND.               &
            IC  .EQ. 3    .AND.               &
            KM6 .EQ. 4    .AND.               &
            IS  .EQ. 2    .AND.               &
            IL  .EQ. 2          ) IP =  1
        IF( AK  .GT. 0.D0 .AND.               &
            IC  .EQ. 1    .AND.               &
            KM6 .EQ. 3    .AND.               &
            IS  .EQ. 1    .AND.               &
            IL  .EQ. 1          ) IP =  1
        IF( AK  .GT. 0.D0 .AND.               &
            IC  .EQ. 3    .AND.               &
            KM6 .EQ. 2    .AND.               &
            IS  .EQ. 2    .AND.               &
            IL  .EQ. 2          ) IP =  1
        IF( AK  .GT. 0.D0 .AND.               &
            IC  .EQ. 3    .AND.               &
            KM6 .EQ. 1    .AND.               &
            IS  .EQ. 1    .AND.               &
            IL  .EQ. 1          ) IP =  1
      ELSE
!CC J IMPAIR
!CC 1+6p
        IF( AK  .GT. 0.D0 .AND.               &
            IC  .EQ. 3    .AND.               &
            KM6 .EQ. 1    .AND.               &
            IS  .EQ. IL         ) V  =  1.D0
        IF( AK  .GT. 0.D0 .AND.               &
            IC  .EQ. 3    .AND.               &
            KM6 .EQ. 1    .AND.               &
            IS  .EQ. IL   .AND.               &
            IL  .EQ. 1          ) V  = -1.D0
        IF( AK  .GT. 0.D0 .AND.               &
            IC  .EQ. 3    .AND.               &
            KM6 .EQ. 1    .AND.               &
            IS  .EQ. IL   .AND.               &
            IL  .EQ. 2          ) IP =  1
!CC 2+6p
        IF( AK  .GT. 0.D0 .AND.               &
            IC  .EQ. 3    .AND.               &
            KM6 .EQ. 2    .AND.               &
            IS  .EQ. IL         ) V  =  1.D0
        IF( AK  .GT. 0.D0 .AND.               &
            IC  .EQ. 3    .AND.               &
            KM6 .EQ. 2    .AND.               &
            IS  .EQ. IL   .AND.               &
            IL  .EQ. 2          ) V  = -1.D0
        IF( AK  .GT. 0.D0 .AND.               &
            IC  .EQ. 3    .AND.               &
            KM6 .EQ. 2    .AND.               &
            IS  .EQ. IL   .AND.               &
            IL  .EQ. 1          ) IP =  1
!CC 3+6p
        IF( AK  .GT. 0.D0 .AND.               &
            IC  .EQ. 1    .AND.               &
            KM6 .EQ. 3    .AND.               &
            IS  .EQ. 1    .AND.               &
            IL  .EQ. 1          ) V  =  1.D0
        IF( AK  .GT. 0.D0 .AND.               &
            IC  .EQ. 2    .AND.               &
            KM6 .EQ. 3    .AND.               &
            IS  .EQ. 2    .AND.               &
            IL  .EQ. 2          ) V  = -1.D0
        IF( AK  .GT. 0.D0 .AND.               &
            IC  .EQ. 2    .AND.               &
            KM6 .EQ. 3    .AND.               &
            IS  .EQ. 2    .AND.               &
            IL  .EQ. 2          ) IP =  1
!CC 4+6p
        IF( AK  .GT. 0.D0 .AND.               &
            IC  .EQ. 3    .AND.               &
            KM6 .EQ. 4    .AND.               &
            IS  .EQ. IL         ) V  = -1.D0
        IF( AK  .GT. 0.D0 .AND.               &
            IC  .EQ. 3    .AND.               &
            KM6 .EQ. 4    .AND.               &
            IS  .EQ. IL   .AND.               &
            IL  .EQ. 1          ) IP =  1
!CC 5+6p
        IF( AK  .GT. 0.D0 .AND.               &
            IC  .EQ. 3    .AND.               &
            KM6 .EQ. 5    .AND.               &
            IS  .EQ. IL         ) V  = -1.D0
        IF( AK  .GT. 0.D0 .AND.               &
            IC  .EQ. 3    .AND.               &
            KM6 .EQ. 5    .AND.               &
            IS  .EQ. IL   .AND.               &
            IL  .EQ. 2          ) IP =  1
!CC 6+6p
        IF( AK  .GT. 0.D0 .AND.               &
            IC  .EQ. 1    .AND.               &
            KM6 .EQ. 6    .AND.               &
            IS  .EQ. 1    .AND.               &
            IL  .EQ. 1          ) V  =  1.D0
        IF( AK  .GT. 0.D0 .AND.               &
            IC  .EQ. 1    .AND.               &
            KM6 .EQ. 6    .AND.               &
            IS  .EQ. 1    .AND.               &
            IL  .EQ. 1          ) IP =  1
        IF( AK  .GT. 0.D0 .AND.               &
            IC  .EQ. 2    .AND.               &
            KM6 .EQ. 6    .AND.               &
            IS  .EQ. 2    .AND.               &
            IL  .EQ. 2          ) V  = -1.D0
      ENDIF
!
      RETURN
      END SUBROUTINE VKCS
