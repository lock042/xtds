!
! *** CALCUL DE FACTORIELLES
!
      SUBROUTINE FACTO
      use mod_dppr
      use mod_par_tds
      use mod_com_fa
      IMPLICIT NONE

      real(kind=dppr)  :: A
      real(kind=dppr)  :: B

      integer          :: I,IK
!
      A  = 1.D0
      IK = 0
      DO I=1,MXFAC
        B = I
        A = A*B
        IF( A .LT. 10000.D0 ) GOTO 3
        A  = A/10000.D0
        IK = IK+4
!
3       KFAC(I) = IK
        FACT(I) = A
      ENDDO
!
      RETURN
      END SUBROUTINE FACTO
