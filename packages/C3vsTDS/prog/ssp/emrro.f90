!
!     ELEM. MAT. REDUIT DU ROTATEUR SPHERIQUE.
!
! 05/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3vs.
!
      FUNCTION EMRRO(IMEGA,KR,J)
      use mod_dppr
      use mod_com_fa
      IMPLICIT NONE
      real(kind=dppr)  :: EMRRO,J
      integer          :: IMEGA,KR

      real(kind=dppr)  :: A
      real(kind=dppr)  :: B
      real(kind=dppr)  :: C
      real(kind=dppr)  :: D
      real(kind=dppr)  :: J2MK,J2P2,J2PK1

      integer          :: I,IA
      integer          :: K,K2M1
!
      J2P2  = 2*J+2
      J2PK1 = 2*J+KR+1
      K2M1  = 2*KR-1
      J2MK  = 2*J-KR
      C     = 1.D0
      B     = 1.D0
      IF( J2MK .LE. 1 ) GOTO 2
      A  = FACT(NINT(J2PK1))/FACT(NINT(J2MK))
      IA = KFAC(NINT(J2PK1))-KFAC(NINT(J2MK))
      GOTO 3
!
2     A  = FACT(NINT(J2PK1))
      IA = KFAC(NINT(J2PK1))
!
3     A = A*( (10.D0)**IA )
      IF( KR .LE. 1 ) GOTO 6
      DO K=1,K2M1,2
        B = B*DBLE(K)
      ENDDO
      C = FACT(KR)*( (10.D0)**KFAC(KR) )
!
6     D = SQRT(A/B*C)
      I = (IMEGA-KR)/2
      IF( I .GT. 0 ) GOTO 8
      EMRRO = D
      EMRRO = EMRRO*( (-1)**KR )
      RETURN
!
8     EMRRO = D*( (-2.D0*DBLE(J*J2P2)/SQRT(3.D0))**I )
      EMRRO = EMRRO*( (-1)**KR )
!
      RETURN
      END FUNCTION EMRRO
