      PROGRAM SPECT
!
!  WRITTEN FOR SUN4 OS XXX FORTRAN XXX  11.02.91   J.P.CHAMPION
!  SMIL LABORATOIRE DE SPECTRONOMIE MOLECULAIRE ET INSTRUMENTATION LASER
!       URA CNRS 0777    UNIVERSITE DE DIJON
!    6 BOULEVARD GABRIEL  21000  DIJON  FRANCE
!
!  CALCULATED SPECTRUM : POSITIONS + INTENSITIES
!   XY4  TETRAHEDRAL MOLECULES
!
!  NON-INTERACTIVE VERSION   23 JULY 1993
! REV    JAN 1995 JPC,CW (PARAMETER)
! REV    NOV 1996 JPC,CW (STRxxx)
! REV    JUN 2000 JPC    (STRTOT)
! MODIFIE 05/09 V. BOUDON, A. EL HILALI ---> XY3Z/C3vs.
!
! APPEL : spect  FMIN  FMAX  TVIB  TROT  RINMI [fpvib FPVIB] [abund ABUND]
!
      use mod_dppr
      use mod_par_tds
      use mod_com_branch
      use mod_com_const
      use mod_com_fdate
      use mod_com_fp
      use mod_com_spin
      use mod_com_sy
      IMPLICIT NONE

      real(kind=dppr)  :: A2,AJI,AJI1,AJI2,AJIM,AJIMAX,AJIMIN,AJMAX1,AJS
      real(kind=dppr)  :: COEF
      real(kind=dppr)  :: ENINF,ERINF,ERSUP,EVINF
      real(kind=dppr)  :: F,F1,F2,FM,FMAX,FMIN,FR,FRJMAX,FV
      real(kind=dppr)  :: HHT,HT
      real(kind=dppr)  :: POP
      real(kind=dppr)  :: RINMI
      real(kind=dppr)  :: STRSEL,STRTOT
      real(kind=dppr)  :: TR,TR1,TR2,TRJMAX,TRM,TROT,TVIB
      real(kind=dppr)  :: W0

      integer          :: ICI,ICS,III,IPCINF,IPCSUP,IRA,ISV
      integer          :: NINF,NSUP,NSVINF,NSVSUP,NTRANS,NUSVI,NUSVS

      character(len = NBCTIT)  :: BUFF,TITRE
      character(len =   2)  :: BR,BR1,BR2,BRJMAX,BRM
      character(len =   2)  :: SYJMAX,SYM1,SYM2,SYMM
      character(len =  10)  :: OPT
      character(len =  30)  :: PUNIT
!
1001  FORMAT(A)
1011  FORMAT(I4,A)
1020  FORMAT('     #     v1        v2        v3        v4        v5            v6        Cv')
1021  FORMAT('     Frequency  Intensity     J" C"  n" #vib"    J  C   n  #vib      Lower Energy    Lower Population',/)
1022  FORMAT(F15.6,1PE9.2,0P,2X,A,2(F5.1,1X,2A,2I3,I4,'% '),  &
             F15.6,1X,E15.6)
1023  FORMAT(F12.6,1PE9.2)
1030  FORMAT(/,              &
             'SPECT  : ',A)
1121  FORMAT(/,                                 &
             I3,' Upper Vibrational States',/)
1122  FORMAT(/,                                 &
             I3,' Lower Vibrational States',/)
1222  FORMAT(/,                                            &
             ' Number of Calculated Transitions     ',I9)
1223  FORMAT(' First Transition      -> ',F15.6,1PE9.2,0P,2X,A,F5.1,1X,2A)
1224  FORMAT(' Strongest Transition  -> ',F15.6,1PE9.2,0P,2X,A,F5.1,1X,2A)
1225  FORMAT(' Last Transition       -> ',F15.6,1PE9.2,0P,2X,A,F5.1,1X,2A)
1226  FORMAT(' Effective Jo range    -> ',F5.1,' -',F5.1)
1227  FORMAT(' Strongest Tr at Jmax  -> ',F15.6,1PE9.2,0P,2X,A,F5.1,1X,2A)
1228  FORMAT(' Intensity summations:'            ,/,   &
             E9.2,1X,A,'with    threshold =',E9.2,/,   &
             E9.2,1X,A,'without threshold'          )
1230  FORMAT(/,                                                  &
             'Spectroscopic Data Calculated through J = ',F5.1)
1231  FORMAT('Imposed Frequency Range:',F20.6,' - ',F20.6)
1232  FORMAT('Vibrational Temperature:',F8.2,' Kelvin')
1233  FORMAT('Rotational  Temperature:',F8.2,' Kelvin')
1234  FORMAT('Intensity Lower Limit:',E10.2,1X,A)
1235  FORMAT('Hamiltonian Parameters in C3vs Formalism',/)
1236  FORMAT(/,                                                   &
             'Transition Moment Parameters in C3vs Formalism',/)
1237  FORMAT(/,'  Calculated Transitions',/)
1251  FORMAT('Abundance: ',F9.4)
1252  FORMAT('Vibrational partition function: ',1PE12.4)
2000  FORMAT(' <  ',A)
2001  FORMAT(' >> ',A)
8000  FORMAT(' !!! SPECT  : STOP ON ERROR')
8001  FORMAT(' !!! ERROR OPENING CONTROL FILE')
8002  FORMAT(' !!! INCOMPLETE CONTROL FILE')
8004  FORMAT(' !!! BAD OPTION IN CONTROL FILE')
8013  FORMAT(' !!! 0. < FPVIB    REQUESTED')
8014  FORMAT(' !!! 0. < ABUND <=1.    REQUESTED')
8090  FORMAT(' !!! ERROR OPENING trans.t FILE')
8091  FORMAT(' !!! ERROR OPENING spectr.t FILE')
8094  FORMAT(' !!! ERROR OPENING spectr.xy FILE')
!
      OPEN(10,FILE=CTRLF,ERR=9998,STATUS='OLD')
      READ(10,1001,END=9997) FDATE
      PRINT 1030,            FDATE
!
! *** INPUT / OUTPUT FILES
!
      PRINT 2000,           'trans.t'
      OPEN(90,ERR=4090,FILE='trans.t',FORM='UNFORMATTED',STATUS='OLD')
      PRINT 2001,           'spectr.t'
      OPEN(91,ERR=4091,FILE='spectr.t',STATUS='UNKNOWN')
      PRINT 2001,           'spectr.xy'
      OPEN(92,ERR=4094,FILE='spectr.xy',STATUS='UNKNOWN')
!
! *** BASIC MOLECULAR CONSTANTS
!
      READ(90)
      READ(90) BUFF
      READ(BUFF,*) SPIN,SPINY
      READ(90)
      READ(90) BUFF
      READ(BUFF,*) VIBNU,B0,A0,BB0,DD0
      REWIND(90)
!
! *** READING INPUT FILE HEADINGS
!
      WRITE(91,1235)
      CALL IOPBA0(90,91)
!
! INTENSITY UNIT
!
      READ(90)
      READ(90)
      READ(90)
      READ(90) BUFF
      READ(BUFF,1011) III,PUNIT
      REWIND(90)
      CALL IOPBA0(90,0)
      WRITE(91,1236)
      CALL IOPBA0(90,91)
      READ (90)      NSVSUP
      WRITE(91,1121) NSVSUP
      PRINT    1121, NSVSUP
      WRITE(91,1020)
      PRINT 1020
      DO ISV=1,NSVSUP
        READ (90)      TITRE(:122)                                                                 ! cf. FORMAT 1002 *mod*.f
        WRITE(91,1001) TITRE(:122)                                                                 ! cf. FORMAT 1002 *mod*.f
        PRINT 1001,    TITRE(:122)                                                                 ! cf. FORMAT 1002 *mod*.f
      ENDDO
      READ (90)      NSVINF
      WRITE(91,1122) NSVINF
      PRINT    1122, NSVINF
      WRITE(91,1020)
      PRINT 1020
      DO ISV=1,NSVINF
        READ(90)       TITRE(:122)                                                                 ! cf. FORMAT 1002 *mod*.f
        WRITE(* ,1001) TITRE(:122)                                                                 ! cf. FORMAT 1002 *mod*.f
        WRITE(91,1001) TITRE(:122)                                                                 ! cf. FORMAT 1002 *mod*.f
      ENDDO
!
! *** READING OPTIONS IN INPUT FILE
!
      READ(90) AJMAX1
      READ(90) IRA
!
! *** ENTERING NEW OPTIONS FROM ARGUMENTS OF THE SPECT COMMAND
!
      WRITE(91,1230) AJMAX1
      PRINT    1230, AJMAX1
      READ(10,*,END=9997) FMIN
      READ(10,*,END=9997) FMAX
      WRITE(91,1231) FMIN,FMAX
      PRINT    1231, FMIN,FMAX
      READ (10,*,END=9997) TVIB
      WRITE(91,1232)       TVIB
      PRINT    1232,       TVIB
      READ (10,*,END=9997) TROT
      WRITE(91,1233)       TROT
      PRINT    1233,       TROT
      READ (10,*,END=9997) RINMI
      WRITE(91,1234)       RINMI,TRIM(PUNIT)
      PRINT    1234,       RINMI,TRIM(PUNIT)
      ABUND = 1.D0                                                                                 ! abundance
      FPVIB = 0.D0                                                                                 ! no fpvib option
!
19    READ(10,1001,END=21) OPT
      IF( OPT .EQ. 'fpvib' ) THEN
        READ(10,*,END=9997) FPVIB
        IF( FPVIB .LE. 0.D0 ) GOTO 9992
        GOTO 19
      ENDIF
      IF( OPT .EQ. 'abund' ) THEN
        READ(10,*,END=9997) ABUND
        IF( ABUND .LE. 0.D0 .OR.              &
            ABUND .GT. 1.D0      ) GOTO 9991
        GOTO 19
      ENDIF
      GOTO 9996                                                                                    ! bad option
!
21    CLOSE(10)
!
! *** CALCULATING  CONSTANT FACTOR FROM PARTITION FUNCTION
!
      COEF = (8.D0*PI*PI*PI*CL)/(CLUM*PLANK)*1.D-36
      COEF = COEF*T0/TROT/FPART(TROT,TVIB)
      WRITE(91,1251) ABUND
      PRINT    1251, ABUND
      WRITE(91,1252) FPVIB
      PRINT    1252, FPVIB
!
! *** HEADINGS
!
      WRITE(91,1237)
      WRITE(91,1021)
!
! *** INITIAL VALUES FOR Fmin,Fmax,Imax,JImin,JImax,Ntrans
!
      F1     =   1.D+10
      F2     =   0.D0
      TRM    =   0.D0
      TRJMAX =   0.D0
      FRJMAX =   0.D0
      BRJMAX = '  '
      SYJMAX = '  '
      AJIMIN = 200.D0
      AJIMAX =   0.D0
      NTRANS =   0
!
! *** READING INPUT FILE
!
      STRTOT = 0.D0
      STRSEL = 0.D0
!
1003  READ(90,END=9000) AJI,ICI,NINF,NUSVI,IPCINF,  &
                        ENINF,                      &
                        AJS,ICS,NSUP,NUSVS,IPCSUP,  &
                        F,A2
      IF( FMIN .GT. F .OR.              &
          FMAX .LT. F      ) GOTO 1003
!
! *** CALCULATING INTENSITY
!
      ERINF = B0*AJI*(AJI+1.D0)
      EVINF = ENINF-ERINF
      HT    = ((EVINF/TVIB)+(ERINF/TROT))*HCOVRK
      W0    = EXP(-HT)*SPIN(ICI)
      POP   = W0*(2.D0*AJI+1.D0)/FPART(TROT,TVIB)
      ERSUP = B0*AJS*(AJS+1.D0)
      FR    = ERSUP-ERINF
      FV    = F-FR
      HHT   = ((FR/TROT)+(FV/TVIB))*HCOVRK
      IF( EXP(-HHT) .GE. 1.D0 ) GOTO 1003
      TR = A2*W0*F*COEF*(1.D0-EXP(-HHT))
      IF( IRA .EQ. 0 ) TR = A2*W0
      TR     = TR*ABUND
      STRTOT = STRTOT+TR
      IF( TR .LT. RINMI ) GOTO 1003
      STRSEL = STRSEL+TR
      BR     = BRANCH(NINT(AJS-AJI)+MXBRA-2)
      NTRANS = NTRANS+1
!
! *** DETERMINING SPECTRUM LIMITS
!
      IF( F .LT. F1 ) THEN
        F1   = F
        TR1  = TR
        BR1  = BR
        AJI1 = AJI
        SYM1 = SYM(ICI)
      ENDIF
      IF( F .GT. F2 ) THEN
        F2   = F
        TR2  = TR
        BR2  = BR
        AJI2 = AJI
        SYM2 = SYM(ICI)
      ENDIF
      IF( TR .GT. TRM ) THEN
        FM   = F
        TRM  = TR
        BRM  = BR
        AJIM = AJI
        SYMM = SYM(ICI)
      ENDIF
      IF( AJI .LT. AJIMIN       ) AJIMIN = AJI
      IF( AJI .GT. AJIMAX       ) AJIMAX = AJI
      IF( AJS .EQ. AJMAX1 .AND.         &
          TR  .GT. TRJMAX       ) THEN
        FRJMAX = F
        TRJMAX = TR
        BRJMAX = BR
        SYJMAX = SYM(ICI)
      ENDIF
!
! *** WRITING IN OUTPUT FILE
!
      WRITE(91,1022) F,TR,BR,                                &
                     AJI,SYM(ICI),PARGEN,NINF,NUSVI,IPCINF,  &
                     AJS,SYM(ICS),PARGEN,NSUP,NUSVS,IPCSUP,  &
                     ENINF,POP
      WRITE(92,1023) F,TR
      GOTO 1003
!
4090  PRINT 8090
      GOTO  9999
4091  PRINT 8091
      GOTO  9999
4094  PRINT 8094
      GOTO  9999
9991  PRINT 8014
      GOTO  9999
9992  PRINT 8013
      GOTO  9999
9996  PRINT 8004
      GOTO  9999
9997  PRINT 8002
      GOTO  9999
9998  PRINT 8001
9999  PRINT 8000
      STOP
!
9000  PRINT *
      PRINT    1222, NTRANS
      WRITE(91,1222) NTRANS
      PRINT    1223, F1,TR1,BR1,AJI1,SYM1,PARGEN
      WRITE(91,1223) F1,TR1,BR1,AJI1,SYM1,PARGEN
      PRINT    1224, FM,TRM,BRM,AJIM,SYMM,PARGEN
      WRITE(91,1224) FM,TRM,BRM,AJIM,SYMM,PARGEN
      PRINT    1225, F2,TR2,BR2,AJI2,SYM2,PARGEN
      WRITE(91,1225) F2,TR2,BR2,AJI2,SYM2,PARGEN
      PRINT    1226, AJIMIN,AJIMAX
      WRITE(91,1226) AJIMIN,AJIMAX
      IF( TRJMAX .NE. 0.D0 ) THEN
        PRINT    1227, FRJMAX,TRJMAX,BRJMAX,AJMAX1,SYJMAX,PARGEN
        WRITE(91,1227) FRJMAX,TRJMAX,BRJMAX,AJMAX1,SYJMAX,PARGEN
      ENDIF
      PRINT    1228, STRSEL,PUNIT,RINMI,STRTOT,PUNIT
      WRITE(91,1228) STRSEL,PUNIT,RINMI,STRTOT,PUNIT
      CLOSE(90)
      CLOSE(91)
      CLOSE(92)
      PRINT *
      END PROGRAM SPECT
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
!     READING AND COPYING PARAMETERS
!
      SUBROUTINE IOPBA0(LUI,LUO)
      use mod_dppr
      use mod_par_tds
      IMPLICIT NONE
      integer          :: LUI,LUO

      real(kind=dppr)  :: PARA,PREC

      integer          :: I,IP
      integer          :: NBOPH

      character(len = NBCLAB+13)  :: CHAINE
      character(len = NBCTIT)     :: TITRE
!
1000  FORMAT(A)
1001  FORMAT(I4,A)
1002  FORMAT(A,E18.11,E14.7)
8000  FORMAT(' !!! SPECT  : STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF IN IOPBA0')
!
      DO I=1,4
        READ(LUI,END=2000)               TITRE
        IF( LUO .NE. 0 ) WRITE(LUO,1000) TRIM(TITRE)
      ENDDO
      READ(LUI,END=2000)               NBOPH,TITRE
      IF( LUO .NE. 0 ) WRITE(LUO,1001) NBOPH,TRIM(TITRE)
      DO I=1,2
        READ(LUI,END=2000)               TITRE
        IF( LUO .NE. 0 ) WRITE(LUO,1000) TRIM(TITRE)
      ENDDO
      DO IP=1,NBOPH
        READ(LUI,END=2000)               CHAINE,PARA,PREC
        IF( LUO .NE. 0 ) WRITE(LUO,1002) CHAINE,PARA,PREC
      ENDDO
      RETURN
!
2000  PRINT 8003
      PRINT 8000
      PRINT *
      STOP
!
      END SUBROUTINE IOPBA0
