      PROGRAM ASFN
!
! REV    SEPT 2006  CW
!
!  lire un fichier binaire (ex. INFILE) de type FN_
!  et l'ecrire en ASCII dans INFILE'_ASC'
!
! APPEL : asfn
!
      use mod_dppr
      use mod_par_tds
      use mod_main_rovbas
      IMPLICIT NONE

      real(kind=dppr)  :: AJ

      integer          :: I,IC,ICC,IP,ISV
      integer          :: NELMA,NFB,NSV

      character(len = NBCTIT)  :: IDENT,TITRE
      character(len =  40)  :: IDEMR
      character(len = 150)  :: INFILE
      character(len = 160)  :: OUTFILE
!
1000  FORMAT(A)
1020  FORMAT(/,            &
             'ASFN   : ')
2000  FORMAT(' <  ',A)
2001  FORMAT(' >> ',A)
7000  FORMAT('ENTER FN_ TYPE FILE NAME :')
8000  FORMAT(' !!! ASFN   : STOP ON ERROR')
8003  FORMAT(' !!! UNEXPECTED EOF FOR ',A)
!
! ARRAY INITIAL ALLOCATION
!
      CALL ALLOC_ALL
!
      PRINT 1020
!
!  FICHIER D'ENTREE
!
      PRINT 7000
      READ(*,1000) INFILE
      PRINT 2000,  TRIM(INFILE)
      OPEN(30,FILE=TRIM(INFILE),FORM='UNFORMATTED',STATUS='OLD')
!
!  FICHIER DE SORTIE
!
      OUTFILE = TRIM(INFILE)//'_ASC'
      PRINT 2001,  TRIM(OUTFILE)
      OPEN(10,FILE=TRIM(OUTFILE))
      DO I=1,3
        READ (30,END=9003) TITRE
        WRITE(10,*)        TITRE
      ENDDO
      READ (30,END=9003) IDEMR
      WRITE(10,*)        IDEMR
      DO I=1,3
        READ (30,END=9003) TITRE
        WRITE(10,*)        TITRE
      ENDDO
!
5     READ(30,END=9003) TITRE
      IF( TITRE(:28) .NE. '     (v1;v2;v3;v4;v5;v6) = (' ) THEN
        BACKSPACE(30)
        GOTO 4
      ENDIF
      WRITE(10,*)       TITRE
      GOTO 5
!
4     READ (30,END=9003) TITRE
      WRITE(10,*)        TITRE
      WRITE(10,*) '>>> NSV'
      READ (30,END=9003) NSV,TITRE
      WRITE(10,*)        NSV,TITRE
      READ (30,END=9003) TITRE
      WRITE(10,*)        TITRE
      DO ISV=1,NSV
        READ (30,END=9003) IDENT(:122)                                                             ! cf. FORMAT 1002 *mod*.f
        WRITE(10,*)        IDENT(:122)                                                             ! cf. FORMAT 1002 *mod*.f
      ENDDO
      WRITE(10,*) '>>> AJ,IC,NELMA,NFB'
      WRITE(10,*) '>>> (NVCOD(IP),NRCOD(IP),NRVCOD(IP),IP=1,NFB)'
!
12    DO ICC=1,MXSYM
        READ (30,END=9000) AJ,IC,NELMA,NFB
        WRITE(10,*)        AJ,IC,NELMA,NFB
        DO WHILE( NFB .GT.MXDIMS )
          CALL RESIZE_MXDIMS
        ENDDO
        IF(NFB.NE.0) THEN
          READ (30,END=9003) (NVCOD(IP),NRCOD(IP),NRVCOD(IP),IP=1,NFB)
          WRITE(10,*)        (NVCOD(IP),NRCOD(IP),NRVCOD(IP),IP=1,NFB)
        ENDIF
      ENDDO
      GOTO 12
!
9003  PRINT 8003, TRIM(INFILE)
      PRINT 8000
!
9000  CLOSE(10)
      PRINT *
      END PROGRAM ASFN
