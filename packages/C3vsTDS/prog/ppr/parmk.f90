      PROGRAM PARMK
!
!  CREER   UN FICHIER DE PARAMETRE sans [ou avec] para d'intensite
!  A PARTIR DE MH_xxx [et MD_xxx ou MP_xxx]
!
!  DEC 2004 CW VB
! MODIF 05/09 V. BOUDON, A. EL HILALI ---> XY3Z/C3vs.
!
! APPEL : parmk  Pnsup Dksup [Pninf Dktrm dip|pol]     ParaMolDir ParaFileName
!
!
      use mod_par_tds
      use mod_com_fdate
      IMPLICIT NONE

      integer ,dimension(NBVQN)  :: IVA,IVC
      integer          :: I
      integer          :: J
      integer          :: NBOPH,NBOPT,NBP,NSVQN

      character(len = NBCTIT)  :: TITRE
      character(len =  11) ,dimension(5)      :: CARG
      character(len =   3)  :: OPTION
      character(len =   3)  :: PI,PS
      character(len =  12)  :: DS,DT
      character(len = 100)  :: MOLDIR
      character(len = 120)  :: FEMRV,FPARA
!
1000  FORMAT(A)
1001  FORMAT(I4,A)
1002  FORMAT(6I1)                                                                                  ! 6 = NBVQN
1003  FORMAT(A,1X,I1,3A,I6,1X,E18.11,E14.7)                                                        ! 3A = NBAM, cf. FORMAT 1001 DE PARADJ
1010  FORMAT(/,              &
             'PARMK  : ',A)
2000  FORMAT(' <  ',A)
2001  FORMAT(' >> ',A)
8000  FORMAT(' !!! PARMK  : STOP ON ERROR')
8001  FORMAT(' !!! ERROR OPENING CONTROL FILE')
8002  FORMAT(' !!! INCOMPLETE CONTROL FILE')
8003  FORMAT(' !!! ERROR OPENING VIBRATIONAL REDUCED MATRIX ELEMENTS FILE (HAMILTONIAN)')
8004  FORMAT(' !!! ERROR OPENING SKELETON PARAMETER FILE')
8005  FORMAT(' !!! ERROR OPENING FINAL PARAMETER FILE')
8006  FORMAT(' !!! ERROR READING VIBRATIONAL REDUCED MATRIX ELEMENTS FILE (HAMILTONIAN)')
8007  FORMAT(' !!! NEITHER dip NOR pol')
8008  FORMAT(' !!! ERROR READING SKELETON PARAMETER FILE')
8009  FORMAT(' !!! ERROR WRITING FINAL PARAMETER FILE')
8010  FORMAT(' !!! BAD NUMBER OF LINES IN CONTROL FILE')
8011  FORMAT(' !!! ERROR OPENING VIBRATIONAL REDUCED MATRIX ELEMENTS FILE (TRANSITION MOMENT)')
8012  FORMAT(' !!! ERROR READING VIBRATIONAL REDUCED MATRIX ELEMENTS FILE (TRANSITION MOMENT)')
!
      OPEN(10,FILE=CTRLF,ERR=9998,STATUS='OLD')
      NBP = 0                                                                                      ! nb total de paras
!
6     READ(10,1000,END=7)
      NBP = NBP+1
      GOTO 6
!
7     REWIND(10)
      NBP = NBP-3                                                                                  ! nb reduit de paras
      IF( NBP .NE. 2 .AND.         &
          NBP .NE. 5       ) THEN
        PRINT 8010
        GOTO  9999
      ENDIF
      READ(10,1000,END=9997) FDATE
      PRINT 1010,            FDATE
      DO I=1,NBP
        READ(10,1000,END=9997) CARG(I)
      ENDDO
      READ(10,1000,END=9997) MOLDIR
      READ(10,1000,END=9997) FPARA
      CLOSE(10)
      PS = TRIM(CARG(1))                                                                           ! upper polyad number
      DS = CARG(2)                                                                                 ! upper polyad development order
      IF( NBP .NE. 2 ) THEN                                                                        ! lower polyad characteristics
        PI     = TRIM(CARG(3))
        DT     = CARG(4)
        OPTION = TRIM(CARG(5))
      ENDIF
      FEMRV = 'MH_'//TRIM(PS)//'_'//DS
      PRINT 2000,  TRIM(FEMRV)
      OPEN(11,FILE=TRIM(FEMRV),ERR=9996,STATUS='OLD')
!
!  COPIE DE L'ENTETE DE Pa_skel
!
      PRINT 2000,  TRIM(MOLDIR)//'/Pa_skel'
      OPEN(12,FILE=TRIM(MOLDIR)//'/Pa_skel',ERR=9995,STATUS='OLD')
      PRINT 2001,  TRIM(FPARA)
      OPEN(13,FILE=TRIM(FPARA),ERR=9994,STATUS='UNKNOWN')
      DO I=1,4
        READ (12,1000,ERR=9992) TITRE
        WRITE(13,1000,ERR=9991) TRIM(TITRE)
      ENDDO
!
!  COPIE DES PARAMETRES
!
3     READ(11,1000,ERR=9993) TITRE                                                                 ! CHERCHER NBOPH
      IF( TITRE(5:25) .NE. ' ROVIBRONIC OPERATORS' ) GOTO 3                                        ! FORMAT 1025 DE HMODEL
      READ(TITRE(:4),1001)    NBOPH
      READ(12,1000,ERR=9992)        TITRE
      WRITE(13,1001,ERR=9991) NBOPH,TRIM(TITRE(5:))
      READ (12,1000,ERR=9992) TITRE
      WRITE(13,1000,ERR=9991) TRIM(TITRE)
      READ(12,1000,ERR=9992)  TITRE
      TITRE(:32) = FDATE
      WRITE(13,1000,ERR=9991) TRIM(TITRE)
      READ(11,1000,ERR=9993)
      DO I=1,NBOPH
        READ(11,1000,ERR=9993) TITRE
        READ(TITRE(40:40+NBVQN-1),1002) IVC                                                        ! FORMAT 1313 DE HMODEL
        READ(TITRE(51:51+NBVQN-1),1002) IVA                                                        ! FORMAT 1313 DE HMODEL
        NSVQN = 0
        DO J=1,NBVQN
          NSVQN = NSVQN+IVC(J)+IVA(J)
        ENDDO
        WRITE(13,1003,ERR=9991) TITRE(:NBCLAB),NSVQN,TITRE(7:7),TITRE(17:17),TITRE(27:27),  &
                                0,0.D0,0.D0
      ENDDO
      CLOSE(11)
      IF( NBP .EQ. 2 ) THEN
        CLOSE(13)
        GOTO 9000
      ENDIF
      IF    ( OPTION .EQ. 'dip' ) THEN
        FEMRV = 'MD_'//TRIM(PS)//'m'//TRIM(PI)//'_'//DT
      ELSEIF( OPTION .EQ. 'pol' ) THEN
        FEMRV = 'MP_'//TRIM(PS)//'m'//TRIM(PI)//'_'//DT
      ELSE
        PRINT 8007
        GOTO  9999
      ENDIF
      PRINT 2000,  TRIM(FEMRV)
      OPEN(11,FILE=TRIM(FEMRV),ERR=9990,STATUS='OLD')
!
! COPIE DEPUIS Pa_skel
!
      DO I=1,4
        READ (12,1000,ERR=9992) TITRE
        WRITE(13,1000,ERR=9991) TRIM(TITRE)
      ENDDO
!
!  COPIE DES PARAMETRES
!
9     READ(11,1000,ERR=9989) TITRE                                                                 ! CHERCHER NBOPT
      IF( TITRE(5:28) .NE. ' ROVIBRATIONAL OPERATORS' ) GOTO 9                                     ! FORMAT 1220 DIPMOD POLMOD
      READ(TITRE(:4),1001)    NBOPT
      READ(12,1000,ERR=9992)        TITRE
      WRITE(13,1001,ERR=9991) NBOPT,TRIM(TITRE(5:))
      DO I=1,2
        READ (12,1000,ERR=9992) TITRE
        WRITE(13,1000,ERR=9991) TRIM(TITRE)
      ENDDO
      READ(11,1000,ERR=9989)
      DO I=1,NBOPT
        READ(11,1000,ERR=9989) TITRE
        READ(TITRE(40:40+NBVQN-1),1002) IVC                                                        ! FORMAT 1313 DIPMOD POLMOD
        READ(TITRE(51:51+NBVQN-1),1002) IVA                                                        ! FORMAT 1313 DIPMOD POLMOD
        NSVQN = 0
        DO J=1,NBVQN
          NSVQN = NSVQN+IVC(J)+IVA(J)
        ENDDO
        WRITE(13,1003,ERR=9991) TITRE(:NBCLAB),NSVQN,TITRE(7:7),TITRE(17:17),TITRE(27:27),  &
                                0,0.D0,0.D0
      ENDDO
      CLOSE(11)
      CLOSE(13)
      GOTO 9000
!
9989  PRINT 8012
      GOTO  9999
9990  PRINT 8011
      GOTO  9999
9991  PRINT 8009
      GOTO  9999
9992  PRINT 8008
      GOTO  9999
9993  PRINT 8006
      GOTO  9999
9994  PRINT 8005
      GOTO  9999
9995  PRINT 8004
      GOTO  9999
9996  PRINT 8003
      GOTO  9999
9997  PRINT 8002
      CLOSE(10)
      GOTO  9999
9998  PRINT 8001
9999  PRINT 8000
!
9000  PRINT *
      END PROGRAM PARMK
