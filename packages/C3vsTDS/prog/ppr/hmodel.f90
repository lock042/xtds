      PROGRAM HMODEL
!
!  6.7.88 FORTRAN 77 DU SUN4
!  REV 25 JAN 1990
!  REV    DEC 1992 T.GABARD
!  REV 29 SEP 1994
!  REV    JAN 1995 JPC,CW (PARAMETER)
!
! MODIF 01/99 V. BOUDON ---> SCHEMA DE POLYADE QUELCONQUE.
! MODIFIE 05/09 V. BOUDON, A. EL HILALI ---> XY3Z/C3vs.
!
!      CODAGE DES OPERATEURS ROVIBRATIONNELS DE L'HAMILTONIEN EFFECTIF
!     POUR UNE RESTRICTION DONNEE D'UNE POLYADE VIBRATIONNELLE TYPE CH4
!
!     CALCUL DES 6-C
!
!     CALCUL DES K JMKCUB = FIXE DANS LE PROGRAMME
!
! APPEL : hmodel Pn Nm  Dk  v1  v2  v3  v4  v5  v6   ...   w1  w2  w3  w4  w5  w6            |
!                   Nm' Dk' v1' v2' v3' v4' v5' v6'  ...   w1' w2' w3' w4' w5' w6'           | (n+1 lines)
!                   ...........................                                              |
!
!  *****  LIMITATIONS DU PROGRAMME GEREES PAR PARAMETER.
!
!     MXOPH  = NB MAXIMUM D'OPERATEURS ROVIBRATIONNELS
!               (ICODVI,ICODRO,ICODVC,ICODVA,ICODSY)
!     MXOCV  = NB MAXIMUM D'OPERATEURS ROVIBRATIONNELS PAR CASE
!              VIBRATIONNELLE
!               (IRDRE,IDVI,IDRO,IDVC,IDVA,IDSY)
!     MXEMR  = NB MAXIMUM D'E.M.R.V. NON NULS
!               (EMOP,LI,IC)
!     MXSNV  = NB MAXIMUM TOTAL DE SOUS-NIVEAUX VIBRATIONNELS
!               (IFF)
!     MXSNB  = NB MAXIMUM DE SOUS-NIVEAUX VIBRATIONNELS POUR UN
!              ENSEMBLE DONNE DES VI
!               (IKVC,IKVA)
!     MXOPR  = NB MAXIMUM D'OPERATEURS ROVIBRATIONNELS PAR OPERATEUR
!              VIBRATIONNEL
!               (IROCO)
!     MXNIV  = NB MAXIMUM DE NIVEAUX VIBRATIONNELS DANS UNE POLYADE DONNEE
!               (NVIB)
!     MXPOL  = NB MAXIMUM DE POLYADES
!               (NIVI,NVIB)
!
      use mod_dppr
      use mod_par_tds
      use mod_com_fdate
      use mod_com_phase
      use mod_main_hmodel
      IMPLICIT NONE

! functions
      real(kind=dppr)  :: ELMR
      integer          :: KICTR
      character(len =   3)  :: KC
      character(len =   5)  :: KCE

      real(kind=dppr) ,parameter  :: SYML = 1.D0
      real(kind=dppr) ,parameter  :: SYMS = 0.5D0
      real(kind=dppr) ,parameter  :: OL   = 1.D0
      real(kind=dppr) ,parameter  :: OS   = 0.5D0

      real(kind=dppr) ,dimension(MDMIGA) :: AGAMA,AKE,AKEV,AKKT,AKLS,AKT
      real(kind=dppr)  :: AKL,AKM,AKV,AKS
      real(kind=dppr)  :: EM,EM1,EM2,EME,EMET,EMP,EMRT,EN
      real(kind=dppr)  :: SP

      integer ,dimension(NBVQN+1)  :: IBELMR,ICELMR,IAELMR,IKELMR
      integer ,dimension(NBVQN)    :: ILA,IL,ILC
      integer ,dimension(NBVQN)    :: IV,IVA,IVC
      integer ,dimension(MXSYR)    :: IELL,IELS,IROT
      integer ,dimension(MXPOL)    :: IORH,NIVI
      integer          :: NF_SUP,NFF_SUP,IOP_SUP,NROT_SUP,NBOPH_SUP
      integer          :: IEE_SUP,IELLA_SUP,IELSA_SUP
      integer          :: IROTA_SUP,NIV_SUP
      integer          :: I,IAE,IAEV,IAV,IB,IBE,IBEV,IBL,IBLL,IBV,ICRT
      integer          :: ID,IDEGV,IDK,IDL,IDLL
      integer          :: IDLS,IDRL,IDROT,IDS,IEE,IELLA,IELSA,IF,IF1,IF2,IGA,IH,IJK,IK,IK45,IK456
      integer          :: IKF,IKL,IKS,ILL,ILO,ILS,IM,IMIA
      integer          :: IOA,IOA1,IOC,IOC1,IOP,IOPA,IOPC,IP,IPH,IPOL,IPP,IRO
      integer          :: IR,IRLL,IROTA,IS,ISO,IU,IUU,IVI
      integer          :: IKA45,IKA456,IKC45,IKC456
      integer          :: J,J1,J2,J3,JDLS,JJJ,JKL,JLL,JLS,JMKCUB
      integer          :: KEV,KNVIB,KNIVP,KPOL,KOP,KTOT
      integer          :: L,LOP
      integer          :: M,MEV
      integer          :: NBOPH,NBVA,NBVC,NE,NEV,NEVT,NF,NFF,NK,NGAMA,NN
      integer          :: NPOL,NROT

      character(len =   1)  :: AVI,AORH
      character(len =  11)  :: CHAINE
      character(len = 120)  :: IFICH
!
1000  FORMAT('   EFFECTIVE ROVIBRATIONIC HAMILTONIAN :', /,   &
             ' VIBRONIC REDUCED MATRIX ELEMENTS (RME)' ,//,   &
             A                                             )
1001  FORMAT(//)
1002  FORMAT(I6,'  |[[[',                        &
             I2  ,'(',I1  ,',',A,')*',           &
             I2  ,'(',I1  ,',',A,')*',           &
             I2  ,'(',I1  ,',',A,')*',           &
             I2  ,'(',I1  ,',',A,')*',           &
             I2  ,'(',I1  ,',',A,')]',A,'*',     &
             I2  ,'(',I1  ,',',A,')]',A,'*[',    &
             I2  ,'(',I2  ,',',A,')*',           &
             F3.1,'(',F3.1,',',A,')]',A,']',A,   &
             ' >'                             )
1004  FORMAT(I4,' TH VIBRONIC OPERATOR'        ,//,   &
             A,1X,' {[[',                             &
             I2,'(',I1,',',A,')*',                    &
             I2,'(',I1,',',A,')*',                    &
             I2,'(',I1,',',A,')*',                    &
             I2,'(',I1,',',A,')*',                    &
             I2,'(',I1,',',A,')]',A,'*'               &
             I2,'(',I1,',',A,')]',A,                  &
             '} '                              , /,   &
             2X,' {[[',                               &
             I2,'(',I1,',',A,')*',                    &
             I2,'(',I1,',',A,')*',                    &
             I2,'(',I1,',',A,')*',                    &
             I2,'(',I1,',',A,')*',                    &
             I2,'(',I1,',',A,')]',A,'*',              &
             I2,'(',I1,',',A,')]',A,'}',A,'*[[',      &
             I2,'(',I1,',',A,')','*',                 &
             I2,'(',I1,',',A,')',']',A,']',A       )
1005  FORMAT(/,              &
             'HMODEL : ',A)
1025  FORMAT(I4,' ROVIBRONIC OPERATORS',/)
1026  FORMAT(/,            &
             1X, 46('-'))
1027  FORMAT(/,                                          &
             ' HAMILTONIAN DEVELOPMENT ORDER :  ',10I2)
1028  FORMAT(A)
1029  FORMAT(/,                     &
             I5,' NON ZERO RME',/)
1030  FORMAT(2X,A1,'<',I5,'|',3X,'|',I5,'>',3X,F19.13)
1031  FORMAT(/,                               &
             I4,' ROTATIONAL OPERATOR(S)',/)
1032  FORMAT(I1)
1033  FORMAT(/,                         &
             ' POLYAD NUMBER : ',I2,/)
1034  FORMAT(5X,'(v1;v2;v3;v4;v5;v6) = (',5(I2,';'),I2,')')                                        ! ATTENTION respecter cette regle dans tous les packages pour VAMDC
1035  FORMAT(I2)
1100  FORMAT(/,                                  &
             I4,' VIBRATIONIC SUBLEVEL(S) :',/)
1313  FORMAT(I4,I3,'(',I1,',',A,')',                              &
                I3,'(',I1,',',A,')',                              &
                I3,'(',I1,',',A,')',1X,A,                         &
             1X,6I1,1X,A,1X,6I1,1X,A,1X,A,1X,A,1X,A,I8,I8,I8,I8)                                   ! 6 = NBVQN
2001  FORMAT(' >> ',A)
8000  FORMAT(' !!! HMODEL : STOP ON ERROR')
8001  FORMAT(' !!! ERROR OPENING CONTROL FILE')
8002  FORMAT(' !!! INCOMPLETE CONTROL FILE')
!
! ARRAY INITIAL ALLOCATION
!
      CALL ALLOC_ALL
!
      NF_SUP    = -1
      NFF_SUP   = -1
      IOP_SUP   = -1
      NROT_SUP  = -1
      NBOPH_SUP = -1
      IROTA_SUP = -1
      IELSA_SUP = -1
      IELLA_SUP = -1
      NIV_SUP   = -1
      IEE_SUP   = -1
      OPEN(10,FILE=CTRLF,ERR=9998,STATUS='OLD')
      READ(10,1028,END=9997) FDATE
      PRINT 1005,            FDATE
!
! NUMERO DE LA POLYADE COURANTE
!
      READ(10,1028,END=9997) CHAINE
      READ (CHAINE(2:2),1032) IVI
      WRITE(AVI,1032)         IVI
!
! NOMBRE TOTAL DE POLYADES
!
      NPOL = IVI+1
!
! NOM DU FICHIER DE SORTIE
!
      IFICH = 'MH_P'//AVI//'_D'
!
! LECTURE DES NIVEAUX VIBRATIONNELS
!
      JMKCUB = 0
      DO KPOL=1,NPOL
        READ(10,1028,END=9997) CHAINE
        READ(CHAINE(2:3),1035) NIVI(KPOL)
        IF( NIVI(KPOL) .GT. NIV_SUP ) NIV_SUP = NIVI(KPOL)
        DO WHILE( NIVI(KPOL) .GT. MXNIV )
          CALL RESIZE_MXNIV
        ENDDO
        READ(10,1028,END=9997) CHAINE
        READ (CHAINE(2:2),1032) IORH(KPOL)
        WRITE(AORH,1032)        IORH(KPOL)
        IFICH = TRIM(IFICH)//AORH
        DO KNIVP=1,NIVI(KPOL)
          DO KNVIB=1,NBVQN
            READ(10,1032,END=9997) NVIB(KPOL,KNIVP,KNVIB)
            IF( KNVIB .GT. 2 ) JMKCUB = MAX(JMKCUB,NVIB(KPOL,KNIVP,KNVIB))
          ENDDO
        ENDDO
      ENDDO
      CLOSE(10)
      PRINT 2001, TRIM(IFICH)
!
! INITIALISATIONS
!
      CALL FACTO
      OPEN(20,FILE=IFICH,STATUS='UNKNOWN')
      WRITE(20,1000) FDATE
      WRITE(20,1033) IVI
      DO I=1,NIVI(NPOL)
        WRITE(20,1034) (NVIB(NPOL,I,J),J=1,NBVQN)
      ENDDO
!
! CODAGE DES SOUS-NIVEAUX VIBRATIONNELS
!
      NFF = 0
      DO IF1=1,NIVI(NPOL)
        DO IF2=1,NBVQN
          IV(IF2) = NVIB(NPOL,IF1,IF2)
        ENDDO
        CALL SIGVI(IV,'IFF',NFF,NF)
        NFF = NFF+NF
        IF( NF  .GT. NF_SUP  ) NF_SUP  = NF
        IF( NFF .GT. NFF_SUP ) NFF_SUP = NFF
      ENDDO
!CC***
      NEVT = 0
      IEE  = 0
      DO IF=1,NFF
        CALL VLNC(IFF(7,IF),IK45,IK456)
        CAll MULCIV (SYML,SYMS,NK,AKE)
        DO L=1,NK
          AKL = AKE(L)
          CALL MULCIV(DBLE(IK456-2),AKL,NEV,AKT)
          NEVT = NEVT+NEV
        ENDDO
      ENDDO
      WRITE(20,1100) NEVT
!
! NFF est le nombre de sous-niveaux vibrationnels
!
      DO IF=1,NFF
        DO I=1,NBVQN
          CALL VLNC(IFF(I,IF),IV(I),IL(I))
        ENDDO
        CALL VLNC(IFF(NBVQN+1,IF),IK45,IK456)
        CAll MULCIV(SYML,SYMS,NK,AKE)
        DO L=1,NK
          AKL = AKE(L)
          CALL MULCIV(DBLE(IK456-2),AKL,NEV,AKT)
          DO M=1,NEV
            AKM = AKT(M)
            IEE = IEE+1
            IF( IEE .GT. IEE_SUP ) IEE_SUP = IEE
            IF( IEE .GT. MXSNEV  ) CALL RESIZE_MXSNEV
            IND(IEE) = IF
            WRITE(20,1002) IEE,                                         &
                           (IV(I),IL(I),KC(IL(I)+2),I=1,5),KC(IK45),    &
                            IV(6),IL(6),KC(IL(6)+2)       ,KC(IK456),   &
                           INT(SYML),INT(SYML),KCE(3),                  &
                           SYMS,SYMS,                                   &
                           KCE(MXSYR+1),KCE(INT(AKL+(MXSYR+0.5D0))),    &
                           KCE(INT(AKM+(MXSYR+0.5D0))               )
            ICODE(IEE) = INT(AKL+(MXSYR+0.5D0))*10000+  &
                         INT(AKM+(MXSYR+0.5D0))*10+     &
                         IK456
          ENDDO
        ENDDO
      ENDDO
      WRITE(20,1027) (IORH(IPOL),IPOL=1,NPOL)
      LOP   = 0
      NBOPH = 0
!
! BOUCLE SUR LES POLYADES
!
E36:  DO KPOL=1,NPOL
!
! DETERMINATION DES CREATEURS
!
E35:    DO IOC=1,NIVI(KPOL)
          DO IOC1=1,NBVQN
            IVC(IOC1) = NVIB(KPOL,IOC,IOC1)
          ENDDO
          CALL SIGVI(IVC,'IKVC',0,NBVC)
!
! DETERMINATION DES ANNIHILATEURS
!
E33:      DO IOA=1,NIVI(KPOL)
            DO IOA1=1,NBVQN
              IVA(IOA1) = NVIB(KPOL,IOA,IOA1)
            ENDDO
            IF( IOA .LT. IOC ) CYCLE E33
            CALL SIGVI(IVA,'IKVA',0,NBVA)
            IDEGV = 0
            DO IOP=1,NBVQN
              IDEGV = IVA(IOP)+IVC(IOP)+IDEGV
            ENDDO
!
! COUPLAGE CREATEURS-ANNIHILATEURS
!
E1:         DO IOPC=1,NBVC
              IMIA = 1
              IF( IOA .EQ. IOC ) IMIA = IOPC
              DO I=1,NBVQN
                CALL VLNC(IKVC(I,IOPC),IVC(I),ILC(I))
              ENDDO
              CALL VLNC(IKVC(NBVQN+1,IOPC),IKC45,IKC456)
E2:           DO IOPA=IMIA,NBVA
                DO I=1,NBVQN
                  CALL VLNC(IKVA(I,IOPA),IVA(I),ILA(I))
                ENDDO
                CALL VLNC(IKVA(NBVQN+1,IOPA),IKA45,IKA456)
                CALL MULCIV(DBLE(IKC456-2),DBLE(IKA456-2),NGAMA,AGAMA)
                NROT = 0
E111:           DO I=1,NGAMA
E733:             DO IU=1,2
                    DO IJK=1,MXSYR
                      IROT(IJK) = 0
                      IELL(IJK) = 0
                      IELS(IJK) = 0
                    ENDDO
!
! DETERMINATION DES OPERATEURS ELECTRONIQUE DE SPIN POSSIBLES
! ****E(OMEGAS,K,L)=E(IDES,IDES,ILS)***
!
                    IELSA = 0
                    DO IDLS=0,1
                      DO IDS=0,IDLS
                        ILS = IDS
                        IF( IDLS-(IDLS/2)*2 .EQ. 1 .AND.             &
                            IDS             .EQ. 0       ) ILS = -1
                        IELSA       = IELSA+1
                        IELS(ILS+2) = IELS(ILS+2)+1
                        IF( IELSA .GT. IELSA_SUP ) IELSA_SUP = IELSA
                        IF( IELSA .GT. MXOPR     ) CALL RESIZE_MXOPR
                        IELCOS(IELSA) = 100*IDLS+10*IDLS+ILS+2
                      ENDDO
                    ENDDO
!
! DETERMINATION DES OPERATEURS ELECTRONIQUE DE SPIN POSSIBLES
! ****E(OMEGAS,K,L)=E(IDES,IDES,ILS)***
!
                    IELLA = 0
                    DO IKL=0,2
                      DO IDL=0,IKL
                        ILL = IDL
                        IF( IKL-(IKL/2)*2 .EQ. 1 .AND.             &
                            IDL           .EQ. 0       ) ILL = -1
                        IELLA       = IELLA+1
                        IELL(ILL+2) = IELL(ILL+2)+1
                        IF( IELLA .GT. IELLA_SUP ) IELLA_SUP = IELLA
                        IF( IELLA .GT. MXOPR     ) CALL RESIZE_MXOPR
                        IELCOL(IELLA) = 100*IKL+10*IKL+ILL+2
                      ENDDO
                    ENDDO
!
E79:                DO ILO=1,IELLA
                      JKL = IELCOL(ILO)/100
                      JLL = IELCOL(ILO)-(IELCOL(ILO)/10)*10
E78:                  DO ISO=1,IELSA
                        JDLS = IELCOS(ISO)/100
                        JLS  = IELCOS(ISO)-(IELCOS(ISO)/10)*10
                        CALL MULCIV(DBLE(JLL-2),DBLE(JLS-2),NE,AKLS)
E123:                   DO IR=1,NE
                          AKS = AKLS(IR)
                          CALL MULCIV(AGAMA(I),AKS,MEV,AKEV)
E1234:                    DO IM=1,MEV
                            AKV   = AKEV(IM)
                            IUU   = (-1)**(JDLS+JKL+IU-1)
                            IP    = ABS((IUU-1)/2)
                            IDROT = IORH(KPOL)+2-IDEGV-JDLS-JKL
!C
!CCC****  DETERMINATION DES OPERATEURS ROTATIONNELS POSSIBLES
!C
                            IROTA = 0
                            IF( IDROT .LT. IU-1 ) CYCLE E78
E334:                       DO ID=IP,IDROT,2
                              IF( JDLS  .EQ. 0 .AND.             &
                                  JKL   .EQ. 0 .AND.             &
                                  IDEGV .EQ. 0 .AND.             &
                                  ID    .EQ. 0       ) CYCLE E334
                              DO IDK=ID,IP,-2
                                DO IDRL=0,IDK
                                  IDLL = IDRL
                                  IF( IDK-(IDK/2)*2 .NE. 0 .AND.              &
                                      IDRL          .EQ. 0       ) IDLL = -1
                                  IROTA        = IROTA+1
                                  IROT(IDLL+2) = IROT(IDLL+2)+1
                                  IF( IROTA .GT. IROTA_SUP ) IROTA_SUP = IROTA
                                  IF( IROTA .GT. MXOPR     ) CALL RESIZE_MXOPR
                                  IROCO(IROTA) = 1000*ID+100*IDK+10*(IDLL+2)+INT(AKS)+2
                                ENDDO
                              ENDDO
                            ENDDO E334
! CALCUL DU FACTEUR DE NORMALISATION
!      IU = 1, EPSILON = +
!      IU = 2, EPSILON = -
                            IF( IROTA            .EQ. 0 .OR.            &
                                IROT(INT(AKV)+2) .EQ. 0      ) CYCLE E78
                            SP        = (-1)**(IU-1+IKA456+IKC456+INT(AGAMA(I)))
                            IBELMR(:) = IKVC(:,IOPC)
                            ICELMR(:) = IKVC(:,IOPC)
                            IAELMR(:) = IKVA(:,IOPA)
                            IKELMR(:) = IKVA(:,IOPA)
                            EN        = ELMR(IBELMR,ICELMR,IAELMR,IKELMR,INT(AGAMA(I)))
                            IBELMR(:) = IKVC(:,IOPC)
                            ICELMR(:) = IKVA(:,IOPA)
                            IKELMR(:) = IKVC(:,IOPC)
                            IKELMR(:) = IKVA(:,IOPA)
                            EN        = ELMR(IBELMR,ICELMR,IAELMR,IKELMR,INT(AGAMA(I)))*SP+EN
                            EMET      = 0.D0
                            DO IB=1,IEE
                              DO IK=IB,IEE
                                IAE  = ICODE(IB)/10000
                                IBE  = ICODE(IK)/10000
                                IAEV = ICODE(IB)/10-(ICODE(IB)/10000)*1000
                                IBEV = ICODE(IK)/10-(ICODE(IK)/10000)*1000
                                IAV  = ICODE(IB)-(ICODE(IB)/10)*10
                                IBV  = ICODE(IK)-(ICODE(IK)/10)*10
                                CALL EMRVIB(DBLE(IAE)-(MXSYR+0.5D0),IAV-2,DBLE(IAEV)-(MXSYR+0.5D0),   &
                                            JDLS,JLS-2,JKL,JLL-2,INT(AKS),INT(AGAMA(I)),INT(AKV),     &
                                            DBLE(IBE)-(MXSYR+0.5D0),IBV-2,DBLE(IBEV)-(MXSYR+0.5D0),   &
                                            EME,IKF                                                )
                                EMET = EMET+ABS(EME)
                              ENDDO
                            ENDDO
                            IF( ABS(EN*EMET) .LT. 1.D-05 ) CYCLE E1234
                            LOP = LOP+1
                            WRITE(20,1026)
                            WRITE(20,1004) LOP,AUG(IU),                                    &
                                           (IVC(J),ILC(J),KC(ILC(J)+2),J=1,5),KC(IKC45),   &
                                            IVC(6),ILC(6),KC(ILC(6)+2)       ,KC(IKC456),  &
                                           (IVA(J),ILA(J),KC(ILA(J)+2),J=1,5),KC(IKA45),   &
                                            IVA(6),ILA(6),KC(ILA(6)+2)       ,KC(IKA456),  &
                                           KC(INT(AGAMA(I))+2),JDLS,JDLS,KC(JLS),JKL,JKL,  &
                                           KC(JLL),KC(INT(AKS)+2),KC(INT(AKV)+2)
!
! CALCUL ET CODAGE DES E.M.R.V.
!
                            IOP = 0
                            DO IB=1,IEE
E5:                           DO IK=IB,IEE
                                IAE  = ICODE(IB)/10000
                                IBE  = ICODE(IK)/10000
                                IAEV = ICODE(IB)/10-(ICODE(IB)/10000)*1000
                                IBEV = ICODE(IK)/10-(ICODE(IK)/10000)*1000
                                IAV  = ICODE(IB)-(ICODE(IB)/10)*10
                                IBV  = ICODE(IK)-(ICODE(IK)/10)*10
!
!CCC*****   EMR VIBRATIONNELS *****
!
                                IBELMR(:) = IFF(:,IND(IB))
                                ICELMR(:) = IKVC(:,IOPC)
                                IAELMR(:) = IKVA(:,IOPA)
                                IKELMR(:) = IFF(:,IND(IK))
                                EM1       = ELMR(IBELMR,ICELMR,IAELMR,IKELMR,INT(AGAMA(I)))
                                IBELMR(:) = IFF(:,IND(IB))
                                ICELMR(:) = IKVA(:,IOPA)
                                IAELMR(:) = IKVC(:,IOPC)
                                IKELMR(:) = IFF(:,IND(IK))
                                EM2       = ELMR(IBELMR,ICELMR,IAELMR,IKELMR,INT(AGAMA(I)))*SP
                                EM        = EM1+EM2
                                IF( ABS(EM) .LT. 1.D-10 ) CYCLE E5
                                EM = EM/EN
                                IF( IU .EQ. 2 ) EM = -EM
!CC
!CC***************   FIN EMR VIBRATIONNELS  ************
!C
                                CALL EMRVIB(DBLE(IAE)-(MXSYR+0.5D0),IAV-2,DBLE(IAEV)-(MXSYR+0.5D0),   &
                                            JDLS,JLS-2,JKL,JLL-2,INT(AKS),INT(AGAMA(I)),INT(AKV),     &
                                            DBLE(IBE)-(MXSYR+0.5D0),IBV-2,DBLE(IBEV)-(MXSYR+0.5D0),   &
                                            EMP,IPP                                                )
!CC
!CCCCC************   EMR VIBRONIQUES       ***********
!CCC
                                EMRT = EM*EMP
                                IH   = ABS(IU-IPP)
                                IPH  = IH-(IH/2)*2
                                IF( IU  .EQ. 2 .AND.                 &
                                    IPP .EQ. 1       ) EMRT = -EMRT
                                IF( ABS(EMRT) .LT. 1.D-10 ) CYCLE E5
                                IOP = IOP+1
                                IF( IOP .GT. IOP_SUP ) IOP_SUP = IOP
                                IF( IOP .GT. MXEMR   ) CALL RESIZE_MXEMR
                                LI(IOP)   = IK
                                IC(IOP)   = IB
                                EMOP(IOP) = EMRT
                              ENDDO E5
                            ENDDO
                            WRITE(20,1029) IOP
                            DO KOP=1,IOP
                              WRITE(20,1030) AIM(IPH+1),IC(KOP),LI(KOP),EMOP(KOP)
                            ENDDO
                            WRITE(20,1031) IROTA
!
! CODAGE DES OPERATEURS ROVIBRATIONNELS
!
                            DO IRO=1,IROTA
                              ICRT = IROCO(IRO)
                              IRLL = ICRT/10-(ICRT/100)*10-2
                              CALL MULCIV(DBLE(IRLL),AKV,NN,AKKT)
E632:                         DO L=1,NN
                                IF( KICTR(AKKT(L),1) .NE. 1 ) CYCLE E632
                                 NROT = NROT+1
                                 IF( NROT .GT. NROT_SUP ) NROT_SUP = NROT
                                 IF( NROT .GT. MXOCV    ) CALL RESIZE_MXOCV
                                 IDELS(NROT) = IELCOS(ISO)
                                 IDELL(NROT) = IELCOL(ILO)
                                 IDRO(NROT)  = IROCO(IRO)
                                 IDVC(NROT)  = IVC(1)*100000+IVC(2)*10000+IVC(3)*1000+  &
                                               IVC(4)*100+IVC(5)*10+IVC(6)
                                 IDVA(NROT)  = IVA(1)*100000+IVA(2)*10000+IVA(3)*1000+  &
                                               IVA(4)*100+IVA(5)*10+IVA(6)
                                 IDSY(NROT)  = IKC456*100+IKA456*10+INT(AGAMA(I))+2
                                 IDVI(NROT)  = LOP*10000+IKC456*1000+(INT(AKV)+2)*100+  &
                                               INT(AGAMA(I)+2)*10+INT(AKKT(L))+2
                              ENDDO E632
                            ENDDO
                          ENDDO E1234
                        ENDDO E123
                      ENDDO E78
                    ENDDO E79
                  ENDDO E733
                ENDDO E111
!
! MISE EN ORDRE A L'INTERIEUR D'UNE CASE VIBRATIONNELLE DONNEE
!
                IF( NROT .EQ. 0 ) CYCLE E2
                CALL IRDER(NROT,IDRO,IRDRE,MXOCV)
                DO JJJ=1,NROT
                  NBOPH = NBOPH+1
                  IF( NBOPH .GT. NBOPH_SUP ) NBOPH_SUP = NBOPH
                  IF( NBOPH .GT. MXOPH     ) CALL RESIZE_MXOPH
                  IRO           = IRDRE(JJJ)
                  ICODLS(NBOPH) = IDELS(IRO)
                  ICODLL(NBOPH) = IDELL(IRO)
                  ICODRO(NBOPH) = IDRO(IRO)
                  ICODVC(NBOPH) = IDVC(IRO)
                  ICODVA(NBOPH) = IDVA(IRO)
                  ICODSY(NBOPH) = IDSY(IRO)
                  ICODVI(NBOPH) = IDVI(IRO)
                ENDDO
              ENDDO E2
            ENDDO E1
          ENDDO E33
        ENDDO E35
      ENDDO E36
      CALL DEBUG( 'HMODEL => MXOPVH=',LOP)
!
! STOCKAGE DES CODES DES OPERATEURS ROVIBRATIONNELS
!
      WRITE(20,1001)
      WRITE(20,1025) NBOPH
      DO IRO=1,NBOPH
!CCCC*** PARTIE ROTATIONNELLE********
        IM  = ICODRO(IRO)/1000
        IK  = ICODRO(IRO)/100-(ICODRO(IRO)/1000)*10
        IDL = ICODRO(IRO)/10-(ICODRO(IRO)/100)*10
        IKS = ICODRO(IRO)-(ICODRO(IRO)/10)*10
!CCC***** PARTIE ELECTRONIQUE DE SPIN****
        IS  = ICODLS(IRO)/100
        ILS = ICODLS(IRO)-(ICODLS(IRO)/10)*10
!CCC***** PARTIE ELECTRONIQUE ORBITALE ****
        IBL  = ICODLL(IRO)/100
        IBLL = ICODLL(IRO)-(ICODLL(IRO)/10)*10
!CCC***** PARTIE ELECTRONIQUE ORBITALE ****
        KEV  = ICODVI(IRO)/100-(ICODVI(IRO)/1000)*10
        KTOT = ICODVI(IRO)-(ICODVI(IRO)/10)*10
        CALL V1TO6(ICODVC(IRO),IVC(1),IVC(2),IVC(3),IVC(4),IVC(5),IVC(6))
        CALL V1TO6(ICODVA(IRO),IVA(1),IVA(2),IVA(3),IVA(4),IVA(5),IVA(6))
        CALL V1TO6(ICODSY(IRO),J1,J2,J3,IKC456,IKA456,IGA)
        WRITE(20,1313) IRO,                                             &
                       IM,IK,KC(IDL),                                   &
                       IS,IS,KC(ILS),                                   &
                       IBL,IBL,KC(IBLL),KC(IKS),                        &
                       (IVC(L),L=1,NBVQN),KC(IKC456),                   &
                       (IVA(L),L=1,NBVQN),KC(IKA456),                   &
                       KC(IGA),KC(KEV),KC(KTOT),                        &
                       ICODRO(IRO),ICODVI(IRO),ICODLS(IRO),ICODLL(IRO)
      ENDDO
      CLOSE(20)
      CALL DEBUG( 'HMODEL => MXSNB=',NF_SUP)
      CALL DEBUG( 'HMODEL => MXSNV=',NFF_SUP)
      CALL DEBUG( 'HMODEL => MXEMR=',IOP_SUP)
      CALL DEBUG( 'HMODEL => MXOCV=',NROT_SUP)
      CALL DEBUG( 'HMODEL => MXOPH=',NBOPH_SUP)
      CALL DEBUG( 'HMODEL => MXOPR=',IELSA_SUP)
      CALL DEBUG( 'HMODEL => MXOPR=',IELLA_SUP)
      CALL DEBUG( 'HMODEL => MXOPR=',IROTA_SUP)
      CALL DEBUG( 'HMODEL => MXNIV=',NIV_SUP)
      CALL DEBUG( 'HMODEL => MXSNEV=',IEE_SUP)
      GOTO 9000
!
9997  PRINT 8002
      CLOSE(10)
      GOTO  9999
9998  PRINT 8001
9999  PRINT 8000
!
9000  PRINT *
      END PROGRAM HMODEL
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! 05/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3v.
!
! *** COUPLAGE DES OPERATEURS CREATIONS ET ANNHILATIONS
!
! ***   < IB !! (IC * IA)IGAMA !! IK > = (-1)**(ICB+ICK+IC+IA) * SQRT(IGAMA)
!
!                        ( ICC , ICA ,IGAMA)
! ***   SOMME SUR IBK DE (                 ) <IB!!IC!!IBK><IBK!!IA!!IK>
!                        ( ICB , ICK , ICBK)
!
! ***   AVEC IX(I)       = 1000*VI+100*LI+10*NI+CI     POUR I=1,NBVQN
! ***   ET   IX(NBVQN+1) = 1000*C1+100*C2+10*C23+C234
! ***   AVEC C23=C2*C3   ET   C234=C23*C4
! ***   IX PEUT ETRE  IB,IA,IC OU IK
!
      FUNCTION ELMR(IB,IC,IA,IK,IGAMA)
      use mod_dppr
      use mod_par_tds
      use mod_com_sigvi
      IMPLICIT NONE
      real(kind=dppr)  :: ELMR
      integer ,dimension(NBVQN+1)  :: IB,IC,IA,IK
      integer          :: IGAMA

! functions
      real(kind=dppr)  :: E1TO6
      integer          :: IPSIK

      real(kind=dppr)  :: EM1,EM2
      real(kind=dppr)  :: P
      real(kind=dppr)  :: SIX

      integer ,dimension(NBVQN)          :: IPB
      integer ,dimension(NBVQN,4)        :: IBCAK
      integer          :: I,ICA,ICB,ICC,ICK,ICPK,IDGAM,IFF,II,IL,IV
      integer          :: J
      integer          :: NB
!
      ELMR = 0.D0
!
! *** TEST SUR LES FONCTIONS ET LES OPERATEURS.
!
      DO I=1,NBVQN
        IBCAK(I,1) = IB(I)
        IBCAK(I,2) = IC(I)
        IBCAK(I,3) = IA(I)
        IBCAK(I,4) = IK(I)
      ENDDO
!
! TEST DE LA COHERENCE DE L'ENSEMBLE DES NOMBRES QUANTIQUES
!
      DO J=1,4
        DO I=1,NBVQN
          CALL VLNC(IBCAK(I,J),IV,IL)
          II = 1
          IF( I               .GT. 3 ) II = 2
          IF( IPSIK(II,IV,IL) .EQ. 0 ) RETURN
        ENDDO
      ENDDO
!
! *** TEST SUR LES VI ET LES OMEGAI
!
      DO I=1,NBVQN
        IPK(I,1) = IB(I)/10-IC(I)/10
        IPB(I)   = IK(I)/10-IA(I)/10
        IF( IPB(I)   .LT. 0      ) RETURN
        IF( IPK(I,1) .NE. IPB(I) ) RETURN
      ENDDO
      ICB   = IB(NBVQN+1)-10*(IB(NBVQN+1)/10)
      ICK   = IK(NBVQN+1)-10*(IK(NBVQN+1)/10)
      ICC   = IC(NBVQN+1)-10*(IC(NBVQN+1)/10)
      ICA   = IA(NBVQN+1)-10*(IA(NBVQN+1)/10)
      IDGAM = 2
      IF( IGAMA .LT. 1 ) IDGAM = 1
      P = (-1)**(ICC+ICA+ICB+ICK)*SQRT(DBLE(IDGAM))
!
! *** SOMMATION SUR LES ETATS INTERMEDIAIRES.
!
      CALL SIGVI(IPB,'IPK',0,NB)
      DO I=1,NB
        EM1  = E1TO6(1,IB,IC,IPK(1,I))
        EM2  = E1TO6(-1,IPK(1,I),IA,IK)
        ICPK = IPK(NBVQN+1,I)-10*(IPK(NBVQN+1,I)/10)
        CALL SIX6K(DBLE(ICC-2),DBLE(ICA-2),DBLE(IGAMA),DBLE(ICK-2),  &
                   DBLE(ICB-2),DBLE(ICPK-2),SIX,IFF)
        ELMR = ELMR+P*SIX*EM1*EM2
      ENDDO
!
      RETURN
      END FUNCTION ELMR
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! HMODEL version
!
! 05/09 V. BOUDON, A. EL HILALI ----> XY3Z/C3v.
!
! *** CHERCHE POUR UN ENSEMBLE DONNE DE VI : (IV(I),I=1,4)
! *** LES NB INDICES DE LA SIGNATURE VIBRATIONNELLE
! *** IKNC(I,K), I=1,5 ET K=ISN+1,ISN+NB
!
!  REV 29 SEP 94
!
! ATTENTION : SIGVI peut redimensionner les tableaux dependants de MXC
!
      SUBROUTINE SIGVI(IV,MXC,ISN,NB)
      use mod_dppr
      use mod_par_tds
      use mod_main_hmodel
      IMPLICIT NONE
      integer            ,dimension(NBVQN)  :: IV
      character(len = *)                    :: MXC
      integer                               :: ISN,NB

      integer ,parameter  :: MDMIB = 22

      real(kind=dppr) ,dimension(MDMIGA)  :: AK45,AK456

      integer ,pointer ,dimension(:,:)    :: IKNC
      integer          ,dimension(MDMIB)  :: IB4,IB5,IB6
      integer                             :: I4,I45,I456,I5,I6
      integer                             :: IK4,IK5,IK6
      integer                             :: K
      integer                             :: MDMKNC
      integer                             :: N45,N456
      integer                             :: NB4,NB5,NB6
!
8000  FORMAT(' !!! SIGVI  : STOP ON ERROR'   ,/,   &
             '              ',A,' UNEXPECTED'   )
!
      SELECT CASE( MXC )
        CASE( 'IFF' )
          MDMKNC = MXSNV
          IKNC => IFF
        CASE( 'IKVA' )
          MDMKNC = MXSNB
          IKNC => IKVA
        CASE( 'IKVC' )
          MDMKNC = MXSNB
          IKNC => IKVC
        CASE( 'IPK' )
          MDMKNC = MXSNB
          IKNC => IPK
        CASE DEFAULT
          PRINT 8000, MXC
          STOP
!
      END SELECT
      K = ISN
      CALL TCUBE(4,IV(4),NB4,IB4,MDMIB)
      CALL TCUBE(5,IV(5),NB5,IB5,MDMIB)
      CALL TCUBE(6,IV(6),NB6,IB6,MDMIB)
      DO I4=1,NB4
        IK4 = IB4(I4)-10*(IB4(I4)/10)
        DO I5=1,NB5
          IK5 = IB5(I5)-10*(IB5(I5)/10)
          CALL MULCIV(DBLE(IK4),DBLE(IK5),N45,AK45)
          DO I45=1,N45
            DO I6=1,NB6
              IK6 = IB6(I6)-10*(IB6(I6)/10)
              CALL MULCIV(AK45(I45),DBLE(IK6),N456,AK456)
E2:           DO I456=1,N456
                K = K+1
                IF( K .GT. MDMKNC ) THEN
                  SELECT CASE( MXC )
                    CASE( 'IFF' )
                      CALL RESIZE_MXSNV
                      MDMKNC = MXSNV
                      IKNC => IFF
                    CASE( 'IKVA' )
                      CALL RESIZE_MXSNB
                      MDMKNC = MXSNB
                      IKNC => IKVA
                    CASE( 'IKVC' )
                      CALL RESIZE_MXSNB
                      MDMKNC = MXSNB
                      IKNC => IKVC
                    CASE( 'IPK' )
                      CALL RESIZE_MXSNB
                      MDMKNC = MXSNB
                      IKNC => IPK
                  END SELECT
                ENDIF
                IKNC(1,K) = 10*IV(1)
                IKNC(2,K) = 10*IV(2)
                IKNC(3,K) = 10*IV(3)
                IKNC(4,K) = IB4(I4)
                IKNC(5,K) = IB5(I5)
                IKNC(6,K) = IB6(I6)
                IKNC(7,K) = (INT(AK45(I45))+2)*10+INT(AK456(I456))+2
              ENDDO E2
            ENDDO
          ENDDO
        ENDDO
      ENDDO
      NB = K-ISN
!
      RETURN
      END SUBROUTINE SIGVI
