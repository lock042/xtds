      PROGRAM HMATRI
!
!  VERSION POUR TEST DE RAPIDITE DANS DIRECTORY COURANT
!  OPTIMISATION DE SXC ET KCUBU VISANT A EVITER LES
!  CALCULS REDONDANTS ET CONSISTANT A STOCKER LES SYMBOLES PUIS
!  A LES EXTRAIRE PAR TRI DICHOTOMIQUE.
!
!  8.7.88 FORTRAN 77 POUR SUN4
!  REV 25 JAN 1990
!  REV 15 FEV 1990
!  REV 27 MAR 1990
!  REV 22 JAN 1992   CH.WENGER  J.P.CHAMPION
!  REV    DEC 1992   T.GABARD
! REV    JAN 1995 JPC,CW (PARAMETER)
! MODIF 01/99 V. BOUDON ---> SCHEMA DE POLYADE QUELCONQUE.
! MODIFIE 05/09 V. BOUDON, A. EL HILALI ---> XY3Z/C3vs.
!
! ***  HAMILTONIEN ROVIBRATIONNEL EFFECTIF DES XY4
! ***  RESTRICTION VIBRATIONNELLE D'UNE POLYADE DE CH4
! ***  CALCUL ET STOCKAGE DES ELEMENTS MATRICIELS NON NULS
! ***  A PARTIR DES DONNEES DU FICHIER ISSU DE HMODEL
!
! APPEL : hmatri Pn Nm Dk Jmax
!
!  ******    LIMITATIONS DU PROGRAMME
!
! DIMENSION MAXIMALE D'UN BLOC J,C
!     MXDIMS              !NRCOD,NVCOD,NRVCOD
!
! NB MAXIMUM D'OPERATEURS ROVIBRATIONNELS
!     MXOPH               !ICODR:ICODV:IGAV
!
! NB MAXIMUM D'OPERATEURS VIBRATIONNELS
!     MXOPVH              !LIR:KOR:EMRV
!
! NB MAXIMUM DE SOUS-NIVEAUX VIBRATIONNELS
!     MXSNV               !EMRD
!
! NB MAXIMUM D'ELEMENTS MATRICIELS NON NULS D'UN OPERATEUR
!     MXELMH              !H:LI:KO
!
! NB MAXIMUM D'ELEMENTS MATRICIELS REDUITS NON NULS D'UN OPERATEUR
! VIBRATIONNEL
!     MXEMR               !LIR:KOR:EMRV
!
!
      use mod_dppr
      use mod_par_tds
      use mod_com_fdate
      use mod_main_hmatri
      IMPLICIT NONE

      real(kind=dppr)  :: ADUM,AJ,AJMAX

      integer          :: JCL_SUP,NB_SUP,NFB_SUP
      integer          :: I,IC,ICOO,ICOPH,ICOPL,ICOPS,IDUM,IEL,IGE,IGEV,IGREV,IKC,IMR
      integer          :: IO,IOPH,IOV,IP,ISV
      integer          :: J,JC,JL
      integer          :: NEL,NELMA,NFB,NNIV,NSV

      character(len = NBCTIT)  :: IDENT,TITRE
      character(len =  11) ,dimension(3)  :: CARG
      character(len =  40)  :: IDEMR
      character(len = 120)  :: FCFB,FEMRV,FSEM
!
1000  FORMAT(A)
1001  FORMAT(I4,A)
1007  FORMAT(4X,I5,5X,I5,4X,F19.13)
1008  FORMAT(I4,A,I8,I8,I8,I8)                                                                     ! cf 1313 de hmodel.f
1010  FORMAT(/,              &
             'HMATRI : ',A)
1011  FORMAT(' HMATRI -> J = ',F5.1,F5.1)
1030  FORMAT(/,     &
             I4,/)
1031  FORMAT(/////,   &
             I5,/  )
2000  FORMAT(' <  ',A)
2001  FORMAT(' >> ',A)
3000  FORMAT(I2)
8000  FORMAT(' !!! HMATRI : STOP ON ERROR')
8001  FORMAT(' !!! ERROR OPENING CONTROL FILE')
8002  FORMAT(' !!! INCOMPLETE CONTROL FILE')
8113  FORMAT(' !!! INCOMPATIBLE JMAX : ',F5.1)
8114  FORMAT(' !!! JMAX TOO LARGE'                ,/,   &
             ' !!! MXJ+2  EXCEEDED : ',F8.1,'>',I3   )
8119  FORMAT(' !!! UNEXPECTED EOF IN BASIS FUNCTIONS CODE FILE')
!
! ARRAY INITIAL ALLOCATION
!
      CALL ALLOC_ALL
!
      OPEN(10,FILE=CTRLF,ERR=9998,STATUS='OLD')
      READ(10,1000,END=9997) FDATE
      PRINT 1010,            FDATE
      DO I=1,3
        READ(10,1000,END=9997) CARG(I)
      ENDDO
      READ(CARG(2)(2:3),3000) NNIV
      READ(10,*,END=9997) AJMAX
      FEMRV = 'MH_'//TRIM(CARG(1))//'_'//CARG(3)
      FCFB  = 'FN_'//TRIM(CARG(1))//'_'
      FSEM  = 'HA_'//TRIM(CARG(1))//'_'//TRIM(CARG(3))//'_'
      CLOSE(10)
      PRINT 2000, TRIM(FEMRV)
      PRINT 2000, TRIM(FCFB)
      PRINT 2001, TRIM(FSEM)
      IF( AJMAX .LT. 0.D0 ) THEN
        PRINT 8113, AJMAX
        GOTO  9999
      ENDIF
!
! APPLICATION DES DIRECTIVES
!
      IF( AJMAX .GT. MXJ+2 ) THEN
        PRINT 8114, AJMAX,MXJ+2
        GOTO  9999
      ENDIF
      OPEN(20,STATUS='OLD',FILE=FEMRV)                                                             ! FICHIER DES E.M.R.V.
      OPEN(30,STATUS='OLD',FILE=FCFB,FORM='UNFORMATTED')                                           ! FICHIER DE CODES DES FCTS DE BASE
      OPEN(40,FILE=FSEM,FORM='UNFORMATTED',STATUS='UNKNOWN')                                       ! FICHIER DE STOCKAGE DES E.M.
      CALL FACTO
!
! *** REMISE A ZERO.
!
      KOR  = 0
      LIR  = 0
      EMRV = 0.D0
!
! LECTURE DES CARACTERISTIQUES GENERALES
!
      DO I=1,3
        READ (20,1000) TITRE
        WRITE(40)      TITRE
        READ(30,END=3994)
      ENDDO
      READ (20,1000) IDEMR
      WRITE(40)      IDEMR
      READ(30,END=3994)
      DO I=1,4+NNIV
        READ (20,1000) TITRE
        WRITE(40)      TITRE
        READ(30,END=3994)
      ENDDO
      READ (20,1001) NSV,TITRE
      WRITE(40)      NSV,TITRE
      READ(30,END=3994)
      READ (20,1000) TITRE
      WRITE(40)      TITRE
      READ(30,END=3994)
      DO ISV=1,NSV
        READ (20,1000) IDENT(:122)                                                                 ! cf. FORMAT 1002 *mod*.f
        WRITE(40)      IDENT(:122)                                                                 ! cf. FORMAT 1002 *mod*.f
        READ(30,END=3994)
      ENDDO
      DO I=1,3
        READ (20,1000) TITRE
        WRITE(40)      TITRE
      ENDDO
      WRITE(40) ' ROVIBRATIONAL MATRIX ELEMENTS  -  ',FDATE
!
! LECTURE DES ELEMENTS MATRICIELS REDUITS
!
      IOV = 0
!
5     READ(20,1000) TITRE
      IF( TITRE(1:3) .EQ. '   ' ) GOTO 2201
      IOV = IOV+1
      IF( IOV .GT. MXOPVH ) CALL RESIZE_MXOPVH
      READ(20,1031) NEL
      DO WHILE( NEL .GT. MXEMR )
        CALL RESIZE_MXEMR
      ENDDO
!
!      KOR <=> < | BRA
!      LIR <=> | > KET
!
      DO IEL=1,NEL
        READ(20,1007) KOR(IEL,IOV),LIR(IEL,IOV),EMRV(IEL,IOV)
      ENDDO
      DO I=1,4
        READ(20,1000)
      ENDDO
      GOTO 5
!
2201  CALL DEBUG( 'HMATRI => MXOPVH=',IOV)
!
! OPERATEURS DE L'HAMILTONIEN
! NBOPH EST LE NOMBRE D'OPERATEURS ROVIBRATIONNELS.
! IOPH EST L'INDICE DE L'OPERATEUR ROVIBRATIONNEL.
!
      READ (20,1030) NBOPH
      WRITE(40)      NBOPH,' ROVIBRATIONAL OPERATORS'
      DO WHILE( NBOPH .GT. MXOPH )
        CALL RESIZE_MXOPH
      ENDDO
      DO IOPH=1,NBOPH
        READ(20,1008) IO,IDENT(:NBCLAB-4),                             &                           ! cf 1313 de hmodel.f
                      ICODR(IOPH),ICODV(IOPH),ICODS(IOPH),ICODL(IOPH)
        ICOO         = ICODV(IOPH)
        IGAV(IOPH)   = ICOO/10-(ICOO/100)*10
        IGAEV(IOPH)  = ICOO/100-(ICOO/1000)*10
        IKAC(IOPH)   = ICOO/1000-(ICOO/10000)*10
        IGAREV(IOPH) = ICOO-(ICOO/10)*10
      ENDDO
!
! IGAV EST KC=SYMETRIE DE L OPERATEUR DE VIBRATION
!
! ***  BOUCLE J
!
      JCL_SUP = -1
      NB_SUP  = -1
      NFB_SUP = -1
E12:  DO J=1,INT(2.D0*AJMAX),2
        AJ = DBLE(J)/2.D0
!
! ***  BOUCLE IC
!
E13:    DO IC=1,MXSYM
          READ(30,END=3994) ADUM,IDUM,NELMA,NFB
          DO WHILE( NFB .GT. MXDIMS )
            CALL RESIZE_MXDIMS
          ENDDO
          IF( NFB .GT. NFB_SUP ) NFB_SUP = NFB
          WRITE(40) AJ,IC,NELMA,NFB
          IF( NFB .EQ. 0 ) CYCLE E13
          READ (30,END=3994) (NVCOD(IP),NRCOD(IP),NRVCOD(IP),IP=1,NFB)
          WRITE(40)          (NVCOD(IP),NRCOD(IP),NRVCOD(IP),IP=1,NFB)
!
! ***  BOUCLE OP H.
!
E14:      DO IOPH=1,NBOPH
            EMRD = 0.D0
            IOV = ICODV(IOPH)/10000
            DO IMR=1,MXEMR
              IF( LIR(IMR,IOV) .EQ. 0 ) GOTO 639
!
! *** JL <=> KET
! *** JC <=> BRA
!
              JL = LIR(IMR,IOV)
              JC = KOR(IMR,IOV)
              DO WHILE( JC .GT. MXSNV .OR.    &
                        JL .GT. MXSNV      )
                CALL RESIZE_MXSNV
              ENDDO
              IF( JC .GT. JCL_SUP ) JCL_SUP = JC
              IF( JL .GT. JCL_SUP ) JCL_SUP = JL
              EMRD(JL,JC) = EMRV(IMR,IOV)
            ENDDO
!
639         IKC   = IKAC(IOPH)
            IGE   = IGAE(IOPH)
            IGEV  = IGAEV(IOPH)
            IGREV = IGAREV(IOPH)
            ICOPH = ICODR(IOPH)
            ICOPS = ICODS(IOPH)
            ICOPL = ICODL(IOPH)
            CALL CALEM(AJ,IC,NFB,ICOPH,ICOPS,ICOPL,IKC,IGE,IGEV,IGREV)
            IF( NBELM .GT. NB_SUP ) NB_SUP = NBELM
            WRITE(40) AJ,IC,NELMA,NFB,IOPH,NBELM
            IF( NBELM .EQ. 0 ) CYCLE E14
            WRITE(40) (LI(I),KO(I),H(I),I=1,NBELM)
          ENDDO E14
        ENDDO E13
      ENDDO E12
      CALL DEBUG( 'HMATRI => MXSNV=',JCL_SUP)
      CALL DEBUG( 'HMATRI => MXELMH=',NB_SUP)
      CALL DEBUG( 'HMATRI => MXDIMS=',NFB_SUP)
      CLOSE(20)
      CLOSE(30)
      CLOSE(40)
      PRINT 1011, AJ,AJMAX
      GOTO  9000                                                                                   ! BONNE FIN
!
3994  PRINT 8119
      GOTO  9999
9997  PRINT 8002
      CLOSE(10)
      GOTO  9999
9998  PRINT 8001
9999  PRINT 8000
!
9000  PRINT *
      END PROGRAM HMATRI
!
!CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
! ***  CALCULE LES ELEM. MAT. DES OP. DE H.
!
! SMIL G.P.  J.P.C. JUIL 85
! MOD. DEC 92 T.GABARD
! MOD. 2008 A. EL HILALI
!
      SUBROUTINE CALEM(AJ,IC,NFB,ICOR,ICOS,ICOL,IKC,IGE,IGEV,IGREV)
      use mod_dppr
      use mod_par_tds
      use mod_main_hmatri
      IMPLICIT NONE
      real(kind=dppr)  :: AJ
      real(kind=dppr)  :: XK
      integer          :: IC,NFB,ICOR,ICOS,ICOL,IKC,IGE,IGEV,IGREV
      integer          :: IAV1,IAV2,IKE1,IKE2,IKEV1,IKEV2,IKREV1,IKREV2,IL,ILL,ILS,IS,IX
      integer          :: NK

! functions
      real(kind=dppr)  :: EMRRO
      integer          :: IDIMK

      real(kind=dppr)  :: COEF
      real(kind=dppr)  :: FROT
      real(kind=dppr)  :: HH
      real(kind=dppr)  :: NFK
      real(kind=dppr)  :: RMUN
      real(kind=dppr)  :: XXK

      integer          :: I1,I2,IBRA,IG
      integer          :: IF9,IFK,IKET,IKR1,IKR2,IO
      integer          :: KR
!
      RMUN = -1.D0
      COEF =  1.D0
!
! IO = DEGRE OP. ROTATIONNEL
!
      IO = ICOR/1000
!
! KR = RANG OP. ROTATIONNEL
!
      KR = ICOR/100-(ICOR/1000)*10
!
! IG = SYMETRIE OP. ROTATIONNEL
!
      IG = ICOR/10-(ICOR/100)*10
!
!  IGE= SYMERTIE ELECTRONIQUE
!
      IGE = ICOR-(ICOR/10)*10
!CCC***** PARTIE ELECTRONIQUE DE SPIN****
      IS  = ICOS/100
      ILS = ICOS-(ICOS/10)*10
!CCC***** PARTIE ELECTRONIQUE ORBITALE ****
      IL  = ICOL/100
      ILL = ICOL-(ICOL/10)*10
!C
!CC*****  ELEMENTS MATRICIELS REDUITS DE L OPERATEUR ROTATIONNEL ****
!C
      IF( KR .NE. 0 .OR.           &
          IS .NE. 0 .OR.           &
          IL .NE. 0      ) GOTO 6
      IX   = NINT(DBLE(IO)/2.D0)
      COEF = SQRT(DBLE(IDIMK(DBLE(IKC-2))))*((-SQRT(3.D0/16.D0))**IX)
!
6     NBELM = 0
! IJ,IVJ,NRJ,ICRJ (J=1,2) : NUM. SS NIVEAU VIBRATIONNEL, SYMETRIE VIBR-
! ATIONNELLE, INDICE DE MULTIPLICITE ET SYMETRIE ROTATIONNELLE DES KET
! ET BRAS
!****** AJK EST LA SYMETRIE ROTATIONNELLE
!****** AK  EST LA SYMETRIE TOTALE
!
E3:   DO IKET=1,NFB
        I1     = NVCOD(IKET)/1000000
        IAV1   = NVCOD(IKET)-100*(NVCOD(IKET)/100)
        IKE1   = NVCOD(IKET)/10000-100*(NVCOD(IKET)/1000000)
        IKEV1  = NVCOD(IKET)/100-100*(NVCOD(IKET)/10000)
        IKR1   = NRCOD(IKET)
        IKREV1 = NRVCOD(IKET)
E4:     DO IBRA=1,IKET
          I2 = NVCOD(IBRA)/1000000
          IF( EMRD(I1,I2) .EQ. 0.D0 ) CYCLE E4
          IAV2   = NVCOD(IBRA)-100*(NVCOD(IBRA)/100)
          IKE2   = NVCOD(IBRA)/10000-100*(NVCOD(IBRA)/1000000)
          IKEV2  = NVCOD(IBRA)/100-100*(NVCOD(IBRA)/10000)
          IKR2   = NRCOD(IBRA)
          IKREV2 = NRVCOD(IBRA)
!CCC IAV EST LA SYMETRIE VIBRATIONNEL (DONC -2)
!CCC IKE EST LA SYMETRIE ELECTRONIQUE (-9.5)
!CCC ICV EST LA SYMETRIE VIBRONIQUE   (-9.5)
          CALL KIV3C(DBLE(IGREV-2),DBLE(IKREV2-2),DBLE(IKREV1-2),1,IC,IC,XK,NK)
          CALL KSU2CV(DBLE(KR),AJ,AJ,DBLE(IG-2),DBLE(IKR2)-0.5D0,DBLE(IKR1)-0.5D0,XXK,IFK)
          CALL NF9K(DBLE(IKR1)-0.5D0,DBLE(IKEV1)-(MXSYR+0.5D0),DBLE(IKREV1-2),  &
                    DBLE(IG-2),DBLE(IGEV-2),DBLE(IGREV-2),                      &
                    DBLE(IKR2)-0.5D0,DBLE(IKEV2)-(MXSYR+0.5D0),DBLE(IKREV2-2),  &
                    NFK,IF9)
          FROT = SQRT(DBLE(IDIMK(DBLE(IKR1)-0.5D0))*  &
                      DBLE(IDIMK(DBLE(IGREV-2)))*     &
                      DBLE(IDIMK(DBLE(IKREV2-2)))/    &
                      (2.D0*AJ+1.D0))
          IF( NK+IFK+IF9 .EQ. 2 .OR.                 &
              NK+IFK+IF9 .EQ. 3      ) FROT = -FROT
          HH = -FROT*XK*XXK*NFK*EMRRO(IO,KR,AJ)*EMRD(I1,I2)
          IF( HH    .EQ. 0.D0   ) CYCLE E4
          NBELM = NBELM+1
          IF( NBELM .GT. MXELMH ) CALL RESIZE_MXELMH
          LI(NBELM) = IKET
          KO(NBELM) = IBRA
          H(NBELM)  = HH*COEF
        ENDDO E4
      ENDDO E3
!
      RETURN
      END SUBROUTINE CALEM
